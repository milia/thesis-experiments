Wed Nov 01 14.44.13 EET 2017
Dataset: vehicle
Test set size: 423
Initial Labelled set size: 84
Initial Unlabelled set size: 339
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Wed Nov 01 14.44.13 EET 2017
		
Iteration: 1
Labeled set size: 84
Unlabelled set size: 339
	
Mean region size: 42.61229314420804
Incorrectly Classified Instances: 56.973995271867615
Correctly Classified Instances: 43.026004728132385
Weighted Precision: 0.47286272715546157
Weighted AreaUnderROC: 0.7072352509962665
Root mean squared error: 0.492151805160643
Relative absolute error: 77.18637208446364
Root relative squared error: 113.65759087666301
Weighted TruePositiveRate: 0.4302600472813239
Weighted MatthewsCorrelation: 0.26070341097941313
Weighted FMeasure: 0.4275458007359552
Iteration time: 11.0
Weighted AreaUnderPRC: 0.4702915387045143
Mean absolute error: 0.2894488953167387
Coverage of cases: 65.2482269503546
Instances selection time: 10.0
Test time: 17.0
Accumulative iteration time: 11.0
Weighted Recall: 0.4302600472813239
Weighted FalsePositiveRate: 0.18583439534404564
Kappa statistic: 0.24367896251891863
Training time: 1.0
		
Iteration: 2
Labeled set size: 104
Unlabelled set size: 319
	
Mean region size: 39.00709219858156
Incorrectly Classified Instances: 57.91962174940898
Correctly Classified Instances: 42.08037825059102
Weighted Precision: 0.4680723513738207
Weighted AreaUnderROC: 0.6998121208784366
Root mean squared error: 0.5048901932658327
Relative absolute error: 78.22631031670578
Root relative squared error: 116.59939559729229
Weighted TruePositiveRate: 0.42080378250591016
Weighted MatthewsCorrelation: 0.24946358273726998
Weighted FMeasure: 0.41190883518763044
Iteration time: 10.0
Weighted AreaUnderPRC: 0.4636022091305845
Mean absolute error: 0.29334866368764667
Coverage of cases: 59.1016548463357
Instances selection time: 9.0
Test time: 17.0
Accumulative iteration time: 21.0
Weighted Recall: 0.42080378250591016
Weighted FalsePositiveRate: 0.18861866039029365
Kappa statistic: 0.23150791590968073
Training time: 1.0
		
Iteration: 3
Labeled set size: 124
Unlabelled set size: 299
	
Mean region size: 38.2387706855792
Incorrectly Classified Instances: 57.91962174940898
Correctly Classified Instances: 42.08037825059102
Weighted Precision: 0.4712203600689114
Weighted AreaUnderROC: 0.6956003910524258
Root mean squared error: 0.5053773030316998
Relative absolute error: 78.50829601978754
Root relative squared error: 116.71188877907159
Weighted TruePositiveRate: 0.42080378250591016
Weighted MatthewsCorrelation: 0.25051744046448843
Weighted FMeasure: 0.4080510616829452
Iteration time: 10.0
Weighted AreaUnderPRC: 0.4653748839995601
Mean absolute error: 0.29440611007420325
Coverage of cases: 58.156028368794324
Instances selection time: 9.0
Test time: 16.0
Accumulative iteration time: 31.0
Weighted Recall: 0.42080378250591016
Weighted FalsePositiveRate: 0.1884382076510192
Kappa statistic: 0.23165605237209094
Training time: 1.0
		
Iteration: 4
Labeled set size: 144
Unlabelled set size: 279
	
Mean region size: 38.53427895981088
Incorrectly Classified Instances: 58.392434988179666
Correctly Classified Instances: 41.607565011820334
Weighted Precision: 0.47384114232732477
Weighted AreaUnderROC: 0.6977324318225526
Root mean squared error: 0.5043379030348534
Relative absolute error: 78.19433058784698
Root relative squared error: 116.4718496318816
Weighted TruePositiveRate: 0.4160756501182033
Weighted MatthewsCorrelation: 0.24630723987985512
Weighted FMeasure: 0.3979314772397274
Iteration time: 9.0
Weighted AreaUnderPRC: 0.46868158178753283
Mean absolute error: 0.29322873970442614
Coverage of cases: 60.7565011820331
Instances selection time: 8.0
Test time: 16.0
Accumulative iteration time: 40.0
Weighted Recall: 0.4160756501182033
Weighted FalsePositiveRate: 0.18995577722933507
Kappa statistic: 0.2254929577464789
Training time: 1.0
		
Iteration: 5
Labeled set size: 164
Unlabelled set size: 259
	
Mean region size: 37.70685579196218
Incorrectly Classified Instances: 58.392434988179666
Correctly Classified Instances: 41.607565011820334
Weighted Precision: 0.46621241071676683
Weighted AreaUnderROC: 0.6938156660682971
Root mean squared error: 0.5056196024776183
Relative absolute error: 78.43613782560948
Root relative squared error: 116.7678454392018
Weighted TruePositiveRate: 0.4160756501182033
Weighted MatthewsCorrelation: 0.24356127549234335
Weighted FMeasure: 0.39693026971830603
Iteration time: 9.0
Weighted AreaUnderPRC: 0.46674003080579113
Mean absolute error: 0.29413551684603556
Coverage of cases: 58.86524822695036
Instances selection time: 8.0
Test time: 16.0
Accumulative iteration time: 49.0
Weighted Recall: 0.4160756501182033
Weighted FalsePositiveRate: 0.1899859164726391
Kappa statistic: 0.22546999169730753
Training time: 1.0
		
Iteration: 6
Labeled set size: 184
Unlabelled set size: 239
	
Mean region size: 36.170212765957444
Incorrectly Classified Instances: 57.21040189125296
Correctly Classified Instances: 42.78959810874704
Weighted Precision: 0.5049797474640609
Weighted AreaUnderROC: 0.7035423065361253
Root mean squared error: 0.5073375345653967
Relative absolute error: 77.33137876767275
Root relative squared error: 117.16458486053314
Weighted TruePositiveRate: 0.42789598108747046
Weighted MatthewsCorrelation: 0.268843720591513
Weighted FMeasure: 0.3913963613417975
Iteration time: 8.0
Weighted AreaUnderPRC: 0.46468903602772077
Mean absolute error: 0.2899926703787728
Coverage of cases: 58.392434988179666
Instances selection time: 7.0
Test time: 18.0
Accumulative iteration time: 57.0
Weighted Recall: 0.42789598108747046
Weighted FalsePositiveRate: 0.1847350802564681
Kappa statistic: 0.24251886932070446
Training time: 1.0
		
Iteration: 7
Labeled set size: 204
Unlabelled set size: 219
	
Mean region size: 34.810874704491724
Incorrectly Classified Instances: 56.501182033096924
Correctly Classified Instances: 43.498817966903076
Weighted Precision: 0.521485717230398
Weighted AreaUnderROC: 0.7080732836261645
Root mean squared error: 0.5087615205275829
Relative absolute error: 76.76324144163023
Root relative squared error: 117.49344033196934
Weighted TruePositiveRate: 0.43498817966903075
Weighted MatthewsCorrelation: 0.2821452767266471
Weighted FMeasure: 0.38704479014030874
Iteration time: 8.0
Weighted AreaUnderPRC: 0.46311236614804857
Mean absolute error: 0.2878621554061134
Coverage of cases: 57.21040189125296
Instances selection time: 6.0
Test time: 16.0
Accumulative iteration time: 65.0
Weighted Recall: 0.43498817966903075
Weighted FalsePositiveRate: 0.18155873620042987
Kappa statistic: 0.25265017667844525
Training time: 2.0
		
Iteration: 8
Labeled set size: 224
Unlabelled set size: 199
	
Mean region size: 34.63356973995272
Incorrectly Classified Instances: 58.392434988179666
Correctly Classified Instances: 41.607565011820334
Weighted Precision: 0.5513598530027294
Weighted AreaUnderROC: 0.7179990240577098
Root mean squared error: 0.5161659186116613
Relative absolute error: 78.21645951229567
Root relative squared error: 119.20341282278125
Weighted TruePositiveRate: 0.4160756501182033
Weighted MatthewsCorrelation: 0.2662292358966999
Weighted FMeasure: 0.3477016451552796
Iteration time: 8.0
Weighted AreaUnderPRC: 0.48597123489325944
Mean absolute error: 0.29331172317110876
Coverage of cases: 55.7919621749409
Instances selection time: 6.0
Test time: 16.0
Accumulative iteration time: 73.0
Weighted Recall: 0.4160756501182033
Weighted FalsePositiveRate: 0.18713619564916129
Kappa statistic: 0.22845559674489357
Training time: 2.0
		
Iteration: 9
Labeled set size: 244
Unlabelled set size: 179
	
Mean region size: 33.924349881796694
Incorrectly Classified Instances: 58.392434988179666
Correctly Classified Instances: 41.607565011820334
Weighted Precision: 0.5593009785049463
Weighted AreaUnderROC: 0.712526493022706
Root mean squared error: 0.516953935466827
Relative absolute error: 78.0758545198096
Root relative squared error: 119.38539752016361
Weighted TruePositiveRate: 0.4160756501182033
Weighted MatthewsCorrelation: 0.26819237762963216
Weighted FMeasure: 0.3504418809039936
Iteration time: 7.0
Weighted AreaUnderPRC: 0.47518670708885324
Mean absolute error: 0.29278445444928597
Coverage of cases: 55.319148936170215
Instances selection time: 5.0
Test time: 16.0
Accumulative iteration time: 80.0
Weighted Recall: 0.4160756501182033
Weighted FalsePositiveRate: 0.18715622469127235
Kappa statistic: 0.2284043158135722
Training time: 2.0
		
Iteration: 10
Labeled set size: 264
Unlabelled set size: 159
	
Mean region size: 34.160756501182036
Incorrectly Classified Instances: 58.628841607565015
Correctly Classified Instances: 41.371158392434985
Weighted Precision: 0.5402865301188284
Weighted AreaUnderROC: 0.7204889091580097
Root mean squared error: 0.5135126562224156
Relative absolute error: 77.50744938771415
Root relative squared error: 118.59066812091658
Weighted TruePositiveRate: 0.41371158392434987
Weighted MatthewsCorrelation: 0.2604838062858547
Weighted FMeasure: 0.34387952251660914
Iteration time: 6.0
Weighted AreaUnderPRC: 0.4877486358493427
Mean absolute error: 0.29065293520392804
Coverage of cases: 55.55555555555556
Instances selection time: 4.0
Test time: 17.0
Accumulative iteration time: 86.0
Weighted Recall: 0.41371158392434987
Weighted FalsePositiveRate: 0.18782953482932307
Kappa statistic: 0.22540057594329171
Training time: 2.0
		
Iteration: 11
Labeled set size: 284
Unlabelled set size: 139
	
Mean region size: 33.983451536643024
Incorrectly Classified Instances: 56.26477541371158
Correctly Classified Instances: 43.73522458628842
Weighted Precision: 0.5490164161450349
Weighted AreaUnderROC: 0.7197617069740153
Root mean squared error: 0.5078239529693037
Relative absolute error: 75.75177037996518
Root relative squared error: 117.27691837910696
Weighted TruePositiveRate: 0.4373522458628842
Weighted MatthewsCorrelation: 0.2932326331548558
Weighted FMeasure: 0.37918014504464564
Iteration time: 6.0
Weighted AreaUnderPRC: 0.48464186971087875
Mean absolute error: 0.28406913892486946
Coverage of cases: 56.02836879432624
Instances selection time: 4.0
Test time: 16.0
Accumulative iteration time: 92.0
Weighted Recall: 0.4373522458628842
Weighted FalsePositiveRate: 0.1800517634447172
Kappa statistic: 0.25645324490202887
Training time: 2.0
		
Iteration: 12
Labeled set size: 304
Unlabelled set size: 119
	
Mean region size: 35.9338061465721
Incorrectly Classified Instances: 57.21040189125296
Correctly Classified Instances: 42.78959810874704
Weighted Precision: 0.5340123789079169
Weighted AreaUnderROC: 0.7300073907813561
Root mean squared error: 0.5059486628486971
Relative absolute error: 76.21812332010249
Root relative squared error: 116.8438386767306
Weighted TruePositiveRate: 0.42789598108747046
Weighted MatthewsCorrelation: 0.2768916708980722
Weighted FMeasure: 0.36425594236892794
Iteration time: 6.0
Weighted AreaUnderPRC: 0.49059466565423715
Mean absolute error: 0.2858179624503844
Coverage of cases: 60.047281323877066
Instances selection time: 4.0
Test time: 16.0
Accumulative iteration time: 98.0
Weighted Recall: 0.42789598108747046
Weighted FalsePositiveRate: 0.18330253334286997
Kappa statistic: 0.24397341211225998
Training time: 2.0
		
Iteration: 13
Labeled set size: 324
Unlabelled set size: 99
	
Mean region size: 35.1063829787234
Incorrectly Classified Instances: 60.7565011820331
Correctly Classified Instances: 39.2434988179669
Weighted Precision: 0.464836434251328
Weighted AreaUnderROC: 0.7104272898397682
Root mean squared error: 0.522144640956156
Relative absolute error: 80.24125141013722
Root relative squared error: 120.58413960478286
Weighted TruePositiveRate: 0.39243498817966904
Weighted MatthewsCorrelation: 0.2142635674670477
Weighted FMeasure: 0.31721051836395414
Iteration time: 4.0
Weighted AreaUnderPRC: 0.4347547262281407
Mean absolute error: 0.30090469278801457
Coverage of cases: 56.02836879432624
Instances selection time: 3.0
Test time: 16.0
Accumulative iteration time: 102.0
Weighted Recall: 0.39243498817966904
Weighted FalsePositiveRate: 0.19530916905265638
Kappa statistic: 0.19702330391106845
Training time: 1.0
		
Iteration: 14
Labeled set size: 344
Unlabelled set size: 79
	
Mean region size: 36.406619385342786
Incorrectly Classified Instances: 60.047281323877066
Correctly Classified Instances: 39.952718676122934
Weighted Precision: 0.4556288289474734
Weighted AreaUnderROC: 0.7069697943675584
Root mean squared error: 0.5170861611004438
Relative absolute error: 79.66456400338565
Root relative squared error: 119.41593372222859
Weighted TruePositiveRate: 0.39952718676122934
Weighted MatthewsCorrelation: 0.22181753897436932
Weighted FMeasure: 0.33328280791566645
Iteration time: 4.0
Weighted AreaUnderPRC: 0.43145137893086055
Mean absolute error: 0.2987421150126962
Coverage of cases: 58.156028368794324
Instances selection time: 2.0
Test time: 15.0
Accumulative iteration time: 106.0
Weighted Recall: 0.39952718676122934
Weighted FalsePositiveRate: 0.19318551021173755
Kappa statistic: 0.20616789561568133
Training time: 2.0
		
Iteration: 15
Labeled set size: 364
Unlabelled set size: 59
	
Mean region size: 37.70685579196218
Incorrectly Classified Instances: 59.810874704491724
Correctly Classified Instances: 40.189125295508276
Weighted Precision: 0.467034239989485
Weighted AreaUnderROC: 0.7258021460155043
Root mean squared error: 0.513111258690457
Relative absolute error: 79.48529115296644
Root relative squared error: 118.49796933166523
Weighted TruePositiveRate: 0.40189125295508277
Weighted MatthewsCorrelation: 0.22833034375928526
Weighted FMeasure: 0.34432277592115834
Iteration time: 4.0
Weighted AreaUnderPRC: 0.44040005386488995
Mean absolute error: 0.29806984182362417
Coverage of cases: 59.810874704491724
Instances selection time: 2.0
Test time: 15.0
Accumulative iteration time: 110.0
Weighted Recall: 0.40189125295508277
Weighted FalsePositiveRate: 0.19242624702189226
Kappa statistic: 0.20922895038238448
Training time: 2.0
		
Iteration: 16
Labeled set size: 384
Unlabelled set size: 39
	
Mean region size: 39.83451536643026
Incorrectly Classified Instances: 60.047281323877066
Correctly Classified Instances: 39.952718676122934
Weighted Precision: 0.4546324439941461
Weighted AreaUnderROC: 0.7280530687311981
Root mean squared error: 0.508738081764644
Relative absolute error: 79.92946434824715
Root relative squared error: 117.48802738153242
Weighted TruePositiveRate: 0.39952718676122934
Weighted MatthewsCorrelation: 0.22224926684162133
Weighted FMeasure: 0.34178974825959696
Iteration time: 3.0
Weighted AreaUnderPRC: 0.4360882483060615
Mean absolute error: 0.2997354913059268
Coverage of cases: 63.593380614657214
Instances selection time: 1.0
Test time: 15.0
Accumulative iteration time: 113.0
Weighted Recall: 0.39952718676122934
Weighted FalsePositiveRate: 0.19320832434565208
Kappa statistic: 0.20611510525576898
Training time: 2.0
		
Iteration: 17
Labeled set size: 404
Unlabelled set size: 19
	
Mean region size: 40.780141843971634
Incorrectly Classified Instances: 60.520094562647756
Correctly Classified Instances: 39.479905437352244
Weighted Precision: 0.4432063879880229
Weighted AreaUnderROC: 0.7334486148009685
Root mean squared error: 0.5037868817117066
Relative absolute error: 79.56225585544408
Root relative squared error: 116.34459670818238
Weighted TruePositiveRate: 0.3947990543735225
Weighted MatthewsCorrelation: 0.2136913409741622
Weighted FMeasure: 0.33922142796262966
Iteration time: 2.0
Weighted AreaUnderPRC: 0.4442418996215035
Mean absolute error: 0.2983584594579153
Coverage of cases: 65.72104018912529
Instances selection time: 0.0
Test time: 15.0
Accumulative iteration time: 115.0
Weighted Recall: 0.3947990543735225
Weighted FalsePositiveRate: 0.19488948543348475
Kappa statistic: 0.19978717744080873
Training time: 2.0
		
Time end:Wed Nov 01 14.44.14 EET 2017