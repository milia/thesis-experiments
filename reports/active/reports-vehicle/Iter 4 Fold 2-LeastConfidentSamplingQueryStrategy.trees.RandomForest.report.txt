Wed Nov 01 14.42.08 EET 2017
Dataset: vehicle
Test set size: 423
Initial Labelled set size: 84
Initial Unlabelled set size: 339
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Wed Nov 01 14.42.08 EET 2017
		
Iteration: 1
Labeled set size: 84
Unlabelled set size: 339
	
Mean region size: 63.12056737588652
Incorrectly Classified Instances: 32.860520094562645
Correctly Classified Instances: 67.13947990543736
Weighted Precision: 0.6612042092930814
Weighted AreaUnderROC: 0.8712903191737744
Root mean squared error: 0.32998316455372206
Relative absolute error: 56.26477541371154
Root relative squared error: 76.2063475532544
Weighted TruePositiveRate: 0.6713947990543735
Weighted MatthewsCorrelation: 0.5566583159732775
Weighted FMeasure: 0.6592479941394205
Iteration time: 23.0
Weighted AreaUnderPRC: 0.7005794119564296
Mean absolute error: 0.21099290780141827
Coverage of cases: 96.45390070921985
Instances selection time: 6.0
Test time: 2.0
Accumulative iteration time: 23.0
Weighted Recall: 0.6713947990543735
Weighted FalsePositiveRate: 0.10935868603940488
Kappa statistic: 0.5622356900351421
Training time: 17.0
		
Iteration: 2
Labeled set size: 104
Unlabelled set size: 319
	
Mean region size: 63.593380614657214
Incorrectly Classified Instances: 31.678486997635932
Correctly Classified Instances: 68.32151300236407
Weighted Precision: 0.6717064382745044
Weighted AreaUnderROC: 0.8731064949802789
Root mean squared error: 0.3302696083268898
Relative absolute error: 56.92671394799052
Root relative squared error: 76.27249890907284
Weighted TruePositiveRate: 0.6832151300236406
Weighted MatthewsCorrelation: 0.5710389854800352
Weighted FMeasure: 0.6628270359402183
Iteration time: 12.0
Weighted AreaUnderPRC: 0.6849675939076655
Mean absolute error: 0.21347517730496446
Coverage of cases: 96.21749408983452
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 35.0
Weighted Recall: 0.6832151300236406
Weighted FalsePositiveRate: 0.10580719672984804
Kappa statistic: 0.5778568875119161
Training time: 11.0
		
Iteration: 3
Labeled set size: 124
Unlabelled set size: 299
	
Mean region size: 60.815602836879435
Incorrectly Classified Instances: 32.860520094562645
Correctly Classified Instances: 67.13947990543736
Weighted Precision: 0.6690010362388445
Weighted AreaUnderROC: 0.8554557962406462
Root mean squared error: 0.3403169152964329
Relative absolute error: 56.51694247438928
Root relative squared error: 78.59282506247145
Weighted TruePositiveRate: 0.6713947990543735
Weighted MatthewsCorrelation: 0.5587957318163552
Weighted FMeasure: 0.6478890478890479
Iteration time: 15.0
Weighted AreaUnderPRC: 0.6637153067999305
Mean absolute error: 0.2119385342789598
Coverage of cases: 95.74468085106383
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 50.0
Weighted Recall: 0.6713947990543735
Weighted FalsePositiveRate: 0.10920935415077074
Kappa statistic: 0.5625027903031385
Training time: 14.0
		
Iteration: 4
Labeled set size: 144
Unlabelled set size: 279
	
Mean region size: 55.20094562647754
Incorrectly Classified Instances: 30.73286052009456
Correctly Classified Instances: 69.26713947990544
Weighted Precision: 0.6694008889029817
Weighted AreaUnderROC: 0.8764878979334164
Root mean squared error: 0.322647880585532
Relative absolute error: 49.45626477541373
Root relative squared error: 74.51233628380766
Weighted TruePositiveRate: 0.6926713947990544
Weighted MatthewsCorrelation: 0.5779887965110417
Weighted FMeasure: 0.6619766571800564
Iteration time: 16.0
Weighted AreaUnderPRC: 0.7099658157597509
Mean absolute error: 0.1854609929078015
Coverage of cases: 95.50827423167848
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 66.0
Weighted Recall: 0.6926713947990544
Weighted FalsePositiveRate: 0.10197286140192012
Kappa statistic: 0.5909334364864463
Training time: 15.0
		
Iteration: 5
Labeled set size: 164
Unlabelled set size: 259
	
Mean region size: 59.04255319148936
Incorrectly Classified Instances: 30.49645390070922
Correctly Classified Instances: 69.50354609929079
Weighted Precision: 0.6799095297816917
Weighted AreaUnderROC: 0.8589299676990549
Root mean squared error: 0.33420099839558404
Relative absolute error: 54.40504334121358
Root relative squared error: 77.18041455485286
Weighted TruePositiveRate: 0.6950354609929078
Weighted MatthewsCorrelation: 0.5849750594214518
Weighted FMeasure: 0.6652654826583572
Iteration time: 19.0
Weighted AreaUnderPRC: 0.6800346516615818
Mean absolute error: 0.20401891252955093
Coverage of cases: 95.74468085106383
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 85.0
Weighted Recall: 0.6950354609929078
Weighted FalsePositiveRate: 0.10114570265506598
Kappa statistic: 0.5940197012082614
Training time: 18.0
		
Iteration: 6
Labeled set size: 184
Unlabelled set size: 239
	
Mean region size: 62.11583924349882
Incorrectly Classified Instances: 31.44208037825059
Correctly Classified Instances: 68.5579196217494
Weighted Precision: 0.6815315937856486
Weighted AreaUnderROC: 0.8739259027437872
Root mean squared error: 0.32458374871376106
Relative absolute error: 55.44523246650905
Root relative squared error: 74.95940587778713
Weighted TruePositiveRate: 0.6855791962174941
Weighted MatthewsCorrelation: 0.5778433883984712
Weighted FMeasure: 0.6768641638236697
Iteration time: 22.0
Weighted AreaUnderPRC: 0.7051354557053268
Mean absolute error: 0.20791962174940892
Coverage of cases: 98.10874704491725
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 107.0
Weighted Recall: 0.6855791962174941
Weighted FalsePositiveRate: 0.10566809334196223
Kappa statistic: 0.5809010861306039
Training time: 21.0
		
Iteration: 7
Labeled set size: 204
Unlabelled set size: 219
	
Mean region size: 59.988179669030735
Incorrectly Classified Instances: 29.078014184397162
Correctly Classified Instances: 70.92198581560284
Weighted Precision: 0.7152738137966724
Weighted AreaUnderROC: 0.8881347051173168
Root mean squared error: 0.3144848314622791
Relative absolute error: 52.04097714736011
Root relative squared error: 72.62716084032037
Weighted TruePositiveRate: 0.7092198581560284
Weighted MatthewsCorrelation: 0.6138275267521027
Weighted FMeasure: 0.700354220802149
Iteration time: 23.0
Weighted AreaUnderPRC: 0.7367748310627715
Mean absolute error: 0.19515366430260042
Coverage of cases: 98.3451536643026
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 130.0
Weighted Recall: 0.7092198581560284
Weighted FalsePositiveRate: 0.09802911990828678
Kappa statistic: 0.6122765312129727
Training time: 22.0
		
Iteration: 8
Labeled set size: 224
Unlabelled set size: 199
	
Mean region size: 59.751773049645394
Incorrectly Classified Instances: 32.38770685579196
Correctly Classified Instances: 67.61229314420804
Weighted Precision: 0.6890240231725712
Weighted AreaUnderROC: 0.864632853938422
Root mean squared error: 0.331020343914247
Relative absolute error: 55.161544523246704
Root relative squared error: 76.44587386645321
Weighted TruePositiveRate: 0.6761229314420804
Weighted MatthewsCorrelation: 0.5723555429107204
Weighted FMeasure: 0.6776640542808735
Iteration time: 27.0
Weighted AreaUnderPRC: 0.6914922647896099
Mean absolute error: 0.20685579196217516
Coverage of cases: 97.63593380614657
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 157.0
Weighted Recall: 0.6761229314420804
Weighted FalsePositiveRate: 0.10994800575453975
Kappa statistic: 0.5676169726995306
Training time: 26.0
		
Iteration: 9
Labeled set size: 244
Unlabelled set size: 179
	
Mean region size: 60.638297872340424
Incorrectly Classified Instances: 32.860520094562645
Correctly Classified Instances: 67.13947990543736
Weighted Precision: 0.6849250863991512
Weighted AreaUnderROC: 0.8789699029133428
Root mean squared error: 0.32347112659394
Relative absolute error: 54.18439716312056
Root relative squared error: 74.70245680563312
Weighted TruePositiveRate: 0.6713947990543735
Weighted MatthewsCorrelation: 0.5664985706466974
Weighted FMeasure: 0.6763778903889222
Iteration time: 29.0
Weighted AreaUnderPRC: 0.71779482289183
Mean absolute error: 0.2031914893617021
Coverage of cases: 98.81796690307328
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 186.0
Weighted Recall: 0.6713947990543735
Weighted FalsePositiveRate: 0.11124852822139679
Kappa statistic: 0.5613408137990719
Training time: 28.0
		
Iteration: 10
Labeled set size: 264
Unlabelled set size: 159
	
Mean region size: 57.6241134751773
Incorrectly Classified Instances: 28.84160756501182
Correctly Classified Instances: 71.15839243498817
Weighted Precision: 0.706152268831018
Weighted AreaUnderROC: 0.8822049640345019
Root mean squared error: 0.3152919019758303
Relative absolute error: 51.41055949566587
Root relative squared error: 72.81354579162189
Weighted TruePositiveRate: 0.7115839243498818
Weighted MatthewsCorrelation: 0.6114194380433802
Weighted FMeasure: 0.7073652507512449
Iteration time: 31.0
Weighted AreaUnderPRC: 0.7142104437181293
Mean absolute error: 0.19278959810874705
Coverage of cases: 97.63593380614657
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 217.0
Weighted Recall: 0.7115839243498818
Weighted FalsePositiveRate: 0.0979698297017432
Kappa statistic: 0.6151706549541018
Training time: 30.0
		
Iteration: 11
Labeled set size: 284
Unlabelled set size: 139
	
Mean region size: 58.56973995271868
Incorrectly Classified Instances: 28.368794326241133
Correctly Classified Instances: 71.63120567375887
Weighted Precision: 0.7154936989496783
Weighted AreaUnderROC: 0.8987006727201435
Root mean squared error: 0.30299919247136264
Relative absolute error: 49.55082742316785
Root relative squared error: 69.9746661350322
Weighted TruePositiveRate: 0.7163120567375887
Weighted MatthewsCorrelation: 0.6198369719553094
Weighted FMeasure: 0.7146866818176395
Iteration time: 34.0
Weighted AreaUnderPRC: 0.7493011154522282
Mean absolute error: 0.18581560283687945
Coverage of cases: 98.58156028368795
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 251.0
Weighted Recall: 0.7163120567375887
Weighted FalsePositiveRate: 0.09642682896056746
Kappa statistic: 0.621394634186364
Training time: 33.0
		
Iteration: 12
Labeled set size: 304
Unlabelled set size: 119
	
Mean region size: 57.09219858156028
Incorrectly Classified Instances: 26.713947990543737
Correctly Classified Instances: 73.28605200945627
Weighted Precision: 0.730311079903679
Weighted AreaUnderROC: 0.8989413384146406
Root mean squared error: 0.30104231852244817
Relative absolute error: 48.47911741528757
Root relative squared error: 69.5227454545618
Weighted TruePositiveRate: 0.7328605200945626
Weighted MatthewsCorrelation: 0.6414619987183342
Weighted FMeasure: 0.7311871436742466
Iteration time: 36.0
Weighted AreaUnderPRC: 0.7569470107789813
Mean absolute error: 0.1817966903073284
Coverage of cases: 97.87234042553192
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 287.0
Weighted Recall: 0.7328605200945626
Weighted FalsePositiveRate: 0.09031138754663566
Kappa statistic: 0.6436314565191458
Training time: 35.0
		
Iteration: 13
Labeled set size: 324
Unlabelled set size: 99
	
Mean region size: 58.86524822695036
Incorrectly Classified Instances: 29.55082742316785
Correctly Classified Instances: 70.44917257683215
Weighted Precision: 0.7058401415180193
Weighted AreaUnderROC: 0.8871859459928229
Root mean squared error: 0.31484169912556736
Relative absolute error: 51.50512214342002
Root relative squared error: 72.7095758969062
Weighted TruePositiveRate: 0.7044917257683215
Weighted MatthewsCorrelation: 0.6050323251833127
Weighted FMeasure: 0.7044450701676224
Iteration time: 39.0
Weighted AreaUnderPRC: 0.7227954374538914
Mean absolute error: 0.19314420803782506
Coverage of cases: 99.05437352245863
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 326.0
Weighted Recall: 0.7044917257683215
Weighted FalsePositiveRate: 0.10045403459807795
Kappa statistic: 0.6055223145675108
Training time: 38.0
		
Iteration: 14
Labeled set size: 344
Unlabelled set size: 79
	
Mean region size: 57.6241134751773
Incorrectly Classified Instances: 27.423167848699762
Correctly Classified Instances: 72.57683215130024
Weighted Precision: 0.7297274424584629
Weighted AreaUnderROC: 0.8896975284900382
Root mean squared error: 0.3092926422246074
Relative absolute error: 50.149724192277375
Root relative squared error: 71.42807609869908
Weighted TruePositiveRate: 0.7257683215130024
Weighted MatthewsCorrelation: 0.6347437074601497
Weighted FMeasure: 0.7255700698287668
Iteration time: 40.0
Weighted AreaUnderPRC: 0.7415393687883405
Mean absolute error: 0.18806146572104016
Coverage of cases: 98.3451536643026
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 366.0
Weighted Recall: 0.7257683215130024
Weighted FalsePositiveRate: 0.09314424023279645
Kappa statistic: 0.633971131252098
Training time: 40.0
		
Iteration: 15
Labeled set size: 364
Unlabelled set size: 59
	
Mean region size: 56.85579196217494
Incorrectly Classified Instances: 29.314420803782507
Correctly Classified Instances: 70.68557919621749
Weighted Precision: 0.7028559189974489
Weighted AreaUnderROC: 0.9015747059414377
Root mean squared error: 0.303291634574442
Relative absolute error: 49.20409771473599
Root relative squared error: 70.04220274580629
Weighted TruePositiveRate: 0.706855791962175
Weighted MatthewsCorrelation: 0.6059747343533769
Weighted FMeasure: 0.7042193008932366
Iteration time: 42.0
Weighted AreaUnderPRC: 0.7439971627570198
Mean absolute error: 0.18451536643025995
Coverage of cases: 99.05437352245863
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 408.0
Weighted Recall: 0.706855791962175
Weighted FalsePositiveRate: 0.09934638244080347
Kappa statistic: 0.6088182211416554
Training time: 41.0
		
Iteration: 16
Labeled set size: 384
Unlabelled set size: 39
	
Mean region size: 55.673758865248224
Incorrectly Classified Instances: 29.314420803782507
Correctly Classified Instances: 70.68557919621749
Weighted Precision: 0.7086830261657256
Weighted AreaUnderROC: 0.8887565067144075
Root mean squared error: 0.30952186111147334
Relative absolute error: 48.573680063041756
Root relative squared error: 71.48101193311324
Weighted TruePositiveRate: 0.706855791962175
Weighted MatthewsCorrelation: 0.608764898428049
Weighted FMeasure: 0.7047219178910105
Iteration time: 45.0
Weighted AreaUnderPRC: 0.7345928638959563
Mean absolute error: 0.18215130023640658
Coverage of cases: 97.16312056737588
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 453.0
Weighted Recall: 0.706855791962175
Weighted FalsePositiveRate: 0.09891655714174903
Kappa statistic: 0.60900193068901
Training time: 44.0
		
Iteration: 17
Labeled set size: 404
Unlabelled set size: 19
	
Mean region size: 55.9692671394799
Incorrectly Classified Instances: 27.89598108747045
Correctly Classified Instances: 72.10401891252955
Weighted Precision: 0.7142250099411986
Weighted AreaUnderROC: 0.8986679728330763
Root mean squared error: 0.30379786735268216
Relative absolute error: 47.37588652482264
Root relative squared error: 70.1591121981221
Weighted TruePositiveRate: 0.7210401891252955
Weighted MatthewsCorrelation: 0.6234620069504971
Weighted FMeasure: 0.7172949423847095
Iteration time: 45.0
Weighted AreaUnderPRC: 0.7498001059089568
Mean absolute error: 0.1776595744680849
Coverage of cases: 98.58156028368795
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 498.0
Weighted Recall: 0.7210401891252955
Weighted FalsePositiveRate: 0.09460333480897135
Kappa statistic: 0.6278601623834128
Training time: 44.0
		
Time end:Wed Nov 01 14.42.09 EET 2017