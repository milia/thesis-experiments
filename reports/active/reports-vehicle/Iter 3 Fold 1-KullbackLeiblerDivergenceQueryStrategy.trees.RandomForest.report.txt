Wed Nov 01 14.41.03 EET 2017
Dataset: vehicle
Test set size: 423
Initial Labelled set size: 84
Initial Unlabelled set size: 339
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Wed Nov 01 14.41.03 EET 2017
		
Iteration: 1
Labeled set size: 84
Unlabelled set size: 339
	
Mean region size: 65.83924349881796
Incorrectly Classified Instances: 34.04255319148936
Correctly Classified Instances: 65.95744680851064
Weighted Precision: 0.6566067908552466
Weighted AreaUnderROC: 0.8656907066955597
Root mean squared error: 0.33663260834679115
Relative absolute error: 60.83530338849491
Root relative squared error: 77.7419708188103
Weighted TruePositiveRate: 0.6595744680851063
Weighted MatthewsCorrelation: 0.5440173406433018
Weighted FMeasure: 0.6527047970037235
Iteration time: 25.0
Weighted AreaUnderPRC: 0.6962671285779507
Mean absolute error: 0.2281323877068559
Coverage of cases: 96.45390070921985
Instances selection time: 5.0
Test time: 4.0
Accumulative iteration time: 25.0
Weighted Recall: 0.6595744680851063
Weighted FalsePositiveRate: 0.11454268356308751
Kappa statistic: 0.5457481001991155
Training time: 20.0
		
Iteration: 2
Labeled set size: 104
Unlabelled set size: 319
	
Mean region size: 60.46099290780142
Incorrectly Classified Instances: 33.80614657210402
Correctly Classified Instances: 66.19385342789599
Weighted Precision: 0.6420190186547194
Weighted AreaUnderROC: 0.872528909454694
Root mean squared error: 0.3298219306082685
Relative absolute error: 56.044129235618584
Root relative squared error: 76.16911216853035
Weighted TruePositiveRate: 0.6619385342789598
Weighted MatthewsCorrelation: 0.5397585201421455
Weighted FMeasure: 0.6448608745872841
Iteration time: 12.0
Weighted AreaUnderPRC: 0.7006717279188599
Mean absolute error: 0.21016548463356968
Coverage of cases: 95.27186761229315
Instances selection time: 2.0
Test time: 2.0
Accumulative iteration time: 37.0
Weighted Recall: 0.6619385342789598
Weighted FalsePositiveRate: 0.11373531439469113
Kappa statistic: 0.549037149694706
Training time: 10.0
		
Iteration: 3
Labeled set size: 124
Unlabelled set size: 299
	
Mean region size: 56.7966903073286
Incorrectly Classified Instances: 31.678486997635932
Correctly Classified Instances: 68.32151300236407
Weighted Precision: 0.6776408118676644
Weighted AreaUnderROC: 0.8800795228297412
Root mean squared error: 0.325674422985954
Relative absolute error: 53.42789598108748
Root relative squared error: 75.21128631164665
Weighted TruePositiveRate: 0.6832151300236406
Weighted MatthewsCorrelation: 0.5746993032534576
Weighted FMeasure: 0.6743559565377737
Iteration time: 14.0
Weighted AreaUnderPRC: 0.7206727913391617
Mean absolute error: 0.20035460992907805
Coverage of cases: 95.27186761229315
Instances selection time: 2.0
Test time: 2.0
Accumulative iteration time: 51.0
Weighted Recall: 0.6832151300236406
Weighted FalsePositiveRate: 0.1068920288199876
Kappa statistic: 0.5770946802954563
Training time: 12.0
		
Iteration: 4
Labeled set size: 144
Unlabelled set size: 279
	
Mean region size: 61.997635933806144
Incorrectly Classified Instances: 33.56973995271868
Correctly Classified Instances: 66.43026004728132
Weighted Precision: 0.6411596696129473
Weighted AreaUnderROC: 0.868190996712894
Root mean squared error: 0.3329075294550258
Relative absolute error: 56.32781717888102
Root relative squared error: 76.88170069844496
Weighted TruePositiveRate: 0.6643026004728132
Weighted MatthewsCorrelation: 0.5417672399340443
Weighted FMeasure: 0.6438542623740766
Iteration time: 33.0
Weighted AreaUnderPRC: 0.7039723716492475
Mean absolute error: 0.2112293144208038
Coverage of cases: 94.79905437352245
Instances selection time: 2.0
Test time: 2.0
Accumulative iteration time: 84.0
Weighted Recall: 0.6643026004728132
Weighted FalsePositiveRate: 0.11302847315739994
Kappa statistic: 0.552087217192883
Training time: 31.0
		
Iteration: 5
Labeled set size: 164
Unlabelled set size: 259
	
Mean region size: 60.69739952718676
Incorrectly Classified Instances: 33.56973995271868
Correctly Classified Instances: 66.43026004728132
Weighted Precision: 0.6677939776813369
Weighted AreaUnderROC: 0.8818419623376511
Root mean squared error: 0.33019802067928883
Relative absolute error: 56.32781717888101
Root relative squared error: 76.25596645002761
Weighted TruePositiveRate: 0.6643026004728132
Weighted MatthewsCorrelation: 0.5529292031103827
Weighted FMeasure: 0.6478381418469985
Iteration time: 16.0
Weighted AreaUnderPRC: 0.717936725379973
Mean absolute error: 0.21122931442080378
Coverage of cases: 95.0354609929078
Instances selection time: 2.0
Test time: 2.0
Accumulative iteration time: 100.0
Weighted Recall: 0.6643026004728132
Weighted FalsePositiveRate: 0.1135332404864061
Kappa statistic: 0.551809818010879
Training time: 14.0
		
Iteration: 6
Labeled set size: 184
Unlabelled set size: 239
	
Mean region size: 56.146572104018915
Incorrectly Classified Instances: 32.38770685579196
Correctly Classified Instances: 67.61229314420804
Weighted Precision: 0.6707371819314167
Weighted AreaUnderROC: 0.8726310922574231
Root mean squared error: 0.33693094044453925
Relative absolute error: 53.61702127659576
Root relative squared error: 77.8108676655874
Weighted TruePositiveRate: 0.6761229314420804
Weighted MatthewsCorrelation: 0.5652835308251826
Weighted FMeasure: 0.661264697132434
Iteration time: 17.0
Weighted AreaUnderPRC: 0.7037919767048189
Mean absolute error: 0.2010638297872341
Coverage of cases: 93.85342789598108
Instances selection time: 2.0
Test time: 2.0
Accumulative iteration time: 117.0
Weighted Recall: 0.6761229314420804
Weighted FalsePositiveRate: 0.10944664176286703
Kappa statistic: 0.5677169583314684
Training time: 15.0
		
Iteration: 7
Labeled set size: 204
Unlabelled set size: 219
	
Mean region size: 52.4822695035461
Incorrectly Classified Instances: 33.096926713947994
Correctly Classified Instances: 66.903073286052
Weighted Precision: 0.6484997093491021
Weighted AreaUnderROC: 0.8868274981119605
Root mean squared error: 0.3302338164429236
Relative absolute error: 50.14972419227738
Root relative squared error: 76.26423312753576
Weighted TruePositiveRate: 0.6690307328605201
Weighted MatthewsCorrelation: 0.5493399879275495
Weighted FMeasure: 0.6400540024338398
Iteration time: 18.0
Weighted AreaUnderPRC: 0.7163137007157179
Mean absolute error: 0.1880614657210402
Coverage of cases: 95.0354609929078
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 135.0
Weighted Recall: 0.6690307328605201
Weighted FalsePositiveRate: 0.11049865916873672
Kappa statistic: 0.5588892530465095
Training time: 17.0
		
Iteration: 8
Labeled set size: 224
Unlabelled set size: 199
	
Mean region size: 52.304964539007095
Incorrectly Classified Instances: 28.368794326241133
Correctly Classified Instances: 71.63120567375887
Weighted Precision: 0.7022289940115418
Weighted AreaUnderROC: 0.8959039612218573
Root mean squared error: 0.314954310169094
Relative absolute error: 47.0921985815603
Root relative squared error: 72.73558230342373
Weighted TruePositiveRate: 0.7163120567375887
Weighted MatthewsCorrelation: 0.6143119793119285
Weighted FMeasure: 0.6908552298972496
Iteration time: 20.0
Weighted AreaUnderPRC: 0.738209955095967
Mean absolute error: 0.1765957446808511
Coverage of cases: 94.32624113475177
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 155.0
Weighted Recall: 0.7163120567375887
Weighted FalsePositiveRate: 0.09479016651586558
Kappa statistic: 0.621913522773826
Training time: 19.0
		
Iteration: 9
Labeled set size: 244
Unlabelled set size: 179
	
Mean region size: 48.69976359338062
Incorrectly Classified Instances: 28.60520094562648
Correctly Classified Instances: 71.39479905437352
Weighted Precision: 0.7010253381693997
Weighted AreaUnderROC: 0.8898333301683434
Root mean squared error: 0.32064503546864037
Relative absolute error: 45.83136327817181
Root relative squared error: 74.04979901685465
Weighted TruePositiveRate: 0.7139479905437353
Weighted MatthewsCorrelation: 0.6123555427631243
Weighted FMeasure: 0.6895495314839121
Iteration time: 21.0
Weighted AreaUnderPRC: 0.7265916174295287
Mean absolute error: 0.17186761229314426
Coverage of cases: 92.90780141843972
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 176.0
Weighted Recall: 0.7139479905437353
Weighted FalsePositiveRate: 0.09500876158951876
Kappa statistic: 0.619032378116859
Training time: 20.0
		
Iteration: 10
Labeled set size: 264
Unlabelled set size: 159
	
Mean region size: 48.28605200945626
Incorrectly Classified Instances: 31.20567375886525
Correctly Classified Instances: 68.79432624113475
Weighted Precision: 0.67705035304886
Weighted AreaUnderROC: 0.884827473373171
Root mean squared error: 0.32462016356199525
Relative absolute error: 46.3672182821119
Root relative squared error: 74.96781552675932
Weighted TruePositiveRate: 0.6879432624113475
Weighted MatthewsCorrelation: 0.5763725686244717
Weighted FMeasure: 0.6548933930215208
Iteration time: 24.0
Weighted AreaUnderPRC: 0.7234135035853674
Mean absolute error: 0.17387706855791962
Coverage of cases: 93.61702127659575
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 200.0
Weighted Recall: 0.6879432624113475
Weighted FalsePositiveRate: 0.10338350152021744
Kappa statistic: 0.5845412065835293
Training time: 23.0
		
Iteration: 11
Labeled set size: 284
Unlabelled set size: 139
	
Mean region size: 52.600472813238774
Incorrectly Classified Instances: 28.13238770685579
Correctly Classified Instances: 71.8676122931442
Weighted Precision: 0.7093948033630849
Weighted AreaUnderROC: 0.8973096408190708
Root mean squared error: 0.3060076174518116
Relative absolute error: 46.30417651694251
Root relative squared error: 70.66943212395178
Weighted TruePositiveRate: 0.7186761229314421
Weighted MatthewsCorrelation: 0.6181220377897884
Weighted FMeasure: 0.6896826838192444
Iteration time: 27.0
Weighted AreaUnderPRC: 0.7409256401217931
Mean absolute error: 0.17364066193853442
Coverage of cases: 96.45390070921985
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 227.0
Weighted Recall: 0.7186761229314421
Weighted FalsePositiveRate: 0.09317376915612044
Kappa statistic: 0.6254715366701141
Training time: 26.0
		
Iteration: 12
Labeled set size: 304
Unlabelled set size: 119
	
Mean region size: 52.895981087470446
Incorrectly Classified Instances: 30.49645390070922
Correctly Classified Instances: 69.50354609929079
Weighted Precision: 0.6759253435606002
Weighted AreaUnderROC: 0.8795267336346717
Root mean squared error: 0.314541205905945
Relative absolute error: 47.72261623325455
Root relative squared error: 72.64017996041075
Weighted TruePositiveRate: 0.6950354609929078
Weighted MatthewsCorrelation: 0.5839168949796881
Weighted FMeasure: 0.680099593044303
Iteration time: 34.0
Weighted AreaUnderPRC: 0.7206571796690847
Mean absolute error: 0.17895981087470456
Coverage of cases: 95.27186761229315
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 261.0
Weighted Recall: 0.6950354609929078
Weighted FalsePositiveRate: 0.10222099468616413
Kappa statistic: 0.593481338001937
Training time: 33.0
		
Iteration: 13
Labeled set size: 324
Unlabelled set size: 99
	
Mean region size: 53.723404255319146
Incorrectly Classified Instances: 28.368794326241133
Correctly Classified Instances: 71.63120567375887
Weighted Precision: 0.706629296510057
Weighted AreaUnderROC: 0.886723236664098
Root mean squared error: 0.30881455642512584
Relative absolute error: 47.78565799842399
Root relative squared error: 71.31766691268852
Weighted TruePositiveRate: 0.7163120567375887
Weighted MatthewsCorrelation: 0.6160189245646099
Weighted FMeasure: 0.6960846126100056
Iteration time: 35.0
Weighted AreaUnderPRC: 0.7350244102579356
Mean absolute error: 0.17919621749408998
Coverage of cases: 96.6903073286052
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 296.0
Weighted Recall: 0.7163120567375887
Weighted FalsePositiveRate: 0.09484436610465802
Kappa statistic: 0.6219698380189909
Training time: 34.0
		
Iteration: 14
Labeled set size: 344
Unlabelled set size: 79
	
Mean region size: 55.319148936170215
Incorrectly Classified Instances: 31.678486997635932
Correctly Classified Instances: 68.32151300236407
Weighted Precision: 0.6584538935883151
Weighted AreaUnderROC: 0.8971020082982133
Root mean squared error: 0.3029796862948548
Relative absolute error: 47.65957446808513
Root relative squared error: 69.97016137652912
Weighted TruePositiveRate: 0.6832151300236406
Weighted MatthewsCorrelation: 0.565493401990986
Weighted FMeasure: 0.6535244473803132
Iteration time: 37.0
Weighted AreaUnderPRC: 0.741556693695634
Mean absolute error: 0.17872340425531924
Coverage of cases: 98.81796690307328
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 333.0
Weighted Recall: 0.6832151300236406
Weighted FalsePositiveRate: 0.10605694334290253
Kappa statistic: 0.5778914680190343
Training time: 36.0
		
Iteration: 15
Labeled set size: 364
Unlabelled set size: 59
	
Mean region size: 53.427895981087474
Incorrectly Classified Instances: 29.55082742316785
Correctly Classified Instances: 70.44917257683215
Weighted Precision: 0.6944903396925377
Weighted AreaUnderROC: 0.8898559541331789
Root mean squared error: 0.3102656529713071
Relative absolute error: 46.99763593380617
Root relative squared error: 71.65278330531167
Weighted TruePositiveRate: 0.7044917257683215
Weighted MatthewsCorrelation: 0.600448645557784
Weighted FMeasure: 0.685730179333268
Iteration time: 39.0
Weighted AreaUnderPRC: 0.7377739108828211
Mean absolute error: 0.17624113475177314
Coverage of cases: 97.63593380614657
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 372.0
Weighted Recall: 0.7044917257683215
Weighted FalsePositiveRate: 0.09889201224946426
Kappa statistic: 0.6062215138967499
Training time: 38.0
		
Iteration: 16
Labeled set size: 384
Unlabelled set size: 39
	
Mean region size: 54.787234042553195
Incorrectly Classified Instances: 27.89598108747045
Correctly Classified Instances: 72.10401891252955
Weighted Precision: 0.7212278581907284
Weighted AreaUnderROC: 0.8889949404826483
Root mean squared error: 0.3094645723067543
Relative absolute error: 47.565011820331
Root relative squared error: 71.4677816503828
Weighted TruePositiveRate: 0.7210401891252955
Weighted MatthewsCorrelation: 0.6272188117953396
Weighted FMeasure: 0.712676235136151
Iteration time: 41.0
Weighted AreaUnderPRC: 0.7414297269859405
Mean absolute error: 0.17836879432624125
Coverage of cases: 96.45390070921985
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 413.0
Weighted Recall: 0.7210401891252955
Weighted FalsePositiveRate: 0.09353574160591459
Kappa statistic: 0.6281706508540738
Training time: 40.0
		
Iteration: 17
Labeled set size: 404
Unlabelled set size: 19
	
Mean region size: 53.84160756501182
Incorrectly Classified Instances: 25.768321513002363
Correctly Classified Instances: 74.23167848699764
Weighted Precision: 0.7381010076585154
Weighted AreaUnderROC: 0.9103338695210287
Root mean squared error: 0.2903898089374897
Relative absolute error: 45.32702915681639
Root relative squared error: 67.06265374399348
Weighted TruePositiveRate: 0.7423167848699763
Weighted MatthewsCorrelation: 0.6537766368776227
Weighted FMeasure: 0.7389103090025885
Iteration time: 45.0
Weighted AreaUnderPRC: 0.763624683530117
Mean absolute error: 0.16997635933806143
Coverage of cases: 98.81796690307328
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 458.0
Weighted Recall: 0.7423167848699763
Weighted FalsePositiveRate: 0.08657129229386193
Kappa statistic: 0.6563718073887477
Training time: 45.0
		
Time end:Wed Nov 01 14.41.04 EET 2017