Wed Nov 01 14.40.57 EET 2017
Dataset: vehicle
Test set size: 423
Initial Labelled set size: 84
Initial Unlabelled set size: 339
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Wed Nov 01 14.40.57 EET 2017
		
Iteration: 1
Labeled set size: 84
Unlabelled set size: 339
	
Mean region size: 34.57446808510638
Incorrectly Classified Instances: 40.189125295508276
Correctly Classified Instances: 59.810874704491724
Weighted Precision: 0.6325377787048626
Weighted AreaUnderROC: 0.7712529111476395
Root mean squared error: 0.41789525330764765
Relative absolute error: 55.21933280798561
Root relative squared error: 96.5087747960949
Weighted TruePositiveRate: 0.5981087470449172
Weighted MatthewsCorrelation: 0.47733005366240006
Weighted FMeasure: 0.6094627179568338
Iteration time: 15.0
Weighted AreaUnderPRC: 0.5590400501099008
Mean absolute error: 0.20707249802994604
Coverage of cases: 75.65011820330969
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 15.0
Weighted Recall: 0.5981087470449172
Weighted FalsePositiveRate: 0.1363857132196317
Kappa statistic: 0.46315388692711407
Training time: 14.0
		
Iteration: 2
Labeled set size: 104
Unlabelled set size: 319
	
Mean region size: 39.8936170212766
Incorrectly Classified Instances: 40.42553191489362
Correctly Classified Instances: 59.57446808510638
Weighted Precision: 0.5993217132297015
Weighted AreaUnderROC: 0.7444918099723841
Root mean squared error: 0.4247492268386458
Relative absolute error: 56.26477541371154
Root relative squared error: 98.09163218135103
Weighted TruePositiveRate: 0.5957446808510638
Weighted MatthewsCorrelation: 0.461569323585969
Weighted FMeasure: 0.597105929410998
Iteration time: 6.0
Weighted AreaUnderPRC: 0.5390687991485319
Mean absolute error: 0.2109929078014183
Coverage of cases: 71.8676122931442
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 21.0
Weighted Recall: 0.5957446808510638
Weighted FalsePositiveRate: 0.13568618084986758
Kappa statistic: 0.46084525939177096
Training time: 5.0
		
Iteration: 3
Labeled set size: 124
Unlabelled set size: 299
	
Mean region size: 33.274231678487
Incorrectly Classified Instances: 35.9338061465721
Correctly Classified Instances: 64.06619385342789
Weighted Precision: 0.6271502614469134
Weighted AreaUnderROC: 0.7576842347788323
Root mean squared error: 0.4160829787805044
Relative absolute error: 51.11223685691757
Root relative squared error: 96.09024792165822
Weighted TruePositiveRate: 0.640661938534279
Weighted MatthewsCorrelation: 0.5148053545320496
Weighted FMeasure: 0.626356756840696
Iteration time: 6.0
Weighted AreaUnderPRC: 0.5357355724044855
Mean absolute error: 0.1916708882134409
Coverage of cases: 69.50354609929079
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 27.0
Weighted Recall: 0.640661938534279
Weighted FalsePositiveRate: 0.11897043774233722
Kappa statistic: 0.5217957204376251
Training time: 6.0
		
Iteration: 4
Labeled set size: 144
Unlabelled set size: 279
	
Mean region size: 33.274231678487
Incorrectly Classified Instances: 35.9338061465721
Correctly Classified Instances: 64.06619385342789
Weighted Precision: 0.6271502614469134
Weighted AreaUnderROC: 0.7576842347788323
Root mean squared error: 0.4160829787805044
Relative absolute error: 51.11223685691757
Root relative squared error: 96.09024792165822
Weighted TruePositiveRate: 0.640661938534279
Weighted MatthewsCorrelation: 0.5148053545320496
Weighted FMeasure: 0.626356756840696
Iteration time: 7.0
Weighted AreaUnderPRC: 0.5357355724044855
Mean absolute error: 0.1916708882134409
Coverage of cases: 69.50354609929079
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 34.0
Weighted Recall: 0.640661938534279
Weighted FalsePositiveRate: 0.11897043774233722
Kappa statistic: 0.5217957204376251
Training time: 6.0
		
Iteration: 5
Labeled set size: 164
Unlabelled set size: 259
	
Mean region size: 33.03782505910166
Incorrectly Classified Instances: 35.46099290780142
Correctly Classified Instances: 64.53900709219859
Weighted Precision: 0.6317122446536371
Weighted AreaUnderROC: 0.7902137961421187
Root mean squared error: 0.4030582318437522
Relative absolute error: 49.446633394623824
Root relative squared error: 93.08231146163398
Weighted TruePositiveRate: 0.6453900709219859
Weighted MatthewsCorrelation: 0.5215171942850213
Weighted FMeasure: 0.6274123546655476
Iteration time: 7.0
Weighted AreaUnderPRC: 0.6026175970946346
Mean absolute error: 0.18542487522983933
Coverage of cases: 72.10401891252955
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 41.0
Weighted Recall: 0.6453900709219859
Weighted FalsePositiveRate: 0.11681731763979941
Kappa statistic: 0.5283720100495043
Training time: 7.0
		
Iteration: 6
Labeled set size: 184
Unlabelled set size: 239
	
Mean region size: 31.087470449172578
Incorrectly Classified Instances: 34.988179669030735
Correctly Classified Instances: 65.01182033096927
Weighted Precision: 0.6455226044543841
Weighted AreaUnderROC: 0.7878446972732226
Root mean squared error: 0.403040592498412
Relative absolute error: 47.14473338586816
Root relative squared error: 93.07823782932178
Weighted TruePositiveRate: 0.6501182033096927
Weighted MatthewsCorrelation: 0.5309372159057762
Weighted FMeasure: 0.6351229077347568
Iteration time: 9.0
Weighted AreaUnderPRC: 0.5896632760015522
Mean absolute error: 0.17679275019700558
Coverage of cases: 72.10401891252955
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 50.0
Weighted Recall: 0.6501182033096927
Weighted FalsePositiveRate: 0.11569875734016707
Kappa statistic: 0.5345289079229123
Training time: 9.0
		
Iteration: 7
Labeled set size: 204
Unlabelled set size: 219
	
Mean region size: 29.37352245862884
Incorrectly Classified Instances: 35.46099290780142
Correctly Classified Instances: 64.53900709219859
Weighted Precision: 0.6689767796888166
Weighted AreaUnderROC: 0.7730457416522736
Root mean squared error: 0.4086686153714193
Relative absolute error: 47.63557984824521
Root relative squared error: 94.37797403761623
Weighted TruePositiveRate: 0.6453900709219859
Weighted MatthewsCorrelation: 0.536271067484457
Weighted FMeasure: 0.6438615076973788
Iteration time: 11.0
Weighted AreaUnderPRC: 0.5793946664869868
Mean absolute error: 0.1786334244309195
Coverage of cases: 72.10401891252955
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 61.0
Weighted Recall: 0.6453900709219859
Weighted FalsePositiveRate: 0.11841428687063028
Kappa statistic: 0.5278842218832547
Training time: 10.0
		
Iteration: 8
Labeled set size: 224
Unlabelled set size: 199
	
Mean region size: 36.111111111111114
Incorrectly Classified Instances: 35.69739952718676
Correctly Classified Instances: 64.30260047281324
Weighted Precision: 0.6682678516087298
Weighted AreaUnderROC: 0.7978350216925186
Root mean squared error: 0.398611121001338
Relative absolute error: 49.72703111463519
Root relative squared error: 92.0552952048404
Weighted TruePositiveRate: 0.6430260047281324
Weighted MatthewsCorrelation: 0.5337521238338634
Weighted FMeasure: 0.6457436231958605
Iteration time: 13.0
Weighted AreaUnderPRC: 0.5874753623556209
Mean absolute error: 0.18647636667988196
Coverage of cases: 78.25059101654847
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 74.0
Weighted Recall: 0.6430260047281324
Weighted FalsePositiveRate: 0.1194455260351516
Kappa statistic: 0.5245351278119371
Training time: 13.0
		
Iteration: 9
Labeled set size: 244
Unlabelled set size: 179
	
Mean region size: 33.56973995271868
Incorrectly Classified Instances: 37.11583924349882
Correctly Classified Instances: 62.88416075650118
Weighted Precision: 0.6748891441777135
Weighted AreaUnderROC: 0.7541391658354485
Root mean squared error: 0.41450530364025595
Relative absolute error: 52.37328425734217
Root relative squared error: 95.72589945489175
Weighted TruePositiveRate: 0.6288416075650118
Weighted MatthewsCorrelation: 0.5238010970189161
Weighted FMeasure: 0.6348939419112445
Iteration time: 16.0
Weighted AreaUnderPRC: 0.5472723568607645
Mean absolute error: 0.19639981596503314
Coverage of cases: 70.2127659574468
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 90.0
Weighted Recall: 0.6288416075650118
Weighted FalsePositiveRate: 0.12426254557099614
Kappa statistic: 0.5056645626153755
Training time: 15.0
		
Iteration: 10
Labeled set size: 264
Unlabelled set size: 159
	
Mean region size: 32.97872340425532
Incorrectly Classified Instances: 35.46099290780142
Correctly Classified Instances: 64.53900709219859
Weighted Precision: 0.6745887579393761
Weighted AreaUnderROC: 0.7735603438764962
Root mean squared error: 0.40698857648005327
Relative absolute error: 50.166624977148025
Root relative squared error: 93.98998567514455
Weighted TruePositiveRate: 0.6453900709219859
Weighted MatthewsCorrelation: 0.5385306535541834
Weighted FMeasure: 0.6505300123037293
Iteration time: 14.0
Weighted AreaUnderPRC: 0.5678514384830491
Mean absolute error: 0.1881248436643051
Coverage of cases: 72.57683215130024
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 104.0
Weighted Recall: 0.6453900709219859
Weighted FalsePositiveRate: 0.11893977334337885
Kappa statistic: 0.5275537784528783
Training time: 13.0
		
Iteration: 11
Labeled set size: 284
Unlabelled set size: 139
	
Mean region size: 35.638297872340424
Incorrectly Classified Instances: 36.87943262411348
Correctly Classified Instances: 63.12056737588652
Weighted Precision: 0.6575255208642556
Weighted AreaUnderROC: 0.7866054335091809
Root mean squared error: 0.4014743398387944
Relative absolute error: 50.9011527201989
Root relative squared error: 92.7165272714621
Weighted TruePositiveRate: 0.6312056737588653
Weighted MatthewsCorrelation: 0.5180041229904211
Weighted FMeasure: 0.6354837019988808
Iteration time: 15.0
Weighted AreaUnderPRC: 0.5582520073456433
Mean absolute error: 0.1908793227007459
Coverage of cases: 77.06855791962175
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 119.0
Weighted Recall: 0.6312056737588653
Weighted FalsePositiveRate: 0.12382489521657751
Kappa statistic: 0.5086303185548127
Training time: 15.0
		
Iteration: 12
Labeled set size: 304
Unlabelled set size: 119
	
Mean region size: 37.174940898345156
Incorrectly Classified Instances: 33.80614657210402
Correctly Classified Instances: 66.19385342789599
Weighted Precision: 0.6791868472457581
Weighted AreaUnderROC: 0.8032163943045811
Root mean squared error: 0.38129860058922754
Relative absolute error: 47.502412502538284
Root relative squared error: 88.05713987672725
Weighted TruePositiveRate: 0.6619385342789598
Weighted MatthewsCorrelation: 0.555869262181836
Weighted FMeasure: 0.666714005721098
Iteration time: 18.0
Weighted AreaUnderPRC: 0.5997055131398946
Mean absolute error: 0.1781340468845186
Coverage of cases: 81.08747044917257
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 137.0
Weighted Recall: 0.6619385342789598
Weighted FalsePositiveRate: 0.11303440396875822
Kappa statistic: 0.5495207667731629
Training time: 17.0
		
Iteration: 13
Labeled set size: 324
Unlabelled set size: 99
	
Mean region size: 34.9290780141844
Incorrectly Classified Instances: 30.969267139479907
Correctly Classified Instances: 69.03073286052009
Weighted Precision: 0.6947910821164854
Weighted AreaUnderROC: 0.8263516615659561
Root mean squared error: 0.36786430132306297
Relative absolute error: 43.093366509567936
Root relative squared error: 84.95462135764961
Weighted TruePositiveRate: 0.6903073286052009
Weighted MatthewsCorrelation: 0.5883874961900563
Weighted FMeasure: 0.6912275911260627
Iteration time: 17.0
Weighted AreaUnderPRC: 0.62314130001038
Mean absolute error: 0.16160012441087976
Coverage of cases: 82.97872340425532
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 154.0
Weighted Recall: 0.6903073286052009
Weighted FalsePositiveRate: 0.10344965172057788
Kappa statistic: 0.5872340742506406
Training time: 17.0
		
Iteration: 14
Labeled set size: 344
Unlabelled set size: 79
	
Mean region size: 34.751773049645394
Incorrectly Classified Instances: 30.969267139479907
Correctly Classified Instances: 69.03073286052009
Weighted Precision: 0.6947910821164854
Weighted AreaUnderROC: 0.8280682522773338
Root mean squared error: 0.3668735982545014
Relative absolute error: 42.82252918510367
Root relative squared error: 84.7258282843212
Weighted TruePositiveRate: 0.6903073286052009
Weighted MatthewsCorrelation: 0.5883874961900563
Weighted FMeasure: 0.6912275911260627
Iteration time: 19.0
Weighted AreaUnderPRC: 0.6270136436155811
Mean absolute error: 0.16058448444413878
Coverage of cases: 82.97872340425532
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 173.0
Weighted Recall: 0.6903073286052009
Weighted FalsePositiveRate: 0.10344965172057788
Kappa statistic: 0.5872340742506406
Training time: 18.0
		
Iteration: 15
Labeled set size: 364
Unlabelled set size: 59
	
Mean region size: 32.6241134751773
Incorrectly Classified Instances: 32.15130023640662
Correctly Classified Instances: 67.84869976359339
Weighted Precision: 0.683758826246267
Weighted AreaUnderROC: 0.8267016418119981
Root mean squared error: 0.36962019568245547
Relative absolute error: 42.69107631632042
Root relative squared error: 85.36012779007513
Weighted TruePositiveRate: 0.6784869976359338
Weighted MatthewsCorrelation: 0.5728885482990287
Weighted FMeasure: 0.6797353672259429
Iteration time: 19.0
Weighted AreaUnderPRC: 0.6320459093242106
Mean absolute error: 0.1600915361862016
Coverage of cases: 81.32387706855792
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 192.0
Weighted Recall: 0.6784869976359338
Weighted FalsePositiveRate: 0.10758096456673165
Kappa statistic: 0.5713966413851679
Training time: 18.0
		
Iteration: 16
Labeled set size: 384
Unlabelled set size: 39
	
Mean region size: 30.437352245862883
Incorrectly Classified Instances: 31.20567375886525
Correctly Classified Instances: 68.79432624113475
Weighted Precision: 0.6864414151648195
Weighted AreaUnderROC: 0.8107618159302034
Root mean squared error: 0.37540261488568744
Relative absolute error: 43.05512242286046
Root relative squared error: 86.69552030349644
Weighted TruePositiveRate: 0.6879432624113475
Weighted MatthewsCorrelation: 0.5825897145993951
Weighted FMeasure: 0.6848637077919125
Iteration time: 21.0
Weighted AreaUnderPRC: 0.6116173653014124
Mean absolute error: 0.1614567090857267
Coverage of cases: 77.30496453900709
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 213.0
Weighted Recall: 0.6879432624113475
Weighted FalsePositiveRate: 0.10420891491042321
Kappa statistic: 0.5841203634738567
Training time: 20.0
		
Iteration: 17
Labeled set size: 404
Unlabelled set size: 19
	
Mean region size: 28.95981087470449
Incorrectly Classified Instances: 28.60520094562648
Correctly Classified Instances: 71.39479905437352
Weighted Precision: 0.7116196753076187
Weighted AreaUnderROC: 0.8291581596593435
Root mean squared error: 0.3589472442072775
Relative absolute error: 38.999435530294186
Root relative squared error: 82.89531522717841
Weighted TruePositiveRate: 0.7139479905437353
Weighted MatthewsCorrelation: 0.6169822540662423
Weighted FMeasure: 0.7120280314752377
Iteration time: 22.0
Weighted AreaUnderPRC: 0.6485620343161398
Mean absolute error: 0.1462478832386032
Coverage of cases: 78.01418439716312
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 235.0
Weighted Recall: 0.7139479905437353
Weighted FalsePositiveRate: 0.09555338566961978
Kappa statistic: 0.6187088411454453
Training time: 21.0
		
Time end:Wed Nov 01 14.40.57 EET 2017