Wed Nov 01 14.38.48 EET 2017
Dataset: vehicle
Test set size: 423
Initial Labelled set size: 84
Initial Unlabelled set size: 339
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Nov 01 14.38.48 EET 2017
		
Iteration: 1
Labeled set size: 84
Unlabelled set size: 339
	
Mean region size: 42.25768321513002
Incorrectly Classified Instances: 49.881796690307326
Correctly Classified Instances: 50.118203309692674
Weighted Precision: 0.5365123366844884
Weighted AreaUnderROC: 0.7687866875296883
Root mean squared error: 0.4560211665714755
Relative absolute error: 70.93198404843159
Root relative squared error: 105.31357731048342
Weighted TruePositiveRate: 0.5011820330969267
Weighted MatthewsCorrelation: 0.35558484992150263
Weighted FMeasure: 0.4787230130424239
Iteration time: 25.0
Weighted AreaUnderPRC: 0.4880028279161965
Mean absolute error: 0.26599494018161846
Coverage of cases: 72.34042553191489
Instances selection time: 24.0
Test time: 31.0
Accumulative iteration time: 25.0
Weighted Recall: 0.5011820330969267
Weighted FalsePositiveRate: 0.16037460152851277
Kappa statistic: 0.33975188820914176
Training time: 1.0
		
Iteration: 2
Labeled set size: 104
Unlabelled set size: 319
	
Mean region size: 43.262411347517734
Incorrectly Classified Instances: 49.40898345153664
Correctly Classified Instances: 50.59101654846336
Weighted Precision: 0.5766561540029047
Weighted AreaUnderROC: 0.7805322571132615
Root mean squared error: 0.4488511922195843
Relative absolute error: 69.48821108378003
Root relative squared error: 103.6577426616246
Weighted TruePositiveRate: 0.5059101654846335
Weighted MatthewsCorrelation: 0.3744758024110594
Weighted FMeasure: 0.48290419696864095
Iteration time: 21.0
Weighted AreaUnderPRC: 0.5308379624236075
Mean absolute error: 0.26058079156417513
Coverage of cases: 73.99527186761229
Instances selection time: 21.0
Test time: 28.0
Accumulative iteration time: 46.0
Weighted Recall: 0.5059101654846335
Weighted FalsePositiveRate: 0.15883811235224837
Kappa statistic: 0.3458359539753597
Training time: 0.0
		
Iteration: 3
Labeled set size: 124
Unlabelled set size: 299
	
Mean region size: 43.2033096926714
Incorrectly Classified Instances: 53.90070921985816
Correctly Classified Instances: 46.09929078014184
Weighted Precision: 0.4895697714653973
Weighted AreaUnderROC: 0.7660127271577563
Root mean squared error: 0.4572478285203441
Relative absolute error: 70.13073005372426
Root relative squared error: 105.59686275303702
Weighted TruePositiveRate: 0.46099290780141844
Weighted MatthewsCorrelation: 0.29841756953981813
Weighted FMeasure: 0.42930928412740293
Iteration time: 21.0
Weighted AreaUnderPRC: 0.5171708393284137
Mean absolute error: 0.26299023770146596
Coverage of cases: 75.177304964539
Instances selection time: 20.0
Test time: 30.0
Accumulative iteration time: 67.0
Weighted Recall: 0.46099290780141844
Weighted FalsePositiveRate: 0.173940305023498
Kappa statistic: 0.28643513517512836
Training time: 1.0
		
Iteration: 4
Labeled set size: 144
Unlabelled set size: 279
	
Mean region size: 42.31678486997636
Incorrectly Classified Instances: 51.536643026004725
Correctly Classified Instances: 48.463356973995275
Weighted Precision: 0.5761335941117692
Weighted AreaUnderROC: 0.7777255291216719
Root mean squared error: 0.45565134520906453
Relative absolute error: 69.97079301453492
Root relative squared error: 105.22817072522741
Weighted TruePositiveRate: 0.4846335697399527
Weighted MatthewsCorrelation: 0.35284607818185504
Weighted FMeasure: 0.45596710264791507
Iteration time: 18.0
Weighted AreaUnderPRC: 0.5440216831058233
Mean absolute error: 0.262390473804506
Coverage of cases: 74.94089834515367
Instances selection time: 18.0
Test time: 33.0
Accumulative iteration time: 85.0
Weighted Recall: 0.4846335697399527
Weighted FalsePositiveRate: 0.16533278282043393
Kappa statistic: 0.31830681885386475
Training time: 0.0
		
Iteration: 5
Labeled set size: 164
Unlabelled set size: 259
	
Mean region size: 41.548463356974
Incorrectly Classified Instances: 54.1371158392435
Correctly Classified Instances: 45.8628841607565
Weighted Precision: 0.5113637427567852
Weighted AreaUnderROC: 0.7433056951994849
Root mean squared error: 0.46801492590719584
Relative absolute error: 71.88713557356688
Root relative squared error: 108.08341738291291
Weighted TruePositiveRate: 0.458628841607565
Weighted MatthewsCorrelation: 0.3022439863819959
Weighted FMeasure: 0.4162249300302682
Iteration time: 15.0
Weighted AreaUnderPRC: 0.49336224902145076
Mean absolute error: 0.2695767584008758
Coverage of cases: 72.10401891252955
Instances selection time: 14.0
Test time: 31.0
Accumulative iteration time: 100.0
Weighted Recall: 0.458628841607565
Weighted FalsePositiveRate: 0.17462464344662937
Kappa statistic: 0.2834963090077962
Training time: 1.0
		
Iteration: 6
Labeled set size: 184
Unlabelled set size: 239
	
Mean region size: 40.36643026004728
Incorrectly Classified Instances: 47.5177304964539
Correctly Classified Instances: 52.4822695035461
Weighted Precision: 0.634269599259289
Weighted AreaUnderROC: 0.7681130496829333
Root mean squared error: 0.453680722393493
Relative absolute error: 67.52296700278372
Root relative squared error: 104.77307488001084
Weighted TruePositiveRate: 0.524822695035461
Weighted MatthewsCorrelation: 0.4154362140478577
Weighted FMeasure: 0.49965647102259825
Iteration time: 13.0
Weighted AreaUnderPRC: 0.5557220342431859
Mean absolute error: 0.25321112626043896
Coverage of cases: 71.15839243498817
Instances selection time: 12.0
Test time: 38.0
Accumulative iteration time: 113.0
Weighted Recall: 0.524822695035461
Weighted FalsePositiveRate: 0.15085366180564294
Kappa statistic: 0.37209302325581395
Training time: 1.0
		
Iteration: 7
Labeled set size: 204
Unlabelled set size: 219
	
Mean region size: 40.0709219858156
Incorrectly Classified Instances: 49.40898345153664
Correctly Classified Instances: 50.59101654846336
Weighted Precision: 0.608309172379535
Weighted AreaUnderROC: 0.7740482652303515
Root mean squared error: 0.45629790039428025
Relative absolute error: 68.14592306093671
Root relative squared error: 105.3774862493195
Weighted TruePositiveRate: 0.5059101654846335
Weighted MatthewsCorrelation: 0.3862295294626105
Weighted FMeasure: 0.4714753336173793
Iteration time: 17.0
Weighted AreaUnderPRC: 0.5595358045937416
Mean absolute error: 0.25554721147851267
Coverage of cases: 71.8676122931442
Instances selection time: 15.0
Test time: 31.0
Accumulative iteration time: 130.0
Weighted Recall: 0.5059101654846335
Weighted FalsePositiveRate: 0.1570794316920427
Kappa statistic: 0.3472945137212341
Training time: 2.0
		
Iteration: 8
Labeled set size: 224
Unlabelled set size: 199
	
Mean region size: 39.657210401891255
Incorrectly Classified Instances: 51.30023640661938
Correctly Classified Instances: 48.69976359338062
Weighted Precision: 0.5798654743976597
Weighted AreaUnderROC: 0.7738598435738291
Root mean squared error: 0.45670483873426176
Relative absolute error: 69.18561212690598
Root relative squared error: 105.47146463337228
Weighted TruePositiveRate: 0.48699763593380613
Weighted MatthewsCorrelation: 0.35934592362591306
Weighted FMeasure: 0.4600344614200471
Iteration time: 13.0
Weighted AreaUnderPRC: 0.5487013551136057
Mean absolute error: 0.25944604547589745
Coverage of cases: 71.39479905437352
Instances selection time: 11.0
Test time: 34.0
Accumulative iteration time: 143.0
Weighted Recall: 0.48699763593380613
Weighted FalsePositiveRate: 0.16383195617232044
Kappa statistic: 0.32192509418630416
Training time: 2.0
		
Iteration: 9
Labeled set size: 244
Unlabelled set size: 179
	
Mean region size: 44.917257683215134
Incorrectly Classified Instances: 48.463356973995275
Correctly Classified Instances: 51.536643026004725
Weighted Precision: 0.5882530442601368
Weighted AreaUnderROC: 0.7930256094454279
Root mean squared error: 0.4358711371524693
Relative absolute error: 68.22315099169776
Root relative squared error: 100.66012734678658
Weighted TruePositiveRate: 0.5153664302600472
Weighted MatthewsCorrelation: 0.38986814757578386
Weighted FMeasure: 0.49640554665559405
Iteration time: 16.0
Weighted AreaUnderPRC: 0.5739852977862728
Mean absolute error: 0.2558368162188666
Coverage of cases: 78.4869976359338
Instances selection time: 15.0
Test time: 31.0
Accumulative iteration time: 159.0
Weighted Recall: 0.5153664302600472
Weighted FalsePositiveRate: 0.15536681227185545
Kappa statistic: 0.35858839889344196
Training time: 1.0
		
Iteration: 10
Labeled set size: 264
Unlabelled set size: 159
	
Mean region size: 46.98581560283688
Incorrectly Classified Instances: 49.645390070921984
Correctly Classified Instances: 50.354609929078016
Weighted Precision: 0.5362705499883551
Weighted AreaUnderROC: 0.7896183602050539
Root mean squared error: 0.4282111524515328
Relative absolute error: 68.28269348454215
Root relative squared error: 98.89112965515694
Weighted TruePositiveRate: 0.5035460992907801
Weighted MatthewsCorrelation: 0.3565033219061115
Weighted FMeasure: 0.4745334253567967
Iteration time: 13.0
Weighted AreaUnderPRC: 0.5337068376397146
Mean absolute error: 0.25606010056703304
Coverage of cases: 81.08747044917257
Instances selection time: 11.0
Test time: 34.0
Accumulative iteration time: 172.0
Weighted Recall: 0.5035460992907801
Weighted FalsePositiveRate: 0.1605132628952749
Kappa statistic: 0.34234587735339184
Training time: 2.0
		
Iteration: 11
Labeled set size: 284
Unlabelled set size: 139
	
Mean region size: 48.226950354609926
Incorrectly Classified Instances: 49.645390070921984
Correctly Classified Instances: 50.354609929078016
Weighted Precision: 0.5593196239851396
Weighted AreaUnderROC: 0.7978777283918058
Root mean squared error: 0.4255130395473319
Relative absolute error: 68.21574650667354
Root relative squared error: 98.26802717053917
Weighted TruePositiveRate: 0.5035460992907801
Weighted MatthewsCorrelation: 0.364581240255685
Weighted FMeasure: 0.48164678798902844
Iteration time: 14.0
Weighted AreaUnderPRC: 0.5783942761864518
Mean absolute error: 0.2558090494000258
Coverage of cases: 82.50591016548464
Instances selection time: 12.0
Test time: 34.0
Accumulative iteration time: 186.0
Weighted Recall: 0.5035460992907801
Weighted FalsePositiveRate: 0.16055127803137653
Kappa statistic: 0.3421120846078075
Training time: 2.0
		
Iteration: 12
Labeled set size: 304
Unlabelled set size: 119
	
Mean region size: 47.28132387706856
Incorrectly Classified Instances: 48.93617021276596
Correctly Classified Instances: 51.06382978723404
Weighted Precision: 0.5799567001273936
Weighted AreaUnderROC: 0.8067771358352647
Root mean squared error: 0.4242937377200643
Relative absolute error: 67.89190857763856
Root relative squared error: 97.98644147526063
Weighted TruePositiveRate: 0.5106382978723404
Weighted MatthewsCorrelation: 0.38001794707511405
Weighted FMeasure: 0.4888443161114097
Iteration time: 6.0
Weighted AreaUnderPRC: 0.5917789935356769
Mean absolute error: 0.2545946571661446
Coverage of cases: 82.74231678486997
Instances selection time: 4.0
Test time: 34.0
Accumulative iteration time: 192.0
Weighted Recall: 0.5106382978723404
Weighted FalsePositiveRate: 0.15775644048131734
Kappa statistic: 0.35187529145293445
Training time: 2.0
		
Iteration: 13
Labeled set size: 324
Unlabelled set size: 99
	
Mean region size: 48.64066193853428
Incorrectly Classified Instances: 50.354609929078016
Correctly Classified Instances: 49.645390070921984
Weighted Precision: 0.5448681338615049
Weighted AreaUnderROC: 0.8017312217802168
Root mean squared error: 0.4181337956735293
Relative absolute error: 67.81422372498201
Root relative squared error: 96.56386379575686
Weighted TruePositiveRate: 0.49645390070921985
Weighted MatthewsCorrelation: 0.3509931743867012
Weighted FMeasure: 0.46688284394945107
Iteration time: 6.0
Weighted AreaUnderPRC: 0.568211208721423
Mean absolute error: 0.25430333896868257
Coverage of cases: 83.68794326241135
Instances selection time: 5.0
Test time: 36.0
Accumulative iteration time: 198.0
Weighted Recall: 0.49645390070921985
Weighted FalsePositiveRate: 0.16340639406804383
Kappa statistic: 0.332604943667084
Training time: 1.0
		
Iteration: 14
Labeled set size: 344
Unlabelled set size: 79
	
Mean region size: 47.399527186761226
Incorrectly Classified Instances: 52.24586288416076
Correctly Classified Instances: 47.75413711583924
Weighted Precision: 0.5445201147295194
Weighted AreaUnderROC: 0.798700340551898
Root mean squared error: 0.4339134891888669
Relative absolute error: 70.09476637419947
Root relative squared error: 100.20802791528084
Weighted TruePositiveRate: 0.47754137115839246
Weighted MatthewsCorrelation: 0.3320620606394777
Weighted FMeasure: 0.4406559879624935
Iteration time: 12.0
Weighted AreaUnderPRC: 0.5659696431371417
Mean absolute error: 0.262855373903248
Coverage of cases: 83.21513002364067
Instances selection time: 7.0
Test time: 27.0
Accumulative iteration time: 210.0
Weighted Recall: 0.47754137115839246
Weighted FalsePositiveRate: 0.16910919226970444
Kappa statistic: 0.3078768314984415
Training time: 5.0
		
Iteration: 15
Labeled set size: 364
Unlabelled set size: 59
	
Mean region size: 46.276595744680854
Incorrectly Classified Instances: 52.4822695035461
Correctly Classified Instances: 47.5177304964539
Weighted Precision: 0.5381415884843135
Weighted AreaUnderROC: 0.794239234224555
Root mean squared error: 0.4448309773584657
Relative absolute error: 71.043258990433
Root relative squared error: 102.7293138087178
Weighted TruePositiveRate: 0.475177304964539
Weighted MatthewsCorrelation: 0.32692993639095347
Weighted FMeasure: 0.42950313024155895
Iteration time: 4.0
Weighted AreaUnderPRC: 0.563753889819443
Mean absolute error: 0.2664122212141238
Coverage of cases: 80.61465721040189
Instances selection time: 2.0
Test time: 30.0
Accumulative iteration time: 214.0
Weighted Recall: 0.475177304964539
Weighted FalsePositiveRate: 0.1697555155567342
Kappa statistic: 0.30481196328101867
Training time: 2.0
		
Iteration: 16
Labeled set size: 384
Unlabelled set size: 39
	
Mean region size: 45.33096926713948
Incorrectly Classified Instances: 52.24586288416076
Correctly Classified Instances: 47.75413711583924
Weighted Precision: 0.5481838169400131
Weighted AreaUnderROC: 0.7932230533175227
Root mean squared error: 0.448744870689665
Relative absolute error: 71.41089962323575
Root relative squared error: 103.63318875605677
Weighted TruePositiveRate: 0.47754137115839246
Weighted MatthewsCorrelation: 0.33329249660646276
Weighted FMeasure: 0.4330621907171474
Iteration time: 4.0
Weighted AreaUnderPRC: 0.5599163195800559
Mean absolute error: 0.26779087358713405
Coverage of cases: 78.72340425531915
Instances selection time: 2.0
Test time: 24.0
Accumulative iteration time: 218.0
Weighted Recall: 0.47754137115839246
Weighted FalsePositiveRate: 0.16873828112937192
Kappa statistic: 0.3080305262144977
Training time: 2.0
		
Iteration: 17
Labeled set size: 404
Unlabelled set size: 19
	
Mean region size: 46.335697399527184
Incorrectly Classified Instances: 52.24586288416076
Correctly Classified Instances: 47.75413711583924
Weighted Precision: 0.5531210665503987
Weighted AreaUnderROC: 0.791348340279762
Root mean squared error: 0.44519622185200575
Relative absolute error: 71.16999041088755
Root relative squared error: 102.81366341138396
Weighted TruePositiveRate: 0.47754137115839246
Weighted MatthewsCorrelation: 0.33505298093004676
Weighted FMeasure: 0.4388691905378096
Iteration time: 4.0
Weighted AreaUnderPRC: 0.5654291723451912
Mean absolute error: 0.2668874640408283
Coverage of cases: 78.4869976359338
Instances selection time: 2.0
Test time: 18.0
Accumulative iteration time: 222.0
Weighted Recall: 0.47754137115839246
Weighted FalsePositiveRate: 0.16887458988818332
Kappa statistic: 0.30790245202558636
Training time: 2.0
		
Time end:Wed Nov 01 14.38.50 EET 2017