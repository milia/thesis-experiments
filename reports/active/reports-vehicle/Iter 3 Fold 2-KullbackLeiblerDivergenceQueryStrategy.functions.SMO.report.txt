Wed Nov 01 14.40.25 EET 2017
Dataset: vehicle
Test set size: 423
Initial Labelled set size: 84
Initial Unlabelled set size: 339
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Wed Nov 01 14.40.25 EET 2017
		
Iteration: 1
Labeled set size: 84
Unlabelled set size: 339
	
Mean region size: 75.65011820330969
Incorrectly Classified Instances: 34.04255319148936
Correctly Classified Instances: 65.95744680851064
Weighted Precision: 0.641423247386051
Weighted AreaUnderROC: 0.8176498962257258
Root mean squared error: 0.37197250012038535
Relative absolute error: 77.8040451799316
Root relative squared error: 85.90336923025704
Weighted TruePositiveRate: 0.6595744680851063
Weighted MatthewsCorrelation: 0.5369230624456783
Weighted FMeasure: 0.6360708520215251
Iteration time: 89.0
Weighted AreaUnderPRC: 0.5713984536793135
Mean absolute error: 0.2917651694247435
Coverage of cases: 96.45390070921985
Instances selection time: 2.0
Test time: 9.0
Accumulative iteration time: 89.0
Weighted Recall: 0.6595744680851063
Weighted FalsePositiveRate: 0.11272009966845355
Kappa statistic: 0.5468598890062638
Training time: 87.0
		
Iteration: 2
Labeled set size: 104
Unlabelled set size: 319
	
Mean region size: 75.76832151300236
Incorrectly Classified Instances: 42.08037825059102
Correctly Classified Instances: 57.91962174940898
Weighted Precision: 0.5666996909429439
Weighted AreaUnderROC: 0.792737173509175
Root mean squared error: 0.38605009127164175
Relative absolute error: 80.74599422117153
Root relative squared error: 89.15444964654479
Weighted TruePositiveRate: 0.5791962174940898
Weighted MatthewsCorrelation: 0.4336225509171906
Weighted FMeasure: 0.5537902210574652
Iteration time: 81.0
Weighted AreaUnderPRC: 0.5021599579087763
Mean absolute error: 0.30279747832939324
Coverage of cases: 95.27186761229315
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 170.0
Weighted Recall: 0.5791962174940898
Weighted FalsePositiveRate: 0.14121974148356317
Kappa statistic: 0.43804157181774067
Training time: 79.0
		
Iteration: 3
Labeled set size: 124
Unlabelled set size: 299
	
Mean region size: 75.59101654846336
Incorrectly Classified Instances: 48.463356973995275
Correctly Classified Instances: 51.536643026004725
Weighted Precision: 0.5013127293284627
Weighted AreaUnderROC: 0.7512637669076987
Root mean squared error: 0.4017625993492688
Relative absolute error: 84.05568689256637
Root relative squared error: 92.78309795384963
Weighted TruePositiveRate: 0.5153664302600472
Weighted MatthewsCorrelation: 0.3473947438054064
Weighted FMeasure: 0.48831491595634274
Iteration time: 56.0
Weighted AreaUnderPRC: 0.44959201162584955
Mean absolute error: 0.3152088258471239
Coverage of cases: 91.96217494089835
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 226.0
Weighted Recall: 0.5153664302600472
Weighted FalsePositiveRate: 0.16364983291364021
Kappa statistic: 0.352331799713193
Training time: 54.0
		
Iteration: 4
Labeled set size: 144
Unlabelled set size: 279
	
Mean region size: 75.65011820330969
Incorrectly Classified Instances: 48.93617021276596
Correctly Classified Instances: 51.06382978723404
Weighted Precision: 0.508356384673458
Weighted AreaUnderROC: 0.7463731299620503
Root mean squared error: 0.40306808645939474
Relative absolute error: 84.31836091410565
Root relative squared error: 93.08458728763158
Weighted TruePositiveRate: 0.5106382978723404
Weighted MatthewsCorrelation: 0.346202718355476
Weighted FMeasure: 0.4844020445962265
Iteration time: 56.0
Weighted AreaUnderPRC: 0.44522504366323234
Mean absolute error: 0.3161938534278962
Coverage of cases: 91.96217494089835
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 282.0
Weighted Recall: 0.5106382978723404
Weighted FalsePositiveRate: 0.16577471949827693
Kappa statistic: 0.3457932054720829
Training time: 55.0
		
Iteration: 5
Labeled set size: 164
Unlabelled set size: 259
	
Mean region size: 75.177304964539
Incorrectly Classified Instances: 41.843971631205676
Correctly Classified Instances: 58.156028368794324
Weighted Precision: 0.5696450240809936
Weighted AreaUnderROC: 0.7596034447615426
Root mean squared error: 0.3987272896813593
Relative absolute error: 83.21513002364068
Root relative squared error: 92.08212321231308
Weighted TruePositiveRate: 0.5815602836879432
Weighted MatthewsCorrelation: 0.4341361938582974
Weighted FMeasure: 0.5484461138815382
Iteration time: 58.0
Weighted AreaUnderPRC: 0.48455522797462364
Mean absolute error: 0.31205673758865254
Coverage of cases: 90.54373522458629
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 340.0
Weighted Recall: 0.5815602836879432
Weighted FalsePositiveRate: 0.14030080067891068
Kappa statistic: 0.4423141382315478
Training time: 56.0
		
Iteration: 6
Labeled set size: 184
Unlabelled set size: 239
	
Mean region size: 75.23640661938535
Incorrectly Classified Instances: 39.716312056737586
Correctly Classified Instances: 60.283687943262414
Weighted Precision: 0.601675979654703
Weighted AreaUnderROC: 0.7675364518928455
Root mean squared error: 0.39682877404844485
Relative absolute error: 82.84738639348565
Root relative squared error: 91.64367980762353
Weighted TruePositiveRate: 0.6028368794326241
Weighted MatthewsCorrelation: 0.4645456412394773
Weighted FMeasure: 0.5663125348383917
Iteration time: 63.0
Weighted AreaUnderPRC: 0.5012066166283
Mean absolute error: 0.3106776989755712
Coverage of cases: 90.54373522458629
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 403.0
Weighted Recall: 0.6028368794326241
Weighted FalsePositiveRate: 0.13292285193422348
Kappa statistic: 0.4711791757824708
Training time: 62.0
		
Iteration: 7
Labeled set size: 204
Unlabelled set size: 219
	
Mean region size: 75.177304964539
Incorrectly Classified Instances: 40.189125295508276
Correctly Classified Instances: 59.810874704491724
Weighted Precision: 0.6005196302895623
Weighted AreaUnderROC: 0.766173400516744
Root mean squared error: 0.39815043890337326
Relative absolute error: 83.11006041502499
Root relative squared error: 91.9489052315321
Weighted TruePositiveRate: 0.5981087470449172
Weighted MatthewsCorrelation: 0.45816034971312847
Weighted FMeasure: 0.5546305551480692
Iteration time: 60.0
Weighted AreaUnderPRC: 0.4962159193098997
Mean absolute error: 0.3116627265563437
Coverage of cases: 90.54373522458629
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 463.0
Weighted Recall: 0.5981087470449172
Weighted FalsePositiveRate: 0.13380793655973497
Kappa statistic: 0.4652299042902081
Training time: 59.0
		
Iteration: 8
Labeled set size: 224
Unlabelled set size: 199
	
Mean region size: 75.177304964539
Incorrectly Classified Instances: 39.952718676122934
Correctly Classified Instances: 60.047281323877066
Weighted Precision: 0.5978292918902274
Weighted AreaUnderROC: 0.764133168527568
Root mean squared error: 0.398397762939381
Relative absolute error: 83.16259521933281
Root relative squared error: 92.00602227103855
Weighted TruePositiveRate: 0.6004728132387707
Weighted MatthewsCorrelation: 0.45963985116180894
Weighted FMeasure: 0.5570363459113692
Iteration time: 67.0
Weighted AreaUnderPRC: 0.49725908042952927
Mean absolute error: 0.31185973207249806
Coverage of cases: 90.54373522458629
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 530.0
Weighted Recall: 0.6004728132387707
Weighted FalsePositiveRate: 0.13318387662321174
Kappa statistic: 0.46833607270617805
Training time: 66.0
		
Iteration: 9
Labeled set size: 244
Unlabelled set size: 179
	
Mean region size: 75.11820330969267
Incorrectly Classified Instances: 37.825059101654844
Correctly Classified Instances: 62.174940898345156
Weighted Precision: 0.46279483423136053
Weighted AreaUnderROC: 0.8031152862394476
Root mean squared error: 0.39137167604476225
Relative absolute error: 81.69162069871284
Root relative squared error: 90.3834170070554
Weighted TruePositiveRate: 0.6217494089834515
Weighted MatthewsCorrelation: 0.4548915275884757
Weighted FMeasure: 0.5300623501340822
Iteration time: 66.0
Weighted AreaUnderPRC: 0.531238217674491
Mean absolute error: 0.3063435776201731
Coverage of cases: 90.0709219858156
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 596.0
Weighted Recall: 0.6217494089834515
Weighted FalsePositiveRate: 0.12607854539614732
Kappa statistic: 0.49673562261120446
Training time: 64.0
		
Iteration: 10
Labeled set size: 264
Unlabelled set size: 159
	
Mean region size: 75.35460992907801
Incorrectly Classified Instances: 37.11583924349882
Correctly Classified Instances: 62.88416075650118
Weighted Precision: 0.47559546941286956
Weighted AreaUnderROC: 0.8111854771554579
Root mean squared error: 0.3854968582048905
Relative absolute error: 80.43078539532435
Root relative squared error: 89.0266859425394
Weighted TruePositiveRate: 0.6288416075650118
Weighted MatthewsCorrelation: 0.46677887652407046
Weighted FMeasure: 0.5403439240571183
Iteration time: 67.0
Weighted AreaUnderPRC: 0.5467679464789088
Mean absolute error: 0.3016154452324663
Coverage of cases: 92.90780141843972
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 663.0
Weighted Recall: 0.6288416075650118
Weighted FalsePositiveRate: 0.1237844161803207
Kappa statistic: 0.5061351061931109
Training time: 66.0
		
Iteration: 11
Labeled set size: 284
Unlabelled set size: 139
	
Mean region size: 75.35460992907801
Incorrectly Classified Instances: 37.35224586288416
Correctly Classified Instances: 62.64775413711584
Weighted Precision: 0.7323020342245207
Weighted AreaUnderROC: 0.8220666314024413
Root mean squared error: 0.3801362204516828
Relative absolute error: 79.38008930916725
Root relative squared error: 87.78869968260241
Weighted TruePositiveRate: 0.6264775413711584
Weighted MatthewsCorrelation: 0.48392267387375737
Weighted FMeasure: 0.5424609592769509
Iteration time: 65.0
Weighted AreaUnderPRC: 0.5602647030990324
Mean absolute error: 0.2976753349093772
Coverage of cases: 95.74468085106383
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 728.0
Weighted Recall: 0.6264775413711584
Weighted FalsePositiveRate: 0.12451464563235128
Kappa statistic: 0.5030227318357241
Training time: 64.0
		
Iteration: 12
Labeled set size: 304
Unlabelled set size: 119
	
Mean region size: 75.177304964539
Incorrectly Classified Instances: 38.297872340425535
Correctly Classified Instances: 61.702127659574465
Weighted Precision: 0.7155011230836416
Weighted AreaUnderROC: 0.8147987047941896
Root mean squared error: 0.38112823814461166
Relative absolute error: 79.53769372209082
Root relative squared error: 88.01779635542374
Weighted TruePositiveRate: 0.6170212765957447
Weighted MatthewsCorrelation: 0.48908813030773735
Weighted FMeasure: 0.540293319706008
Iteration time: 67.0
Weighted AreaUnderPRC: 0.5482928647239873
Mean absolute error: 0.2982663514578406
Coverage of cases: 95.50827423167848
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 795.0
Weighted Recall: 0.6170212765957447
Weighted FalsePositiveRate: 0.12858085468602795
Kappa statistic: 0.4899365826063655
Training time: 66.0
		
Iteration: 13
Labeled set size: 324
Unlabelled set size: 99
	
Mean region size: 75.177304964539
Incorrectly Classified Instances: 36.643026004728135
Correctly Classified Instances: 63.356973995271865
Weighted Precision: 0.47676745282384686
Weighted AreaUnderROC: 0.8234560460032527
Root mean squared error: 0.38013622045168277
Relative absolute error: 79.3275545048594
Root relative squared error: 87.78869968260238
Weighted TruePositiveRate: 0.6335697399527187
Weighted MatthewsCorrelation: 0.47163928542787076
Weighted FMeasure: 0.5427027053431339
Iteration time: 71.0
Weighted AreaUnderPRC: 0.5652940277184398
Mean absolute error: 0.2974783293932228
Coverage of cases: 95.50827423167848
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 866.0
Weighted Recall: 0.6335697399527187
Weighted FalsePositiveRate: 0.12252054452167913
Kappa statistic: 0.5122450194164646
Training time: 70.0
		
Iteration: 14
Labeled set size: 344
Unlabelled set size: 79
	
Mean region size: 75.177304964539
Incorrectly Classified Instances: 34.2789598108747
Correctly Classified Instances: 65.72104018912529
Weighted Precision: 0.5015900013011131
Weighted AreaUnderROC: 0.8373837723488013
Root mean squared error: 0.377492591563137
Relative absolute error: 78.80220646178088
Root relative squared error: 87.17817974242665
Weighted TruePositiveRate: 0.6572104018912529
Weighted MatthewsCorrelation: 0.504087165266755
Weighted FMeasure: 0.5665570000162047
Iteration time: 71.0
Weighted AreaUnderPRC: 0.5930761348410925
Mean absolute error: 0.2955082742316783
Coverage of cases: 95.50827423167848
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 937.0
Weighted Recall: 0.6572104018912529
Weighted FalsePositiveRate: 0.11414111313522479
Kappa statistic: 0.5438827413885418
Training time: 70.0
		
Iteration: 15
Labeled set size: 364
Unlabelled set size: 59
	
Mean region size: 75.53191489361703
Incorrectly Classified Instances: 33.096926713947994
Correctly Classified Instances: 66.903073286052
Weighted Precision: 0.5141519393184643
Weighted AreaUnderROC: 0.8438736173830683
Root mean squared error: 0.3748303179717677
Relative absolute error: 78.3293932230102
Root relative squared error: 86.56335399257325
Weighted TruePositiveRate: 0.6690307328605201
Weighted MatthewsCorrelation: 0.5208931192176787
Weighted FMeasure: 0.5778621646314805
Iteration time: 72.0
Weighted AreaUnderPRC: 0.6074261777556319
Mean absolute error: 0.29373522458628826
Coverage of cases: 95.74468085106383
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 1009.0
Weighted Recall: 0.6690307328605201
Weighted FalsePositiveRate: 0.10958442424806387
Kappa statistic: 0.5598204198132841
Training time: 71.0
		
Iteration: 16
Labeled set size: 384
Unlabelled set size: 39
	
Mean region size: 75.65011820330969
Incorrectly Classified Instances: 31.914893617021278
Correctly Classified Instances: 68.08510638297872
Weighted Precision: 0.7754228928381093
Weighted AreaUnderROC: 0.8550481227239142
Root mean squared error: 0.3673090583052739
Relative absolute error: 76.8584187023902
Root relative squared error: 84.82639347533514
Weighted TruePositiveRate: 0.6808510638297872
Weighted MatthewsCorrelation: 0.5726507203237048
Weighted FMeasure: 0.603281209833945
Iteration time: 72.0
Weighted AreaUnderPRC: 0.6205885908950671
Mean absolute error: 0.28821907013396325
Coverage of cases: 97.39952718676123
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 1081.0
Weighted Recall: 0.6808510638297872
Weighted FalsePositiveRate: 0.1057301781775802
Kappa statistic: 0.5754622298548074
Training time: 72.0
		
Iteration: 17
Labeled set size: 404
Unlabelled set size: 19
	
Mean region size: 75.76832151300236
Incorrectly Classified Instances: 29.314420803782507
Correctly Classified Instances: 70.68557919621749
Weighted Precision: 0.7119661053940883
Weighted AreaUnderROC: 0.8558132121022243
Root mean squared error: 0.36190583580541796
Relative absolute error: 75.80772261623311
Root relative squared error: 83.57857268942183
Weighted TruePositiveRate: 0.706855791962175
Weighted MatthewsCorrelation: 0.6056433460723833
Weighted FMeasure: 0.6698885675276733
Iteration time: 72.0
Weighted AreaUnderPRC: 0.6280556904791852
Mean absolute error: 0.28427895981087414
Coverage of cases: 97.87234042553192
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 1153.0
Weighted Recall: 0.706855791962175
Weighted FalsePositiveRate: 0.0973359832855953
Kappa statistic: 0.6098018211033744
Training time: 72.0
		
Time end:Wed Nov 01 14.40.27 EET 2017