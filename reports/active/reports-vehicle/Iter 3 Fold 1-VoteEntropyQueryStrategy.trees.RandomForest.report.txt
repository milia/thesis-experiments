Wed Nov 01 14.45.04 EET 2017
Dataset: vehicle
Test set size: 423
Initial Labelled set size: 84
Initial Unlabelled set size: 339
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Wed Nov 01 14.45.04 EET 2017
		
Iteration: 1
Labeled set size: 84
Unlabelled set size: 339
	
Mean region size: 65.83924349881796
Incorrectly Classified Instances: 34.04255319148936
Correctly Classified Instances: 65.95744680851064
Weighted Precision: 0.6566067908552466
Weighted AreaUnderROC: 0.8656907066955597
Root mean squared error: 0.33663260834679115
Relative absolute error: 60.83530338849491
Root relative squared error: 77.7419708188103
Weighted TruePositiveRate: 0.6595744680851063
Weighted MatthewsCorrelation: 0.5440173406433018
Weighted FMeasure: 0.6527047970037235
Iteration time: 10.0
Weighted AreaUnderPRC: 0.6962671285779507
Mean absolute error: 0.2281323877068559
Coverage of cases: 96.45390070921985
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 10.0
Weighted Recall: 0.6595744680851063
Weighted FalsePositiveRate: 0.11454268356308751
Kappa statistic: 0.5457481001991155
Training time: 9.0
		
Iteration: 2
Labeled set size: 104
Unlabelled set size: 319
	
Mean region size: 59.515366430260045
Incorrectly Classified Instances: 32.15130023640662
Correctly Classified Instances: 67.84869976359339
Weighted Precision: 0.664265391665936
Weighted AreaUnderROC: 0.8795031613611307
Root mean squared error: 0.32305061852232925
Relative absolute error: 54.56264775413715
Root relative squared error: 74.60534462629676
Weighted TruePositiveRate: 0.6784869976359338
Weighted MatthewsCorrelation: 0.5642671449398169
Weighted FMeasure: 0.6627842914850989
Iteration time: 11.0
Weighted AreaUnderPRC: 0.7167889112196464
Mean absolute error: 0.20460992907801434
Coverage of cases: 95.74468085106383
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 21.0
Weighted Recall: 0.6784869976359338
Weighted FalsePositiveRate: 0.10811172381485801
Kappa statistic: 0.5711442266834647
Training time: 10.0
		
Iteration: 3
Labeled set size: 124
Unlabelled set size: 299
	
Mean region size: 61.643026004728135
Incorrectly Classified Instances: 31.678486997635932
Correctly Classified Instances: 68.32151300236407
Weighted Precision: 0.6809304783300054
Weighted AreaUnderROC: 0.8745039465895876
Root mean squared error: 0.3289067786329934
Relative absolute error: 56.29629629629625
Root relative squared error: 75.95776687282056
Weighted TruePositiveRate: 0.6832151300236406
Weighted MatthewsCorrelation: 0.5757231159748909
Weighted FMeasure: 0.6782536200139649
Iteration time: 13.0
Weighted AreaUnderPRC: 0.7141260005966646
Mean absolute error: 0.21111111111111094
Coverage of cases: 95.74468085106383
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 34.0
Weighted Recall: 0.6832151300236406
Weighted FalsePositiveRate: 0.10722489004540463
Kappa statistic: 0.5770694363611945
Training time: 12.0
		
Iteration: 4
Labeled set size: 144
Unlabelled set size: 279
	
Mean region size: 63.47517730496454
Incorrectly Classified Instances: 34.04255319148936
Correctly Classified Instances: 65.95744680851064
Weighted Precision: 0.6475007744994797
Weighted AreaUnderROC: 0.8673956377965144
Root mean squared error: 0.3392210518022365
Relative absolute error: 58.345153664302664
Root relative squared error: 78.3397462291237
Weighted TruePositiveRate: 0.6595744680851063
Weighted MatthewsCorrelation: 0.5405173600858232
Weighted FMeasure: 0.6449613075527921
Iteration time: 14.0
Weighted AreaUnderPRC: 0.6913896080172435
Mean absolute error: 0.21879432624113498
Coverage of cases: 95.74468085106383
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 48.0
Weighted Recall: 0.6595744680851063
Weighted FalsePositiveRate: 0.11472855778185437
Kappa statistic: 0.5456362822616738
Training time: 13.0
		
Iteration: 5
Labeled set size: 164
Unlabelled set size: 259
	
Mean region size: 62.5886524822695
Incorrectly Classified Instances: 34.2789598108747
Correctly Classified Instances: 65.72104018912529
Weighted Precision: 0.6470404141505579
Weighted AreaUnderROC: 0.8787212188177048
Root mean squared error: 0.33173375065536675
Relative absolute error: 56.64302600472812
Root relative squared error: 76.61062809606408
Weighted TruePositiveRate: 0.6572104018912529
Weighted MatthewsCorrelation: 0.5374027930873012
Weighted FMeasure: 0.6358926346896216
Iteration time: 15.0
Weighted AreaUnderPRC: 0.7068471498124512
Mean absolute error: 0.21241134751773047
Coverage of cases: 95.98108747044917
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 63.0
Weighted Recall: 0.6572104018912529
Weighted FalsePositiveRate: 0.11560333918597344
Kappa statistic: 0.5426140388817217
Training time: 14.0
		
Iteration: 6
Labeled set size: 184
Unlabelled set size: 239
	
Mean region size: 56.6193853427896
Incorrectly Classified Instances: 32.6241134751773
Correctly Classified Instances: 67.37588652482269
Weighted Precision: 0.6546609036958261
Weighted AreaUnderROC: 0.884153501222884
Root mean squared error: 0.32416468394441883
Relative absolute error: 51.85185185185182
Root relative squared error: 74.86262701483207
Weighted TruePositiveRate: 0.6737588652482269
Weighted MatthewsCorrelation: 0.5558786106363431
Weighted FMeasure: 0.6567219635750737
Iteration time: 17.0
Weighted AreaUnderPRC: 0.7194658442588787
Mean absolute error: 0.19444444444444434
Coverage of cases: 95.27186761229315
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 80.0
Weighted Recall: 0.6737588652482269
Weighted FalsePositiveRate: 0.10990165208839979
Kappa statistic: 0.5647791239515377
Training time: 16.0
		
Iteration: 7
Labeled set size: 204
Unlabelled set size: 219
	
Mean region size: 51.477541371158395
Incorrectly Classified Instances: 29.78723404255319
Correctly Classified Instances: 70.2127659574468
Weighted Precision: 0.680728485400976
Weighted AreaUnderROC: 0.8796890309089529
Root mean squared error: 0.32730361986154105
Relative absolute error: 48.79432624113476
Root relative squared error: 75.58753321351988
Weighted TruePositiveRate: 0.7021276595744681
Weighted MatthewsCorrelation: 0.5926240325085163
Weighted FMeasure: 0.675180003525879
Iteration time: 18.0
Weighted AreaUnderPRC: 0.7059307986619757
Mean absolute error: 0.18297872340425536
Coverage of cases: 92.67139479905437
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 98.0
Weighted Recall: 0.7021276595744681
Weighted FalsePositiveRate: 0.09961222481065261
Kappa statistic: 0.6030712860079239
Training time: 17.0
		
Iteration: 8
Labeled set size: 224
Unlabelled set size: 199
	
Mean region size: 51.30023640661938
Incorrectly Classified Instances: 29.314420803782507
Correctly Classified Instances: 70.68557919621749
Weighted Precision: 0.6845764049523737
Weighted AreaUnderROC: 0.8953314269268828
Root mean squared error: 0.31454120590594487
Relative absolute error: 46.74546887312845
Root relative squared error: 72.6401799604107
Weighted TruePositiveRate: 0.706855791962175
Weighted MatthewsCorrelation: 0.5987852327103119
Weighted FMeasure: 0.6804412071171013
Iteration time: 19.0
Weighted AreaUnderPRC: 0.7313588226249715
Mean absolute error: 0.17529550827423168
Coverage of cases: 94.79905437352245
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 117.0
Weighted Recall: 0.706855791962175
Weighted FalsePositiveRate: 0.09738684507372455
Kappa statistic: 0.6095898058071768
Training time: 18.0
		
Iteration: 9
Labeled set size: 244
Unlabelled set size: 179
	
Mean region size: 50.05910165484634
Incorrectly Classified Instances: 31.20567375886525
Correctly Classified Instances: 68.79432624113475
Weighted Precision: 0.6707887879248293
Weighted AreaUnderROC: 0.8726399753238018
Root mean squared error: 0.33384712176267145
Relative absolute error: 48.163908589440496
Root relative squared error: 77.09869024714405
Weighted TruePositiveRate: 0.6879432624113475
Weighted MatthewsCorrelation: 0.5756170556823086
Weighted FMeasure: 0.6559643306286145
Iteration time: 21.0
Weighted AreaUnderPRC: 0.7104474830402838
Mean absolute error: 0.18061465721040187
Coverage of cases: 90.30732860520095
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 138.0
Weighted Recall: 0.6879432624113475
Weighted FalsePositiveRate: 0.10372079474044404
Kappa statistic: 0.5843989579456643
Training time: 20.0
		
Iteration: 10
Labeled set size: 264
Unlabelled set size: 159
	
Mean region size: 49.645390070921984
Incorrectly Classified Instances: 30.969267139479907
Correctly Classified Instances: 69.03073286052009
Weighted Precision: 0.6683493735616611
Weighted AreaUnderROC: 0.879547558180116
Root mean squared error: 0.32699650448894785
Relative absolute error: 46.99763593380615
Root relative squared error: 75.5166079563043
Weighted TruePositiveRate: 0.6903073286052009
Weighted MatthewsCorrelation: 0.5746484931318707
Weighted FMeasure: 0.654577798288138
Iteration time: 22.0
Weighted AreaUnderPRC: 0.7114086979242842
Mean absolute error: 0.17624113475177305
Coverage of cases: 92.43498817966903
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 160.0
Weighted Recall: 0.6903073286052009
Weighted FalsePositiveRate: 0.10324074929716581
Kappa statistic: 0.5875136780830585
Training time: 22.0
		
Iteration: 11
Labeled set size: 284
Unlabelled set size: 139
	
Mean region size: 49.40898345153664
Incorrectly Classified Instances: 29.314420803782507
Correctly Classified Instances: 70.68557919621749
Weighted Precision: 0.7210662223545798
Weighted AreaUnderROC: 0.884505053376774
Root mean squared error: 0.3219510557049061
Relative absolute error: 47.02915681639087
Root relative squared error: 74.3514114708447
Weighted TruePositiveRate: 0.706855791962175
Weighted MatthewsCorrelation: 0.6058618485896301
Weighted FMeasure: 0.6592434892166661
Iteration time: 25.0
Weighted AreaUnderPRC: 0.7244248272423791
Mean absolute error: 0.17635933806146578
Coverage of cases: 92.90780141843972
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 185.0
Weighted Recall: 0.706855791962175
Weighted FalsePositiveRate: 0.0968138593865578
Kappa statistic: 0.6099004893721459
Training time: 24.0
		
Iteration: 12
Labeled set size: 304
Unlabelled set size: 119
	
Mean region size: 49.94089834515366
Incorrectly Classified Instances: 29.314420803782507
Correctly Classified Instances: 70.68557919621749
Weighted Precision: 0.7145045482876308
Weighted AreaUnderROC: 0.8871018039124683
Root mean squared error: 0.3231786571610887
Relative absolute error: 47.029156816390916
Root relative squared error: 74.63491388331853
Weighted TruePositiveRate: 0.706855791962175
Weighted MatthewsCorrelation: 0.6051458693609988
Weighted FMeasure: 0.66326999059699
Iteration time: 29.0
Weighted AreaUnderPRC: 0.7210110868792621
Mean absolute error: 0.17635933806146592
Coverage of cases: 93.14420803782505
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 214.0
Weighted Recall: 0.706855791962175
Weighted FalsePositiveRate: 0.09673168091124949
Kappa statistic: 0.6099004893721459
Training time: 28.0
		
Iteration: 13
Labeled set size: 324
Unlabelled set size: 99
	
Mean region size: 46.335697399527184
Incorrectly Classified Instances: 29.55082742316785
Correctly Classified Instances: 70.44917257683215
Weighted Precision: 0.7650844338501768
Weighted AreaUnderROC: 0.8884318426476803
Root mean squared error: 0.32585584711176613
Relative absolute error: 43.877068557919664
Root relative squared error: 75.25318441879669
Weighted TruePositiveRate: 0.7044917257683215
Weighted MatthewsCorrelation: 0.6029796012136784
Weighted FMeasure: 0.6318074141683658
Iteration time: 30.0
Weighted AreaUnderPRC: 0.7460973744468327
Mean absolute error: 0.16453900709219874
Coverage of cases: 91.725768321513
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 244.0
Weighted Recall: 0.7044917257683215
Weighted FalsePositiveRate: 0.09711073236537418
Kappa statistic: 0.6069825176904323
Training time: 29.0
		
Iteration: 14
Labeled set size: 344
Unlabelled set size: 79
	
Mean region size: 51.35933806146572
Incorrectly Classified Instances: 28.84160756501182
Correctly Classified Instances: 71.15839243498817
Weighted Precision: 0.7456650172189723
Weighted AreaUnderROC: 0.8894963772749989
Root mean squared error: 0.31632120013499937
Relative absolute error: 46.49330181245077
Root relative squared error: 73.05125201933095
Weighted TruePositiveRate: 0.7115839243498818
Weighted MatthewsCorrelation: 0.6161237799881579
Weighted FMeasure: 0.658617507225959
Iteration time: 33.0
Weighted AreaUnderPRC: 0.7387424171869419
Mean absolute error: 0.17434988179669036
Coverage of cases: 94.56264775413712
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 277.0
Weighted Recall: 0.7115839243498818
Weighted FalsePositiveRate: 0.09580051573973129
Kappa statistic: 0.6160410699006733
Training time: 33.0
		
Iteration: 15
Labeled set size: 364
Unlabelled set size: 59
	
Mean region size: 51.477541371158395
Incorrectly Classified Instances: 25.53191489361702
Correctly Classified Instances: 74.46808510638297
Weighted Precision: 0.7487937461233695
Weighted AreaUnderROC: 0.9084763102819936
Root mean squared error: 0.29519396996794733
Relative absolute error: 43.97163120567374
Root relative squared error: 68.17212720965948
Weighted TruePositiveRate: 0.7446808510638298
Weighted MatthewsCorrelation: 0.6589851296440795
Weighted FMeasure: 0.7235156928383082
Iteration time: 38.0
Weighted AreaUnderPRC: 0.7835541024840698
Mean absolute error: 0.16489361702127653
Coverage of cases: 95.74468085106383
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 315.0
Weighted Recall: 0.7446808510638298
Weighted FalsePositiveRate: 0.08459709190130504
Kappa statistic: 0.6600235164540759
Training time: 37.0
		
Iteration: 16
Labeled set size: 384
Unlabelled set size: 39
	
Mean region size: 56.146572104018915
Incorrectly Classified Instances: 25.059101654846337
Correctly Classified Instances: 74.94089834515367
Weighted Precision: 0.7543421188126513
Weighted AreaUnderROC: 0.9053355992585255
Root mean squared error: 0.2937086573693592
Relative absolute error: 46.93459416863675
Root relative squared error: 67.82910895820923
Weighted TruePositiveRate: 0.7494089834515366
Weighted MatthewsCorrelation: 0.6669575132745995
Weighted FMeasure: 0.7383676863009924
Iteration time: 41.0
Weighted AreaUnderPRC: 0.7780991307898013
Mean absolute error: 0.1760047281323878
Coverage of cases: 97.63593380614657
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 356.0
Weighted Recall: 0.7494089834515366
Weighted FalsePositiveRate: 0.08335717650404863
Kappa statistic: 0.6661802587888443
Training time: 40.0
		
Iteration: 17
Labeled set size: 404
Unlabelled set size: 19
	
Mean region size: 55.26004728132388
Incorrectly Classified Instances: 25.768321513002363
Correctly Classified Instances: 74.23167848699764
Weighted Precision: 0.7405225022950874
Weighted AreaUnderROC: 0.9071068971706455
Root mean squared error: 0.2939098139283619
Relative absolute error: 46.14657210401894
Root relative squared error: 67.87556407560503
Weighted TruePositiveRate: 0.7423167848699763
Weighted MatthewsCorrelation: 0.6548152203449674
Weighted FMeasure: 0.7370007283420531
Iteration time: 44.0
Weighted AreaUnderPRC: 0.7717813954373716
Mean absolute error: 0.173049645390071
Coverage of cases: 98.81796690307328
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 400.0
Weighted Recall: 0.7423167848699763
Weighted FalsePositiveRate: 0.08622534441517345
Kappa statistic: 0.6565484260238665
Training time: 44.0
		
Time end:Wed Nov 01 14.45.05 EET 2017