Wed Nov 01 14.40.05 EET 2017
Dataset: vehicle
Test set size: 423
Initial Labelled set size: 84
Initial Unlabelled set size: 339
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Wed Nov 01 14.40.05 EET 2017
		
Iteration: 1
Labeled set size: 84
Unlabelled set size: 339
	
Mean region size: 42.25768321513002
Incorrectly Classified Instances: 49.881796690307326
Correctly Classified Instances: 50.118203309692674
Weighted Precision: 0.5365123366844884
Weighted AreaUnderROC: 0.7687866875296883
Root mean squared error: 0.4560211665714755
Relative absolute error: 70.93198404843159
Root relative squared error: 105.31357731048342
Weighted TruePositiveRate: 0.5011820330969267
Weighted MatthewsCorrelation: 0.35558484992150263
Weighted FMeasure: 0.4787230130424239
Iteration time: 54.0
Weighted AreaUnderPRC: 0.4880028279161965
Mean absolute error: 0.26599494018161846
Coverage of cases: 72.34042553191489
Instances selection time: 47.0
Test time: 18.0
Accumulative iteration time: 54.0
Weighted Recall: 0.5011820330969267
Weighted FalsePositiveRate: 0.16037460152851277
Kappa statistic: 0.33975188820914176
Training time: 7.0
		
Iteration: 2
Labeled set size: 104
Unlabelled set size: 319
	
Mean region size: 42.78959810874704
Incorrectly Classified Instances: 53.191489361702125
Correctly Classified Instances: 46.808510638297875
Weighted Precision: 0.47914635272791306
Weighted AreaUnderROC: 0.7708197971877835
Root mean squared error: 0.46585602390963754
Relative absolute error: 72.49450596202112
Root relative squared error: 107.5848403231352
Weighted TruePositiveRate: 0.46808510638297873
Weighted MatthewsCorrelation: 0.29763001267437017
Weighted FMeasure: 0.4060581707403831
Iteration time: 23.0
Weighted AreaUnderPRC: 0.4792290981735482
Mean absolute error: 0.2718543973575792
Coverage of cases: 73.5224586288416
Instances selection time: 22.0
Test time: 15.0
Accumulative iteration time: 77.0
Weighted Recall: 0.46808510638297873
Weighted FalsePositiveRate: 0.17084680369617058
Kappa statistic: 0.2968438317301299
Training time: 1.0
		
Iteration: 3
Labeled set size: 124
Unlabelled set size: 299
	
Mean region size: 43.14420803782506
Incorrectly Classified Instances: 54.1371158392435
Correctly Classified Instances: 45.8628841607565
Weighted Precision: 0.4437781193100342
Weighted AreaUnderROC: 0.7649543906301861
Root mean squared error: 0.47175648167212964
Relative absolute error: 73.82317334509924
Root relative squared error: 108.94749267414193
Weighted TruePositiveRate: 0.458628841607565
Weighted MatthewsCorrelation: 0.27427310754953754
Weighted FMeasure: 0.382912520114481
Iteration time: 21.0
Weighted AreaUnderPRC: 0.4652149304081597
Mean absolute error: 0.27683690004412215
Coverage of cases: 72.57683215130024
Instances selection time: 20.0
Test time: 15.0
Accumulative iteration time: 98.0
Weighted Recall: 0.458628841607565
Weighted FalsePositiveRate: 0.17414514420953672
Kappa statistic: 0.28439614668597263
Training time: 1.0
		
Iteration: 4
Labeled set size: 144
Unlabelled set size: 279
	
Mean region size: 44.2080378250591
Incorrectly Classified Instances: 52.4822695035461
Correctly Classified Instances: 47.5177304964539
Weighted Precision: 0.49674799956401955
Weighted AreaUnderROC: 0.7628475777981617
Root mean squared error: 0.4642946227566484
Relative absolute error: 73.99848610106885
Root relative squared error: 107.22425017273868
Weighted TruePositiveRate: 0.475177304964539
Weighted MatthewsCorrelation: 0.3100572896394568
Weighted FMeasure: 0.4165600540122441
Iteration time: 20.0
Weighted AreaUnderPRC: 0.4601123067638582
Mean absolute error: 0.27749432287900816
Coverage of cases: 72.10401891252955
Instances selection time: 18.0
Test time: 16.0
Accumulative iteration time: 118.0
Weighted Recall: 0.475177304964539
Weighted FalsePositiveRate: 0.168816051777283
Kappa statistic: 0.305973127577491
Training time: 2.0
		
Iteration: 5
Labeled set size: 164
Unlabelled set size: 259
	
Mean region size: 44.976359338061464
Incorrectly Classified Instances: 52.4822695035461
Correctly Classified Instances: 47.5177304964539
Weighted Precision: 0.5118994357955694
Weighted AreaUnderROC: 0.7721121426303711
Root mean squared error: 0.45476414249830366
Relative absolute error: 72.72511115088889
Root relative squared error: 105.02328003567398
Weighted TruePositiveRate: 0.475177304964539
Weighted MatthewsCorrelation: 0.3145332200907922
Weighted FMeasure: 0.4171431974741435
Iteration time: 17.0
Weighted AreaUnderPRC: 0.4820367096796205
Mean absolute error: 0.27271916681583336
Coverage of cases: 75.177304964539
Instances selection time: 16.0
Test time: 18.0
Accumulative iteration time: 135.0
Weighted Recall: 0.475177304964539
Weighted FalsePositiveRate: 0.16921059059600826
Kappa statistic: 0.3057473643743254
Training time: 1.0
		
Iteration: 6
Labeled set size: 184
Unlabelled set size: 239
	
Mean region size: 44.385342789598106
Incorrectly Classified Instances: 52.4822695035461
Correctly Classified Instances: 47.5177304964539
Weighted Precision: 0.549453192116435
Weighted AreaUnderROC: 0.7715177783033474
Root mean squared error: 0.4582479229951171
Relative absolute error: 72.84581622477315
Root relative squared error: 105.8278246787271
Weighted TruePositiveRate: 0.475177304964539
Weighted MatthewsCorrelation: 0.3269378554341881
Weighted FMeasure: 0.41162588878061523
Iteration time: 16.0
Weighted AreaUnderPRC: 0.495554627101719
Mean absolute error: 0.27317181084289927
Coverage of cases: 73.99527186761229
Instances selection time: 15.0
Test time: 17.0
Accumulative iteration time: 151.0
Weighted Recall: 0.475177304964539
Weighted FalsePositiveRate: 0.16825779361221474
Kappa statistic: 0.3063627365528652
Training time: 1.0
		
Iteration: 7
Labeled set size: 204
Unlabelled set size: 219
	
Mean region size: 43.73522458628842
Incorrectly Classified Instances: 52.4822695035461
Correctly Classified Instances: 47.5177304964539
Weighted Precision: 0.603635782723865
Weighted AreaUnderROC: 0.7783729689701869
Root mean squared error: 0.4600872505894737
Relative absolute error: 72.89241734147662
Root relative squared error: 106.25259919141898
Weighted TruePositiveRate: 0.475177304964539
Weighted MatthewsCorrelation: 0.3448899891062022
Weighted FMeasure: 0.41832919258525897
Iteration time: 15.0
Weighted AreaUnderPRC: 0.5418543040914715
Mean absolute error: 0.2733465650305373
Coverage of cases: 73.28605200945627
Instances selection time: 14.0
Test time: 17.0
Accumulative iteration time: 166.0
Weighted Recall: 0.475177304964539
Weighted FalsePositiveRate: 0.16788037090697083
Kappa statistic: 0.3064037225792156
Training time: 1.0
		
Iteration: 8
Labeled set size: 224
Unlabelled set size: 199
	
Mean region size: 44.858156028368796
Incorrectly Classified Instances: 54.37352245862884
Correctly Classified Instances: 45.62647754137116
Weighted Precision: 0.5553739416879345
Weighted AreaUnderROC: 0.7786372144757807
Root mean squared error: 0.458922566705839
Relative absolute error: 73.62816313490147
Root relative squared error: 105.98362696992406
Weighted TruePositiveRate: 0.4562647754137116
Weighted MatthewsCorrelation: 0.3112708501635421
Weighted FMeasure: 0.407063647116634
Iteration time: 13.0
Weighted AreaUnderPRC: 0.5366166394856013
Mean absolute error: 0.2761056117558805
Coverage of cases: 74.46808510638297
Instances selection time: 12.0
Test time: 18.0
Accumulative iteration time: 179.0
Weighted Recall: 0.4562647754137116
Weighted FalsePositiveRate: 0.1746578873935275
Kappa statistic: 0.2808143230976212
Training time: 1.0
		
Iteration: 9
Labeled set size: 244
Unlabelled set size: 179
	
Mean region size: 45.0354609929078
Incorrectly Classified Instances: 53.90070921985816
Correctly Classified Instances: 46.09929078014184
Weighted Precision: 0.5615979427875107
Weighted AreaUnderROC: 0.7804630816137477
Root mean squared error: 0.45692476742883226
Relative absolute error: 73.1495294638062
Root relative squared error: 105.52225498977738
Weighted TruePositiveRate: 0.46099290780141844
Weighted MatthewsCorrelation: 0.31751399809328973
Weighted FMeasure: 0.40827868341209556
Iteration time: 13.0
Weighted AreaUnderPRC: 0.5368080282479784
Mean absolute error: 0.27431073548927326
Coverage of cases: 74.46808510638297
Instances selection time: 11.0
Test time: 17.0
Accumulative iteration time: 192.0
Weighted Recall: 0.46099290780141844
Weighted FalsePositiveRate: 0.17304673227294604
Kappa statistic: 0.28721037655666826
Training time: 2.0
		
Iteration: 10
Labeled set size: 264
Unlabelled set size: 159
	
Mean region size: 45.33096926713948
Incorrectly Classified Instances: 50.59101654846336
Correctly Classified Instances: 49.40898345153664
Weighted Precision: 0.5634463549644111
Weighted AreaUnderROC: 0.7805703209229994
Root mean squared error: 0.44438266831827267
Relative absolute error: 70.5165611321476
Root relative squared error: 102.62578127070356
Weighted TruePositiveRate: 0.4940898345153664
Weighted MatthewsCorrelation: 0.3514129806762295
Weighted FMeasure: 0.44302860765116553
Iteration time: 12.0
Weighted AreaUnderPRC: 0.5413612907677654
Mean absolute error: 0.2644371042455535
Coverage of cases: 76.35933806146572
Instances selection time: 10.0
Test time: 18.0
Accumulative iteration time: 204.0
Weighted Recall: 0.4940898345153664
Weighted FalsePositiveRate: 0.16278172189927761
Kappa statistic: 0.33066155973410427
Training time: 2.0
		
Iteration: 11
Labeled set size: 284
Unlabelled set size: 139
	
Mean region size: 45.62647754137116
Incorrectly Classified Instances: 50.118203309692674
Correctly Classified Instances: 49.881796690307326
Weighted Precision: 0.5636924791537922
Weighted AreaUnderROC: 0.7837096706164906
Root mean squared error: 0.4400606127218722
Relative absolute error: 69.4171604569166
Root relative squared error: 101.62764528588983
Weighted TruePositiveRate: 0.4988179669030733
Weighted MatthewsCorrelation: 0.3572667462352914
Weighted FMeasure: 0.45079760562172655
Iteration time: 10.0
Weighted AreaUnderPRC: 0.5433386992563758
Mean absolute error: 0.26031435171343725
Coverage of cases: 76.8321513002364
Instances selection time: 8.0
Test time: 17.0
Accumulative iteration time: 214.0
Weighted Recall: 0.4988179669030733
Weighted FalsePositiveRate: 0.16095822774768156
Kappa statistic: 0.33699549746039015
Training time: 2.0
		
Iteration: 12
Labeled set size: 304
Unlabelled set size: 119
	
Mean region size: 44.79905437352246
Incorrectly Classified Instances: 49.881796690307326
Correctly Classified Instances: 50.118203309692674
Weighted Precision: 0.5720965142005782
Weighted AreaUnderROC: 0.7935530011947964
Root mean squared error: 0.4440788196630947
Relative absolute error: 70.10125769648286
Root relative squared error: 102.55561042955961
Weighted TruePositiveRate: 0.5011820330969267
Weighted MatthewsCorrelation: 0.36403771784218575
Weighted FMeasure: 0.459411820183446
Iteration time: 10.0
Weighted AreaUnderPRC: 0.556195379662789
Mean absolute error: 0.26287971636181073
Coverage of cases: 76.59574468085107
Instances selection time: 8.0
Test time: 17.0
Accumulative iteration time: 224.0
Weighted Recall: 0.5011820330969267
Weighted FalsePositiveRate: 0.15989510229142012
Kappa statistic: 0.34014726974316506
Training time: 2.0
		
Iteration: 13
Labeled set size: 324
Unlabelled set size: 99
	
Mean region size: 45.212765957446805
Incorrectly Classified Instances: 50.59101654846336
Correctly Classified Instances: 49.40898345153664
Weighted Precision: 0.5992056790818401
Weighted AreaUnderROC: 0.7857703126928153
Root mean squared error: 0.44643456552568955
Relative absolute error: 70.33944515297286
Root relative squared error: 103.0996466327242
Weighted TruePositiveRate: 0.4940898345153664
Weighted MatthewsCorrelation: 0.3654390168997346
Weighted FMeasure: 0.45220728492142376
Iteration time: 8.0
Weighted AreaUnderPRC: 0.556428007342324
Mean absolute error: 0.2637729193236482
Coverage of cases: 76.35933806146572
Instances selection time: 6.0
Test time: 17.0
Accumulative iteration time: 232.0
Weighted Recall: 0.4940898345153664
Weighted FalsePositiveRate: 0.16160870999167187
Kappa statistic: 0.33104243337915135
Training time: 2.0
		
Iteration: 14
Labeled set size: 344
Unlabelled set size: 79
	
Mean region size: 46.21749408983452
Incorrectly Classified Instances: 50.8274231678487
Correctly Classified Instances: 49.1725768321513
Weighted Precision: 0.55752019813222
Weighted AreaUnderROC: 0.7906641318973393
Root mean squared error: 0.4480800995541967
Relative absolute error: 69.82720982765586
Root relative squared error: 103.47966643845191
Weighted TruePositiveRate: 0.491725768321513
Weighted MatthewsCorrelation: 0.3460411439303398
Weighted FMeasure: 0.4324562262231332
Iteration time: 7.0
Weighted AreaUnderPRC: 0.5676334511960113
Mean absolute error: 0.2618520368537095
Coverage of cases: 77.54137115839244
Instances selection time: 5.0
Test time: 17.0
Accumulative iteration time: 239.0
Weighted Recall: 0.491725768321513
Weighted FalsePositiveRate: 0.1625053874458179
Kappa statistic: 0.3280058521014364
Training time: 2.0
		
Iteration: 15
Labeled set size: 364
Unlabelled set size: 59
	
Mean region size: 45.80378250591016
Incorrectly Classified Instances: 54.1371158392435
Correctly Classified Instances: 45.8628841607565
Weighted Precision: 0.5046757201980168
Weighted AreaUnderROC: 0.769242242123324
Root mean squared error: 0.45860762150084644
Relative absolute error: 72.94466868708038
Root relative squared error: 105.9108934903711
Weighted TruePositiveRate: 0.458628841607565
Weighted MatthewsCorrelation: 0.30055062531600657
Weighted FMeasure: 0.43358663652002094
Iteration time: 5.0
Weighted AreaUnderPRC: 0.5261524870181616
Mean absolute error: 0.2735425075765514
Coverage of cases: 76.12293144208037
Instances selection time: 3.0
Test time: 18.0
Accumulative iteration time: 244.0
Weighted Recall: 0.458628841607565
Weighted FalsePositiveRate: 0.17528255956229363
Kappa statistic: 0.2827110762258786
Training time: 2.0
		
Iteration: 16
Labeled set size: 384
Unlabelled set size: 39
	
Mean region size: 47.695035460992905
Incorrectly Classified Instances: 52.009456264775416
Correctly Classified Instances: 47.990543735224584
Weighted Precision: 0.5339617069941568
Weighted AreaUnderROC: 0.778177414280248
Root mean squared error: 0.44857249640312186
Relative absolute error: 72.41477279945778
Root relative squared error: 103.59338061976193
Weighted TruePositiveRate: 0.4799054373522459
Weighted MatthewsCorrelation: 0.3327055665417007
Weighted FMeasure: 0.4602856074154211
Iteration time: 5.0
Weighted AreaUnderPRC: 0.5394036412715739
Mean absolute error: 0.2715553979979667
Coverage of cases: 79.90543735224587
Instances selection time: 3.0
Test time: 15.0
Accumulative iteration time: 249.0
Weighted Recall: 0.4799054373522459
Weighted FalsePositiveRate: 0.16808544627743083
Kappa statistic: 0.3108963678773743
Training time: 2.0
		
Iteration: 17
Labeled set size: 404
Unlabelled set size: 19
	
Mean region size: 47.695035460992905
Incorrectly Classified Instances: 53.427895981087474
Correctly Classified Instances: 46.572104018912526
Weighted Precision: 0.5111784043013696
Weighted AreaUnderROC: 0.7774006355323712
Root mean squared error: 0.45301316089112026
Relative absolute error: 72.7904584503247
Root relative squared error: 104.61890815477263
Weighted TruePositiveRate: 0.4657210401891253
Weighted MatthewsCorrelation: 0.3087183993251998
Weighted FMeasure: 0.432001132881317
Iteration time: 4.0
Weighted AreaUnderPRC: 0.540644435767735
Mean absolute error: 0.27296421918871766
Coverage of cases: 79.19621749408984
Instances selection time: 2.0
Test time: 15.0
Accumulative iteration time: 253.0
Weighted Recall: 0.4657210401891253
Weighted FalsePositiveRate: 0.17328058255882395
Kappa statistic: 0.2917879764418269
Training time: 2.0
		
Time end:Wed Nov 01 14.40.06 EET 2017