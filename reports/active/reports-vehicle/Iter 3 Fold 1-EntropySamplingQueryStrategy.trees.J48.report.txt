Wed Nov 01 14.39.40 EET 2017
Dataset: vehicle
Test set size: 423
Initial Labelled set size: 84
Initial Unlabelled set size: 339
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Nov 01 14.39.40 EET 2017
		
Iteration: 1
Labeled set size: 84
Unlabelled set size: 339
	
Mean region size: 39.657210401891255
Incorrectly Classified Instances: 41.371158392434985
Correctly Classified Instances: 58.628841607565015
Weighted Precision: 0.5419725099651093
Weighted AreaUnderROC: 0.7777282174336856
Root mean squared error: 0.39206332539816935
Relative absolute error: 56.499161463700496
Root relative squared error: 90.54314658320517
Weighted TruePositiveRate: 0.5862884160756501
Weighted MatthewsCorrelation: 0.43184581214178025
Weighted FMeasure: 0.5375377321196223
Iteration time: 10.0
Weighted AreaUnderPRC: 0.5317579871527601
Mean absolute error: 0.21187185548887685
Coverage of cases: 79.66903073286052
Instances selection time: 3.0
Test time: 5.0
Accumulative iteration time: 10.0
Weighted Recall: 0.5862884160756501
Weighted FalsePositiveRate: 0.1383194849546742
Kappa statistic: 0.44862799427958516
Training time: 7.0
		
Iteration: 2
Labeled set size: 104
Unlabelled set size: 319
	
Mean region size: 43.321513002364064
Incorrectly Classified Instances: 31.678486997635932
Correctly Classified Instances: 68.32151300236407
Weighted Precision: 0.6704266306000711
Weighted AreaUnderROC: 0.8137461499751703
Root mean squared error: 0.3807023018960211
Relative absolute error: 46.35864023073183
Root relative squared error: 87.9194305923112
Weighted TruePositiveRate: 0.6832151300236406
Weighted MatthewsCorrelation: 0.5707203797311475
Weighted FMeasure: 0.6681905781839956
Iteration time: 8.0
Weighted AreaUnderPRC: 0.5948800488086802
Mean absolute error: 0.17384490086524437
Coverage of cases: 82.97872340425532
Instances selection time: 3.0
Test time: 5.0
Accumulative iteration time: 18.0
Weighted Recall: 0.6832151300236406
Weighted FalsePositiveRate: 0.10637009288014228
Kappa statistic: 0.5776555793991416
Training time: 5.0
		
Iteration: 3
Labeled set size: 124
Unlabelled set size: 299
	
Mean region size: 36.46572104018912
Incorrectly Classified Instances: 32.6241134751773
Correctly Classified Instances: 67.37588652482269
Weighted Precision: 0.672959006537008
Weighted AreaUnderROC: 0.8064274132115401
Root mean squared error: 0.38128284152492226
Relative absolute error: 46.74710879805425
Root relative squared error: 88.05350047671972
Weighted TruePositiveRate: 0.6737588652482269
Weighted MatthewsCorrelation: 0.5629383110089288
Weighted FMeasure: 0.6613879519591276
Iteration time: 8.0
Weighted AreaUnderPRC: 0.5941058715337415
Mean absolute error: 0.17530165799270342
Coverage of cases: 76.8321513002364
Instances selection time: 2.0
Test time: 2.0
Accumulative iteration time: 26.0
Weighted Recall: 0.6737588652482269
Weighted FalsePositiveRate: 0.1093818111387266
Kappa statistic: 0.5651519666269368
Training time: 6.0
		
Iteration: 4
Labeled set size: 144
Unlabelled set size: 279
	
Mean region size: 35.1063829787234
Incorrectly Classified Instances: 28.84160756501182
Correctly Classified Instances: 71.15839243498817
Weighted Precision: 0.7005137827968932
Weighted AreaUnderROC: 0.814895931627555
Root mean squared error: 0.3690696621741259
Relative absolute error: 42.33159731480002
Root relative squared error: 85.23298752238233
Weighted TruePositiveRate: 0.7115839243498818
Weighted MatthewsCorrelation: 0.6097705930866554
Weighted FMeasure: 0.703187141639472
Iteration time: 15.0
Weighted AreaUnderPRC: 0.6161598707480564
Mean absolute error: 0.15874348993050008
Coverage of cases: 78.25059101654847
Instances selection time: 5.0
Test time: 2.0
Accumulative iteration time: 41.0
Weighted Recall: 0.7115839243498818
Weighted FalsePositiveRate: 0.0966827503939927
Kappa statistic: 0.6154602762999062
Training time: 10.0
		
Iteration: 5
Labeled set size: 164
Unlabelled set size: 259
	
Mean region size: 42.02127659574468
Incorrectly Classified Instances: 31.44208037825059
Correctly Classified Instances: 68.5579196217494
Weighted Precision: 0.7042461942203735
Weighted AreaUnderROC: 0.819549219537677
Root mean squared error: 0.37065643642376983
Relative absolute error: 45.16753673072369
Root relative squared error: 85.59943733845238
Weighted TruePositiveRate: 0.6855791962174941
Weighted MatthewsCorrelation: 0.5880246745814521
Weighted FMeasure: 0.6910666337638022
Iteration time: 20.0
Weighted AreaUnderPRC: 0.6464865237191925
Mean absolute error: 0.16937826274021384
Coverage of cases: 81.7966903073286
Instances selection time: 3.0
Test time: 5.0
Accumulative iteration time: 61.0
Weighted Recall: 0.6855791962174941
Weighted FalsePositiveRate: 0.10529611658754959
Kappa statistic: 0.5809010861306039
Training time: 17.0
		
Iteration: 6
Labeled set size: 184
Unlabelled set size: 239
	
Mean region size: 32.860520094562645
Incorrectly Classified Instances: 32.860520094562645
Correctly Classified Instances: 67.13947990543736
Weighted Precision: 0.6674353575603011
Weighted AreaUnderROC: 0.7888547265057398
Root mean squared error: 0.3971682990458098
Relative absolute error: 47.19328409138043
Root relative squared error: 91.72208974707364
Weighted TruePositiveRate: 0.6713947990543735
Weighted MatthewsCorrelation: 0.5597147685045497
Weighted FMeasure: 0.667969503473586
Iteration time: 12.0
Weighted AreaUnderPRC: 0.5704902851419956
Mean absolute error: 0.1769748153426766
Coverage of cases: 72.81323877068557
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 73.0
Weighted Recall: 0.6713947990543735
Weighted FalsePositiveRate: 0.11005397311512764
Kappa statistic: 0.5616318861974099
Training time: 11.0
		
Iteration: 7
Labeled set size: 204
Unlabelled set size: 219
	
Mean region size: 32.6241134751773
Incorrectly Classified Instances: 33.80614657210402
Correctly Classified Instances: 66.19385342789599
Weighted Precision: 0.649407025117814
Weighted AreaUnderROC: 0.8126543473295302
Root mean squared error: 0.38471053137978356
Relative absolute error: 45.842534140406514
Root relative squared error: 88.84509154088082
Weighted TruePositiveRate: 0.6619385342789598
Weighted MatthewsCorrelation: 0.5427644345005228
Weighted FMeasure: 0.6494276282199151
Iteration time: 12.0
Weighted AreaUnderPRC: 0.6038974113359382
Mean absolute error: 0.17190950302652444
Coverage of cases: 77.30496453900709
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 85.0
Weighted Recall: 0.6619385342789598
Weighted FalsePositiveRate: 0.11299511132563587
Kappa statistic: 0.5495039918970447
Training time: 11.0
		
Iteration: 8
Labeled set size: 224
Unlabelled set size: 199
	
Mean region size: 33.628841607565015
Incorrectly Classified Instances: 37.5886524822695
Correctly Classified Instances: 62.4113475177305
Weighted Precision: 0.634629298740972
Weighted AreaUnderROC: 0.7693304936637971
Root mean squared error: 0.4199540459589461
Relative absolute error: 51.727026508002744
Root relative squared error: 96.984232592668
Weighted TruePositiveRate: 0.624113475177305
Weighted MatthewsCorrelation: 0.5034575656595104
Weighted FMeasure: 0.6265814563686906
Iteration time: 13.0
Weighted AreaUnderPRC: 0.5365052970280058
Mean absolute error: 0.1939763494050103
Coverage of cases: 70.92198581560284
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 98.0
Weighted Recall: 0.624113475177305
Weighted FalsePositiveRate: 0.125309803037613
Kappa statistic: 0.498680679785331
Training time: 12.0
		
Iteration: 9
Labeled set size: 244
Unlabelled set size: 179
	
Mean region size: 34.692671394799056
Incorrectly Classified Instances: 29.314420803782507
Correctly Classified Instances: 70.68557919621749
Weighted Precision: 0.6965112849306972
Weighted AreaUnderROC: 0.8365912856534329
Root mean squared error: 0.3646767698570767
Relative absolute error: 40.593958865043376
Root relative squared error: 84.21849249767457
Weighted TruePositiveRate: 0.706855791962175
Weighted MatthewsCorrelation: 0.6041498081245518
Weighted FMeasure: 0.6997058000423035
Iteration time: 19.0
Weighted AreaUnderPRC: 0.6299458386734712
Mean absolute error: 0.15222734574391267
Coverage of cases: 80.37825059101655
Instances selection time: 5.0
Test time: 5.0
Accumulative iteration time: 117.0
Weighted Recall: 0.706855791962175
Weighted FalsePositiveRate: 0.09760196710103686
Kappa statistic: 0.6093601048617732
Training time: 14.0
		
Iteration: 10
Labeled set size: 264
Unlabelled set size: 159
	
Mean region size: 30.851063829787233
Incorrectly Classified Instances: 30.969267139479907
Correctly Classified Instances: 69.03073286052009
Weighted Precision: 0.6896437142219154
Weighted AreaUnderROC: 0.8241439905381097
Root mean squared error: 0.3707137631695943
Relative absolute error: 42.07062178693385
Root relative squared error: 85.61267638330578
Weighted TruePositiveRate: 0.6903073286052009
Weighted MatthewsCorrelation: 0.5860425551212874
Weighted FMeasure: 0.6899365643293034
Iteration time: 32.0
Weighted AreaUnderPRC: 0.6201483228699748
Mean absolute error: 0.15776483170100195
Coverage of cases: 77.54137115839244
Instances selection time: 6.0
Test time: 2.0
Accumulative iteration time: 149.0
Weighted Recall: 0.6903073286052009
Weighted FalsePositiveRate: 0.10401073159943096
Kappa statistic: 0.5869017444461011
Training time: 26.0
		
Iteration: 11
Labeled set size: 284
Unlabelled set size: 139
	
Mean region size: 31.20567375886525
Incorrectly Classified Instances: 29.55082742316785
Correctly Classified Instances: 70.44917257683215
Weighted Precision: 0.6979506338729564
Weighted AreaUnderROC: 0.8175663259895298
Root mean squared error: 0.3721094354660826
Relative absolute error: 41.36083905587453
Root relative squared error: 85.93499309373699
Weighted TruePositiveRate: 0.7044917257683215
Weighted MatthewsCorrelation: 0.6023110553430716
Weighted FMeasure: 0.6994895562859432
Iteration time: 36.0
Weighted AreaUnderPRC: 0.6128832713605283
Mean absolute error: 0.15510314645952947
Coverage of cases: 76.59574468085107
Instances selection time: 3.0
Test time: 2.0
Accumulative iteration time: 185.0
Weighted Recall: 0.7044917257683215
Weighted FalsePositiveRate: 0.09915346261908935
Kappa statistic: 0.6058927879311887
Training time: 33.0
		
Iteration: 12
Labeled set size: 304
Unlabelled set size: 119
	
Mean region size: 32.801418439716315
Incorrectly Classified Instances: 30.260047281323878
Correctly Classified Instances: 69.73995271867612
Weighted Precision: 0.6976161269052729
Weighted AreaUnderROC: 0.8560795765365304
Root mean squared error: 0.34715027056406994
Relative absolute error: 40.6861420691208
Root relative squared error: 80.17092086376688
Weighted TruePositiveRate: 0.6973995271867612
Weighted MatthewsCorrelation: 0.5960165849914286
Weighted FMeasure: 0.6893405444407237
Iteration time: 36.0
Weighted AreaUnderPRC: 0.6662984175153035
Mean absolute error: 0.152573032759203
Coverage of cases: 82.50591016548464
Instances selection time: 5.0
Test time: 2.0
Accumulative iteration time: 221.0
Weighted Recall: 0.6973995271867612
Weighted FalsePositiveRate: 0.10034934187541616
Kappa statistic: 0.5971158782954216
Training time: 31.0
		
Iteration: 13
Labeled set size: 324
Unlabelled set size: 99
	
Mean region size: 34.751773049645394
Incorrectly Classified Instances: 26.713947990543737
Correctly Classified Instances: 73.28605200945627
Weighted Precision: 0.7283212708168081
Weighted AreaUnderROC: 0.8631736020513546
Root mean squared error: 0.3325521278705945
Relative absolute error: 38.53965305586384
Root relative squared error: 76.79962421826824
Weighted TruePositiveRate: 0.7328605200945626
Weighted MatthewsCorrelation: 0.6407497117463336
Weighted FMeasure: 0.723874824467529
Iteration time: 19.0
Weighted AreaUnderPRC: 0.6760248253589112
Mean absolute error: 0.14452369895948938
Coverage of cases: 85.81560283687944
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 240.0
Weighted Recall: 0.7328605200945626
Weighted FalsePositiveRate: 0.08963703675184609
Kappa statistic: 0.6437801824360578
Training time: 18.0
		
Iteration: 14
Labeled set size: 344
Unlabelled set size: 79
	
Mean region size: 34.751773049645394
Incorrectly Classified Instances: 26.713947990543737
Correctly Classified Instances: 73.28605200945627
Weighted Precision: 0.7283212708168081
Weighted AreaUnderROC: 0.8631736020513546
Root mean squared error: 0.33274693634607583
Relative absolute error: 38.36916751303788
Root relative squared error: 76.84461330857205
Weighted TruePositiveRate: 0.7328605200945626
Weighted MatthewsCorrelation: 0.6407497117463336
Weighted FMeasure: 0.723874824467529
Iteration time: 20.0
Weighted AreaUnderPRC: 0.6760248253589112
Mean absolute error: 0.14388437817389205
Coverage of cases: 85.81560283687944
Instances selection time: 2.0
Test time: 2.0
Accumulative iteration time: 260.0
Weighted Recall: 0.7328605200945626
Weighted FalsePositiveRate: 0.08963703675184609
Kappa statistic: 0.6437801824360578
Training time: 18.0
		
Iteration: 15
Labeled set size: 364
Unlabelled set size: 59
	
Mean region size: 35.16548463356974
Incorrectly Classified Instances: 27.423167848699762
Correctly Classified Instances: 72.57683215130024
Weighted Precision: 0.7194677633370107
Weighted AreaUnderROC: 0.8562596128906051
Root mean squared error: 0.3363339048884469
Relative absolute error: 39.73316144585018
Root relative squared error: 77.67298820997712
Weighted TruePositiveRate: 0.7257683215130024
Weighted MatthewsCorrelation: 0.6303322750097358
Weighted FMeasure: 0.7149391786287568
Iteration time: 19.0
Weighted AreaUnderPRC: 0.6668338890602141
Mean absolute error: 0.14899935542193818
Coverage of cases: 85.81560283687944
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 279.0
Weighted Recall: 0.7257683215130024
Weighted FalsePositiveRate: 0.09211900733978541
Kappa statistic: 0.6342984907769705
Training time: 19.0
		
Iteration: 16
Labeled set size: 384
Unlabelled set size: 39
	
Mean region size: 34.988179669030735
Incorrectly Classified Instances: 26.713947990543737
Correctly Classified Instances: 73.28605200945627
Weighted Precision: 0.7294531550410318
Weighted AreaUnderROC: 0.8617248109969972
Root mean squared error: 0.3309607154686184
Relative absolute error: 38.58790271193896
Root relative squared error: 76.43210326679919
Weighted TruePositiveRate: 0.7328605200945626
Weighted MatthewsCorrelation: 0.6411333264865967
Weighted FMeasure: 0.7225892236493466
Iteration time: 20.0
Weighted AreaUnderPRC: 0.6787908348108477
Mean absolute error: 0.1447046351697711
Coverage of cases: 86.28841607565012
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 299.0
Weighted Recall: 0.7328605200945626
Weighted FalsePositiveRate: 0.08967709483606821
Kappa statistic: 0.6437589434772238
Training time: 19.0
		
Iteration: 17
Labeled set size: 404
Unlabelled set size: 19
	
Mean region size: 34.988179669030735
Incorrectly Classified Instances: 27.89598108747045
Correctly Classified Instances: 72.10401891252955
Weighted Precision: 0.715724568646131
Weighted AreaUnderROC: 0.8530898975108724
Root mean squared error: 0.33885039797995437
Relative absolute error: 40.270267278464296
Root relative squared error: 78.2541473954954
Weighted TruePositiveRate: 0.7210401891252955
Weighted MatthewsCorrelation: 0.6246910388997224
Weighted FMeasure: 0.7096844436732388
Iteration time: 21.0
Weighted AreaUnderPRC: 0.666407974539823
Mean absolute error: 0.1510135022942411
Coverage of cases: 85.34278959810875
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 320.0
Weighted Recall: 0.7210401891252955
Weighted FalsePositiveRate: 0.09353246577078475
Kappa statistic: 0.6279683377308708
Training time: 21.0
		
Time end:Wed Nov 01 14.39.41 EET 2017