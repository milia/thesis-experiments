Wed Nov 01 14.45.03 EET 2017
Dataset: vehicle
Test set size: 423
Initial Labelled set size: 84
Initial Unlabelled set size: 339
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Wed Nov 01 14.45.03 EET 2017
		
Iteration: 1
Labeled set size: 84
Unlabelled set size: 339
	
Mean region size: 63.652482269503544
Incorrectly Classified Instances: 33.096926713947994
Correctly Classified Instances: 66.903073286052
Weighted Precision: 0.6478000711865357
Weighted AreaUnderROC: 0.8379024503444362
Root mean squared error: 0.3590768305150948
Relative absolute error: 60.551615445232464
Root relative squared error: 82.92524190305905
Weighted TruePositiveRate: 0.6690307328605201
Weighted MatthewsCorrelation: 0.5467610217454545
Weighted FMeasure: 0.6404386489270931
Iteration time: 10.0
Weighted AreaUnderPRC: 0.6620098885456306
Mean absolute error: 0.22706855791962172
Coverage of cases: 89.83451536643027
Instances selection time: 2.0
Test time: 2.0
Accumulative iteration time: 10.0
Weighted Recall: 0.6690307328605201
Weighted FalsePositiveRate: 0.11256442642602729
Kappa statistic: 0.5580168225275586
Training time: 8.0
		
Iteration: 2
Labeled set size: 104
Unlabelled set size: 319
	
Mean region size: 63.94799054373522
Incorrectly Classified Instances: 34.751773049645394
Correctly Classified Instances: 65.2482269503546
Weighted Precision: 0.6630521783710581
Weighted AreaUnderROC: 0.8589479714466635
Root mean squared error: 0.3415304173931338
Relative absolute error: 58.66036249014971
Root relative squared error: 78.87307136734842
Weighted TruePositiveRate: 0.6524822695035462
Weighted MatthewsCorrelation: 0.540266413112098
Weighted FMeasure: 0.6475982896163189
Iteration time: 13.0
Weighted AreaUnderPRC: 0.6889586670876848
Mean absolute error: 0.2199763593380614
Coverage of cases: 96.92671394799055
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 23.0
Weighted Recall: 0.6524822695035462
Weighted FalsePositiveRate: 0.1172489359469591
Kappa statistic: 0.5364366282233837
Training time: 12.0
		
Iteration: 3
Labeled set size: 124
Unlabelled set size: 299
	
Mean region size: 60.16548463356974
Incorrectly Classified Instances: 32.6241134751773
Correctly Classified Instances: 67.37588652482269
Weighted Precision: 0.6996767968829183
Weighted AreaUnderROC: 0.8524032661664774
Root mean squared error: 0.3440648407745413
Relative absolute error: 57.21040189125299
Root relative squared error: 79.45837137594687
Weighted TruePositiveRate: 0.6737588652482269
Weighted MatthewsCorrelation: 0.5747995708053278
Weighted FMeasure: 0.6602969344926597
Iteration time: 14.0
Weighted AreaUnderPRC: 0.6956836759312603
Mean absolute error: 0.21453900709219872
Coverage of cases: 93.85342789598108
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 37.0
Weighted Recall: 0.6737588652482269
Weighted FalsePositiveRate: 0.10896408124737758
Kappa statistic: 0.5654044878571748
Training time: 13.0
		
Iteration: 4
Labeled set size: 144
Unlabelled set size: 279
	
Mean region size: 65.42553191489361
Incorrectly Classified Instances: 38.297872340425535
Correctly Classified Instances: 61.702127659574465
Weighted Precision: 0.7161613589733007
Weighted AreaUnderROC: 0.8663799387645429
Root mean squared error: 0.354838224384907
Relative absolute error: 63.230890464933026
Root relative squared error: 81.94637774695795
Weighted TruePositiveRate: 0.6170212765957447
Weighted MatthewsCorrelation: 0.531384723102017
Weighted FMeasure: 0.603114023314945
Iteration time: 16.0
Weighted AreaUnderPRC: 0.706669662255697
Mean absolute error: 0.23711583924349885
Coverage of cases: 97.87234042553192
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 53.0
Weighted Recall: 0.6170212765957447
Weighted FalsePositiveRate: 0.12766638291285565
Kappa statistic: 0.4899403791617355
Training time: 15.0
		
Iteration: 5
Labeled set size: 164
Unlabelled set size: 259
	
Mean region size: 60.638297872340424
Incorrectly Classified Instances: 37.35224586288416
Correctly Classified Instances: 62.64775413711584
Weighted Precision: 0.759115833876513
Weighted AreaUnderROC: 0.8640222946318209
Root mean squared error: 0.35204569335920866
Relative absolute error: 59.448384554767586
Root relative squared error: 81.30147033119503
Weighted TruePositiveRate: 0.6264775413711584
Weighted MatthewsCorrelation: 0.5597994561888444
Weighted FMeasure: 0.6158844941063926
Iteration time: 18.0
Weighted AreaUnderPRC: 0.7014196364624402
Mean absolute error: 0.22293144208037843
Coverage of cases: 95.0354609929078
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 71.0
Weighted Recall: 0.6264775413711584
Weighted FalsePositiveRate: 0.12505746208634402
Kappa statistic: 0.5022195078353096
Training time: 17.0
		
Iteration: 6
Labeled set size: 184
Unlabelled set size: 239
	
Mean region size: 61.22931442080378
Incorrectly Classified Instances: 39.479905437352244
Correctly Classified Instances: 60.520094562647756
Weighted Precision: 0.7273926041904365
Weighted AreaUnderROC: 0.8557781256676077
Root mean squared error: 0.36406231768530023
Relative absolute error: 62.15918045705286
Root relative squared error: 84.07659084696286
Weighted TruePositiveRate: 0.6052009456264775
Weighted MatthewsCorrelation: 0.5228989894134791
Weighted FMeasure: 0.5814887611132108
Iteration time: 21.0
Weighted AreaUnderPRC: 0.6969292906202531
Mean absolute error: 0.23309692671394824
Coverage of cases: 93.61702127659575
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 92.0
Weighted Recall: 0.6052009456264775
Weighted FalsePositiveRate: 0.1318994160164694
Kappa statistic: 0.47404511949966494
Training time: 20.0
		
Iteration: 7
Labeled set size: 204
Unlabelled set size: 219
	
Mean region size: 59.86997635933806
Incorrectly Classified Instances: 37.5886524822695
Correctly Classified Instances: 62.4113475177305
Weighted Precision: 0.7463814046972572
Weighted AreaUnderROC: 0.8611824750183573
Root mean squared error: 0.36027635833277
Relative absolute error: 59.66903073286061
Root relative squared error: 83.20226098643315
Weighted TruePositiveRate: 0.624113475177305
Weighted MatthewsCorrelation: 0.5339590505351739
Weighted FMeasure: 0.5760218100012731
Iteration time: 22.0
Weighted AreaUnderPRC: 0.6933760075070531
Mean absolute error: 0.22375886524822727
Coverage of cases: 94.08983451536643
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 114.0
Weighted Recall: 0.624113475177305
Weighted FalsePositiveRate: 0.123600239832481
Kappa statistic: 0.5006607667864461
Training time: 21.0
		
Iteration: 8
Labeled set size: 224
Unlabelled set size: 199
	
Mean region size: 59.515366430260045
Incorrectly Classified Instances: 30.969267139479907
Correctly Classified Instances: 69.03073286052009
Weighted Precision: 0.746244177146898
Weighted AreaUnderROC: 0.878925004636032
Root mean squared error: 0.33700109785091015
Relative absolute error: 54.468085106383036
Root relative squared error: 77.82706982456897
Weighted TruePositiveRate: 0.6903073286052009
Weighted MatthewsCorrelation: 0.6037198710175905
Weighted FMeasure: 0.6471303609110006
Iteration time: 25.0
Weighted AreaUnderPRC: 0.7303688221120389
Mean absolute error: 0.20425531914893638
Coverage of cases: 93.85342789598108
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 139.0
Weighted Recall: 0.6903073286052009
Weighted FalsePositiveRate: 0.10236641194058074
Kappa statistic: 0.588175838876296
Training time: 24.0
		
Iteration: 9
Labeled set size: 244
Unlabelled set size: 179
	
Mean region size: 56.146572104018915
Incorrectly Classified Instances: 34.04255319148936
Correctly Classified Instances: 65.95744680851064
Weighted Precision: 0.7337849437940253
Weighted AreaUnderROC: 0.8825194213071528
Root mean squared error: 0.3446140816162726
Relative absolute error: 54.31048069345949
Root relative squared error: 79.58521311507627
Weighted TruePositiveRate: 0.6595744680851063
Weighted MatthewsCorrelation: 0.5709965190588384
Weighted FMeasure: 0.6234362091767431
Iteration time: 25.0
Weighted AreaUnderPRC: 0.7326911700648849
Mean absolute error: 0.2036643026004731
Coverage of cases: 95.27186761229315
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 164.0
Weighted Recall: 0.6595744680851063
Weighted FalsePositiveRate: 0.1127032936294918
Kappa statistic: 0.5473447973485129
Training time: 24.0
		
Iteration: 10
Labeled set size: 264
Unlabelled set size: 159
	
Mean region size: 55.20094562647754
Incorrectly Classified Instances: 32.38770685579196
Correctly Classified Instances: 67.61229314420804
Weighted Precision: 0.7248951834748736
Weighted AreaUnderROC: 0.8707046865598831
Root mean squared error: 0.34076814910152686
Relative absolute error: 51.631205673758934
Root relative squared error: 78.69703304600682
Weighted TruePositiveRate: 0.6761229314420804
Weighted MatthewsCorrelation: 0.5842774508482896
Weighted FMeasure: 0.6444539386847132
Iteration time: 27.0
Weighted AreaUnderPRC: 0.7181991149609791
Mean absolute error: 0.193617021276596
Coverage of cases: 92.19858156028369
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 191.0
Weighted Recall: 0.6761229314420804
Weighted FalsePositiveRate: 0.10698753827131044
Kappa statistic: 0.569390469538338
Training time: 26.0
		
Iteration: 11
Labeled set size: 284
Unlabelled set size: 139
	
Mean region size: 48.3451536643026
Incorrectly Classified Instances: 29.314420803782507
Correctly Classified Instances: 70.68557919621749
Weighted Precision: 0.7457363595487274
Weighted AreaUnderROC: 0.8748653501765202
Root mean squared error: 0.32976816844149437
Relative absolute error: 46.58786446020496
Root relative squared error: 76.15669632794666
Weighted TruePositiveRate: 0.706855791962175
Weighted MatthewsCorrelation: 0.6168088987898066
Weighted FMeasure: 0.6619269366904098
Iteration time: 29.0
Weighted AreaUnderPRC: 0.7230127807560159
Mean absolute error: 0.1747044917257686
Coverage of cases: 91.01654846335697
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 220.0
Weighted Recall: 0.706855791962175
Weighted FalsePositiveRate: 0.09775551411059939
Kappa statistic: 0.6097001986769751
Training time: 28.0
		
Iteration: 12
Labeled set size: 304
Unlabelled set size: 119
	
Mean region size: 50.05910165484634
Incorrectly Classified Instances: 26.713947990543737
Correctly Classified Instances: 73.28605200945627
Weighted Precision: 0.7984958716877837
Weighted AreaUnderROC: 0.8941888222944996
Root mean squared error: 0.3184440600566178
Relative absolute error: 45.79984239558715
Root relative squared error: 73.54150551821026
Weighted TruePositiveRate: 0.7328605200945626
Weighted MatthewsCorrelation: 0.6591962839363319
Weighted FMeasure: 0.6878448280261846
Iteration time: 31.0
Weighted AreaUnderPRC: 0.7609368520341734
Mean absolute error: 0.1717494089834518
Coverage of cases: 92.19858156028369
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 251.0
Weighted Recall: 0.7328605200945626
Weighted FalsePositiveRate: 0.08875065845973568
Kappa statistic: 0.6445431019096912
Training time: 30.0
		
Iteration: 13
Labeled set size: 324
Unlabelled set size: 99
	
Mean region size: 52.304964539007095
Incorrectly Classified Instances: 29.55082742316785
Correctly Classified Instances: 70.44917257683215
Weighted Precision: 0.7046452499789055
Weighted AreaUnderROC: 0.8809397592020335
Root mean squared error: 0.3255836730126187
Relative absolute error: 48.63672182821123
Root relative squared error: 75.190328503033
Weighted TruePositiveRate: 0.7044917257683215
Weighted MatthewsCorrelation: 0.5974765598127746
Weighted FMeasure: 0.6461859096906574
Iteration time: 31.0
Weighted AreaUnderPRC: 0.7192857657455323
Mean absolute error: 0.18238770685579211
Coverage of cases: 93.14420803782505
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 282.0
Weighted Recall: 0.7044917257683215
Weighted FalsePositiveRate: 0.09869671826575843
Kappa statistic: 0.6066082376049046
Training time: 31.0
		
Iteration: 14
Labeled set size: 344
Unlabelled set size: 79
	
Mean region size: 50.29550827423168
Incorrectly Classified Instances: 30.49645390070922
Correctly Classified Instances: 69.50354609929079
Weighted Precision: 0.7232907630109295
Weighted AreaUnderROC: 0.8942421084174318
Root mean squared error: 0.32060816912353796
Relative absolute error: 45.768321513002434
Root relative squared error: 74.04128509914707
Weighted TruePositiveRate: 0.6950354609929078
Weighted MatthewsCorrelation: 0.595922419995706
Weighted FMeasure: 0.6448815450922842
Iteration time: 34.0
Weighted AreaUnderPRC: 0.7441926299938644
Mean absolute error: 0.17163120567375914
Coverage of cases: 94.32624113475177
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 316.0
Weighted Recall: 0.6950354609929078
Weighted FalsePositiveRate: 0.10096623152312778
Kappa statistic: 0.5942792987047749
Training time: 33.0
		
Iteration: 15
Labeled set size: 364
Unlabelled set size: 59
	
Mean region size: 53.959810874704495
Incorrectly Classified Instances: 27.89598108747045
Correctly Classified Instances: 72.10401891252955
Weighted Precision: 0.720086900012423
Weighted AreaUnderROC: 0.9001643083120014
Root mean squared error: 0.30457504403153024
Relative absolute error: 46.49330181245077
Root relative squared error: 70.33859346401844
Weighted TruePositiveRate: 0.7210401891252955
Weighted MatthewsCorrelation: 0.6256512101673826
Weighted FMeasure: 0.6971393317074783
Iteration time: 38.0
Weighted AreaUnderPRC: 0.7506633301315305
Mean absolute error: 0.17434988179669036
Coverage of cases: 96.92671394799055
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 354.0
Weighted Recall: 0.7210401891252955
Weighted FalsePositiveRate: 0.092950823664273
Kappa statistic: 0.6285939639264243
Training time: 37.0
		
Iteration: 16
Labeled set size: 384
Unlabelled set size: 39
	
Mean region size: 53.664302600472816
Incorrectly Classified Instances: 29.314420803782507
Correctly Classified Instances: 70.68557919621749
Weighted Precision: 0.6971143607809326
Weighted AreaUnderROC: 0.8997243869719286
Root mean squared error: 0.3058723910746922
Relative absolute error: 47.691095350669805
Root relative squared error: 70.63820292985922
Weighted TruePositiveRate: 0.706855791962175
Weighted MatthewsCorrelation: 0.6026619226591948
Weighted FMeasure: 0.6802465566597086
Iteration time: 40.0
Weighted AreaUnderPRC: 0.7364076325934624
Mean absolute error: 0.17884160756501177
Coverage of cases: 98.3451536643026
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 394.0
Weighted Recall: 0.706855791962175
Weighted FalsePositiveRate: 0.09767024361771241
Kappa statistic: 0.6097640818087806
Training time: 40.0
		
Iteration: 17
Labeled set size: 404
Unlabelled set size: 19
	
Mean region size: 52.12765957446808
Incorrectly Classified Instances: 26.95035460992908
Correctly Classified Instances: 73.04964539007092
Weighted Precision: 0.7170730610223953
Weighted AreaUnderROC: 0.9061682472952073
Root mean squared error: 0.29673160497336803
Relative absolute error: 44.25531914893613
Root relative squared error: 68.5272288033775
Weighted TruePositiveRate: 0.7304964539007093
Weighted MatthewsCorrelation: 0.6336477840608007
Weighted FMeasure: 0.7209042983557089
Iteration time: 44.0
Weighted AreaUnderPRC: 0.773625861452157
Mean absolute error: 0.1659574468085105
Coverage of cases: 96.92671394799055
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 438.0
Weighted Recall: 0.7304964539007093
Weighted FalsePositiveRate: 0.09023164033859547
Kappa statistic: 0.6408927414490293
Training time: 43.0
		
Time end:Wed Nov 01 14.45.04 EET 2017