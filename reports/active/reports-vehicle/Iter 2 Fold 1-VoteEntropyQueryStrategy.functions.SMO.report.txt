Wed Nov 01 14.44.24 EET 2017
Dataset: vehicle
Test set size: 423
Initial Labelled set size: 84
Initial Unlabelled set size: 339
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Wed Nov 01 14.44.24 EET 2017
		
Iteration: 1
Labeled set size: 84
Unlabelled set size: 339
	
Mean region size: 75.76832151300236
Incorrectly Classified Instances: 35.9338061465721
Correctly Classified Instances: 64.06619385342789
Weighted Precision: 0.6435965452499278
Weighted AreaUnderROC: 0.8077732896482025
Root mean squared error: 0.37718803920088284
Relative absolute error: 78.90727607039655
Root relative squared error: 87.10784638709474
Weighted TruePositiveRate: 0.640661938534279
Weighted MatthewsCorrelation: 0.5199301462414682
Weighted FMeasure: 0.6158149940782246
Iteration time: 61.0
Weighted AreaUnderPRC: 0.5796853538179392
Mean absolute error: 0.29590228526398704
Coverage of cases: 95.74468085106383
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 61.0
Weighted Recall: 0.640661938534279
Weighted FalsePositiveRate: 0.12086792594233248
Kappa statistic: 0.5205905379711442
Training time: 60.0
		
Iteration: 2
Labeled set size: 104
Unlabelled set size: 319
	
Mean region size: 76.24113475177305
Incorrectly Classified Instances: 47.990543735224584
Correctly Classified Instances: 52.009456264775416
Weighted Precision: 0.5857608514094237
Weighted AreaUnderROC: 0.7857599969880251
Root mean squared error: 0.38361845702126457
Relative absolute error: 80.3782505910164
Root relative squared error: 88.5928877709344
Weighted TruePositiveRate: 0.5200945626477541
Weighted MatthewsCorrelation: 0.3883616584162143
Weighted FMeasure: 0.534700424701151
Iteration time: 46.0
Weighted AreaUnderPRC: 0.5432109717972834
Mean absolute error: 0.3014184397163115
Coverage of cases: 98.58156028368795
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 107.0
Weighted Recall: 0.5200945626477541
Weighted FalsePositiveRate: 0.16086870428112238
Kappa statistic: 0.36013681174971496
Training time: 45.0
		
Iteration: 3
Labeled set size: 124
Unlabelled set size: 299
	
Mean region size: 75.65011820330969
Incorrectly Classified Instances: 54.846335697399525
Correctly Classified Instances: 45.153664302600475
Weighted Precision: 0.787253980676975
Weighted AreaUnderROC: 0.8108765638303984
Root mean squared error: 0.39158135817930473
Relative absolute error: 81.84922511163627
Root relative squared error: 90.43184102178434
Weighted TruePositiveRate: 0.4515366430260047
Weighted MatthewsCorrelation: 0.36131908864820134
Weighted FMeasure: 0.40487296365617487
Iteration time: 48.0
Weighted AreaUnderPRC: 0.5717139743393905
Mean absolute error: 0.306934594168636
Coverage of cases: 98.3451536643026
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 155.0
Weighted Recall: 0.4515366430260047
Weighted FalsePositiveRate: 0.1835803568103136
Kappa statistic: 0.26837341762714895
Training time: 47.0
		
Iteration: 4
Labeled set size: 144
Unlabelled set size: 279
	
Mean region size: 75.59101654846336
Incorrectly Classified Instances: 67.13947990543736
Correctly Classified Instances: 32.860520094562645
Weighted Precision: 0.5236610844998948
Weighted AreaUnderROC: 0.7724048943150489
Root mean squared error: 0.4105740533969808
Relative absolute error: 85.84187023903318
Root relative squared error: 94.81801610040907
Weighted TruePositiveRate: 0.32860520094562645
Weighted MatthewsCorrelation: 0.17169486408119697
Weighted FMeasure: 0.22909099841987576
Iteration time: 49.0
Weighted AreaUnderPRC: 0.520532203345036
Mean absolute error: 0.32190701339637445
Coverage of cases: 97.39952718676123
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 204.0
Weighted Recall: 0.32860520094562645
Weighted FalsePositiveRate: 0.2246850900332111
Kappa statistic: 0.10333863275039741
Training time: 48.0
		
Iteration: 5
Labeled set size: 164
Unlabelled set size: 259
	
Mean region size: 75.59101654846336
Incorrectly Classified Instances: 66.66666666666667
Correctly Classified Instances: 33.333333333333336
Weighted Precision: 0.5258607511687018
Weighted AreaUnderROC: 0.7827398239707262
Root mean squared error: 0.4054235673689906
Relative absolute error: 84.73863934856826
Root relative squared error: 93.62856230252204
Weighted TruePositiveRate: 0.3333333333333333
Weighted MatthewsCorrelation: 0.1776458085524406
Weighted FMeasure: 0.2347989671338292
Iteration time: 54.0
Weighted AreaUnderPRC: 0.5307442998866347
Mean absolute error: 0.317769897557131
Coverage of cases: 98.3451536643026
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 258.0
Weighted Recall: 0.3333333333333333
Weighted FalsePositiveRate: 0.22310407415593375
Kappa statistic: 0.10961327451463374
Training time: 53.0
		
Iteration: 6
Labeled set size: 184
Unlabelled set size: 239
	
Mean region size: 75.0
Incorrectly Classified Instances: 50.354609929078016
Correctly Classified Instances: 49.645390070921984
Weighted Precision: 0.5307725158239915
Weighted AreaUnderROC: 0.8094704257821707
Root mean squared error: 0.39208413778914536
Relative absolute error: 81.84922511163627
Root relative squared error: 90.54795299901815
Weighted TruePositiveRate: 0.49645390070921985
Weighted MatthewsCorrelation: 0.3751570125979933
Weighted FMeasure: 0.42692768121773683
Iteration time: 59.0
Weighted AreaUnderPRC: 0.56506437393018
Mean absolute error: 0.306934594168636
Coverage of cases: 96.45390070921985
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 317.0
Weighted Recall: 0.49645390070921985
Weighted FalsePositiveRate: 0.16773528261312054
Kappa statistic: 0.33041268142599156
Training time: 58.0
		
Iteration: 7
Labeled set size: 204
Unlabelled set size: 219
	
Mean region size: 75.65011820330969
Incorrectly Classified Instances: 48.226950354609926
Correctly Classified Instances: 51.773049645390074
Weighted Precision: 0.4981193422995676
Weighted AreaUnderROC: 0.7949345205620537
Root mean squared error: 0.3975727511553769
Relative absolute error: 83.05752561071706
Root relative squared error: 91.81549396080678
Weighted TruePositiveRate: 0.5177304964539007
Weighted MatthewsCorrelation: 0.3735544421965137
Weighted FMeasure: 0.42276218232765916
Iteration time: 56.0
Weighted AreaUnderPRC: 0.5175972894356751
Mean absolute error: 0.311465721040189
Coverage of cases: 96.21749408983452
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 373.0
Weighted Recall: 0.5177304964539007
Weighted FalsePositiveRate: 0.15886274415655322
Kappa statistic: 0.3600747513849028
Training time: 56.0
		
Iteration: 8
Labeled set size: 224
Unlabelled set size: 199
	
Mean region size: 75.41371158392435
Incorrectly Classified Instances: 53.427895981087474
Correctly Classified Instances: 46.572104018912526
Weighted Precision: 0.4577736351389987
Weighted AreaUnderROC: 0.7444241417286197
Root mean squared error: 0.4118117472295936
Relative absolute error: 86.05200945626478
Root relative squared error: 95.1038492473824
Weighted TruePositiveRate: 0.4657210401891253
Weighted MatthewsCorrelation: 0.2898612821854223
Weighted FMeasure: 0.33440606634763914
Iteration time: 61.0
Weighted AreaUnderPRC: 0.45341266105918715
Mean absolute error: 0.32269503546099293
Coverage of cases: 91.725768321513
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 434.0
Weighted Recall: 0.4657210401891253
Weighted FalsePositiveRate: 0.17516899935800873
Kappa statistic: 0.2919925346605048
Training time: 60.0
		
Iteration: 9
Labeled set size: 244
Unlabelled set size: 179
	
Mean region size: 75.05910165484633
Incorrectly Classified Instances: 54.37352245862884
Correctly Classified Instances: 45.62647754137116
Weighted Precision: 0.4465025477497964
Weighted AreaUnderROC: 0.73517753574404
Root mean squared error: 0.41475131085450334
Relative absolute error: 86.6298923036512
Root relative squared error: 95.78271238743908
Weighted TruePositiveRate: 0.4562647754137116
Weighted MatthewsCorrelation: 0.27608473146333923
Weighted FMeasure: 0.3242265512857969
Iteration time: 61.0
Weighted AreaUnderPRC: 0.4392191534351806
Mean absolute error: 0.324862096138692
Coverage of cases: 91.25295508274232
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 495.0
Weighted Recall: 0.4562647754137116
Weighted FalsePositiveRate: 0.17745204760815392
Kappa statistic: 0.279941382832275
Training time: 61.0
		
Iteration: 10
Labeled set size: 264
Unlabelled set size: 159
	
Mean region size: 75.177304964539
Incorrectly Classified Instances: 51.06382978723404
Correctly Classified Instances: 48.93617021276596
Weighted Precision: 0.47584425818468373
Weighted AreaUnderROC: 0.7548495288051016
Root mean squared error: 0.4078862076516371
Relative absolute error: 85.15891778303133
Root relative squared error: 94.19728471456331
Weighted TruePositiveRate: 0.48936170212765956
Weighted MatthewsCorrelation: 0.33321077087841594
Weighted FMeasure: 0.3839426706878096
Iteration time: 67.0
Weighted AreaUnderPRC: 0.45480566679798834
Mean absolute error: 0.3193459416863675
Coverage of cases: 95.74468085106383
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 562.0
Weighted Recall: 0.48936170212765956
Weighted FalsePositiveRate: 0.1662677386666244
Kappa statistic: 0.3236058364981011
Training time: 67.0
		
Iteration: 11
Labeled set size: 284
Unlabelled set size: 139
	
Mean region size: 75.29550827423168
Incorrectly Classified Instances: 42.5531914893617
Correctly Classified Instances: 57.4468085106383
Weighted Precision: 0.482909513660488
Weighted AreaUnderROC: 0.7945051310058924
Root mean squared error: 0.3909519743947614
Relative absolute error: 81.58655109009713
Root relative squared error: 90.28649106281249
Weighted TruePositiveRate: 0.574468085106383
Weighted MatthewsCorrelation: 0.42425841386200175
Weighted FMeasure: 0.49222500100392097
Iteration time: 66.0
Weighted AreaUnderPRC: 0.5232765721147966
Mean absolute error: 0.30594956658786426
Coverage of cases: 95.50827423167848
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 628.0
Weighted Recall: 0.574468085106383
Weighted FalsePositiveRate: 0.1385176784381018
Kappa statistic: 0.43557354445581115
Training time: 65.0
		
Iteration: 12
Labeled set size: 304
Unlabelled set size: 119
	
Mean region size: 75.35460992907801
Incorrectly Classified Instances: 34.751773049645394
Correctly Classified Instances: 65.2482269503546
Weighted Precision: 0.5124617449534927
Weighted AreaUnderROC: 0.8305301835012144
Root mean squared error: 0.38078348163973247
Relative absolute error: 79.48515891778298
Root relative squared error: 87.93817825106498
Weighted TruePositiveRate: 0.6524822695035462
Weighted MatthewsCorrelation: 0.5052425850205924
Weighted FMeasure: 0.5670634507971501
Iteration time: 70.0
Weighted AreaUnderPRC: 0.591973866700302
Mean absolute error: 0.2980693459416862
Coverage of cases: 94.79905437352245
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 698.0
Weighted Recall: 0.6524822695035462
Weighted FalsePositiveRate: 0.11478327040635485
Kappa statistic: 0.5378698356781344
Training time: 69.0
		
Iteration: 13
Labeled set size: 324
Unlabelled set size: 99
	
Mean region size: 75.0
Incorrectly Classified Instances: 32.6241134751773
Correctly Classified Instances: 67.37588652482269
Weighted Precision: 0.5136614370656923
Weighted AreaUnderROC: 0.8278610210642819
Root mean squared error: 0.3874509043862098
Relative absolute error: 80.79852902547947
Root relative squared error: 89.47795357805688
Weighted TruePositiveRate: 0.6737588652482269
Weighted MatthewsCorrelation: 0.5236353330640002
Weighted FMeasure: 0.5812694477030256
Iteration time: 68.0
Weighted AreaUnderPRC: 0.589658630041095
Mean absolute error: 0.302994483845548
Coverage of cases: 92.43498817966903
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 766.0
Weighted Recall: 0.6737588652482269
Weighted FalsePositiveRate: 0.10835182403671521
Kappa statistic: 0.565815017293317
Training time: 68.0
		
Iteration: 14
Labeled set size: 344
Unlabelled set size: 79
	
Mean region size: 75.0
Incorrectly Classified Instances: 33.80614657210402
Correctly Classified Instances: 66.19385342789599
Weighted Precision: 0.5021942501481489
Weighted AreaUnderROC: 0.8219077173678645
Root mean squared error: 0.3888044487860819
Relative absolute error: 81.06120304701874
Root relative squared error: 89.79054126750738
Weighted TruePositiveRate: 0.6619385342789598
Weighted MatthewsCorrelation: 0.5077615832262155
Weighted FMeasure: 0.5700743789458075
Iteration time: 70.0
Weighted AreaUnderPRC: 0.5766155312222916
Mean absolute error: 0.3039795114263203
Coverage of cases: 92.43498817966903
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 836.0
Weighted Recall: 0.6619385342789598
Weighted FalsePositiveRate: 0.11257393733333107
Kappa statistic: 0.5499732166770823
Training time: 70.0
		
Iteration: 15
Labeled set size: 364
Unlabelled set size: 59
	
Mean region size: 75.177304964539
Incorrectly Classified Instances: 34.04255319148936
Correctly Classified Instances: 65.95744680851064
Weighted Precision: 0.5000194259794675
Weighted AreaUnderROC: 0.8362018016670925
Root mean squared error: 0.3752243003125573
Relative absolute error: 78.32939322301014
Root relative squared error: 86.65434031677758
Weighted TruePositiveRate: 0.6595744680851063
Weighted MatthewsCorrelation: 0.504650368588446
Weighted FMeasure: 0.5678797590171234
Iteration time: 64.0
Weighted AreaUnderPRC: 0.5941903422197431
Mean absolute error: 0.29373522458628804
Coverage of cases: 95.27186761229315
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 900.0
Weighted Recall: 0.6595744680851063
Weighted FalsePositiveRate: 0.11339458451527382
Kappa statistic: 0.5468160614244582
Training time: 63.0
		
Iteration: 16
Labeled set size: 384
Unlabelled set size: 39
	
Mean region size: 76.12293144208037
Incorrectly Classified Instances: 32.38770685579196
Correctly Classified Instances: 67.61229314420804
Weighted Precision: 0.7741831208929107
Weighted AreaUnderROC: 0.839602666894713
Root mean squared error: 0.37069036609781236
Relative absolute error: 77.69897557131584
Root relative squared error: 85.60727306102916
Weighted TruePositiveRate: 0.6761229314420804
Weighted MatthewsCorrelation: 0.547897557903446
Weighted FMeasure: 0.5881204190880127
Iteration time: 63.0
Weighted AreaUnderPRC: 0.6012495197763899
Mean absolute error: 0.29137115839243444
Coverage of cases: 97.39952718676123
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 963.0
Weighted Recall: 0.6761229314420804
Weighted FalsePositiveRate: 0.10770697355565438
Kappa statistic: 0.5689163957718085
Training time: 62.0
		
Iteration: 17
Labeled set size: 404
Unlabelled set size: 19
	
Mean region size: 75.70921985815603
Incorrectly Classified Instances: 29.78723404255319
Correctly Classified Instances: 70.2127659574468
Weighted Precision: 0.680763108848242
Weighted AreaUnderROC: 0.834792995046592
Root mean squared error: 0.36493249912882425
Relative absolute error: 76.3856054636196
Root relative squared error: 84.27755064322783
Weighted TruePositiveRate: 0.7021276595744681
Weighted MatthewsCorrelation: 0.592419454785614
Weighted FMeasure: 0.6855949352565364
Iteration time: 66.0
Weighted AreaUnderPRC: 0.6073304615867254
Mean absolute error: 0.2864460204885735
Coverage of cases: 98.3451536643026
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 1029.0
Weighted Recall: 0.7021276595744681
Weighted FalsePositiveRate: 0.09972232154543403
Kappa statistic: 0.6030683299199404
Training time: 65.0
		
Time end:Wed Nov 01 14.44.26 EET 2017