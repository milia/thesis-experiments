Wed Nov 01 14.40.38 EET 2017
Dataset: vehicle
Test set size: 423
Initial Labelled set size: 84
Initial Unlabelled set size: 339
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Wed Nov 01 14.40.38 EET 2017
		
Iteration: 1
Labeled set size: 84
Unlabelled set size: 339
	
Mean region size: 25.0
Incorrectly Classified Instances: 41.843971631205676
Correctly Classified Instances: 58.156028368794324
Weighted Precision: 0.5697090154706326
Weighted AreaUnderROC: 0.7201966604230061
Root mean squared error: 0.44732232371936864
Relative absolute error: 57.80141843971625
Root relative squared error: 103.30466560556258
Weighted TruePositiveRate: 0.5815602836879432
Weighted MatthewsCorrelation: 0.434697668444269
Weighted FMeasure: 0.5618083772270441
Iteration time: 20.0
Weighted AreaUnderPRC: 0.4628630798339099
Mean absolute error: 0.21675531914893595
Coverage of cases: 58.156028368794324
Instances selection time: 20.0
Test time: 14.0
Accumulative iteration time: 20.0
Weighted Recall: 0.5815602836879432
Weighted FalsePositiveRate: 0.14116696284193098
Kappa statistic: 0.4414362662449082
Training time: 0.0
		
Iteration: 2
Labeled set size: 104
Unlabelled set size: 319
	
Mean region size: 25.0
Incorrectly Classified Instances: 41.371158392434985
Correctly Classified Instances: 58.628841607565015
Weighted Precision: 0.5915690083076699
Weighted AreaUnderROC: 0.7235719766059937
Root mean squared error: 0.446600120891204
Relative absolute error: 56.8222280594224
Root relative squared error: 103.13788000666243
Weighted TruePositiveRate: 0.5862884160756501
Weighted MatthewsCorrelation: 0.44954589121165794
Weighted FMeasure: 0.5875509077452221
Iteration time: 23.0
Weighted AreaUnderPRC: 0.479924920502476
Mean absolute error: 0.213083355222834
Coverage of cases: 58.628841607565015
Instances selection time: 22.0
Test time: 17.0
Accumulative iteration time: 43.0
Weighted Recall: 0.5862884160756501
Weighted FalsePositiveRate: 0.1391444628636626
Kappa statistic: 0.44811004249608577
Training time: 1.0
		
Iteration: 3
Labeled set size: 124
Unlabelled set size: 299
	
Mean region size: 25.0
Incorrectly Classified Instances: 40.8983451536643
Correctly Classified Instances: 59.1016548463357
Weighted Precision: 0.6097779350785092
Weighted AreaUnderROC: 0.7268995103055127
Root mean squared error: 0.4452914380569331
Relative absolute error: 55.952029156816394
Root relative squared error: 102.83565265200237
Weighted TruePositiveRate: 0.5910165484633569
Weighted MatthewsCorrelation: 0.46237936530671603
Weighted FMeasure: 0.5935765683028585
Iteration time: 25.0
Weighted AreaUnderPRC: 0.48718327353469415
Mean absolute error: 0.20982010933806147
Coverage of cases: 59.1016548463357
Instances selection time: 25.0
Test time: 19.0
Accumulative iteration time: 68.0
Weighted Recall: 0.5910165484633569
Weighted FalsePositiveRate: 0.13721752785233157
Kappa statistic: 0.4544905626621343
Training time: 0.0
		
Iteration: 4
Labeled set size: 144
Unlabelled set size: 279
	
Mean region size: 25.0
Incorrectly Classified Instances: 42.5531914893617
Correctly Classified Instances: 57.4468085106383
Weighted Precision: 0.6121500547073437
Weighted AreaUnderROC: 0.7159180971644554
Root mean squared error: 0.4551400745375778
Relative absolute error: 57.90684301322628
Root relative squared error: 105.11009782130277
Weighted TruePositiveRate: 0.574468085106383
Weighted MatthewsCorrelation: 0.4495865518645976
Weighted FMeasure: 0.5738320626000228
Iteration time: 26.0
Weighted AreaUnderPRC: 0.47552224024765744
Mean absolute error: 0.21715066129959854
Coverage of cases: 57.4468085106383
Instances selection time: 25.0
Test time: 20.0
Accumulative iteration time: 94.0
Weighted Recall: 0.574468085106383
Weighted FalsePositiveRate: 0.1426318907774723
Kappa statistic: 0.4324263883712263
Training time: 1.0
		
Iteration: 5
Labeled set size: 164
Unlabelled set size: 259
	
Mean region size: 25.0
Incorrectly Classified Instances: 45.153664302600475
Correctly Classified Instances: 54.846335697399525
Weighted Precision: 0.6244210648124415
Weighted AreaUnderROC: 0.698529242135704
Root mean squared error: 0.4695733998040237
Relative absolute error: 61.152388457352934
Root relative squared error: 108.44333151245634
Weighted TruePositiveRate: 0.5484633569739953
Weighted MatthewsCorrelation: 0.4324824805210105
Weighted FMeasure: 0.5493801410406126
Iteration time: 26.0
Weighted AreaUnderPRC: 0.4659207768829333
Mean absolute error: 0.2293214567150735
Coverage of cases: 54.846335697399525
Instances selection time: 25.0
Test time: 23.0
Accumulative iteration time: 120.0
Weighted Recall: 0.5484633569739953
Weighted FalsePositiveRate: 0.1514048727025873
Kappa statistic: 0.3974673537724944
Training time: 1.0
		
Iteration: 6
Labeled set size: 184
Unlabelled set size: 239
	
Mean region size: 25.0
Incorrectly Classified Instances: 42.5531914893617
Correctly Classified Instances: 57.4468085106383
Weighted Precision: 0.6503401087341629
Weighted AreaUnderROC: 0.7162155516510971
Root mean squared error: 0.45642513320170874
Relative absolute error: 57.65806548966368
Root relative squared error: 105.40686940756694
Weighted TruePositiveRate: 0.574468085106383
Weighted MatthewsCorrelation: 0.4680199576309215
Weighted FMeasure: 0.573568658428717
Iteration time: 26.0
Weighted AreaUnderPRC: 0.4895033977259865
Mean absolute error: 0.2162177455862388
Coverage of cases: 57.4468085106383
Instances selection time: 25.0
Test time: 24.0
Accumulative iteration time: 146.0
Weighted Recall: 0.574468085106383
Weighted FalsePositiveRate: 0.14203698180418856
Kappa statistic: 0.43271816955870634
Training time: 1.0
		
Iteration: 7
Labeled set size: 204
Unlabelled set size: 219
	
Mean region size: 25.0
Incorrectly Classified Instances: 39.2434988179669
Correctly Classified Instances: 60.7565011820331
Weighted Precision: 0.6668884194509582
Weighted AreaUnderROC: 0.7385282350136644
Root mean squared error: 0.4387635154134062
Relative absolute error: 53.24149845426465
Root relative squared error: 101.32809349380663
Weighted TruePositiveRate: 0.607565011820331
Weighted MatthewsCorrelation: 0.5031932666251382
Weighted FMeasure: 0.6012257337078741
Iteration time: 27.0
Weighted AreaUnderPRC: 0.5156303726552863
Mean absolute error: 0.19965561920349245
Coverage of cases: 60.7565011820331
Instances selection time: 27.0
Test time: 26.0
Accumulative iteration time: 173.0
Weighted Recall: 0.607565011820331
Weighted FalsePositiveRate: 0.13050854179300223
Kappa statistic: 0.4775368681081564
Training time: 0.0
		
Iteration: 8
Labeled set size: 224
Unlabelled set size: 199
	
Mean region size: 25.0
Incorrectly Classified Instances: 38.297872340425535
Correctly Classified Instances: 61.702127659574465
Weighted Precision: 0.6697066810208228
Weighted AreaUnderROC: 0.7450719856601697
Root mean squared error: 0.4338059580973517
Relative absolute error: 51.92235908921257
Root relative squared error: 100.1831946734278
Weighted TruePositiveRate: 0.6170212765957447
Weighted MatthewsCorrelation: 0.5120874829577522
Weighted FMeasure: 0.6056886395414361
Iteration time: 29.0
Weighted AreaUnderPRC: 0.5187600313990873
Mean absolute error: 0.19470884658454715
Coverage of cases: 61.702127659574465
Instances selection time: 29.0
Test time: 28.0
Accumulative iteration time: 202.0
Weighted Recall: 0.6170212765957447
Weighted FalsePositiveRate: 0.12687730527540522
Kappa statistic: 0.4904902820943685
Training time: 0.0
		
Iteration: 9
Labeled set size: 244
Unlabelled set size: 179
	
Mean region size: 25.0
Incorrectly Classified Instances: 38.77068557919622
Correctly Classified Instances: 61.22931442080378
Weighted Precision: 0.6645584864012558
Weighted AreaUnderROC: 0.7420196430967861
Root mean squared error: 0.436778590385801
Relative absolute error: 52.47337248023607
Root relative squared error: 100.869694694203
Weighted TruePositiveRate: 0.6122931442080378
Weighted MatthewsCorrelation: 0.505347590828261
Weighted FMeasure: 0.6007667526267101
Iteration time: 24.0
Weighted AreaUnderPRC: 0.5132548480279429
Mean absolute error: 0.1967751468008853
Coverage of cases: 61.22931442080378
Instances selection time: 23.0
Test time: 30.0
Accumulative iteration time: 226.0
Weighted Recall: 0.6122931442080378
Weighted FalsePositiveRate: 0.1282538580144655
Kappa statistic: 0.4843188998327448
Training time: 1.0
		
Iteration: 10
Labeled set size: 264
Unlabelled set size: 159
	
Mean region size: 25.0
Incorrectly Classified Instances: 37.825059101654844
Correctly Classified Instances: 62.174940898345156
Weighted Precision: 0.6583169243570538
Weighted AreaUnderROC: 0.7483176596865171
Root mean squared error: 0.431676150223489
Relative absolute error: 51.17321195441237
Root relative squared error: 99.69133661370908
Weighted TruePositiveRate: 0.6217494089834515
Weighted MatthewsCorrelation: 0.5108715090653201
Weighted FMeasure: 0.6078070711109805
Iteration time: 23.0
Weighted AreaUnderPRC: 0.5176395543286919
Mean absolute error: 0.1918995448290464
Coverage of cases: 62.174940898345156
Instances selection time: 22.0
Test time: 32.0
Accumulative iteration time: 249.0
Weighted Recall: 0.6217494089834515
Weighted FalsePositiveRate: 0.12511408961041726
Kappa statistic: 0.4968590863472475
Training time: 1.0
		
Iteration: 11
Labeled set size: 284
Unlabelled set size: 139
	
Mean region size: 25.0
Incorrectly Classified Instances: 34.04255319148936
Correctly Classified Instances: 65.95744680851064
Weighted Precision: 0.6770437168413996
Weighted AreaUnderROC: 0.773334719986585
Root mean squared error: 0.40973756081798846
Relative absolute error: 46.14854215918024
Root relative squared error: 94.62483641414653
Weighted TruePositiveRate: 0.6595744680851063
Weighted MatthewsCorrelation: 0.5522345209948258
Weighted FMeasure: 0.6406583122144097
Iteration time: 21.0
Weighted AreaUnderPRC: 0.5541540765131123
Mean absolute error: 0.17305703309692588
Coverage of cases: 65.95744680851064
Instances selection time: 20.0
Test time: 38.0
Accumulative iteration time: 270.0
Weighted Recall: 0.6595744680851063
Weighted FalsePositiveRate: 0.1129050281119365
Kappa statistic: 0.5468598890062638
Training time: 1.0
		
Iteration: 12
Labeled set size: 304
Unlabelled set size: 119
	
Mean region size: 25.0
Incorrectly Classified Instances: 34.2789598108747
Correctly Classified Instances: 65.72104018912529
Weighted Precision: 0.6718329660038643
Weighted AreaUnderROC: 0.7717272936770347
Root mean squared error: 0.411339904207073
Relative absolute error: 46.4104059848741
Root relative squared error: 94.9948817689554
Weighted TruePositiveRate: 0.6572104018912529
Weighted MatthewsCorrelation: 0.5476424122765943
Weighted FMeasure: 0.6378386112851757
Iteration time: 20.0
Weighted AreaUnderPRC: 0.5503534500368518
Mean absolute error: 0.1740390224432779
Coverage of cases: 65.72104018912529
Instances selection time: 19.0
Test time: 35.0
Accumulative iteration time: 290.0
Weighted Recall: 0.6572104018912529
Weighted FalsePositiveRate: 0.11375581453718331
Kappa statistic: 0.5436825307075952
Training time: 1.0
		
Iteration: 13
Labeled set size: 324
Unlabelled set size: 99
	
Mean region size: 25.0
Incorrectly Classified Instances: 34.2789598108747
Correctly Classified Instances: 65.72104018912529
Weighted Precision: 0.670748660417976
Weighted AreaUnderROC: 0.7716781468656799
Root mean squared error: 0.4115003083419131
Relative absolute error: 46.36741048261549
Root relative squared error: 95.03192551712702
Weighted TruePositiveRate: 0.6572104018912529
Weighted MatthewsCorrelation: 0.5470619364368594
Weighted FMeasure: 0.6377428912999246
Iteration time: 17.0
Weighted AreaUnderPRC: 0.5504965020729603
Mean absolute error: 0.17387778930980807
Coverage of cases: 65.72104018912529
Instances selection time: 17.0
Test time: 37.0
Accumulative iteration time: 307.0
Weighted Recall: 0.6572104018912529
Weighted FalsePositiveRate: 0.11385410815989307
Kappa statistic: 0.5436383928571429
Training time: 0.0
		
Iteration: 14
Labeled set size: 344
Unlabelled set size: 79
	
Mean region size: 25.0
Incorrectly Classified Instances: 34.2789598108747
Correctly Classified Instances: 65.72104018912529
Weighted Precision: 0.6623546581753268
Weighted AreaUnderROC: 0.771534644378014
Root mean squared error: 0.4116424613539518
Relative absolute error: 46.329356992110824
Root relative squared error: 95.06475434903369
Weighted TruePositiveRate: 0.6572104018912529
Weighted MatthewsCorrelation: 0.5437810966594437
Weighted FMeasure: 0.6393359258266176
Iteration time: 15.0
Weighted AreaUnderPRC: 0.5516423094735861
Mean absolute error: 0.1737350887204156
Coverage of cases: 65.72104018912529
Instances selection time: 14.0
Test time: 43.0
Accumulative iteration time: 322.0
Weighted Recall: 0.6572104018912529
Weighted FalsePositiveRate: 0.1141411131352248
Kappa statistic: 0.5434753483386924
Training time: 1.0
		
Iteration: 15
Labeled set size: 364
Unlabelled set size: 59
	
Mean region size: 25.0
Incorrectly Classified Instances: 34.04255319148936
Correctly Classified Instances: 65.95744680851064
Weighted Precision: 0.6640352460175737
Weighted AreaUnderROC: 0.7730025061462971
Root mean squared error: 0.4103471455286872
Relative absolute error: 45.983657107616644
Root relative squared error: 94.76561397287284
Weighted TruePositiveRate: 0.6595744680851063
Weighted MatthewsCorrelation: 0.5470587781336483
Weighted FMeasure: 0.6491692931383299
Iteration time: 12.0
Weighted AreaUnderPRC: 0.5554518796958197
Mean absolute error: 0.17243871415356243
Coverage of cases: 65.95744680851064
Instances selection time: 11.0
Test time: 40.0
Accumulative iteration time: 334.0
Weighted Recall: 0.6595744680851063
Weighted FalsePositiveRate: 0.11356945579251214
Kappa statistic: 0.5464887724105069
Training time: 1.0
		
Iteration: 16
Labeled set size: 384
Unlabelled set size: 39
	
Mean region size: 25.0
Incorrectly Classified Instances: 34.751773049645394
Correctly Classified Instances: 65.2482269503546
Weighted Precision: 0.6560531053973118
Weighted AreaUnderROC: 0.7681802272176467
Root mean squared error: 0.4147137138669091
Relative absolute error: 46.88893763252192
Root relative squared error: 95.77402973507577
Weighted TruePositiveRate: 0.6524822695035462
Weighted MatthewsCorrelation: 0.5373731360355114
Weighted FMeasure: 0.6470849149146001
Iteration time: 9.0
Weighted AreaUnderPRC: 0.5511294151019727
Mean absolute error: 0.1758335161219572
Coverage of cases: 65.2482269503546
Instances selection time: 8.0
Test time: 42.0
Accumulative iteration time: 343.0
Weighted Recall: 0.6524822695035462
Weighted FalsePositiveRate: 0.11612181506825256
Kappa statistic: 0.5369130515732639
Training time: 1.0
		
Iteration: 17
Labeled set size: 404
Unlabelled set size: 19
	
Mean region size: 25.0
Incorrectly Classified Instances: 34.751773049645394
Correctly Classified Instances: 65.2482269503546
Weighted Precision: 0.6565746510327397
Weighted AreaUnderROC: 0.7680255930547276
Root mean squared error: 0.41481727167046406
Relative absolute error: 46.861818013257086
Root relative squared error: 95.79794538537944
Weighted TruePositiveRate: 0.6524822695035462
Weighted MatthewsCorrelation: 0.5375229154090471
Weighted FMeasure: 0.6517682954833991
Iteration time: 5.0
Weighted AreaUnderPRC: 0.5506377741950685
Mean absolute error: 0.1757318175497141
Coverage of cases: 65.2482269503546
Instances selection time: 5.0
Test time: 43.0
Accumulative iteration time: 348.0
Weighted Recall: 0.6524822695035462
Weighted FalsePositiveRate: 0.11643108339409078
Kappa statistic: 0.5367543526361667
Training time: 0.0
		
Time end:Wed Nov 01 14.40.40 EET 2017