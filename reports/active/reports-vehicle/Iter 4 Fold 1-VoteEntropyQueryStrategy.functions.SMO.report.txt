Wed Nov 01 14.44.31 EET 2017
Dataset: vehicle
Test set size: 423
Initial Labelled set size: 84
Initial Unlabelled set size: 339
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Wed Nov 01 14.44.31 EET 2017
		
Iteration: 1
Labeled set size: 84
Unlabelled set size: 339
	
Mean region size: 76.00472813238771
Incorrectly Classified Instances: 43.97163120567376
Correctly Classified Instances: 56.02836879432624
Weighted Precision: 0.5506795848403413
Weighted AreaUnderROC: 0.7668404582872281
Root mean squared error: 0.3875356394127866
Relative absolute error: 81.11373785132638
Root relative squared error: 89.49752229421844
Weighted TruePositiveRate: 0.5602836879432624
Weighted MatthewsCorrelation: 0.4080214756141045
Weighted FMeasure: 0.5536272619319976
Iteration time: 50.0
Weighted AreaUnderPRC: 0.495793595733729
Mean absolute error: 0.3041765169424739
Coverage of cases: 95.74468085106383
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 50.0
Weighted Recall: 0.5602836879432624
Weighted FalsePositiveRate: 0.14755761934051104
Kappa statistic: 0.4139572300060334
Training time: 48.0
		
Iteration: 2
Labeled set size: 104
Unlabelled set size: 319
	
Mean region size: 75.70921985815603
Incorrectly Classified Instances: 41.607565011820334
Correctly Classified Instances: 58.392434988179666
Weighted Precision: 0.5695697056287086
Weighted AreaUnderROC: 0.7967238653751821
Root mean squared error: 0.384217126251641
Relative absolute error: 80.37825059101638
Root relative squared error: 88.73114450745975
Weighted TruePositiveRate: 0.5839243498817966
Weighted MatthewsCorrelation: 0.43922085914492953
Weighted FMeasure: 0.5621445510907749
Iteration time: 50.0
Weighted AreaUnderPRC: 0.5132046546327084
Mean absolute error: 0.30141843971631144
Coverage of cases: 96.45390070921985
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 100.0
Weighted Recall: 0.5839243498817966
Weighted FalsePositiveRate: 0.14078782902126208
Kappa statistic: 0.44421052631578944
Training time: 49.0
		
Iteration: 3
Labeled set size: 124
Unlabelled set size: 299
	
Mean region size: 75.70921985815603
Incorrectly Classified Instances: 43.498817966903076
Correctly Classified Instances: 56.501182033096924
Weighted Precision: 0.563550343247998
Weighted AreaUnderROC: 0.7878836092746105
Root mean squared error: 0.3891842849731747
Relative absolute error: 81.42894667717341
Root relative squared error: 89.87826067745378
Weighted TruePositiveRate: 0.5650118203309693
Weighted MatthewsCorrelation: 0.4206653600814344
Weighted FMeasure: 0.5379269737570751
Iteration time: 49.0
Weighted AreaUnderPRC: 0.5003839791507525
Mean absolute error: 0.3053585500394003
Coverage of cases: 95.74468085106383
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 149.0
Weighted Recall: 0.5650118203309693
Weighted FalsePositiveRate: 0.14785754976222726
Kappa statistic: 0.4185262937700314
Training time: 48.0
		
Iteration: 4
Labeled set size: 144
Unlabelled set size: 279
	
Mean region size: 75.65011820330969
Incorrectly Classified Instances: 47.5177304964539
Correctly Classified Instances: 52.4822695035461
Weighted Precision: 0.5377144242495844
Weighted AreaUnderROC: 0.7665182866603258
Root mean squared error: 0.3996320871700315
Relative absolute error: 83.58287365379549
Root relative squared error: 92.29107724177187
Weighted TruePositiveRate: 0.524822695035461
Weighted MatthewsCorrelation: 0.3720547079990494
Weighted FMeasure: 0.47258844019323537
Iteration time: 51.0
Weighted AreaUnderPRC: 0.4735553947750377
Mean absolute error: 0.3134357762017331
Coverage of cases: 95.0354609929078
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 200.0
Weighted Recall: 0.524822695035461
Weighted FalsePositiveRate: 0.16178986233506756
Kappa statistic: 0.3646607832734283
Training time: 50.0
		
Iteration: 5
Labeled set size: 164
Unlabelled set size: 259
	
Mean region size: 75.76832151300236
Incorrectly Classified Instances: 49.881796690307326
Correctly Classified Instances: 50.118203309692674
Weighted Precision: 0.5199777578467344
Weighted AreaUnderROC: 0.7527647199963025
Root mean squared error: 0.4036785802001689
Relative absolute error: 84.52850013133688
Root relative squared error: 93.22557477786138
Weighted TruePositiveRate: 0.5011820330969267
Weighted MatthewsCorrelation: 0.34563877065232784
Weighted FMeasure: 0.44825412303110623
Iteration time: 51.0
Weighted AreaUnderPRC: 0.45055783233589436
Mean absolute error: 0.31698187549251333
Coverage of cases: 95.27186761229315
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 251.0
Weighted Recall: 0.5011820330969267
Weighted FalsePositiveRate: 0.1701084605128325
Kappa statistic: 0.33262298391619366
Training time: 50.0
		
Iteration: 6
Labeled set size: 184
Unlabelled set size: 239
	
Mean region size: 75.29550827423168
Incorrectly Classified Instances: 51.536643026004725
Correctly Classified Instances: 48.463356973995275
Weighted Precision: 0.4010424366948155
Weighted AreaUnderROC: 0.7683740671913263
Root mean squared error: 0.4071610760423586
Relative absolute error: 85.15891778303127
Root relative squared error: 94.02982274263739
Weighted TruePositiveRate: 0.4846335697399527
Weighted MatthewsCorrelation: 0.3104207837748793
Weighted FMeasure: 0.4074449021762722
Iteration time: 52.0
Weighted AreaUnderPRC: 0.48640822891118657
Mean absolute error: 0.31934594168636726
Coverage of cases: 92.43498817966903
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 303.0
Weighted Recall: 0.4846335697399527
Weighted FalsePositiveRate: 0.1754569073056362
Kappa statistic: 0.31051344743276277
Training time: 51.0
		
Iteration: 7
Labeled set size: 204
Unlabelled set size: 219
	
Mean region size: 75.177304964539
Incorrectly Classified Instances: 48.69976359338062
Correctly Classified Instances: 51.30023640661938
Weighted Precision: 0.44353862730051474
Weighted AreaUnderROC: 0.7803866758720079
Root mean squared error: 0.40893134993839386
Relative absolute error: 85.52666141318632
Root relative squared error: 94.43864998680351
Weighted TruePositiveRate: 0.5130023640661938
Weighted MatthewsCorrelation: 0.3491381299714121
Weighted FMeasure: 0.4341014911749618
Iteration time: 53.0
Weighted AreaUnderPRC: 0.5023629080539228
Mean absolute error: 0.3207249802994487
Coverage of cases: 92.67139479905437
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 356.0
Weighted Recall: 0.5130023640661938
Weighted FalsePositiveRate: 0.16505487763865095
Kappa statistic: 0.34871518902192916
Training time: 52.0
		
Iteration: 8
Labeled set size: 224
Unlabelled set size: 199
	
Mean region size: 75.23640661938535
Incorrectly Classified Instances: 47.990543735224584
Correctly Classified Instances: 52.009456264775416
Weighted Precision: 0.46362506561549555
Weighted AreaUnderROC: 0.7943004513468578
Root mean squared error: 0.4108138980596691
Relative absolute error: 85.94693984764916
Root relative squared error: 94.87340585263578
Weighted TruePositiveRate: 0.5200945626477541
Weighted MatthewsCorrelation: 0.36197974526967874
Weighted FMeasure: 0.4288889131132054
Iteration time: 54.0
Weighted AreaUnderPRC: 0.5198729333975903
Mean absolute error: 0.32230102442868436
Coverage of cases: 92.90780141843972
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 410.0
Weighted Recall: 0.5200945626477541
Weighted FalsePositiveRate: 0.16183777542390693
Kappa statistic: 0.3581999192788914
Training time: 53.0
		
Iteration: 9
Labeled set size: 244
Unlabelled set size: 179
	
Mean region size: 75.23640661938535
Incorrectly Classified Instances: 49.645390070921984
Correctly Classified Instances: 50.354609929078016
Weighted Precision: 0.4639541864166057
Weighted AreaUnderROC: 0.7921276209528741
Root mean squared error: 0.41292647557607376
Relative absolute error: 86.41975308641986
Root relative squared error: 95.36128473174784
Weighted TruePositiveRate: 0.5035460992907801
Weighted MatthewsCorrelation: 0.3455325559217506
Weighted FMeasure: 0.4093949608144051
Iteration time: 58.0
Weighted AreaUnderPRC: 0.5170026975063168
Mean absolute error: 0.32407407407407446
Coverage of cases: 92.90780141843972
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 468.0
Weighted Recall: 0.5035460992907801
Weighted FalsePositiveRate: 0.16724909443484726
Kappa statistic: 0.33603910664633596
Training time: 57.0
		
Iteration: 10
Labeled set size: 264
Unlabelled set size: 159
	
Mean region size: 75.05910165484633
Incorrectly Classified Instances: 47.990543735224584
Correctly Classified Instances: 52.009456264775416
Weighted Precision: 0.49743323726179994
Weighted AreaUnderROC: 0.8064464804677177
Root mean squared error: 0.4074835160660005
Relative absolute error: 85.21145258733911
Root relative squared error: 94.10428707641623
Weighted TruePositiveRate: 0.5200945626477541
Weighted MatthewsCorrelation: 0.375575653601316
Weighted FMeasure: 0.42660028695504687
Iteration time: 61.0
Weighted AreaUnderPRC: 0.5527123332136235
Mean absolute error: 0.31954294720252163
Coverage of cases: 91.96217494089835
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 529.0
Weighted Recall: 0.5200945626477541
Weighted FalsePositiveRate: 0.16150120498861972
Kappa statistic: 0.35831502488454464
Training time: 61.0
		
Iteration: 11
Labeled set size: 284
Unlabelled set size: 139
	
Mean region size: 75.0
Incorrectly Classified Instances: 35.69739952718676
Correctly Classified Instances: 64.30260047281324
Weighted Precision: 0.5102934971672516
Weighted AreaUnderROC: 0.8249858753694804
Root mean squared error: 0.3903636339945977
Relative absolute error: 81.42894667717366
Root relative squared error: 90.15061966744862
Weighted TruePositiveRate: 0.6430260047281324
Weighted MatthewsCorrelation: 0.4964499875199464
Weighted FMeasure: 0.5594445543173873
Iteration time: 66.0
Weighted AreaUnderPRC: 0.59326207796519
Mean absolute error: 0.3053585500394012
Coverage of cases: 90.78014184397163
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 595.0
Weighted Recall: 0.6430260047281324
Weighted FalsePositiveRate: 0.11963132688927228
Kappa statistic: 0.5242234636871509
Training time: 66.0
		
Iteration: 12
Labeled set size: 304
Unlabelled set size: 119
	
Mean region size: 75.11820330969267
Incorrectly Classified Instances: 34.515366430260045
Correctly Classified Instances: 65.48463356973996
Weighted Precision: 0.5142992598737932
Weighted AreaUnderROC: 0.8267191294235067
Root mean squared error: 0.3901532974127044
Relative absolute error: 81.3764118728658
Root relative squared error: 90.102044514578
Weighted TruePositiveRate: 0.6548463356973995
Weighted MatthewsCorrelation: 0.5084069090029613
Weighted FMeasure: 0.5690928924029056
Iteration time: 66.0
Weighted AreaUnderPRC: 0.5977381393167527
Mean absolute error: 0.3051615445232468
Coverage of cases: 90.78014184397163
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 661.0
Weighted Recall: 0.6548463356973995
Weighted FalsePositiveRate: 0.11538243352198066
Kappa statistic: 0.5402139682397873
Training time: 65.0
		
Iteration: 13
Labeled set size: 324
Unlabelled set size: 99
	
Mean region size: 75.05910165484633
Incorrectly Classified Instances: 33.096926713947994
Correctly Classified Instances: 66.903073286052
Weighted Precision: 0.5149436124348755
Weighted AreaUnderROC: 0.8287314418634153
Root mean squared error: 0.3884242411611528
Relative absolute error: 81.00866824271087
Root relative squared error: 89.70273607766708
Weighted TruePositiveRate: 0.6690307328605201
Weighted MatthewsCorrelation: 0.5204449633522636
Weighted FMeasure: 0.5784062508781718
Iteration time: 67.0
Weighted AreaUnderPRC: 0.6006463630243514
Mean absolute error: 0.30378250591016576
Coverage of cases: 90.0709219858156
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 728.0
Weighted Recall: 0.6690307328605201
Weighted FalsePositiveRate: 0.11005003771458052
Kappa statistic: 0.5595126522961574
Training time: 66.0
		
Iteration: 14
Labeled set size: 344
Unlabelled set size: 79
	
Mean region size: 75.177304964539
Incorrectly Classified Instances: 33.096926713947994
Correctly Classified Instances: 66.903073286052
Weighted Precision: 0.5141301892309914
Weighted AreaUnderROC: 0.8332087442805832
Root mean squared error: 0.3808697000222797
Relative absolute error: 79.48515891778298
Root relative squared error: 87.95808953361409
Weighted TruePositiveRate: 0.6690307328605201
Weighted MatthewsCorrelation: 0.5200739649343573
Weighted FMeasure: 0.5780462939056571
Iteration time: 73.0
Weighted AreaUnderPRC: 0.5979453947736783
Mean absolute error: 0.2980693459416862
Coverage of cases: 91.48936170212765
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 801.0
Weighted Recall: 0.6690307328605201
Weighted FalsePositiveRate: 0.10999143881428655
Kappa statistic: 0.5595323098893252
Training time: 73.0
		
Iteration: 15
Labeled set size: 364
Unlabelled set size: 59
	
Mean region size: 75.41371158392435
Incorrectly Classified Instances: 32.860520094562645
Correctly Classified Instances: 67.13947990543736
Weighted Precision: 0.5161523950465478
Weighted AreaUnderROC: 0.843592436850634
Root mean squared error: 0.37338215936486185
Relative absolute error: 78.01418439716302
Root relative squared error: 86.2289160879627
Weighted TruePositiveRate: 0.6713947990543735
Weighted MatthewsCorrelation: 0.5230669171137432
Weighted FMeasure: 0.5801665602128117
Iteration time: 64.0
Weighted AreaUnderPRC: 0.6047107169931246
Mean absolute error: 0.2925531914893613
Coverage of cases: 94.56264775413712
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 865.0
Weighted Recall: 0.6713947990543735
Weighted FalsePositiveRate: 0.1091707916323438
Kappa statistic: 0.5627077801825119
Training time: 63.0
		
Iteration: 16
Labeled set size: 384
Unlabelled set size: 39
	
Mean region size: 75.41371158392435
Incorrectly Classified Instances: 32.6241134751773
Correctly Classified Instances: 67.37588652482269
Weighted Precision: 0.7694600891184568
Weighted AreaUnderROC: 0.8477132092446218
Root mean squared error: 0.36967033922916354
Relative absolute error: 77.27869713685304
Root relative squared error: 85.37170794615113
Weighted TruePositiveRate: 0.6737588652482269
Weighted MatthewsCorrelation: 0.5562185222485996
Weighted FMeasure: 0.5914291194113517
Iteration time: 64.0
Weighted AreaUnderPRC: 0.6068800994887352
Mean absolute error: 0.28979511426319887
Coverage of cases: 95.74468085106383
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 929.0
Weighted Recall: 0.6737588652482269
Weighted FalsePositiveRate: 0.10835182403671521
Kappa statistic: 0.5658247054623349
Training time: 64.0
		
Iteration: 17
Labeled set size: 404
Unlabelled set size: 19
	
Mean region size: 75.41371158392435
Incorrectly Classified Instances: 31.914893617021278
Correctly Classified Instances: 68.08510638297872
Weighted Precision: 0.658726976809446
Weighted AreaUnderROC: 0.831960701534266
Root mean squared error: 0.3682910504917818
Relative absolute error: 77.01602311531387
Root relative squared error: 85.05317485662412
Weighted TruePositiveRate: 0.6808510638297872
Weighted MatthewsCorrelation: 0.5636243107558853
Weighted FMeasure: 0.663779283811482
Iteration time: 66.0
Weighted AreaUnderPRC: 0.5934882875244258
Mean absolute error: 0.288810086682427
Coverage of cases: 96.21749408983452
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 995.0
Weighted Recall: 0.6808510638297872
Weighted FalsePositiveRate: 0.10671969519259426
Kappa statistic: 0.5748173959659585
Training time: 65.0
		
Time end:Wed Nov 01 14.44.33 EET 2017