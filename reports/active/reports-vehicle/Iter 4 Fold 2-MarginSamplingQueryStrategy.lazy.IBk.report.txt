Wed Nov 01 14.42.48 EET 2017
Dataset: vehicle
Test set size: 423
Initial Labelled set size: 84
Initial Unlabelled set size: 339
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Wed Nov 01 14.42.48 EET 2017
		
Iteration: 1
Labeled set size: 84
Unlabelled set size: 339
	
Mean region size: 25.0
Incorrectly Classified Instances: 38.061465721040186
Correctly Classified Instances: 61.938534278959814
Weighted Precision: 0.6089375656723774
Weighted AreaUnderROC: 0.7455596398571276
Root mean squared error: 0.426666605420435
Relative absolute error: 52.98732000859656
Root relative squared error: 98.5344317974848
Weighted TruePositiveRate: 0.6193853427895981
Weighted MatthewsCorrelation: 0.4873687630442058
Weighted FMeasure: 0.6078617389138415
Iteration time: 10.0
Weighted AreaUnderPRC: 0.4908781805310687
Mean absolute error: 0.19870245003223713
Coverage of cases: 61.938534278959814
Instances selection time: 10.0
Test time: 13.0
Accumulative iteration time: 10.0
Weighted Recall: 0.6193853427895981
Weighted FalsePositiveRate: 0.12826606307534294
Kappa statistic: 0.4918748321246307
Training time: 0.0
		
Iteration: 2
Labeled set size: 104
Unlabelled set size: 319
	
Mean region size: 25.0
Incorrectly Classified Instances: 39.00709219858156
Correctly Classified Instances: 60.99290780141844
Weighted Precision: 0.5992754574905874
Weighted AreaUnderROC: 0.739099795375623
Root mean squared error: 0.4336693752695357
Relative absolute error: 53.786883810524294
Root relative squared error: 100.15165222046531
Weighted TruePositiveRate: 0.6099290780141844
Weighted MatthewsCorrelation: 0.4751261382305797
Weighted FMeasure: 0.593653737534317
Iteration time: 12.0
Weighted AreaUnderPRC: 0.47797789524438083
Mean absolute error: 0.2017008142894661
Coverage of cases: 60.99290780141844
Instances selection time: 12.0
Test time: 15.0
Accumulative iteration time: 22.0
Weighted Recall: 0.6099290780141844
Weighted FalsePositiveRate: 0.13172948726293832
Kappa statistic: 0.47907959159302604
Training time: 0.0
		
Iteration: 3
Labeled set size: 124
Unlabelled set size: 299
	
Mean region size: 25.0
Incorrectly Classified Instances: 39.479905437352244
Correctly Classified Instances: 60.520094562647756
Weighted Precision: 0.6006969541121406
Weighted AreaUnderROC: 0.7357525719441103
Root mean squared error: 0.43750873983376026
Relative absolute error: 54.11987785657998
Root relative squared error: 101.03831548633417
Weighted TruePositiveRate: 0.6052009456264775
Weighted MatthewsCorrelation: 0.47182877404818985
Weighted FMeasure: 0.5884960126114913
Iteration time: 13.0
Weighted AreaUnderPRC: 0.47266309689312086
Mean absolute error: 0.20294954196217493
Coverage of cases: 60.520094562647756
Instances selection time: 13.0
Test time: 18.0
Accumulative iteration time: 35.0
Weighted Recall: 0.6052009456264775
Weighted FalsePositiveRate: 0.13369580173825718
Kappa statistic: 0.4725173796491961
Training time: 0.0
		
Iteration: 4
Labeled set size: 144
Unlabelled set size: 279
	
Mean region size: 25.0
Incorrectly Classified Instances: 39.952718676122934
Correctly Classified Instances: 60.047281323877066
Weighted Precision: 0.5983219136915495
Weighted AreaUnderROC: 0.7324735028920031
Root mean squared error: 0.4410232858125636
Relative absolute error: 54.5332566609164
Root relative squared error: 101.84996511311074
Weighted TruePositiveRate: 0.6004728132387707
Weighted MatthewsCorrelation: 0.46687259001040776
Weighted FMeasure: 0.5816559319863139
Iteration time: 13.0
Weighted AreaUnderPRC: 0.46812477471758385
Mean absolute error: 0.20449971247843649
Coverage of cases: 60.047281323877066
Instances selection time: 13.0
Test time: 20.0
Accumulative iteration time: 48.0
Weighted Recall: 0.6004728132387707
Weighted FalsePositiveRate: 0.13552580745476464
Kappa statistic: 0.4661244790966527
Training time: 0.0
		
Iteration: 5
Labeled set size: 164
Unlabelled set size: 259
	
Mean region size: 25.0
Incorrectly Classified Instances: 41.371158392434985
Correctly Classified Instances: 58.628841607565015
Weighted Precision: 0.5883785743544848
Weighted AreaUnderROC: 0.7228892235213104
Root mean squared error: 0.44948520173118095
Relative absolute error: 56.22912679650265
Root relative squared error: 103.80416088650024
Weighted TruePositiveRate: 0.5862884160756501
Weighted MatthewsCorrelation: 0.45026001737149374
Weighted FMeasure: 0.5657045582319116
Iteration time: 13.0
Weighted AreaUnderPRC: 0.4541917200630656
Mean absolute error: 0.21085922548688493
Coverage of cases: 58.628841607565015
Instances selection time: 13.0
Test time: 23.0
Accumulative iteration time: 61.0
Weighted Recall: 0.5862884160756501
Weighted FalsePositiveRate: 0.14050996903302926
Kappa statistic: 0.44702164851418574
Training time: 0.0
		
Iteration: 6
Labeled set size: 184
Unlabelled set size: 239
	
Mean region size: 25.0
Incorrectly Classified Instances: 40.8983451536643
Correctly Classified Instances: 59.1016548463357
Weighted Precision: 0.5846869658725111
Weighted AreaUnderROC: 0.7262056255455762
Root mean squared error: 0.4474658847579686
Relative absolute error: 55.49854970407268
Root relative squared error: 103.33781960727491
Weighted TruePositiveRate: 0.5910165484633569
Weighted MatthewsCorrelation: 0.4519799244590168
Weighted FMeasure: 0.5672117306576347
Iteration time: 14.0
Weighted AreaUnderPRC: 0.4610582142677266
Mean absolute error: 0.20811956139027257
Coverage of cases: 59.1016548463357
Instances selection time: 14.0
Test time: 24.0
Accumulative iteration time: 75.0
Weighted Recall: 0.5910165484633569
Weighted FalsePositiveRate: 0.13860529737220473
Kappa statistic: 0.45355774759369466
Training time: 0.0
		
Iteration: 7
Labeled set size: 204
Unlabelled set size: 219
	
Mean region size: 25.0
Incorrectly Classified Instances: 42.08037825059102
Correctly Classified Instances: 57.91962174940898
Weighted Precision: 0.5832971409149221
Weighted AreaUnderROC: 0.7184842738885752
Root mean squared error: 0.45434022797076773
Relative absolute error: 56.95126386615769
Root relative squared error: 104.92538116903948
Weighted TruePositiveRate: 0.5791962174940898
Weighted MatthewsCorrelation: 0.4395650942204545
Weighted FMeasure: 0.5522525759241577
Iteration time: 13.0
Weighted AreaUnderPRC: 0.4575043957254718
Mean absolute error: 0.21356723949809134
Coverage of cases: 57.91962174940898
Instances selection time: 13.0
Test time: 26.0
Accumulative iteration time: 88.0
Weighted Recall: 0.5791962174940898
Weighted FalsePositiveRate: 0.14222766971693965
Kappa statistic: 0.43769743769743763
Training time: 0.0
		
Iteration: 8
Labeled set size: 224
Unlabelled set size: 199
	
Mean region size: 25.0
Incorrectly Classified Instances: 42.78959810874704
Correctly Classified Instances: 57.21040189125296
Weighted Precision: 0.579342038877426
Weighted AreaUnderROC: 0.7138238228516982
Root mean squared error: 0.4585326892273442
Relative absolute error: 57.806257171692266
Root relative squared error: 105.89358862306008
Weighted TruePositiveRate: 0.5721040189125296
Weighted MatthewsCorrelation: 0.43100732222832816
Weighted FMeasure: 0.5403237439464739
Iteration time: 14.0
Weighted AreaUnderPRC: 0.454294231290864
Mean absolute error: 0.216773464393846
Coverage of cases: 57.21040189125296
Instances selection time: 14.0
Test time: 27.0
Accumulative iteration time: 102.0
Weighted Recall: 0.5721040189125296
Weighted FalsePositiveRate: 0.14445637320913293
Kappa statistic: 0.4283399661019481
Training time: 0.0
		
Iteration: 9
Labeled set size: 244
Unlabelled set size: 179
	
Mean region size: 25.0
Incorrectly Classified Instances: 43.026004728132385
Correctly Classified Instances: 56.973995271867615
Weighted Precision: 0.5940258538242861
Weighted AreaUnderROC: 0.7124385029205298
Root mean squared error: 0.4601184215666444
Relative absolute error: 58.0556191057222
Root relative squared error: 106.25979782024315
Weighted TruePositiveRate: 0.5697399527186762
Weighted MatthewsCorrelation: 0.4348518171399752
Weighted FMeasure: 0.5379470733506184
Iteration time: 13.0
Weighted AreaUnderPRC: 0.45874406483716085
Mean absolute error: 0.21770857164645824
Coverage of cases: 56.973995271867615
Instances selection time: 13.0
Test time: 30.0
Accumulative iteration time: 115.0
Weighted Recall: 0.5697399527186762
Weighted FalsePositiveRate: 0.14486294687761636
Kappa statistic: 0.42525028556070693
Training time: 0.0
		
Iteration: 10
Labeled set size: 264
Unlabelled set size: 159
	
Mean region size: 25.0
Incorrectly Classified Instances: 40.66193853427896
Correctly Classified Instances: 59.33806146572104
Weighted Precision: 0.6019135086719255
Weighted AreaUnderROC: 0.7283588140170552
Root mean squared error: 0.4475678590052549
Relative absolute error: 54.8992625524858
Root relative squared error: 103.36136955092337
Weighted TruePositiveRate: 0.5933806146572104
Weighted MatthewsCorrelation: 0.4596935399393628
Weighted FMeasure: 0.5607134278833529
Iteration time: 12.0
Weighted AreaUnderPRC: 0.47911557952693556
Mean absolute error: 0.20587223457182177
Coverage of cases: 59.33806146572104
Instances selection time: 12.0
Test time: 32.0
Accumulative iteration time: 127.0
Weighted Recall: 0.5933806146572104
Weighted FalsePositiveRate: 0.13666298662310022
Kappa statistic: 0.45727563648298847
Training time: 0.0
		
Iteration: 11
Labeled set size: 284
Unlabelled set size: 139
	
Mean region size: 25.0
Incorrectly Classified Instances: 39.952718676122934
Correctly Classified Instances: 60.047281323877066
Weighted Precision: 0.6023200868610701
Weighted AreaUnderROC: 0.733188968838503
Root mean squared error: 0.4438752842968256
Relative absolute error: 53.91931529638362
Root relative squared error: 102.50860595015757
Weighted TruePositiveRate: 0.6004728132387707
Weighted MatthewsCorrelation: 0.4659863751933165
Weighted FMeasure: 0.5660543319524414
Iteration time: 11.0
Weighted AreaUnderPRC: 0.48572026255312245
Mean absolute error: 0.20219743236143858
Coverage of cases: 60.047281323877066
Instances selection time: 11.0
Test time: 34.0
Accumulative iteration time: 138.0
Weighted Recall: 0.6004728132387707
Weighted FalsePositiveRate: 0.13409487556176464
Kappa statistic: 0.4669723744547589
Training time: 0.0
		
Iteration: 12
Labeled set size: 304
Unlabelled set size: 119
	
Mean region size: 25.0
Incorrectly Classified Instances: 37.11583924349882
Correctly Classified Instances: 62.88416075650118
Weighted Precision: 0.6241615044498053
Weighted AreaUnderROC: 0.7523844110407366
Root mean squared error: 0.4280195100763915
Relative absolute error: 50.14378844166083
Root relative squared error: 98.84687174440654
Weighted TruePositiveRate: 0.6288416075650118
Weighted MatthewsCorrelation: 0.5002946627826501
Weighted FMeasure: 0.5947297727471739
Iteration time: 10.0
Weighted AreaUnderPRC: 0.5180624072398381
Mean absolute error: 0.18803920665622811
Coverage of cases: 62.88416075650118
Instances selection time: 10.0
Test time: 35.0
Accumulative iteration time: 148.0
Weighted Recall: 0.6288416075650118
Weighted FalsePositiveRate: 0.12407278548353862
Kappa statistic: 0.5054400035745403
Training time: 0.0
		
Iteration: 13
Labeled set size: 324
Unlabelled set size: 99
	
Mean region size: 25.0
Incorrectly Classified Instances: 36.406619385342786
Correctly Classified Instances: 63.593380614657214
Weighted Precision: 0.6270068427021864
Weighted AreaUnderROC: 0.7572827202415903
Root mean squared error: 0.42407672218352577
Relative absolute error: 49.16969382459786
Root relative squared error: 97.9363238838851
Weighted TruePositiveRate: 0.6359338061465721
Weighted MatthewsCorrelation: 0.507381449597284
Weighted FMeasure: 0.5992804954279348
Iteration time: 9.0
Weighted AreaUnderPRC: 0.5224655314078688
Mean absolute error: 0.18438635184224197
Coverage of cases: 63.593380614657214
Instances selection time: 9.0
Test time: 37.0
Accumulative iteration time: 157.0
Weighted Recall: 0.6359338061465721
Weighted FalsePositiveRate: 0.12136836566339167
Kappa statistic: 0.5151140719788604
Training time: 0.0
		
Iteration: 14
Labeled set size: 344
Unlabelled set size: 79
	
Mean region size: 25.0
Incorrectly Classified Instances: 36.406619385342786
Correctly Classified Instances: 63.593380614657214
Weighted Precision: 0.62558269450285
Weighted AreaUnderROC: 0.7569655760229544
Root mean squared error: 0.42422344887569674
Relative absolute error: 49.1336286151645
Root relative squared error: 97.97020896197398
Weighted TruePositiveRate: 0.6359338061465721
Weighted MatthewsCorrelation: 0.5086122931210518
Weighted FMeasure: 0.6174603945890587
Iteration time: 7.0
Weighted AreaUnderPRC: 0.5274561888828062
Mean absolute error: 0.18425110730686686
Coverage of cases: 63.593380614657214
Instances selection time: 7.0
Test time: 39.0
Accumulative iteration time: 164.0
Weighted Recall: 0.6359338061465721
Weighted FalsePositiveRate: 0.12200265410066323
Kappa statistic: 0.5147492625368731
Training time: 0.0
		
Iteration: 15
Labeled set size: 364
Unlabelled set size: 59
	
Mean region size: 25.0
Incorrectly Classified Instances: 35.69739952718676
Correctly Classified Instances: 64.30260047281324
Weighted Precision: 0.6387407459250964
Weighted AreaUnderROC: 0.7614595790577158
Root mean squared error: 0.4202012306278692
Relative absolute error: 48.166135608318974
Root relative squared error: 97.04131744672492
Weighted TruePositiveRate: 0.6430260047281324
Weighted MatthewsCorrelation: 0.52098531506863
Weighted FMeasure: 0.6367009512508512
Iteration time: 6.0
Weighted AreaUnderPRC: 0.5371539281715977
Mean absolute error: 0.18062300853119617
Coverage of cases: 64.30260047281324
Instances selection time: 6.0
Test time: 41.0
Accumulative iteration time: 170.0
Weighted Recall: 0.6430260047281324
Weighted FalsePositiveRate: 0.12010684661270082
Kappa statistic: 0.5238900980947554
Training time: 0.0
		
Iteration: 16
Labeled set size: 384
Unlabelled set size: 39
	
Mean region size: 25.0
Incorrectly Classified Instances: 36.406619385342786
Correctly Classified Instances: 63.593380614657214
Weighted Precision: 0.6313571072079015
Weighted AreaUnderROC: 0.7566523697507174
Root mean squared error: 0.4244719010609077
Relative absolute error: 49.07265238478232
Root relative squared error: 98.02758653637892
Weighted TruePositiveRate: 0.6359338061465721
Weighted MatthewsCorrelation: 0.5113362343623199
Weighted FMeasure: 0.6314278444500422
Iteration time: 4.0
Weighted AreaUnderPRC: 0.5336552196849124
Mean absolute error: 0.18402244644293372
Coverage of cases: 63.593380614657214
Instances selection time: 4.0
Test time: 43.0
Accumulative iteration time: 174.0
Weighted Recall: 0.6359338061465721
Weighted FalsePositiveRate: 0.1226290666451372
Kappa statistic: 0.5143549409177323
Training time: 0.0
		
Iteration: 17
Labeled set size: 404
Unlabelled set size: 19
	
Mean region size: 25.0
Incorrectly Classified Instances: 36.87943262411348
Correctly Classified Instances: 63.12056737588652
Weighted Precision: 0.628723012996507
Weighted AreaUnderROC: 0.7533733006986102
Root mean squared error: 0.4273258355289365
Relative absolute error: 49.67088490242414
Root relative squared error: 98.6866744697253
Weighted TruePositiveRate: 0.6312056737588653
Weighted MatthewsCorrelation: 0.505860387793704
Weighted FMeasure: 0.6290644004212842
Iteration time: 2.0
Weighted AreaUnderPRC: 0.532331644466176
Mean absolute error: 0.18626581838409054
Coverage of cases: 63.12056737588652
Instances selection time: 2.0
Test time: 44.0
Accumulative iteration time: 176.0
Weighted Recall: 0.6312056737588653
Weighted FalsePositiveRate: 0.12445907236164465
Kappa statistic: 0.5078864353312303
Training time: 0.0
		
Time end:Wed Nov 01 14.42.49 EET 2017