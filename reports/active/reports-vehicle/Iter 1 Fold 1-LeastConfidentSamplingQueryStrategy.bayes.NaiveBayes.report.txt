Wed Nov 01 14.41.10 EET 2017
Dataset: vehicle
Test set size: 423
Initial Labelled set size: 84
Initial Unlabelled set size: 339
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Wed Nov 01 14.41.10 EET 2017
		
Iteration: 1
Labeled set size: 84
Unlabelled set size: 339
	
Mean region size: 38.17966903073286
Incorrectly Classified Instances: 59.810874704491724
Correctly Classified Instances: 40.189125295508276
Weighted Precision: 0.42258359454081257
Weighted AreaUnderROC: 0.736997418355665
Root mean squared error: 0.49978642221250225
Relative absolute error: 78.12717723571389
Root relative squared error: 115.42073016068326
Weighted TruePositiveRate: 0.40189125295508277
Weighted MatthewsCorrelation: 0.21634258971334627
Weighted FMeasure: 0.3644925386086929
Iteration time: 26.0
Weighted AreaUnderPRC: 0.47423893674612705
Mean absolute error: 0.2929769146339271
Coverage of cases: 64.06619385342789
Instances selection time: 26.0
Test time: 36.0
Accumulative iteration time: 26.0
Weighted Recall: 0.40189125295508277
Weighted FalsePositiveRate: 0.1935243364612577
Kappa statistic: 0.2080586080586081
Training time: 0.0
		
Iteration: 2
Labeled set size: 104
Unlabelled set size: 319
	
Mean region size: 38.061465721040186
Incorrectly Classified Instances: 54.846335697399525
Correctly Classified Instances: 45.153664302600475
Weighted Precision: 0.5422072171958586
Weighted AreaUnderROC: 0.7499761150027364
Root mean squared error: 0.4813734274554968
Relative absolute error: 74.05226857950375
Root relative squared error: 111.16843116886555
Weighted TruePositiveRate: 0.4515366430260047
Weighted MatthewsCorrelation: 0.3117054943973294
Weighted FMeasure: 0.4142489206221107
Iteration time: 12.0
Weighted AreaUnderPRC: 0.5058786066370631
Mean absolute error: 0.277696007173139
Coverage of cases: 63.12056737588652
Instances selection time: 11.0
Test time: 19.0
Accumulative iteration time: 38.0
Weighted Recall: 0.4515366430260047
Weighted FalsePositiveRate: 0.17517256179236732
Kappa statistic: 0.27515529326607036
Training time: 1.0
		
Iteration: 3
Labeled set size: 124
Unlabelled set size: 299
	
Mean region size: 38.53427895981088
Incorrectly Classified Instances: 55.082742316784866
Correctly Classified Instances: 44.917257683215134
Weighted Precision: 0.5257377235268171
Weighted AreaUnderROC: 0.7552391043669371
Root mean squared error: 0.4817468506900499
Relative absolute error: 74.04862487928521
Root relative squared error: 111.25466957086192
Weighted TruePositiveRate: 0.4491725768321513
Weighted MatthewsCorrelation: 0.29970432005671405
Weighted FMeasure: 0.40602131183664936
Iteration time: 11.0
Weighted AreaUnderPRC: 0.5100104298862993
Mean absolute error: 0.27768234329731956
Coverage of cases: 65.48463356973996
Instances selection time: 10.0
Test time: 16.0
Accumulative iteration time: 49.0
Weighted Recall: 0.4491725768321513
Weighted FalsePositiveRate: 0.17659487836204976
Kappa statistic: 0.2715305700094607
Training time: 1.0
		
Iteration: 4
Labeled set size: 144
Unlabelled set size: 279
	
Mean region size: 38.94799054373522
Incorrectly Classified Instances: 55.7919621749409
Correctly Classified Instances: 44.2080378250591
Weighted Precision: 0.5164854171569
Weighted AreaUnderROC: 0.7556578787142234
Root mean squared error: 0.4793397331835407
Relative absolute error: 73.47896904675582
Root relative squared error: 110.69876959472025
Weighted TruePositiveRate: 0.44208037825059104
Weighted MatthewsCorrelation: 0.2908495413951151
Weighted FMeasure: 0.39544604960994684
Iteration time: 10.0
Weighted AreaUnderPRC: 0.5130653046023168
Mean absolute error: 0.27554613392533434
Coverage of cases: 66.19385342789599
Instances selection time: 9.0
Test time: 17.0
Accumulative iteration time: 59.0
Weighted Recall: 0.44208037825059104
Weighted FalsePositiveRate: 0.17881361097127338
Kappa statistic: 0.2622111198977141
Training time: 1.0
		
Iteration: 5
Labeled set size: 164
Unlabelled set size: 259
	
Mean region size: 38.356973995271865
Incorrectly Classified Instances: 54.37352245862884
Correctly Classified Instances: 45.62647754137116
Weighted Precision: 0.5416178535675952
Weighted AreaUnderROC: 0.760381231748961
Root mean squared error: 0.47666115469141607
Relative absolute error: 72.41142595848892
Root relative squared error: 110.08017838933078
Weighted TruePositiveRate: 0.4562647754137116
Weighted MatthewsCorrelation: 0.31508500635591535
Weighted FMeasure: 0.41535430624524805
Iteration time: 12.0
Weighted AreaUnderPRC: 0.5178240724006473
Mean absolute error: 0.2715428473443334
Coverage of cases: 66.19385342789599
Instances selection time: 11.0
Test time: 17.0
Accumulative iteration time: 71.0
Weighted Recall: 0.4562647754137116
Weighted FalsePositiveRate: 0.1738917901864809
Kappa statistic: 0.28103754064439845
Training time: 1.0
		
Iteration: 6
Labeled set size: 184
Unlabelled set size: 239
	
Mean region size: 37.94326241134752
Incorrectly Classified Instances: 51.536643026004725
Correctly Classified Instances: 48.463356973995275
Weighted Precision: 0.5865004747175498
Weighted AreaUnderROC: 0.7603396192476068
Root mean squared error: 0.47713996999575115
Relative absolute error: 71.98120167923885
Root relative squared error: 110.19075604727077
Weighted TruePositiveRate: 0.4846335697399527
Weighted MatthewsCorrelation: 0.36144328091208705
Weighted FMeasure: 0.4541981412649616
Iteration time: 9.0
Weighted AreaUnderPRC: 0.522880703192123
Mean absolute error: 0.2699295062971457
Coverage of cases: 65.72104018912529
Instances selection time: 9.0
Test time: 35.0
Accumulative iteration time: 80.0
Weighted Recall: 0.4846335697399527
Weighted FalsePositiveRate: 0.164136886760494
Kappa statistic: 0.31869463387243346
Training time: 0.0
		
Iteration: 7
Labeled set size: 204
Unlabelled set size: 219
	
Mean region size: 37.470449172576835
Incorrectly Classified Instances: 55.55555555555556
Correctly Classified Instances: 44.44444444444444
Weighted Precision: 0.5155308279666313
Weighted AreaUnderROC: 0.7552479267984531
Root mean squared error: 0.4826475781317023
Relative absolute error: 72.85697846554235
Root relative squared error: 111.4626836632237
Weighted TruePositiveRate: 0.4444444444444444
Weighted MatthewsCorrelation: 0.2952420493239455
Weighted FMeasure: 0.40326922175315794
Iteration time: 17.0
Weighted AreaUnderPRC: 0.5090214758475838
Mean absolute error: 0.27321366924578383
Coverage of cases: 65.01182033096927
Instances selection time: 16.0
Test time: 28.0
Accumulative iteration time: 97.0
Weighted Recall: 0.4444444444444444
Weighted FalsePositiveRate: 0.17782598042387746
Kappa statistic: 0.26544591988295013
Training time: 1.0
		
Iteration: 8
Labeled set size: 224
Unlabelled set size: 199
	
Mean region size: 36.997635933806144
Incorrectly Classified Instances: 53.191489361702125
Correctly Classified Instances: 46.808510638297875
Weighted Precision: 0.5638199218272428
Weighted AreaUnderROC: 0.7575566758807879
Root mean squared error: 0.48221501933595734
Relative absolute error: 72.50056037801534
Root relative squared error: 111.36278848835823
Weighted TruePositiveRate: 0.46808510638297873
Weighted MatthewsCorrelation: 0.33769772134626186
Weighted FMeasure: 0.43626944070938245
Iteration time: 16.0
Weighted AreaUnderPRC: 0.5167525624595146
Mean absolute error: 0.2718771014175575
Coverage of cases: 64.30260047281324
Instances selection time: 15.0
Test time: 19.0
Accumulative iteration time: 113.0
Weighted Recall: 0.46808510638297873
Weighted FalsePositiveRate: 0.16961333165671008
Kappa statistic: 0.2968230513483561
Training time: 1.0
		
Iteration: 9
Labeled set size: 244
Unlabelled set size: 179
	
Mean region size: 36.87943262411348
Incorrectly Classified Instances: 54.60992907801418
Correctly Classified Instances: 45.39007092198582
Weighted Precision: 0.5256539429637158
Weighted AreaUnderROC: 0.7643865952390541
Root mean squared error: 0.4827502573185715
Relative absolute error: 72.9332021869523
Root relative squared error: 111.48639640569534
Weighted TruePositiveRate: 0.45390070921985815
Weighted MatthewsCorrelation: 0.3095841609616108
Weighted FMeasure: 0.4174324548912502
Iteration time: 7.0
Weighted AreaUnderPRC: 0.5202468986449256
Mean absolute error: 0.2734995082010711
Coverage of cases: 64.06619385342789
Instances selection time: 6.0
Test time: 17.0
Accumulative iteration time: 120.0
Weighted Recall: 0.45390070921985815
Weighted FalsePositiveRate: 0.17462408194537551
Kappa statistic: 0.2779649745067612
Training time: 1.0
		
Iteration: 10
Labeled set size: 264
Unlabelled set size: 159
	
Mean region size: 36.938534278959814
Incorrectly Classified Instances: 56.501182033096924
Correctly Classified Instances: 43.498817966903076
Weighted Precision: 0.46034464809590425
Weighted AreaUnderROC: 0.7603607053569729
Root mean squared error: 0.4867300484649385
Relative absolute error: 73.5702883832116
Root relative squared error: 112.40548980156476
Weighted TruePositiveRate: 0.43498817966903075
Weighted MatthewsCorrelation: 0.2650998807413903
Weighted FMeasure: 0.38693903467585006
Iteration time: 6.0
Weighted AreaUnderPRC: 0.504559710726075
Mean absolute error: 0.27588858143704353
Coverage of cases: 64.53900709219859
Instances selection time: 5.0
Test time: 17.0
Accumulative iteration time: 126.0
Weighted Recall: 0.43498817966903075
Weighted FalsePositiveRate: 0.18173946363747784
Kappa statistic: 0.2525286132552569
Training time: 1.0
		
Iteration: 11
Labeled set size: 284
Unlabelled set size: 139
	
Mean region size: 37.88416075650118
Incorrectly Classified Instances: 58.628841607565015
Correctly Classified Instances: 41.371158392434985
Weighted Precision: 0.42187114021888844
Weighted AreaUnderROC: 0.7588811213284902
Root mean squared error: 0.4893439764073826
Relative absolute error: 74.57511628106433
Root relative squared error: 113.0091506020497
Weighted TruePositiveRate: 0.41371158392434987
Weighted MatthewsCorrelation: 0.2281317190599679
Weighted FMeasure: 0.3571512495803719
Iteration time: 6.0
Weighted AreaUnderPRC: 0.5013109872669692
Mean absolute error: 0.27965668605399124
Coverage of cases: 66.19385342789599
Instances selection time: 5.0
Test time: 16.0
Accumulative iteration time: 132.0
Weighted Recall: 0.41371158392434987
Weighted FalsePositiveRate: 0.18899404701972847
Kappa statistic: 0.22418039151881788
Training time: 1.0
		
Iteration: 12
Labeled set size: 304
Unlabelled set size: 119
	
Mean region size: 40.307328605200944
Incorrectly Classified Instances: 55.55555555555556
Correctly Classified Instances: 44.44444444444444
Weighted Precision: 0.5004476495089761
Weighted AreaUnderROC: 0.7625618832190186
Root mean squared error: 0.4671377045395624
Relative absolute error: 72.88342435714908
Root relative squared error: 107.88083178581608
Weighted TruePositiveRate: 0.4444444444444444
Weighted MatthewsCorrelation: 0.28881504018504656
Weighted FMeasure: 0.40869941115440295
Iteration time: 5.0
Weighted AreaUnderPRC: 0.5218252458728427
Mean absolute error: 0.27331284133930905
Coverage of cases: 70.2127659574468
Instances selection time: 4.0
Test time: 17.0
Accumulative iteration time: 137.0
Weighted Recall: 0.4444444444444444
Weighted FalsePositiveRate: 0.17846232907250723
Kappa statistic: 0.2650548963069757
Training time: 1.0
		
Iteration: 13
Labeled set size: 324
Unlabelled set size: 99
	
Mean region size: 45.62647754137116
Incorrectly Classified Instances: 54.60992907801418
Correctly Classified Instances: 45.39007092198582
Weighted Precision: 0.4893845192462501
Weighted AreaUnderROC: 0.7575262255625977
Root mean squared error: 0.4484395157201744
Relative absolute error: 72.01507195406462
Root relative squared error: 103.56267004652324
Weighted TruePositiveRate: 0.45390070921985815
Weighted MatthewsCorrelation: 0.29207751508842383
Weighted FMeasure: 0.41614885826107517
Iteration time: 4.0
Weighted AreaUnderPRC: 0.5277761123708116
Mean absolute error: 0.27005651982774237
Coverage of cases: 76.8321513002364
Instances selection time: 3.0
Test time: 17.0
Accumulative iteration time: 141.0
Weighted Recall: 0.45390070921985815
Weighted FalsePositiveRate: 0.17631105350226065
Kappa statistic: 0.2769658805857499
Training time: 1.0
		
Iteration: 14
Labeled set size: 344
Unlabelled set size: 79
	
Mean region size: 45.0354609929078
Incorrectly Classified Instances: 54.1371158392435
Correctly Classified Instances: 45.8628841607565
Weighted Precision: 0.5321331317826937
Weighted AreaUnderROC: 0.7660402268366154
Root mean squared error: 0.454425173117291
Relative absolute error: 73.00703952406359
Root relative squared error: 104.94499841032412
Weighted TruePositiveRate: 0.458628841607565
Weighted MatthewsCorrelation: 0.3125194545288713
Weighted FMeasure: 0.4151575760451103
Iteration time: 4.0
Weighted AreaUnderPRC: 0.5349013892382415
Mean absolute error: 0.2737763982152384
Coverage of cases: 74.94089834515367
Instances selection time: 3.0
Test time: 19.0
Accumulative iteration time: 145.0
Weighted Recall: 0.458628841607565
Weighted FalsePositiveRate: 0.17349430593906087
Kappa statistic: 0.2842375160713494
Training time: 1.0
		
Iteration: 15
Labeled set size: 364
Unlabelled set size: 59
	
Mean region size: 42.730496453900706
Incorrectly Classified Instances: 51.30023640661938
Correctly Classified Instances: 48.69976359338062
Weighted Precision: 0.6122361230512967
Weighted AreaUnderROC: 0.7744658297487402
Root mean squared error: 0.4601160916221178
Relative absolute error: 73.01861841890718
Root relative squared error: 106.2592597426033
Weighted TruePositiveRate: 0.48699763593380613
Weighted MatthewsCorrelation: 0.36896322470681103
Weighted FMeasure: 0.44909541683085624
Iteration time: 4.0
Weighted AreaUnderPRC: 0.5715045068942955
Mean absolute error: 0.2738198190709019
Coverage of cases: 71.63120567375887
Instances selection time: 3.0
Test time: 14.0
Accumulative iteration time: 149.0
Weighted Recall: 0.48699763593380613
Weighted FalsePositiveRate: 0.16337582796352806
Kappa statistic: 0.32202025275317786
Training time: 1.0
		
Iteration: 16
Labeled set size: 384
Unlabelled set size: 39
	
Mean region size: 42.3758865248227
Incorrectly Classified Instances: 52.24586288416076
Correctly Classified Instances: 47.75413711583924
Weighted Precision: 0.6095350669818755
Weighted AreaUnderROC: 0.7764153885276579
Root mean squared error: 0.46129771003087305
Relative absolute error: 73.32741435014282
Root relative squared error: 106.532142825153
Weighted TruePositiveRate: 0.47754137115839246
Weighted MatthewsCorrelation: 0.3558190963492875
Weighted FMeasure: 0.4355231498005395
Iteration time: 3.0
Weighted AreaUnderPRC: 0.5746385950064564
Mean absolute error: 0.27497780381303555
Coverage of cases: 70.68557919621749
Instances selection time: 2.0
Test time: 15.0
Accumulative iteration time: 152.0
Weighted Recall: 0.47754137115839246
Weighted FalsePositiveRate: 0.16649909849962494
Kappa statistic: 0.3095739259521858
Training time: 1.0
		
Iteration: 17
Labeled set size: 404
Unlabelled set size: 19
	
Mean region size: 43.2033096926714
Incorrectly Classified Instances: 54.1371158392435
Correctly Classified Instances: 45.8628841607565
Weighted Precision: 0.5943481450375347
Weighted AreaUnderROC: 0.7750651790158722
Root mean squared error: 0.4659362320548327
Relative absolute error: 73.76318731304478
Root relative squared error: 107.60336360082304
Weighted TruePositiveRate: 0.458628841607565
Weighted MatthewsCorrelation: 0.3292022650121837
Weighted FMeasure: 0.406500825462027
Iteration time: 2.0
Weighted AreaUnderPRC: 0.5763827703187727
Mean absolute error: 0.2766119524239179
Coverage of cases: 70.92198581560284
Instances selection time: 1.0
Test time: 15.0
Accumulative iteration time: 154.0
Weighted Recall: 0.458628841607565
Weighted FalsePositiveRate: 0.17297744144146607
Kappa statistic: 0.28459697788806665
Training time: 1.0
		
Time end:Wed Nov 01 14.41.11 EET 2017