Wed Nov 01 14.43.35 EET 2017
Dataset: vehicle
Test set size: 423
Initial Labelled set size: 84
Initial Unlabelled set size: 339
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Wed Nov 01 14.43.35 EET 2017
		
Iteration: 1
Labeled set size: 84
Unlabelled set size: 339
	
Mean region size: 75.94562647754137
Incorrectly Classified Instances: 41.371158392434985
Correctly Classified Instances: 58.628841607565015
Weighted Precision: 0.5719532125413398
Weighted AreaUnderROC: 0.7838293297176606
Root mean squared error: 0.3892686426819734
Relative absolute error: 81.42894667717354
Root relative squared error: 89.89774225580705
Weighted TruePositiveRate: 0.5862884160756501
Weighted MatthewsCorrelation: 0.4404798180047194
Weighted FMeasure: 0.5738822383936644
Iteration time: 49.0
Weighted AreaUnderPRC: 0.5054452237461833
Mean absolute error: 0.30535855003940077
Coverage of cases: 93.14420803782505
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 49.0
Weighted Recall: 0.5862884160756501
Weighted FalsePositiveRate: 0.13871664125765099
Kappa statistic: 0.4488086373790022
Training time: 48.0
		
Iteration: 2
Labeled set size: 104
Unlabelled set size: 319
	
Mean region size: 75.41371158392435
Incorrectly Classified Instances: 34.515366430260045
Correctly Classified Instances: 65.48463356973996
Weighted Precision: 0.6396733689727983
Weighted AreaUnderROC: 0.8080252784810797
Root mean squared error: 0.37970410002048466
Relative absolute error: 79.38008930916727
Root relative squared error: 87.68890574369257
Weighted TruePositiveRate: 0.6548463356973995
Weighted MatthewsCorrelation: 0.5317705371360524
Weighted FMeasure: 0.6413570611391188
Iteration time: 48.0
Weighted AreaUnderPRC: 0.5580278914870132
Mean absolute error: 0.29767533490937725
Coverage of cases: 94.56264775413712
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 97.0
Weighted Recall: 0.6548463356973995
Weighted FalsePositiveRate: 0.11582971545421224
Kappa statistic: 0.5399159663865545
Training time: 47.0
		
Iteration: 3
Labeled set size: 124
Unlabelled set size: 299
	
Mean region size: 75.41371158392435
Incorrectly Classified Instances: 34.2789598108747
Correctly Classified Instances: 65.72104018912529
Weighted Precision: 0.6906213524354622
Weighted AreaUnderROC: 0.8164566157510673
Root mean squared error: 0.37974733419085194
Relative absolute error: 79.48515891778298
Root relative squared error: 87.69889024765247
Weighted TruePositiveRate: 0.6572104018912529
Weighted MatthewsCorrelation: 0.5457203259871276
Weighted FMeasure: 0.6134309580425503
Iteration time: 49.0
Weighted AreaUnderPRC: 0.565999517248989
Mean absolute error: 0.2980693459416862
Coverage of cases: 94.32624113475177
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 146.0
Weighted Recall: 0.6572104018912529
Weighted FalsePositiveRate: 0.115116080532196
Kappa statistic: 0.5428935542289891
Training time: 48.0
		
Iteration: 4
Labeled set size: 144
Unlabelled set size: 279
	
Mean region size: 75.88652482269504
Incorrectly Classified Instances: 36.643026004728135
Correctly Classified Instances: 63.356973995271865
Weighted Precision: 0.6192522819828691
Weighted AreaUnderROC: 0.8020081364426361
Root mean squared error: 0.37931477073551284
Relative absolute error: 79.32755450485939
Root relative squared error: 87.59899399669982
Weighted TruePositiveRate: 0.6335697399527187
Weighted MatthewsCorrelation: 0.5034278837881846
Weighted FMeasure: 0.6158697722380396
Iteration time: 50.0
Weighted AreaUnderPRC: 0.5503822165767305
Mean absolute error: 0.2974783293932227
Coverage of cases: 94.56264775413712
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 196.0
Weighted Recall: 0.6335697399527187
Weighted FalsePositiveRate: 0.12345279612414477
Kappa statistic: 0.5113544050023476
Training time: 49.0
		
Iteration: 5
Labeled set size: 164
Unlabelled set size: 259
	
Mean region size: 75.65011820330969
Incorrectly Classified Instances: 34.04255319148936
Correctly Classified Instances: 65.95744680851064
Weighted Precision: 0.6705641813226224
Weighted AreaUnderROC: 0.8139108946509706
Root mean squared error: 0.37399721452125906
Relative absolute error: 78.22432361439454
Root relative squared error: 86.37095699200765
Weighted TruePositiveRate: 0.6595744680851063
Weighted MatthewsCorrelation: 0.5476096982830324
Weighted FMeasure: 0.6438905309180228
Iteration time: 51.0
Weighted AreaUnderPRC: 0.5673174102181051
Mean absolute error: 0.2933412135539795
Coverage of cases: 95.74468085106383
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 247.0
Weighted Recall: 0.6595744680851063
Weighted FalsePositiveRate: 0.11481515827911995
Kappa statistic: 0.5458971051984165
Training time: 50.0
		
Iteration: 6
Labeled set size: 184
Unlabelled set size: 239
	
Mean region size: 75.47281323877068
Incorrectly Classified Instances: 34.515366430260045
Correctly Classified Instances: 65.48463356973996
Weighted Precision: 0.6497894355975155
Weighted AreaUnderROC: 0.8279196860416377
Root mean squared error: 0.3698479372885217
Relative absolute error: 77.33123194116098
Root relative squared error: 85.41272246110235
Weighted TruePositiveRate: 0.6548463356973995
Weighted MatthewsCorrelation: 0.5324985267702502
Weighted FMeasure: 0.6207075771524913
Iteration time: 51.0
Weighted AreaUnderPRC: 0.5779042744504098
Mean absolute error: 0.2899921197793537
Coverage of cases: 96.92671394799055
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 298.0
Weighted Recall: 0.6548463356973995
Weighted FalsePositiveRate: 0.11642593067915148
Kappa statistic: 0.5396107139396017
Training time: 50.0
		
Iteration: 7
Labeled set size: 204
Unlabelled set size: 219
	
Mean region size: 75.70921985815603
Incorrectly Classified Instances: 34.751773049645394
Correctly Classified Instances: 65.2482269503546
Weighted Precision: 0.6497587385525421
Weighted AreaUnderROC: 0.8279378072473169
Root mean squared error: 0.36927043129876097
Relative absolute error: 77.27869713685314
Root relative squared error: 85.27935316564354
Weighted TruePositiveRate: 0.6524822695035462
Weighted MatthewsCorrelation: 0.5307658515529388
Weighted FMeasure: 0.622358240998389
Iteration time: 52.0
Weighted AreaUnderPRC: 0.5745390920343043
Mean absolute error: 0.28979511426319926
Coverage of cases: 97.63593380614657
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 350.0
Weighted Recall: 0.6524822695035462
Weighted FalsePositiveRate: 0.11769045993935937
Kappa statistic: 0.53619804875138
Training time: 51.0
		
Iteration: 8
Labeled set size: 224
Unlabelled set size: 199
	
Mean region size: 75.59101654846336
Incorrectly Classified Instances: 33.333333333333336
Correctly Classified Instances: 66.66666666666667
Weighted Precision: 0.659415286868233
Weighted AreaUnderROC: 0.8305057361309508
Root mean squared error: 0.36775574344941364
Relative absolute error: 76.96348831100603
Root relative squared error: 84.92955099061997
Weighted TruePositiveRate: 0.6666666666666666
Weighted MatthewsCorrelation: 0.5488954721717622
Weighted FMeasure: 0.6388133584629203
Iteration time: 53.0
Weighted AreaUnderPRC: 0.5857547283659224
Mean absolute error: 0.2886130811662726
Coverage of cases: 98.10874704491725
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 403.0
Weighted Recall: 0.6666666666666666
Weighted FalsePositiveRate: 0.11222235819860747
Kappa statistic: 0.5555133249865856
Training time: 52.0
		
Iteration: 9
Labeled set size: 244
Unlabelled set size: 179
	
Mean region size: 75.70921985815603
Incorrectly Classified Instances: 32.6241134751773
Correctly Classified Instances: 67.37588652482269
Weighted Precision: 0.6587382923869578
Weighted AreaUnderROC: 0.8284466347257606
Root mean squared error: 0.36842475572417377
Relative absolute error: 77.06855791962171
Root relative squared error: 85.08405275738954
Weighted TruePositiveRate: 0.6737588652482269
Weighted MatthewsCorrelation: 0.5565607326121182
Weighted FMeasure: 0.6547062761975219
Iteration time: 54.0
Weighted AreaUnderPRC: 0.5856612473211297
Mean absolute error: 0.28900709219858145
Coverage of cases: 97.39952718676123
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 457.0
Weighted Recall: 0.6737588652482269
Weighted FalsePositiveRate: 0.10944311979135561
Kappa statistic: 0.5651616844081255
Training time: 54.0
		
Iteration: 10
Labeled set size: 264
Unlabelled set size: 159
	
Mean region size: 75.65011820330969
Incorrectly Classified Instances: 32.38770685579196
Correctly Classified Instances: 67.61229314420804
Weighted Precision: 0.6625640334754339
Weighted AreaUnderROC: 0.8337925126046689
Root mean squared error: 0.3670407862853289
Relative absolute error: 76.80588389808244
Root relative squared error: 84.76443870616262
Weighted TruePositiveRate: 0.6761229314420804
Weighted MatthewsCorrelation: 0.5607275970954136
Weighted FMeasure: 0.660576462718424
Iteration time: 55.0
Weighted AreaUnderPRC: 0.58880898942253
Mean absolute error: 0.28802206461780916
Coverage of cases: 98.58156028368795
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 512.0
Weighted Recall: 0.6761229314420804
Weighted FalsePositiveRate: 0.10848375754067466
Kappa statistic: 0.5683962791113362
Training time: 54.0
		
Iteration: 11
Labeled set size: 284
Unlabelled set size: 139
	
Mean region size: 75.76832151300236
Incorrectly Classified Instances: 30.260047281323878
Correctly Classified Instances: 69.73995271867612
Weighted Precision: 0.6870163778473167
Weighted AreaUnderROC: 0.835240622585554
Root mean squared error: 0.3640316522866673
Relative absolute error: 76.22800105069608
Root relative squared error: 84.06950897650064
Weighted TruePositiveRate: 0.6973995271867612
Weighted MatthewsCorrelation: 0.5902964484814398
Weighted FMeasure: 0.680468391739101
Iteration time: 57.0
Weighted AreaUnderPRC: 0.6041520953366557
Mean absolute error: 0.2858550039401103
Coverage of cases: 98.58156028368795
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 569.0
Weighted Recall: 0.6973995271867612
Weighted FalsePositiveRate: 0.10131901780751136
Kappa statistic: 0.5966957415587221
Training time: 57.0
		
Iteration: 12
Labeled set size: 304
Unlabelled set size: 119
	
Mean region size: 75.65011820330969
Incorrectly Classified Instances: 27.89598108747045
Correctly Classified Instances: 72.10401891252955
Weighted Precision: 0.7167575064680052
Weighted AreaUnderROC: 0.8454512266221815
Root mean squared error: 0.36276670849345255
Relative absolute error: 76.01786183346464
Root relative squared error: 83.77738272069175
Weighted TruePositiveRate: 0.7210401891252955
Weighted MatthewsCorrelation: 0.6247942768149439
Weighted FMeasure: 0.7084116835725343
Iteration time: 58.0
Weighted AreaUnderPRC: 0.6317004993273607
Mean absolute error: 0.2850669818754924
Coverage of cases: 97.63593380614657
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 627.0
Weighted Recall: 0.7210401891252955
Weighted FalsePositiveRate: 0.09325164377737197
Kappa statistic: 0.628248192035273
Training time: 57.0
		
Iteration: 13
Labeled set size: 324
Unlabelled set size: 99
	
Mean region size: 76.12293144208037
Incorrectly Classified Instances: 28.60520094562648
Correctly Classified Instances: 71.39479905437352
Weighted Precision: 0.7004522894166586
Weighted AreaUnderROC: 0.8488559953730767
Root mean squared error: 0.3603601939705272
Relative absolute error: 75.59758339900178
Root relative squared error: 83.22162199764385
Weighted TruePositiveRate: 0.7139479905437353
Weighted MatthewsCorrelation: 0.6118536600768455
Weighted FMeasure: 0.7033246011969416
Iteration time: 58.0
Weighted AreaUnderPRC: 0.6288466095696023
Mean absolute error: 0.28349093774625667
Coverage of cases: 98.3451536643026
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 685.0
Weighted Recall: 0.7139479905437353
Weighted FalsePositiveRate: 0.0954416093485242
Kappa statistic: 0.6189245934838288
Training time: 57.0
		
Iteration: 14
Labeled set size: 344
Unlabelled set size: 79
	
Mean region size: 75.94562647754137
Incorrectly Classified Instances: 28.368794326241133
Correctly Classified Instances: 71.63120567375887
Weighted Precision: 0.7056469738788155
Weighted AreaUnderROC: 0.8481897638729443
Root mean squared error: 0.3606789558143403
Relative absolute error: 75.65011820330967
Root relative squared error: 83.295236892177
Weighted TruePositiveRate: 0.7163120567375887
Weighted MatthewsCorrelation: 0.6158368626526555
Weighted FMeasure: 0.7025491319817561
Iteration time: 61.0
Weighted AreaUnderPRC: 0.6303847178197592
Mean absolute error: 0.28368794326241126
Coverage of cases: 98.10874704491725
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 746.0
Weighted Recall: 0.7163120567375887
Weighted FalsePositiveRate: 0.09485307141731028
Kappa statistic: 0.6219529452070098
Training time: 61.0
		
Iteration: 15
Labeled set size: 364
Unlabelled set size: 59
	
Mean region size: 75.88652482269504
Incorrectly Classified Instances: 28.84160756501182
Correctly Classified Instances: 71.15839243498817
Weighted Precision: 0.6959236800701571
Weighted AreaUnderROC: 0.8468143330489556
Root mean squared error: 0.36154274984914114
Relative absolute error: 75.80772261623319
Root relative squared error: 83.49472157958368
Weighted TruePositiveRate: 0.7115839243498818
Weighted MatthewsCorrelation: 0.6075260647547334
Weighted FMeasure: 0.6991201963929411
Iteration time: 61.0
Weighted AreaUnderPRC: 0.6272190429720931
Mean absolute error: 0.2842789598108745
Coverage of cases: 97.63593380614657
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 807.0
Weighted Recall: 0.7115839243498818
Weighted FalsePositiveRate: 0.09657020863179733
Kappa statistic: 0.6156636107035667
Training time: 60.0
		
Iteration: 16
Labeled set size: 384
Unlabelled set size: 39
	
Mean region size: 75.88652482269504
Incorrectly Classified Instances: 28.13238770685579
Correctly Classified Instances: 71.8676122931442
Weighted Precision: 0.7041544881570702
Weighted AreaUnderROC: 0.8460431831499655
Root mean squared error: 0.3614064985724543
Relative absolute error: 75.75518781192532
Root relative squared error: 83.46325569507465
Weighted TruePositiveRate: 0.7186761229314421
Weighted MatthewsCorrelation: 0.6177618026689304
Weighted FMeasure: 0.7074186215880471
Iteration time: 63.0
Weighted AreaUnderPRC: 0.6294097374546738
Mean absolute error: 0.28408195429471994
Coverage of cases: 96.92671394799055
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 870.0
Weighted Recall: 0.7186761229314421
Weighted FalsePositiveRate: 0.09382034402674977
Kappa statistic: 0.6252149893156825
Training time: 63.0
		
Iteration: 17
Labeled set size: 404
Unlabelled set size: 19
	
Mean region size: 76.00472813238771
Incorrectly Classified Instances: 28.13238770685579
Correctly Classified Instances: 71.8676122931442
Weighted Precision: 0.704810853389529
Weighted AreaUnderROC: 0.85062727732135
Root mean squared error: 0.3599499425256857
Relative absolute error: 75.49251379038601
Root relative squared error: 83.12687848479798
Weighted TruePositiveRate: 0.7186761229314421
Weighted MatthewsCorrelation: 0.6179979846382443
Weighted FMeasure: 0.7069078440437663
Iteration time: 65.0
Weighted AreaUnderPRC: 0.6332380120096329
Mean absolute error: 0.28309692671394754
Coverage of cases: 97.39952718676123
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 935.0
Weighted Recall: 0.7186761229314421
Weighted FalsePositiveRate: 0.09378047730280259
Kappa statistic: 0.625251261893063
Training time: 65.0
		
Time end:Wed Nov 01 14.43.37 EET 2017