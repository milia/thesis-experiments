Wed Nov 01 14.44.36 EET 2017
Dataset: vehicle
Test set size: 423
Initial Labelled set size: 84
Initial Unlabelled set size: 339
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Wed Nov 01 14.44.36 EET 2017
		
Iteration: 1
Labeled set size: 84
Unlabelled set size: 339
	
Mean region size: 75.05910165484633
Incorrectly Classified Instances: 49.645390070921984
Correctly Classified Instances: 50.354609929078016
Weighted Precision: 0.5611538665192689
Weighted AreaUnderROC: 0.738528699612318
Root mean squared error: 0.40086261072423346
Relative absolute error: 83.74047806671906
Root relative squared error: 92.57525448387695
Weighted TruePositiveRate: 0.5035460992907801
Weighted MatthewsCorrelation: 0.361351698023011
Weighted FMeasure: 0.4739636338863184
Iteration time: 52.0
Weighted AreaUnderPRC: 0.43680014509841014
Mean absolute error: 0.3140267927501965
Coverage of cases: 92.19858156028369
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 52.0
Weighted Recall: 0.5035460992907801
Weighted FalsePositiveRate: 0.16047146485003425
Kappa statistic: 0.34256977286351825
Training time: 51.0
		
Iteration: 2
Labeled set size: 104
Unlabelled set size: 319
	
Mean region size: 75.41371158392435
Incorrectly Classified Instances: 52.95508274231678
Correctly Classified Instances: 47.04491725768322
Weighted Precision: 0.5487384758661354
Weighted AreaUnderROC: 0.7364819297422134
Root mean squared error: 0.40319025915827345
Relative absolute error: 84.21329130548975
Root relative squared error: 93.11280186386567
Weighted TruePositiveRate: 0.47044917257683216
Weighted MatthewsCorrelation: 0.32383252841894966
Weighted FMeasure: 0.41477601897728034
Iteration time: 48.0
Weighted AreaUnderPRC: 0.4263318011611609
Mean absolute error: 0.31579984239558656
Coverage of cases: 92.90780141843972
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 100.0
Weighted Recall: 0.47044917257683216
Weighted FalsePositiveRate: 0.1696867489450856
Kappa statistic: 0.3002636418016262
Training time: 47.0
		
Iteration: 3
Labeled set size: 124
Unlabelled set size: 299
	
Mean region size: 75.29550827423168
Incorrectly Classified Instances: 51.536643026004725
Correctly Classified Instances: 48.463356973995275
Weighted Precision: 0.5568120809900307
Weighted AreaUnderROC: 0.7387086956718295
Root mean squared error: 0.4022118384825313
Relative absolute error: 84.00315208825833
Root relative squared error: 92.8868452876575
Weighted TruePositiveRate: 0.4846335697399527
Weighted MatthewsCorrelation: 0.3435169453350576
Weighted FMeasure: 0.43915410858372717
Iteration time: 49.0
Weighted AreaUnderPRC: 0.4328687715932752
Mean absolute error: 0.31501182033096875
Coverage of cases: 92.19858156028369
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 149.0
Weighted Recall: 0.4846335697399527
Weighted FalsePositiveRate: 0.1648440433626575
Kappa statistic: 0.3189764041209704
Training time: 48.0
		
Iteration: 4
Labeled set size: 144
Unlabelled set size: 279
	
Mean region size: 75.53191489361703
Incorrectly Classified Instances: 52.24586288416076
Correctly Classified Instances: 47.75413711583924
Weighted Precision: 0.560354849426946
Weighted AreaUnderROC: 0.7371315940638659
Root mean squared error: 0.4039631615398507
Relative absolute error: 84.4234305227212
Root relative squared error: 93.29129602309004
Weighted TruePositiveRate: 0.47754137115839246
Weighted MatthewsCorrelation: 0.3400178119363748
Weighted FMeasure: 0.43170234740985025
Iteration time: 50.0
Weighted AreaUnderPRC: 0.4298142661193233
Mean absolute error: 0.3165878644602045
Coverage of cases: 91.725768321513
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 199.0
Weighted Recall: 0.47754137115839246
Weighted FalsePositiveRate: 0.16637545127519876
Kappa statistic: 0.31009365244536946
Training time: 49.0
		
Iteration: 5
Labeled set size: 164
Unlabelled set size: 259
	
Mean region size: 75.59101654846336
Incorrectly Classified Instances: 52.4822695035461
Correctly Classified Instances: 47.5177304964539
Weighted Precision: 0.5876321540766654
Weighted AreaUnderROC: 0.7344620173834584
Root mean squared error: 0.40566645719649114
Relative absolute error: 84.73863934856833
Root relative squared error: 93.6846553054384
Weighted TruePositiveRate: 0.475177304964539
Weighted MatthewsCorrelation: 0.34575449551271975
Weighted FMeasure: 0.42331336474013415
Iteration time: 51.0
Weighted AreaUnderPRC: 0.43397446808433754
Mean absolute error: 0.3177698975571312
Coverage of cases: 91.725768321513
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 250.0
Weighted Recall: 0.475177304964539
Weighted FalsePositiveRate: 0.16669811877868132
Kappa statistic: 0.3072888620051194
Training time: 50.0
		
Iteration: 6
Labeled set size: 184
Unlabelled set size: 239
	
Mean region size: 76.12293144208037
Incorrectly Classified Instances: 56.02836879432624
Correctly Classified Instances: 43.97163120567376
Weighted Precision: 0.43631773456169826
Weighted AreaUnderROC: 0.7501552101098459
Root mean squared error: 0.4083287097083081
Relative absolute error: 85.3690570002625
Root relative squared error: 94.2994761871777
Weighted TruePositiveRate: 0.4397163120567376
Weighted MatthewsCorrelation: 0.26209498353165245
Weighted FMeasure: 0.3122582363577038
Iteration time: 52.0
Weighted AreaUnderPRC: 0.44171888448877095
Mean absolute error: 0.3201339637509844
Coverage of cases: 91.725768321513
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 302.0
Weighted Recall: 0.4397163120567376
Weighted FalsePositiveRate: 0.17933830393080072
Kappa statistic: 0.26059314220809393
Training time: 51.0
		
Iteration: 7
Labeled set size: 204
Unlabelled set size: 219
	
Mean region size: 75.23640661938535
Incorrectly Classified Instances: 56.02836879432624
Correctly Classified Instances: 43.97163120567376
Weighted Precision: 0.4694204609320279
Weighted AreaUnderROC: 0.746480419997441
Root mean squared error: 0.4139588926471819
Relative absolute error: 86.47228789072751
Root relative squared error: 95.59971124131594
Weighted TruePositiveRate: 0.4397163120567376
Weighted MatthewsCorrelation: 0.24793956338880566
Weighted FMeasure: 0.29411458846115635
Iteration time: 53.0
Weighted AreaUnderPRC: 0.4317549917961515
Mean absolute error: 0.3242710795902282
Coverage of cases: 91.725768321513
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 355.0
Weighted Recall: 0.4397163120567376
Weighted FalsePositiveRate: 0.1801941716197707
Kappa statistic: 0.26021119744969123
Training time: 52.0
		
Iteration: 8
Labeled set size: 224
Unlabelled set size: 199
	
Mean region size: 75.41371158392435
Incorrectly Classified Instances: 56.737588652482266
Correctly Classified Instances: 43.262411347517734
Weighted Precision: 0.20969657658069543
Weighted AreaUnderROC: 0.7367388655525485
Root mean squared error: 0.4197482578254077
Relative absolute error: 87.7856579984239
Root relative squared error: 96.93670785895023
Weighted TruePositiveRate: 0.4326241134751773
Weighted MatthewsCorrelation: 0.21371223120813773
Weighted FMeasure: 0.282474977448645
Iteration time: 56.0
Weighted AreaUnderPRC: 0.4172616108699949
Mean absolute error: 0.3291962174940896
Coverage of cases: 91.725768321513
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 411.0
Weighted Recall: 0.4326241134751773
Weighted FalsePositiveRate: 0.1825656954356867
Kappa statistic: 0.25087995041285727
Training time: 55.0
		
Iteration: 9
Labeled set size: 244
Unlabelled set size: 179
	
Mean region size: 75.177304964539
Incorrectly Classified Instances: 55.082742316784866
Correctly Classified Instances: 44.917257683215134
Weighted Precision: 0.22162894495472102
Weighted AreaUnderROC: 0.7394600920713904
Root mean squared error: 0.4184948074484411
Relative absolute error: 87.47044917257676
Root relative squared error: 96.64723589392723
Weighted TruePositiveRate: 0.4491725768321513
Weighted MatthewsCorrelation: 0.23330183593848028
Weighted FMeasure: 0.2961415218236368
Iteration time: 60.0
Weighted AreaUnderPRC: 0.42473598937409135
Mean absolute error: 0.3280141843971629
Coverage of cases: 91.725768321513
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 471.0
Weighted Recall: 0.4491725768321513
Weighted FalsePositiveRate: 0.17846338183273547
Kappa statistic: 0.27193954436663414
Training time: 59.0
		
Iteration: 10
Labeled set size: 264
Unlabelled set size: 159
	
Mean region size: 75.0
Incorrectly Classified Instances: 55.082742316784866
Correctly Classified Instances: 44.917257683215134
Weighted Precision: 0.22162894495472102
Weighted AreaUnderROC: 0.7514159427587765
Root mean squared error: 0.41601604048604435
Relative absolute error: 86.89256632519037
Root relative squared error: 96.07478918472799
Weighted TruePositiveRate: 0.4491725768321513
Weighted MatthewsCorrelation: 0.23330183593848028
Weighted FMeasure: 0.2961415218236368
Iteration time: 60.0
Weighted AreaUnderPRC: 0.43518893461674435
Mean absolute error: 0.3258471237194639
Coverage of cases: 92.19858156028369
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 531.0
Weighted Recall: 0.4491725768321513
Weighted FalsePositiveRate: 0.17846338183273547
Kappa statistic: 0.27193954436663414
Training time: 59.0
		
Iteration: 11
Labeled set size: 284
Unlabelled set size: 139
	
Mean region size: 75.0
Incorrectly Classified Instances: 55.082742316784866
Correctly Classified Instances: 44.917257683215134
Weighted Precision: 0.22105860553920625
Weighted AreaUnderROC: 0.687883940537619
Root mean squared error: 0.4293001984188808
Relative absolute error: 89.88705017073808
Root relative squared error: 99.14263404812023
Weighted TruePositiveRate: 0.4491725768321513
Weighted MatthewsCorrelation: 0.23315829430005774
Weighted FMeasure: 0.29575064328463496
Iteration time: 59.0
Weighted AreaUnderPRC: 0.3853197844945191
Mean absolute error: 0.3370764381402678
Coverage of cases: 81.56028368794327
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 590.0
Weighted Recall: 0.4491725768321513
Weighted FalsePositiveRate: 0.1783270730739241
Kappa statistic: 0.2720148316664943
Training time: 59.0
		
Iteration: 12
Labeled set size: 304
Unlabelled set size: 119
	
Mean region size: 75.0
Incorrectly Classified Instances: 51.06382978723404
Correctly Classified Instances: 48.93617021276596
Weighted Precision: 0.46116331057992005
Weighted AreaUnderROC: 0.6994656632264651
Root mean squared error: 0.4255745972815185
Relative absolute error: 89.0464933018124
Root relative squared error: 98.28224332030051
Weighted TruePositiveRate: 0.48936170212765956
Weighted MatthewsCorrelation: 0.32856591508258026
Weighted FMeasure: 0.37658989575228463
Iteration time: 59.0
Weighted AreaUnderPRC: 0.40720960049321303
Mean absolute error: 0.33392434988179653
Coverage of cases: 81.08747044917257
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 649.0
Weighted Recall: 0.48936170212765956
Weighted FalsePositiveRate: 0.16494871660367452
Kappa statistic: 0.32481544157312503
Training time: 59.0
		
Iteration: 13
Labeled set size: 324
Unlabelled set size: 99
	
Mean region size: 75.0
Incorrectly Classified Instances: 47.990543735224584
Correctly Classified Instances: 52.009456264775416
Weighted Precision: 0.41522929167745304
Weighted AreaUnderROC: 0.710591374493211
Root mean squared error: 0.4202564041540195
Relative absolute error: 87.94326241134748
Root relative squared error: 97.05405922679493
Weighted TruePositiveRate: 0.5200945626477541
Weighted MatthewsCorrelation: 0.34641210906319797
Weighted FMeasure: 0.4265056595686311
Iteration time: 61.0
Weighted AreaUnderPRC: 0.4188806104531892
Mean absolute error: 0.32978723404255306
Coverage of cases: 81.08747044917257
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 710.0
Weighted Recall: 0.5200945626477541
Weighted FalsePositiveRate: 0.155972193847105
Kappa statistic: 0.3645781688212702
Training time: 61.0
		
Iteration: 14
Labeled set size: 344
Unlabelled set size: 79
	
Mean region size: 75.0
Incorrectly Classified Instances: 45.8628841607565
Correctly Classified Instances: 54.1371158392435
Weighted Precision: 0.6557108301466492
Weighted AreaUnderROC: 0.7023045524697625
Root mean squared error: 0.4155422179627208
Relative absolute error: 86.89256632519043
Root relative squared error: 95.9653645601724
Weighted TruePositiveRate: 0.541371158392435
Weighted MatthewsCorrelation: 0.3760871718929661
Weighted FMeasure: 0.45809598143686153
Iteration time: 61.0
Weighted AreaUnderPRC: 0.42609601635735206
Mean absolute error: 0.3258471237194641
Coverage of cases: 83.21513002364067
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 771.0
Weighted Recall: 0.541371158392435
Weighted FalsePositiveRate: 0.1500488046738105
Kappa statistic: 0.39182248704893613
Training time: 60.0
		
Iteration: 15
Labeled set size: 364
Unlabelled set size: 59
	
Mean region size: 76.00472813238771
Incorrectly Classified Instances: 32.38770685579196
Correctly Classified Instances: 67.61229314420804
Weighted Precision: 0.6949031898001383
Weighted AreaUnderROC: 0.837120646849965
Root mean squared error: 0.3677557434494142
Relative absolute error: 77.06855791962165
Root relative squared error: 84.9295509906201
Weighted TruePositiveRate: 0.6761229314420804
Weighted MatthewsCorrelation: 0.5751324196725016
Weighted FMeasure: 0.6695891638911304
Iteration time: 64.0
Weighted AreaUnderPRC: 0.6044029802249158
Mean absolute error: 0.28900709219858123
Coverage of cases: 96.92671394799055
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 835.0
Weighted Recall: 0.6761229314420804
Weighted FalsePositiveRate: 0.10702009609586728
Kappa statistic: 0.5692912566519012
Training time: 63.0
		
Iteration: 16
Labeled set size: 384
Unlabelled set size: 39
	
Mean region size: 75.82742316784869
Incorrectly Classified Instances: 24.822695035460992
Correctly Classified Instances: 75.177304964539
Weighted Precision: 0.7481035856011917
Weighted AreaUnderROC: 0.8599938123376124
Root mean squared error: 0.35706498456810754
Relative absolute error: 74.91463094299972
Root relative squared error: 82.4606259834346
Weighted TruePositiveRate: 0.75177304964539
Weighted MatthewsCorrelation: 0.6662516669119205
Weighted FMeasure: 0.7475019053292332
Iteration time: 63.0
Weighted AreaUnderPRC: 0.6635289970353373
Mean absolute error: 0.2809298660362489
Coverage of cases: 97.16312056737588
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 898.0
Weighted Recall: 0.75177304964539
Weighted FalsePositiveRate: 0.08308692964754322
Kappa statistic: 0.6693221159215277
Training time: 63.0
		
Iteration: 17
Labeled set size: 404
Unlabelled set size: 19
	
Mean region size: 75.94562647754137
Incorrectly Classified Instances: 24.113475177304963
Correctly Classified Instances: 75.88652482269504
Weighted Precision: 0.7518424715988674
Weighted AreaUnderROC: 0.8652143397637616
Root mean squared error: 0.3554521126120768
Relative absolute error: 74.65195692146045
Root relative squared error: 82.0881491602415
Weighted TruePositiveRate: 0.7588652482269503
Weighted MatthewsCorrelation: 0.6741564851035903
Weighted FMeasure: 0.7535950853735474
Iteration time: 64.0
Weighted AreaUnderPRC: 0.6729598754412656
Mean absolute error: 0.2799448384554767
Coverage of cases: 97.39952718676123
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 962.0
Weighted Recall: 0.7588652482269503
Weighted FalsePositiveRate: 0.08091986896984425
Kappa statistic: 0.6786911127329054
Training time: 64.0
		
Time end:Wed Nov 01 14.44.38 EET 2017