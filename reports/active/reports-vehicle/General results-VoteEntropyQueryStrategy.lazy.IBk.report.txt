Wed Nov 01 14.44.38 EET 2017
Dataset: vehicle
Test set size: 423
Initial Labelled set size: 84
Initial Unlabelled set size: 339
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Wed Nov 01 14.44.38 EET 2017
		
Iteration: 1
Labeled set size: 84
Unlabelled set size: 339
	
Mean region size: 25.0
Incorrectly Classified Instances: 41.276595744680854
Correctly Classified Instances: 58.723404255319146
Weighted Precision: 0.5848598374306027
Weighted AreaUnderROC: 0.7241610305352424
Root mean squared error: 0.44418358872915337
Relative absolute error: 57.07930367504834
Root relative squared error: 102.57980580895631
Weighted TruePositiveRate: 0.5872340425531914
Weighted MatthewsCorrelation: 0.44752056755638125
Weighted FMeasure: 0.5787799526018512
Iteration time: 14.1
Weighted AreaUnderPRC: 0.4727842896550797
Mean absolute error: 0.21404738878143123
Coverage of cases: 58.723404255319146
Instances selection time: 13.0
Test time: 16.6
Accumulative iteration time: 14.1
Weighted Recall: 0.5872340425531914
Weighted FalsePositiveRate: 0.1389119814827066
Kappa statistic: 0.44912885589278184
Training time: 1.1
		
Iteration: 2
Labeled set size: 104
Unlabelled set size: 319
	
Mean region size: 25.0
Incorrectly Classified Instances: 41.08747044917258
Correctly Classified Instances: 58.91252955082742
Weighted Precision: 0.5874155209576449
Weighted AreaUnderROC: 0.7254628287244212
Root mean squared error: 0.4449894958401467
Relative absolute error: 56.457986749554685
Root relative squared error: 102.76592208394587
Weighted TruePositiveRate: 0.5891252955082742
Weighted MatthewsCorrelation: 0.45053057118727946
Weighted FMeasure: 0.5806729053685297
Iteration time: 13.0
Weighted AreaUnderPRC: 0.4751315762149198
Mean absolute error: 0.21171745031083006
Coverage of cases: 58.91252955082742
Instances selection time: 12.5
Test time: 16.9
Accumulative iteration time: 27.1
Weighted Recall: 0.5891252955082742
Weighted FalsePositiveRate: 0.1381996380594317
Kappa statistic: 0.45170216160998855
Training time: 0.5
		
Iteration: 3
Labeled set size: 124
Unlabelled set size: 299
	
Mean region size: 25.0
Incorrectly Classified Instances: 40.87470449172577
Correctly Classified Instances: 59.125295508274235
Weighted Precision: 0.5950620713150165
Weighted AreaUnderROC: 0.7269325328603242
Root mean squared error: 0.44502715862443376
Relative absolute error: 55.92149330181246
Root relative squared error: 102.77461993140446
Weighted TruePositiveRate: 0.5912529550827423
Weighted MatthewsCorrelation: 0.4560388454452754
Weighted FMeasure: 0.581210255523238
Iteration time: 14.0
Weighted AreaUnderPRC: 0.47742416007896427
Mean absolute error: 0.2097055998817967
Coverage of cases: 59.125295508274235
Instances selection time: 13.0
Test time: 18.5
Accumulative iteration time: 41.1
Weighted Recall: 0.5912529550827423
Weighted FalsePositiveRate: 0.13738788936209378
Kappa statistic: 0.4545817043999749
Training time: 1.0
		
Iteration: 4
Labeled set size: 144
Unlabelled set size: 279
	
Mean region size: 25.0
Incorrectly Classified Instances: 41.5839243498818
Correctly Classified Instances: 58.4160756501182
Weighted Precision: 0.5951110521717602
Weighted AreaUnderROC: 0.7221902050746463
Root mean squared error: 0.44975123273852136
Relative absolute error: 56.6494153728198
Root relative squared error: 103.86559811598056
Weighted TruePositiveRate: 0.5841607565011819
Weighted MatthewsCorrelation: 0.45009395805505903
Weighted FMeasure: 0.572198631031706
Iteration time: 13.4
Weighted AreaUnderPRC: 0.4717820249643382
Mean absolute error: 0.21243530764807428
Coverage of cases: 58.4160756501182
Instances selection time: 12.7
Test time: 19.8
Accumulative iteration time: 54.5
Weighted Recall: 0.5841607565011819
Weighted FalsePositiveRate: 0.1397803463518894
Kappa statistic: 0.4451041107667428
Training time: 0.7
		
Iteration: 5
Labeled set size: 164
Unlabelled set size: 259
	
Mean region size: 25.0
Incorrectly Classified Instances: 41.867612293144205
Correctly Classified Instances: 58.13238770685579
Weighted Precision: 0.5986245782478291
Weighted AreaUnderROC: 0.7202977263601453
Root mean squared error: 0.4519385226998658
Relative absolute error: 56.87530488948924
Root relative squared error: 104.37073109517173
Weighted TruePositiveRate: 0.5813238770685578
Weighted MatthewsCorrelation: 0.4493394114816677
Weighted FMeasure: 0.5678159412377022
Iteration time: 14.9
Weighted AreaUnderPRC: 0.47062754845179977
Mean absolute error: 0.21328239333558466
Coverage of cases: 58.13238770685579
Instances selection time: 14.3
Test time: 22.2
Accumulative iteration time: 69.4
Weighted Recall: 0.5813238770685578
Weighted FalsePositiveRate: 0.1407284243482675
Kappa statistic: 0.44129676307036336
Training time: 0.6
		
Iteration: 6
Labeled set size: 184
Unlabelled set size: 239
	
Mean region size: 25.0
Incorrectly Classified Instances: 40.543735224586285
Correctly Classified Instances: 59.456264775413715
Weighted Precision: 0.6095219136278988
Weighted AreaUnderROC: 0.7293467847838325
Root mean squared error: 0.44522306344628326
Relative absolute error: 55.03579632144605
Root relative squared error: 102.81986221205659
Weighted TruePositiveRate: 0.5945626477541371
Weighted MatthewsCorrelation: 0.4658141868634079
Weighted FMeasure: 0.5799816078900086
Iteration time: 14.1
Weighted AreaUnderPRC: 0.48253155768590633
Mean absolute error: 0.20638423620542273
Coverage of cases: 59.456264775413715
Instances selection time: 13.5
Test time: 23.6
Accumulative iteration time: 83.5
Weighted Recall: 0.5945626477541371
Weighted FalsePositiveRate: 0.13586907818647195
Kappa statistic: 0.4592992487763697
Training time: 0.6
		
Iteration: 7
Labeled set size: 204
Unlabelled set size: 219
	
Mean region size: 25.0
Incorrectly Classified Instances: 39.148936170212764
Correctly Classified Instances: 60.851063829787236
Weighted Precision: 0.6190460234712085
Weighted AreaUnderROC: 0.7388654082442404
Root mean squared error: 0.43806717723686994
Relative absolute error: 53.117839607201475
Root relative squared error: 101.16728108033854
Weighted TruePositiveRate: 0.6085106382978723
Weighted MatthewsCorrelation: 0.48183908743091697
Weighted FMeasure: 0.5905873679163461
Iteration time: 13.8
Weighted AreaUnderPRC: 0.496798696891418
Mean absolute error: 0.19919189852700553
Coverage of cases: 60.851063829787236
Instances selection time: 13.2
Test time: 26.4
Accumulative iteration time: 97.3
Weighted Recall: 0.6085106382978723
Weighted FalsePositiveRate: 0.1307798218093917
Kappa statistic: 0.4782522523153654
Training time: 0.6
		
Iteration: 8
Labeled set size: 224
Unlabelled set size: 199
	
Mean region size: 25.0
Incorrectly Classified Instances: 38.652482269503544
Correctly Classified Instances: 61.34751773049645
Weighted Precision: 0.6225738108145024
Weighted AreaUnderROC: 0.7423292867502078
Root mean squared error: 0.43562869271698645
Relative absolute error: 52.38687735888203
Root relative squared error: 100.60413720275076
Weighted TruePositiveRate: 0.6134751773049646
Weighted MatthewsCorrelation: 0.48746700670833654
Weighted FMeasure: 0.5916002330565429
Iteration time: 13.4
Weighted AreaUnderPRC: 0.5021029658558243
Mean absolute error: 0.19645079009580763
Coverage of cases: 61.34751773049645
Instances selection time: 12.5
Test time: 27.5
Accumulative iteration time: 110.7
Weighted Recall: 0.6134751773049646
Weighted FalsePositiveRate: 0.12881660380454893
Kappa statistic: 0.48509470630586343
Training time: 0.9
		
Iteration: 9
Labeled set size: 244
Unlabelled set size: 179
	
Mean region size: 25.0
Incorrectly Classified Instances: 38.62884160756501
Correctly Classified Instances: 61.371158392434985
Weighted Precision: 0.6254178962076795
Weighted AreaUnderROC: 0.742564442514771
Root mean squared error: 0.4357586849802014
Relative absolute error: 52.28729759271981
Root relative squared error: 100.63415763001464
Weighted TruePositiveRate: 0.6137115839243499
Weighted MatthewsCorrelation: 0.488736942909636
Weighted FMeasure: 0.5902639403736094
Iteration time: 12.9
Weighted AreaUnderPRC: 0.5044459236000707
Mean absolute error: 0.1960773659726993
Coverage of cases: 61.371158392434985
Instances selection time: 12.3
Test time: 29.4
Accumulative iteration time: 123.6
Weighted Recall: 0.6137115839243499
Weighted FalsePositiveRate: 0.12858269889480783
Kappa statistic: 0.4855219466014641
Training time: 0.6
		
Iteration: 10
Labeled set size: 264
Unlabelled set size: 159
	
Mean region size: 25.0
Incorrectly Classified Instances: 37.44680851063829
Correctly Classified Instances: 62.5531914893617
Weighted Precision: 0.6304715422066604
Weighted AreaUnderROC: 0.750432362535033
Root mean squared error: 0.42939791107610314
Relative absolute error: 50.67640520800255
Root relative squared error: 99.16519981970046
Weighted TruePositiveRate: 0.6255319148936171
Weighted MatthewsCorrelation: 0.5013282356829442
Weighted FMeasure: 0.6022657664179798
Iteration time: 12.3
Weighted AreaUnderPRC: 0.5160910969759633
Mean absolute error: 0.1900365195300096
Coverage of cases: 62.5531914893617
Instances selection time: 11.5
Test time: 31.0
Accumulative iteration time: 135.9
Weighted Recall: 0.6255319148936171
Weighted FalsePositiveRate: 0.1246671898235511
Kappa statistic: 0.5012515598360474
Training time: 0.8
		
Iteration: 11
Labeled set size: 284
Unlabelled set size: 139
	
Mean region size: 25.0
Incorrectly Classified Instances: 36.12293144208038
Correctly Classified Instances: 63.87706855791963
Weighted Precision: 0.638430506448146
Weighted AreaUnderROC: 0.7591979556163702
Root mean squared error: 0.42190637271357784
Relative absolute error: 48.883854303475815
Root relative squared error: 97.43510314360108
Weighted TruePositiveRate: 0.6387706855791963
Weighted MatthewsCorrelation: 0.5162453275710674
Weighted FMeasure: 0.6151035113897343
Iteration time: 11.2
Weighted AreaUnderPRC: 0.5294999764638455
Mean absolute error: 0.18331445363803428
Coverage of cases: 63.87706855791963
Instances selection time: 10.8
Test time: 33.0
Accumulative iteration time: 147.1
Weighted Recall: 0.6387706855791963
Weighted FalsePositiveRate: 0.12037477434645572
Kappa statistic: 0.5188416170970382
Training time: 0.4
		
Iteration: 12
Labeled set size: 304
Unlabelled set size: 119
	
Mean region size: 25.0
Incorrectly Classified Instances: 35.082742316784866
Correctly Classified Instances: 64.91725768321513
Weighted Precision: 0.646503267476963
Weighted AreaUnderROC: 0.7661335420913864
Root mean squared error: 0.4160407941678767
Relative absolute error: 47.468197680963634
Root relative squared error: 96.08050580267572
Weighted TruePositiveRate: 0.6491725768321512
Weighted MatthewsCorrelation: 0.5288578802438861
Weighted FMeasure: 0.6252081455754032
Iteration time: 10.4
Weighted AreaUnderPRC: 0.5394880769039169
Mean absolute error: 0.17800574130361363
Coverage of cases: 64.91725768321513
Instances selection time: 9.9
Test time: 34.4
Accumulative iteration time: 157.5
Weighted Recall: 0.6491725768321512
Weighted FalsePositiveRate: 0.11690549264937874
Kappa statistic: 0.5326977805345046
Training time: 0.5
		
Iteration: 13
Labeled set size: 324
Unlabelled set size: 99
	
Mean region size: 25.0
Incorrectly Classified Instances: 34.680851063829785
Correctly Classified Instances: 65.31914893617021
Weighted Precision: 0.6508139713695181
Weighted AreaUnderROC: 0.768754587190741
Root mean squared error: 0.41381330816581324
Relative absolute error: 46.896730669434405
Root relative squared error: 95.56608994551274
Weighted TruePositiveRate: 0.6531914893617021
Weighted MatthewsCorrelation: 0.5340117936369518
Weighted FMeasure: 0.6286739229060964
Iteration time: 9.5
Weighted AreaUnderPRC: 0.5437671051594732
Mean absolute error: 0.175862740010379
Coverage of cases: 65.31914893617021
Instances selection time: 8.7
Test time: 36.3
Accumulative iteration time: 167.0
Weighted Recall: 0.6531914893617021
Weighted FalsePositiveRate: 0.11568231498022041
Kappa statistic: 0.5380127231406308
Training time: 0.8
		
Iteration: 14
Labeled set size: 344
Unlabelled set size: 79
	
Mean region size: 25.0
Incorrectly Classified Instances: 34.37352245862884
Correctly Classified Instances: 65.62647754137116
Weighted Precision: 0.6510116343429136
Weighted AreaUnderROC: 0.7706498599520414
Root mean squared error: 0.4121007188782366
Relative absolute error: 46.45399128646875
Root relative squared error: 95.1705843910353
Weighted TruePositiveRate: 0.6562647754137115
Weighted MatthewsCorrelation: 0.5377699125357838
Weighted FMeasure: 0.638787280156016
Iteration time: 7.9
Weighted AreaUnderPRC: 0.5483634004063299
Mean absolute error: 0.1742024673242578
Coverage of cases: 65.62647754137116
Instances selection time: 7.1
Test time: 38.0
Accumulative iteration time: 174.9
Weighted Recall: 0.6562647754137115
Weighted FalsePositiveRate: 0.1149650555096287
Kappa statistic: 0.5419305971487491
Training time: 0.8
		
Iteration: 15
Labeled set size: 364
Unlabelled set size: 59
	
Mean region size: 25.0
Incorrectly Classified Instances: 33.68794326241135
Correctly Classified Instances: 66.31205673758866
Weighted Precision: 0.6581091403329121
Weighted AreaUnderROC: 0.7750941084719031
Root mean squared error: 0.40809872330624464
Relative absolute error: 45.51598314318035
Root relative squared error: 94.24636310272119
Weighted TruePositiveRate: 0.6631205673758865
Weighted MatthewsCorrelation: 0.5476179171114276
Weighted FMeasure: 0.655089682053831
Iteration time: 6.4
Weighted AreaUnderPRC: 0.5555167136864083
Mean absolute error: 0.17068493678692634
Coverage of cases: 66.31205673758866
Instances selection time: 5.7
Test time: 39.7
Accumulative iteration time: 181.3
Weighted Recall: 0.6631205673758865
Weighted FalsePositiveRate: 0.11293235043208043
Kappa statistic: 0.55086577162766
Training time: 0.7
		
Iteration: 16
Labeled set size: 384
Unlabelled set size: 39
	
Mean region size: 25.0
Incorrectly Classified Instances: 33.35697399527187
Correctly Classified Instances: 66.64302600472813
Weighted Precision: 0.6606835936548241
Weighted AreaUnderROC: 0.7771805590957398
Root mean squared error: 0.40611011775496275
Relative absolute error: 45.048378055616595
Root relative squared error: 93.78711432258334
Weighted TruePositiveRate: 0.6664302600472813
Weighted MatthewsCorrelation: 0.5517466983699948
Weighted FMeasure: 0.6613683854888506
Iteration time: 5.1
Weighted AreaUnderPRC: 0.5611374381191736
Mean absolute error: 0.16893141770856226
Coverage of cases: 66.64302600472813
Instances selection time: 4.1
Test time: 41.3
Accumulative iteration time: 186.4
Weighted Recall: 0.6664302600472813
Weighted FalsePositiveRate: 0.11206914185580164
Kappa statistic: 0.5551313114806524
Training time: 1.0
		
Iteration: 17
Labeled set size: 404
Unlabelled set size: 19
	
Mean region size: 25.0
Incorrectly Classified Instances: 33.3096926713948
Correctly Classified Instances: 66.6903073286052
Weighted Precision: 0.6625316765125392
Weighted AreaUnderROC: 0.7774152082278861
Root mean squared error: 0.4059061574308174
Relative absolute error: 44.95789489948834
Root relative squared error: 93.7400117033636
Weighted TruePositiveRate: 0.666903073286052
Weighted MatthewsCorrelation: 0.5529369868339732
Weighted FMeasure: 0.6636861382771144
Iteration time: 2.8
Weighted AreaUnderPRC: 0.5637161773926687
Mean absolute error: 0.16859210587308132
Coverage of cases: 66.6903073286052
Instances selection time: 2.3
Test time: 42.9
Accumulative iteration time: 189.2
Weighted Recall: 0.666903073286052
Weighted FalsePositiveRate: 0.11207265683027978
Kappa statistic: 0.5556655453822067
Training time: 0.5
		
Time end:Wed Nov 01 14.44.51 EET 2017