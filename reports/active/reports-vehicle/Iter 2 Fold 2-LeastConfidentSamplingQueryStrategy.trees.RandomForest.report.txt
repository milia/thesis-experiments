Wed Nov 01 14.42.03 EET 2017
Dataset: vehicle
Test set size: 423
Initial Labelled set size: 84
Initial Unlabelled set size: 339
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Wed Nov 01 14.42.03 EET 2017
		
Iteration: 1
Labeled set size: 84
Unlabelled set size: 339
	
Mean region size: 63.652482269503544
Incorrectly Classified Instances: 33.096926713947994
Correctly Classified Instances: 66.903073286052
Weighted Precision: 0.6478000711865357
Weighted AreaUnderROC: 0.8379024503444362
Root mean squared error: 0.3590768305150948
Relative absolute error: 60.551615445232464
Root relative squared error: 82.92524190305905
Weighted TruePositiveRate: 0.6690307328605201
Weighted MatthewsCorrelation: 0.5467610217454545
Weighted FMeasure: 0.6404386489270931
Iteration time: 9.0
Weighted AreaUnderPRC: 0.6620098885456306
Mean absolute error: 0.22706855791962172
Coverage of cases: 89.83451536643027
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 9.0
Weighted Recall: 0.6690307328605201
Weighted FalsePositiveRate: 0.11256442642602729
Kappa statistic: 0.5580168225275586
Training time: 8.0
		
Iteration: 2
Labeled set size: 104
Unlabelled set size: 319
	
Mean region size: 65.60283687943263
Incorrectly Classified Instances: 33.56973995271868
Correctly Classified Instances: 66.43026004728132
Weighted Precision: 0.6726638916618851
Weighted AreaUnderROC: 0.8602186387952372
Root mean squared error: 0.3391687795079435
Relative absolute error: 59.795114263199345
Root relative squared error: 78.32767445985121
Weighted TruePositiveRate: 0.6643026004728132
Weighted MatthewsCorrelation: 0.554846134735528
Weighted FMeasure: 0.6677493304417151
Iteration time: 11.0
Weighted AreaUnderPRC: 0.6918462288895039
Mean absolute error: 0.22423167848699754
Coverage of cases: 97.87234042553192
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 20.0
Weighted Recall: 0.6643026004728132
Weighted FalsePositiveRate: 0.11349813073621037
Kappa statistic: 0.5518934081346423
Training time: 10.0
		
Iteration: 3
Labeled set size: 124
Unlabelled set size: 299
	
Mean region size: 61.643026004728135
Incorrectly Classified Instances: 30.260047281323878
Correctly Classified Instances: 69.73995271867612
Weighted Precision: 0.7011348157798403
Weighted AreaUnderROC: 0.8775459961507587
Root mean squared error: 0.3251113648077944
Relative absolute error: 55.00394011032302
Root relative squared error: 75.0812535953547
Weighted TruePositiveRate: 0.6973995271867612
Weighted MatthewsCorrelation: 0.5972367873653777
Weighted FMeasure: 0.6909378096017772
Iteration time: 14.0
Weighted AreaUnderPRC: 0.7205885453356133
Mean absolute error: 0.20626477541371135
Coverage of cases: 97.16312056737588
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 34.0
Weighted Recall: 0.6973995271867612
Weighted FalsePositiveRate: 0.10152684229614684
Kappa statistic: 0.5966146143759685
Training time: 13.0
		
Iteration: 4
Labeled set size: 144
Unlabelled set size: 279
	
Mean region size: 64.00709219858156
Incorrectly Classified Instances: 31.44208037825059
Correctly Classified Instances: 68.5579196217494
Weighted Precision: 0.6961886021608549
Weighted AreaUnderROC: 0.8748864986515259
Root mean squared error: 0.32804312695773424
Relative absolute error: 57.24192277383761
Root relative squared error: 75.75831506194179
Weighted TruePositiveRate: 0.6855791962174941
Weighted MatthewsCorrelation: 0.5847976968578492
Weighted FMeasure: 0.6848857498094726
Iteration time: 17.0
Weighted AreaUnderPRC: 0.7243780758059813
Mean absolute error: 0.21465721040189104
Coverage of cases: 98.10874704491725
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 51.0
Weighted Recall: 0.6855791962174941
Weighted FalsePositiveRate: 0.10516874933561587
Kappa statistic: 0.5809229394018399
Training time: 16.0
		
Iteration: 5
Labeled set size: 164
Unlabelled set size: 259
	
Mean region size: 65.54373522458629
Incorrectly Classified Instances: 30.49645390070922
Correctly Classified Instances: 69.50354609929079
Weighted Precision: 0.717864415699543
Weighted AreaUnderROC: 0.8706500038554504
Root mean squared error: 0.3312166839483911
Relative absolute error: 57.651694247438904
Root relative squared error: 76.49121665507953
Weighted TruePositiveRate: 0.6950354609929078
Weighted MatthewsCorrelation: 0.6026627090986592
Weighted FMeasure: 0.6954876983570613
Iteration time: 19.0
Weighted AreaUnderPRC: 0.7171790073979843
Mean absolute error: 0.21619385342789588
Coverage of cases: 96.21749408983452
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 70.0
Weighted Recall: 0.6950354609929078
Weighted FalsePositiveRate: 0.10234097791317821
Kappa statistic: 0.5935418994413408
Training time: 18.0
		
Iteration: 6
Labeled set size: 184
Unlabelled set size: 239
	
Mean region size: 65.42553191489361
Incorrectly Classified Instances: 30.260047281323878
Correctly Classified Instances: 69.73995271867612
Weighted Precision: 0.7247589673023179
Weighted AreaUnderROC: 0.8840600473026698
Root mean squared error: 0.3239458262505533
Relative absolute error: 56.57998423955869
Root relative squared error: 74.81208399544508
Weighted TruePositiveRate: 0.6973995271867612
Weighted MatthewsCorrelation: 0.6083184646293278
Weighted FMeasure: 0.6934892717630154
Iteration time: 21.0
Weighted AreaUnderPRC: 0.7176175046889695
Mean absolute error: 0.2121749408983451
Coverage of cases: 97.63593380614657
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 91.0
Weighted Recall: 0.6973995271867612
Weighted FalsePositiveRate: 0.10136963451471513
Kappa statistic: 0.5967287840193055
Training time: 20.0
		
Iteration: 7
Labeled set size: 204
Unlabelled set size: 219
	
Mean region size: 57.15130023640662
Incorrectly Classified Instances: 29.314420803782507
Correctly Classified Instances: 70.68557919621749
Weighted Precision: 0.7302433381951384
Weighted AreaUnderROC: 0.8787009888436819
Root mean squared error: 0.3241829154162108
Relative absolute error: 51.53664302600473
Root relative squared error: 74.8668373928908
Weighted TruePositiveRate: 0.706855791962175
Weighted MatthewsCorrelation: 0.6181850666796054
Weighted FMeasure: 0.6937135088951675
Iteration time: 24.0
Weighted AreaUnderPRC: 0.7254762479202422
Mean absolute error: 0.19326241134751776
Coverage of cases: 96.21749408983452
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 115.0
Weighted Recall: 0.706855791962175
Weighted FalsePositiveRate: 0.09792847367762623
Kappa statistic: 0.60952295872789
Training time: 23.0
		
Iteration: 8
Labeled set size: 224
Unlabelled set size: 199
	
Mean region size: 60.46099290780142
Incorrectly Classified Instances: 31.678486997635932
Correctly Classified Instances: 68.32151300236407
Weighted Precision: 0.6725016332671124
Weighted AreaUnderROC: 0.88417344148118
Root mean squared error: 0.32108710144437147
Relative absolute error: 52.860520094562595
Root relative squared error: 74.15188978088982
Weighted TruePositiveRate: 0.6832151300236406
Weighted MatthewsCorrelation: 0.571558152072542
Weighted FMeasure: 0.6515989676961772
Iteration time: 27.0
Weighted AreaUnderPRC: 0.7135900257460299
Mean absolute error: 0.19822695035460974
Coverage of cases: 98.81796690307328
Instances selection time: 2.0
Test time: 2.0
Accumulative iteration time: 142.0
Weighted Recall: 0.6832151300236406
Weighted FalsePositiveRate: 0.10562121403299847
Kappa statistic: 0.5782620665024814
Training time: 25.0
		
Iteration: 9
Labeled set size: 244
Unlabelled set size: 179
	
Mean region size: 57.801418439716315
Incorrectly Classified Instances: 30.023640661938533
Correctly Classified Instances: 69.97635933806147
Weighted Precision: 0.6914292681474938
Weighted AreaUnderROC: 0.8791010219923047
Root mean squared error: 0.3210134660572214
Relative absolute error: 52.57683215130026
Root relative squared error: 74.13488441665264
Weighted TruePositiveRate: 0.6997635933806147
Weighted MatthewsCorrelation: 0.5947445745318246
Weighted FMeasure: 0.6823119583742198
Iteration time: 27.0
Weighted AreaUnderPRC: 0.7048846611367513
Mean absolute error: 0.19716312056737598
Coverage of cases: 97.16312056737588
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 169.0
Weighted Recall: 0.6997635933806147
Weighted FalsePositiveRate: 0.09954893499219349
Kappa statistic: 0.6004299091835444
Training time: 26.0
		
Iteration: 10
Labeled set size: 264
Unlabelled set size: 159
	
Mean region size: 55.9692671394799
Incorrectly Classified Instances: 26.24113475177305
Correctly Classified Instances: 73.75886524822695
Weighted Precision: 0.7319346070525315
Weighted AreaUnderROC: 0.8874282595537976
Root mean squared error: 0.3105893125015559
Relative absolute error: 49.77147360126082
Root relative squared error: 71.72752927207765
Weighted TruePositiveRate: 0.7375886524822695
Weighted MatthewsCorrelation: 0.6464288822626886
Weighted FMeasure: 0.7243238675095964
Iteration time: 30.0
Weighted AreaUnderPRC: 0.7362274475839888
Mean absolute error: 0.18664302600472807
Coverage of cases: 97.16312056737588
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 199.0
Weighted Recall: 0.7375886524822695
Weighted FalsePositiveRate: 0.08715116214109078
Kappa statistic: 0.6506525200517849
Training time: 29.0
		
Iteration: 11
Labeled set size: 284
Unlabelled set size: 139
	
Mean region size: 56.6193853427896
Incorrectly Classified Instances: 30.49645390070922
Correctly Classified Instances: 69.50354609929079
Weighted Precision: 0.6882454153096502
Weighted AreaUnderROC: 0.8831462919995576
Root mean squared error: 0.3158350408831252
Relative absolute error: 50.46493301812452
Root relative squared error: 72.93897834935552
Weighted TruePositiveRate: 0.6950354609929078
Weighted MatthewsCorrelation: 0.5891380689166409
Weighted FMeasure: 0.6902644578878693
Iteration time: 32.0
Weighted AreaUnderPRC: 0.7250945337560091
Mean absolute error: 0.18924349881796693
Coverage of cases: 97.87234042553192
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 231.0
Weighted Recall: 0.6950354609929078
Weighted FalsePositiveRate: 0.10237626439350746
Kappa statistic: 0.5935540095639609
Training time: 31.0
		
Iteration: 12
Labeled set size: 304
Unlabelled set size: 119
	
Mean region size: 59.456264775413715
Incorrectly Classified Instances: 29.078014184397162
Correctly Classified Instances: 70.92198581560284
Weighted Precision: 0.7023202923727441
Weighted AreaUnderROC: 0.8776351073160757
Root mean squared error: 0.3174402644797951
Relative absolute error: 52.16706067769902
Root relative squared error: 73.30968885961428
Weighted TruePositiveRate: 0.7092198581560284
Weighted MatthewsCorrelation: 0.608183911049619
Weighted FMeasure: 0.7044087558277592
Iteration time: 35.0
Weighted AreaUnderPRC: 0.7157085466265817
Mean absolute error: 0.1956264775413713
Coverage of cases: 96.92671394799055
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 266.0
Weighted Recall: 0.7092198581560284
Weighted FalsePositiveRate: 0.09748252054515502
Kappa statistic: 0.612441153685716
Training time: 34.0
		
Iteration: 13
Labeled set size: 324
Unlabelled set size: 99
	
Mean region size: 57.565011820330966
Incorrectly Classified Instances: 28.13238770685579
Correctly Classified Instances: 71.8676122931442
Weighted Precision: 0.7124289120895226
Weighted AreaUnderROC: 0.8906982979604103
Root mean squared error: 0.31039896541466827
Relative absolute error: 50.149724192277354
Root relative squared error: 71.68357049533603
Weighted TruePositiveRate: 0.7186761229314421
Weighted MatthewsCorrelation: 0.6208701119608792
Weighted FMeasure: 0.7123967597277282
Iteration time: 37.0
Weighted AreaUnderPRC: 0.7222960115927661
Mean absolute error: 0.18806146572104007
Coverage of cases: 98.10874704491725
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 303.0
Weighted Recall: 0.7186761229314421
Weighted FalsePositiveRate: 0.09436637981949947
Kappa statistic: 0.6251563803169308
Training time: 36.0
		
Iteration: 14
Labeled set size: 344
Unlabelled set size: 79
	
Mean region size: 56.26477541371158
Incorrectly Classified Instances: 26.713947990543737
Correctly Classified Instances: 73.28605200945627
Weighted Precision: 0.7211365247713558
Weighted AreaUnderROC: 0.89698073686076
Root mean squared error: 0.30323316871663125
Relative absolute error: 48.321513002364064
Root relative squared error: 70.02870063430811
Weighted TruePositiveRate: 0.7328605200945626
Weighted MatthewsCorrelation: 0.637274880169448
Weighted FMeasure: 0.7253443741978022
Iteration time: 41.0
Weighted AreaUnderPRC: 0.7486008741775843
Mean absolute error: 0.18120567375886523
Coverage of cases: 98.10874704491725
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 344.0
Weighted Recall: 0.7328605200945626
Weighted FalsePositiveRate: 0.08991033716299907
Kappa statistic: 0.6439340891822231
Training time: 40.0
		
Iteration: 15
Labeled set size: 364
Unlabelled set size: 59
	
Mean region size: 54.60992907801418
Incorrectly Classified Instances: 30.260047281323878
Correctly Classified Instances: 69.73995271867612
Weighted Precision: 0.6977393264757239
Weighted AreaUnderROC: 0.9041171026621407
Root mean squared error: 0.3002559979460868
Relative absolute error: 46.745468873128424
Root relative squared error: 69.34115249598918
Weighted TruePositiveRate: 0.6973995271867612
Weighted MatthewsCorrelation: 0.5951922360629757
Weighted FMeasure: 0.6973342072132496
Iteration time: 59.0
Weighted AreaUnderPRC: 0.7615898811815206
Mean absolute error: 0.17529550827423157
Coverage of cases: 99.29078014184397
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 403.0
Weighted Recall: 0.6973995271867612
Weighted FalsePositiveRate: 0.10232621440764762
Kappa statistic: 0.596337935764769
Training time: 59.0
		
Iteration: 16
Labeled set size: 384
Unlabelled set size: 39
	
Mean region size: 53.60520094562648
Incorrectly Classified Instances: 26.47754137115839
Correctly Classified Instances: 73.5224586288416
Weighted Precision: 0.732181038669576
Weighted AreaUnderROC: 0.8930631680022973
Root mean squared error: 0.3062778910925566
Relative absolute error: 47.09219858156025
Root relative squared error: 70.73184914764738
Weighted TruePositiveRate: 0.735224586288416
Weighted MatthewsCorrelation: 0.6441340610957792
Weighted FMeasure: 0.7332297621859449
Iteration time: 45.0
Weighted AreaUnderPRC: 0.7469487404379622
Mean absolute error: 0.17659574468085093
Coverage of cases: 97.39952718676123
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 448.0
Weighted Recall: 0.735224586288416
Weighted FalsePositiveRate: 0.08976199355442951
Kappa statistic: 0.6467640918580375
Training time: 44.0
		
Iteration: 17
Labeled set size: 404
Unlabelled set size: 19
	
Mean region size: 54.60992907801418
Incorrectly Classified Instances: 27.89598108747045
Correctly Classified Instances: 72.10401891252955
Weighted Precision: 0.7173100072091999
Weighted AreaUnderROC: 0.8945584990743508
Root mean squared error: 0.3063357758490049
Relative absolute error: 46.77698975571311
Root relative squared error: 70.74521705953434
Weighted TruePositiveRate: 0.7210401891252955
Weighted MatthewsCorrelation: 0.6249650692193393
Weighted FMeasure: 0.7187020375784063
Iteration time: 49.0
Weighted AreaUnderPRC: 0.7486093625386584
Mean absolute error: 0.17541371158392416
Coverage of cases: 97.39952718676123
Instances selection time: 4.0
Test time: 3.0
Accumulative iteration time: 497.0
Weighted Recall: 0.7210401891252955
Weighted FalsePositiveRate: 0.09419440853253722
Kappa statistic: 0.6279960648700215
Training time: 45.0
		
Time end:Wed Nov 01 14.42.05 EET 2017