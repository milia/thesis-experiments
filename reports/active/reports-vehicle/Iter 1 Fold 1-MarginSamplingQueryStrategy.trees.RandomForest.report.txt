Wed Nov 01 14.43.00 EET 2017
Dataset: vehicle
Test set size: 423
Initial Labelled set size: 84
Initial Unlabelled set size: 339
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Wed Nov 01 14.43.00 EET 2017
		
Iteration: 1
Labeled set size: 84
Unlabelled set size: 339
	
Mean region size: 60.99290780141844
Incorrectly Classified Instances: 34.515366430260045
Correctly Classified Instances: 65.48463356973996
Weighted Precision: 0.6594754557044818
Weighted AreaUnderROC: 0.8415873261888481
Root mean squared error: 0.3551711870408674
Relative absolute error: 59.10165484633561
Root relative squared error: 82.0232721785775
Weighted TruePositiveRate: 0.6548463356973995
Weighted MatthewsCorrelation: 0.5385731621011199
Weighted FMeasure: 0.6273598258692883
Iteration time: 11.0
Weighted AreaUnderPRC: 0.6470465594450817
Mean absolute error: 0.22163120567375857
Coverage of cases: 90.0709219858156
Instances selection time: 2.0
Test time: 2.0
Accumulative iteration time: 11.0
Weighted Recall: 0.6548463356973995
Weighted FalsePositiveRate: 0.11399868268856496
Kappa statistic: 0.54056969417436
Training time: 9.0
		
Iteration: 2
Labeled set size: 104
Unlabelled set size: 319
	
Mean region size: 53.78250591016548
Incorrectly Classified Instances: 37.11583924349882
Correctly Classified Instances: 62.88416075650118
Weighted Precision: 0.6523698033919143
Weighted AreaUnderROC: 0.8365218262170689
Root mean squared error: 0.3661343653939883
Relative absolute error: 57.17888100866827
Root relative squared error: 84.55510976791679
Weighted TruePositiveRate: 0.6288416075650118
Weighted MatthewsCorrelation: 0.5025924268313747
Weighted FMeasure: 0.57683747125153
Iteration time: 11.0
Weighted AreaUnderPRC: 0.610203900528975
Mean absolute error: 0.21442080378250603
Coverage of cases: 90.0709219858156
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 22.0
Weighted Recall: 0.6288416075650118
Weighted FalsePositiveRate: 0.12283039135080018
Kappa statistic: 0.5061314335432919
Training time: 10.0
		
Iteration: 3
Labeled set size: 124
Unlabelled set size: 299
	
Mean region size: 56.56028368794326
Incorrectly Classified Instances: 33.333333333333336
Correctly Classified Instances: 66.66666666666667
Weighted Precision: 0.667627303088296
Weighted AreaUnderROC: 0.8210154660844368
Root mean squared error: 0.36681170567904164
Relative absolute error: 57.588652482269566
Root relative squared error: 84.71153480628018
Weighted TruePositiveRate: 0.6666666666666666
Weighted MatthewsCorrelation: 0.5490783432787035
Weighted FMeasure: 0.6216746172997496
Iteration time: 13.0
Weighted AreaUnderPRC: 0.5966589498481673
Mean absolute error: 0.21595744680851087
Coverage of cases: 88.88888888888889
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 35.0
Weighted Recall: 0.6666666666666666
Weighted FalsePositiveRate: 0.11040635837228914
Kappa statistic: 0.5561880525046878
Training time: 12.0
		
Iteration: 4
Labeled set size: 144
Unlabelled set size: 279
	
Mean region size: 61.87943262411348
Incorrectly Classified Instances: 30.023640661938533
Correctly Classified Instances: 69.97635933806147
Weighted Precision: 0.705004894445324
Weighted AreaUnderROC: 0.8726831887786469
Root mean squared error: 0.3320898793147156
Relative absolute error: 54.90937746256896
Root relative squared error: 76.69287248700056
Weighted TruePositiveRate: 0.6997635933806147
Weighted MatthewsCorrelation: 0.597559440493813
Weighted FMeasure: 0.6676270265039833
Iteration time: 16.0
Weighted AreaUnderPRC: 0.7133351898531328
Mean absolute error: 0.20591016548463362
Coverage of cases: 93.3806146572104
Instances selection time: 2.0
Test time: 2.0
Accumulative iteration time: 51.0
Weighted Recall: 0.6997635933806147
Weighted FalsePositiveRate: 0.09922357034010688
Kappa statistic: 0.6002931525807099
Training time: 14.0
		
Iteration: 5
Labeled set size: 164
Unlabelled set size: 259
	
Mean region size: 63.94799054373522
Incorrectly Classified Instances: 34.04255319148936
Correctly Classified Instances: 65.95744680851064
Weighted Precision: 0.6446260745956809
Weighted AreaUnderROC: 0.8514302910812742
Root mean squared error: 0.3423255193953766
Relative absolute error: 58.5027580772261
Root relative squared error: 79.05669230935965
Weighted TruePositiveRate: 0.6595744680851063
Weighted MatthewsCorrelation: 0.5387276260880485
Weighted FMeasure: 0.6420069284173405
Iteration time: 18.0
Weighted AreaUnderPRC: 0.6769848452150566
Mean absolute error: 0.21938534278959787
Coverage of cases: 92.90780141843972
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 69.0
Weighted Recall: 0.6595744680851063
Weighted FalsePositiveRate: 0.11333087151656894
Kappa statistic: 0.5463435342484118
Training time: 17.0
		
Iteration: 6
Labeled set size: 184
Unlabelled set size: 239
	
Mean region size: 57.09219858156028
Incorrectly Classified Instances: 29.55082742316785
Correctly Classified Instances: 70.44917257683215
Weighted Precision: 0.7035985487049317
Weighted AreaUnderROC: 0.8661196123476758
Root mean squared error: 0.33232115828797737
Relative absolute error: 51.50512214342002
Root relative squared error: 76.74628407798879
Weighted TruePositiveRate: 0.7044917257683215
Weighted MatthewsCorrelation: 0.6039528048795147
Weighted FMeasure: 0.6826371942654742
Iteration time: 20.0
Weighted AreaUnderPRC: 0.68554872690852
Mean absolute error: 0.1931442080378251
Coverage of cases: 93.3806146572104
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 89.0
Weighted Recall: 0.7044917257683215
Weighted FalsePositiveRate: 0.09730556862368436
Kappa statistic: 0.6067311268129416
Training time: 19.0
		
Iteration: 7
Labeled set size: 204
Unlabelled set size: 219
	
Mean region size: 59.456264775413715
Incorrectly Classified Instances: 29.314420803782507
Correctly Classified Instances: 70.68557919621749
Weighted Precision: 0.6992461682079864
Weighted AreaUnderROC: 0.8716462425777886
Root mean squared error: 0.3261640365267211
Relative absolute error: 52.5453112687155
Root relative squared error: 75.32435771547095
Weighted TruePositiveRate: 0.706855791962175
Weighted MatthewsCorrelation: 0.6042806016728448
Weighted FMeasure: 0.6838226265916677
Iteration time: 24.0
Weighted AreaUnderPRC: 0.7211651937260449
Mean absolute error: 0.19704491725768314
Coverage of cases: 95.50827423167848
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 113.0
Weighted Recall: 0.706855791962175
Weighted FalsePositiveRate: 0.09678799346109641
Kappa statistic: 0.6098018211033744
Training time: 23.0
		
Iteration: 8
Labeled set size: 224
Unlabelled set size: 199
	
Mean region size: 54.787234042553195
Incorrectly Classified Instances: 30.49645390070922
Correctly Classified Instances: 69.50354609929079
Weighted Precision: 0.6828626097321043
Weighted AreaUnderROC: 0.8516963974285999
Root mean squared error: 0.3370712406548439
Relative absolute error: 51.851851851851784
Root relative squared error: 77.8432686112621
Weighted TruePositiveRate: 0.6950354609929078
Weighted MatthewsCorrelation: 0.5879707753194325
Weighted FMeasure: 0.6821921278699982
Iteration time: 26.0
Weighted AreaUnderPRC: 0.6678891664295471
Mean absolute error: 0.19444444444444417
Coverage of cases: 91.01654846335697
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 139.0
Weighted Recall: 0.6950354609929078
Weighted FalsePositiveRate: 0.10070081513445726
Kappa statistic: 0.5940317828765288
Training time: 25.0
		
Iteration: 9
Labeled set size: 244
Unlabelled set size: 179
	
Mean region size: 54.018912529550825
Incorrectly Classified Instances: 31.678486997635932
Correctly Classified Instances: 68.32151300236407
Weighted Precision: 0.6683377831054788
Weighted AreaUnderROC: 0.8631394458962486
Root mean squared error: 0.33427172876640676
Relative absolute error: 51.50512214342
Root relative squared error: 77.1967490343066
Weighted TruePositiveRate: 0.6832151300236406
Weighted MatthewsCorrelation: 0.570413615064073
Weighted FMeasure: 0.6661385330387876
Iteration time: 30.0
Weighted AreaUnderPRC: 0.6824654016680816
Mean absolute error: 0.193144208037825
Coverage of cases: 92.43498817966903
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 169.0
Weighted Recall: 0.6832151300236406
Weighted FalsePositiveRate: 0.10502723093866458
Kappa statistic: 0.5780674264361056
Training time: 29.0
		
Iteration: 10
Labeled set size: 264
Unlabelled set size: 159
	
Mean region size: 59.515366430260045
Incorrectly Classified Instances: 29.78723404255319
Correctly Classified Instances: 70.2127659574468
Weighted Precision: 0.6957592276047517
Weighted AreaUnderROC: 0.8811151569934853
Root mean squared error: 0.3223546642299868
Relative absolute error: 53.585500394011
Root relative squared error: 74.44462086708573
Weighted TruePositiveRate: 0.7021276595744681
Weighted MatthewsCorrelation: 0.5999905717198348
Weighted FMeasure: 0.6930374354745462
Iteration time: 33.0
Weighted AreaUnderPRC: 0.7151250538034324
Mean absolute error: 0.20094562647754124
Coverage of cases: 98.3451536643026
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 202.0
Weighted Recall: 0.7021276595744681
Weighted FalsePositiveRate: 0.09835794233580605
Kappa statistic: 0.6034256971933688
Training time: 32.0
		
Iteration: 11
Labeled set size: 284
Unlabelled set size: 139
	
Mean region size: 60.16548463356974
Incorrectly Classified Instances: 29.314420803782507
Correctly Classified Instances: 70.68557919621749
Weighted Precision: 0.7054304842924398
Weighted AreaUnderROC: 0.8805356223004348
Root mean squared error: 0.3215653204547233
Relative absolute error: 53.61702127659572
Root relative squared error: 74.26232973063311
Weighted TruePositiveRate: 0.706855791962175
Weighted MatthewsCorrelation: 0.6076954407538055
Weighted FMeasure: 0.6911754440240198
Iteration time: 35.0
Weighted AreaUnderPRC: 0.7181692911552792
Mean absolute error: 0.20106382978723394
Coverage of cases: 97.63593380614657
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 237.0
Weighted Recall: 0.706855791962175
Weighted FalsePositiveRate: 0.09659517685348992
Kappa statistic: 0.6098540634623109
Training time: 34.0
		
Iteration: 12
Labeled set size: 304
Unlabelled set size: 119
	
Mean region size: 58.274231678487
Incorrectly Classified Instances: 28.368794326241133
Correctly Classified Instances: 71.63120567375887
Weighted Precision: 0.7119901649452653
Weighted AreaUnderROC: 0.8940843140510605
Root mean squared error: 0.3092544225590643
Relative absolute error: 50.21276595744671
Root relative squared error: 71.41924964502323
Weighted TruePositiveRate: 0.7163120567375887
Weighted MatthewsCorrelation: 0.6192268026992593
Weighted FMeasure: 0.711432494650445
Iteration time: 38.0
Weighted AreaUnderPRC: 0.7509880617591893
Mean absolute error: 0.18829787234042517
Coverage of cases: 97.39952718676123
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 275.0
Weighted Recall: 0.7163120567375887
Weighted FalsePositiveRate: 0.09447961071015434
Kappa statistic: 0.6220796045088376
Training time: 37.0
		
Iteration: 13
Labeled set size: 324
Unlabelled set size: 99
	
Mean region size: 58.924349881796694
Incorrectly Classified Instances: 27.423167848699762
Correctly Classified Instances: 72.57683215130024
Weighted Precision: 0.7251232358653882
Weighted AreaUnderROC: 0.9085735745770481
Root mean squared error: 0.30000000000000004
Relative absolute error: 49.424743892828964
Root relative squared error: 69.2820323027551
Weighted TruePositiveRate: 0.7257683215130024
Weighted MatthewsCorrelation: 0.6333773096065558
Weighted FMeasure: 0.7207659204113815
Iteration time: 52.0
Weighted AreaUnderPRC: 0.775066923139541
Mean absolute error: 0.18534278959810863
Coverage of cases: 98.58156028368795
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 327.0
Weighted Recall: 0.7257683215130024
Weighted FalsePositiveRate: 0.09137884692689052
Kappa statistic: 0.6346415886702259
Training time: 52.0
		
Iteration: 14
Labeled set size: 344
Unlabelled set size: 79
	
Mean region size: 58.687943262411345
Incorrectly Classified Instances: 29.314420803782507
Correctly Classified Instances: 70.68557919621749
Weighted Precision: 0.6898857301577831
Weighted AreaUnderROC: 0.8893336406367338
Root mean squared error: 0.3092544225590645
Relative absolute error: 50.496453900709184
Root relative squared error: 71.41924964502327
Weighted TruePositiveRate: 0.706855791962175
Weighted MatthewsCorrelation: 0.6011757275385184
Weighted FMeasure: 0.6946207984028572
Iteration time: 43.0
Weighted AreaUnderPRC: 0.7406776780991404
Mean absolute error: 0.18936170212765946
Coverage of cases: 97.63593380614657
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 370.0
Weighted Recall: 0.706855791962175
Weighted FalsePositiveRate: 0.09752165957231766
Kappa statistic: 0.6094240993640818
Training time: 42.0
		
Iteration: 15
Labeled set size: 364
Unlabelled set size: 59
	
Mean region size: 56.38297872340426
Incorrectly Classified Instances: 28.13238770685579
Correctly Classified Instances: 71.8676122931442
Weighted Precision: 0.7098354815237509
Weighted AreaUnderROC: 0.8978922784812098
Root mean squared error: 0.3053889509257296
Relative absolute error: 49.23561859732069
Root relative squared error: 70.52655720980296
Weighted TruePositiveRate: 0.7186761229314421
Weighted MatthewsCorrelation: 0.6203507583857522
Weighted FMeasure: 0.7127449471052566
Iteration time: 46.0
Weighted AreaUnderPRC: 0.7531190189217437
Mean absolute error: 0.18463356973995257
Coverage of cases: 99.05437352245863
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 416.0
Weighted Recall: 0.7186761229314421
Weighted FalsePositiveRate: 0.09390366069085544
Kappa statistic: 0.6250921684727964
Training time: 46.0
		
Iteration: 16
Labeled set size: 384
Unlabelled set size: 39
	
Mean region size: 56.02836879432624
Incorrectly Classified Instances: 29.078014184397162
Correctly Classified Instances: 70.92198581560284
Weighted Precision: 0.7015874339955317
Weighted AreaUnderROC: 0.9014266826249206
Root mean squared error: 0.29988177339442346
Relative absolute error: 47.911741528762775
Root relative squared error: 69.2547290377331
Weighted TruePositiveRate: 0.7092198581560284
Weighted MatthewsCorrelation: 0.6080428348680919
Weighted FMeasure: 0.7047201550026343
Iteration time: 47.0
Weighted AreaUnderPRC: 0.766326779274382
Mean absolute error: 0.17966903073286042
Coverage of cases: 97.63593380614657
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 463.0
Weighted Recall: 0.7092198581560284
Weighted FalsePositiveRate: 0.09749869888138252
Kappa statistic: 0.6123371978660627
Training time: 47.0
		
Iteration: 17
Labeled set size: 404
Unlabelled set size: 19
	
Mean region size: 54.846335697399525
Incorrectly Classified Instances: 26.713947990543737
Correctly Classified Instances: 73.28605200945627
Weighted Precision: 0.7251540731610657
Weighted AreaUnderROC: 0.9046419791469448
Root mean squared error: 0.2976861119981847
Relative absolute error: 46.65090622537425
Root relative squared error: 68.747662758466
Weighted TruePositiveRate: 0.7328605200945626
Weighted MatthewsCorrelation: 0.639587684232798
Weighted FMeasure: 0.7275165286740597
Iteration time: 47.0
Weighted AreaUnderPRC: 0.7651726390646837
Mean absolute error: 0.17494089834515342
Coverage of cases: 98.3451536643026
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 510.0
Weighted Recall: 0.7328605200945626
Weighted FalsePositiveRate: 0.08948057899144386
Kappa statistic: 0.6439102158187628
Training time: 46.0
		
Time end:Wed Nov 01 14.43.02 EET 2017