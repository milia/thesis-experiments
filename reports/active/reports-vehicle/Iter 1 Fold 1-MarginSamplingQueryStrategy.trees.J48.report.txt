Wed Nov 01 14.42.52 EET 2017
Dataset: vehicle
Test set size: 423
Initial Labelled set size: 84
Initial Unlabelled set size: 339
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Wed Nov 01 14.42.52 EET 2017
		
Iteration: 1
Labeled set size: 84
Unlabelled set size: 339
	
Mean region size: 36.347517730496456
Incorrectly Classified Instances: 41.13475177304964
Correctly Classified Instances: 58.86524822695036
Weighted Precision: 0.591539705519762
Weighted AreaUnderROC: 0.7013982975684749
Root mean squared error: 0.4457625287142173
Relative absolute error: 58.856904463913196
Root relative squared error: 102.94444637912066
Weighted TruePositiveRate: 0.5886524822695035
Weighted MatthewsCorrelation: 0.452788227734768
Weighted FMeasure: 0.583549756409754
Iteration time: 12.0
Weighted AreaUnderPRC: 0.4515670569172998
Mean absolute error: 0.2207133917396745
Coverage of cases: 61.22931442080378
Instances selection time: 5.0
Test time: 2.0
Accumulative iteration time: 12.0
Weighted Recall: 0.5886524822695035
Weighted FalsePositiveRate: 0.13617029398367334
Kappa statistic: 0.45216226274655746
Training time: 7.0
		
Iteration: 2
Labeled set size: 104
Unlabelled set size: 319
	
Mean region size: 37.05673758865248
Incorrectly Classified Instances: 43.73522458628842
Correctly Classified Instances: 56.26477541371158
Weighted Precision: 0.5711475008920375
Weighted AreaUnderROC: 0.7091344873275821
Root mean squared error: 0.4508340497868131
Relative absolute error: 60.07946440987754
Root relative squared error: 104.11566400170628
Weighted TruePositiveRate: 0.5626477541371159
Weighted MatthewsCorrelation: 0.4184565594214368
Weighted FMeasure: 0.542018337175298
Iteration time: 12.0
Weighted AreaUnderPRC: 0.45324706381309204
Mean absolute error: 0.22529799153704078
Coverage of cases: 63.593380614657214
Instances selection time: 4.0
Test time: 4.0
Accumulative iteration time: 24.0
Weighted Recall: 0.5626477541371159
Weighted FalsePositiveRate: 0.14472606073447133
Kappa statistic: 0.41772387365601404
Training time: 8.0
		
Iteration: 3
Labeled set size: 124
Unlabelled set size: 299
	
Mean region size: 34.9290780141844
Incorrectly Classified Instances: 42.5531914893617
Correctly Classified Instances: 57.4468085106383
Weighted Precision: 0.5632251878282022
Weighted AreaUnderROC: 0.7407699929443161
Root mean squared error: 0.4437332392433744
Relative absolute error: 57.49210554778924
Root relative squared error: 102.47580205021873
Weighted TruePositiveRate: 0.574468085106383
Weighted MatthewsCorrelation: 0.42662826176596425
Weighted FMeasure: 0.5497533727856835
Iteration time: 6.0
Weighted AreaUnderPRC: 0.4801295617996679
Mean absolute error: 0.21559539580420964
Coverage of cases: 68.32151300236407
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 30.0
Weighted Recall: 0.574468085106383
Weighted FalsePositiveRate: 0.14086355605064024
Kappa statistic: 0.433452635181892
Training time: 6.0
		
Iteration: 4
Labeled set size: 144
Unlabelled set size: 279
	
Mean region size: 34.33806146572104
Incorrectly Classified Instances: 34.515366430260045
Correctly Classified Instances: 65.48463356973996
Weighted Precision: 0.6516681304585477
Weighted AreaUnderROC: 0.7725794956636161
Root mean squared error: 0.4059415110462047
Relative absolute error: 50.494818201674015
Root relative squared error: 93.74817627110788
Weighted TruePositiveRate: 0.6548463356973995
Weighted MatthewsCorrelation: 0.5369106584385508
Weighted FMeasure: 0.6370233593982816
Iteration time: 8.0
Weighted AreaUnderPRC: 0.5289426168345968
Mean absolute error: 0.18935556825627756
Coverage of cases: 70.44917257683215
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 38.0
Weighted Recall: 0.6548463356973995
Weighted FalsePositiveRate: 0.11448524926588878
Kappa statistic: 0.5405457683609094
Training time: 7.0
		
Iteration: 5
Labeled set size: 164
Unlabelled set size: 259
	
Mean region size: 33.51063829787234
Incorrectly Classified Instances: 35.22458628841608
Correctly Classified Instances: 64.77541371158392
Weighted Precision: 0.6857311058281923
Weighted AreaUnderROC: 0.816885079502043
Root mean squared error: 0.36918416309544205
Relative absolute error: 48.150399639761275
Root relative squared error: 85.25943037748007
Weighted TruePositiveRate: 0.6477541371158393
Weighted MatthewsCorrelation: 0.5237623312686867
Weighted FMeasure: 0.5798770365452312
Iteration time: 6.0
Weighted AreaUnderPRC: 0.5782623818683693
Mean absolute error: 0.18056399864910477
Coverage of cases: 81.08747044917257
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 44.0
Weighted Recall: 0.6477541371158393
Weighted FalsePositiveRate: 0.11652826051369893
Kappa statistic: 0.5312653389061595
Training time: 5.0
		
Iteration: 6
Labeled set size: 184
Unlabelled set size: 239
	
Mean region size: 31.087470449172578
Incorrectly Classified Instances: 33.333333333333336
Correctly Classified Instances: 66.66666666666667
Weighted Precision: 0.6590064355094402
Weighted AreaUnderROC: 0.7932220648286026
Root mean squared error: 0.3870122887228695
Relative absolute error: 46.92082033759127
Root relative squared error: 89.37665962953676
Weighted TruePositiveRate: 0.6666666666666666
Weighted MatthewsCorrelation: 0.5476788568284903
Weighted FMeasure: 0.6279311120939494
Iteration time: 9.0
Weighted AreaUnderPRC: 0.5630081766820874
Mean absolute error: 0.1759530762659673
Coverage of cases: 74.94089834515367
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 53.0
Weighted Recall: 0.6666666666666666
Weighted FalsePositiveRate: 0.11045465571111515
Kappa statistic: 0.5562672975627175
Training time: 8.0
		
Iteration: 7
Labeled set size: 204
Unlabelled set size: 219
	
Mean region size: 33.451536643026
Incorrectly Classified Instances: 34.515366430260045
Correctly Classified Instances: 65.48463356973996
Weighted Precision: 0.6457343994759837
Weighted AreaUnderROC: 0.8120132401094339
Root mean squared error: 0.36809501033136066
Relative absolute error: 48.33859188469119
Root relative squared error: 85.00790132086766
Weighted TruePositiveRate: 0.6548463356973995
Weighted MatthewsCorrelation: 0.5253799564928002
Weighted FMeasure: 0.5917884092063702
Iteration time: 8.0
Weighted AreaUnderPRC: 0.568734495255509
Mean absolute error: 0.18126971956759197
Coverage of cases: 81.56028368794327
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 61.0
Weighted Recall: 0.6548463356973995
Weighted FalsePositiveRate: 0.11426699210953116
Kappa statistic: 0.5406619561175158
Training time: 7.0
		
Iteration: 8
Labeled set size: 224
Unlabelled set size: 199
	
Mean region size: 30.61465721040189
Incorrectly Classified Instances: 30.969267139479907
Correctly Classified Instances: 69.03073286052009
Weighted Precision: 0.6759236782956779
Weighted AreaUnderROC: 0.8040326141670944
Root mean squared error: 0.38006466067489597
Relative absolute error: 44.34394604607368
Root relative squared error: 87.772173660046
Weighted TruePositiveRate: 0.6903073286052009
Weighted MatthewsCorrelation: 0.5801948299679452
Weighted FMeasure: 0.6741980469105727
Iteration time: 14.0
Weighted AreaUnderPRC: 0.5777967872818158
Mean absolute error: 0.1662897976727763
Coverage of cases: 76.35933806146572
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 75.0
Weighted Recall: 0.6903073286052009
Weighted FalsePositiveRate: 0.10285001195639423
Kappa statistic: 0.587492183545246
Training time: 14.0
		
Iteration: 9
Labeled set size: 244
Unlabelled set size: 179
	
Mean region size: 33.156028368794324
Incorrectly Classified Instances: 31.678486997635932
Correctly Classified Instances: 68.32151300236407
Weighted Precision: 0.6651730133264027
Weighted AreaUnderROC: 0.7994002467366431
Root mean squared error: 0.38130459011351664
Relative absolute error: 45.834914456681986
Root relative squared error: 88.0585230981115
Weighted TruePositiveRate: 0.6832151300236406
Weighted MatthewsCorrelation: 0.5696317006560653
Weighted FMeasure: 0.6652613258140282
Iteration time: 15.0
Weighted AreaUnderPRC: 0.5725664073494385
Mean absolute error: 0.17188092921255743
Coverage of cases: 76.59574468085107
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 90.0
Weighted Recall: 0.6832151300236406
Weighted FalsePositiveRate: 0.10484953060788552
Kappa statistic: 0.5780956917855122
Training time: 15.0
		
Iteration: 10
Labeled set size: 264
Unlabelled set size: 159
	
Mean region size: 43.321513002364064
Incorrectly Classified Instances: 32.860520094562645
Correctly Classified Instances: 67.13947990543736
Weighted Precision: 0.6599980149172657
Weighted AreaUnderROC: 0.8128685777026594
Root mean squared error: 0.37255115453855686
Relative absolute error: 45.91586085646648
Root relative squared error: 86.03700374389668
Weighted TruePositiveRate: 0.6713947990543735
Weighted MatthewsCorrelation: 0.5566728015044969
Weighted FMeasure: 0.6581486670871296
Iteration time: 17.0
Weighted AreaUnderPRC: 0.6181924463776616
Mean absolute error: 0.1721844782117493
Coverage of cases: 81.56028368794327
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 107.0
Weighted Recall: 0.6713947990543735
Weighted FalsePositiveRate: 0.1088029844462933
Kappa statistic: 0.5625906666369094
Training time: 16.0
		
Iteration: 11
Labeled set size: 284
Unlabelled set size: 139
	
Mean region size: 38.71158392434988
Incorrectly Classified Instances: 33.56973995271868
Correctly Classified Instances: 66.43026004728132
Weighted Precision: 0.6489815765486441
Weighted AreaUnderROC: 0.8058052400556653
Root mean squared error: 0.3810427856262344
Relative absolute error: 45.58414109212262
Root relative squared error: 87.99806194162852
Weighted TruePositiveRate: 0.6643026004728132
Weighted MatthewsCorrelation: 0.5451816981952324
Weighted FMeasure: 0.6525940705303832
Iteration time: 18.0
Weighted AreaUnderPRC: 0.5973071880196622
Mean absolute error: 0.17094052909545984
Coverage of cases: 80.1418439716312
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 125.0
Weighted Recall: 0.6643026004728132
Weighted FalsePositiveRate: 0.11169533869656535
Kappa statistic: 0.552794198668791
Training time: 17.0
		
Iteration: 12
Labeled set size: 304
Unlabelled set size: 119
	
Mean region size: 35.874704491725765
Incorrectly Classified Instances: 34.515366430260045
Correctly Classified Instances: 65.48463356973996
Weighted Precision: 0.6429969926139659
Weighted AreaUnderROC: 0.7919454376520115
Root mean squared error: 0.39458624678276544
Relative absolute error: 47.88192690477965
Root relative squared error: 91.1257903194215
Weighted TruePositiveRate: 0.6548463356973995
Weighted MatthewsCorrelation: 0.5342449409534464
Weighted FMeasure: 0.6445614504046512
Iteration time: 21.0
Weighted AreaUnderPRC: 0.5938116485931155
Mean absolute error: 0.17955722589292367
Coverage of cases: 75.88652482269504
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 146.0
Weighted Recall: 0.6548463356973995
Weighted FalsePositiveRate: 0.11514754717747842
Kappa statistic: 0.5398542625955562
Training time: 20.0
		
Iteration: 13
Labeled set size: 324
Unlabelled set size: 99
	
Mean region size: 38.94799054373522
Incorrectly Classified Instances: 31.678486997635932
Correctly Classified Instances: 68.32151300236407
Weighted Precision: 0.6728034786336009
Weighted AreaUnderROC: 0.8293087425118468
Root mean squared error: 0.3628858378507576
Relative absolute error: 46.0309286886369
Root relative squared error: 83.80489446729513
Weighted TruePositiveRate: 0.6832151300236406
Weighted MatthewsCorrelation: 0.5725445004847324
Weighted FMeasure: 0.6714558648092881
Iteration time: 15.0
Weighted AreaUnderPRC: 0.6143587116342801
Mean absolute error: 0.1726159825823884
Coverage of cases: 83.92434988179669
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 161.0
Weighted Recall: 0.6832151300236406
Weighted FalsePositiveRate: 0.105274106428979
Kappa statistic: 0.5779386141267926
Training time: 14.0
		
Iteration: 14
Labeled set size: 344
Unlabelled set size: 79
	
Mean region size: 31.14657210401891
Incorrectly Classified Instances: 33.096926713947994
Correctly Classified Instances: 66.903073286052
Weighted Precision: 0.6640072584012106
Weighted AreaUnderROC: 0.7978999975760395
Root mean squared error: 0.3929402574528727
Relative absolute error: 45.32645716856238
Root relative squared error: 90.74566536634278
Weighted TruePositiveRate: 0.6690307328605201
Weighted MatthewsCorrelation: 0.5559735261753763
Weighted FMeasure: 0.6583712830447409
Iteration time: 21.0
Weighted AreaUnderPRC: 0.5817476931503441
Mean absolute error: 0.16997421438210894
Coverage of cases: 75.177304964539
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 182.0
Weighted Recall: 0.6690307328605201
Weighted FalsePositiveRate: 0.10967666034492309
Kappa statistic: 0.5592864637985309
Training time: 21.0
		
Iteration: 15
Labeled set size: 364
Unlabelled set size: 59
	
Mean region size: 31.914893617021278
Incorrectly Classified Instances: 34.04255319148936
Correctly Classified Instances: 65.95744680851064
Weighted Precision: 0.6685004645224912
Weighted AreaUnderROC: 0.8013547618675756
Root mean squared error: 0.39415279493045374
Relative absolute error: 45.75089220292647
Root relative squared error: 91.02568890197634
Weighted TruePositiveRate: 0.6595744680851063
Weighted MatthewsCorrelation: 0.5498707762302163
Weighted FMeasure: 0.6615464069056423
Iteration time: 22.0
Weighted AreaUnderPRC: 0.5959190742632805
Mean absolute error: 0.17156584576097428
Coverage of cases: 76.35933806146572
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 204.0
Weighted Recall: 0.6595744680851063
Weighted FalsePositiveRate: 0.11325486998900051
Kappa statistic: 0.5462793296089385
Training time: 21.0
		
Iteration: 16
Labeled set size: 384
Unlabelled set size: 39
	
Mean region size: 34.57446808510638
Incorrectly Classified Instances: 31.914893617021278
Correctly Classified Instances: 68.08510638297872
Weighted Precision: 0.6817293409363041
Weighted AreaUnderROC: 0.8236328379312732
Root mean squared error: 0.38007583581829574
Relative absolute error: 43.35426597651315
Root relative squared error: 87.77475444886602
Weighted TruePositiveRate: 0.6808510638297872
Weighted MatthewsCorrelation: 0.5741927184892274
Weighted FMeasure: 0.678610034464819
Iteration time: 23.0
Weighted AreaUnderPRC: 0.626592411123578
Mean absolute error: 0.1625784974119243
Coverage of cases: 79.66903073286052
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 227.0
Weighted Recall: 0.6808510638297872
Weighted FalsePositiveRate: 0.10648494955417653
Kappa statistic: 0.5747414043475346
Training time: 23.0
		
Iteration: 17
Labeled set size: 404
Unlabelled set size: 19
	
Mean region size: 34.1016548463357
Incorrectly Classified Instances: 33.333333333333336
Correctly Classified Instances: 66.66666666666667
Weighted Precision: 0.6632672285348541
Weighted AreaUnderROC: 0.7956509972226924
Root mean squared error: 0.38964729561658634
Relative absolute error: 45.41368803593525
Root relative squared error: 89.98518840529833
Weighted TruePositiveRate: 0.6666666666666666
Weighted MatthewsCorrelation: 0.5532822063634554
Weighted FMeasure: 0.658875159206816
Iteration time: 24.0
Weighted AreaUnderPRC: 0.5854152432722065
Mean absolute error: 0.17030133013475718
Coverage of cases: 77.06855791962175
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 251.0
Weighted Recall: 0.6666666666666666
Weighted FalsePositiveRate: 0.11105464551254067
Kappa statistic: 0.5558873243631651
Training time: 24.0
		
Time end:Wed Nov 01 14.42.53 EET 2017