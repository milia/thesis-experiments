Wed Nov 01 14.45.05 EET 2017
Dataset: vehicle
Test set size: 423
Initial Labelled set size: 84
Initial Unlabelled set size: 339
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Wed Nov 01 14.45.05 EET 2017
		
Iteration: 1
Labeled set size: 84
Unlabelled set size: 339
	
Mean region size: 66.07565011820331
Incorrectly Classified Instances: 37.825059101654844
Correctly Classified Instances: 62.174940898345156
Weighted Precision: 0.6134459607340832
Weighted AreaUnderROC: 0.8375697988079412
Root mean squared error: 0.35576973454597355
Relative absolute error: 62.56895193065409
Root relative squared error: 82.16150080385582
Weighted TruePositiveRate: 0.6217494089834515
Weighted MatthewsCorrelation: 0.4901461140960064
Weighted FMeasure: 0.5971814074495032
Iteration time: 10.0
Weighted AreaUnderPRC: 0.6601245556313063
Mean absolute error: 0.2346335697399528
Coverage of cases: 94.32624113475177
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 10.0
Weighted Recall: 0.6217494089834515
Weighted FalsePositiveRate: 0.12628437286225058
Kappa statistic: 0.49628990123769195
Training time: 9.0
		
Iteration: 2
Labeled set size: 104
Unlabelled set size: 319
	
Mean region size: 60.40189125295508
Incorrectly Classified Instances: 34.04255319148936
Correctly Classified Instances: 65.95744680851064
Weighted Precision: 0.6359503947000819
Weighted AreaUnderROC: 0.8433508115112285
Root mean squared error: 0.3455389458810212
Relative absolute error: 56.67454688731285
Root relative squared error: 79.79880136796285
Weighted TruePositiveRate: 0.6595744680851063
Weighted MatthewsCorrelation: 0.5338942606740681
Weighted FMeasure: 0.6308402084237952
Iteration time: 11.0
Weighted AreaUnderPRC: 0.6646957325207832
Mean absolute error: 0.21252955082742317
Coverage of cases: 91.96217494089835
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 21.0
Weighted Recall: 0.6595744680851063
Weighted FalsePositiveRate: 0.11374105103165276
Kappa statistic: 0.5464718890303557
Training time: 10.0
		
Iteration: 3
Labeled set size: 124
Unlabelled set size: 299
	
Mean region size: 61.111111111111114
Incorrectly Classified Instances: 31.20567375886525
Correctly Classified Instances: 68.79432624113475
Weighted Precision: 0.6690458689580298
Weighted AreaUnderROC: 0.8668766093127795
Root mean squared error: 0.3308953395989758
Relative absolute error: 54.97241922773839
Root relative squared error: 76.41700535642453
Weighted TruePositiveRate: 0.6879432624113475
Weighted MatthewsCorrelation: 0.573896271983015
Weighted FMeasure: 0.6632697164860247
Iteration time: 12.0
Weighted AreaUnderPRC: 0.6904281713870694
Mean absolute error: 0.206146572104019
Coverage of cases: 95.27186761229315
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 33.0
Weighted Recall: 0.6879432624113475
Weighted FalsePositiveRate: 0.10461147945061214
Kappa statistic: 0.584101777228239
Training time: 11.0
		
Iteration: 4
Labeled set size: 144
Unlabelled set size: 279
	
Mean region size: 60.638297872340424
Incorrectly Classified Instances: 37.11583924349882
Correctly Classified Instances: 62.88416075650118
Weighted Precision: 0.5940148712577382
Weighted AreaUnderROC: 0.8535914355135304
Root mean squared error: 0.3491631332443636
Relative absolute error: 56.64302600472818
Root relative squared error: 80.6357715878906
Weighted TruePositiveRate: 0.6288416075650118
Weighted MatthewsCorrelation: 0.48880554854340635
Weighted FMeasure: 0.5933484528250824
Iteration time: 14.0
Weighted AreaUnderPRC: 0.6728963143202283
Mean absolute error: 0.21241134751773066
Coverage of cases: 94.56264775413712
Instances selection time: 2.0
Test time: 2.0
Accumulative iteration time: 47.0
Weighted Recall: 0.6288416075650118
Weighted FalsePositiveRate: 0.12479476299494997
Kappa statistic: 0.5050308559162866
Training time: 12.0
		
Iteration: 5
Labeled set size: 164
Unlabelled set size: 259
	
Mean region size: 58.924349881796694
Incorrectly Classified Instances: 33.56973995271868
Correctly Classified Instances: 66.43026004728132
Weighted Precision: 0.6454008668623951
Weighted AreaUnderROC: 0.8727530685242749
Root mean squared error: 0.33857579615198824
Relative absolute error: 54.21591804570529
Root relative squared error: 78.1907308197769
Weighted TruePositiveRate: 0.6643026004728132
Weighted MatthewsCorrelation: 0.5405677750001688
Weighted FMeasure: 0.6226282109955513
Iteration time: 14.0
Weighted AreaUnderPRC: 0.7056202782624629
Mean absolute error: 0.20330969267139484
Coverage of cases: 94.79905437352245
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 61.0
Weighted Recall: 0.6643026004728132
Weighted FalsePositiveRate: 0.11278781202673557
Kappa statistic: 0.5526043334798185
Training time: 13.0
		
Iteration: 6
Labeled set size: 184
Unlabelled set size: 239
	
Mean region size: 56.08747044917258
Incorrectly Classified Instances: 33.56973995271868
Correctly Classified Instances: 66.43026004728132
Weighted Precision: 0.6543189833424778
Weighted AreaUnderROC: 0.876523243863021
Root mean squared error: 0.33096677641751077
Relative absolute error: 51.662726556343564
Root relative squared error: 76.43350298298903
Weighted TruePositiveRate: 0.6643026004728132
Weighted MatthewsCorrelation: 0.5432839706851674
Weighted FMeasure: 0.6224253861185336
Iteration time: 16.0
Weighted AreaUnderPRC: 0.7155430648720206
Mean absolute error: 0.1937352245862884
Coverage of cases: 94.56264775413712
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 77.0
Weighted Recall: 0.6643026004728132
Weighted FalsePositiveRate: 0.11243265267199816
Kappa statistic: 0.5527442497710332
Training time: 15.0
		
Iteration: 7
Labeled set size: 204
Unlabelled set size: 219
	
Mean region size: 53.723404255319146
Incorrectly Classified Instances: 36.87943262411348
Correctly Classified Instances: 63.12056737588652
Weighted Precision: 0.573302458293053
Weighted AreaUnderROC: 0.8569022518715272
Root mean squared error: 0.34797624983491704
Relative absolute error: 52.57683215130026
Root relative squared error: 80.36167260551433
Weighted TruePositiveRate: 0.6312056737588653
Weighted MatthewsCorrelation: 0.48116989235377783
Weighted FMeasure: 0.5747361676543326
Iteration time: 18.0
Weighted AreaUnderPRC: 0.6612813073324706
Mean absolute error: 0.19716312056737598
Coverage of cases: 91.01654846335697
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 95.0
Weighted Recall: 0.6312056737588653
Weighted FalsePositiveRate: 0.12260588098786818
Kappa statistic: 0.5090726481419485
Training time: 17.0
		
Iteration: 8
Labeled set size: 224
Unlabelled set size: 199
	
Mean region size: 53.3096926713948
Incorrectly Classified Instances: 34.2789598108747
Correctly Classified Instances: 65.72104018912529
Weighted Precision: 0.6296303317757039
Weighted AreaUnderROC: 0.8701284071276296
Root mean squared error: 0.33673793220868303
Relative absolute error: 50.591016548463394
Root relative squared error: 77.76629432281645
Weighted TruePositiveRate: 0.6572104018912529
Weighted MatthewsCorrelation: 0.5263973612900181
Weighted FMeasure: 0.603694404224066
Iteration time: 19.0
Weighted AreaUnderPRC: 0.7052446841372566
Mean absolute error: 0.18971631205673772
Coverage of cases: 91.725768321513
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 114.0
Weighted Recall: 0.6572104018912529
Weighted FalsePositiveRate: 0.11381987593293044
Kappa statistic: 0.5437775678550442
Training time: 18.0
		
Iteration: 9
Labeled set size: 244
Unlabelled set size: 179
	
Mean region size: 51.6548463356974
Incorrectly Classified Instances: 34.515366430260045
Correctly Classified Instances: 65.48463356973996
Weighted Precision: 0.6054707707700392
Weighted AreaUnderROC: 0.8622954446237763
Root mean squared error: 0.3426878874131226
Relative absolute error: 50.181245074862126
Root relative squared error: 79.14037761839622
Weighted TruePositiveRate: 0.6548463356973995
Weighted MatthewsCorrelation: 0.5161884013750548
Weighted FMeasure: 0.6083625716184575
Iteration time: 21.0
Weighted AreaUnderPRC: 0.6809488843529093
Mean absolute error: 0.188179669030733
Coverage of cases: 91.48936170212765
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 135.0
Weighted Recall: 0.6548463356973995
Weighted FalsePositiveRate: 0.11501931014800329
Kappa statistic: 0.5405252585373111
Training time: 20.0
		
Iteration: 10
Labeled set size: 264
Unlabelled set size: 159
	
Mean region size: 49.054373522458626
Incorrectly Classified Instances: 33.096926713947994
Correctly Classified Instances: 66.903073286052
Weighted Precision: 0.631574943312148
Weighted AreaUnderROC: 0.8697579078806128
Root mean squared error: 0.33918620450112225
Relative absolute error: 47.88022064617814
Root relative squared error: 78.33169858965215
Weighted TruePositiveRate: 0.6690307328605201
Weighted MatthewsCorrelation: 0.5354992275876885
Weighted FMeasure: 0.6024229924407669
Iteration time: 22.0
Weighted AreaUnderPRC: 0.6956995832247319
Mean absolute error: 0.17955082742316802
Coverage of cases: 91.01654846335697
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 157.0
Weighted Recall: 0.6690307328605201
Weighted FalsePositiveRate: 0.10919639699425003
Kappa statistic: 0.5599316340937801
Training time: 21.0
		
Iteration: 11
Labeled set size: 284
Unlabelled set size: 139
	
Mean region size: 52.836879432624116
Incorrectly Classified Instances: 32.6241134751773
Correctly Classified Instances: 67.37588652482269
Weighted Precision: 0.6233659982665345
Weighted AreaUnderROC: 0.8558309430135771
Root mean squared error: 0.3457783216948933
Relative absolute error: 50.96926713947994
Root relative squared error: 79.85408284419347
Weighted TruePositiveRate: 0.6737588652482269
Weighted MatthewsCorrelation: 0.5383725972749062
Weighted FMeasure: 0.6050919799309817
Iteration time: 24.0
Weighted AreaUnderPRC: 0.6820808179920412
Mean absolute error: 0.19113475177304975
Coverage of cases: 88.88888888888889
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 181.0
Weighted Recall: 0.6737588652482269
Weighted FalsePositiveRate: 0.10788936296248161
Kappa statistic: 0.566086122694735
Training time: 23.0
		
Iteration: 12
Labeled set size: 304
Unlabelled set size: 119
	
Mean region size: 52.18676122931442
Incorrectly Classified Instances: 31.678486997635932
Correctly Classified Instances: 68.32151300236407
Weighted Precision: 0.6713263946283606
Weighted AreaUnderROC: 0.8663252312101664
Root mean squared error: 0.34010845162278497
Relative absolute error: 48.920409771473636
Root relative squared error: 78.5446824392327
Weighted TruePositiveRate: 0.6832151300236406
Weighted MatthewsCorrelation: 0.5599417578123341
Weighted FMeasure: 0.6131906964597347
Iteration time: 27.0
Weighted AreaUnderPRC: 0.7086910762519771
Mean absolute error: 0.18345153664302616
Coverage of cases: 89.83451536643027
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 208.0
Weighted Recall: 0.6832151300236406
Weighted FalsePositiveRate: 0.10512838159156347
Kappa statistic: 0.5785568236737425
Training time: 26.0
		
Iteration: 13
Labeled set size: 324
Unlabelled set size: 99
	
Mean region size: 50.709219858156025
Incorrectly Classified Instances: 31.914893617021278
Correctly Classified Instances: 68.08510638297872
Weighted Precision: 0.6905602582678767
Weighted AreaUnderROC: 0.8598933020852578
Root mean squared error: 0.3519785346990481
Relative absolute error: 50.52797478329402
Root relative squared error: 81.2859607029862
Weighted TruePositiveRate: 0.6808510638297872
Weighted MatthewsCorrelation: 0.5596606792644818
Weighted FMeasure: 0.6047891195478463
Iteration time: 30.0
Weighted AreaUnderPRC: 0.6877040600088873
Mean absolute error: 0.1894799054373526
Coverage of cases: 89.59810874704492
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 238.0
Weighted Recall: 0.6808510638297872
Weighted FalsePositiveRate: 0.1060644385096973
Kappa statistic: 0.5753012048192772
Training time: 29.0
		
Iteration: 14
Labeled set size: 344
Unlabelled set size: 79
	
Mean region size: 53.07328605200946
Incorrectly Classified Instances: 30.260047281323878
Correctly Classified Instances: 69.73995271867612
Weighted Precision: 0.7192213554258852
Weighted AreaUnderROC: 0.8846096505768508
Root mean squared error: 0.32706879287371265
Relative absolute error: 47.97478329393229
Root relative squared error: 75.53330224366559
Weighted TruePositiveRate: 0.6973995271867612
Weighted MatthewsCorrelation: 0.5916306129268978
Weighted FMeasure: 0.634260750800442
Iteration time: 32.0
Weighted AreaUnderPRC: 0.7316717689637413
Mean absolute error: 0.1799054373522461
Coverage of cases: 93.85342789598108
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 270.0
Weighted Recall: 0.6973995271867612
Weighted FalsePositiveRate: 0.1006671916980379
Kappa statistic: 0.5971968039994644
Training time: 32.0
		
Iteration: 15
Labeled set size: 364
Unlabelled set size: 59
	
Mean region size: 53.959810874704495
Incorrectly Classified Instances: 30.49645390070922
Correctly Classified Instances: 69.50354609929079
Weighted Precision: 0.7023547643033332
Weighted AreaUnderROC: 0.9053594852769308
Root mean squared error: 0.31068444231251025
Relative absolute error: 46.36721828211191
Root relative squared error: 71.74949856086262
Weighted TruePositiveRate: 0.6950354609929078
Weighted MatthewsCorrelation: 0.5921406037288688
Weighted FMeasure: 0.6573149926309749
Iteration time: 37.0
Weighted AreaUnderPRC: 0.7587445170798405
Mean absolute error: 0.17387706855791965
Coverage of cases: 97.39952718676123
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 307.0
Weighted Recall: 0.6950354609929078
Weighted FalsePositiveRate: 0.10157038071709527
Kappa statistic: 0.5938293200342402
Training time: 37.0
		
Iteration: 16
Labeled set size: 384
Unlabelled set size: 39
	
Mean region size: 57.033096926713945
Incorrectly Classified Instances: 29.078014184397162
Correctly Classified Instances: 70.92198581560284
Weighted Precision: 0.7044845499585594
Weighted AreaUnderROC: 0.8933307236083656
Root mean squared error: 0.3115772428316788
Relative absolute error: 49.29866036249018
Root relative squared error: 71.95568200889247
Weighted TruePositiveRate: 0.7092198581560284
Weighted MatthewsCorrelation: 0.6083531089949172
Weighted FMeasure: 0.6882308297009229
Iteration time: 41.0
Weighted AreaUnderPRC: 0.7385168084995869
Mean absolute error: 0.1848699763593382
Coverage of cases: 98.3451536643026
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 348.0
Weighted Recall: 0.7092198581560284
Weighted FalsePositiveRate: 0.09696227751618836
Kappa statistic: 0.6126315946215585
Training time: 41.0
		
Iteration: 17
Labeled set size: 404
Unlabelled set size: 19
	
Mean region size: 55.43735224586288
Incorrectly Classified Instances: 28.60520094562648
Correctly Classified Instances: 71.39479905437352
Weighted Precision: 0.7090685808699848
Weighted AreaUnderROC: 0.9146864313114915
Root mean squared error: 0.2928219268618111
Relative absolute error: 46.11505122143419
Root relative squared error: 67.62432731931662
Weighted TruePositiveRate: 0.7139479905437353
Weighted MatthewsCorrelation: 0.6151342378092289
Weighted FMeasure: 0.7011905926165148
Iteration time: 45.0
Weighted AreaUnderPRC: 0.7806513931497125
Mean absolute error: 0.17293144208037822
Coverage of cases: 98.58156028368795
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 393.0
Weighted Recall: 0.7139479905437353
Weighted FalsePositiveRate: 0.0961295954844874
Kappa statistic: 0.6186605473144636
Training time: 45.0
		
Time end:Wed Nov 01 14.45.06 EET 2017