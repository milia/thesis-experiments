Wed Nov 01 14.43.33 EET 2017
Dataset: vehicle
Test set size: 423
Initial Labelled set size: 84
Initial Unlabelled set size: 339
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Wed Nov 01 14.43.33 EET 2017
		
Iteration: 1
Labeled set size: 84
Unlabelled set size: 339
	
Mean region size: 75.82742316784869
Incorrectly Classified Instances: 49.645390070921984
Correctly Classified Instances: 50.354609929078016
Weighted Precision: 0.43132540263314467
Weighted AreaUnderROC: 0.7780686103774865
Root mean squared error: 0.39901540234117483
Relative absolute error: 83.425269240872
Root relative squared error: 92.14865998099366
Weighted TruePositiveRate: 0.5035460992907801
Weighted MatthewsCorrelation: 0.33938698297339054
Weighted FMeasure: 0.44146132617180617
Iteration time: 48.0
Weighted AreaUnderPRC: 0.497672739232112
Mean absolute error: 0.31284475965327
Coverage of cases: 95.50827423167848
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 48.0
Weighted Recall: 0.5035460992907801
Weighted FalsePositiveRate: 0.16614403012630197
Kappa statistic: 0.33828951975149535
Training time: 47.0
		
Iteration: 2
Labeled set size: 104
Unlabelled set size: 319
	
Mean region size: 75.23640661938535
Incorrectly Classified Instances: 42.78959810874704
Correctly Classified Instances: 57.21040189125296
Weighted Precision: 0.45691171531558356
Weighted AreaUnderROC: 0.7890137486848464
Root mean squared error: 0.3895216061935721
Relative absolute error: 81.37641187286569
Root relative squared error: 89.95616167641371
Weighted TruePositiveRate: 0.5721040189125296
Weighted MatthewsCorrelation: 0.4096842142693368
Weighted FMeasure: 0.49855811975572145
Iteration time: 48.0
Weighted AreaUnderPRC: 0.5187946167139449
Mean absolute error: 0.30516154452324634
Coverage of cases: 94.79905437352245
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 96.0
Weighted Recall: 0.5721040189125296
Weighted FalsePositiveRate: 0.14187332843114756
Kappa statistic: 0.43121508379888274
Training time: 47.0
		
Iteration: 3
Labeled set size: 124
Unlabelled set size: 299
	
Mean region size: 75.70921985815603
Incorrectly Classified Instances: 38.061465721040186
Correctly Classified Instances: 61.938534278959814
Weighted Precision: 0.4890083271637552
Weighted AreaUnderROC: 0.8326252445947712
Root mean squared error: 0.3757052732191349
Relative absolute error: 78.53953244024154
Root relative squared error: 86.76541625161177
Weighted TruePositiveRate: 0.6193853427895981
Weighted MatthewsCorrelation: 0.46596488934014013
Weighted FMeasure: 0.539324041632935
Iteration time: 49.0
Weighted AreaUnderPRC: 0.5764169635588144
Mean absolute error: 0.2945232466509058
Coverage of cases: 97.63593380614657
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 145.0
Weighted Recall: 0.6193853427895981
Weighted FalsePositiveRate: 0.12641181744819985
Kappa statistic: 0.4937972453674454
Training time: 48.0
		
Iteration: 4
Labeled set size: 144
Unlabelled set size: 279
	
Mean region size: 75.41371158392435
Incorrectly Classified Instances: 36.406619385342786
Correctly Classified Instances: 63.593380614657214
Weighted Precision: 0.5106213645431513
Weighted AreaUnderROC: 0.838944812389865
Root mean squared error: 0.37561786940889785
Relative absolute error: 78.48699763593379
Root relative squared error: 86.74523120626436
Weighted TruePositiveRate: 0.6359338061465721
Weighted MatthewsCorrelation: 0.4908074765732465
Weighted FMeasure: 0.5555823832836765
Iteration time: 48.0
Weighted AreaUnderPRC: 0.5955696417448124
Mean absolute error: 0.2943262411347517
Coverage of cases: 97.63593380614657
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 193.0
Weighted Recall: 0.6359338061465721
Weighted FalsePositiveRate: 0.1203802821992689
Kappa statistic: 0.5161836574025936
Training time: 48.0
		
Iteration: 5
Labeled set size: 164
Unlabelled set size: 259
	
Mean region size: 75.35460992907801
Incorrectly Classified Instances: 34.04255319148936
Correctly Classified Instances: 65.95744680851064
Weighted Precision: 0.7211171488521475
Weighted AreaUnderROC: 0.8512318764690653
Root mean squared error: 0.3695370846717924
Relative absolute error: 77.27869713685311
Root relative squared error: 85.34093412432355
Weighted TruePositiveRate: 0.6595744680851063
Weighted MatthewsCorrelation: 0.5562266724585874
Weighted FMeasure: 0.6093018424575619
Iteration time: 51.0
Weighted AreaUnderPRC: 0.6187952728530454
Mean absolute error: 0.28979511426319915
Coverage of cases: 98.3451536643026
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 244.0
Weighted Recall: 0.6595744680851063
Weighted FalsePositiveRate: 0.11335984541726986
Kappa statistic: 0.546940779197596
Training time: 50.0
		
Iteration: 6
Labeled set size: 184
Unlabelled set size: 239
	
Mean region size: 75.29550827423168
Incorrectly Classified Instances: 30.023640661938533
Correctly Classified Instances: 69.97635933806147
Weighted Precision: 0.6832632676996973
Weighted AreaUnderROC: 0.8447301677491071
Root mean squared error: 0.36281196099889823
Relative absolute error: 75.91279222484891
Root relative squared error: 83.78783333917197
Weighted TruePositiveRate: 0.6997635933806147
Weighted MatthewsCorrelation: 0.5912248151462735
Weighted FMeasure: 0.6850009921104704
Iteration time: 50.0
Weighted AreaUnderPRC: 0.6071125708288633
Mean absolute error: 0.28467297084318344
Coverage of cases: 97.87234042553192
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 294.0
Weighted Recall: 0.6997635933806147
Weighted FalsePositiveRate: 0.10049112742741706
Kappa statistic: 0.6000163802602972
Training time: 49.0
		
Iteration: 7
Labeled set size: 204
Unlabelled set size: 219
	
Mean region size: 75.29550827423168
Incorrectly Classified Instances: 32.860520094562645
Correctly Classified Instances: 67.13947990543736
Weighted Precision: 0.6555002616652424
Weighted AreaUnderROC: 0.8208530527313221
Root mean squared error: 0.36873654605865147
Relative absolute error: 77.06855791962174
Root relative squared error: 85.15605765080612
Weighted TruePositiveRate: 0.6713947990543735
Weighted MatthewsCorrelation: 0.5493690016996214
Weighted FMeasure: 0.6306744463266203
Iteration time: 52.0
Weighted AreaUnderPRC: 0.5812577457110973
Mean absolute error: 0.2890070921985815
Coverage of cases: 96.92671394799055
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 346.0
Weighted Recall: 0.6713947990543735
Weighted FalsePositiveRate: 0.11089578744791218
Kappa statistic: 0.5617985064615659
Training time: 51.0
		
Iteration: 8
Labeled set size: 224
Unlabelled set size: 199
	
Mean region size: 75.29550827423168
Incorrectly Classified Instances: 30.023640661938533
Correctly Classified Instances: 69.97635933806147
Weighted Precision: 0.6939317718549977
Weighted AreaUnderROC: 0.83640371270428
Root mean squared error: 0.3650224615359911
Relative absolute error: 76.38560546361963
Root relative squared error: 84.29832657122572
Weighted TruePositiveRate: 0.6997635933806147
Weighted MatthewsCorrelation: 0.5927935702918061
Weighted FMeasure: 0.6700217258867829
Iteration time: 53.0
Weighted AreaUnderPRC: 0.6162190883079426
Mean absolute error: 0.2864460204885736
Coverage of cases: 96.6903073286052
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 399.0
Weighted Recall: 0.6997635933806147
Weighted FalsePositiveRate: 0.10131276288942437
Kappa statistic: 0.5995900570193419
Training time: 52.0
		
Iteration: 9
Labeled set size: 244
Unlabelled set size: 179
	
Mean region size: 75.177304964539
Incorrectly Classified Instances: 29.314420803782507
Correctly Classified Instances: 70.68557919621749
Weighted Precision: 0.6949593433426093
Weighted AreaUnderROC: 0.8392661718626789
Root mean squared error: 0.3633545514730412
Relative absolute error: 76.01786183346464
Root relative squared error: 83.91313924169442
Weighted TruePositiveRate: 0.706855791962175
Weighted MatthewsCorrelation: 0.6010748686863309
Weighted FMeasure: 0.6839432063255794
Iteration time: 54.0
Weighted AreaUnderPRC: 0.6164732820548557
Mean absolute error: 0.2850669818754924
Coverage of cases: 96.6903073286052
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 453.0
Weighted Recall: 0.706855791962175
Weighted FalsePositiveRate: 0.09870012512707578
Kappa statistic: 0.6092116732851044
Training time: 53.0
		
Iteration: 10
Labeled set size: 264
Unlabelled set size: 159
	
Mean region size: 75.53191489361703
Incorrectly Classified Instances: 31.44208037825059
Correctly Classified Instances: 68.5579196217494
Weighted Precision: 0.6854270944701559
Weighted AreaUnderROC: 0.8420829262568017
Root mean squared error: 0.3644823540220914
Relative absolute error: 76.28053585500392
Root relative squared error: 84.17359408380918
Weighted TruePositiveRate: 0.6855791962174941
Weighted MatthewsCorrelation: 0.5733129497466726
Weighted FMeasure: 0.6462925240701451
Iteration time: 54.0
Weighted AreaUnderPRC: 0.6030119131028163
Mean absolute error: 0.2860520094562647
Coverage of cases: 96.92671394799055
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 507.0
Weighted Recall: 0.6855791962174941
Weighted FalsePositiveRate: 0.10668222306573047
Kappa statistic: 0.5804854405130309
Training time: 54.0
		
Iteration: 11
Labeled set size: 284
Unlabelled set size: 139
	
Mean region size: 75.11820330969267
Incorrectly Classified Instances: 28.13238770685579
Correctly Classified Instances: 71.8676122931442
Weighted Precision: 0.7119249551705229
Weighted AreaUnderROC: 0.847460139468956
Root mean squared error: 0.3621325796631392
Relative absolute error: 75.80772261623324
Root relative squared error: 83.63093694033883
Weighted TruePositiveRate: 0.7186761229314421
Weighted MatthewsCorrelation: 0.619157567762805
Weighted FMeasure: 0.6976948488046567
Iteration time: 56.0
Weighted AreaUnderPRC: 0.6267833413016319
Mean absolute error: 0.28427895981087464
Coverage of cases: 96.92671394799055
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 563.0
Weighted Recall: 0.7186761229314421
Weighted FalsePositiveRate: 0.09458901332456447
Kappa statistic: 0.6249776493026583
Training time: 56.0
		
Iteration: 12
Labeled set size: 304
Unlabelled set size: 119
	
Mean region size: 75.35460992907801
Incorrectly Classified Instances: 29.55082742316785
Correctly Classified Instances: 70.44917257683215
Weighted Precision: 0.6897019098923528
Weighted AreaUnderROC: 0.8445484177527091
Root mean squared error: 0.362495074828637
Relative absolute error: 75.91279222484893
Root relative squared error: 83.71465161289085
Weighted TruePositiveRate: 0.7044917257683215
Weighted MatthewsCorrelation: 0.5971031698863117
Weighted FMeasure: 0.6822330112360946
Iteration time: 59.0
Weighted AreaUnderPRC: 0.6171644660954285
Mean absolute error: 0.2846729708431835
Coverage of cases: 97.39952718676123
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 622.0
Weighted Recall: 0.7044917257683215
Weighted FalsePositiveRate: 0.09926390657699088
Kappa statistic: 0.6061159117997617
Training time: 58.0
		
Iteration: 13
Labeled set size: 324
Unlabelled set size: 99
	
Mean region size: 75.177304964539
Incorrectly Classified Instances: 28.368794326241133
Correctly Classified Instances: 71.63120567375887
Weighted Precision: 0.7003534562170768
Weighted AreaUnderROC: 0.8479362826441741
Root mean squared error: 0.36140649857245416
Relative absolute error: 75.65011820330965
Root relative squared error: 83.46325569507461
Weighted TruePositiveRate: 0.7163120567375887
Weighted MatthewsCorrelation: 0.6133035636751566
Weighted FMeasure: 0.6999550873030214
Iteration time: 59.0
Weighted AreaUnderPRC: 0.6222524160161087
Mean absolute error: 0.2836879432624112
Coverage of cases: 97.63593380614657
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 681.0
Weighted Recall: 0.7163120567375887
Weighted FalsePositiveRate: 0.09500997445075683
Kappa statistic: 0.6220064339330394
Training time: 59.0
		
Iteration: 14
Labeled set size: 344
Unlabelled set size: 79
	
Mean region size: 75.65011820330969
Incorrectly Classified Instances: 29.314420803782507
Correctly Classified Instances: 70.68557919621749
Weighted Precision: 0.6958606145831018
Weighted AreaUnderROC: 0.847533369834221
Root mean squared error: 0.3614973384629355
Relative absolute error: 75.75518781192538
Root relative squared error: 83.48423426916364
Weighted TruePositiveRate: 0.706855791962175
Weighted MatthewsCorrelation: 0.6020111001968292
Weighted FMeasure: 0.6868835466880568
Iteration time: 62.0
Weighted AreaUnderPRC: 0.61877356394163
Mean absolute error: 0.28408195429472016
Coverage of cases: 97.87234042553192
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 743.0
Weighted Recall: 0.706855791962175
Weighted FalsePositiveRate: 0.09860183150436604
Kappa statistic: 0.6091679954696512
Training time: 61.0
		
Iteration: 15
Labeled set size: 364
Unlabelled set size: 59
	
Mean region size: 75.59101654846336
Incorrectly Classified Instances: 28.60520094562648
Correctly Classified Instances: 71.39479905437352
Weighted Precision: 0.7025486091286833
Weighted AreaUnderROC: 0.8466337402214161
Root mean squared error: 0.3609519561984354
Relative absolute error: 75.5975833990018
Root relative squared error: 83.35828363027547
Weighted TruePositiveRate: 0.7139479905437353
Weighted MatthewsCorrelation: 0.6115944368290963
Weighted FMeasure: 0.6956802231269866
Iteration time: 62.0
Weighted AreaUnderPRC: 0.619259867581561
Mean absolute error: 0.2834909377462568
Coverage of cases: 97.87234042553192
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 805.0
Weighted Recall: 0.7139479905437353
Weighted FalsePositiveRate: 0.09601145709252397
Kappa statistic: 0.6187429235444849
Training time: 61.0
		
Iteration: 16
Labeled set size: 384
Unlabelled set size: 39
	
Mean region size: 75.41371158392435
Incorrectly Classified Instances: 28.60520094562648
Correctly Classified Instances: 71.39479905437352
Weighted Precision: 0.7019720150700409
Weighted AreaUnderROC: 0.8457305344158184
Root mean squared error: 0.3615881555321896
Relative absolute error: 75.70265300761751
Root relative squared error: 83.50520757291598
Weighted TruePositiveRate: 0.7139479905437353
Weighted MatthewsCorrelation: 0.6115644181333998
Weighted FMeasure: 0.6985408362737257
Iteration time: 61.0
Weighted AreaUnderPRC: 0.6209081650275348
Mean absolute error: 0.2838849487785657
Coverage of cases: 97.63593380614657
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 866.0
Weighted Recall: 0.7139479905437353
Weighted FalsePositiveRate: 0.09601933298532152
Kappa statistic: 0.6187940357201377
Training time: 61.0
		
Iteration: 17
Labeled set size: 404
Unlabelled set size: 19
	
Mean region size: 75.47281323877068
Incorrectly Classified Instances: 29.078014184397162
Correctly Classified Instances: 70.92198581560284
Weighted Precision: 0.6957027901737013
Weighted AreaUnderROC: 0.8479716063627671
Root mean squared error: 0.36049684070259647
Relative absolute error: 75.49251379038608
Root relative squared error: 83.25317920866148
Weighted TruePositiveRate: 0.7092198581560284
Weighted MatthewsCorrelation: 0.6047560722394781
Weighted FMeasure: 0.6939687378517639
Iteration time: 65.0
Weighted AreaUnderPRC: 0.6185877976612473
Mean absolute error: 0.2830969267139478
Coverage of cases: 98.3451536643026
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 931.0
Weighted Recall: 0.7092198581560284
Weighted FalsePositiveRate: 0.09750205523988915
Kappa statistic: 0.6125104265967588
Training time: 65.0
		
Time end:Wed Nov 01 14.43.35 EET 2017