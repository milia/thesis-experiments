Wed Nov 01 14.44.48 EET 2017
Dataset: vehicle
Test set size: 423
Initial Labelled set size: 84
Initial Unlabelled set size: 339
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Wed Nov 01 14.44.48 EET 2017
		
Iteration: 1
Labeled set size: 84
Unlabelled set size: 339
	
Mean region size: 25.0
Incorrectly Classified Instances: 41.843971631205676
Correctly Classified Instances: 58.156028368794324
Weighted Precision: 0.6029012915689265
Weighted AreaUnderROC: 0.7200659650662379
Root mean squared error: 0.44732232371936775
Relative absolute error: 57.80141843971628
Root relative squared error: 103.30466560556236
Weighted TruePositiveRate: 0.5815602836879432
Weighted MatthewsCorrelation: 0.45012243292759796
Weighted FMeasure: 0.5889077163088343
Iteration time: 25.0
Weighted AreaUnderPRC: 0.4758243618064405
Mean absolute error: 0.21675531914893603
Coverage of cases: 58.156028368794324
Instances selection time: 22.0
Test time: 25.0
Accumulative iteration time: 25.0
Weighted Recall: 0.5815602836879432
Weighted FalsePositiveRate: 0.14142835355546757
Kappa statistic: 0.44116944572989586
Training time: 3.0
		
Iteration: 2
Labeled set size: 104
Unlabelled set size: 319
	
Mean region size: 25.0
Incorrectly Classified Instances: 39.716312056737586
Correctly Classified Instances: 60.283687943262414
Weighted Precision: 0.6023061445255536
Weighted AreaUnderROC: 0.7348529378168042
Root mean squared error: 0.43758872166302476
Relative absolute error: 54.69748708519371
Root relative squared error: 101.05678649859662
Weighted TruePositiveRate: 0.6028368794326241
Weighted MatthewsCorrelation: 0.46925747261858186
Weighted FMeasure: 0.6017849100310892
Iteration time: 12.0
Weighted AreaUnderPRC: 0.4897238117913666
Mean absolute error: 0.2051155765694764
Coverage of cases: 60.283687943262414
Instances selection time: 12.0
Test time: 16.0
Accumulative iteration time: 37.0
Weighted Recall: 0.6028368794326241
Weighted FalsePositiveRate: 0.1331310037990156
Kappa statistic: 0.470438320640267
Training time: 0.0
		
Iteration: 3
Labeled set size: 124
Unlabelled set size: 299
	
Mean region size: 25.0
Incorrectly Classified Instances: 37.825059101654844
Correctly Classified Instances: 62.174940898345156
Weighted Precision: 0.6137730994333334
Weighted AreaUnderROC: 0.7479789404581231
Root mean squared error: 0.4282502019758901
Relative absolute error: 51.98236800630417
Root relative squared error: 98.90014775651672
Weighted TruePositiveRate: 0.6217494089834515
Weighted MatthewsCorrelation: 0.4916316186740685
Weighted FMeasure: 0.6134867345247598
Iteration time: 13.0
Weighted AreaUnderPRC: 0.5017483813562159
Mean absolute error: 0.19493388002364065
Coverage of cases: 62.174940898345156
Instances selection time: 12.0
Test time: 18.0
Accumulative iteration time: 50.0
Weighted Recall: 0.6217494089834515
Weighted FalsePositiveRate: 0.1257915280672054
Kappa statistic: 0.496312393483616
Training time: 1.0
		
Iteration: 4
Labeled set size: 144
Unlabelled set size: 279
	
Mean region size: 25.0
Incorrectly Classified Instances: 37.35224586288416
Correctly Classified Instances: 62.64775413711584
Weighted Precision: 0.6192107223881482
Weighted AreaUnderROC: 0.7512322672552688
Root mean squared error: 0.42643943275422846
Relative absolute error: 51.15967030860637
Root relative squared error: 98.48196851749006
Weighted TruePositiveRate: 0.6264775413711584
Weighted MatthewsCorrelation: 0.4983857019588832
Weighted FMeasure: 0.6174253302726813
Iteration time: 14.0
Weighted AreaUnderPRC: 0.506909714000822
Mean absolute error: 0.1918487636572739
Coverage of cases: 62.64775413711584
Instances selection time: 13.0
Test time: 21.0
Accumulative iteration time: 64.0
Weighted Recall: 0.6264775413711584
Weighted FalsePositiveRate: 0.1240130068606209
Kappa statistic: 0.5027269142342691
Training time: 1.0
		
Iteration: 5
Labeled set size: 164
Unlabelled set size: 259
	
Mean region size: 25.0
Incorrectly Classified Instances: 36.170212765957444
Correctly Classified Instances: 63.829787234042556
Weighted Precision: 0.6287796580928101
Weighted AreaUnderROC: 0.7592224146716117
Root mean squared error: 0.4202991537423801
Relative absolute error: 49.4596420128334
Root relative squared error: 97.06393182133402
Weighted TruePositiveRate: 0.6382978723404256
Weighted MatthewsCorrelation: 0.5133180514205016
Weighted FMeasure: 0.6254461928516799
Iteration time: 13.0
Weighted AreaUnderPRC: 0.5179624158100777
Mean absolute error: 0.18547365754812525
Coverage of cases: 63.829787234042556
Instances selection time: 13.0
Test time: 22.0
Accumulative iteration time: 77.0
Weighted Recall: 0.6382978723404256
Weighted FalsePositiveRate: 0.11985304299720227
Kappa statistic: 0.5186675393059543
Training time: 0.0
		
Iteration: 6
Labeled set size: 184
Unlabelled set size: 239
	
Mean region size: 25.0
Incorrectly Classified Instances: 34.04255319148936
Correctly Classified Instances: 65.95744680851064
Weighted Precision: 0.6602760625031195
Weighted AreaUnderROC: 0.7736290473230117
Root mean squared error: 0.4082598410103553
Relative absolute error: 46.551984306624384
Root relative squared error: 94.28357164265698
Weighted TruePositiveRate: 0.6595744680851063
Weighted MatthewsCorrelation: 0.5457816355672332
Weighted FMeasure: 0.6445675230020899
Iteration time: 14.0
Weighted AreaUnderPRC: 0.533824242621423
Mean absolute error: 0.17456994114984145
Coverage of cases: 65.95744680851064
Instances selection time: 13.0
Test time: 24.0
Accumulative iteration time: 91.0
Weighted Recall: 0.6595744680851063
Weighted FalsePositiveRate: 0.1123163734390826
Kappa statistic: 0.5471260436725377
Training time: 1.0
		
Iteration: 7
Labeled set size: 204
Unlabelled set size: 219
	
Mean region size: 25.0
Incorrectly Classified Instances: 36.406619385342786
Correctly Classified Instances: 63.593380614657214
Weighted Precision: 0.6422636035181197
Weighted AreaUnderROC: 0.7578436264183704
Root mean squared error: 0.4226130643547716
Relative absolute error: 49.53173304237126
Root relative squared error: 97.59830658731201
Weighted TruePositiveRate: 0.6359338061465721
Weighted MatthewsCorrelation: 0.5165129488482437
Weighted FMeasure: 0.6174827284621027
Iteration time: 13.0
Weighted AreaUnderPRC: 0.5197755702557527
Mean absolute error: 0.18574399890889223
Coverage of cases: 63.593380614657214
Instances selection time: 12.0
Test time: 26.0
Accumulative iteration time: 104.0
Weighted Recall: 0.6359338061465721
Weighted FalsePositiveRate: 0.12024655330983104
Kappa statistic: 0.5157196702177486
Training time: 1.0
		
Iteration: 8
Labeled set size: 224
Unlabelled set size: 199
	
Mean region size: 25.0
Incorrectly Classified Instances: 36.406619385342786
Correctly Classified Instances: 63.593380614657214
Weighted Precision: 0.6471224830047553
Weighted AreaUnderROC: 0.7576836434521602
Root mean squared error: 0.42296246155820116
Relative absolute error: 49.44492831764229
Root relative squared error: 97.67899641509368
Weighted TruePositiveRate: 0.6359338061465721
Weighted MatthewsCorrelation: 0.5183251791646435
Weighted FMeasure: 0.614174322100897
Iteration time: 12.0
Weighted AreaUnderPRC: 0.5251509888525248
Mean absolute error: 0.18541848119115859
Coverage of cases: 63.593380614657214
Instances selection time: 12.0
Test time: 28.0
Accumulative iteration time: 116.0
Weighted Recall: 0.6359338061465721
Weighted FalsePositiveRate: 0.12056651924225159
Kappa statistic: 0.5156404517774424
Training time: 0.0
		
Iteration: 9
Labeled set size: 244
Unlabelled set size: 179
	
Mean region size: 25.0
Incorrectly Classified Instances: 36.406619385342786
Correctly Classified Instances: 63.593380614657214
Weighted Precision: 0.6561751793710073
Weighted AreaUnderROC: 0.7576509684877308
Root mean squared error: 0.4232562539994478
Relative absolute error: 49.372124354965784
Root relative squared error: 97.74684487310952
Weighted TruePositiveRate: 0.6359338061465721
Weighted MatthewsCorrelation: 0.5223422347279182
Weighted FMeasure: 0.6149030896089324
Iteration time: 12.0
Weighted AreaUnderPRC: 0.5311379059944218
Mean absolute error: 0.18514546633112167
Coverage of cases: 63.593380614657214
Instances selection time: 12.0
Test time: 30.0
Accumulative iteration time: 128.0
Weighted Recall: 0.6359338061465721
Weighted FalsePositiveRate: 0.12063186917111027
Kappa statistic: 0.5156332488158883
Training time: 0.0
		
Iteration: 10
Labeled set size: 264
Unlabelled set size: 159
	
Mean region size: 25.0
Incorrectly Classified Instances: 36.87943262411348
Correctly Classified Instances: 63.12056737588652
Weighted Precision: 0.6497164167628693
Weighted AreaUnderROC: 0.754337745924527
Root mean squared error: 0.42624727028355325
Relative absolute error: 49.93119508838776
Root relative squared error: 98.43759049582106
Weighted TruePositiveRate: 0.6312056737588653
Weighted MatthewsCorrelation: 0.5156638235403215
Weighted FMeasure: 0.6212194849734574
Iteration time: 12.0
Weighted AreaUnderPRC: 0.5319120881834383
Mean absolute error: 0.18724198158145408
Coverage of cases: 63.12056737588652
Instances selection time: 11.0
Test time: 31.0
Accumulative iteration time: 140.0
Weighted Recall: 0.6312056737588653
Weighted FalsePositiveRate: 0.12253018190981124
Kappa statistic: 0.5091931453052482
Training time: 1.0
		
Iteration: 11
Labeled set size: 284
Unlabelled set size: 139
	
Mean region size: 25.0
Incorrectly Classified Instances: 37.825059101654844
Correctly Classified Instances: 62.174940898345156
Weighted Precision: 0.6465087709768561
Weighted AreaUnderROC: 0.7477704524511579
Root mean squared error: 0.4318966592535339
Relative absolute error: 51.121836966990315
Root relative squared error: 99.74226099285114
Weighted TruePositiveRate: 0.6217494089834515
Weighted MatthewsCorrelation: 0.5055276308598158
Weighted FMeasure: 0.6240496144032647
Iteration time: 11.0
Weighted AreaUnderPRC: 0.5247924890164968
Mean absolute error: 0.19170688862621368
Coverage of cases: 62.174940898345156
Instances selection time: 10.0
Test time: 33.0
Accumulative iteration time: 151.0
Weighted Recall: 0.6217494089834515
Weighted FalsePositiveRate: 0.12620850408113596
Kappa statistic: 0.49623741151776346
Training time: 1.0
		
Iteration: 12
Labeled set size: 304
Unlabelled set size: 119
	
Mean region size: 25.0
Incorrectly Classified Instances: 37.35224586288416
Correctly Classified Instances: 62.64775413711584
Weighted Precision: 0.6546591427034085
Weighted AreaUnderROC: 0.7508507096288218
Root mean squared error: 0.42938023129131053
Relative absolute error: 50.45490364639287
Root relative squared error: 99.16111684829677
Weighted TruePositiveRate: 0.6264775413711584
Weighted MatthewsCorrelation: 0.5131331252151423
Weighted FMeasure: 0.6321265348209717
Iteration time: 10.0
Weighted AreaUnderPRC: 0.5303113953573352
Mean absolute error: 0.18920588867397328
Coverage of cases: 62.64775413711584
Instances selection time: 10.0
Test time: 34.0
Accumulative iteration time: 161.0
Weighted Recall: 0.6264775413711584
Weighted FalsePositiveRate: 0.12477612211351483
Kappa statistic: 0.5024344485638987
Training time: 0.0
		
Iteration: 13
Labeled set size: 324
Unlabelled set size: 99
	
Mean region size: 25.0
Incorrectly Classified Instances: 36.170212765957444
Correctly Classified Instances: 63.829787234042556
Weighted Precision: 0.6683177971353633
Weighted AreaUnderROC: 0.7586477583835719
Root mean squared error: 0.42269782142656454
Relative absolute error: 48.858329008822125
Root relative squared error: 97.61788039459816
Weighted TruePositiveRate: 0.6382978723404256
Weighted MatthewsCorrelation: 0.5296591689453659
Weighted FMeasure: 0.6461172769048693
Iteration time: 9.0
Weighted AreaUnderPRC: 0.5378000850316638
Mean absolute error: 0.18321873378308295
Coverage of cases: 63.829787234042556
Instances selection time: 8.0
Test time: 36.0
Accumulative iteration time: 170.0
Weighted Recall: 0.6382978723404256
Weighted FalsePositiveRate: 0.12100235557328173
Kappa statistic: 0.5180007745471878
Training time: 1.0
		
Iteration: 14
Labeled set size: 344
Unlabelled set size: 79
	
Mean region size: 25.0
Incorrectly Classified Instances: 35.46099290780142
Correctly Classified Instances: 64.53900709219859
Weighted Precision: 0.6727552227730503
Weighted AreaUnderROC: 0.7633403056111076
Root mean squared error: 0.41867857425185656
Relative absolute error: 47.88728567158509
Root relative squared error: 96.68967501929524
Weighted TruePositiveRate: 0.6453900709219859
Weighted MatthewsCorrelation: 0.538054484412285
Weighted FMeasure: 0.6532498229703005
Iteration time: 8.0
Weighted AreaUnderPRC: 0.5456284600556323
Mean absolute error: 0.17957732126844408
Coverage of cases: 64.53900709219859
Instances selection time: 7.0
Test time: 38.0
Accumulative iteration time: 178.0
Weighted Recall: 0.6453900709219859
Weighted FalsePositiveRate: 0.11870945969977072
Kappa statistic: 0.5273848629080915
Training time: 1.0
		
Iteration: 15
Labeled set size: 364
Unlabelled set size: 59
	
Mean region size: 25.0
Incorrectly Classified Instances: 33.56973995271868
Correctly Classified Instances: 66.43026004728132
Weighted Precision: 0.682851634330222
Weighted AreaUnderROC: 0.7758656486900386
Root mean squared error: 0.407487926227712
Relative absolute error: 45.36009182170148
Root relative squared error: 94.10530555963678
Weighted TruePositiveRate: 0.6643026004728132
Weighted MatthewsCorrelation: 0.5596418894290511
Weighted FMeasure: 0.6711480763260458
Iteration time: 6.0
Weighted AreaUnderPRC: 0.5656048422269143
Mean absolute error: 0.17010034433138058
Coverage of cases: 66.43026004728132
Instances selection time: 5.0
Test time: 39.0
Accumulative iteration time: 184.0
Weighted Recall: 0.6643026004728132
Weighted FalsePositiveRate: 0.11257130309273615
Kappa statistic: 0.5524176421934263
Training time: 1.0
		
Iteration: 16
Labeled set size: 384
Unlabelled set size: 39
	
Mean region size: 25.0
Incorrectly Classified Instances: 32.6241134751773
Correctly Classified Instances: 67.37588652482269
Weighted Precision: 0.6793532608413491
Weighted AreaUnderROC: 0.7821001929597823
Root mean squared error: 0.40181941188907927
Relative absolute error: 44.08130437961554
Root relative squared error: 92.79621824791082
Weighted TruePositiveRate: 0.6737588652482269
Weighted MatthewsCorrelation: 0.5666239282017739
Weighted FMeasure: 0.6762361286230509
Iteration time: 5.0
Weighted AreaUnderPRC: 0.5705226350763875
Mean absolute error: 0.16530489142355825
Coverage of cases: 67.37588652482269
Instances selection time: 4.0
Test time: 41.0
Accumulative iteration time: 189.0
Weighted Recall: 0.6737588652482269
Weighted FalsePositiveRate: 0.1095584793286625
Kappa statistic: 0.5648861789829902
Training time: 1.0
		
Iteration: 17
Labeled set size: 404
Unlabelled set size: 19
	
Mean region size: 25.0
Incorrectly Classified Instances: 32.860520094562645
Correctly Classified Instances: 67.13947990543736
Weighted Precision: 0.6757868613917042
Weighted AreaUnderROC: 0.7804927666502323
Root mean squared error: 0.40337304394183726
Relative absolute error: 44.364869667330844
Root relative squared error: 93.1550142014634
Weighted TruePositiveRate: 0.6713947990543735
Weighted MatthewsCorrelation: 0.5629294119864372
Weighted FMeasure: 0.6733634141291864
Iteration time: 3.0
Weighted AreaUnderPRC: 0.5656796898981923
Mean absolute error: 0.16636826125249066
Coverage of cases: 67.13947990543736
Instances selection time: 2.0
Test time: 43.0
Accumulative iteration time: 192.0
Weighted Recall: 0.6713947990543735
Weighted FalsePositiveRate: 0.11040926575390932
Kappa statistic: 0.5616939752210277
Training time: 1.0
		
Time end:Wed Nov 01 14.44.50 EET 2017