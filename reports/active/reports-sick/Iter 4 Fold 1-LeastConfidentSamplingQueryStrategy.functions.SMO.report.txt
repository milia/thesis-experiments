Wed Nov 01 08.36.28 EET 2017
Dataset: sick
Test set size: 1886
Initial Labelled set size: 377
Initial Unlabelled set size: 1509
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Wed Nov 01 08.36.28 EET 2017
		
Iteration: 1
Labeled set size: 377
Unlabelled set size: 1509
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.2036055143160125
Correctly Classified Instances: 93.79639448568399
Weighted Precision: 0.8817060224742375
Weighted AreaUnderROC: 0.49943534726143424
Root mean squared error: 0.24907038190672157
Relative absolute error: 12.407211028632027
Root relative squared error: 49.814076381344314
Weighted TruePositiveRate: 0.9379639448568399
Weighted MatthewsCorrelation: -0.008302601396688765
Weighted FMeasure: 0.9089653331553835
Iteration time: 53.0
Weighted AreaUnderPRC: 0.8854241161019386
Mean absolute error: 0.06203605514316013
Coverage of cases: 93.79639448568399
Instances selection time: 9.0
Test time: 11.0
Accumulative iteration time: 53.0
Weighted Recall: 0.9379639448568399
Weighted FalsePositiveRate: 0.9390932503339714
Kappa statistic: -0.002088991017338084
Training time: 44.0
		
Iteration: 2
Labeled set size: 397
Unlabelled set size: 1489
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.097560975609756
Correctly Classified Instances: 93.90243902439025
Weighted Precision: 0.9131308156864778
Weighted AreaUnderROC: 0.5081309994353472
Root mean squared error: 0.2469323991623974
Relative absolute error: 12.195121951219512
Root relative squared error: 49.38647983247948
Weighted TruePositiveRate: 0.9390243902439024
Weighted MatthewsCorrelation: 0.08458479909854655
Weighted FMeasure: 0.9115125876487483
Iteration time: 21.0
Weighted AreaUnderPRC: 0.8868256076795146
Mean absolute error: 0.06097560975609756
Coverage of cases: 93.90243902439025
Instances selection time: 7.0
Test time: 7.0
Accumulative iteration time: 74.0
Weighted Recall: 0.9390243902439024
Weighted FalsePositiveRate: 0.9227623913732079
Kappa statistic: 0.02963572751594918
Training time: 14.0
		
Iteration: 3
Labeled set size: 417
Unlabelled set size: 1469
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.150583244962885
Correctly Classified Instances: 93.84941675503711
Weighted Precision: 0.8817364300963965
Weighted AreaUnderROC: 0.4997176736307171
Root mean squared error: 0.24800369442737913
Relative absolute error: 12.30116648992577
Root relative squared error: 49.60073888547583
Weighted TruePositiveRate: 0.9384941675503712
Weighted MatthewsCorrelation: -0.005869268294291826
Weighted FMeasure: 0.9092303997438224
Iteration time: 23.0
Weighted AreaUnderPRC: 0.885454472233064
Mean absolute error: 0.061505832449628844
Coverage of cases: 93.84941675503711
Instances selection time: 6.0
Test time: 7.0
Accumulative iteration time: 97.0
Weighted Recall: 0.9384941675503712
Weighted FalsePositiveRate: 0.939058820288937
Kappa statistic: -0.0010524100189422039
Training time: 17.0
		
Iteration: 4
Labeled set size: 437
Unlabelled set size: 1449
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.150583244962885
Correctly Classified Instances: 93.84941675503711
Weighted Precision: 0.8817364300963965
Weighted AreaUnderROC: 0.4997176736307171
Root mean squared error: 0.24800369442737913
Relative absolute error: 12.30116648992577
Root relative squared error: 49.60073888547583
Weighted TruePositiveRate: 0.9384941675503712
Weighted MatthewsCorrelation: -0.005869268294291826
Weighted FMeasure: 0.9092303997438224
Iteration time: 24.0
Weighted AreaUnderPRC: 0.885454472233064
Mean absolute error: 0.061505832449628844
Coverage of cases: 93.84941675503711
Instances selection time: 6.0
Test time: 7.0
Accumulative iteration time: 121.0
Weighted Recall: 0.9384941675503712
Weighted FalsePositiveRate: 0.939058820288937
Kappa statistic: -0.0010524100189422039
Training time: 18.0
		
Iteration: 5
Labeled set size: 457
Unlabelled set size: 1429
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.097560975609756
Correctly Classified Instances: 93.90243902439025
Weighted Precision: 0.8817668054729327
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.2469323991623974
Relative absolute error: 12.195121951219512
Root relative squared error: 49.38647983247948
Weighted TruePositiveRate: 0.9390243902439024
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.909495321368308
Iteration time: 24.0
Weighted AreaUnderPRC: 0.8854848304580607
Mean absolute error: 0.06097560975609756
Coverage of cases: 93.90243902439025
Instances selection time: 6.0
Test time: 7.0
Accumulative iteration time: 145.0
Weighted Recall: 0.9390243902439024
Weighted FalsePositiveRate: 0.9390243902439024
Kappa statistic: 0.0
Training time: 18.0
		
Iteration: 6
Labeled set size: 477
Unlabelled set size: 1409
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.097560975609756
Correctly Classified Instances: 93.90243902439025
Weighted Precision: 0.8817668054729327
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.2469323991623974
Relative absolute error: 12.195121951219512
Root relative squared error: 49.38647983247948
Weighted TruePositiveRate: 0.9390243902439024
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.909495321368308
Iteration time: 28.0
Weighted AreaUnderPRC: 0.8854848304580607
Mean absolute error: 0.06097560975609756
Coverage of cases: 93.90243902439025
Instances selection time: 6.0
Test time: 7.0
Accumulative iteration time: 173.0
Weighted Recall: 0.9390243902439024
Weighted FalsePositiveRate: 0.9390243902439024
Kappa statistic: 0.0
Training time: 22.0
		
Iteration: 7
Labeled set size: 497
Unlabelled set size: 1389
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.097560975609756
Correctly Classified Instances: 93.90243902439025
Weighted Precision: 0.8817668054729327
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.2469323991623974
Relative absolute error: 12.195121951219512
Root relative squared error: 49.38647983247948
Weighted TruePositiveRate: 0.9390243902439024
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.909495321368308
Iteration time: 26.0
Weighted AreaUnderPRC: 0.8854848304580607
Mean absolute error: 0.06097560975609756
Coverage of cases: 93.90243902439025
Instances selection time: 6.0
Test time: 7.0
Accumulative iteration time: 199.0
Weighted Recall: 0.9390243902439024
Weighted FalsePositiveRate: 0.9390243902439024
Kappa statistic: 0.0
Training time: 20.0
		
Iteration: 8
Labeled set size: 517
Unlabelled set size: 1369
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.097560975609756
Correctly Classified Instances: 93.90243902439025
Weighted Precision: 0.8817668054729327
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.2469323991623974
Relative absolute error: 12.195121951219512
Root relative squared error: 49.38647983247948
Weighted TruePositiveRate: 0.9390243902439024
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.909495321368308
Iteration time: 25.0
Weighted AreaUnderPRC: 0.8854848304580607
Mean absolute error: 0.06097560975609756
Coverage of cases: 93.90243902439025
Instances selection time: 6.0
Test time: 7.0
Accumulative iteration time: 224.0
Weighted Recall: 0.9390243902439024
Weighted FalsePositiveRate: 0.9390243902439024
Kappa statistic: 0.0
Training time: 19.0
		
Iteration: 9
Labeled set size: 537
Unlabelled set size: 1349
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.097560975609756
Correctly Classified Instances: 93.90243902439025
Weighted Precision: 0.8817668054729327
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.2469323991623974
Relative absolute error: 12.195121951219512
Root relative squared error: 49.38647983247948
Weighted TruePositiveRate: 0.9390243902439024
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.909495321368308
Iteration time: 29.0
Weighted AreaUnderPRC: 0.8854848304580607
Mean absolute error: 0.06097560975609756
Coverage of cases: 93.90243902439025
Instances selection time: 6.0
Test time: 6.0
Accumulative iteration time: 253.0
Weighted Recall: 0.9390243902439024
Weighted FalsePositiveRate: 0.9390243902439024
Kappa statistic: 0.0
Training time: 23.0
		
Iteration: 10
Labeled set size: 557
Unlabelled set size: 1329
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.097560975609756
Correctly Classified Instances: 93.90243902439025
Weighted Precision: 0.8817668054729327
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.2469323991623974
Relative absolute error: 12.195121951219512
Root relative squared error: 49.38647983247948
Weighted TruePositiveRate: 0.9390243902439024
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.909495321368308
Iteration time: 27.0
Weighted AreaUnderPRC: 0.8854848304580607
Mean absolute error: 0.06097560975609756
Coverage of cases: 93.90243902439025
Instances selection time: 6.0
Test time: 7.0
Accumulative iteration time: 280.0
Weighted Recall: 0.9390243902439024
Weighted FalsePositiveRate: 0.9390243902439024
Kappa statistic: 0.0
Training time: 21.0
		
Iteration: 11
Labeled set size: 577
Unlabelled set size: 1309
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.097560975609756
Correctly Classified Instances: 93.90243902439025
Weighted Precision: 0.8817668054729327
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.2469323991623974
Relative absolute error: 12.195121951219512
Root relative squared error: 49.38647983247948
Weighted TruePositiveRate: 0.9390243902439024
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.909495321368308
Iteration time: 32.0
Weighted AreaUnderPRC: 0.8854848304580607
Mean absolute error: 0.06097560975609756
Coverage of cases: 93.90243902439025
Instances selection time: 6.0
Test time: 7.0
Accumulative iteration time: 312.0
Weighted Recall: 0.9390243902439024
Weighted FalsePositiveRate: 0.9390243902439024
Kappa statistic: 0.0
Training time: 26.0
		
Iteration: 12
Labeled set size: 597
Unlabelled set size: 1289
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.097560975609756
Correctly Classified Instances: 93.90243902439025
Weighted Precision: 0.8817668054729327
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.2469323991623974
Relative absolute error: 12.195121951219512
Root relative squared error: 49.38647983247948
Weighted TruePositiveRate: 0.9390243902439024
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.909495321368308
Iteration time: 32.0
Weighted AreaUnderPRC: 0.8854848304580607
Mean absolute error: 0.06097560975609756
Coverage of cases: 93.90243902439025
Instances selection time: 6.0
Test time: 7.0
Accumulative iteration time: 344.0
Weighted Recall: 0.9390243902439024
Weighted FalsePositiveRate: 0.9390243902439024
Kappa statistic: 0.0
Training time: 26.0
		
Iteration: 13
Labeled set size: 617
Unlabelled set size: 1269
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.097560975609756
Correctly Classified Instances: 93.90243902439025
Weighted Precision: 0.8817668054729327
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.2469323991623974
Relative absolute error: 12.195121951219512
Root relative squared error: 49.38647983247948
Weighted TruePositiveRate: 0.9390243902439024
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.909495321368308
Iteration time: 33.0
Weighted AreaUnderPRC: 0.8854848304580607
Mean absolute error: 0.06097560975609756
Coverage of cases: 93.90243902439025
Instances selection time: 5.0
Test time: 7.0
Accumulative iteration time: 377.0
Weighted Recall: 0.9390243902439024
Weighted FalsePositiveRate: 0.9390243902439024
Kappa statistic: 0.0
Training time: 28.0
		
Iteration: 14
Labeled set size: 637
Unlabelled set size: 1249
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.097560975609756
Correctly Classified Instances: 93.90243902439025
Weighted Precision: 0.8817668054729327
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.2469323991623974
Relative absolute error: 12.195121951219512
Root relative squared error: 49.38647983247948
Weighted TruePositiveRate: 0.9390243902439024
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.909495321368308
Iteration time: 32.0
Weighted AreaUnderPRC: 0.8854848304580607
Mean absolute error: 0.06097560975609756
Coverage of cases: 93.90243902439025
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 409.0
Weighted Recall: 0.9390243902439024
Weighted FalsePositiveRate: 0.9390243902439024
Kappa statistic: 0.0
Training time: 29.0
		
Iteration: 15
Labeled set size: 657
Unlabelled set size: 1229
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.097560975609756
Correctly Classified Instances: 93.90243902439025
Weighted Precision: 0.8817668054729327
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.2469323991623974
Relative absolute error: 12.195121951219512
Root relative squared error: 49.38647983247948
Weighted TruePositiveRate: 0.9390243902439024
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.909495321368308
Iteration time: 46.0
Weighted AreaUnderPRC: 0.8854848304580607
Mean absolute error: 0.06097560975609756
Coverage of cases: 93.90243902439025
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 455.0
Weighted Recall: 0.9390243902439024
Weighted FalsePositiveRate: 0.9390243902439024
Kappa statistic: 0.0
Training time: 43.0
		
Iteration: 16
Labeled set size: 677
Unlabelled set size: 1209
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.097560975609756
Correctly Classified Instances: 93.90243902439025
Weighted Precision: 0.8817668054729327
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.2469323991623974
Relative absolute error: 12.195121951219512
Root relative squared error: 49.38647983247948
Weighted TruePositiveRate: 0.9390243902439024
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.909495321368308
Iteration time: 52.0
Weighted AreaUnderPRC: 0.8854848304580607
Mean absolute error: 0.06097560975609756
Coverage of cases: 93.90243902439025
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 507.0
Weighted Recall: 0.9390243902439024
Weighted FalsePositiveRate: 0.9390243902439024
Kappa statistic: 0.0
Training time: 49.0
		
Iteration: 17
Labeled set size: 697
Unlabelled set size: 1189
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.097560975609756
Correctly Classified Instances: 93.90243902439025
Weighted Precision: 0.8817668054729327
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.2469323991623974
Relative absolute error: 12.195121951219512
Root relative squared error: 49.38647983247948
Weighted TruePositiveRate: 0.9390243902439024
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.909495321368308
Iteration time: 42.0
Weighted AreaUnderPRC: 0.8854848304580607
Mean absolute error: 0.06097560975609756
Coverage of cases: 93.90243902439025
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 549.0
Weighted Recall: 0.9390243902439024
Weighted FalsePositiveRate: 0.9390243902439024
Kappa statistic: 0.0
Training time: 39.0
		
Iteration: 18
Labeled set size: 717
Unlabelled set size: 1169
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.097560975609756
Correctly Classified Instances: 93.90243902439025
Weighted Precision: 0.8817668054729327
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.2469323991623974
Relative absolute error: 12.195121951219512
Root relative squared error: 49.38647983247948
Weighted TruePositiveRate: 0.9390243902439024
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.909495321368308
Iteration time: 52.0
Weighted AreaUnderPRC: 0.8854848304580607
Mean absolute error: 0.06097560975609756
Coverage of cases: 93.90243902439025
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 601.0
Weighted Recall: 0.9390243902439024
Weighted FalsePositiveRate: 0.9390243902439024
Kappa statistic: 0.0
Training time: 49.0
		
Iteration: 19
Labeled set size: 737
Unlabelled set size: 1149
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.097560975609756
Correctly Classified Instances: 93.90243902439025
Weighted Precision: 0.8817668054729327
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.2469323991623974
Relative absolute error: 12.195121951219512
Root relative squared error: 49.38647983247948
Weighted TruePositiveRate: 0.9390243902439024
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.909495321368308
Iteration time: 48.0
Weighted AreaUnderPRC: 0.8854848304580607
Mean absolute error: 0.06097560975609756
Coverage of cases: 93.90243902439025
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 649.0
Weighted Recall: 0.9390243902439024
Weighted FalsePositiveRate: 0.9390243902439024
Kappa statistic: 0.0
Training time: 45.0
		
Iteration: 20
Labeled set size: 757
Unlabelled set size: 1129
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.097560975609756
Correctly Classified Instances: 93.90243902439025
Weighted Precision: 0.8817668054729327
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.2469323991623974
Relative absolute error: 12.195121951219512
Root relative squared error: 49.38647983247948
Weighted TruePositiveRate: 0.9390243902439024
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.909495321368308
Iteration time: 57.0
Weighted AreaUnderPRC: 0.8854848304580607
Mean absolute error: 0.06097560975609756
Coverage of cases: 93.90243902439025
Instances selection time: 4.0
Test time: 6.0
Accumulative iteration time: 706.0
Weighted Recall: 0.9390243902439024
Weighted FalsePositiveRate: 0.9390243902439024
Kappa statistic: 0.0
Training time: 53.0
		
Iteration: 21
Labeled set size: 777
Unlabelled set size: 1109
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.097560975609756
Correctly Classified Instances: 93.90243902439025
Weighted Precision: 0.8817668054729327
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.2469323991623974
Relative absolute error: 12.195121951219512
Root relative squared error: 49.38647983247948
Weighted TruePositiveRate: 0.9390243902439024
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.909495321368308
Iteration time: 54.0
Weighted AreaUnderPRC: 0.8854848304580607
Mean absolute error: 0.06097560975609756
Coverage of cases: 93.90243902439025
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 760.0
Weighted Recall: 0.9390243902439024
Weighted FalsePositiveRate: 0.9390243902439024
Kappa statistic: 0.0
Training time: 51.0
		
Iteration: 22
Labeled set size: 797
Unlabelled set size: 1089
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.097560975609756
Correctly Classified Instances: 93.90243902439025
Weighted Precision: 0.8817668054729327
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.2469323991623974
Relative absolute error: 12.195121951219512
Root relative squared error: 49.38647983247948
Weighted TruePositiveRate: 0.9390243902439024
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.909495321368308
Iteration time: 56.0
Weighted AreaUnderPRC: 0.8854848304580607
Mean absolute error: 0.06097560975609756
Coverage of cases: 93.90243902439025
Instances selection time: 4.0
Test time: 7.0
Accumulative iteration time: 816.0
Weighted Recall: 0.9390243902439024
Weighted FalsePositiveRate: 0.9390243902439024
Kappa statistic: 0.0
Training time: 52.0
		
Iteration: 23
Labeled set size: 817
Unlabelled set size: 1069
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.097560975609756
Correctly Classified Instances: 93.90243902439025
Weighted Precision: 0.8817668054729327
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.2469323991623974
Relative absolute error: 12.195121951219512
Root relative squared error: 49.38647983247948
Weighted TruePositiveRate: 0.9390243902439024
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.909495321368308
Iteration time: 81.0
Weighted AreaUnderPRC: 0.8854848304580607
Mean absolute error: 0.06097560975609756
Coverage of cases: 93.90243902439025
Instances selection time: 4.0
Test time: 7.0
Accumulative iteration time: 897.0
Weighted Recall: 0.9390243902439024
Weighted FalsePositiveRate: 0.9390243902439024
Kappa statistic: 0.0
Training time: 77.0
		
Iteration: 24
Labeled set size: 837
Unlabelled set size: 1049
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.097560975609756
Correctly Classified Instances: 93.90243902439025
Weighted Precision: 0.8817668054729327
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.2469323991623974
Relative absolute error: 12.195121951219512
Root relative squared error: 49.38647983247948
Weighted TruePositiveRate: 0.9390243902439024
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.909495321368308
Iteration time: 50.0
Weighted AreaUnderPRC: 0.8854848304580607
Mean absolute error: 0.06097560975609756
Coverage of cases: 93.90243902439025
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 947.0
Weighted Recall: 0.9390243902439024
Weighted FalsePositiveRate: 0.9390243902439024
Kappa statistic: 0.0
Training time: 47.0
		
Iteration: 25
Labeled set size: 857
Unlabelled set size: 1029
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.097560975609756
Correctly Classified Instances: 93.90243902439025
Weighted Precision: 0.8817668054729327
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.2469323991623974
Relative absolute error: 12.195121951219512
Root relative squared error: 49.38647983247948
Weighted TruePositiveRate: 0.9390243902439024
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.909495321368308
Iteration time: 52.0
Weighted AreaUnderPRC: 0.8854848304580607
Mean absolute error: 0.06097560975609756
Coverage of cases: 93.90243902439025
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 999.0
Weighted Recall: 0.9390243902439024
Weighted FalsePositiveRate: 0.9390243902439024
Kappa statistic: 0.0
Training time: 49.0
		
Iteration: 26
Labeled set size: 877
Unlabelled set size: 1009
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.097560975609756
Correctly Classified Instances: 93.90243902439025
Weighted Precision: 0.8817668054729327
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.2469323991623974
Relative absolute error: 12.195121951219512
Root relative squared error: 49.38647983247948
Weighted TruePositiveRate: 0.9390243902439024
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.909495321368308
Iteration time: 73.0
Weighted AreaUnderPRC: 0.8854848304580607
Mean absolute error: 0.06097560975609756
Coverage of cases: 93.90243902439025
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 1072.0
Weighted Recall: 0.9390243902439024
Weighted FalsePositiveRate: 0.9390243902439024
Kappa statistic: 0.0
Training time: 70.0
		
Iteration: 27
Labeled set size: 897
Unlabelled set size: 989
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.097560975609756
Correctly Classified Instances: 93.90243902439025
Weighted Precision: 0.8817668054729327
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.2469323991623974
Relative absolute error: 12.195121951219512
Root relative squared error: 49.38647983247948
Weighted TruePositiveRate: 0.9390243902439024
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.909495321368308
Iteration time: 68.0
Weighted AreaUnderPRC: 0.8854848304580607
Mean absolute error: 0.06097560975609756
Coverage of cases: 93.90243902439025
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 1140.0
Weighted Recall: 0.9390243902439024
Weighted FalsePositiveRate: 0.9390243902439024
Kappa statistic: 0.0
Training time: 65.0
		
Iteration: 28
Labeled set size: 917
Unlabelled set size: 969
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.097560975609756
Correctly Classified Instances: 93.90243902439025
Weighted Precision: 0.8817668054729327
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.2469323991623974
Relative absolute error: 12.195121951219512
Root relative squared error: 49.38647983247948
Weighted TruePositiveRate: 0.9390243902439024
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.909495321368308
Iteration time: 79.0
Weighted AreaUnderPRC: 0.8854848304580607
Mean absolute error: 0.06097560975609756
Coverage of cases: 93.90243902439025
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 1219.0
Weighted Recall: 0.9390243902439024
Weighted FalsePositiveRate: 0.9390243902439024
Kappa statistic: 0.0
Training time: 76.0
		
Iteration: 29
Labeled set size: 937
Unlabelled set size: 949
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.097560975609756
Correctly Classified Instances: 93.90243902439025
Weighted Precision: 0.8817668054729327
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.2469323991623974
Relative absolute error: 12.195121951219512
Root relative squared error: 49.38647983247948
Weighted TruePositiveRate: 0.9390243902439024
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.909495321368308
Iteration time: 65.0
Weighted AreaUnderPRC: 0.8854848304580607
Mean absolute error: 0.06097560975609756
Coverage of cases: 93.90243902439025
Instances selection time: 4.0
Test time: 7.0
Accumulative iteration time: 1284.0
Weighted Recall: 0.9390243902439024
Weighted FalsePositiveRate: 0.9390243902439024
Kappa statistic: 0.0
Training time: 61.0
		
Iteration: 30
Labeled set size: 957
Unlabelled set size: 929
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.097560975609756
Correctly Classified Instances: 93.90243902439025
Weighted Precision: 0.8817668054729327
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.2469323991623974
Relative absolute error: 12.195121951219512
Root relative squared error: 49.38647983247948
Weighted TruePositiveRate: 0.9390243902439024
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.909495321368308
Iteration time: 76.0
Weighted AreaUnderPRC: 0.8854848304580607
Mean absolute error: 0.06097560975609756
Coverage of cases: 93.90243902439025
Instances selection time: 4.0
Test time: 7.0
Accumulative iteration time: 1360.0
Weighted Recall: 0.9390243902439024
Weighted FalsePositiveRate: 0.9390243902439024
Kappa statistic: 0.0
Training time: 72.0
		
Iteration: 31
Labeled set size: 977
Unlabelled set size: 909
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.097560975609756
Correctly Classified Instances: 93.90243902439025
Weighted Precision: 0.8817668054729327
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.2469323991623974
Relative absolute error: 12.195121951219512
Root relative squared error: 49.38647983247948
Weighted TruePositiveRate: 0.9390243902439024
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.909495321368308
Iteration time: 80.0
Weighted AreaUnderPRC: 0.8854848304580607
Mean absolute error: 0.06097560975609756
Coverage of cases: 93.90243902439025
Instances selection time: 3.0
Test time: 6.0
Accumulative iteration time: 1440.0
Weighted Recall: 0.9390243902439024
Weighted FalsePositiveRate: 0.9390243902439024
Kappa statistic: 0.0
Training time: 77.0
		
Iteration: 32
Labeled set size: 997
Unlabelled set size: 889
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.097560975609756
Correctly Classified Instances: 93.90243902439025
Weighted Precision: 0.8817668054729327
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.2469323991623974
Relative absolute error: 12.195121951219512
Root relative squared error: 49.38647983247948
Weighted TruePositiveRate: 0.9390243902439024
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.909495321368308
Iteration time: 66.0
Weighted AreaUnderPRC: 0.8854848304580607
Mean absolute error: 0.06097560975609756
Coverage of cases: 93.90243902439025
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 1506.0
Weighted Recall: 0.9390243902439024
Weighted FalsePositiveRate: 0.9390243902439024
Kappa statistic: 0.0
Training time: 64.0
		
Iteration: 33
Labeled set size: 1017
Unlabelled set size: 869
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.097560975609756
Correctly Classified Instances: 93.90243902439025
Weighted Precision: 0.8817668054729327
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.2469323991623974
Relative absolute error: 12.195121951219512
Root relative squared error: 49.38647983247948
Weighted TruePositiveRate: 0.9390243902439024
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.909495321368308
Iteration time: 71.0
Weighted AreaUnderPRC: 0.8854848304580607
Mean absolute error: 0.06097560975609756
Coverage of cases: 93.90243902439025
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 1577.0
Weighted Recall: 0.9390243902439024
Weighted FalsePositiveRate: 0.9390243902439024
Kappa statistic: 0.0
Training time: 69.0
		
Iteration: 34
Labeled set size: 1037
Unlabelled set size: 849
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.097560975609756
Correctly Classified Instances: 93.90243902439025
Weighted Precision: 0.8817668054729327
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.2469323991623974
Relative absolute error: 12.195121951219512
Root relative squared error: 49.38647983247948
Weighted TruePositiveRate: 0.9390243902439024
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.909495321368308
Iteration time: 84.0
Weighted AreaUnderPRC: 0.8854848304580607
Mean absolute error: 0.06097560975609756
Coverage of cases: 93.90243902439025
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 1661.0
Weighted Recall: 0.9390243902439024
Weighted FalsePositiveRate: 0.9390243902439024
Kappa statistic: 0.0
Training time: 81.0
		
Iteration: 35
Labeled set size: 1057
Unlabelled set size: 829
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.097560975609756
Correctly Classified Instances: 93.90243902439025
Weighted Precision: 0.8817668054729327
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.2469323991623974
Relative absolute error: 12.195121951219512
Root relative squared error: 49.38647983247948
Weighted TruePositiveRate: 0.9390243902439024
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.909495321368308
Iteration time: 64.0
Weighted AreaUnderPRC: 0.8854848304580607
Mean absolute error: 0.06097560975609756
Coverage of cases: 93.90243902439025
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 1725.0
Weighted Recall: 0.9390243902439024
Weighted FalsePositiveRate: 0.9390243902439024
Kappa statistic: 0.0
Training time: 61.0
		
Iteration: 36
Labeled set size: 1077
Unlabelled set size: 809
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.097560975609756
Correctly Classified Instances: 93.90243902439025
Weighted Precision: 0.8817668054729327
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.2469323991623974
Relative absolute error: 12.195121951219512
Root relative squared error: 49.38647983247948
Weighted TruePositiveRate: 0.9390243902439024
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.909495321368308
Iteration time: 83.0
Weighted AreaUnderPRC: 0.8854848304580607
Mean absolute error: 0.06097560975609756
Coverage of cases: 93.90243902439025
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 1808.0
Weighted Recall: 0.9390243902439024
Weighted FalsePositiveRate: 0.9390243902439024
Kappa statistic: 0.0
Training time: 80.0
		
Iteration: 37
Labeled set size: 1097
Unlabelled set size: 789
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.097560975609756
Correctly Classified Instances: 93.90243902439025
Weighted Precision: 0.8817668054729327
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.2469323991623974
Relative absolute error: 12.195121951219512
Root relative squared error: 49.38647983247948
Weighted TruePositiveRate: 0.9390243902439024
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.909495321368308
Iteration time: 101.0
Weighted AreaUnderPRC: 0.8854848304580607
Mean absolute error: 0.06097560975609756
Coverage of cases: 93.90243902439025
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 1909.0
Weighted Recall: 0.9390243902439024
Weighted FalsePositiveRate: 0.9390243902439024
Kappa statistic: 0.0
Training time: 99.0
		
Iteration: 38
Labeled set size: 1117
Unlabelled set size: 769
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.097560975609756
Correctly Classified Instances: 93.90243902439025
Weighted Precision: 0.8817668054729327
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.2469323991623974
Relative absolute error: 12.195121951219512
Root relative squared error: 49.38647983247948
Weighted TruePositiveRate: 0.9390243902439024
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.909495321368308
Iteration time: 94.0
Weighted AreaUnderPRC: 0.8854848304580607
Mean absolute error: 0.06097560975609756
Coverage of cases: 93.90243902439025
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 2003.0
Weighted Recall: 0.9390243902439024
Weighted FalsePositiveRate: 0.9390243902439024
Kappa statistic: 0.0
Training time: 91.0
		
Iteration: 39
Labeled set size: 1137
Unlabelled set size: 749
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.097560975609756
Correctly Classified Instances: 93.90243902439025
Weighted Precision: 0.8817668054729327
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.2469323991623974
Relative absolute error: 12.195121951219512
Root relative squared error: 49.38647983247948
Weighted TruePositiveRate: 0.9390243902439024
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.909495321368308
Iteration time: 88.0
Weighted AreaUnderPRC: 0.8854848304580607
Mean absolute error: 0.06097560975609756
Coverage of cases: 93.90243902439025
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 2091.0
Weighted Recall: 0.9390243902439024
Weighted FalsePositiveRate: 0.9390243902439024
Kappa statistic: 0.0
Training time: 86.0
		
Iteration: 40
Labeled set size: 1157
Unlabelled set size: 729
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.097560975609756
Correctly Classified Instances: 93.90243902439025
Weighted Precision: 0.8817668054729327
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.2469323991623974
Relative absolute error: 12.195121951219512
Root relative squared error: 49.38647983247948
Weighted TruePositiveRate: 0.9390243902439024
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.909495321368308
Iteration time: 145.0
Weighted AreaUnderPRC: 0.8854848304580607
Mean absolute error: 0.06097560975609756
Coverage of cases: 93.90243902439025
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 2236.0
Weighted Recall: 0.9390243902439024
Weighted FalsePositiveRate: 0.9390243902439024
Kappa statistic: 0.0
Training time: 142.0
		
Iteration: 41
Labeled set size: 1177
Unlabelled set size: 709
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.097560975609756
Correctly Classified Instances: 93.90243902439025
Weighted Precision: 0.8817668054729327
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.2469323991623974
Relative absolute error: 12.195121951219512
Root relative squared error: 49.38647983247948
Weighted TruePositiveRate: 0.9390243902439024
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.909495321368308
Iteration time: 98.0
Weighted AreaUnderPRC: 0.8854848304580607
Mean absolute error: 0.06097560975609756
Coverage of cases: 93.90243902439025
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 2334.0
Weighted Recall: 0.9390243902439024
Weighted FalsePositiveRate: 0.9390243902439024
Kappa statistic: 0.0
Training time: 95.0
		
Iteration: 42
Labeled set size: 1197
Unlabelled set size: 689
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.097560975609756
Correctly Classified Instances: 93.90243902439025
Weighted Precision: 0.8817668054729327
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.2469323991623974
Relative absolute error: 12.195121951219512
Root relative squared error: 49.38647983247948
Weighted TruePositiveRate: 0.9390243902439024
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.909495321368308
Iteration time: 110.0
Weighted AreaUnderPRC: 0.8854848304580607
Mean absolute error: 0.06097560975609756
Coverage of cases: 93.90243902439025
Instances selection time: 3.0
Test time: 6.0
Accumulative iteration time: 2444.0
Weighted Recall: 0.9390243902439024
Weighted FalsePositiveRate: 0.9390243902439024
Kappa statistic: 0.0
Training time: 107.0
		
Iteration: 43
Labeled set size: 1217
Unlabelled set size: 669
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.097560975609756
Correctly Classified Instances: 93.90243902439025
Weighted Precision: 0.8817668054729327
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.2469323991623974
Relative absolute error: 12.195121951219512
Root relative squared error: 49.38647983247948
Weighted TruePositiveRate: 0.9390243902439024
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.909495321368308
Iteration time: 97.0
Weighted AreaUnderPRC: 0.8854848304580607
Mean absolute error: 0.06097560975609756
Coverage of cases: 93.90243902439025
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 2541.0
Weighted Recall: 0.9390243902439024
Weighted FalsePositiveRate: 0.9390243902439024
Kappa statistic: 0.0
Training time: 95.0
		
Iteration: 44
Labeled set size: 1237
Unlabelled set size: 649
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.097560975609756
Correctly Classified Instances: 93.90243902439025
Weighted Precision: 0.8817668054729327
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.2469323991623974
Relative absolute error: 12.195121951219512
Root relative squared error: 49.38647983247948
Weighted TruePositiveRate: 0.9390243902439024
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.909495321368308
Iteration time: 106.0
Weighted AreaUnderPRC: 0.8854848304580607
Mean absolute error: 0.06097560975609756
Coverage of cases: 93.90243902439025
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 2647.0
Weighted Recall: 0.9390243902439024
Weighted FalsePositiveRate: 0.9390243902439024
Kappa statistic: 0.0
Training time: 104.0
		
Iteration: 45
Labeled set size: 1257
Unlabelled set size: 629
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.097560975609756
Correctly Classified Instances: 93.90243902439025
Weighted Precision: 0.8817668054729327
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.2469323991623974
Relative absolute error: 12.195121951219512
Root relative squared error: 49.38647983247948
Weighted TruePositiveRate: 0.9390243902439024
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.909495321368308
Iteration time: 102.0
Weighted AreaUnderPRC: 0.8854848304580607
Mean absolute error: 0.06097560975609756
Coverage of cases: 93.90243902439025
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 2749.0
Weighted Recall: 0.9390243902439024
Weighted FalsePositiveRate: 0.9390243902439024
Kappa statistic: 0.0
Training time: 100.0
		
Iteration: 46
Labeled set size: 1277
Unlabelled set size: 609
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.097560975609756
Correctly Classified Instances: 93.90243902439025
Weighted Precision: 0.8817668054729327
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.2469323991623974
Relative absolute error: 12.195121951219512
Root relative squared error: 49.38647983247948
Weighted TruePositiveRate: 0.9390243902439024
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.909495321368308
Iteration time: 204.0
Weighted AreaUnderPRC: 0.8854848304580607
Mean absolute error: 0.06097560975609756
Coverage of cases: 93.90243902439025
Instances selection time: 4.0
Test time: 17.0
Accumulative iteration time: 2953.0
Weighted Recall: 0.9390243902439024
Weighted FalsePositiveRate: 0.9390243902439024
Kappa statistic: 0.0
Training time: 200.0
		
Time end:Wed Nov 01 08.36.34 EET 2017