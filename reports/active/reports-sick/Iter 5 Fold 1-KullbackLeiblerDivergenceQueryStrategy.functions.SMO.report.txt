Wed Nov 01 08.23.33 EET 2017
Dataset: sick
Test set size: 1886
Initial Labelled set size: 377
Initial Unlabelled set size: 1509
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Wed Nov 01 08.23.33 EET 2017
		
Iteration: 1
Labeled set size: 377
Unlabelled set size: 1509
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.097560975609756
Correctly Classified Instances: 93.90243902439025
Weighted Precision: 0.8817668054729327
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.2469323991623974
Relative absolute error: 12.195121951219512
Root relative squared error: 49.38647983247948
Weighted TruePositiveRate: 0.9390243902439024
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.909495321368308
Iteration time: 56.0
Weighted AreaUnderPRC: 0.8854848304580607
Mean absolute error: 0.06097560975609756
Coverage of cases: 93.90243902439025
Instances selection time: 8.0
Test time: 7.0
Accumulative iteration time: 56.0
Weighted Recall: 0.9390243902439024
Weighted FalsePositiveRate: 0.9390243902439024
Kappa statistic: 0.0
Training time: 48.0
		
Iteration: 2
Labeled set size: 397
Unlabelled set size: 1489
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.097560975609756
Correctly Classified Instances: 93.90243902439025
Weighted Precision: 0.8817668054729327
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.2469323991623974
Relative absolute error: 12.195121951219512
Root relative squared error: 49.38647983247948
Weighted TruePositiveRate: 0.9390243902439024
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.909495321368308
Iteration time: 25.0
Weighted AreaUnderPRC: 0.8854848304580607
Mean absolute error: 0.06097560975609756
Coverage of cases: 93.90243902439025
Instances selection time: 10.0
Test time: 7.0
Accumulative iteration time: 81.0
Weighted Recall: 0.9390243902439024
Weighted FalsePositiveRate: 0.9390243902439024
Kappa statistic: 0.0
Training time: 15.0
		
Iteration: 3
Labeled set size: 417
Unlabelled set size: 1469
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.097560975609756
Correctly Classified Instances: 93.90243902439025
Weighted Precision: 0.8817668054729327
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.2469323991623974
Relative absolute error: 12.195121951219512
Root relative squared error: 49.38647983247948
Weighted TruePositiveRate: 0.9390243902439024
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.909495321368308
Iteration time: 25.0
Weighted AreaUnderPRC: 0.8854848304580607
Mean absolute error: 0.06097560975609756
Coverage of cases: 93.90243902439025
Instances selection time: 10.0
Test time: 7.0
Accumulative iteration time: 106.0
Weighted Recall: 0.9390243902439024
Weighted FalsePositiveRate: 0.9390243902439024
Kappa statistic: 0.0
Training time: 15.0
		
Iteration: 4
Labeled set size: 437
Unlabelled set size: 1449
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.097560975609756
Correctly Classified Instances: 93.90243902439025
Weighted Precision: 0.8817668054729327
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.2469323991623974
Relative absolute error: 12.195121951219512
Root relative squared error: 49.38647983247948
Weighted TruePositiveRate: 0.9390243902439024
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.909495321368308
Iteration time: 25.0
Weighted AreaUnderPRC: 0.8854848304580607
Mean absolute error: 0.06097560975609756
Coverage of cases: 93.90243902439025
Instances selection time: 9.0
Test time: 8.0
Accumulative iteration time: 131.0
Weighted Recall: 0.9390243902439024
Weighted FalsePositiveRate: 0.9390243902439024
Kappa statistic: 0.0
Training time: 16.0
		
Iteration: 5
Labeled set size: 457
Unlabelled set size: 1429
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.097560975609756
Correctly Classified Instances: 93.90243902439025
Weighted Precision: 0.8817668054729327
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.2469323991623974
Relative absolute error: 12.195121951219512
Root relative squared error: 49.38647983247948
Weighted TruePositiveRate: 0.9390243902439024
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.909495321368308
Iteration time: 36.0
Weighted AreaUnderPRC: 0.8854848304580607
Mean absolute error: 0.06097560975609756
Coverage of cases: 93.90243902439025
Instances selection time: 9.0
Test time: 8.0
Accumulative iteration time: 167.0
Weighted Recall: 0.9390243902439024
Weighted FalsePositiveRate: 0.9390243902439024
Kappa statistic: 0.0
Training time: 27.0
		
Iteration: 6
Labeled set size: 477
Unlabelled set size: 1409
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.097560975609756
Correctly Classified Instances: 93.90243902439025
Weighted Precision: 0.8817668054729327
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.2469323991623974
Relative absolute error: 12.195121951219512
Root relative squared error: 49.38647983247948
Weighted TruePositiveRate: 0.9390243902439024
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.909495321368308
Iteration time: 26.0
Weighted AreaUnderPRC: 0.8854848304580607
Mean absolute error: 0.06097560975609756
Coverage of cases: 93.90243902439025
Instances selection time: 8.0
Test time: 8.0
Accumulative iteration time: 193.0
Weighted Recall: 0.9390243902439024
Weighted FalsePositiveRate: 0.9390243902439024
Kappa statistic: 0.0
Training time: 18.0
		
Iteration: 7
Labeled set size: 497
Unlabelled set size: 1389
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.097560975609756
Correctly Classified Instances: 93.90243902439025
Weighted Precision: 0.8817668054729327
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.2469323991623974
Relative absolute error: 12.195121951219512
Root relative squared error: 49.38647983247948
Weighted TruePositiveRate: 0.9390243902439024
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.909495321368308
Iteration time: 27.0
Weighted AreaUnderPRC: 0.8854848304580607
Mean absolute error: 0.06097560975609756
Coverage of cases: 93.90243902439025
Instances selection time: 9.0
Test time: 7.0
Accumulative iteration time: 220.0
Weighted Recall: 0.9390243902439024
Weighted FalsePositiveRate: 0.9390243902439024
Kappa statistic: 0.0
Training time: 18.0
		
Iteration: 8
Labeled set size: 517
Unlabelled set size: 1369
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.097560975609756
Correctly Classified Instances: 93.90243902439025
Weighted Precision: 0.8817668054729327
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.2469323991623974
Relative absolute error: 12.195121951219512
Root relative squared error: 49.38647983247948
Weighted TruePositiveRate: 0.9390243902439024
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.909495321368308
Iteration time: 27.0
Weighted AreaUnderPRC: 0.8854848304580607
Mean absolute error: 0.06097560975609756
Coverage of cases: 93.90243902439025
Instances selection time: 9.0
Test time: 7.0
Accumulative iteration time: 247.0
Weighted Recall: 0.9390243902439024
Weighted FalsePositiveRate: 0.9390243902439024
Kappa statistic: 0.0
Training time: 18.0
		
Iteration: 9
Labeled set size: 537
Unlabelled set size: 1349
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.097560975609756
Correctly Classified Instances: 93.90243902439025
Weighted Precision: 0.8817668054729327
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.2469323991623974
Relative absolute error: 12.195121951219512
Root relative squared error: 49.38647983247948
Weighted TruePositiveRate: 0.9390243902439024
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.909495321368308
Iteration time: 32.0
Weighted AreaUnderPRC: 0.8854848304580607
Mean absolute error: 0.06097560975609756
Coverage of cases: 93.90243902439025
Instances selection time: 8.0
Test time: 7.0
Accumulative iteration time: 279.0
Weighted Recall: 0.9390243902439024
Weighted FalsePositiveRate: 0.9390243902439024
Kappa statistic: 0.0
Training time: 24.0
		
Iteration: 10
Labeled set size: 557
Unlabelled set size: 1329
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.097560975609756
Correctly Classified Instances: 93.90243902439025
Weighted Precision: 0.8817668054729327
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.2469323991623974
Relative absolute error: 12.195121951219512
Root relative squared error: 49.38647983247948
Weighted TruePositiveRate: 0.9390243902439024
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.909495321368308
Iteration time: 27.0
Weighted AreaUnderPRC: 0.8854848304580607
Mean absolute error: 0.06097560975609756
Coverage of cases: 93.90243902439025
Instances selection time: 9.0
Test time: 7.0
Accumulative iteration time: 306.0
Weighted Recall: 0.9390243902439024
Weighted FalsePositiveRate: 0.9390243902439024
Kappa statistic: 0.0
Training time: 18.0
		
Iteration: 11
Labeled set size: 577
Unlabelled set size: 1309
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.097560975609756
Correctly Classified Instances: 93.90243902439025
Weighted Precision: 0.8817668054729327
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.2469323991623974
Relative absolute error: 12.195121951219512
Root relative squared error: 49.38647983247948
Weighted TruePositiveRate: 0.9390243902439024
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.909495321368308
Iteration time: 30.0
Weighted AreaUnderPRC: 0.8854848304580607
Mean absolute error: 0.06097560975609756
Coverage of cases: 93.90243902439025
Instances selection time: 8.0
Test time: 7.0
Accumulative iteration time: 336.0
Weighted Recall: 0.9390243902439024
Weighted FalsePositiveRate: 0.9390243902439024
Kappa statistic: 0.0
Training time: 22.0
		
Iteration: 12
Labeled set size: 597
Unlabelled set size: 1289
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.097560975609756
Correctly Classified Instances: 93.90243902439025
Weighted Precision: 0.8817668054729327
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.2469323991623974
Relative absolute error: 12.195121951219512
Root relative squared error: 49.38647983247948
Weighted TruePositiveRate: 0.9390243902439024
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.909495321368308
Iteration time: 28.0
Weighted AreaUnderPRC: 0.8854848304580607
Mean absolute error: 0.06097560975609756
Coverage of cases: 93.90243902439025
Instances selection time: 9.0
Test time: 7.0
Accumulative iteration time: 364.0
Weighted Recall: 0.9390243902439024
Weighted FalsePositiveRate: 0.9390243902439024
Kappa statistic: 0.0
Training time: 19.0
		
Iteration: 13
Labeled set size: 617
Unlabelled set size: 1269
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.097560975609756
Correctly Classified Instances: 93.90243902439025
Weighted Precision: 0.8817668054729327
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.2469323991623974
Relative absolute error: 12.195121951219512
Root relative squared error: 49.38647983247948
Weighted TruePositiveRate: 0.9390243902439024
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.909495321368308
Iteration time: 26.0
Weighted AreaUnderPRC: 0.8854848304580607
Mean absolute error: 0.06097560975609756
Coverage of cases: 93.90243902439025
Instances selection time: 8.0
Test time: 7.0
Accumulative iteration time: 390.0
Weighted Recall: 0.9390243902439024
Weighted FalsePositiveRate: 0.9390243902439024
Kappa statistic: 0.0
Training time: 18.0
		
Iteration: 14
Labeled set size: 637
Unlabelled set size: 1249
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.097560975609756
Correctly Classified Instances: 93.90243902439025
Weighted Precision: 0.8817668054729327
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.2469323991623974
Relative absolute error: 12.195121951219512
Root relative squared error: 49.38647983247948
Weighted TruePositiveRate: 0.9390243902439024
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.909495321368308
Iteration time: 28.0
Weighted AreaUnderPRC: 0.8854848304580607
Mean absolute error: 0.06097560975609756
Coverage of cases: 93.90243902439025
Instances selection time: 8.0
Test time: 7.0
Accumulative iteration time: 418.0
Weighted Recall: 0.9390243902439024
Weighted FalsePositiveRate: 0.9390243902439024
Kappa statistic: 0.0
Training time: 20.0
		
Iteration: 15
Labeled set size: 657
Unlabelled set size: 1229
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.097560975609756
Correctly Classified Instances: 93.90243902439025
Weighted Precision: 0.8817668054729327
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.2469323991623974
Relative absolute error: 12.195121951219512
Root relative squared error: 49.38647983247948
Weighted TruePositiveRate: 0.9390243902439024
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.909495321368308
Iteration time: 39.0
Weighted AreaUnderPRC: 0.8854848304580607
Mean absolute error: 0.06097560975609756
Coverage of cases: 93.90243902439025
Instances selection time: 6.0
Test time: 8.0
Accumulative iteration time: 457.0
Weighted Recall: 0.9390243902439024
Weighted FalsePositiveRate: 0.9390243902439024
Kappa statistic: 0.0
Training time: 33.0
		
Iteration: 16
Labeled set size: 677
Unlabelled set size: 1209
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.097560975609756
Correctly Classified Instances: 93.90243902439025
Weighted Precision: 0.8817668054729327
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.2469323991623974
Relative absolute error: 12.195121951219512
Root relative squared error: 49.38647983247948
Weighted TruePositiveRate: 0.9390243902439024
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.909495321368308
Iteration time: 39.0
Weighted AreaUnderPRC: 0.8854848304580607
Mean absolute error: 0.06097560975609756
Coverage of cases: 93.90243902439025
Instances selection time: 6.0
Test time: 7.0
Accumulative iteration time: 496.0
Weighted Recall: 0.9390243902439024
Weighted FalsePositiveRate: 0.9390243902439024
Kappa statistic: 0.0
Training time: 33.0
		
Iteration: 17
Labeled set size: 697
Unlabelled set size: 1189
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.097560975609756
Correctly Classified Instances: 93.90243902439025
Weighted Precision: 0.8817668054729327
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.2469323991623974
Relative absolute error: 12.195121951219512
Root relative squared error: 49.38647983247948
Weighted TruePositiveRate: 0.9390243902439024
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.909495321368308
Iteration time: 39.0
Weighted AreaUnderPRC: 0.8854848304580607
Mean absolute error: 0.06097560975609756
Coverage of cases: 93.90243902439025
Instances selection time: 6.0
Test time: 8.0
Accumulative iteration time: 535.0
Weighted Recall: 0.9390243902439024
Weighted FalsePositiveRate: 0.9390243902439024
Kappa statistic: 0.0
Training time: 33.0
		
Iteration: 18
Labeled set size: 717
Unlabelled set size: 1169
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.097560975609756
Correctly Classified Instances: 93.90243902439025
Weighted Precision: 0.8817668054729327
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.2469323991623974
Relative absolute error: 12.195121951219512
Root relative squared error: 49.38647983247948
Weighted TruePositiveRate: 0.9390243902439024
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.909495321368308
Iteration time: 38.0
Weighted AreaUnderPRC: 0.8854848304580607
Mean absolute error: 0.06097560975609756
Coverage of cases: 93.90243902439025
Instances selection time: 6.0
Test time: 7.0
Accumulative iteration time: 573.0
Weighted Recall: 0.9390243902439024
Weighted FalsePositiveRate: 0.9390243902439024
Kappa statistic: 0.0
Training time: 32.0
		
Iteration: 19
Labeled set size: 737
Unlabelled set size: 1149
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.097560975609756
Correctly Classified Instances: 93.90243902439025
Weighted Precision: 0.8817668054729327
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.2469323991623974
Relative absolute error: 12.195121951219512
Root relative squared error: 49.38647983247948
Weighted TruePositiveRate: 0.9390243902439024
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.909495321368308
Iteration time: 38.0
Weighted AreaUnderPRC: 0.8854848304580607
Mean absolute error: 0.06097560975609756
Coverage of cases: 93.90243902439025
Instances selection time: 5.0
Test time: 8.0
Accumulative iteration time: 611.0
Weighted Recall: 0.9390243902439024
Weighted FalsePositiveRate: 0.9390243902439024
Kappa statistic: 0.0
Training time: 33.0
		
Iteration: 20
Labeled set size: 757
Unlabelled set size: 1129
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.097560975609756
Correctly Classified Instances: 93.90243902439025
Weighted Precision: 0.8817668054729327
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.2469323991623974
Relative absolute error: 12.195121951219512
Root relative squared error: 49.38647983247948
Weighted TruePositiveRate: 0.9390243902439024
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.909495321368308
Iteration time: 38.0
Weighted AreaUnderPRC: 0.8854848304580607
Mean absolute error: 0.06097560975609756
Coverage of cases: 93.90243902439025
Instances selection time: 6.0
Test time: 7.0
Accumulative iteration time: 649.0
Weighted Recall: 0.9390243902439024
Weighted FalsePositiveRate: 0.9390243902439024
Kappa statistic: 0.0
Training time: 32.0
		
Iteration: 21
Labeled set size: 777
Unlabelled set size: 1109
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.097560975609756
Correctly Classified Instances: 93.90243902439025
Weighted Precision: 0.8817668054729327
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.2469323991623974
Relative absolute error: 12.195121951219512
Root relative squared error: 49.38647983247948
Weighted TruePositiveRate: 0.9390243902439024
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.909495321368308
Iteration time: 37.0
Weighted AreaUnderPRC: 0.8854848304580607
Mean absolute error: 0.06097560975609756
Coverage of cases: 93.90243902439025
Instances selection time: 6.0
Test time: 7.0
Accumulative iteration time: 686.0
Weighted Recall: 0.9390243902439024
Weighted FalsePositiveRate: 0.9390243902439024
Kappa statistic: 0.0
Training time: 31.0
		
Iteration: 22
Labeled set size: 797
Unlabelled set size: 1089
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.097560975609756
Correctly Classified Instances: 93.90243902439025
Weighted Precision: 0.8817668054729327
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.2469323991623974
Relative absolute error: 12.195121951219512
Root relative squared error: 49.38647983247948
Weighted TruePositiveRate: 0.9390243902439024
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.909495321368308
Iteration time: 39.0
Weighted AreaUnderPRC: 0.8854848304580607
Mean absolute error: 0.06097560975609756
Coverage of cases: 93.90243902439025
Instances selection time: 5.0
Test time: 8.0
Accumulative iteration time: 725.0
Weighted Recall: 0.9390243902439024
Weighted FalsePositiveRate: 0.9390243902439024
Kappa statistic: 0.0
Training time: 34.0
		
Iteration: 23
Labeled set size: 817
Unlabelled set size: 1069
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.097560975609756
Correctly Classified Instances: 93.90243902439025
Weighted Precision: 0.8817668054729327
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.2469323991623974
Relative absolute error: 12.195121951219512
Root relative squared error: 49.38647983247948
Weighted TruePositiveRate: 0.9390243902439024
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.909495321368308
Iteration time: 43.0
Weighted AreaUnderPRC: 0.8854848304580607
Mean absolute error: 0.06097560975609756
Coverage of cases: 93.90243902439025
Instances selection time: 5.0
Test time: 8.0
Accumulative iteration time: 768.0
Weighted Recall: 0.9390243902439024
Weighted FalsePositiveRate: 0.9390243902439024
Kappa statistic: 0.0
Training time: 38.0
		
Iteration: 24
Labeled set size: 837
Unlabelled set size: 1049
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.097560975609756
Correctly Classified Instances: 93.90243902439025
Weighted Precision: 0.8817668054729327
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.2469323991623974
Relative absolute error: 12.195121951219512
Root relative squared error: 49.38647983247948
Weighted TruePositiveRate: 0.9390243902439024
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.909495321368308
Iteration time: 45.0
Weighted AreaUnderPRC: 0.8854848304580607
Mean absolute error: 0.06097560975609756
Coverage of cases: 93.90243902439025
Instances selection time: 6.0
Test time: 7.0
Accumulative iteration time: 813.0
Weighted Recall: 0.9390243902439024
Weighted FalsePositiveRate: 0.9390243902439024
Kappa statistic: 0.0
Training time: 39.0
		
Iteration: 25
Labeled set size: 857
Unlabelled set size: 1029
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.097560975609756
Correctly Classified Instances: 93.90243902439025
Weighted Precision: 0.8817668054729327
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.2469323991623974
Relative absolute error: 12.195121951219512
Root relative squared error: 49.38647983247948
Weighted TruePositiveRate: 0.9390243902439024
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.909495321368308
Iteration time: 43.0
Weighted AreaUnderPRC: 0.8854848304580607
Mean absolute error: 0.06097560975609756
Coverage of cases: 93.90243902439025
Instances selection time: 5.0
Test time: 7.0
Accumulative iteration time: 856.0
Weighted Recall: 0.9390243902439024
Weighted FalsePositiveRate: 0.9390243902439024
Kappa statistic: 0.0
Training time: 38.0
		
Iteration: 26
Labeled set size: 877
Unlabelled set size: 1009
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.097560975609756
Correctly Classified Instances: 93.90243902439025
Weighted Precision: 0.8817668054729327
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.2469323991623974
Relative absolute error: 12.195121951219512
Root relative squared error: 49.38647983247948
Weighted TruePositiveRate: 0.9390243902439024
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.909495321368308
Iteration time: 47.0
Weighted AreaUnderPRC: 0.8854848304580607
Mean absolute error: 0.06097560975609756
Coverage of cases: 93.90243902439025
Instances selection time: 5.0
Test time: 7.0
Accumulative iteration time: 903.0
Weighted Recall: 0.9390243902439024
Weighted FalsePositiveRate: 0.9390243902439024
Kappa statistic: 0.0
Training time: 42.0
		
Iteration: 27
Labeled set size: 897
Unlabelled set size: 989
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.097560975609756
Correctly Classified Instances: 93.90243902439025
Weighted Precision: 0.8817668054729327
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.2469323991623974
Relative absolute error: 12.195121951219512
Root relative squared error: 49.38647983247948
Weighted TruePositiveRate: 0.9390243902439024
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.909495321368308
Iteration time: 41.0
Weighted AreaUnderPRC: 0.8854848304580607
Mean absolute error: 0.06097560975609756
Coverage of cases: 93.90243902439025
Instances selection time: 5.0
Test time: 8.0
Accumulative iteration time: 944.0
Weighted Recall: 0.9390243902439024
Weighted FalsePositiveRate: 0.9390243902439024
Kappa statistic: 0.0
Training time: 36.0
		
Iteration: 28
Labeled set size: 917
Unlabelled set size: 969
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.097560975609756
Correctly Classified Instances: 93.90243902439025
Weighted Precision: 0.8817668054729327
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.2469323991623974
Relative absolute error: 12.195121951219512
Root relative squared error: 49.38647983247948
Weighted TruePositiveRate: 0.9390243902439024
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.909495321368308
Iteration time: 49.0
Weighted AreaUnderPRC: 0.8854848304580607
Mean absolute error: 0.06097560975609756
Coverage of cases: 93.90243902439025
Instances selection time: 5.0
Test time: 7.0
Accumulative iteration time: 993.0
Weighted Recall: 0.9390243902439024
Weighted FalsePositiveRate: 0.9390243902439024
Kappa statistic: 0.0
Training time: 44.0
		
Iteration: 29
Labeled set size: 937
Unlabelled set size: 949
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.097560975609756
Correctly Classified Instances: 93.90243902439025
Weighted Precision: 0.8817668054729327
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.2469323991623974
Relative absolute error: 12.195121951219512
Root relative squared error: 49.38647983247948
Weighted TruePositiveRate: 0.9390243902439024
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.909495321368308
Iteration time: 43.0
Weighted AreaUnderPRC: 0.8854848304580607
Mean absolute error: 0.06097560975609756
Coverage of cases: 93.90243902439025
Instances selection time: 6.0
Test time: 7.0
Accumulative iteration time: 1036.0
Weighted Recall: 0.9390243902439024
Weighted FalsePositiveRate: 0.9390243902439024
Kappa statistic: 0.0
Training time: 37.0
		
Iteration: 30
Labeled set size: 957
Unlabelled set size: 929
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.097560975609756
Correctly Classified Instances: 93.90243902439025
Weighted Precision: 0.8817668054729327
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.2469323991623974
Relative absolute error: 12.195121951219512
Root relative squared error: 49.38647983247948
Weighted TruePositiveRate: 0.9390243902439024
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.909495321368308
Iteration time: 53.0
Weighted AreaUnderPRC: 0.8854848304580607
Mean absolute error: 0.06097560975609756
Coverage of cases: 93.90243902439025
Instances selection time: 4.0
Test time: 8.0
Accumulative iteration time: 1089.0
Weighted Recall: 0.9390243902439024
Weighted FalsePositiveRate: 0.9390243902439024
Kappa statistic: 0.0
Training time: 49.0
		
Iteration: 31
Labeled set size: 977
Unlabelled set size: 909
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.097560975609756
Correctly Classified Instances: 93.90243902439025
Weighted Precision: 0.8817668054729327
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.2469323991623974
Relative absolute error: 12.195121951219512
Root relative squared error: 49.38647983247948
Weighted TruePositiveRate: 0.9390243902439024
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.909495321368308
Iteration time: 48.0
Weighted AreaUnderPRC: 0.8854848304580607
Mean absolute error: 0.06097560975609756
Coverage of cases: 93.90243902439025
Instances selection time: 5.0
Test time: 7.0
Accumulative iteration time: 1137.0
Weighted Recall: 0.9390243902439024
Weighted FalsePositiveRate: 0.9390243902439024
Kappa statistic: 0.0
Training time: 43.0
		
Iteration: 32
Labeled set size: 997
Unlabelled set size: 889
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.097560975609756
Correctly Classified Instances: 93.90243902439025
Weighted Precision: 0.8817668054729327
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.2469323991623974
Relative absolute error: 12.195121951219512
Root relative squared error: 49.38647983247948
Weighted TruePositiveRate: 0.9390243902439024
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.909495321368308
Iteration time: 42.0
Weighted AreaUnderPRC: 0.8854848304580607
Mean absolute error: 0.06097560975609756
Coverage of cases: 93.90243902439025
Instances selection time: 5.0
Test time: 7.0
Accumulative iteration time: 1179.0
Weighted Recall: 0.9390243902439024
Weighted FalsePositiveRate: 0.9390243902439024
Kappa statistic: 0.0
Training time: 37.0
		
Iteration: 33
Labeled set size: 1017
Unlabelled set size: 869
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.097560975609756
Correctly Classified Instances: 93.90243902439025
Weighted Precision: 0.8817668054729327
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.2469323991623974
Relative absolute error: 12.195121951219512
Root relative squared error: 49.38647983247948
Weighted TruePositiveRate: 0.9390243902439024
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.909495321368308
Iteration time: 53.0
Weighted AreaUnderPRC: 0.8854848304580607
Mean absolute error: 0.06097560975609756
Coverage of cases: 93.90243902439025
Instances selection time: 4.0
Test time: 8.0
Accumulative iteration time: 1232.0
Weighted Recall: 0.9390243902439024
Weighted FalsePositiveRate: 0.9390243902439024
Kappa statistic: 0.0
Training time: 49.0
		
Iteration: 34
Labeled set size: 1037
Unlabelled set size: 849
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.097560975609756
Correctly Classified Instances: 93.90243902439025
Weighted Precision: 0.8817668054729327
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.2469323991623974
Relative absolute error: 12.195121951219512
Root relative squared error: 49.38647983247948
Weighted TruePositiveRate: 0.9390243902439024
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.909495321368308
Iteration time: 45.0
Weighted AreaUnderPRC: 0.8854848304580607
Mean absolute error: 0.06097560975609756
Coverage of cases: 93.90243902439025
Instances selection time: 4.0
Test time: 8.0
Accumulative iteration time: 1277.0
Weighted Recall: 0.9390243902439024
Weighted FalsePositiveRate: 0.9390243902439024
Kappa statistic: 0.0
Training time: 41.0
		
Iteration: 35
Labeled set size: 1057
Unlabelled set size: 829
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.097560975609756
Correctly Classified Instances: 93.90243902439025
Weighted Precision: 0.8817668054729327
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.2469323991623974
Relative absolute error: 12.195121951219512
Root relative squared error: 49.38647983247948
Weighted TruePositiveRate: 0.9390243902439024
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.909495321368308
Iteration time: 55.0
Weighted AreaUnderPRC: 0.8854848304580607
Mean absolute error: 0.06097560975609756
Coverage of cases: 93.90243902439025
Instances selection time: 4.0
Test time: 7.0
Accumulative iteration time: 1332.0
Weighted Recall: 0.9390243902439024
Weighted FalsePositiveRate: 0.9390243902439024
Kappa statistic: 0.0
Training time: 51.0
		
Iteration: 36
Labeled set size: 1077
Unlabelled set size: 809
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.097560975609756
Correctly Classified Instances: 93.90243902439025
Weighted Precision: 0.8817668054729327
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.2469323991623974
Relative absolute error: 12.195121951219512
Root relative squared error: 49.38647983247948
Weighted TruePositiveRate: 0.9390243902439024
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.909495321368308
Iteration time: 51.0
Weighted AreaUnderPRC: 0.8854848304580607
Mean absolute error: 0.06097560975609756
Coverage of cases: 93.90243902439025
Instances selection time: 5.0
Test time: 7.0
Accumulative iteration time: 1383.0
Weighted Recall: 0.9390243902439024
Weighted FalsePositiveRate: 0.9390243902439024
Kappa statistic: 0.0
Training time: 46.0
		
Iteration: 37
Labeled set size: 1097
Unlabelled set size: 789
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.097560975609756
Correctly Classified Instances: 93.90243902439025
Weighted Precision: 0.8817668054729327
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.2469323991623974
Relative absolute error: 12.195121951219512
Root relative squared error: 49.38647983247948
Weighted TruePositiveRate: 0.9390243902439024
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.909495321368308
Iteration time: 57.0
Weighted AreaUnderPRC: 0.8854848304580607
Mean absolute error: 0.06097560975609756
Coverage of cases: 93.90243902439025
Instances selection time: 4.0
Test time: 7.0
Accumulative iteration time: 1440.0
Weighted Recall: 0.9390243902439024
Weighted FalsePositiveRate: 0.9390243902439024
Kappa statistic: 0.0
Training time: 53.0
		
Iteration: 38
Labeled set size: 1117
Unlabelled set size: 769
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.097560975609756
Correctly Classified Instances: 93.90243902439025
Weighted Precision: 0.8817668054729327
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.2469323991623974
Relative absolute error: 12.195121951219512
Root relative squared error: 49.38647983247948
Weighted TruePositiveRate: 0.9390243902439024
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.909495321368308
Iteration time: 69.0
Weighted AreaUnderPRC: 0.8854848304580607
Mean absolute error: 0.06097560975609756
Coverage of cases: 93.90243902439025
Instances selection time: 4.0
Test time: 7.0
Accumulative iteration time: 1509.0
Weighted Recall: 0.9390243902439024
Weighted FalsePositiveRate: 0.9390243902439024
Kappa statistic: 0.0
Training time: 65.0
		
Iteration: 39
Labeled set size: 1137
Unlabelled set size: 749
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.097560975609756
Correctly Classified Instances: 93.90243902439025
Weighted Precision: 0.8817668054729327
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.2469323991623974
Relative absolute error: 12.195121951219512
Root relative squared error: 49.38647983247948
Weighted TruePositiveRate: 0.9390243902439024
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.909495321368308
Iteration time: 61.0
Weighted AreaUnderPRC: 0.8854848304580607
Mean absolute error: 0.06097560975609756
Coverage of cases: 93.90243902439025
Instances selection time: 4.0
Test time: 7.0
Accumulative iteration time: 1570.0
Weighted Recall: 0.9390243902439024
Weighted FalsePositiveRate: 0.9390243902439024
Kappa statistic: 0.0
Training time: 57.0
		
Iteration: 40
Labeled set size: 1157
Unlabelled set size: 729
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.097560975609756
Correctly Classified Instances: 93.90243902439025
Weighted Precision: 0.8817668054729327
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.2469323991623974
Relative absolute error: 12.195121951219512
Root relative squared error: 49.38647983247948
Weighted TruePositiveRate: 0.9390243902439024
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.909495321368308
Iteration time: 86.0
Weighted AreaUnderPRC: 0.8854848304580607
Mean absolute error: 0.06097560975609756
Coverage of cases: 93.90243902439025
Instances selection time: 4.0
Test time: 8.0
Accumulative iteration time: 1656.0
Weighted Recall: 0.9390243902439024
Weighted FalsePositiveRate: 0.9390243902439024
Kappa statistic: 0.0
Training time: 82.0
		
Iteration: 41
Labeled set size: 1177
Unlabelled set size: 709
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.097560975609756
Correctly Classified Instances: 93.90243902439025
Weighted Precision: 0.8817668054729327
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.2469323991623974
Relative absolute error: 12.195121951219512
Root relative squared error: 49.38647983247948
Weighted TruePositiveRate: 0.9390243902439024
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.909495321368308
Iteration time: 61.0
Weighted AreaUnderPRC: 0.8854848304580607
Mean absolute error: 0.06097560975609756
Coverage of cases: 93.90243902439025
Instances selection time: 4.0
Test time: 8.0
Accumulative iteration time: 1717.0
Weighted Recall: 0.9390243902439024
Weighted FalsePositiveRate: 0.9390243902439024
Kappa statistic: 0.0
Training time: 57.0
		
Iteration: 42
Labeled set size: 1197
Unlabelled set size: 689
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.097560975609756
Correctly Classified Instances: 93.90243902439025
Weighted Precision: 0.8817668054729327
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.2469323991623974
Relative absolute error: 12.195121951219512
Root relative squared error: 49.38647983247948
Weighted TruePositiveRate: 0.9390243902439024
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.909495321368308
Iteration time: 59.0
Weighted AreaUnderPRC: 0.8854848304580607
Mean absolute error: 0.06097560975609756
Coverage of cases: 93.90243902439025
Instances selection time: 3.0
Test time: 8.0
Accumulative iteration time: 1776.0
Weighted Recall: 0.9390243902439024
Weighted FalsePositiveRate: 0.9390243902439024
Kappa statistic: 0.0
Training time: 56.0
		
Iteration: 43
Labeled set size: 1217
Unlabelled set size: 669
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.097560975609756
Correctly Classified Instances: 93.90243902439025
Weighted Precision: 0.8817668054729327
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.2469323991623974
Relative absolute error: 12.195121951219512
Root relative squared error: 49.38647983247948
Weighted TruePositiveRate: 0.9390243902439024
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.909495321368308
Iteration time: 54.0
Weighted AreaUnderPRC: 0.8854848304580607
Mean absolute error: 0.06097560975609756
Coverage of cases: 93.90243902439025
Instances selection time: 3.0
Test time: 8.0
Accumulative iteration time: 1830.0
Weighted Recall: 0.9390243902439024
Weighted FalsePositiveRate: 0.9390243902439024
Kappa statistic: 0.0
Training time: 51.0
		
Iteration: 44
Labeled set size: 1237
Unlabelled set size: 649
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.097560975609756
Correctly Classified Instances: 93.90243902439025
Weighted Precision: 0.8817668054729327
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.2469323991623974
Relative absolute error: 12.195121951219512
Root relative squared error: 49.38647983247948
Weighted TruePositiveRate: 0.9390243902439024
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.909495321368308
Iteration time: 74.0
Weighted AreaUnderPRC: 0.8854848304580607
Mean absolute error: 0.06097560975609756
Coverage of cases: 93.90243902439025
Instances selection time: 4.0
Test time: 7.0
Accumulative iteration time: 1904.0
Weighted Recall: 0.9390243902439024
Weighted FalsePositiveRate: 0.9390243902439024
Kappa statistic: 0.0
Training time: 70.0
		
Iteration: 45
Labeled set size: 1257
Unlabelled set size: 629
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.097560975609756
Correctly Classified Instances: 93.90243902439025
Weighted Precision: 0.8817668054729327
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.2469323991623974
Relative absolute error: 12.195121951219512
Root relative squared error: 49.38647983247948
Weighted TruePositiveRate: 0.9390243902439024
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.909495321368308
Iteration time: 61.0
Weighted AreaUnderPRC: 0.8854848304580607
Mean absolute error: 0.06097560975609756
Coverage of cases: 93.90243902439025
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 1965.0
Weighted Recall: 0.9390243902439024
Weighted FalsePositiveRate: 0.9390243902439024
Kappa statistic: 0.0
Training time: 58.0
		
Iteration: 46
Labeled set size: 1277
Unlabelled set size: 609
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.097560975609756
Correctly Classified Instances: 93.90243902439025
Weighted Precision: 0.8817668054729327
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.2469323991623974
Relative absolute error: 12.195121951219512
Root relative squared error: 49.38647983247948
Weighted TruePositiveRate: 0.9390243902439024
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.909495321368308
Iteration time: 80.0
Weighted AreaUnderPRC: 0.8854848304580607
Mean absolute error: 0.06097560975609756
Coverage of cases: 93.90243902439025
Instances selection time: 3.0
Test time: 8.0
Accumulative iteration time: 2045.0
Weighted Recall: 0.9390243902439024
Weighted FalsePositiveRate: 0.9390243902439024
Kappa statistic: 0.0
Training time: 77.0
		
Time end:Wed Nov 01 08.23.38 EET 2017