Wed Nov 01 14.53.42 EET 2017
Dataset: vowel
Test set size: 495
Initial Labelled set size: 99
Initial Unlabelled set size: 396
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Wed Nov 01 14.53.42 EET 2017
		
Iteration: 1
Labeled set size: 99
Unlabelled set size: 396
	
Mean region size: 41.52433425160715
Incorrectly Classified Instances: 60.2020202020202
Correctly Classified Instances: 39.7979797979798
Weighted Precision: 0.4717215434046595
Weighted AreaUnderROC: 0.782763187429854
Root mean squared error: 0.2646724915232174
Relative absolute error: 77.70109285003097
Root relative squared error: 92.06646979254697
Weighted TruePositiveRate: 0.397979797979798
Weighted MatthewsCorrelation: 0.3567391275068142
Weighted FMeasure: 0.391366362152496
Iteration time: 19.0
Weighted AreaUnderPRC: 0.4582299811061918
Mean absolute error: 0.12843155842980408
Coverage of cases: 77.97979797979798
Instances selection time: 3.0
Test time: 6.0
Accumulative iteration time: 19.0
Weighted Recall: 0.397979797979798
Weighted FalsePositiveRate: 0.06020202020202021
Kappa statistic: 0.3377777777777778
Training time: 16.0
		
Iteration: 2
Labeled set size: 119
Unlabelled set size: 376
	
Mean region size: 43.948576675849395
Incorrectly Classified Instances: 56.36363636363637
Correctly Classified Instances: 43.63636363636363
Weighted Precision: 0.4597735052501968
Weighted AreaUnderROC: 0.8172592592592592
Root mean squared error: 0.25727167750776214
Relative absolute error: 76.7485240888551
Root relative squared error: 89.4920926214521
Weighted TruePositiveRate: 0.43636363636363634
Weighted MatthewsCorrelation: 0.37622841107146177
Weighted FMeasure: 0.4109612782878659
Iteration time: 19.0
Weighted AreaUnderPRC: 0.48615065449210665
Mean absolute error: 0.1268570646096787
Coverage of cases: 81.01010101010101
Instances selection time: 3.0
Test time: 5.0
Accumulative iteration time: 38.0
Weighted Recall: 0.43636363636363634
Weighted FalsePositiveRate: 0.05636363636363635
Kappa statistic: 0.38
Training time: 16.0
		
Iteration: 3
Labeled set size: 139
Unlabelled set size: 356
	
Mean region size: 38.93480257116635
Incorrectly Classified Instances: 57.37373737373738
Correctly Classified Instances: 42.62626262626262
Weighted Precision: 0.4864060704853544
Weighted AreaUnderROC: 0.7970909090909091
Root mean squared error: 0.26562438647554193
Relative absolute error: 75.9712514365563
Root relative squared error: 92.39758696822888
Weighted TruePositiveRate: 0.4262626262626263
Weighted MatthewsCorrelation: 0.37716754218604504
Weighted FMeasure: 0.40342173809677045
Iteration time: 20.0
Weighted AreaUnderPRC: 0.44829043809242775
Mean absolute error: 0.1255723164240608
Coverage of cases: 71.71717171717172
Instances selection time: 3.0
Test time: 6.0
Accumulative iteration time: 58.0
Weighted Recall: 0.4262626262626263
Weighted FalsePositiveRate: 0.05737373737373737
Kappa statistic: 0.36888888888888893
Training time: 17.0
		
Iteration: 4
Labeled set size: 159
Unlabelled set size: 336
	
Mean region size: 38.93480257116626
Incorrectly Classified Instances: 58.78787878787879
Correctly Classified Instances: 41.21212121212121
Weighted Precision: 0.43003186452355335
Weighted AreaUnderROC: 0.8173445566778901
Root mean squared error: 0.25966284388031236
Relative absolute error: 74.67162458090992
Root relative squared error: 90.32386114163485
Weighted TruePositiveRate: 0.4121212121212121
Weighted MatthewsCorrelation: 0.34798822533921114
Weighted FMeasure: 0.38251353262617066
Iteration time: 41.0
Weighted AreaUnderPRC: 0.47112825380754475
Mean absolute error: 0.12342417286100892
Coverage of cases: 78.38383838383838
Instances selection time: 7.0
Test time: 10.0
Accumulative iteration time: 99.0
Weighted Recall: 0.4121212121212121
Weighted FalsePositiveRate: 0.0587878787878788
Kappa statistic: 0.3533333333333333
Training time: 34.0
		
Iteration: 5
Labeled set size: 179
Unlabelled set size: 316
	
Mean region size: 38.14508723599639
Incorrectly Classified Instances: 56.96969696969697
Correctly Classified Instances: 43.03030303030303
Weighted Precision: 0.5348907880837194
Weighted AreaUnderROC: 0.8184601571268239
Root mean squared error: 0.25813778365025175
Relative absolute error: 72.62441120086602
Root relative squared error: 89.79336811308235
Weighted TruePositiveRate: 0.4303030303030303
Weighted MatthewsCorrelation: 0.3778953874964756
Weighted FMeasure: 0.3947599340567559
Iteration time: 54.0
Weighted AreaUnderPRC: 0.48547827094441237
Mean absolute error: 0.1200403490923413
Coverage of cases: 75.75757575757575
Instances selection time: 4.0
Test time: 11.0
Accumulative iteration time: 153.0
Weighted Recall: 0.4303030303030303
Weighted FalsePositiveRate: 0.056969696969696976
Kappa statistic: 0.3733333333333333
Training time: 50.0
		
Iteration: 6
Labeled set size: 199
Unlabelled set size: 296
	
Mean region size: 39.50413223140504
Incorrectly Classified Instances: 51.717171717171716
Correctly Classified Instances: 48.282828282828284
Weighted Precision: 0.5572621761719563
Weighted AreaUnderROC: 0.8168035914702579
Root mean squared error: 0.25613120196512673
Relative absolute error: 72.63797135152896
Root relative squared error: 89.09537758510324
Weighted TruePositiveRate: 0.48282828282828283
Weighted MatthewsCorrelation: 0.44143894169525877
Weighted FMeasure: 0.45223972349497654
Iteration time: 28.0
Weighted AreaUnderPRC: 0.4946479560956081
Mean absolute error: 0.12006276256451144
Coverage of cases: 75.55555555555556
Instances selection time: 2.0
Test time: 6.0
Accumulative iteration time: 181.0
Weighted Recall: 0.48282828282828283
Weighted FalsePositiveRate: 0.05171717171717172
Kappa statistic: 0.4311111111111111
Training time: 26.0
		
Iteration: 7
Labeled set size: 219
Unlabelled set size: 276
	
Mean region size: 37.539026629935826
Incorrectly Classified Instances: 52.323232323232325
Correctly Classified Instances: 47.676767676767675
Weighted Precision: 0.5548368784506822
Weighted AreaUnderROC: 0.8379079685746353
Root mean squared error: 0.2537809852942369
Relative absolute error: 71.12872789284837
Root relative squared error: 88.27785344086311
Weighted TruePositiveRate: 0.4767676767676768
Weighted MatthewsCorrelation: 0.44439050564274757
Weighted FMeasure: 0.4620067728086349
Iteration time: 29.0
Weighted AreaUnderPRC: 0.5406446731846629
Mean absolute error: 0.11756814527743609
Coverage of cases: 76.16161616161617
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 210.0
Weighted Recall: 0.4767676767676768
Weighted FalsePositiveRate: 0.05232323232323233
Kappa statistic: 0.4244444444444445
Training time: 27.0
		
Iteration: 8
Labeled set size: 239
Unlabelled set size: 256
	
Mean region size: 34.36179981634538
Incorrectly Classified Instances: 50.303030303030305
Correctly Classified Instances: 49.696969696969695
Weighted Precision: 0.5983971194986639
Weighted AreaUnderROC: 0.8323120089786759
Root mean squared error: 0.2534237217415165
Relative absolute error: 68.97223537080768
Root relative squared error: 88.15357912019148
Weighted TruePositiveRate: 0.49696969696969695
Weighted MatthewsCorrelation: 0.4588322787855961
Weighted FMeasure: 0.45653505752358886
Iteration time: 31.0
Weighted AreaUnderPRC: 0.5204320875478825
Mean absolute error: 0.11400369482778203
Coverage of cases: 73.13131313131314
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 241.0
Weighted Recall: 0.49696969696969695
Weighted FalsePositiveRate: 0.05030303030303031
Kappa statistic: 0.4466666666666666
Training time: 29.0
		
Iteration: 9
Labeled set size: 259
Unlabelled set size: 236
	
Mean region size: 34.7474747474748
Incorrectly Classified Instances: 46.666666666666664
Correctly Classified Instances: 53.333333333333336
Weighted Precision: 0.6386836586711102
Weighted AreaUnderROC: 0.829658810325477
Root mean squared error: 0.24538656056304547
Relative absolute error: 66.86674158732018
Root relative squared error: 85.35784824314783
Weighted TruePositiveRate: 0.5333333333333333
Weighted MatthewsCorrelation: 0.491521954588352
Weighted FMeasure: 0.4846521619752451
Iteration time: 34.0
Weighted AreaUnderPRC: 0.5518335608878667
Mean absolute error: 0.11052353981375307
Coverage of cases: 74.14141414141415
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 275.0
Weighted Recall: 0.5333333333333333
Weighted FalsePositiveRate: 0.04666666666666667
Kappa statistic: 0.48666666666666664
Training time: 32.0
		
Iteration: 10
Labeled set size: 279
Unlabelled set size: 216
	
Mean region size: 34.765840220385776
Incorrectly Classified Instances: 46.26262626262626
Correctly Classified Instances: 53.73737373737374
Weighted Precision: 0.706947198187817
Weighted AreaUnderROC: 0.8735982042648709
Root mean squared error: 0.24078697877545555
Relative absolute error: 64.99272247465312
Root relative squared error: 83.75788122251642
Weighted TruePositiveRate: 0.5373737373737374
Weighted MatthewsCorrelation: 0.5156859905592729
Weighted FMeasure: 0.49112842586357625
Iteration time: 39.0
Weighted AreaUnderPRC: 0.623019978773257
Mean absolute error: 0.10742598756141082
Coverage of cases: 76.76767676767676
Instances selection time: 2.0
Test time: 6.0
Accumulative iteration time: 314.0
Weighted Recall: 0.5373737373737374
Weighted FalsePositiveRate: 0.04626262626262627
Kappa statistic: 0.4911111111111111
Training time: 37.0
		
Iteration: 11
Labeled set size: 299
Unlabelled set size: 196
	
Mean region size: 35.20661157024795
Incorrectly Classified Instances: 44.24242424242424
Correctly Classified Instances: 55.75757575757576
Weighted Precision: 0.7150948865743081
Weighted AreaUnderROC: 0.8583838383838384
Root mean squared error: 0.23606989752781532
Relative absolute error: 64.09845697762601
Root relative squared error: 82.11704195094912
Weighted TruePositiveRate: 0.5575757575757576
Weighted MatthewsCorrelation: 0.5304690903469333
Weighted FMeasure: 0.5083793509537555
Iteration time: 40.0
Weighted AreaUnderPRC: 0.6245397790849034
Mean absolute error: 0.10594786277293623
Coverage of cases: 77.17171717171718
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 354.0
Weighted Recall: 0.5575757575757576
Weighted FalsePositiveRate: 0.04424242424242425
Kappa statistic: 0.5133333333333333
Training time: 39.0
		
Iteration: 12
Labeled set size: 319
Unlabelled set size: 176
	
Mean region size: 33.645546372819155
Incorrectly Classified Instances: 39.39393939393939
Correctly Classified Instances: 60.60606060606061
Weighted Precision: 0.7295294185812897
Weighted AreaUnderROC: 0.873692480359147
Root mean squared error: 0.22905615198327203
Relative absolute error: 61.03407612143869
Root relative squared error: 79.67730675749151
Weighted TruePositiveRate: 0.6060606060606061
Weighted MatthewsCorrelation: 0.575053157730038
Weighted FMeasure: 0.5589959747783483
Iteration time: 42.0
Weighted AreaUnderPRC: 0.6571472116623635
Mean absolute error: 0.10088277044865963
Coverage of cases: 77.37373737373737
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 396.0
Weighted Recall: 0.6060606060606061
Weighted FalsePositiveRate: 0.03939393939393939
Kappa statistic: 0.5666666666666667
Training time: 41.0
		
Iteration: 13
Labeled set size: 339
Unlabelled set size: 156
	
Mean region size: 32.06611570247936
Incorrectly Classified Instances: 37.77777777777778
Correctly Classified Instances: 62.22222222222222
Weighted Precision: 0.7402438001275211
Weighted AreaUnderROC: 0.9027407407407407
Root mean squared error: 0.22192034589922563
Relative absolute error: 57.9312014673894
Root relative squared error: 77.19511273913542
Weighted TruePositiveRate: 0.6222222222222222
Weighted MatthewsCorrelation: 0.5812000901365855
Weighted FMeasure: 0.5604838740334848
Iteration time: 52.0
Weighted AreaUnderPRC: 0.7016635013589442
Mean absolute error: 0.0957540520122145
Coverage of cases: 82.22222222222223
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 448.0
Weighted Recall: 0.6222222222222222
Weighted FalsePositiveRate: 0.037777777777777785
Kappa statistic: 0.5844444444444444
Training time: 51.0
		
Iteration: 14
Labeled set size: 359
Unlabelled set size: 136
	
Mean region size: 33.95775941230486
Incorrectly Classified Instances: 33.13131313131313
Correctly Classified Instances: 66.86868686868686
Weighted Precision: 0.7402531621230362
Weighted AreaUnderROC: 0.9145140291806959
Root mean squared error: 0.21473224355590226
Relative absolute error: 57.31881846280652
Root relative squared error: 74.69472743861293
Weighted TruePositiveRate: 0.6686868686868687
Weighted MatthewsCorrelation: 0.6321380774054345
Weighted FMeasure: 0.6228511063233486
Iteration time: 49.0
Weighted AreaUnderPRC: 0.7358914183204226
Mean absolute error: 0.09474184869885437
Coverage of cases: 83.03030303030303
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 497.0
Weighted Recall: 0.6686868686868687
Weighted FalsePositiveRate: 0.033131313131313136
Kappa statistic: 0.6355555555555555
Training time: 47.0
		
Iteration: 15
Labeled set size: 379
Unlabelled set size: 116
	
Mean region size: 31.955922865013758
Incorrectly Classified Instances: 31.717171717171716
Correctly Classified Instances: 68.28282828282828
Weighted Precision: 0.7552249416813118
Weighted AreaUnderROC: 0.9171425364758696
Root mean squared error: 0.2079794032937912
Relative absolute error: 54.04524567435565
Root relative squared error: 72.3457482892214
Weighted TruePositiveRate: 0.6828282828282828
Weighted MatthewsCorrelation: 0.6499673779744467
Weighted FMeasure: 0.642582349669855
Iteration time: 56.0
Weighted AreaUnderPRC: 0.7566564421016204
Mean absolute error: 0.08933098458571238
Coverage of cases: 86.86868686868686
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 553.0
Weighted Recall: 0.6828282828282828
Weighted FalsePositiveRate: 0.03171717171717172
Kappa statistic: 0.6511111111111111
Training time: 55.0
		
Iteration: 16
Labeled set size: 399
Unlabelled set size: 96
	
Mean region size: 33.535353535353444
Incorrectly Classified Instances: 28.484848484848484
Correctly Classified Instances: 71.51515151515152
Weighted Precision: 0.7925901818067926
Weighted AreaUnderROC: 0.9368552188552188
Root mean squared error: 0.19700260293229122
Relative absolute error: 51.77976025527968
Root relative squared error: 68.52746232725846
Weighted TruePositiveRate: 0.7151515151515152
Weighted MatthewsCorrelation: 0.6930367404077163
Weighted FMeasure: 0.6877529232569681
Iteration time: 61.0
Weighted AreaUnderPRC: 0.8000030674635726
Mean absolute error: 0.0855863805872397
Coverage of cases: 89.29292929292929
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 614.0
Weighted Recall: 0.7151515151515152
Weighted FalsePositiveRate: 0.028484848484848484
Kappa statistic: 0.6866666666666668
Training time: 60.0
		
Iteration: 17
Labeled set size: 419
Unlabelled set size: 76
	
Mean region size: 30.101010101010154
Incorrectly Classified Instances: 26.666666666666668
Correctly Classified Instances: 73.33333333333333
Weighted Precision: 0.7963420593378224
Weighted AreaUnderROC: 0.9184646464646464
Root mean squared error: 0.1991339855947971
Relative absolute error: 49.959031717606294
Root relative squared error: 69.26886494293883
Weighted TruePositiveRate: 0.7333333333333333
Weighted MatthewsCorrelation: 0.7119177758276932
Weighted FMeasure: 0.7155395296553264
Iteration time: 60.0
Weighted AreaUnderPRC: 0.7603235846692374
Mean absolute error: 0.08257691192992828
Coverage of cases: 88.08080808080808
Instances selection time: 0.0
Test time: 6.0
Accumulative iteration time: 674.0
Weighted Recall: 0.7333333333333333
Weighted FalsePositiveRate: 0.026666666666666665
Kappa statistic: 0.7066666666666666
Training time: 60.0
		
Iteration: 18
Labeled set size: 439
Unlabelled set size: 56
	
Mean region size: 32.56198347107443
Incorrectly Classified Instances: 26.262626262626263
Correctly Classified Instances: 73.73737373737374
Weighted Precision: 0.7772176203564434
Weighted AreaUnderROC: 0.9394994388327721
Root mean squared error: 0.1980272111326631
Relative absolute error: 52.053397839680144
Root relative squared error: 68.88387284572921
Weighted TruePositiveRate: 0.7373737373737373
Weighted MatthewsCorrelation: 0.706656763004102
Weighted FMeasure: 0.7135074980313565
Iteration time: 59.0
Weighted AreaUnderPRC: 0.7783690341217302
Mean absolute error: 0.08603867411517435
Coverage of cases: 90.5050505050505
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 733.0
Weighted Recall: 0.7373737373737373
Weighted FalsePositiveRate: 0.026262626262626265
Kappa statistic: 0.711111111111111
Training time: 58.0
		
Iteration: 19
Labeled set size: 459
Unlabelled set size: 36
	
Mean region size: 32.94765840220388
Incorrectly Classified Instances: 22.02020202020202
Correctly Classified Instances: 77.97979797979798
Weighted Precision: 0.8089206801772706
Weighted AreaUnderROC: 0.9578563411896747
Root mean squared error: 0.18160984439980182
Relative absolute error: 47.8378882208154
Root relative squared error: 63.17308291933616
Weighted TruePositiveRate: 0.7797979797979798
Weighted MatthewsCorrelation: 0.7593295928508148
Weighted FMeasure: 0.7670401661699761
Iteration time: 66.0
Weighted AreaUnderPRC: 0.850408878579199
Mean absolute error: 0.07907088962118299
Coverage of cases: 95.15151515151516
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 799.0
Weighted Recall: 0.7797979797979798
Weighted FalsePositiveRate: 0.02202020202020202
Kappa statistic: 0.7577777777777778
Training time: 65.0
		
Iteration: 20
Labeled set size: 479
Unlabelled set size: 16
	
Mean region size: 33.314967860422506
Incorrectly Classified Instances: 17.373737373737374
Correctly Classified Instances: 82.62626262626263
Weighted Precision: 0.833279055807216
Weighted AreaUnderROC: 0.9771133557800226
Root mean squared error: 0.17150205783151143
Relative absolute error: 45.85794937592225
Root relative squared error: 59.65708387688415
Weighted TruePositiveRate: 0.8262626262626263
Weighted MatthewsCorrelation: 0.8106668291292081
Weighted FMeasure: 0.8253078169939027
Iteration time: 73.0
Weighted AreaUnderPRC: 0.8899270374699078
Mean absolute error: 0.07579826343127694
Coverage of cases: 98.38383838383838
Instances selection time: 0.0
Test time: 6.0
Accumulative iteration time: 872.0
Weighted Recall: 0.8262626262626263
Weighted FalsePositiveRate: 0.017373737373737375
Kappa statistic: 0.8088888888888889
Training time: 73.0
		
Time end:Wed Nov 01 14.53.44 EET 2017