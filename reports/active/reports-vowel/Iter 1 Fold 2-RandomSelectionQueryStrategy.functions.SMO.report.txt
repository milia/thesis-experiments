Wed Nov 01 14.59.53 EET 2017
Dataset: vowel
Test set size: 495
Initial Labelled set size: 99
Initial Unlabelled set size: 396
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Wed Nov 01 14.59.53 EET 2017
		
Iteration: 1
Labeled set size: 99
Unlabelled set size: 396
	
Mean region size: 82.46097337006394
Incorrectly Classified Instances: 76.76767676767676
Correctly Classified Instances: 23.232323232323232
Weighted Precision: 0.2567803487037847
Weighted AreaUnderROC: 0.7203344556677891
Root mean squared error: 0.280312255755481
Relative absolute error: 95.70909090909035
Root relative squared error: 97.50677026716559
Weighted TruePositiveRate: 0.23232323232323232
Weighted MatthewsCorrelation: 0.162589899572097
Weighted FMeasure: 0.227432481699997
Iteration time: 399.0
Weighted AreaUnderPRC: 0.2080682080045694
Mean absolute error: 0.1581968444778363
Coverage of cases: 95.55555555555556
Instances selection time: 1.0
Test time: 10.0
Accumulative iteration time: 399.0
Weighted Recall: 0.23232323232323232
Weighted FalsePositiveRate: 0.07676767676767678
Kappa statistic: 0.15555555555555556
Training time: 398.0
		
Iteration: 2
Labeled set size: 119
Unlabelled set size: 376
	
Mean region size: 82.18549127639999
Incorrectly Classified Instances: 72.72727272727273
Correctly Classified Instances: 27.272727272727273
Weighted Precision: 0.2894277155704563
Weighted AreaUnderROC: 0.7368484848484848
Root mean squared error: 0.27916654561015125
Relative absolute error: 95.29292929292889
Root relative squared error: 97.10823437142909
Weighted TruePositiveRate: 0.2727272727272727
Weighted MatthewsCorrelation: 0.20302840759718763
Weighted FMeasure: 0.26407166829822165
Iteration time: 402.0
Weighted AreaUnderPRC: 0.2280173780951447
Mean absolute error: 0.15750897403789998
Coverage of cases: 96.16161616161617
Instances selection time: 1.0
Test time: 11.0
Accumulative iteration time: 801.0
Weighted Recall: 0.2727272727272727
Weighted FalsePositiveRate: 0.07272727272727271
Kappa statistic: 0.19999999999999998
Training time: 401.0
		
Iteration: 3
Labeled set size: 139
Unlabelled set size: 356
	
Mean region size: 82.16712580348907
Incorrectly Classified Instances: 72.12121212121212
Correctly Classified Instances: 27.87878787878788
Weighted Precision: 0.32419662932643767
Weighted AreaUnderROC: 0.7582424242424242
Root mean squared error: 0.27768223356644944
Relative absolute error: 94.77979797979745
Root relative squared error: 96.59191562161222
Weighted TruePositiveRate: 0.2787878787878788
Weighted MatthewsCorrelation: 0.21719933060455784
Weighted FMeasure: 0.27143804390194703
Iteration time: 413.0
Weighted AreaUnderPRC: 0.25260013685173294
Mean absolute error: 0.15666082310710425
Coverage of cases: 97.57575757575758
Instances selection time: 2.0
Test time: 10.0
Accumulative iteration time: 1214.0
Weighted Recall: 0.2787878787878788
Weighted FalsePositiveRate: 0.07212121212121213
Kappa statistic: 0.2066666666666667
Training time: 411.0
		
Iteration: 4
Labeled set size: 159
Unlabelled set size: 336
	
Mean region size: 82.03856749311254
Incorrectly Classified Instances: 70.1010101010101
Correctly Classified Instances: 29.8989898989899
Weighted Precision: 0.33832131728776305
Weighted AreaUnderROC: 0.7876341189674523
Root mean squared error: 0.27650147484608273
Relative absolute error: 94.36767676767617
Root relative squared error: 96.1811880600311
Weighted TruePositiveRate: 0.298989898989899
Weighted MatthewsCorrelation: 0.24168182924127796
Weighted FMeasure: 0.2961308183573072
Iteration time: 415.0
Weighted AreaUnderPRC: 0.2753496397097975
Mean absolute error: 0.15597963102095336
Coverage of cases: 97.97979797979798
Instances selection time: 1.0
Test time: 11.0
Accumulative iteration time: 1629.0
Weighted Recall: 0.298989898989899
Weighted FalsePositiveRate: 0.07010101010101011
Kappa statistic: 0.22888888888888886
Training time: 414.0
		
Iteration: 5
Labeled set size: 179
Unlabelled set size: 316
	
Mean region size: 82.0569329660235
Incorrectly Classified Instances: 63.63636363636363
Correctly Classified Instances: 36.36363636363637
Weighted Precision: 0.38653756971586056
Weighted AreaUnderROC: 0.7967878787878788
Root mean squared error: 0.2759077702944763
Relative absolute error: 94.17373737373669
Root relative squared error: 95.9746676095997
Weighted TruePositiveRate: 0.36363636363636365
Weighted MatthewsCorrelation: 0.30538345362378005
Weighted FMeasure: 0.35556906499858043
Iteration time: 421.0
Weighted AreaUnderPRC: 0.29633648281508707
Mean absolute error: 0.15565907003923518
Coverage of cases: 97.97979797979798
Instances selection time: 1.0
Test time: 10.0
Accumulative iteration time: 2050.0
Weighted Recall: 0.36363636363636365
Weighted FalsePositiveRate: 0.06363636363636363
Kappa statistic: 0.3
Training time: 420.0
		
Iteration: 6
Labeled set size: 199
Unlabelled set size: 296
	
Mean region size: 82.11202938475627
Incorrectly Classified Instances: 64.44444444444444
Correctly Classified Instances: 35.55555555555556
Weighted Precision: 0.3848338741468533
Weighted AreaUnderROC: 0.8012457912457912
Root mean squared error: 0.2756713399559794
Relative absolute error: 94.08080808080746
Root relative squared error: 95.8924251880621
Weighted TruePositiveRate: 0.35555555555555557
Weighted MatthewsCorrelation: 0.3008035269443586
Weighted FMeasure: 0.35367359014515815
Iteration time: 422.0
Weighted AreaUnderPRC: 0.30838894782667065
Mean absolute error: 0.15550546790216208
Coverage of cases: 97.57575757575758
Instances selection time: 1.0
Test time: 10.0
Accumulative iteration time: 2472.0
Weighted Recall: 0.35555555555555557
Weighted FalsePositiveRate: 0.06444444444444444
Kappa statistic: 0.29111111111111115
Training time: 421.0
		
Iteration: 7
Labeled set size: 219
Unlabelled set size: 276
	
Mean region size: 82.18549127640001
Incorrectly Classified Instances: 61.81818181818182
Correctly Classified Instances: 38.18181818181818
Weighted Precision: 0.4283875759659976
Weighted AreaUnderROC: 0.8097306397306395
Root mean squared error: 0.2751453589349383
Relative absolute error: 93.91515151515094
Root relative squared error: 95.7094624044859
Weighted TruePositiveRate: 0.38181818181818183
Weighted MatthewsCorrelation: 0.3358891810858802
Weighted FMeasure: 0.3827686444698528
Iteration time: 435.0
Weighted AreaUnderPRC: 0.3242485514469758
Mean absolute error: 0.1552316553969447
Coverage of cases: 97.77777777777777
Instances selection time: 1.0
Test time: 10.0
Accumulative iteration time: 2907.0
Weighted Recall: 0.38181818181818183
Weighted FalsePositiveRate: 0.06181818181818182
Kappa statistic: 0.32
Training time: 434.0
		
Iteration: 8
Labeled set size: 239
Unlabelled set size: 256
	
Mean region size: 82.02020202020164
Incorrectly Classified Instances: 59.39393939393939
Correctly Classified Instances: 40.60606060606061
Weighted Precision: 0.4564057443048115
Weighted AreaUnderROC: 0.8221077441077442
Root mean squared error: 0.2743648971821541
Relative absolute error: 93.6444444444438
Root relative squared error: 95.43797836028692
Weighted TruePositiveRate: 0.40606060606060607
Weighted MatthewsCorrelation: 0.36260510127475976
Weighted FMeasure: 0.40495703780900133
Iteration time: 435.0
Weighted AreaUnderPRC: 0.35164445005482825
Mean absolute error: 0.15478420569329654
Coverage of cases: 97.97979797979798
Instances selection time: 1.0
Test time: 10.0
Accumulative iteration time: 3342.0
Weighted Recall: 0.40606060606060607
Weighted FalsePositiveRate: 0.059393939393939416
Kappa statistic: 0.34666666666666673
Training time: 434.0
		
Iteration: 9
Labeled set size: 259
Unlabelled set size: 236
	
Mean region size: 82.07529843893444
Incorrectly Classified Instances: 58.58585858585859
Correctly Classified Instances: 41.41414141414141
Weighted Precision: 0.45090253663973634
Weighted AreaUnderROC: 0.8281638608305275
Root mean squared error: 0.273953223101708
Relative absolute error: 93.5151515151509
Root relative squared error: 95.29477730802176
Weighted TruePositiveRate: 0.41414141414141414
Weighted MatthewsCorrelation: 0.36763984385370346
Weighted FMeasure: 0.41367517519383884
Iteration time: 442.0
Weighted AreaUnderPRC: 0.35422027325663175
Mean absolute error: 0.15457049837215123
Coverage of cases: 99.39393939393939
Instances selection time: 1.0
Test time: 10.0
Accumulative iteration time: 3784.0
Weighted Recall: 0.41414141414141414
Weighted FalsePositiveRate: 0.05858585858585858
Kappa statistic: 0.3555555555555555
Training time: 441.0
		
Iteration: 10
Labeled set size: 279
Unlabelled set size: 216
	
Mean region size: 82.05693296602351
Incorrectly Classified Instances: 56.16161616161616
Correctly Classified Instances: 43.83838383838384
Weighted Precision: 0.4514433904722854
Weighted AreaUnderROC: 0.8458047138047137
Root mean squared error: 0.27292565898591037
Relative absolute error: 93.16363636363572
Root relative squared error: 94.93733857276624
Weighted TruePositiveRate: 0.4383838383838384
Weighted MatthewsCorrelation: 0.38459411290112033
Weighted FMeasure: 0.43215961971778444
Iteration time: 446.0
Weighted AreaUnderPRC: 0.3780625482514421
Mean absolute error: 0.1539894815927873
Coverage of cases: 99.39393939393939
Instances selection time: 1.0
Test time: 10.0
Accumulative iteration time: 4230.0
Weighted Recall: 0.4383838383838384
Weighted FalsePositiveRate: 0.05616161616161616
Kappa statistic: 0.38222222222222224
Training time: 445.0
		
Iteration: 11
Labeled set size: 299
Unlabelled set size: 196
	
Mean region size: 81.92837465564699
Incorrectly Classified Instances: 52.121212121212125
Correctly Classified Instances: 47.878787878787875
Weighted Precision: 0.4710256554451949
Weighted AreaUnderROC: 0.8506891133557801
Root mean squared error: 0.27264032896359497
Relative absolute error: 93.07070707070639
Root relative squared error: 94.83808636967846
Weighted TruePositiveRate: 0.47878787878787876
Weighted MatthewsCorrelation: 0.41959369099191435
Weighted FMeasure: 0.4647016352512389
Iteration time: 455.0
Weighted AreaUnderPRC: 0.39821696068083334
Mean absolute error: 0.15383587945571403
Coverage of cases: 99.39393939393939
Instances selection time: 0.0
Test time: 10.0
Accumulative iteration time: 4685.0
Weighted Recall: 0.47878787878787876
Weighted FalsePositiveRate: 0.052121212121212124
Kappa statistic: 0.42666666666666664
Training time: 455.0
		
Iteration: 12
Labeled set size: 319
Unlabelled set size: 176
	
Mean region size: 82.03856749311255
Incorrectly Classified Instances: 50.90909090909091
Correctly Classified Instances: 49.09090909090909
Weighted Precision: 0.48826253369252587
Weighted AreaUnderROC: 0.8557283950617284
Root mean squared error: 0.27219526818562795
Relative absolute error: 92.9212121212115
Root relative squared error: 94.68327173656442
Weighted TruePositiveRate: 0.4909090909090909
Weighted MatthewsCorrelation: 0.43618274702894
Weighted FMeasure: 0.4819390338212968
Iteration time: 460.0
Weighted AreaUnderPRC: 0.412417723797275
Mean absolute error: 0.1535887803656398
Coverage of cases: 99.5959595959596
Instances selection time: 0.0
Test time: 10.0
Accumulative iteration time: 5145.0
Weighted Recall: 0.4909090909090909
Weighted FalsePositiveRate: 0.05090909090909092
Kappa statistic: 0.44000000000000006
Training time: 460.0
		
Iteration: 13
Labeled set size: 339
Unlabelled set size: 156
	
Mean region size: 81.9467401285579
Incorrectly Classified Instances: 48.08080808080808
Correctly Classified Instances: 51.91919191919192
Weighted Precision: 0.5167282821283664
Weighted AreaUnderROC: 0.8676700336700337
Root mean squared error: 0.27162411501797756
Relative absolute error: 92.73535353535293
Root relative squared error: 94.48459579727894
Weighted TruePositiveRate: 0.5191919191919192
Weighted MatthewsCorrelation: 0.46656453862808284
Weighted FMeasure: 0.5076266380211443
Iteration time: 468.0
Weighted AreaUnderPRC: 0.44297845998606805
Mean absolute error: 0.15328157609149343
Coverage of cases: 99.1919191919192
Instances selection time: 1.0
Test time: 9.0
Accumulative iteration time: 5613.0
Weighted Recall: 0.5191919191919192
Weighted FalsePositiveRate: 0.04808080808080809
Kappa statistic: 0.4711111111111111
Training time: 467.0
		
Iteration: 14
Labeled set size: 359
Unlabelled set size: 136
	
Mean region size: 81.92837465564699
Incorrectly Classified Instances: 44.84848484848485
Correctly Classified Instances: 55.15151515151515
Weighted Precision: 0.5517681978124211
Weighted AreaUnderROC: 0.8811806958473625
Root mean squared error: 0.2709242792560749
Relative absolute error: 92.49696969696907
Root relative squared error: 94.24115754775725
Weighted TruePositiveRate: 0.5515151515151515
Weighted MatthewsCorrelation: 0.5040785506638052
Weighted FMeasure: 0.5438526257550684
Iteration time: 478.0
Weighted AreaUnderPRC: 0.4517930015077679
Mean absolute error: 0.1528875532181317
Coverage of cases: 99.1919191919192
Instances selection time: 1.0
Test time: 10.0
Accumulative iteration time: 6091.0
Weighted Recall: 0.5515151515151515
Weighted FalsePositiveRate: 0.04484848484848485
Kappa statistic: 0.5066666666666666
Training time: 477.0
		
Iteration: 15
Labeled set size: 379
Unlabelled set size: 116
	
Mean region size: 81.91000918273605
Incorrectly Classified Instances: 47.878787878787875
Correctly Classified Instances: 52.121212121212125
Weighted Precision: 0.5254025451176423
Weighted AreaUnderROC: 0.8872233445566778
Root mean squared error: 0.2704332941960904
Relative absolute error: 92.32727272727213
Root relative squared error: 94.07036812822406
Weighted TruePositiveRate: 0.5212121212121212
Weighted MatthewsCorrelation: 0.473604882843726
Weighted FMeasure: 0.5179544969533818
Iteration time: 481.0
Weighted AreaUnderPRC: 0.4610089203727218
Mean absolute error: 0.15260706235912846
Coverage of cases: 99.1919191919192
Instances selection time: 1.0
Test time: 10.0
Accumulative iteration time: 6572.0
Weighted Recall: 0.5212121212121212
Weighted FalsePositiveRate: 0.047878787878787875
Kappa statistic: 0.47333333333333333
Training time: 480.0
		
Iteration: 16
Labeled set size: 399
Unlabelled set size: 96
	
Mean region size: 81.98347107437976
Incorrectly Classified Instances: 45.45454545454545
Correctly Classified Instances: 54.54545454545455
Weighted Precision: 0.5552351303862265
Weighted AreaUnderROC: 0.8880493827160495
Root mean squared error: 0.27033539442314997
Relative absolute error: 92.29494949494887
Root relative squared error: 94.0363136390845
Weighted TruePositiveRate: 0.5454545454545454
Weighted MatthewsCorrelation: 0.5027310350014643
Weighted FMeasure: 0.5442857976966875
Iteration time: 492.0
Weighted AreaUnderPRC: 0.4848232558550282
Mean absolute error: 0.15255363552884207
Coverage of cases: 99.1919191919192
Instances selection time: 1.0
Test time: 10.0
Accumulative iteration time: 7064.0
Weighted Recall: 0.5454545454545454
Weighted FalsePositiveRate: 0.04545454545454545
Kappa statistic: 0.49999999999999994
Training time: 491.0
		
Iteration: 17
Labeled set size: 419
Unlabelled set size: 76
	
Mean region size: 82.0018365472907
Incorrectly Classified Instances: 43.03030303030303
Correctly Classified Instances: 56.96969696969697
Weighted Precision: 0.5704049989944937
Weighted AreaUnderROC: 0.9035712682379349
Root mean squared error: 0.2695288422619857
Relative absolute error: 92.02828282828219
Root relative squared error: 93.75575403217337
Weighted TruePositiveRate: 0.5696969696969697
Weighted MatthewsCorrelation: 0.5244131161713129
Weighted FMeasure: 0.5627777848066658
Iteration time: 497.0
Weighted AreaUnderPRC: 0.5044138133110126
Mean absolute error: 0.1521128641789798
Coverage of cases: 99.1919191919192
Instances selection time: 0.0
Test time: 10.0
Accumulative iteration time: 7561.0
Weighted Recall: 0.5696969696969697
Weighted FalsePositiveRate: 0.04303030303030302
Kappa statistic: 0.5266666666666666
Training time: 497.0
		
Iteration: 18
Labeled set size: 439
Unlabelled set size: 56
	
Mean region size: 82.05693296602347
Incorrectly Classified Instances: 40.2020202020202
Correctly Classified Instances: 59.7979797979798
Weighted Precision: 0.6008134469323441
Weighted AreaUnderROC: 0.9037441077441077
Root mean squared error: 0.26947207240878607
Relative absolute error: 92.00808080808015
Root relative squared error: 93.73600660793352
Weighted TruePositiveRate: 0.597979797979798
Weighted MatthewsCorrelation: 0.5570295635906474
Weighted FMeasure: 0.5936133167089959
Iteration time: 522.0
Weighted AreaUnderPRC: 0.5155532025136227
Mean absolute error: 0.15207947241005082
Coverage of cases: 99.1919191919192
Instances selection time: 1.0
Test time: 11.0
Accumulative iteration time: 8083.0
Weighted Recall: 0.597979797979798
Weighted FalsePositiveRate: 0.0402020202020202
Kappa statistic: 0.5577777777777777
Training time: 521.0
		
Iteration: 19
Labeled set size: 459
Unlabelled set size: 36
	
Mean region size: 82.00183654729068
Incorrectly Classified Instances: 41.21212121212121
Correctly Classified Instances: 58.78787878787879
Weighted Precision: 0.5934501424564111
Weighted AreaUnderROC: 0.9097800224466891
Root mean squared error: 0.2690538145962317
Relative absolute error: 91.87070707070637
Root relative squared error: 93.59051540088203
Weighted TruePositiveRate: 0.5878787878787879
Weighted MatthewsCorrelation: 0.547919151608843
Weighted FMeasure: 0.5864552492928272
Iteration time: 529.0
Weighted AreaUnderPRC: 0.517310387642856
Mean absolute error: 0.15185240838133382
Coverage of cases: 99.1919191919192
Instances selection time: 0.0
Test time: 10.0
Accumulative iteration time: 8612.0
Weighted Recall: 0.5878787878787879
Weighted FalsePositiveRate: 0.0412121212121212
Kappa statistic: 0.5466666666666666
Training time: 529.0
		
Iteration: 20
Labeled set size: 479
Unlabelled set size: 16
	
Mean region size: 82.13039485766718
Incorrectly Classified Instances: 42.62626262626262
Correctly Classified Instances: 57.37373737373738
Weighted Precision: 0.5834866330252997
Weighted AreaUnderROC: 0.908260381593715
Root mean squared error: 0.2691760897437346
Relative absolute error: 91.91111111111046
Root relative squared error: 93.63304887728967
Weighted TruePositiveRate: 0.5737373737373738
Weighted MatthewsCorrelation: 0.5333494050744786
Weighted FMeasure: 0.5717483761856126
Iteration time: 533.0
Weighted AreaUnderPRC: 0.5072459639202841
Mean absolute error: 0.15191919191919181
Coverage of cases: 99.5959595959596
Instances selection time: 0.0
Test time: 10.0
Accumulative iteration time: 9145.0
Weighted Recall: 0.5737373737373738
Weighted FalsePositiveRate: 0.04262626262626264
Kappa statistic: 0.5311111111111111
Training time: 533.0
		
Time end:Wed Nov 01 15.00.03 EET 2017