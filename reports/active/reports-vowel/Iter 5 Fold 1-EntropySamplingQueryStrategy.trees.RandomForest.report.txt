Wed Nov 01 14.51.02 EET 2017
Dataset: vowel
Test set size: 495
Initial Labelled set size: 99
Initial Unlabelled set size: 396
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Nov 01 14.51.02 EET 2017
		
Iteration: 1
Labeled set size: 99
Unlabelled set size: 396
	
Mean region size: 43.19559228650154
Incorrectly Classified Instances: 67.27272727272727
Correctly Classified Instances: 32.72727272727273
Weighted Precision: 0.3441561051223121
Weighted AreaUnderROC: 0.7477553310886644
Root mean squared error: 0.2777395483328387
Relative absolute error: 82.72736251030325
Root relative squared error: 96.61185259420077
Weighted TruePositiveRate: 0.32727272727272727
Weighted MatthewsCorrelation: 0.2650739023107312
Weighted FMeasure: 0.32540441263001113
Iteration time: 20.0
Weighted AreaUnderPRC: 0.35974922111126584
Mean absolute error: 0.13673944216579137
Coverage of cases: 73.13131313131314
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 20.0
Weighted Recall: 0.32727272727272727
Weighted FalsePositiveRate: 0.06727272727272728
Kappa statistic: 0.26
Training time: 18.0
		
Iteration: 2
Labeled set size: 119
Unlabelled set size: 376
	
Mean region size: 42.16712580348966
Incorrectly Classified Instances: 56.56565656565657
Correctly Classified Instances: 43.43434343434343
Weighted Precision: 0.45361230472440045
Weighted AreaUnderROC: 0.801726150392817
Root mean squared error: 0.25696837106973847
Relative absolute error: 75.79676747023146
Root relative squared error: 89.38658731240587
Weighted TruePositiveRate: 0.43434343434343436
Weighted MatthewsCorrelation: 0.3833878140960563
Weighted FMeasure: 0.43334306180849347
Iteration time: 17.0
Weighted AreaUnderPRC: 0.4690901461384414
Mean absolute error: 0.12528391317393708
Coverage of cases: 82.82828282828282
Instances selection time: 2.0
Test time: 6.0
Accumulative iteration time: 37.0
Weighted Recall: 0.43434343434343436
Weighted FalsePositiveRate: 0.056565656565656576
Kappa statistic: 0.37777777777777777
Training time: 15.0
		
Iteration: 3
Labeled set size: 139
Unlabelled set size: 356
	
Mean region size: 42.88337924701573
Incorrectly Classified Instances: 54.14141414141414
Correctly Classified Instances: 45.85858585858586
Weighted Precision: 0.4759584652540041
Weighted AreaUnderROC: 0.8225813692480359
Root mean squared error: 0.25348845823946775
Relative absolute error: 74.8371478285573
Root relative squared error: 88.17609774613105
Weighted TruePositiveRate: 0.4585858585858586
Weighted MatthewsCorrelation: 0.41001766148910457
Weighted FMeasure: 0.4587635611349326
Iteration time: 21.0
Weighted AreaUnderPRC: 0.4914655997641456
Mean absolute error: 0.12369776500588063
Coverage of cases: 83.83838383838383
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 58.0
Weighted Recall: 0.4585858585858586
Weighted FalsePositiveRate: 0.05414141414141415
Kappa statistic: 0.4044444444444445
Training time: 19.0
		
Iteration: 4
Labeled set size: 159
Unlabelled set size: 336
	
Mean region size: 40.16528925619847
Incorrectly Classified Instances: 47.676767676767675
Correctly Classified Instances: 52.323232323232325
Weighted Precision: 0.541687881855404
Weighted AreaUnderROC: 0.8549539842873175
Root mean squared error: 0.23956099981344245
Relative absolute error: 69.52666734197632
Root relative squared error: 83.33142377534126
Weighted TruePositiveRate: 0.5232323232323233
Weighted MatthewsCorrelation: 0.48188173702470943
Weighted FMeasure: 0.5245857205216126
Iteration time: 24.0
Weighted AreaUnderPRC: 0.563357861332041
Mean absolute error: 0.11492011130905251
Coverage of cases: 87.67676767676768
Instances selection time: 2.0
Test time: 6.0
Accumulative iteration time: 82.0
Weighted Recall: 0.5232323232323233
Weighted FalsePositiveRate: 0.04767676767676768
Kappa statistic: 0.47555555555555556
Training time: 22.0
		
Iteration: 5
Labeled set size: 179
Unlabelled set size: 316
	
Mean region size: 39.87144168962358
Incorrectly Classified Instances: 45.656565656565654
Correctly Classified Instances: 54.343434343434346
Weighted Precision: 0.5619032243361268
Weighted AreaUnderROC: 0.8514141414141415
Root mean squared error: 0.24076764937998213
Relative absolute error: 70.02273330579487
Root relative squared error: 83.7511574818125
Weighted TruePositiveRate: 0.5434343434343434
Weighted MatthewsCorrelation: 0.503602797384284
Weighted FMeasure: 0.5432029137164589
Iteration time: 28.0
Weighted AreaUnderPRC: 0.5600526640381317
Mean absolute error: 0.11574005505090136
Coverage of cases: 84.84848484848484
Instances selection time: 2.0
Test time: 6.0
Accumulative iteration time: 110.0
Weighted Recall: 0.5434343434343434
Weighted FalsePositiveRate: 0.045656565656565645
Kappa statistic: 0.4977777777777777
Training time: 26.0
		
Iteration: 6
Labeled set size: 199
Unlabelled set size: 296
	
Mean region size: 39.44903581267229
Incorrectly Classified Instances: 43.03030303030303
Correctly Classified Instances: 56.96969696969697
Weighted Precision: 0.5995199514556873
Weighted AreaUnderROC: 0.8643928170594837
Root mean squared error: 0.23516314156994703
Relative absolute error: 67.67771565101657
Root relative squared error: 81.80162639898205
Weighted TruePositiveRate: 0.5696969696969697
Weighted MatthewsCorrelation: 0.5372936852119078
Weighted FMeasure: 0.572768476495943
Iteration time: 27.0
Weighted AreaUnderPRC: 0.5817685989751293
Mean absolute error: 0.11186399281159835
Coverage of cases: 87.87878787878788
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 137.0
Weighted Recall: 0.5696969696969697
Weighted FalsePositiveRate: 0.04303030303030304
Kappa statistic: 0.5266666666666666
Training time: 25.0
		
Iteration: 7
Labeled set size: 219
Unlabelled set size: 276
	
Mean region size: 38.769513314968044
Incorrectly Classified Instances: 40.2020202020202
Correctly Classified Instances: 59.7979797979798
Weighted Precision: 0.6129637313809627
Weighted AreaUnderROC: 0.8825005611672279
Root mean squared error: 0.22850379188049127
Relative absolute error: 65.22241560822899
Root relative squared error: 79.48516799601839
Weighted TruePositiveRate: 0.597979797979798
Weighted MatthewsCorrelation: 0.5621446329188052
Weighted FMeasure: 0.5970512309448512
Iteration time: 65.0
Weighted AreaUnderPRC: 0.634199356651602
Mean absolute error: 0.10780564563343704
Coverage of cases: 88.88888888888889
Instances selection time: 8.0
Test time: 6.0
Accumulative iteration time: 202.0
Weighted Recall: 0.597979797979798
Weighted FalsePositiveRate: 0.04020202020202021
Kappa statistic: 0.5577777777777777
Training time: 57.0
		
Iteration: 8
Labeled set size: 239
Unlabelled set size: 256
	
Mean region size: 37.70431588613427
Incorrectly Classified Instances: 35.95959595959596
Correctly Classified Instances: 64.04040404040404
Weighted Precision: 0.6578884670557479
Weighted AreaUnderROC: 0.8987721661054995
Root mean squared error: 0.2206707572958937
Relative absolute error: 62.75782773980407
Root relative squared error: 76.76044266541649
Weighted TruePositiveRate: 0.6404040404040404
Weighted MatthewsCorrelation: 0.6095430398298973
Weighted FMeasure: 0.6395319325582697
Iteration time: 33.0
Weighted AreaUnderPRC: 0.6757346600854576
Mean absolute error: 0.10373194667736277
Coverage of cases: 89.8989898989899
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 235.0
Weighted Recall: 0.6404040404040404
Weighted FalsePositiveRate: 0.03595959595959596
Kappa statistic: 0.6044444444444445
Training time: 31.0
		
Iteration: 9
Labeled set size: 259
Unlabelled set size: 236
	
Mean region size: 36.51056014692394
Incorrectly Classified Instances: 34.54545454545455
Correctly Classified Instances: 65.45454545454545
Weighted Precision: 0.6754278392589799
Weighted AreaUnderROC: 0.9111739618406286
Root mean squared error: 0.21833961734841026
Relative absolute error: 60.92750022015298
Root relative squared error: 75.94955436976466
Weighted TruePositiveRate: 0.6545454545454545
Weighted MatthewsCorrelation: 0.6273598608938563
Weighted FMeasure: 0.6569527902627702
Iteration time: 37.0
Weighted AreaUnderPRC: 0.6810286527555098
Mean absolute error: 0.1007066119341378
Coverage of cases: 91.31313131313131
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 272.0
Weighted Recall: 0.6545454545454545
Weighted FalsePositiveRate: 0.034545454545454546
Kappa statistic: 0.62
Training time: 36.0
		
Iteration: 10
Labeled set size: 279
Unlabelled set size: 216
	
Mean region size: 36.85950413223154
Incorrectly Classified Instances: 31.515151515151516
Correctly Classified Instances: 68.48484848484848
Weighted Precision: 0.7036719330585632
Weighted AreaUnderROC: 0.9168619528619529
Root mean squared error: 0.21244314858727192
Relative absolute error: 59.27648888230344
Root relative squared error: 73.89846451166954
Weighted TruePositiveRate: 0.6848484848484848
Weighted MatthewsCorrelation: 0.6582510855671448
Weighted FMeasure: 0.6823465952365494
Iteration time: 40.0
Weighted AreaUnderPRC: 0.7204486190785601
Mean absolute error: 0.0979776675740559
Coverage of cases: 90.70707070707071
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 312.0
Weighted Recall: 0.6848484848484848
Weighted FalsePositiveRate: 0.03151515151515152
Kappa statistic: 0.6533333333333333
Training time: 39.0
		
Iteration: 11
Labeled set size: 299
Unlabelled set size: 196
	
Mean region size: 35.6290174471994
Incorrectly Classified Instances: 26.86868686868687
Correctly Classified Instances: 73.13131313131314
Weighted Precision: 0.7393576245474213
Weighted AreaUnderROC: 0.9376992143658811
Root mean squared error: 0.20686792171353857
Relative absolute error: 57.33328117952476
Root relative squared error: 71.95911881842004
Weighted TruePositiveRate: 0.7313131313131314
Weighted MatthewsCorrelation: 0.706879151138022
Weighted FMeasure: 0.7311586477756088
Iteration time: 44.0
Weighted AreaUnderPRC: 0.7438536005586307
Mean absolute error: 0.09476575401574401
Coverage of cases: 95.35353535353535
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 356.0
Weighted Recall: 0.7313131313131314
Weighted FalsePositiveRate: 0.02686868686868687
Kappa statistic: 0.7044444444444445
Training time: 43.0
		
Iteration: 12
Labeled set size: 319
Unlabelled set size: 176
	
Mean region size: 35.75757575757591
Incorrectly Classified Instances: 29.09090909090909
Correctly Classified Instances: 70.9090909090909
Weighted Precision: 0.7250125043156942
Weighted AreaUnderROC: 0.9278720538720541
Root mean squared error: 0.20437897798356847
Relative absolute error: 56.08604461964815
Root relative squared error: 71.09333839140297
Weighted TruePositiveRate: 0.7090909090909091
Weighted MatthewsCorrelation: 0.6848385656490895
Weighted FMeasure: 0.7086617428859099
Iteration time: 47.0
Weighted AreaUnderPRC: 0.7521876703363011
Mean absolute error: 0.09270420598289009
Coverage of cases: 92.92929292929293
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 403.0
Weighted Recall: 0.7090909090909091
Weighted FalsePositiveRate: 0.02909090909090909
Kappa statistic: 0.68
Training time: 45.0
		
Iteration: 13
Labeled set size: 339
Unlabelled set size: 156
	
Mean region size: 34.87603305785135
Incorrectly Classified Instances: 23.03030303030303
Correctly Classified Instances: 76.96969696969697
Weighted Precision: 0.7756305507382087
Weighted AreaUnderROC: 0.9419214365881035
Root mean squared error: 0.19401549504610985
Relative absolute error: 53.449829747962546
Root relative squared error: 67.4883952281902
Weighted TruePositiveRate: 0.7696969696969697
Weighted MatthewsCorrelation: 0.7475579871921464
Weighted FMeasure: 0.7672021212648175
Iteration time: 47.0
Weighted AreaUnderPRC: 0.799363818543724
Mean absolute error: 0.08834682602969073
Coverage of cases: 93.13131313131314
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 450.0
Weighted Recall: 0.7696969696969697
Weighted FalsePositiveRate: 0.02303030303030303
Kappa statistic: 0.7466666666666666
Training time: 45.0
		
Iteration: 14
Labeled set size: 359
Unlabelled set size: 136
	
Mean region size: 34.32506887052351
Incorrectly Classified Instances: 24.646464646464647
Correctly Classified Instances: 75.35353535353535
Weighted Precision: 0.7721820177745827
Weighted AreaUnderROC: 0.9574837261503928
Root mean squared error: 0.1898814582672937
Relative absolute error: 51.60304882693328
Root relative squared error: 66.05036829147409
Weighted TruePositiveRate: 0.7535353535353535
Weighted MatthewsCorrelation: 0.7346505052959084
Weighted FMeasure: 0.7538629498150933
Iteration time: 59.0
Weighted AreaUnderPRC: 0.8230046700712887
Mean absolute error: 0.08529429558170845
Coverage of cases: 96.36363636363636
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 509.0
Weighted Recall: 0.7535353535353535
Weighted FalsePositiveRate: 0.02464646464646465
Kappa statistic: 0.7288888888888888
Training time: 58.0
		
Iteration: 15
Labeled set size: 379
Unlabelled set size: 116
	
Mean region size: 32.63544536271814
Incorrectly Classified Instances: 19.19191919191919
Correctly Classified Instances: 80.8080808080808
Weighted Precision: 0.8117236603552005
Weighted AreaUnderROC: 0.9653153759820426
Root mean squared error: 0.17848608338321048
Relative absolute error: 47.44423933222982
Root relative squared error: 62.086480954704285
Weighted TruePositiveRate: 0.8080808080808081
Weighted MatthewsCorrelation: 0.7898402471670404
Weighted FMeasure: 0.80762599543149
Iteration time: 58.0
Weighted AreaUnderPRC: 0.8523514571856635
Mean absolute error: 0.07842023030120682
Coverage of cases: 96.76767676767676
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 567.0
Weighted Recall: 0.8080808080808081
Weighted FalsePositiveRate: 0.01919191919191919
Kappa statistic: 0.7888888888888889
Training time: 57.0
		
Iteration: 16
Labeled set size: 399
Unlabelled set size: 96
	
Mean region size: 32.929292929293005
Incorrectly Classified Instances: 19.19191919191919
Correctly Classified Instances: 80.8080808080808
Weighted Precision: 0.8207809836490604
Weighted AreaUnderROC: 0.966736251402918
Root mean squared error: 0.1797464424994341
Relative absolute error: 48.16353794567475
Root relative squared error: 62.524897557176864
Weighted TruePositiveRate: 0.8080808080808081
Weighted MatthewsCorrelation: 0.7928292686999266
Weighted FMeasure: 0.808242352559691
Iteration time: 59.0
Weighted AreaUnderPRC: 0.85462499195877
Mean absolute error: 0.07960915362921496
Coverage of cases: 97.37373737373737
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 626.0
Weighted Recall: 0.8080808080808081
Weighted FalsePositiveRate: 0.01919191919191919
Kappa statistic: 0.7888888888888889
Training time: 58.0
		
Iteration: 17
Labeled set size: 419
Unlabelled set size: 76
	
Mean region size: 33.97612488521594
Incorrectly Classified Instances: 18.78787878787879
Correctly Classified Instances: 81.21212121212122
Weighted Precision: 0.821591147790176
Weighted AreaUnderROC: 0.9760875420875421
Root mean squared error: 0.17781644694952023
Relative absolute error: 47.926803726767496
Root relative squared error: 61.853547557887985
Weighted TruePositiveRate: 0.8121212121212121
Weighted MatthewsCorrelation: 0.7968279981196087
Weighted FMeasure: 0.8137629247208635
Iteration time: 67.0
Weighted AreaUnderPRC: 0.8653615074899824
Mean absolute error: 0.0792178573996162
Coverage of cases: 97.17171717171718
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 693.0
Weighted Recall: 0.8121212121212121
Weighted FalsePositiveRate: 0.018787878787878787
Kappa statistic: 0.7933333333333333
Training time: 66.0
		
Iteration: 18
Labeled set size: 439
Unlabelled set size: 56
	
Mean region size: 33.406795224977124
Incorrectly Classified Instances: 15.353535353535353
Correctly Classified Instances: 84.64646464646465
Weighted Precision: 0.8528267421938304
Weighted AreaUnderROC: 0.9842199775533109
Root mean squared error: 0.1709887188627722
Relative absolute error: 46.28759776327213
Root relative squared error: 59.478518638060976
Weighted TruePositiveRate: 0.8464646464646465
Weighted MatthewsCorrelation: 0.8328910635858167
Weighted FMeasure: 0.8459864661901807
Iteration time: 72.0
Weighted AreaUnderPRC: 0.8954604059537807
Mean absolute error: 0.07650842605499575
Coverage of cases: 98.98989898989899
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 765.0
Weighted Recall: 0.8464646464646465
Weighted FalsePositiveRate: 0.015353535353535357
Kappa statistic: 0.8311111111111111
Training time: 71.0
		
Iteration: 19
Labeled set size: 459
Unlabelled set size: 36
	
Mean region size: 32.4517906336089
Incorrectly Classified Instances: 15.95959595959596
Correctly Classified Instances: 84.04040404040404
Weighted Precision: 0.8476007764475538
Weighted AreaUnderROC: 0.9824938271604938
Root mean squared error: 0.16824171515802447
Relative absolute error: 44.63774355937071
Root relative squared error: 58.522971908789394
Weighted TruePositiveRate: 0.8404040404040404
Weighted MatthewsCorrelation: 0.8264941872327861
Weighted FMeasure: 0.8400138428733639
Iteration time: 71.0
Weighted AreaUnderPRC: 0.893924663029886
Mean absolute error: 0.07378139431300991
Coverage of cases: 99.79797979797979
Instances selection time: 0.0
Test time: 6.0
Accumulative iteration time: 836.0
Weighted Recall: 0.8404040404040404
Weighted FalsePositiveRate: 0.015959595959595962
Kappa statistic: 0.8244444444444444
Training time: 71.0
		
Iteration: 20
Labeled set size: 479
Unlabelled set size: 16
	
Mean region size: 32.984389348025836
Incorrectly Classified Instances: 14.747474747474747
Correctly Classified Instances: 85.25252525252525
Weighted Precision: 0.854983489617408
Weighted AreaUnderROC: 0.9847946127946129
Root mean squared error: 0.16622908629851044
Relative absolute error: 44.27205297220263
Root relative squared error: 57.82287786791774
Weighted TruePositiveRate: 0.8525252525252526
Weighted MatthewsCorrelation: 0.8383022086022063
Weighted FMeasure: 0.8519127262793871
Iteration time: 75.0
Weighted AreaUnderPRC: 0.8966469425685172
Mean absolute error: 0.07317694706149241
Coverage of cases: 99.39393939393939
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 911.0
Weighted Recall: 0.8525252525252526
Weighted FalsePositiveRate: 0.014747474747474749
Kappa statistic: 0.8377777777777778
Training time: 74.0
		
Time end:Wed Nov 01 14.51.04 EET 2017