Wed Nov 01 14.51.14 EET 2017
Dataset: vowel
Test set size: 495
Initial Labelled set size: 99
Initial Unlabelled set size: 396
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Wed Nov 01 14.51.14 EET 2017
		
Iteration: 1
Labeled set size: 99
Unlabelled set size: 396
	
Mean region size: 19.559228650137793
Incorrectly Classified Instances: 54.14141414141414
Correctly Classified Instances: 45.85858585858586
Weighted Precision: 0.4620053958009882
Weighted AreaUnderROC: 0.8832143658810325
Root mean squared error: 0.2765821688286196
Relative absolute error: 61.97512336997083
Root relative squared error: 96.2092575056426
Weighted TruePositiveRate: 0.4585858585858586
Weighted MatthewsCorrelation: 0.40301024369334676
Weighted FMeasure: 0.4511635354977387
Iteration time: 46.0
Weighted AreaUnderPRC: 0.48166697877031145
Mean absolute error: 0.10243822044623344
Coverage of cases: 69.29292929292929
Instances selection time: 45.0
Test time: 35.0
Accumulative iteration time: 46.0
Weighted Recall: 0.4585858585858586
Weighted FalsePositiveRate: 0.05414141414141414
Kappa statistic: 0.4044444444444445
Training time: 1.0
		
Iteration: 2
Labeled set size: 119
Unlabelled set size: 376
	
Mean region size: 19.834710743801708
Incorrectly Classified Instances: 53.73737373737374
Correctly Classified Instances: 46.26262626262626
Weighted Precision: 0.46696088178701745
Weighted AreaUnderROC: 0.8849292929292929
Root mean squared error: 0.27319286372366425
Relative absolute error: 61.00007337010739
Root relative squared error: 95.03028588578415
Weighted TruePositiveRate: 0.4626262626262626
Weighted MatthewsCorrelation: 0.4076610460908969
Weighted FMeasure: 0.45495018805682463
Iteration time: 44.0
Weighted AreaUnderPRC: 0.49931499126827195
Mean absolute error: 0.10082656755389716
Coverage of cases: 69.29292929292929
Instances selection time: 43.0
Test time: 40.0
Accumulative iteration time: 90.0
Weighted Recall: 0.4626262626262626
Weighted FalsePositiveRate: 0.053737373737373736
Kappa statistic: 0.40888888888888886
Training time: 1.0
		
Iteration: 3
Labeled set size: 139
Unlabelled set size: 356
	
Mean region size: 20.202020202020243
Incorrectly Classified Instances: 51.91919191919192
Correctly Classified Instances: 48.08080808080808
Weighted Precision: 0.4950685430233011
Weighted AreaUnderROC: 0.8994523007856341
Root mean squared error: 0.2679658116138192
Relative absolute error: 59.538303376169935
Root relative squared error: 93.21205297307928
Weighted TruePositiveRate: 0.4808080808080808
Weighted MatthewsCorrelation: 0.43242252506694373
Weighted FMeasure: 0.47755994849568456
Iteration time: 41.0
Weighted AreaUnderPRC: 0.534210731833387
Mean absolute error: 0.09841041880358731
Coverage of cases: 73.73737373737374
Instances selection time: 40.0
Test time: 39.0
Accumulative iteration time: 131.0
Weighted Recall: 0.4808080808080808
Weighted FalsePositiveRate: 0.05191919191919192
Kappa statistic: 0.4288888888888889
Training time: 1.0
		
Iteration: 4
Labeled set size: 159
Unlabelled set size: 336
	
Mean region size: 20.40404040404045
Incorrectly Classified Instances: 50.505050505050505
Correctly Classified Instances: 49.494949494949495
Weighted Precision: 0.4937618424106911
Weighted AreaUnderROC: 0.8971537598204264
Root mean squared error: 0.2630464977666664
Relative absolute error: 58.468217707689064
Root relative squared error: 91.50086698203637
Weighted TruePositiveRate: 0.494949494949495
Weighted MatthewsCorrelation: 0.4409385496757198
Weighted FMeasure: 0.4859832770849739
Iteration time: 38.0
Weighted AreaUnderPRC: 0.5356825738139621
Mean absolute error: 0.09664168216147015
Coverage of cases: 72.12121212121212
Instances selection time: 37.0
Test time: 39.0
Accumulative iteration time: 169.0
Weighted Recall: 0.494949494949495
Weighted FalsePositiveRate: 0.050505050505050504
Kappa statistic: 0.44444444444444453
Training time: 1.0
		
Iteration: 5
Labeled set size: 179
Unlabelled set size: 316
	
Mean region size: 20.40404040404044
Incorrectly Classified Instances: 50.303030303030305
Correctly Classified Instances: 49.696969696969695
Weighted Precision: 0.49185985075634414
Weighted AreaUnderROC: 0.8960583613916947
Root mean squared error: 0.2623040064970426
Relative absolute error: 58.37099660799575
Root relative squared error: 91.24259099100823
Weighted TruePositiveRate: 0.49696969696969695
Weighted MatthewsCorrelation: 0.4413598237764575
Weighted FMeasure: 0.4863265812314661
Iteration time: 36.0
Weighted AreaUnderPRC: 0.5254216871840293
Mean absolute error: 0.09648098612891921
Coverage of cases: 71.71717171717172
Instances selection time: 35.0
Test time: 37.0
Accumulative iteration time: 205.0
Weighted Recall: 0.49696969696969695
Weighted FalsePositiveRate: 0.05030303030303031
Kappa statistic: 0.4466666666666666
Training time: 1.0
		
Iteration: 6
Labeled set size: 199
Unlabelled set size: 296
	
Mean region size: 20.29384756657489
Incorrectly Classified Instances: 50.101010101010104
Correctly Classified Instances: 49.898989898989896
Weighted Precision: 0.49496716901017446
Weighted AreaUnderROC: 0.8980112233445567
Root mean squared error: 0.26311844996268435
Relative absolute error: 59.076663729272035
Root relative squared error: 91.52589559246381
Weighted TruePositiveRate: 0.498989898989899
Weighted MatthewsCorrelation: 0.44176487249530993
Weighted FMeasure: 0.4821964962889822
Iteration time: 33.0
Weighted AreaUnderPRC: 0.5236361365490597
Mean absolute error: 0.09764737806491308
Coverage of cases: 71.11111111111111
Instances selection time: 32.0
Test time: 36.0
Accumulative iteration time: 238.0
Weighted Recall: 0.498989898989899
Weighted FalsePositiveRate: 0.050101010101010104
Kappa statistic: 0.44888888888888884
Training time: 1.0
		
Iteration: 7
Labeled set size: 219
Unlabelled set size: 276
	
Mean region size: 21.157024793388462
Incorrectly Classified Instances: 48.484848484848484
Correctly Classified Instances: 51.515151515151516
Weighted Precision: 0.5086399423173492
Weighted AreaUnderROC: 0.8994388327721661
Root mean squared error: 0.2611020887564232
Relative absolute error: 59.026107685697966
Root relative squared error: 90.82450325275094
Weighted TruePositiveRate: 0.5151515151515151
Weighted MatthewsCorrelation: 0.45726226911888423
Weighted FMeasure: 0.4943370280307505
Iteration time: 32.0
Weighted AreaUnderPRC: 0.5239055010049187
Mean absolute error: 0.09756381435652618
Coverage of cases: 73.93939393939394
Instances selection time: 31.0
Test time: 43.0
Accumulative iteration time: 270.0
Weighted Recall: 0.5151515151515151
Weighted FalsePositiveRate: 0.048484848484848485
Kappa statistic: 0.4666666666666666
Training time: 1.0
		
Iteration: 8
Labeled set size: 239
Unlabelled set size: 256
	
Mean region size: 21.377410468319564
Incorrectly Classified Instances: 47.676767676767675
Correctly Classified Instances: 52.323232323232325
Weighted Precision: 0.5212238641871697
Weighted AreaUnderROC: 0.9093782267115601
Root mean squared error: 0.25440962194588185
Relative absolute error: 57.13908126353399
Root relative squared error: 88.4965250412477
Weighted TruePositiveRate: 0.5232323232323233
Weighted MatthewsCorrelation: 0.4674797475913416
Weighted FMeasure: 0.5019736435064316
Iteration time: 41.0
Weighted AreaUnderPRC: 0.5597645204615916
Mean absolute error: 0.09444476241906505
Coverage of cases: 74.34343434343434
Instances selection time: 37.0
Test time: 36.0
Accumulative iteration time: 311.0
Weighted Recall: 0.5232323232323233
Weighted FalsePositiveRate: 0.04767676767676768
Kappa statistic: 0.47555555555555556
Training time: 4.0
		
Iteration: 9
Labeled set size: 259
Unlabelled set size: 236
	
Mean region size: 21.340679522497705
Incorrectly Classified Instances: 43.83838383838384
Correctly Classified Instances: 56.16161616161616
Weighted Precision: 0.5779233416265139
Weighted AreaUnderROC: 0.9140291806958474
Root mean squared error: 0.24591922356325555
Relative absolute error: 54.58036495717194
Root relative squared error: 85.54313535680372
Weighted TruePositiveRate: 0.5616161616161616
Weighted MatthewsCorrelation: 0.5132622819851088
Weighted FMeasure: 0.5369980073591432
Iteration time: 28.0
Weighted AreaUnderPRC: 0.5966295137877269
Mean absolute error: 0.09021547926805336
Coverage of cases: 76.16161616161617
Instances selection time: 27.0
Test time: 34.0
Accumulative iteration time: 339.0
Weighted Recall: 0.5616161616161616
Weighted FalsePositiveRate: 0.04383838383838384
Kappa statistic: 0.5177777777777777
Training time: 1.0
		
Iteration: 10
Labeled set size: 279
Unlabelled set size: 216
	
Mean region size: 21.395775941230486
Incorrectly Classified Instances: 44.64646464646464
Correctly Classified Instances: 55.35353535353536
Weighted Precision: 0.569146851619844
Weighted AreaUnderROC: 0.9143838383838384
Root mean squared error: 0.24651520642981867
Relative absolute error: 54.703140232729986
Root relative squared error: 85.75044832032904
Weighted TruePositiveRate: 0.5535353535353535
Weighted MatthewsCorrelation: 0.5041035734515471
Weighted FMeasure: 0.528828867908031
Iteration time: 25.0
Weighted AreaUnderPRC: 0.5976237910166852
Mean absolute error: 0.09041841360781874
Coverage of cases: 75.95959595959596
Instances selection time: 24.0
Test time: 34.0
Accumulative iteration time: 364.0
Weighted Recall: 0.5535353535353535
Weighted FalsePositiveRate: 0.044646464646464656
Kappa statistic: 0.5088888888888888
Training time: 1.0
		
Iteration: 11
Labeled set size: 299
Unlabelled set size: 196
	
Mean region size: 22.64462809917354
Incorrectly Classified Instances: 44.84848484848485
Correctly Classified Instances: 55.15151515151515
Weighted Precision: 0.5741585133486302
Weighted AreaUnderROC: 0.9230931537598203
Root mean squared error: 0.2427024362026765
Relative absolute error: 54.75426070041766
Root relative squared error: 84.4241741279379
Weighted TruePositiveRate: 0.5515151515151515
Weighted MatthewsCorrelation: 0.5042371827173631
Weighted FMeasure: 0.5285867097193182
Iteration time: 23.0
Weighted AreaUnderPRC: 0.5979520791705439
Mean absolute error: 0.09050291024862482
Coverage of cases: 81.01010101010101
Instances selection time: 21.0
Test time: 34.0
Accumulative iteration time: 387.0
Weighted Recall: 0.5515151515151515
Weighted FalsePositiveRate: 0.044848484848484846
Kappa statistic: 0.5066666666666666
Training time: 2.0
		
Iteration: 12
Labeled set size: 319
Unlabelled set size: 176
	
Mean region size: 22.66299357208446
Incorrectly Classified Instances: 42.42424242424242
Correctly Classified Instances: 57.57575757575758
Weighted Precision: 0.6040532273369265
Weighted AreaUnderROC: 0.9272368125701459
Root mean squared error: 0.237369097694616
Relative absolute error: 53.66339432407417
Root relative squared error: 82.56896943394081
Weighted TruePositiveRate: 0.5757575757575758
Weighted MatthewsCorrelation: 0.5348148229457809
Weighted FMeasure: 0.5592322111011541
Iteration time: 19.0
Weighted AreaUnderPRC: 0.6063233017743502
Mean absolute error: 0.08869982532904878
Coverage of cases: 81.81818181818181
Instances selection time: 18.0
Test time: 36.0
Accumulative iteration time: 406.0
Weighted Recall: 0.5757575757575758
Weighted FalsePositiveRate: 0.042424242424242434
Kappa statistic: 0.5333333333333333
Training time: 1.0
		
Iteration: 13
Labeled set size: 339
Unlabelled set size: 156
	
Mean region size: 22.5528007346189
Incorrectly Classified Instances: 44.04040404040404
Correctly Classified Instances: 55.95959595959596
Weighted Precision: 0.587484712857584
Weighted AreaUnderROC: 0.9279685746352413
Root mean squared error: 0.24108758040372877
Relative absolute error: 54.52957342417912
Root relative squared error: 83.86244566202326
Weighted TruePositiveRate: 0.5595959595959596
Weighted MatthewsCorrelation: 0.514519107797771
Weighted FMeasure: 0.536614108484308
Iteration time: 18.0
Weighted AreaUnderPRC: 0.6007270829311604
Mean absolute error: 0.09013152632095779
Coverage of cases: 82.42424242424242
Instances selection time: 16.0
Test time: 36.0
Accumulative iteration time: 424.0
Weighted Recall: 0.5595959595959596
Weighted FalsePositiveRate: 0.04404040404040404
Kappa statistic: 0.5155555555555555
Training time: 2.0
		
Iteration: 14
Labeled set size: 359
Unlabelled set size: 136
	
Mean region size: 22.13039485766757
Incorrectly Classified Instances: 43.63636363636363
Correctly Classified Instances: 56.36363636363637
Weighted Precision: 0.5879881417053685
Weighted AreaUnderROC: 0.9298047138047137
Root mean squared error: 0.24030151285640244
Relative absolute error: 54.3845169537602
Root relative squared error: 83.589011639151
Weighted TruePositiveRate: 0.5636363636363636
Weighted MatthewsCorrelation: 0.5181712379417397
Weighted FMeasure: 0.540778032264284
Iteration time: 16.0
Weighted AreaUnderPRC: 0.6058698712200523
Mean absolute error: 0.08989176355993478
Coverage of cases: 81.21212121212122
Instances selection time: 15.0
Test time: 36.0
Accumulative iteration time: 440.0
Weighted Recall: 0.5636363636363636
Weighted FalsePositiveRate: 0.04363636363636364
Kappa statistic: 0.5199999999999999
Training time: 1.0
		
Iteration: 15
Labeled set size: 379
Unlabelled set size: 116
	
Mean region size: 22.773186409550025
Incorrectly Classified Instances: 42.22222222222222
Correctly Classified Instances: 57.77777777777778
Weighted Precision: 0.6027097437417811
Weighted AreaUnderROC: 0.9366644219977553
Root mean squared error: 0.2344330333839472
Relative absolute error: 53.41568081693101
Root relative squared error: 81.5476578703118
Weighted TruePositiveRate: 0.5777777777777777
Weighted MatthewsCorrelation: 0.5347676060876558
Weighted FMeasure: 0.557601274136104
Iteration time: 14.0
Weighted AreaUnderPRC: 0.6265784489187194
Mean absolute error: 0.088290381515589
Coverage of cases: 83.43434343434343
Instances selection time: 12.0
Test time: 36.0
Accumulative iteration time: 454.0
Weighted Recall: 0.5777777777777777
Weighted FalsePositiveRate: 0.04222222222222222
Kappa statistic: 0.5355555555555555
Training time: 2.0
		
Iteration: 16
Labeled set size: 399
Unlabelled set size: 96
	
Mean region size: 23.250688705234154
Incorrectly Classified Instances: 41.01010101010101
Correctly Classified Instances: 58.98989898989899
Weighted Precision: 0.611158409410657
Weighted AreaUnderROC: 0.939551066217733
Root mean squared error: 0.2301567492357022
Relative absolute error: 52.569213388456106
Root relative squared error: 80.06015010895379
Weighted TruePositiveRate: 0.5898989898989899
Weighted MatthewsCorrelation: 0.5450999557906252
Weighted FMeasure: 0.5669880509762427
Iteration time: 12.0
Weighted AreaUnderPRC: 0.6391174418862825
Mean absolute error: 0.08689126179910156
Coverage of cases: 86.26262626262626
Instances selection time: 10.0
Test time: 36.0
Accumulative iteration time: 466.0
Weighted Recall: 0.5898989898989899
Weighted FalsePositiveRate: 0.04101010101010101
Kappa statistic: 0.5488888888888889
Training time: 2.0
		
Iteration: 17
Labeled set size: 419
Unlabelled set size: 76
	
Mean region size: 23.305785123966942
Incorrectly Classified Instances: 40.0
Correctly Classified Instances: 60.0
Weighted Precision: 0.6309847768899228
Weighted AreaUnderROC: 0.9398294051627386
Root mean squared error: 0.22926503289720848
Relative absolute error: 52.0771178891261
Root relative squared error: 79.74996609674695
Weighted TruePositiveRate: 0.6
Weighted MatthewsCorrelation: 0.5591622381698944
Weighted FMeasure: 0.5782529725711846
Iteration time: 9.0
Weighted AreaUnderPRC: 0.6419565271628354
Mean absolute error: 0.08607788080847344
Coverage of cases: 87.87878787878788
Instances selection time: 8.0
Test time: 35.0
Accumulative iteration time: 475.0
Weighted Recall: 0.6
Weighted FalsePositiveRate: 0.04
Kappa statistic: 0.5599999999999999
Training time: 1.0
		
Iteration: 18
Labeled set size: 439
Unlabelled set size: 56
	
Mean region size: 23.856749311294738
Incorrectly Classified Instances: 39.7979797979798
Correctly Classified Instances: 60.2020202020202
Weighted Precision: 0.619943225903817
Weighted AreaUnderROC: 0.9385903479236811
Root mean squared error: 0.23058636569801017
Relative absolute error: 52.9270775033181
Root relative squared error: 80.20959242848546
Weighted TruePositiveRate: 0.602020202020202
Weighted MatthewsCorrelation: 0.5626554651186121
Weighted FMeasure: 0.5911413792963449
Iteration time: 8.0
Weighted AreaUnderPRC: 0.6326278879491197
Mean absolute error: 0.08748277273275774
Coverage of cases: 89.0909090909091
Instances selection time: 6.0
Test time: 34.0
Accumulative iteration time: 483.0
Weighted Recall: 0.602020202020202
Weighted FalsePositiveRate: 0.039797979797979804
Kappa statistic: 0.5622222222222222
Training time: 2.0
		
Iteration: 19
Labeled set size: 459
Unlabelled set size: 36
	
Mean region size: 24.830119375573886
Incorrectly Classified Instances: 39.19191919191919
Correctly Classified Instances: 60.80808080808081
Weighted Precision: 0.6119830475908766
Weighted AreaUnderROC: 0.9388552188552187
Root mean squared error: 0.22903048953916585
Relative absolute error: 53.21753779872005
Root relative squared error: 79.66838006238424
Weighted TruePositiveRate: 0.6080808080808081
Weighted MatthewsCorrelation: 0.5679803173320215
Weighted FMeasure: 0.6025479171692117
Iteration time: 7.0
Weighted AreaUnderPRC: 0.627981327290126
Mean absolute error: 0.08796287239457916
Coverage of cases: 91.91919191919192
Instances selection time: 4.0
Test time: 35.0
Accumulative iteration time: 490.0
Weighted Recall: 0.6080808080808081
Weighted FalsePositiveRate: 0.039191919191919194
Kappa statistic: 0.5688888888888889
Training time: 3.0
		
Iteration: 20
Labeled set size: 479
Unlabelled set size: 16
	
Mean region size: 26.00550964187323
Incorrectly Classified Instances: 39.7979797979798
Correctly Classified Instances: 60.2020202020202
Weighted Precision: 0.601274120779056
Weighted AreaUnderROC: 0.9429315375982041
Root mean squared error: 0.2265580851872659
Relative absolute error: 53.829680753470164
Root relative squared error: 78.80835286700345
Weighted TruePositiveRate: 0.602020202020202
Weighted MatthewsCorrelation: 0.5600847864013264
Weighted FMeasure: 0.5967137198556122
Iteration time: 4.0
Weighted AreaUnderPRC: 0.6349632406781328
Mean absolute error: 0.08897467893135622
Coverage of cases: 94.74747474747475
Instances selection time: 2.0
Test time: 33.0
Accumulative iteration time: 494.0
Weighted Recall: 0.602020202020202
Weighted FalsePositiveRate: 0.0397979797979798
Kappa statistic: 0.5622222222222222
Training time: 2.0
		
Time end:Wed Nov 01 14.51.17 EET 2017