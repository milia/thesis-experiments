Wed Nov 01 14.51.27 EET 2017
Dataset: vowel
Test set size: 495
Initial Labelled set size: 99
Initial Unlabelled set size: 396
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Wed Nov 01 14.51.27 EET 2017
		
Iteration: 1
Labeled set size: 99
Unlabelled set size: 396
	
Mean region size: 21.193755739210275
Incorrectly Classified Instances: 46.26262626262626
Correctly Classified Instances: 53.73737373737374
Weighted Precision: 0.574259792993914
Weighted AreaUnderROC: 0.8937463524130191
Root mean squared error: 0.2554728587616999
Relative absolute error: 57.71251582993313
Root relative squared error: 88.86637254456198
Weighted TruePositiveRate: 0.5373737373737374
Weighted MatthewsCorrelation: 0.5034470551579465
Weighted FMeasure: 0.5400362615434603
Iteration time: 45.0
Weighted AreaUnderPRC: 0.5460307998204044
Mean absolute error: 0.09539258814865041
Coverage of cases: 75.35353535353535
Instances selection time: 44.0
Test time: 34.0
Accumulative iteration time: 45.0
Weighted Recall: 0.5373737373737374
Weighted FalsePositiveRate: 0.04626262626262627
Kappa statistic: 0.4911111111111111
Training time: 1.0
		
Iteration: 2
Labeled set size: 119
Unlabelled set size: 376
	
Mean region size: 20.863177226813598
Incorrectly Classified Instances: 46.86868686868687
Correctly Classified Instances: 53.13131313131313
Weighted Precision: 0.5456297232370269
Weighted AreaUnderROC: 0.9042334455667788
Root mean squared error: 0.25759252370212005
Relative absolute error: 56.99866904570187
Root relative squared error: 89.60369914425658
Weighted TruePositiveRate: 0.5313131313131313
Weighted MatthewsCorrelation: 0.4871182959534983
Weighted FMeasure: 0.5264091734160342
Iteration time: 43.0
Weighted AreaUnderPRC: 0.5637600874037827
Mean absolute error: 0.09421267610859874
Coverage of cases: 73.53535353535354
Instances selection time: 42.0
Test time: 33.0
Accumulative iteration time: 88.0
Weighted Recall: 0.5313131313131313
Weighted FalsePositiveRate: 0.046868686868686865
Kappa statistic: 0.4844444444444444
Training time: 1.0
		
Iteration: 3
Labeled set size: 139
Unlabelled set size: 356
	
Mean region size: 21.30394857667586
Incorrectly Classified Instances: 47.07070707070707
Correctly Classified Instances: 52.92929292929293
Weighted Precision: 0.5461155814298744
Weighted AreaUnderROC: 0.9042244668911336
Root mean squared error: 0.2559499409393038
Relative absolute error: 56.66134009099717
Root relative squared error: 89.03232583891511
Weighted TruePositiveRate: 0.5292929292929293
Weighted MatthewsCorrelation: 0.48484576727438866
Weighted FMeasure: 0.5218296055902268
Iteration time: 39.0
Weighted AreaUnderPRC: 0.5637896720348605
Mean absolute error: 0.09365510758842568
Coverage of cases: 76.16161616161617
Instances selection time: 38.0
Test time: 35.0
Accumulative iteration time: 127.0
Weighted Recall: 0.5292929292929293
Weighted FalsePositiveRate: 0.047070707070707075
Kappa statistic: 0.48222222222222216
Training time: 1.0
		
Iteration: 4
Labeled set size: 159
Unlabelled set size: 336
	
Mean region size: 21.65289256198348
Incorrectly Classified Instances: 45.656565656565654
Correctly Classified Instances: 54.343434343434346
Weighted Precision: 0.5589794283617148
Weighted AreaUnderROC: 0.9073265993265992
Root mean squared error: 0.24938202618843622
Relative absolute error: 55.1405990803916
Root relative squared error: 86.74767312895365
Weighted TruePositiveRate: 0.5434343434343434
Weighted MatthewsCorrelation: 0.5000535862278931
Weighted FMeasure: 0.5361998256918832
Iteration time: 37.0
Weighted AreaUnderPRC: 0.5819202435711319
Mean absolute error: 0.09114148608329248
Coverage of cases: 76.56565656565657
Instances selection time: 37.0
Test time: 34.0
Accumulative iteration time: 164.0
Weighted Recall: 0.5434343434343434
Weighted FalsePositiveRate: 0.04565656565656566
Kappa statistic: 0.4977777777777777
Training time: 0.0
		
Iteration: 5
Labeled set size: 179
Unlabelled set size: 316
	
Mean region size: 21.30394857667585
Incorrectly Classified Instances: 45.25252525252525
Correctly Classified Instances: 54.74747474747475
Weighted Precision: 0.5533699926685723
Weighted AreaUnderROC: 0.9072637485970819
Root mean squared error: 0.2493103834522459
Relative absolute error: 55.06879066277427
Root relative squared error: 86.72275216429517
Weighted TruePositiveRate: 0.5474747474747474
Weighted MatthewsCorrelation: 0.499682343875505
Weighted FMeasure: 0.5348442804854814
Iteration time: 35.0
Weighted AreaUnderPRC: 0.5802485050290798
Mean absolute error: 0.09102279448392499
Coverage of cases: 76.56565656565657
Instances selection time: 34.0
Test time: 35.0
Accumulative iteration time: 199.0
Weighted Recall: 0.5474747474747474
Weighted FalsePositiveRate: 0.04525252525252525
Kappa statistic: 0.5022222222222221
Training time: 1.0
		
Iteration: 6
Labeled set size: 199
Unlabelled set size: 296
	
Mean region size: 21.579430670339775
Incorrectly Classified Instances: 47.27272727272727
Correctly Classified Instances: 52.72727272727273
Weighted Precision: 0.538071832376097
Weighted AreaUnderROC: 0.9098630751964085
Root mean squared error: 0.25126038017575386
Relative absolute error: 55.76357922070713
Root relative squared error: 87.40105958267176
Weighted TruePositiveRate: 0.5272727272727272
Weighted MatthewsCorrelation: 0.47923110437136357
Weighted FMeasure: 0.515143401380461
Iteration time: 33.0
Weighted AreaUnderPRC: 0.5863052036766517
Mean absolute error: 0.09217120532348344
Coverage of cases: 77.17171717171718
Instances selection time: 32.0
Test time: 35.0
Accumulative iteration time: 232.0
Weighted Recall: 0.5272727272727272
Weighted FalsePositiveRate: 0.04727272727272728
Kappa statistic: 0.4799999999999999
Training time: 1.0
		
Iteration: 7
Labeled set size: 219
Unlabelled set size: 276
	
Mean region size: 21.946740128558307
Incorrectly Classified Instances: 47.878787878787875
Correctly Classified Instances: 52.121212121212125
Weighted Precision: 0.5267821978609817
Weighted AreaUnderROC: 0.9119685746352414
Root mean squared error: 0.25176198361325647
Relative absolute error: 56.01234303371797
Root relative squared error: 87.57554261058635
Weighted TruePositiveRate: 0.5212121212121212
Weighted MatthewsCorrelation: 0.4681219077864034
Weighted FMeasure: 0.5015934611995799
Iteration time: 31.0
Weighted AreaUnderPRC: 0.5782041093124822
Mean absolute error: 0.09258238517969972
Coverage of cases: 78.18181818181819
Instances selection time: 30.0
Test time: 35.0
Accumulative iteration time: 263.0
Weighted Recall: 0.5212121212121212
Weighted FalsePositiveRate: 0.047878787878787875
Kappa statistic: 0.47333333333333333
Training time: 1.0
		
Iteration: 8
Labeled set size: 239
Unlabelled set size: 256
	
Mean region size: 21.579430670339775
Incorrectly Classified Instances: 49.494949494949495
Correctly Classified Instances: 50.505050505050505
Weighted Precision: 0.5106025003979137
Weighted AreaUnderROC: 0.9104466891133556
Root mean squared error: 0.25985976428654445
Relative absolute error: 57.75530798188778
Root relative squared error: 90.39236001179535
Weighted TruePositiveRate: 0.5050505050505051
Weighted MatthewsCorrelation: 0.44628574681881467
Weighted FMeasure: 0.47576670211431077
Iteration time: 29.0
Weighted AreaUnderPRC: 0.5647325967197804
Mean absolute error: 0.09546331897832752
Coverage of cases: 77.17171717171718
Instances selection time: 28.0
Test time: 33.0
Accumulative iteration time: 292.0
Weighted Recall: 0.5050505050505051
Weighted FalsePositiveRate: 0.04949494949494949
Kappa statistic: 0.45555555555555555
Training time: 1.0
		
Iteration: 9
Labeled set size: 259
Unlabelled set size: 236
	
Mean region size: 22.71808999081726
Incorrectly Classified Instances: 50.101010101010104
Correctly Classified Instances: 49.898989898989896
Weighted Precision: 0.5004699610288346
Weighted AreaUnderROC: 0.9203815937149271
Root mean squared error: 0.25592523796112127
Relative absolute error: 57.36625885687777
Root relative squared error: 89.02373289455004
Weighted TruePositiveRate: 0.498989898989899
Weighted MatthewsCorrelation: 0.4405324112915508
Weighted FMeasure: 0.4735508891263686
Iteration time: 27.0
Weighted AreaUnderPRC: 0.5844216705449611
Mean absolute error: 0.0948202625733523
Coverage of cases: 81.21212121212122
Instances selection time: 26.0
Test time: 33.0
Accumulative iteration time: 319.0
Weighted Recall: 0.498989898989899
Weighted FalsePositiveRate: 0.050101010101010104
Kappa statistic: 0.44888888888888884
Training time: 1.0
		
Iteration: 10
Labeled set size: 279
Unlabelled set size: 216
	
Mean region size: 22.58953168044076
Incorrectly Classified Instances: 48.484848484848484
Correctly Classified Instances: 51.515151515151516
Weighted Precision: 0.5182506348141024
Weighted AreaUnderROC: 0.9227789001122334
Root mean squared error: 0.25152939504438426
Relative absolute error: 56.46225539252733
Root relative squared error: 87.4946365506973
Weighted TruePositiveRate: 0.5151515151515151
Weighted MatthewsCorrelation: 0.4600900837782974
Weighted FMeasure: 0.49381789737847237
Iteration time: 25.0
Weighted AreaUnderPRC: 0.5913571340362325
Mean absolute error: 0.09332604197112015
Coverage of cases: 80.20202020202021
Instances selection time: 24.0
Test time: 33.0
Accumulative iteration time: 344.0
Weighted Recall: 0.5151515151515151
Weighted FalsePositiveRate: 0.048484848484848485
Kappa statistic: 0.4666666666666666
Training time: 1.0
		
Iteration: 11
Labeled set size: 299
Unlabelled set size: 196
	
Mean region size: 23.122130394857628
Incorrectly Classified Instances: 46.86868686868687
Correctly Classified Instances: 53.13131313131313
Weighted Precision: 0.532422220848617
Weighted AreaUnderROC: 0.9271919191919191
Root mean squared error: 0.2484580714215773
Relative absolute error: 56.48020079373133
Root relative squared error: 86.42627496194682
Weighted TruePositiveRate: 0.5313131313131313
Weighted MatthewsCorrelation: 0.4785016930373806
Weighted FMeasure: 0.5134070304825802
Iteration time: 23.0
Weighted AreaUnderPRC: 0.6000189820337337
Mean absolute error: 0.09335570379129206
Coverage of cases: 84.24242424242425
Instances selection time: 22.0
Test time: 33.0
Accumulative iteration time: 367.0
Weighted Recall: 0.5313131313131313
Weighted FalsePositiveRate: 0.046868686868686865
Kappa statistic: 0.4844444444444444
Training time: 1.0
		
Iteration: 12
Labeled set size: 319
Unlabelled set size: 176
	
Mean region size: 23.34251606978875
Incorrectly Classified Instances: 45.656565656565654
Correctly Classified Instances: 54.343434343434346
Weighted Precision: 0.5592405118224177
Weighted AreaUnderROC: 0.9285656565656565
Root mean squared error: 0.24891706895465648
Relative absolute error: 57.20960546203277
Root relative squared error: 86.58593750288897
Weighted TruePositiveRate: 0.5434343434343434
Weighted MatthewsCorrelation: 0.49626399967451634
Weighted FMeasure: 0.5254107882262636
Iteration time: 19.0
Weighted AreaUnderPRC: 0.5970242177675167
Mean absolute error: 0.09456133134220354
Coverage of cases: 82.82828282828282
Instances selection time: 18.0
Test time: 35.0
Accumulative iteration time: 386.0
Weighted Recall: 0.5434343434343434
Weighted FalsePositiveRate: 0.04565656565656565
Kappa statistic: 0.4977777777777777
Training time: 1.0
		
Iteration: 13
Labeled set size: 339
Unlabelled set size: 156
	
Mean region size: 23.434343434343397
Incorrectly Classified Instances: 44.84848484848485
Correctly Classified Instances: 55.15151515151515
Weighted Precision: 0.5572031827657417
Weighted AreaUnderROC: 0.9288754208754211
Root mean squared error: 0.24502797755572125
Relative absolute error: 55.943166709912624
Root relative squared error: 85.23311494947633
Weighted TruePositiveRate: 0.5515151515151515
Weighted MatthewsCorrelation: 0.5027727389966923
Weighted FMeasure: 0.5352603395260757
Iteration time: 17.0
Weighted AreaUnderPRC: 0.6103336379307177
Mean absolute error: 0.0924680441486165
Coverage of cases: 82.62626262626263
Instances selection time: 16.0
Test time: 34.0
Accumulative iteration time: 403.0
Weighted Recall: 0.5515151515151515
Weighted FalsePositiveRate: 0.044848484848484846
Kappa statistic: 0.5066666666666666
Training time: 1.0
		
Iteration: 14
Labeled set size: 359
Unlabelled set size: 136
	
Mean region size: 23.581267217630824
Incorrectly Classified Instances: 44.44444444444444
Correctly Classified Instances: 55.55555555555556
Weighted Precision: 0.5553930509963656
Weighted AreaUnderROC: 0.9289113355780023
Root mean squared error: 0.24410177304990643
Relative absolute error: 55.990812376889465
Root relative squared error: 84.91093420955292
Weighted TruePositiveRate: 0.5555555555555556
Weighted MatthewsCorrelation: 0.5056145695923243
Weighted FMeasure: 0.5401885452356628
Iteration time: 15.0
Weighted AreaUnderPRC: 0.6154926256582283
Mean absolute error: 0.09254679731717326
Coverage of cases: 81.81818181818181
Instances selection time: 14.0
Test time: 35.0
Accumulative iteration time: 418.0
Weighted Recall: 0.5555555555555556
Weighted FalsePositiveRate: 0.04444444444444445
Kappa statistic: 0.5111111111111111
Training time: 1.0
		
Iteration: 15
Labeled set size: 379
Unlabelled set size: 116
	
Mean region size: 24.20569329660236
Incorrectly Classified Instances: 44.04040404040404
Correctly Classified Instances: 55.95959595959596
Weighted Precision: 0.567077007480637
Weighted AreaUnderROC: 0.9327317620650954
Root mean squared error: 0.24371644046439972
Relative absolute error: 56.569175340648414
Root relative squared error: 84.77689606059582
Weighted TruePositiveRate: 0.5595959595959596
Weighted MatthewsCorrelation: 0.5135826443817082
Weighted FMeasure: 0.5478464345613534
Iteration time: 14.0
Weighted AreaUnderPRC: 0.6145773356982437
Mean absolute error: 0.09350276915809715
Coverage of cases: 84.84848484848484
Instances selection time: 12.0
Test time: 35.0
Accumulative iteration time: 432.0
Weighted Recall: 0.5595959595959596
Weighted FalsePositiveRate: 0.04404040404040404
Kappa statistic: 0.5155555555555555
Training time: 2.0
		
Iteration: 16
Labeled set size: 399
Unlabelled set size: 96
	
Mean region size: 24.38934802571163
Incorrectly Classified Instances: 43.83838383838384
Correctly Classified Instances: 56.16161616161616
Weighted Precision: 0.5647639681852246
Weighted AreaUnderROC: 0.9321661054994388
Root mean squared error: 0.24291122442824814
Relative absolute error: 56.8998025813177
Root relative squared error: 84.49680122549536
Weighted TruePositiveRate: 0.5616161616161616
Weighted MatthewsCorrelation: 0.5134723327822791
Weighted FMeasure: 0.5471681126246876
Iteration time: 11.0
Weighted AreaUnderPRC: 0.614900353000524
Mean absolute error: 0.09404926046498854
Coverage of cases: 85.65656565656566
Instances selection time: 10.0
Test time: 35.0
Accumulative iteration time: 443.0
Weighted Recall: 0.5616161616161616
Weighted FalsePositiveRate: 0.043838383838383836
Kappa statistic: 0.5177777777777777
Training time: 1.0
		
Iteration: 17
Labeled set size: 419
Unlabelled set size: 76
	
Mean region size: 24.334251606978867
Incorrectly Classified Instances: 43.23232323232323
Correctly Classified Instances: 56.76767676767677
Weighted Precision: 0.571170561728498
Weighted AreaUnderROC: 0.9346576879910214
Root mean squared error: 0.23994801651028413
Relative absolute error: 55.86453980781444
Root relative squared error: 83.46604774334008
Weighted TruePositiveRate: 0.5676767676767677
Weighted MatthewsCorrelation: 0.5186375942732546
Weighted FMeasure: 0.5491813099218489
Iteration time: 9.0
Weighted AreaUnderPRC: 0.626914366900045
Mean absolute error: 0.09233808232696661
Coverage of cases: 86.46464646464646
Instances selection time: 8.0
Test time: 33.0
Accumulative iteration time: 452.0
Weighted Recall: 0.5676767676767677
Weighted FalsePositiveRate: 0.043232323232323226
Kappa statistic: 0.5244444444444444
Training time: 1.0
		
Iteration: 18
Labeled set size: 439
Unlabelled set size: 56
	
Mean region size: 24.921946740128558
Incorrectly Classified Instances: 43.63636363636363
Correctly Classified Instances: 56.36363636363637
Weighted Precision: 0.5672593303918169
Weighted AreaUnderROC: 0.9408395061728395
Root mean squared error: 0.23440280207956785
Relative absolute error: 54.79776883753717
Root relative squared error: 81.53714189467937
Weighted TruePositiveRate: 0.5636363636363636
Weighted MatthewsCorrelation: 0.514821138830451
Weighted FMeasure: 0.5470240464466801
Iteration time: 8.0
Weighted AreaUnderPRC: 0.6487017254150528
Mean absolute error: 0.09057482452485541
Coverage of cases: 89.6969696969697
Instances selection time: 6.0
Test time: 33.0
Accumulative iteration time: 460.0
Weighted Recall: 0.5636363636363636
Weighted FalsePositiveRate: 0.04363636363636364
Kappa statistic: 0.5199999999999999
Training time: 2.0
		
Iteration: 19
Labeled set size: 459
Unlabelled set size: 36
	
Mean region size: 24.81175390266299
Incorrectly Classified Instances: 40.2020202020202
Correctly Classified Instances: 59.7979797979798
Weighted Precision: 0.5973863449401935
Weighted AreaUnderROC: 0.9452615039281707
Root mean squared error: 0.2288987627132261
Relative absolute error: 53.12380605723754
Root relative squared error: 79.62255881450369
Weighted TruePositiveRate: 0.597979797979798
Weighted MatthewsCorrelation: 0.5494179811149332
Weighted FMeasure: 0.5763100733334134
Iteration time: 5.0
Weighted AreaUnderPRC: 0.6696483324674259
Mean absolute error: 0.08780794389626095
Coverage of cases: 90.1010101010101
Instances selection time: 4.0
Test time: 34.0
Accumulative iteration time: 465.0
Weighted Recall: 0.597979797979798
Weighted FalsePositiveRate: 0.040202020202020204
Kappa statistic: 0.5577777777777777
Training time: 1.0
		
Iteration: 20
Labeled set size: 479
Unlabelled set size: 16
	
Mean region size: 25.234159779614313
Incorrectly Classified Instances: 40.4040404040404
Correctly Classified Instances: 59.5959595959596
Weighted Precision: 0.6029985830188613
Weighted AreaUnderROC: 0.9404758698092033
Root mean squared error: 0.2280240660070371
Relative absolute error: 53.05205891695786
Root relative squared error: 79.3182950906292
Weighted TruePositiveRate: 0.5959595959595959
Weighted MatthewsCorrelation: 0.5555404257611704
Weighted FMeasure: 0.5898058293301396
Iteration time: 3.0
Weighted AreaUnderPRC: 0.6637374401067061
Mean absolute error: 0.08768935358174909
Coverage of cases: 90.5050505050505
Instances selection time: 2.0
Test time: 33.0
Accumulative iteration time: 468.0
Weighted Recall: 0.5959595959595959
Weighted FalsePositiveRate: 0.04040404040404041
Kappa statistic: 0.5555555555555555
Training time: 1.0
		
Time end:Wed Nov 01 14.51.29 EET 2017