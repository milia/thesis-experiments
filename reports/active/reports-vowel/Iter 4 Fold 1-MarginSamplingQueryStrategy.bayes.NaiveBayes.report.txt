Wed Nov 01 14.56.58 EET 2017
Dataset: vowel
Test set size: 495
Initial Labelled set size: 99
Initial Unlabelled set size: 396
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Wed Nov 01 14.56.58 EET 2017
		
Iteration: 1
Labeled set size: 99
Unlabelled set size: 396
	
Mean region size: 21.21212121212122
Incorrectly Classified Instances: 54.54545454545455
Correctly Classified Instances: 45.45454545454545
Weighted Precision: 0.48532409617442945
Weighted AreaUnderROC: 0.8810280583613915
Root mean squared error: 0.2717315346950137
Relative absolute error: 62.9515511678047
Root relative squared error: 94.52196179022388
Weighted TruePositiveRate: 0.45454545454545453
Weighted MatthewsCorrelation: 0.40910946528028763
Weighted FMeasure: 0.45298970248922915
Iteration time: 23.0
Weighted AreaUnderPRC: 0.477607099533025
Mean absolute error: 0.10405215069058694
Coverage of cases: 74.34343434343434
Instances selection time: 22.0
Test time: 33.0
Accumulative iteration time: 23.0
Weighted Recall: 0.45454545454545453
Weighted FalsePositiveRate: 0.05454545454545455
Kappa statistic: 0.4
Training time: 1.0
		
Iteration: 2
Labeled set size: 119
Unlabelled set size: 376
	
Mean region size: 21.98347107438018
Incorrectly Classified Instances: 47.27272727272727
Correctly Classified Instances: 52.72727272727273
Weighted Precision: 0.551692515300815
Weighted AreaUnderROC: 0.903470258136925
Root mean squared error: 0.2546059052297033
Relative absolute error: 58.38900136430566
Root relative squared error: 88.56480228803191
Weighted TruePositiveRate: 0.5272727272727272
Weighted MatthewsCorrelation: 0.4863103216890024
Weighted FMeasure: 0.5238025967946979
Iteration time: 21.0
Weighted AreaUnderPRC: 0.5280578223169615
Mean absolute error: 0.0965107460567042
Coverage of cases: 80.4040404040404
Instances selection time: 21.0
Test time: 33.0
Accumulative iteration time: 44.0
Weighted Recall: 0.5272727272727272
Weighted FalsePositiveRate: 0.04727272727272727
Kappa statistic: 0.4799999999999999
Training time: 0.0
		
Iteration: 3
Labeled set size: 139
Unlabelled set size: 356
	
Mean region size: 21.910009182736456
Incorrectly Classified Instances: 46.26262626262626
Correctly Classified Instances: 53.73737373737374
Weighted Precision: 0.5642766447997759
Weighted AreaUnderROC: 0.9056745230078564
Root mean squared error: 0.25319743568697856
Relative absolute error: 57.952474496555816
Root relative squared error: 88.07486539333347
Weighted TruePositiveRate: 0.5373737373737374
Weighted MatthewsCorrelation: 0.4981761041635247
Weighted FMeasure: 0.5340969226847998
Iteration time: 20.0
Weighted AreaUnderPRC: 0.5279142033908886
Mean absolute error: 0.09578921404389452
Coverage of cases: 80.60606060606061
Instances selection time: 20.0
Test time: 33.0
Accumulative iteration time: 64.0
Weighted Recall: 0.5373737373737374
Weighted FalsePositiveRate: 0.04626262626262626
Kappa statistic: 0.4911111111111111
Training time: 0.0
		
Iteration: 4
Labeled set size: 159
Unlabelled set size: 336
	
Mean region size: 22.27731864095503
Incorrectly Classified Instances: 48.282828282828284
Correctly Classified Instances: 51.717171717171716
Weighted Precision: 0.5490209590845906
Weighted AreaUnderROC: 0.9035914702581368
Root mean squared error: 0.25527838251349755
Relative absolute error: 57.995258554812224
Root relative squared error: 88.79872387609839
Weighted TruePositiveRate: 0.5171717171717172
Weighted MatthewsCorrelation: 0.4784002503016685
Weighted FMeasure: 0.5160437870116602
Iteration time: 18.0
Weighted AreaUnderPRC: 0.5280432687338278
Mean absolute error: 0.095859931495558
Coverage of cases: 80.0
Instances selection time: 17.0
Test time: 35.0
Accumulative iteration time: 82.0
Weighted Recall: 0.5171717171717172
Weighted FalsePositiveRate: 0.04828282828282829
Kappa statistic: 0.46888888888888886
Training time: 1.0
		
Iteration: 5
Labeled set size: 179
Unlabelled set size: 316
	
Mean region size: 23.65472910927457
Incorrectly Classified Instances: 47.27272727272727
Correctly Classified Instances: 52.72727272727273
Weighted Precision: 0.5441590634179755
Weighted AreaUnderROC: 0.9167901234567901
Root mean squared error: 0.2499133306533013
Relative absolute error: 57.911493254691294
Root relative squared error: 86.9324876753526
Weighted TruePositiveRate: 0.5272727272727272
Weighted MatthewsCorrelation: 0.4834096166063099
Weighted FMeasure: 0.5217508661676894
Iteration time: 17.0
Weighted AreaUnderPRC: 0.53363294318736
Mean absolute error: 0.09572147645403581
Coverage of cases: 84.64646464646465
Instances selection time: 16.0
Test time: 35.0
Accumulative iteration time: 99.0
Weighted Recall: 0.5272727272727272
Weighted FalsePositiveRate: 0.04727272727272727
Kappa statistic: 0.4799999999999999
Training time: 1.0
		
Iteration: 6
Labeled set size: 199
Unlabelled set size: 296
	
Mean region size: 24.11386593204773
Incorrectly Classified Instances: 44.84848484848485
Correctly Classified Instances: 55.15151515151515
Weighted Precision: 0.5744355583183279
Weighted AreaUnderROC: 0.9190392817059484
Root mean squared error: 0.24504306812483165
Relative absolute error: 56.65278478066143
Root relative squared error: 85.2383642121298
Weighted TruePositiveRate: 0.5515151515151515
Weighted MatthewsCorrelation: 0.5125905853078816
Weighted FMeasure: 0.5479404793135086
Iteration time: 16.0
Weighted AreaUnderPRC: 0.5437495723652304
Mean absolute error: 0.09364096657960627
Coverage of cases: 84.64646464646465
Instances selection time: 16.0
Test time: 33.0
Accumulative iteration time: 115.0
Weighted Recall: 0.5515151515151515
Weighted FalsePositiveRate: 0.044848484848484846
Kappa statistic: 0.5066666666666666
Training time: 0.0
		
Iteration: 7
Labeled set size: 219
Unlabelled set size: 276
	
Mean region size: 25.32598714416894
Incorrectly Classified Instances: 43.03030303030303
Correctly Classified Instances: 56.96969696969697
Weighted Precision: 0.6093138903015042
Weighted AreaUnderROC: 0.9240044893378225
Root mean squared error: 0.24070153233867872
Relative absolute error: 56.21256325369335
Root relative squared error: 83.72815863311877
Weighted TruePositiveRate: 0.5696969696969697
Weighted MatthewsCorrelation: 0.5391216947650356
Weighted FMeasure: 0.5700522842013195
Iteration time: 15.0
Weighted AreaUnderPRC: 0.5706533772685188
Mean absolute error: 0.09291332769205571
Coverage of cases: 87.27272727272727
Instances selection time: 14.0
Test time: 35.0
Accumulative iteration time: 130.0
Weighted Recall: 0.5696969696969697
Weighted FalsePositiveRate: 0.04303030303030303
Kappa statistic: 0.5266666666666666
Training time: 1.0
		
Iteration: 8
Labeled set size: 239
Unlabelled set size: 256
	
Mean region size: 25.803489439853024
Incorrectly Classified Instances: 40.2020202020202
Correctly Classified Instances: 59.7979797979798
Weighted Precision: 0.6270455279749879
Weighted AreaUnderROC: 0.9277755331088665
Root mean squared error: 0.23479531736472953
Relative absolute error: 55.20860262970196
Root relative squared error: 81.67367854960892
Weighted TruePositiveRate: 0.597979797979798
Weighted MatthewsCorrelation: 0.5671829193652754
Weighted FMeasure: 0.5990881210487414
Iteration time: 14.0
Weighted AreaUnderPRC: 0.5860376650893709
Mean absolute error: 0.09125388864413604
Coverage of cases: 87.87878787878788
Instances selection time: 13.0
Test time: 34.0
Accumulative iteration time: 144.0
Weighted Recall: 0.597979797979798
Weighted FalsePositiveRate: 0.040202020202020204
Kappa statistic: 0.5577777777777777
Training time: 1.0
		
Iteration: 9
Labeled set size: 259
Unlabelled set size: 236
	
Mean region size: 26.244260789715295
Incorrectly Classified Instances: 41.01010101010101
Correctly Classified Instances: 58.98989898989899
Weighted Precision: 0.6126509312958864
Weighted AreaUnderROC: 0.93272278338945
Root mean squared error: 0.23035694987855534
Relative absolute error: 54.58904697090332
Root relative squared error: 80.12979001120283
Weighted TruePositiveRate: 0.5898989898989899
Weighted MatthewsCorrelation: 0.5558979864412085
Weighted FMeasure: 0.5900494353094901
Iteration time: 13.0
Weighted AreaUnderPRC: 0.6029337520769866
Mean absolute error: 0.090229829703973
Coverage of cases: 89.6969696969697
Instances selection time: 12.0
Test time: 35.0
Accumulative iteration time: 157.0
Weighted Recall: 0.5898989898989899
Weighted FalsePositiveRate: 0.04101010101010102
Kappa statistic: 0.5488888888888889
Training time: 1.0
		
Iteration: 10
Labeled set size: 279
Unlabelled set size: 216
	
Mean region size: 26.20752984389344
Incorrectly Classified Instances: 37.77777777777778
Correctly Classified Instances: 62.22222222222222
Weighted Precision: 0.644614540414723
Weighted AreaUnderROC: 0.9377418630751964
Root mean squared error: 0.2246321375664454
Relative absolute error: 53.55520953704937
Root relative squared error: 78.13841094204619
Weighted TruePositiveRate: 0.6222222222222222
Weighted MatthewsCorrelation: 0.590056682038767
Weighted FMeasure: 0.6188144643352175
Iteration time: 13.0
Weighted AreaUnderPRC: 0.6196140725865641
Mean absolute error: 0.08852100749925573
Coverage of cases: 90.70707070707071
Instances selection time: 12.0
Test time: 34.0
Accumulative iteration time: 170.0
Weighted Recall: 0.6222222222222222
Weighted FalsePositiveRate: 0.03777777777777778
Kappa statistic: 0.5844444444444444
Training time: 1.0
		
Iteration: 11
Labeled set size: 299
Unlabelled set size: 196
	
Mean region size: 26.299357208448075
Incorrectly Classified Instances: 38.78787878787879
Correctly Classified Instances: 61.21212121212121
Weighted Precision: 0.6275793168678925
Weighted AreaUnderROC: 0.9340920314253648
Root mean squared error: 0.22575850211973386
Relative absolute error: 54.2222297334741
Root relative squared error: 78.53021746309385
Weighted TruePositiveRate: 0.6121212121212121
Weighted MatthewsCorrelation: 0.5771148073579501
Weighted FMeasure: 0.6096800100076923
Iteration time: 11.0
Weighted AreaUnderPRC: 0.6121695792733897
Mean absolute error: 0.08962352022061892
Coverage of cases: 90.3030303030303
Instances selection time: 10.0
Test time: 34.0
Accumulative iteration time: 181.0
Weighted Recall: 0.6121212121212121
Weighted FalsePositiveRate: 0.038787878787878795
Kappa statistic: 0.5733333333333333
Training time: 1.0
		
Iteration: 12
Labeled set size: 319
Unlabelled set size: 176
	
Mean region size: 26.299357208448097
Incorrectly Classified Instances: 37.77777777777778
Correctly Classified Instances: 62.22222222222222
Weighted Precision: 0.6350835392600818
Weighted AreaUnderROC: 0.9374186307519641
Root mean squared error: 0.22351354476974686
Relative absolute error: 53.74346334442082
Root relative squared error: 77.74930783074544
Weighted TruePositiveRate: 0.6222222222222222
Weighted MatthewsCorrelation: 0.5869168672877118
Weighted FMeasure: 0.6182968706751732
Iteration time: 10.0
Weighted AreaUnderPRC: 0.6279190015402032
Mean absolute error: 0.08883217081722505
Coverage of cases: 89.8989898989899
Instances selection time: 9.0
Test time: 34.0
Accumulative iteration time: 191.0
Weighted Recall: 0.6222222222222222
Weighted FalsePositiveRate: 0.037777777777777785
Kappa statistic: 0.5844444444444444
Training time: 1.0
		
Iteration: 13
Labeled set size: 339
Unlabelled set size: 156
	
Mean region size: 26.28099173553715
Incorrectly Classified Instances: 37.17171717171717
Correctly Classified Instances: 62.82828282828283
Weighted Precision: 0.6410596597783752
Weighted AreaUnderROC: 0.941364758698092
Root mean squared error: 0.218912573020709
Relative absolute error: 52.62505511880881
Root relative squared error: 76.14885731127013
Weighted TruePositiveRate: 0.6282828282828283
Weighted MatthewsCorrelation: 0.5943288860102877
Weighted FMeasure: 0.6263873535689162
Iteration time: 9.0
Weighted AreaUnderPRC: 0.6427938445233828
Mean absolute error: 0.08698356217984983
Coverage of cases: 91.71717171717172
Instances selection time: 8.0
Test time: 34.0
Accumulative iteration time: 200.0
Weighted Recall: 0.6282828282828283
Weighted FalsePositiveRate: 0.037171717171717175
Kappa statistic: 0.5911111111111111
Training time: 1.0
		
Iteration: 14
Labeled set size: 359
Unlabelled set size: 136
	
Mean region size: 25.932047750229533
Incorrectly Classified Instances: 37.77777777777778
Correctly Classified Instances: 62.22222222222222
Weighted Precision: 0.6338770981257045
Weighted AreaUnderROC: 0.9396228956228958
Root mean squared error: 0.21882629641859982
Relative absolute error: 52.61603167363509
Root relative squared error: 76.11884594841118
Weighted TruePositiveRate: 0.6222222222222222
Weighted MatthewsCorrelation: 0.5876896880763249
Weighted FMeasure: 0.6211966681551161
Iteration time: 8.0
Weighted AreaUnderPRC: 0.6505042343165506
Mean absolute error: 0.08696864739443873
Coverage of cases: 91.11111111111111
Instances selection time: 7.0
Test time: 33.0
Accumulative iteration time: 208.0
Weighted Recall: 0.6222222222222222
Weighted FalsePositiveRate: 0.03777777777777778
Kappa statistic: 0.5844444444444444
Training time: 1.0
		
Iteration: 15
Labeled set size: 379
Unlabelled set size: 116
	
Mean region size: 26.501377410468287
Incorrectly Classified Instances: 39.39393939393939
Correctly Classified Instances: 60.60606060606061
Weighted Precision: 0.6132356556491553
Weighted AreaUnderROC: 0.9415712682379349
Root mean squared error: 0.2192606321438008
Relative absolute error: 52.922503906697685
Root relative squared error: 76.26992986610094
Weighted TruePositiveRate: 0.6060606060606061
Weighted MatthewsCorrelation: 0.5688744255964798
Weighted FMeasure: 0.6058982289977253
Iteration time: 8.0
Weighted AreaUnderPRC: 0.6534523771435922
Mean absolute error: 0.08747521306892235
Coverage of cases: 92.12121212121212
Instances selection time: 7.0
Test time: 32.0
Accumulative iteration time: 216.0
Weighted Recall: 0.6060606060606061
Weighted FalsePositiveRate: 0.03939393939393939
Kappa statistic: 0.5666666666666667
Training time: 1.0
		
Iteration: 16
Labeled set size: 399
Unlabelled set size: 96
	
Mean region size: 26.06060606060602
Incorrectly Classified Instances: 36.16161616161616
Correctly Classified Instances: 63.83838383838384
Weighted Precision: 0.6515529135289813
Weighted AreaUnderROC: 0.9425948372615038
Root mean squared error: 0.2162460099981887
Relative absolute error: 52.11622622099678
Root relative squared error: 75.22129191695997
Weighted TruePositiveRate: 0.6383838383838384
Weighted MatthewsCorrelation: 0.6062525394567687
Weighted FMeasure: 0.6380790192853171
Iteration time: 6.0
Weighted AreaUnderPRC: 0.6687036056665715
Mean absolute error: 0.08614252267933407
Coverage of cases: 90.70707070707071
Instances selection time: 5.0
Test time: 33.0
Accumulative iteration time: 222.0
Weighted Recall: 0.6383838383838384
Weighted FalsePositiveRate: 0.03616161616161616
Kappa statistic: 0.6022222222222222
Training time: 1.0
		
Iteration: 17
Labeled set size: 419
Unlabelled set size: 76
	
Mean region size: 26.409550045913647
Incorrectly Classified Instances: 37.97979797979798
Correctly Classified Instances: 62.02020202020202
Weighted Precision: 0.6302156190103827
Weighted AreaUnderROC: 0.942594837261504
Root mean squared error: 0.21549200634622914
Relative absolute error: 52.18880564198088
Root relative squared error: 74.95901133748949
Weighted TruePositiveRate: 0.6202020202020202
Weighted MatthewsCorrelation: 0.5854112377592224
Weighted FMeasure: 0.6202092792593915
Iteration time: 5.0
Weighted AreaUnderPRC: 0.6720081792610401
Mean absolute error: 0.08626248866443176
Coverage of cases: 90.9090909090909
Instances selection time: 4.0
Test time: 33.0
Accumulative iteration time: 227.0
Weighted Recall: 0.6202020202020202
Weighted FalsePositiveRate: 0.03797979797979798
Kappa statistic: 0.5822222222222222
Training time: 1.0
		
Iteration: 18
Labeled set size: 439
Unlabelled set size: 56
	
Mean region size: 26.20752984389344
Incorrectly Classified Instances: 38.18181818181818
Correctly Classified Instances: 61.81818181818182
Weighted Precision: 0.6241420230093108
Weighted AreaUnderROC: 0.9398428731762065
Root mean squared error: 0.21820970710000653
Relative absolute error: 52.25578179551322
Root relative squared error: 75.90436501936573
Weighted TruePositiveRate: 0.6181818181818182
Weighted MatthewsCorrelation: 0.581480107153706
Weighted FMeasure: 0.6170600597227541
Iteration time: 5.0
Weighted AreaUnderPRC: 0.6601666848878142
Mean absolute error: 0.08637319305043564
Coverage of cases: 91.71717171717172
Instances selection time: 3.0
Test time: 33.0
Accumulative iteration time: 232.0
Weighted Recall: 0.6181818181818182
Weighted FalsePositiveRate: 0.038181818181818185
Kappa statistic: 0.58
Training time: 2.0
		
Iteration: 19
Labeled set size: 459
Unlabelled set size: 36
	
Mean region size: 25.65656565656561
Incorrectly Classified Instances: 40.60606060606061
Correctly Classified Instances: 59.39393939393939
Weighted Precision: 0.5987565692718139
Weighted AreaUnderROC: 0.9360044893378228
Root mean squared error: 0.2237130056792056
Relative absolute error: 53.167219754177324
Root relative squared error: 77.81869041633185
Weighted TruePositiveRate: 0.593939393939394
Weighted MatthewsCorrelation: 0.5540493320857601
Weighted FMeasure: 0.5916696297989201
Iteration time: 4.0
Weighted AreaUnderPRC: 0.641162903037322
Mean absolute error: 0.08787970207302094
Coverage of cases: 90.3030303030303
Instances selection time: 3.0
Test time: 33.0
Accumulative iteration time: 236.0
Weighted Recall: 0.593939393939394
Weighted FalsePositiveRate: 0.04060606060606061
Kappa statistic: 0.5533333333333333
Training time: 1.0
		
Iteration: 20
Labeled set size: 479
Unlabelled set size: 16
	
Mean region size: 25.674931129476537
Incorrectly Classified Instances: 43.83838383838384
Correctly Classified Instances: 56.16161616161616
Weighted Precision: 0.5656979237074915
Weighted AreaUnderROC: 0.9351245791245791
Root mean squared error: 0.2266416237272719
Relative absolute error: 53.529744136582416
Root relative squared error: 78.8374117934741
Weighted TruePositiveRate: 0.5616161616161616
Weighted MatthewsCorrelation: 0.5188714591122611
Weighted FMeasure: 0.5610274766865472
Iteration time: 2.0
Weighted AreaUnderPRC: 0.6370959933837149
Mean absolute error: 0.08847891592823597
Coverage of cases: 90.1010101010101
Instances selection time: 1.0
Test time: 33.0
Accumulative iteration time: 238.0
Weighted Recall: 0.5616161616161616
Weighted FalsePositiveRate: 0.04383838383838384
Kappa statistic: 0.5177777777777777
Training time: 1.0
		
Time end:Wed Nov 01 14.57.00 EET 2017