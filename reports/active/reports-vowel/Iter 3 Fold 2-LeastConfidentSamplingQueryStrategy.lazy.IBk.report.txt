Wed Nov 01 14.56.09 EET 2017
Dataset: vowel
Test set size: 495
Initial Labelled set size: 99
Initial Unlabelled set size: 396
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Wed Nov 01 14.56.09 EET 2017
		
Iteration: 1
Labeled set size: 99
Unlabelled set size: 396
	
Mean region size: 54.545454545454064
Incorrectly Classified Instances: 59.39393939393939
Correctly Classified Instances: 40.60606060606061
Weighted Precision: 0.42621648423037506
Weighted AreaUnderROC: 0.6733333333333333
Root mean squared error: 0.31307591559495496
Relative absolute error: 68.80000000000037
Root relative squared error: 108.90362712049524
Weighted TruePositiveRate: 0.40606060606060607
Weighted MatthewsCorrelation: 0.35462436403519804
Weighted FMeasure: 0.4100771460499974
Iteration time: 15.0
Weighted AreaUnderPRC: 0.23195138821540656
Mean absolute error: 0.11371900826446416
Coverage of cases: 71.51515151515152
Instances selection time: 15.0
Test time: 16.0
Accumulative iteration time: 15.0
Weighted Recall: 0.40606060606060607
Weighted FalsePositiveRate: 0.05939393939393939
Kappa statistic: 0.34666666666666673
Training time: 0.0
		
Iteration: 2
Labeled set size: 119
Unlabelled set size: 376
	
Mean region size: 45.45454545454594
Incorrectly Classified Instances: 55.15151515151515
Correctly Classified Instances: 44.84848484848485
Weighted Precision: 0.4735794310635139
Weighted AreaUnderROC: 0.6966666666666664
Root mean squared error: 0.3039446488838826
Relative absolute error: 63.99487179487143
Root relative squared error: 105.72731104025429
Weighted TruePositiveRate: 0.4484848484848485
Weighted MatthewsCorrelation: 0.4014335713445723
Weighted FMeasure: 0.44765113783932464
Iteration time: 6.0
Weighted AreaUnderPRC: 0.263846227907791
Mean absolute error: 0.10577664759482948
Coverage of cases: 70.1010101010101
Instances selection time: 6.0
Test time: 11.0
Accumulative iteration time: 21.0
Weighted Recall: 0.4484848484848485
Weighted FalsePositiveRate: 0.055151515151515146
Kappa statistic: 0.3933333333333333
Training time: 0.0
		
Iteration: 3
Labeled set size: 139
Unlabelled set size: 356
	
Mean region size: 36.36363636363658
Incorrectly Classified Instances: 53.333333333333336
Correctly Classified Instances: 46.666666666666664
Weighted Precision: 0.5142149956431344
Weighted AreaUnderROC: 0.7066666666666667
Root mean squared error: 0.3005046260925615
Relative absolute error: 61.69777777777737
Root relative squared error: 104.53069724567317
Weighted TruePositiveRate: 0.4666666666666667
Weighted MatthewsCorrelation: 0.4297524671950393
Weighted FMeasure: 0.4689079389388534
Iteration time: 7.0
Weighted AreaUnderPRC: 0.28384490206517904
Mean absolute error: 0.10197979797979796
Coverage of cases: 64.24242424242425
Instances selection time: 7.0
Test time: 11.0
Accumulative iteration time: 28.0
Weighted Recall: 0.4666666666666667
Weighted FalsePositiveRate: 0.05333333333333334
Kappa statistic: 0.41333333333333333
Training time: 0.0
		
Iteration: 4
Labeled set size: 159
Unlabelled set size: 336
	
Mean region size: 27.272727272727032
Incorrectly Classified Instances: 48.888888888888886
Correctly Classified Instances: 51.111111111111114
Weighted Precision: 0.5873637643982721
Weighted AreaUnderROC: 0.7311111111111112
Root mean squared error: 0.28893472560979366
Relative absolute error: 56.768627450979736
Root relative squared error: 100.50610108470008
Weighted TruePositiveRate: 0.5111111111111111
Weighted MatthewsCorrelation: 0.4853876501012475
Weighted FMeasure: 0.5084237691949497
Iteration time: 7.0
Weighted AreaUnderPRC: 0.329391678347382
Mean absolute error: 0.0938324420677357
Coverage of cases: 62.02020202020202
Instances selection time: 7.0
Test time: 13.0
Accumulative iteration time: 35.0
Weighted Recall: 0.5111111111111111
Weighted FalsePositiveRate: 0.048888888888888885
Kappa statistic: 0.46222222222222215
Training time: 0.0
		
Iteration: 5
Labeled set size: 179
Unlabelled set size: 316
	
Mean region size: 18.18181818181829
Incorrectly Classified Instances: 47.878787878787875
Correctly Classified Instances: 52.121212121212125
Weighted Precision: 0.6161675194535267
Weighted AreaUnderROC: 0.7366666666666666
Root mean squared error: 0.28686142560433664
Relative absolute error: 55.407017543859425
Root relative squared error: 99.78490255279087
Weighted TruePositiveRate: 0.5212121212121212
Weighted MatthewsCorrelation: 0.5025610449712051
Weighted FMeasure: 0.5199504677665736
Iteration time: 7.0
Weighted AreaUnderPRC: 0.34591924088535986
Mean absolute error: 0.09158184717993352
Coverage of cases: 57.17171717171717
Instances selection time: 7.0
Test time: 14.0
Accumulative iteration time: 42.0
Weighted Recall: 0.5212121212121212
Weighted FalsePositiveRate: 0.04787878787878788
Kappa statistic: 0.47333333333333333
Training time: 0.0
		
Iteration: 6
Labeled set size: 199
Unlabelled set size: 296
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 44.64646464646464
Correctly Classified Instances: 55.35353535353536
Weighted Precision: 0.6369814812324425
Weighted AreaUnderROC: 0.7544444444444446
Root mean squared error: 0.27775907435018143
Relative absolute error: 51.77671957671947
Root relative squared error: 96.61864472992862
Weighted TruePositiveRate: 0.5535353535353535
Weighted MatthewsCorrelation: 0.5304750162007775
Weighted FMeasure: 0.5426267883059696
Iteration time: 8.0
Weighted AreaUnderPRC: 0.3730792711585127
Mean absolute error: 0.08558135467226413
Coverage of cases: 55.35353535353536
Instances selection time: 8.0
Test time: 14.0
Accumulative iteration time: 50.0
Weighted Recall: 0.5535353535353535
Weighted FalsePositiveRate: 0.044646464646464656
Kappa statistic: 0.5088888888888888
Training time: 0.0
		
Iteration: 7
Labeled set size: 219
Unlabelled set size: 276
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 41.21212121212121
Correctly Classified Instances: 58.78787878787879
Weighted Precision: 0.6478330930376044
Weighted AreaUnderROC: 0.7733333333333333
Root mean squared error: 0.2674630865732895
Relative absolute error: 47.947826086956226
Root relative squared error: 93.0371797949431
Weighted TruePositiveRate: 0.5878787878787879
Weighted MatthewsCorrelation: 0.5604111264737812
Weighted FMeasure: 0.5754299318731454
Iteration time: 8.0
Weighted AreaUnderPRC: 0.40643764500035906
Mean absolute error: 0.07925260510240749
Coverage of cases: 58.78787878787879
Instances selection time: 8.0
Test time: 15.0
Accumulative iteration time: 58.0
Weighted Recall: 0.5878787878787879
Weighted FalsePositiveRate: 0.041212121212121214
Kappa statistic: 0.5466666666666666
Training time: 0.0
		
Iteration: 8
Labeled set size: 239
Unlabelled set size: 256
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 38.98989898989899
Correctly Classified Instances: 61.01010101010101
Weighted Precision: 0.6600463977771813
Weighted AreaUnderROC: 0.7855555555555556
Root mean squared error: 0.26063678465824464
Relative absolute error: 45.40177777777757
Root relative squared error: 90.6626469697169
Weighted TruePositiveRate: 0.6101010101010101
Weighted MatthewsCorrelation: 0.5796484294765867
Weighted FMeasure: 0.5924803160487522
Iteration time: 8.0
Weighted AreaUnderPRC: 0.4293433244217585
Mean absolute error: 0.07504426078971548
Coverage of cases: 61.01010101010101
Instances selection time: 8.0
Test time: 16.0
Accumulative iteration time: 66.0
Weighted Recall: 0.6101010101010101
Weighted FalsePositiveRate: 0.038989898989899
Kappa statistic: 0.5711111111111111
Training time: 0.0
		
Iteration: 9
Labeled set size: 259
Unlabelled set size: 236
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 36.96969696969697
Correctly Classified Instances: 63.03030303030303
Weighted Precision: 0.6706150393243
Weighted AreaUnderROC: 0.7966666666666669
Root mean squared error: 0.2541973819562535
Relative absolute error: 43.08395061728364
Root relative squared error: 88.42269724569013
Weighted TruePositiveRate: 0.6303030303030303
Weighted MatthewsCorrelation: 0.598581815167228
Weighted FMeasure: 0.6122498421067226
Iteration time: 7.0
Weighted AreaUnderPRC: 0.4489225954808073
Mean absolute error: 0.07121314151617177
Coverage of cases: 63.03030303030303
Instances selection time: 7.0
Test time: 17.0
Accumulative iteration time: 73.0
Weighted Recall: 0.6303030303030303
Weighted FalsePositiveRate: 0.03696969696969698
Kappa statistic: 0.5933333333333333
Training time: 0.0
		
Iteration: 10
Labeled set size: 279
Unlabelled set size: 216
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 34.74747474747475
Correctly Classified Instances: 65.25252525252525
Weighted Precision: 0.6960049144031103
Weighted AreaUnderROC: 0.808888888888889
Root mean squared error: 0.24677875781559533
Relative absolute error: 40.56551724137898
Root relative squared error: 85.84212481287932
Weighted TruePositiveRate: 0.6525252525252525
Weighted MatthewsCorrelation: 0.6233899466464311
Weighted FMeasure: 0.6328771834062454
Iteration time: 7.0
Weighted AreaUnderPRC: 0.4802066213910688
Mean absolute error: 0.06705044172128799
Coverage of cases: 65.25252525252525
Instances selection time: 7.0
Test time: 19.0
Accumulative iteration time: 80.0
Weighted Recall: 0.6525252525252525
Weighted FalsePositiveRate: 0.034747474747474756
Kappa statistic: 0.6177777777777778
Training time: 0.0
		
Iteration: 11
Labeled set size: 299
Unlabelled set size: 196
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 31.717171717171716
Correctly Classified Instances: 68.28282828282828
Weighted Precision: 0.7354323420685243
Weighted AreaUnderROC: 0.8255555555555556
Root mean squared error: 0.23606222499566018
Relative absolute error: 37.19928315412164
Root relative squared error: 82.1143730564757
Weighted TruePositiveRate: 0.6828282828282828
Weighted MatthewsCorrelation: 0.6590859157344386
Weighted FMeasure: 0.6631864625442663
Iteration time: 7.0
Weighted AreaUnderPRC: 0.5159308292196955
Mean absolute error: 0.06148641843656509
Coverage of cases: 68.28282828282828
Instances selection time: 7.0
Test time: 19.0
Accumulative iteration time: 87.0
Weighted Recall: 0.6828282828282828
Weighted FalsePositiveRate: 0.03171717171717172
Kappa statistic: 0.6511111111111111
Training time: 0.0
		
Iteration: 12
Labeled set size: 319
Unlabelled set size: 176
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 29.8989898989899
Correctly Classified Instances: 70.1010101010101
Weighted Precision: 0.7623400059403694
Weighted AreaUnderROC: 0.8355555555555556
Root mean squared error: 0.22943750170149058
Relative absolute error: 35.12592592592563
Root relative squared error: 79.80995946390125
Weighted TruePositiveRate: 0.701010101010101
Weighted MatthewsCorrelation: 0.6811069487501955
Weighted FMeasure: 0.6797732826677629
Iteration time: 7.0
Weighted AreaUnderPRC: 0.5415166800599825
Mean absolute error: 0.058059381695745205
Coverage of cases: 70.1010101010101
Instances selection time: 7.0
Test time: 19.0
Accumulative iteration time: 94.0
Weighted Recall: 0.701010101010101
Weighted FalsePositiveRate: 0.029898989898989908
Kappa statistic: 0.6711111111111111
Training time: 0.0
		
Iteration: 13
Labeled set size: 339
Unlabelled set size: 156
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 25.050505050505052
Correctly Classified Instances: 74.94949494949495
Weighted Precision: 0.7900271207983324
Weighted AreaUnderROC: 0.8622222222222223
Root mean squared error: 0.2102297506080044
Relative absolute error: 29.83238095238065
Root relative squared error: 73.12853282355059
Weighted TruePositiveRate: 0.7494949494949495
Weighted MatthewsCorrelation: 0.7279611036055587
Weighted FMeasure: 0.7280189478015097
Iteration time: 6.0
Weighted AreaUnderPRC: 0.6074377434719732
Mean absolute error: 0.04930972058244767
Coverage of cases: 74.94949494949495
Instances selection time: 6.0
Test time: 20.0
Accumulative iteration time: 100.0
Weighted Recall: 0.7494949494949495
Weighted FalsePositiveRate: 0.025050505050505052
Kappa statistic: 0.7244444444444444
Training time: 0.0
		
Iteration: 14
Labeled set size: 359
Unlabelled set size: 136
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 24.04040404040404
Correctly Classified Instances: 75.95959595959596
Weighted Precision: 0.7972288467708162
Weighted AreaUnderROC: 0.8677777777777779
Root mean squared error: 0.20611499277713424
Relative absolute error: 28.631231231231073
Root relative squared error: 71.6972120793386
Weighted TruePositiveRate: 0.7595959595959596
Weighted MatthewsCorrelation: 0.7392938132368307
Weighted FMeasure: 0.7407466872583166
Iteration time: 6.0
Weighted AreaUnderPRC: 0.6191678933508714
Mean absolute error: 0.047324349142531
Coverage of cases: 75.95959595959596
Instances selection time: 6.0
Test time: 21.0
Accumulative iteration time: 106.0
Weighted Recall: 0.7595959595959596
Weighted FalsePositiveRate: 0.024040404040404043
Kappa statistic: 0.7355555555555555
Training time: 0.0
		
Iteration: 15
Labeled set size: 379
Unlabelled set size: 116
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 20.404040404040405
Correctly Classified Instances: 79.5959595959596
Weighted Precision: 0.8158270489198381
Weighted AreaUnderROC: 0.8877777777777779
Root mean squared error: 0.19004641657580595
Relative absolute error: 24.631908831908696
Root relative squared error: 66.10774912859951
Weighted TruePositiveRate: 0.795959595959596
Weighted MatthewsCorrelation: 0.773819690638483
Weighted FMeasure: 0.7774030399223398
Iteration time: 5.0
Weighted AreaUnderPRC: 0.6637311978115717
Mean absolute error: 0.04071389889571711
Coverage of cases: 79.5959595959596
Instances selection time: 5.0
Test time: 22.0
Accumulative iteration time: 111.0
Weighted Recall: 0.795959595959596
Weighted FalsePositiveRate: 0.020404040404040407
Kappa statistic: 0.7755555555555556
Training time: 0.0
		
Iteration: 16
Labeled set size: 399
Unlabelled set size: 96
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 16.767676767676768
Correctly Classified Instances: 83.23232323232324
Weighted Precision: 0.8438975307768688
Weighted AreaUnderROC: 0.9077777777777777
Root mean squared error: 0.1724187866208896
Relative absolute error: 20.632520325203167
Root relative squared error: 59.97596848370337
Weighted TruePositiveRate: 0.8323232323232324
Weighted MatthewsCorrelation: 0.8141416367057449
Weighted FMeasure: 0.820164170038198
Iteration time: 5.0
Weighted AreaUnderPRC: 0.7233447257130909
Mean absolute error: 0.03410333938050132
Coverage of cases: 83.23232323232324
Instances selection time: 5.0
Test time: 23.0
Accumulative iteration time: 116.0
Weighted Recall: 0.8323232323232324
Weighted FalsePositiveRate: 0.016767676767676768
Kappa statistic: 0.8155555555555556
Training time: 0.0
		
Iteration: 17
Labeled set size: 419
Unlabelled set size: 76
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 15.353535353535353
Correctly Classified Instances: 84.64646464646465
Weighted Precision: 0.8556117601265318
Weighted AreaUnderROC: 0.9155555555555555
Root mean squared error: 0.16509235048739646
Relative absolute error: 19.01498708010323
Root relative squared error: 57.42746369920785
Weighted TruePositiveRate: 0.8464646464646465
Weighted MatthewsCorrelation: 0.8288731701303047
Weighted FMeasure: 0.8340890289544445
Iteration time: 4.0
Weighted AreaUnderPRC: 0.7474294643316589
Mean absolute error: 0.03142973071091463
Coverage of cases: 84.64646464646465
Instances selection time: 4.0
Test time: 23.0
Accumulative iteration time: 120.0
Weighted Recall: 0.8464646464646465
Weighted FalsePositiveRate: 0.015353535353535354
Kappa statistic: 0.8311111111111111
Training time: 0.0
		
Iteration: 18
Labeled set size: 439
Unlabelled set size: 56
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 12.525252525252526
Correctly Classified Instances: 87.47474747474747
Weighted Precision: 0.8778644716464226
Weighted AreaUnderROC: 0.9311111111111111
Root mean squared error: 0.14921759693678305
Relative absolute error: 15.88543209876525
Root relative squared error: 51.90542206269168
Weighted TruePositiveRate: 0.8747474747474747
Weighted MatthewsCorrelation: 0.8597782142115113
Weighted FMeasure: 0.8662060038313246
Iteration time: 3.0
Weighted AreaUnderPRC: 0.792856955957021
Mean absolute error: 0.02625691255994273
Coverage of cases: 87.47474747474747
Instances selection time: 3.0
Test time: 24.0
Accumulative iteration time: 123.0
Weighted Recall: 0.8747474747474747
Weighted FalsePositiveRate: 0.012525252525252524
Kappa statistic: 0.8622222222222222
Training time: 0.0
		
Iteration: 19
Labeled set size: 459
Unlabelled set size: 36
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 10.505050505050505
Correctly Classified Instances: 89.4949494949495
Weighted Precision: 0.8985623103479484
Weighted AreaUnderROC: 0.9422222222222222
Root mean squared error: 0.13674186540436653
Relative absolute error: 13.625531914893475
Root relative squared error: 47.56573207957761
Weighted TruePositiveRate: 0.8949494949494949
Weighted MatthewsCorrelation: 0.8828563579055692
Weighted FMeasure: 0.8881429064632259
Iteration time: 2.0
Weighted AreaUnderPRC: 0.8265134191872495
Mean absolute error: 0.02252154035519597
Coverage of cases: 89.4949494949495
Instances selection time: 2.0
Test time: 25.0
Accumulative iteration time: 125.0
Weighted Recall: 0.8949494949494949
Weighted FalsePositiveRate: 0.010505050505050502
Kappa statistic: 0.8844444444444445
Training time: 0.0
		
Iteration: 20
Labeled set size: 479
Unlabelled set size: 16
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 6.8686868686868685
Correctly Classified Instances: 93.13131313131314
Weighted Precision: 0.9340175935204997
Weighted AreaUnderROC: 0.9622222222222222
Root mean squared error: 0.11067888834889904
Relative absolute error: 9.630839002267523
Root relative squared error: 38.499711368579185
Weighted TruePositiveRate: 0.9313131313131313
Weighted MatthewsCorrelation: 0.9252972132136719
Weighted FMeasure: 0.9313881998124949
Iteration time: 1.0
Weighted AreaUnderPRC: 0.8796495168184448
Mean absolute error: 0.015918742152508405
Coverage of cases: 93.13131313131314
Instances selection time: 1.0
Test time: 26.0
Accumulative iteration time: 126.0
Weighted Recall: 0.9313131313131313
Weighted FalsePositiveRate: 0.0068686868686868695
Kappa statistic: 0.9244444444444444
Training time: 0.0
		
Time end:Wed Nov 01 14.56.10 EET 2017