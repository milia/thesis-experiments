Wed Nov 01 15.03.33 EET 2017
Dataset: vowel
Test set size: 495
Initial Labelled set size: 99
Initial Unlabelled set size: 396
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Wed Nov 01 15.03.33 EET 2017
		
Iteration: 1
Labeled set size: 99
Unlabelled set size: 396
	
Mean region size: 82.22222222222187
Incorrectly Classified Instances: 77.77777777777777
Correctly Classified Instances: 22.22222222222222
Weighted Precision: 0.31188876896861184
Weighted AreaUnderROC: 0.7243299663299664
Root mean squared error: 0.2801345971903701
Relative absolute error: 95.60808080808033
Root relative squared error: 97.44497163889093
Weighted TruePositiveRate: 0.2222222222222222
Weighted MatthewsCorrelation: 0.16570027320291472
Weighted FMeasure: 0.21416093734391647
Iteration time: 362.0
Weighted AreaUnderPRC: 0.22554102010820365
Mean absolute error: 0.15802988563319165
Coverage of cases: 97.77777777777777
Instances selection time: 5.0
Test time: 11.0
Accumulative iteration time: 362.0
Weighted Recall: 0.2222222222222222
Weighted FalsePositiveRate: 0.07777777777777778
Kappa statistic: 0.14444444444444443
Training time: 357.0
		
Iteration: 2
Labeled set size: 119
Unlabelled set size: 376
	
Mean region size: 82.27731864095463
Incorrectly Classified Instances: 76.76767676767676
Correctly Classified Instances: 23.232323232323232
Weighted Precision: 0.37330572641577414
Weighted AreaUnderROC: 0.7519842873176206
Root mean squared error: 0.27939980100034933
Relative absolute error: 95.3575757575754
Root relative squared error: 97.1893723854782
Weighted TruePositiveRate: 0.23232323232323232
Weighted MatthewsCorrelation: 0.19297336874770066
Weighted FMeasure: 0.2132433152885789
Iteration time: 361.0
Weighted AreaUnderPRC: 0.2521683938785875
Mean absolute error: 0.15761582769847277
Coverage of cases: 97.17171717171718
Instances selection time: 5.0
Test time: 10.0
Accumulative iteration time: 723.0
Weighted Recall: 0.23232323232323232
Weighted FalsePositiveRate: 0.07676767676767676
Kappa statistic: 0.15555555555555556
Training time: 356.0
		
Iteration: 3
Labeled set size: 139
Unlabelled set size: 356
	
Mean region size: 82.0018365472907
Incorrectly Classified Instances: 74.74747474747475
Correctly Classified Instances: 25.252525252525253
Weighted Precision: 0.4075386695311321
Weighted AreaUnderROC: 0.7697283950617283
Root mean squared error: 0.2788460179580474
Relative absolute error: 95.14747474747442
Root relative squared error: 96.99673865372053
Weighted TruePositiveRate: 0.25252525252525254
Weighted MatthewsCorrelation: 0.20854470953766963
Weighted FMeasure: 0.22682040857080044
Iteration time: 372.0
Weighted AreaUnderPRC: 0.2710715891466903
Mean absolute error: 0.1572685533016116
Coverage of cases: 96.56565656565657
Instances selection time: 5.0
Test time: 11.0
Accumulative iteration time: 1095.0
Weighted Recall: 0.25252525252525254
Weighted FalsePositiveRate: 0.07474747474747474
Kappa statistic: 0.1777777777777778
Training time: 367.0
		
Iteration: 4
Labeled set size: 159
Unlabelled set size: 336
	
Mean region size: 81.98347107437976
Incorrectly Classified Instances: 73.93939393939394
Correctly Classified Instances: 26.060606060606062
Weighted Precision: 0.39769070366610937
Weighted AreaUnderROC: 0.7712772166105499
Root mean squared error: 0.2792043840465091
Relative absolute error: 95.25252525252486
Root relative squared error: 97.121396492048
Weighted TruePositiveRate: 0.2606060606060606
Weighted MatthewsCorrelation: 0.2086532841049177
Weighted FMeasure: 0.2185795205697365
Iteration time: 378.0
Weighted AreaUnderPRC: 0.27486925204274415
Mean absolute error: 0.1574421905000421
Coverage of cases: 96.56565656565657
Instances selection time: 5.0
Test time: 10.0
Accumulative iteration time: 1473.0
Weighted Recall: 0.2606060606060606
Weighted FalsePositiveRate: 0.07393939393939393
Kappa statistic: 0.18666666666666668
Training time: 373.0
		
Iteration: 5
Labeled set size: 179
Unlabelled set size: 316
	
Mean region size: 82.02020202020162
Incorrectly Classified Instances: 68.48484848484848
Correctly Classified Instances: 31.515151515151516
Weighted Precision: 0.42770860299917557
Weighted AreaUnderROC: 0.7834725028058361
Root mean squared error: 0.2784926408483462
Relative absolute error: 95.02626262626225
Root relative squared error: 96.873816234362
Weighted TruePositiveRate: 0.3151515151515151
Weighted MatthewsCorrelation: 0.2670355881484766
Weighted FMeasure: 0.28193720085912705
Iteration time: 384.0
Weighted AreaUnderPRC: 0.3110216203981431
Mean absolute error: 0.15706820268803778
Coverage of cases: 95.95959595959596
Instances selection time: 4.0
Test time: 11.0
Accumulative iteration time: 1857.0
Weighted Recall: 0.3151515151515151
Weighted FalsePositiveRate: 0.0684848484848485
Kappa statistic: 0.24666666666666665
Training time: 380.0
		
Iteration: 6
Labeled set size: 199
Unlabelled set size: 296
	
Mean region size: 82.02020202020164
Incorrectly Classified Instances: 68.88888888888889
Correctly Classified Instances: 31.11111111111111
Weighted Precision: 0.4416940528516746
Weighted AreaUnderROC: 0.7752323232323233
Root mean squared error: 0.2792659149209475
Relative absolute error: 95.28888888888847
Root relative squared error: 97.1428000401092
Weighted TruePositiveRate: 0.3111111111111111
Weighted MatthewsCorrelation: 0.2506736657588901
Weighted FMeasure: 0.26233993908699316
Iteration time: 389.0
Weighted AreaUnderPRC: 0.3064298825137601
Mean absolute error: 0.15750229568411417
Coverage of cases: 94.74747474747475
Instances selection time: 4.0
Test time: 11.0
Accumulative iteration time: 2246.0
Weighted Recall: 0.3111111111111111
Weighted FalsePositiveRate: 0.06888888888888887
Kappa statistic: 0.24222222222222223
Training time: 385.0
		
Iteration: 7
Labeled set size: 219
Unlabelled set size: 276
	
Mean region size: 81.98347107437979
Incorrectly Classified Instances: 68.08080808080808
Correctly Classified Instances: 31.91919191919192
Weighted Precision: 0.31603717524451963
Weighted AreaUnderROC: 0.7852839506172841
Root mean squared error: 0.27914001215333073
Relative absolute error: 95.25656565656514
Root relative squared error: 97.09900469407653
Weighted TruePositiveRate: 0.3191919191919192
Weighted MatthewsCorrelation: 0.23725692774427806
Weighted FMeasure: 0.2592849611487546
Iteration time: 396.0
Weighted AreaUnderPRC: 0.31308861699566665
Mean absolute error: 0.1574488688538277
Coverage of cases: 94.14141414141415
Instances selection time: 4.0
Test time: 10.0
Accumulative iteration time: 2642.0
Weighted Recall: 0.3191919191919192
Weighted FalsePositiveRate: 0.06808080808080809
Kappa statistic: 0.2511111111111111
Training time: 392.0
		
Iteration: 8
Labeled set size: 239
Unlabelled set size: 256
	
Mean region size: 82.00183654729071
Incorrectly Classified Instances: 66.46464646464646
Correctly Classified Instances: 33.535353535353536
Weighted Precision: 0.3720333443961429
Weighted AreaUnderROC: 0.7838271604938271
Root mean squared error: 0.2792739585914921
Relative absolute error: 95.2969696969692
Root relative squared error: 97.14559803527278
Weighted TruePositiveRate: 0.33535353535353535
Weighted MatthewsCorrelation: 0.2564455927672744
Weighted FMeasure: 0.26229034394296347
Iteration time: 403.0
Weighted AreaUnderPRC: 0.3172195771631036
Mean absolute error: 0.15751565239168563
Coverage of cases: 94.74747474747475
Instances selection time: 3.0
Test time: 10.0
Accumulative iteration time: 3045.0
Weighted Recall: 0.33535353535353535
Weighted FalsePositiveRate: 0.06646464646464646
Kappa statistic: 0.2688888888888889
Training time: 400.0
		
Iteration: 9
Labeled set size: 259
Unlabelled set size: 236
	
Mean region size: 82.31404958677652
Incorrectly Classified Instances: 63.83838383838384
Correctly Classified Instances: 36.16161616161616
Weighted Precision: 0.2822355483648233
Weighted AreaUnderROC: 0.7921324354657688
Root mean squared error: 0.27888411758033105
Relative absolute error: 95.17171717171657
Root relative squared error: 97.00999162800545
Weighted TruePositiveRate: 0.3616161616161616
Weighted MatthewsCorrelation: 0.2648604361905662
Weighted FMeasure: 0.28847078157792994
Iteration time: 407.0
Weighted AreaUnderPRC: 0.33494966625202094
Mean absolute error: 0.15730862342432592
Coverage of cases: 92.72727272727273
Instances selection time: 3.0
Test time: 12.0
Accumulative iteration time: 3452.0
Weighted Recall: 0.3616161616161616
Weighted FalsePositiveRate: 0.06383838383838383
Kappa statistic: 0.29777777777777775
Training time: 404.0
		
Iteration: 10
Labeled set size: 279
Unlabelled set size: 216
	
Mean region size: 82.1854912764
Incorrectly Classified Instances: 62.02020202020202
Correctly Classified Instances: 37.97979797979798
Weighted Precision: 0.3730896961444846
Weighted AreaUnderROC: 0.8095959595959595
Root mean squared error: 0.2783622443639623
Relative absolute error: 94.985858585858
Root relative squared error: 96.82845774651351
Weighted TruePositiveRate: 0.3797979797979798
Weighted MatthewsCorrelation: 0.2883757643742883
Weighted FMeasure: 0.29994395543883107
Iteration time: 424.0
Weighted AreaUnderPRC: 0.34477659554629114
Mean absolute error: 0.1570014191501795
Coverage of cases: 93.33333333333333
Instances selection time: 2.0
Test time: 11.0
Accumulative iteration time: 3876.0
Weighted Recall: 0.3797979797979798
Weighted FalsePositiveRate: 0.06202020202020202
Kappa statistic: 0.31777777777777777
Training time: 422.0
		
Iteration: 11
Labeled set size: 299
Unlabelled set size: 196
	
Mean region size: 82.20385674931093
Incorrectly Classified Instances: 60.2020202020202
Correctly Classified Instances: 39.7979797979798
Weighted Precision: 0.297506692111645
Weighted AreaUnderROC: 0.821371492704826
Root mean squared error: 0.2780285612164644
Relative absolute error: 94.86060606060548
Root relative squared error: 96.71238588259372
Weighted TruePositiveRate: 0.397979797979798
Weighted MatthewsCorrelation: 0.29431352543579786
Weighted FMeasure: 0.3114368518823967
Iteration time: 429.0
Weighted AreaUnderPRC: 0.3585634993892533
Mean absolute error: 0.15679439018281996
Coverage of cases: 93.53535353535354
Instances selection time: 3.0
Test time: 10.0
Accumulative iteration time: 4305.0
Weighted Recall: 0.397979797979798
Weighted FalsePositiveRate: 0.060202020202020194
Kappa statistic: 0.3377777777777778
Training time: 426.0
		
Iteration: 12
Labeled set size: 319
Unlabelled set size: 176
	
Mean region size: 82.20385674931093
Incorrectly Classified Instances: 58.18181818181818
Correctly Classified Instances: 41.81818181818182
Weighted Precision: 0.33768580804156073
Weighted AreaUnderROC: 0.8233961840628506
Root mean squared error: 0.2774440329751092
Relative absolute error: 94.6747474747469
Root relative squared error: 96.50905741666246
Weighted TruePositiveRate: 0.41818181818181815
Weighted MatthewsCorrelation: 0.32155502648654005
Weighted FMeasure: 0.3428940087024453
Iteration time: 440.0
Weighted AreaUnderPRC: 0.36715922983897126
Mean absolute error: 0.15648718590867355
Coverage of cases: 93.33333333333333
Instances selection time: 2.0
Test time: 11.0
Accumulative iteration time: 4745.0
Weighted Recall: 0.41818181818181815
Weighted FalsePositiveRate: 0.05818181818181818
Kappa statistic: 0.36
Training time: 438.0
		
Iteration: 13
Labeled set size: 339
Unlabelled set size: 156
	
Mean region size: 82.13039485766718
Incorrectly Classified Instances: 56.76767676767677
Correctly Classified Instances: 43.23232323232323
Weighted Precision: 0.33252284471908194
Weighted AreaUnderROC: 0.8277104377104376
Root mean squared error: 0.2774451271090646
Relative absolute error: 94.65858585858518
Root relative squared error: 96.50943801175254
Weighted TruePositiveRate: 0.43232323232323233
Weighted MatthewsCorrelation: 0.33052760145436916
Weighted FMeasure: 0.3569907919773364
Iteration time: 446.0
Weighted AreaUnderPRC: 0.37270888858285905
Mean absolute error: 0.15646047249353023
Coverage of cases: 92.32323232323232
Instances selection time: 2.0
Test time: 10.0
Accumulative iteration time: 5191.0
Weighted Recall: 0.43232323232323233
Weighted FalsePositiveRate: 0.05676767676767676
Kappa statistic: 0.37555555555555553
Training time: 444.0
		
Iteration: 14
Labeled set size: 359
Unlabelled set size: 136
	
Mean region size: 82.16712580348907
Incorrectly Classified Instances: 55.55555555555556
Correctly Classified Instances: 44.44444444444444
Weighted Precision: 0.3864081264279811
Weighted AreaUnderROC: 0.8376094276094275
Root mean squared error: 0.27676374041071944
Relative absolute error: 94.44444444444385
Root relative squared error: 96.27241727900014
Weighted TruePositiveRate: 0.4444444444444444
Weighted MatthewsCorrelation: 0.35996908494429525
Weighted FMeasure: 0.38946447604934553
Iteration time: 454.0
Weighted AreaUnderPRC: 0.37747258638058995
Mean absolute error: 0.1561065197428834
Coverage of cases: 93.33333333333333
Instances selection time: 2.0
Test time: 10.0
Accumulative iteration time: 5645.0
Weighted Recall: 0.4444444444444444
Weighted FalsePositiveRate: 0.05555555555555556
Kappa statistic: 0.38888888888888884
Training time: 452.0
		
Iteration: 15
Labeled set size: 379
Unlabelled set size: 116
	
Mean region size: 82.29568411386556
Incorrectly Classified Instances: 52.121212121212125
Correctly Classified Instances: 47.878787878787875
Weighted Precision: 0.41915579698062294
Weighted AreaUnderROC: 0.84258810325477
Root mean squared error: 0.2763261998976172
Relative absolute error: 94.29898989898918
Root relative squared error: 96.12021857409994
Weighted TruePositiveRate: 0.47878787878787876
Weighted MatthewsCorrelation: 0.39752460494213215
Weighted FMeasure: 0.4253886918316786
Iteration time: 454.0
Weighted AreaUnderPRC: 0.4020830079475344
Mean absolute error: 0.15586609900659468
Coverage of cases: 93.53535353535354
Instances selection time: 1.0
Test time: 11.0
Accumulative iteration time: 6099.0
Weighted Recall: 0.47878787878787876
Weighted FalsePositiveRate: 0.052121212121212124
Kappa statistic: 0.42666666666666664
Training time: 453.0
		
Iteration: 16
Labeled set size: 399
Unlabelled set size: 96
	
Mean region size: 81.96510560146882
Incorrectly Classified Instances: 50.505050505050505
Correctly Classified Instances: 49.494949494949495
Weighted Precision: 0.4816646405940944
Weighted AreaUnderROC: 0.8533378226711559
Root mean squared error: 0.27469573718706924
Relative absolute error: 93.7454545454539
Root relative squared error: 95.55306123551657
Weighted TruePositiveRate: 0.494949494949495
Weighted MatthewsCorrelation: 0.4237184009551326
Weighted FMeasure: 0.44282557442675036
Iteration time: 455.0
Weighted AreaUnderPRC: 0.42094370614372273
Mean absolute error: 0.1549511645379413
Coverage of cases: 95.15151515151516
Instances selection time: 2.0
Test time: 10.0
Accumulative iteration time: 6554.0
Weighted Recall: 0.494949494949495
Weighted FalsePositiveRate: 0.05050505050505051
Kappa statistic: 0.44444444444444453
Training time: 453.0
		
Iteration: 17
Labeled set size: 419
Unlabelled set size: 76
	
Mean region size: 81.94674012855789
Incorrectly Classified Instances: 47.676767676767675
Correctly Classified Instances: 52.323232323232325
Weighted Precision: 0.5247260542138297
Weighted AreaUnderROC: 0.861023569023569
Root mean squared error: 0.27447087130698844
Relative absolute error: 93.66464646464583
Root relative squared error: 95.47484151711406
Weighted TruePositiveRate: 0.5232323232323233
Weighted MatthewsCorrelation: 0.46606521131630013
Weighted FMeasure: 0.48981825484264435
Iteration time: 462.0
Weighted AreaUnderPRC: 0.4361099749771392
Mean absolute error: 0.15481759746222548
Coverage of cases: 94.74747474747475
Instances selection time: 1.0
Test time: 10.0
Accumulative iteration time: 7016.0
Weighted Recall: 0.5232323232323233
Weighted FalsePositiveRate: 0.04767676767676768
Kappa statistic: 0.47555555555555556
Training time: 461.0
		
Iteration: 18
Labeled set size: 439
Unlabelled set size: 56
	
Mean region size: 82.00183654729071
Incorrectly Classified Instances: 44.84848484848485
Correctly Classified Instances: 55.15151515151515
Weighted Precision: 0.5395137991974502
Weighted AreaUnderROC: 0.88268911335578
Root mean squared error: 0.2720598450373992
Relative absolute error: 92.86868686868627
Root relative squared error: 94.63616472096994
Weighted TruePositiveRate: 0.5515151515151515
Weighted MatthewsCorrelation: 0.4998545646089265
Weighted FMeasure: 0.5305185795662822
Iteration time: 471.0
Weighted AreaUnderPRC: 0.47673618708651816
Mean absolute error: 0.15350196176642458
Coverage of cases: 97.37373737373737
Instances selection time: 1.0
Test time: 10.0
Accumulative iteration time: 7487.0
Weighted Recall: 0.5515151515151515
Weighted FalsePositiveRate: 0.044848484848484846
Kappa statistic: 0.5066666666666666
Training time: 470.0
		
Iteration: 19
Labeled set size: 459
Unlabelled set size: 36
	
Mean region size: 81.98347107437979
Incorrectly Classified Instances: 42.62626262626262
Correctly Classified Instances: 57.37373737373738
Weighted Precision: 0.5475230784868361
Weighted AreaUnderROC: 0.8900763187429853
Root mean squared error: 0.2713450237993489
Relative absolute error: 92.63030303030249
Root relative squared error: 94.38751376543888
Weighted TruePositiveRate: 0.5737373737373738
Weighted MatthewsCorrelation: 0.5191279820491547
Weighted FMeasure: 0.5512004279175209
Iteration time: 477.0
Weighted AreaUnderPRC: 0.4883691277884095
Mean absolute error: 0.15310793889306293
Coverage of cases: 98.38383838383838
Instances selection time: 1.0
Test time: 10.0
Accumulative iteration time: 7964.0
Weighted Recall: 0.5737373737373738
Weighted FalsePositiveRate: 0.04262626262626263
Kappa statistic: 0.5311111111111111
Training time: 476.0
		
Iteration: 20
Labeled set size: 479
Unlabelled set size: 16
	
Mean region size: 82.11202938475631
Incorrectly Classified Instances: 42.82828282828283
Correctly Classified Instances: 57.17171717171717
Weighted Precision: 0.5825972949241199
Weighted AreaUnderROC: 0.9027856341189675
Root mean squared error: 0.2694639614534228
Relative absolute error: 92.01212121212065
Root relative squared error: 93.73318520770923
Weighted TruePositiveRate: 0.5717171717171717
Weighted MatthewsCorrelation: 0.5285160814881908
Weighted FMeasure: 0.5610107182144247
Iteration time: 483.0
Weighted AreaUnderPRC: 0.5018548833287805
Mean absolute error: 0.15208615076383675
Coverage of cases: 99.79797979797979
Instances selection time: 0.0
Test time: 11.0
Accumulative iteration time: 8447.0
Weighted Recall: 0.5717171717171717
Weighted FalsePositiveRate: 0.042828282828282827
Kappa statistic: 0.5288888888888889
Training time: 483.0
		
Time end:Wed Nov 01 15.03.42 EET 2017