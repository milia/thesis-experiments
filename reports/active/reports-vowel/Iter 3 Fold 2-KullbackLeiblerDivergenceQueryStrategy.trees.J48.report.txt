Wed Nov 01 14.53.35 EET 2017
Dataset: vowel
Test set size: 495
Initial Labelled set size: 99
Initial Unlabelled set size: 396
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Wed Nov 01 14.53.35 EET 2017
		
Iteration: 1
Labeled set size: 99
Unlabelled set size: 396
	
Mean region size: 18.84297520661166
Incorrectly Classified Instances: 54.94949494949495
Correctly Classified Instances: 45.05050505050505
Weighted Precision: 0.4667365870107907
Weighted AreaUnderROC: 0.7424354657687992
Root mean squared error: 0.2862471917569525
Relative absolute error: 64.70841455490537
Root relative squared error: 99.57124097568365
Weighted TruePositiveRate: 0.4505050505050505
Weighted MatthewsCorrelation: 0.40040749251603014
Weighted FMeasure: 0.4498429492731039
Iteration time: 7.0
Weighted AreaUnderPRC: 0.31221448832515514
Mean absolute error: 0.10695605711554677
Coverage of cases: 61.21212121212121
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 7.0
Weighted Recall: 0.4505050505050505
Weighted FalsePositiveRate: 0.054949494949494956
Kappa statistic: 0.39555555555555555
Training time: 6.0
		
Iteration: 2
Labeled set size: 119
Unlabelled set size: 376
	
Mean region size: 14.710743801652916
Incorrectly Classified Instances: 57.17171717171717
Correctly Classified Instances: 42.82828282828283
Weighted Precision: 0.4014624763129668
Weighted AreaUnderROC: 0.7425117845117845
Root mean squared error: 0.29788410742988686
Relative absolute error: 64.1809116809113
Root relative squared error: 103.61914840691982
Weighted TruePositiveRate: 0.42828282828282827
Weighted MatthewsCorrelation: 0.35867219560483143
Weighted FMeasure: 0.40473080203916706
Iteration time: 15.0
Weighted AreaUnderPRC: 0.2983925349851636
Mean absolute error: 0.10608415153869703
Coverage of cases: 56.56565656565657
Instances selection time: 5.0
Test time: 4.0
Accumulative iteration time: 22.0
Weighted Recall: 0.42828282828282827
Weighted FalsePositiveRate: 0.057171717171717165
Kappa statistic: 0.37111111111111106
Training time: 10.0
		
Iteration: 3
Labeled set size: 139
Unlabelled set size: 356
	
Mean region size: 19.467401285583097
Incorrectly Classified Instances: 58.58585858585859
Correctly Classified Instances: 41.41414141414141
Weighted Precision: 0.41319227284530624
Weighted AreaUnderROC: 0.7596700336700337
Root mean squared error: 0.296983793025161
Relative absolute error: 66.3153439153435
Root relative squared error: 103.30597355270866
Weighted TruePositiveRate: 0.41414141414141414
Weighted MatthewsCorrelation: 0.3487503195216281
Weighted FMeasure: 0.3830929808914374
Iteration time: 12.0
Weighted AreaUnderPRC: 0.3016045022061295
Mean absolute error: 0.10961213870304783
Coverage of cases: 61.01010101010101
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 34.0
Weighted Recall: 0.41414141414141414
Weighted FalsePositiveRate: 0.05858585858585858
Kappa statistic: 0.3555555555555555
Training time: 11.0
		
Iteration: 4
Labeled set size: 159
Unlabelled set size: 336
	
Mean region size: 15.610651974288416
Incorrectly Classified Instances: 52.92929292929293
Correctly Classified Instances: 47.07070707070707
Weighted Precision: 0.5249269879717261
Weighted AreaUnderROC: 0.7560269360269359
Root mean squared error: 0.2977423687096238
Relative absolute error: 60.99942578237114
Root relative squared error: 103.56984451616627
Weighted TruePositiveRate: 0.4707070707070707
Weighted MatthewsCorrelation: 0.43266955771286597
Weighted FMeasure: 0.4635770844196175
Iteration time: 13.0
Weighted AreaUnderPRC: 0.3221089433442394
Mean absolute error: 0.10082549716094467
Coverage of cases: 61.01010101010101
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 47.0
Weighted Recall: 0.4707070707070707
Weighted FalsePositiveRate: 0.05292929292929292
Kappa statistic: 0.4177777777777778
Training time: 12.0
		
Iteration: 5
Labeled set size: 179
Unlabelled set size: 316
	
Mean region size: 16.841138659320574
Incorrectly Classified Instances: 52.525252525252526
Correctly Classified Instances: 47.474747474747474
Weighted Precision: 0.5552306696322579
Weighted AreaUnderROC: 0.7855241301907969
Root mean squared error: 0.28712302591457045
Relative absolute error: 61.895061728394694
Root relative squared error: 99.87590036265489
Weighted TruePositiveRate: 0.47474747474747475
Weighted MatthewsCorrelation: 0.44292075358492256
Weighted FMeasure: 0.46563956896601977
Iteration time: 15.0
Weighted AreaUnderPRC: 0.3575254594353713
Mean absolute error: 0.10230588715437205
Coverage of cases: 63.03030303030303
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 62.0
Weighted Recall: 0.47474747474747475
Weighted FalsePositiveRate: 0.052525252525252523
Kappa statistic: 0.4222222222222223
Training time: 14.0
		
Iteration: 6
Labeled set size: 199
Unlabelled set size: 296
	
Mean region size: 17.1717171717173
Incorrectly Classified Instances: 54.343434343434346
Correctly Classified Instances: 45.656565656565654
Weighted Precision: 0.5615707259429269
Weighted AreaUnderROC: 0.7900763187429855
Root mean squared error: 0.28884203359324256
Relative absolute error: 62.74330484330429
Root relative squared error: 100.47385811644638
Weighted TruePositiveRate: 0.45656565656565656
Weighted MatthewsCorrelation: 0.41672418911201187
Weighted FMeasure: 0.4235785263348374
Iteration time: 17.0
Weighted AreaUnderPRC: 0.3416957441868558
Mean absolute error: 0.10370794188975982
Coverage of cases: 66.06060606060606
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 79.0
Weighted Recall: 0.45656565656565656
Weighted FalsePositiveRate: 0.05434343434343434
Kappa statistic: 0.40222222222222226
Training time: 16.0
		
Iteration: 7
Labeled set size: 219
Unlabelled set size: 276
	
Mean region size: 16.804407713498716
Incorrectly Classified Instances: 51.91919191919192
Correctly Classified Instances: 48.08080808080808
Weighted Precision: 0.5485177791017399
Weighted AreaUnderROC: 0.7851111111111111
Root mean squared error: 0.28468976563168275
Relative absolute error: 60.570974704307545
Root relative squared error: 99.0294894529203
Weighted TruePositiveRate: 0.4808080808080808
Weighted MatthewsCorrelation: 0.4373263934872241
Weighted FMeasure: 0.4501048593819767
Iteration time: 17.0
Weighted AreaUnderPRC: 0.3469248194416807
Mean absolute error: 0.10011731356083955
Coverage of cases: 64.24242424242425
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 96.0
Weighted Recall: 0.4808080808080808
Weighted FalsePositiveRate: 0.05191919191919193
Kappa statistic: 0.4288888888888889
Training time: 16.0
		
Iteration: 8
Labeled set size: 239
Unlabelled set size: 256
	
Mean region size: 21.836547291092778
Incorrectly Classified Instances: 58.98989898989899
Correctly Classified Instances: 41.01010101010101
Weighted Precision: 0.41555497714615874
Weighted AreaUnderROC: 0.7930729517396184
Root mean squared error: 0.28643252087067494
Relative absolute error: 66.7243902439019
Root relative squared error: 99.63570780845501
Weighted TruePositiveRate: 0.4101010101010101
Weighted MatthewsCorrelation: 0.33378072588469926
Weighted FMeasure: 0.35003375821315863
Iteration time: 18.0
Weighted AreaUnderPRC: 0.33116848404192495
Mean absolute error: 0.11028824833702863
Coverage of cases: 71.11111111111111
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 114.0
Weighted Recall: 0.4101010101010101
Weighted FalsePositiveRate: 0.058989898989898995
Kappa statistic: 0.35111111111111104
Training time: 17.0
		
Iteration: 9
Labeled set size: 259
Unlabelled set size: 236
	
Mean region size: 16.400367309458268
Incorrectly Classified Instances: 52.323232323232325
Correctly Classified Instances: 47.676767676767675
Weighted Precision: 0.4689604825619175
Weighted AreaUnderROC: 0.7917037037037037
Root mean squared error: 0.28322983249945805
Relative absolute error: 60.08378226711519
Root relative squared error: 98.52165092068918
Weighted TruePositiveRate: 0.4767676767676768
Weighted MatthewsCorrelation: 0.4104882886509608
Weighted FMeasure: 0.43648035081699826
Iteration time: 20.0
Weighted AreaUnderPRC: 0.3636201318967868
Mean absolute error: 0.0993120368051497
Coverage of cases: 67.27272727272727
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 134.0
Weighted Recall: 0.4767676767676768
Weighted FalsePositiveRate: 0.05232323232323233
Kappa statistic: 0.4244444444444445
Training time: 19.0
		
Iteration: 10
Labeled set size: 279
Unlabelled set size: 216
	
Mean region size: 17.447199265381148
Incorrectly Classified Instances: 49.09090909090909
Correctly Classified Instances: 50.90909090909091
Weighted Precision: 0.5036565454533531
Weighted AreaUnderROC: 0.8087025813692481
Root mean squared error: 0.27493307227324965
Relative absolute error: 57.40009470711188
Root relative squared error: 95.63561837402683
Weighted TruePositiveRate: 0.509090909090909
Weighted MatthewsCorrelation: 0.4448441400535195
Weighted FMeasure: 0.4632415466558719
Iteration time: 23.0
Weighted AreaUnderPRC: 0.38253803813738835
Mean absolute error: 0.09487618959853264
Coverage of cases: 70.3030303030303
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 157.0
Weighted Recall: 0.509090909090909
Weighted FalsePositiveRate: 0.049090909090909095
Kappa statistic: 0.4599999999999999
Training time: 22.0
		
Iteration: 11
Labeled set size: 299
Unlabelled set size: 196
	
Mean region size: 16.58402203856755
Incorrectly Classified Instances: 48.888888888888886
Correctly Classified Instances: 51.111111111111114
Weighted Precision: 0.5009437899027529
Weighted AreaUnderROC: 0.8167968574635242
Root mean squared error: 0.2725107034700974
Relative absolute error: 56.538970556901184
Root relative squared error: 94.79299607142816
Weighted TruePositiveRate: 0.5111111111111111
Weighted MatthewsCorrelation: 0.4415458533330511
Weighted FMeasure: 0.4579656895502831
Iteration time: 24.0
Weighted AreaUnderPRC: 0.407619214304583
Mean absolute error: 0.09345284389570503
Coverage of cases: 70.5050505050505
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 181.0
Weighted Recall: 0.5111111111111111
Weighted FalsePositiveRate: 0.04888888888888889
Kappa statistic: 0.46222222222222215
Training time: 23.0
		
Iteration: 12
Labeled set size: 319
Unlabelled set size: 176
	
Mean region size: 16.565656565656617
Incorrectly Classified Instances: 44.64646464646464
Correctly Classified Instances: 55.35353535353536
Weighted Precision: 0.6202102018304824
Weighted AreaUnderROC: 0.8196520763187429
Root mean squared error: 0.2671474448175283
Relative absolute error: 52.50457439896002
Root relative squared error: 92.9273836389285
Weighted TruePositiveRate: 0.5535353535353535
Weighted MatthewsCorrelation: 0.5160519681650275
Weighted FMeasure: 0.5280483311969572
Iteration time: 26.0
Weighted AreaUnderPRC: 0.4369987276461018
Mean absolute error: 0.08678442049414935
Coverage of cases: 71.31313131313131
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 207.0
Weighted Recall: 0.5535353535353535
Weighted FalsePositiveRate: 0.04464646464646465
Kappa statistic: 0.5088888888888888
Training time: 25.0
		
Iteration: 13
Labeled set size: 339
Unlabelled set size: 156
	
Mean region size: 18.163452708907272
Incorrectly Classified Instances: 45.45454545454545
Correctly Classified Instances: 54.54545454545455
Weighted Precision: 0.5229547258960711
Weighted AreaUnderROC: 0.8427968574635242
Root mean squared error: 0.264706900590533
Relative absolute error: 52.97036622361666
Root relative squared error: 92.07843900528368
Weighted TruePositiveRate: 0.5454545454545454
Weighted MatthewsCorrelation: 0.48359893837617934
Weighted FMeasure: 0.5094306840579411
Iteration time: 28.0
Weighted AreaUnderPRC: 0.44114494908857504
Mean absolute error: 0.08755432433655702
Coverage of cases: 76.76767676767676
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 235.0
Weighted Recall: 0.5454545454545454
Weighted FalsePositiveRate: 0.045454545454545456
Kappa statistic: 0.49999999999999994
Training time: 27.0
		
Iteration: 14
Labeled set size: 359
Unlabelled set size: 136
	
Mean region size: 16.19834710743802
Incorrectly Classified Instances: 44.04040404040404
Correctly Classified Instances: 55.95959595959596
Weighted Precision: 0.6272222469631792
Weighted AreaUnderROC: 0.8371582491582491
Root mean squared error: 0.25828721452655673
Relative absolute error: 50.38412604098851
Root relative squared error: 89.845347724489
Weighted TruePositiveRate: 0.5595959595959596
Weighted MatthewsCorrelation: 0.5201344073581626
Weighted FMeasure: 0.5326874747356776
Iteration time: 29.0
Weighted AreaUnderPRC: 0.45634588830418665
Mean absolute error: 0.08327954717518816
Coverage of cases: 74.94949494949495
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 264.0
Weighted Recall: 0.5595959595959596
Weighted FalsePositiveRate: 0.04404040404040404
Kappa statistic: 0.5155555555555555
Training time: 28.0
		
Iteration: 15
Labeled set size: 379
Unlabelled set size: 116
	
Mean region size: 17.72268135904506
Incorrectly Classified Instances: 38.98989898989899
Correctly Classified Instances: 61.01010101010101
Weighted Precision: 0.6734134836360992
Weighted AreaUnderROC: 0.8689943883277216
Root mean squared error: 0.2412017595452425
Relative absolute error: 45.83413122824864
Root relative squared error: 83.90216293835454
Weighted TruePositiveRate: 0.6101010101010101
Weighted MatthewsCorrelation: 0.5724999812411816
Weighted FMeasure: 0.5772849511805038
Iteration time: 31.0
Weighted AreaUnderPRC: 0.5155105596622757
Mean absolute error: 0.07575889459214699
Coverage of cases: 81.01010101010101
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 295.0
Weighted Recall: 0.6101010101010101
Weighted FalsePositiveRate: 0.038989898989899
Kappa statistic: 0.5711111111111111
Training time: 30.0
		
Iteration: 16
Labeled set size: 399
Unlabelled set size: 96
	
Mean region size: 14.857667584940355
Incorrectly Classified Instances: 39.7979797979798
Correctly Classified Instances: 60.2020202020202
Weighted Precision: 0.5921708812083144
Weighted AreaUnderROC: 0.849746352413019
Root mean squared error: 0.24940775221620326
Relative absolute error: 45.61667091078833
Root relative squared error: 86.75662194167184
Weighted TruePositiveRate: 0.602020202020202
Weighted MatthewsCorrelation: 0.552417669287783
Weighted FMeasure: 0.5737031468676601
Iteration time: 33.0
Weighted AreaUnderPRC: 0.4914929285429424
Mean absolute error: 0.07539945605089028
Coverage of cases: 76.36363636363636
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 328.0
Weighted Recall: 0.602020202020202
Weighted FalsePositiveRate: 0.0397979797979798
Kappa statistic: 0.5622222222222222
Training time: 32.0
		
Iteration: 17
Labeled set size: 419
Unlabelled set size: 76
	
Mean region size: 17.373737373737402
Incorrectly Classified Instances: 40.60606060606061
Correctly Classified Instances: 59.39393939393939
Weighted Precision: 0.6278956604626093
Weighted AreaUnderROC: 0.8568215488215488
Root mean squared error: 0.2505073438148406
Relative absolute error: 48.38812413298815
Root relative squared error: 87.1391154759166
Weighted TruePositiveRate: 0.593939393939394
Weighted MatthewsCorrelation: 0.5635005162604005
Weighted FMeasure: 0.5931339165269766
Iteration time: 35.0
Weighted AreaUnderPRC: 0.49918415927130805
Mean absolute error: 0.07998037046774953
Coverage of cases: 76.96969696969697
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 363.0
Weighted Recall: 0.593939393939394
Weighted FalsePositiveRate: 0.04060606060606061
Kappa statistic: 0.5533333333333333
Training time: 34.0
		
Iteration: 18
Labeled set size: 439
Unlabelled set size: 56
	
Mean region size: 14.600550964187384
Incorrectly Classified Instances: 40.0
Correctly Classified Instances: 60.0
Weighted Precision: 0.6592634743818314
Weighted AreaUnderROC: 0.8538608305274974
Root mean squared error: 0.2433501828295756
Relative absolute error: 44.573346247822144
Root relative squared error: 84.64949314358407
Weighted TruePositiveRate: 0.6
Weighted MatthewsCorrelation: 0.580645928561308
Weighted FMeasure: 0.6058757869004118
Iteration time: 35.0
Weighted AreaUnderPRC: 0.5214757863896724
Mean absolute error: 0.07367495247573956
Coverage of cases: 76.36363636363636
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 398.0
Weighted Recall: 0.6
Weighted FalsePositiveRate: 0.04
Kappa statistic: 0.5599999999999999
Training time: 34.0
		
Iteration: 19
Labeled set size: 459
Unlabelled set size: 36
	
Mean region size: 13.976124885215867
Incorrectly Classified Instances: 35.55555555555556
Correctly Classified Instances: 64.44444444444444
Weighted Precision: 0.6805847797356166
Weighted AreaUnderROC: 0.8621863075196409
Root mean squared error: 0.23526545054972062
Relative absolute error: 41.23397739638852
Root relative squared error: 81.83721463311105
Weighted TruePositiveRate: 0.6444444444444445
Weighted MatthewsCorrelation: 0.6218307488109632
Weighted FMeasure: 0.6492028831069265
Iteration time: 37.0
Weighted AreaUnderPRC: 0.5544606085844519
Mean absolute error: 0.06815533453948559
Coverage of cases: 77.77777777777777
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 435.0
Weighted Recall: 0.6444444444444445
Weighted FalsePositiveRate: 0.035555555555555556
Kappa statistic: 0.6088888888888889
Training time: 37.0
		
Iteration: 20
Labeled set size: 479
Unlabelled set size: 16
	
Mean region size: 13.35169880624429
Incorrectly Classified Instances: 36.16161616161616
Correctly Classified Instances: 63.83838383838384
Weighted Precision: 0.6696912233287566
Weighted AreaUnderROC: 0.8626262626262625
Root mean squared error: 0.23818854459094896
Relative absolute error: 41.68496830027802
Root relative squared error: 82.8540144814773
Weighted TruePositiveRate: 0.6383838383838384
Weighted MatthewsCorrelation: 0.6112853495912897
Weighted FMeasure: 0.6374699656733781
Iteration time: 38.0
Weighted AreaUnderPRC: 0.5547385312179738
Mean absolute error: 0.06890077405004676
Coverage of cases: 74.74747474747475
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 473.0
Weighted Recall: 0.6383838383838384
Weighted FalsePositiveRate: 0.036161616161616165
Kappa statistic: 0.6022222222222222
Training time: 38.0
		
Time end:Wed Nov 01 14.53.37 EET 2017