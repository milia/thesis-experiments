Wed Nov 01 14.57.23 EET 2017
Dataset: vowel
Test set size: 495
Initial Labelled set size: 99
Initial Unlabelled set size: 396
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Wed Nov 01 14.57.23 EET 2017
		
Iteration: 1
Labeled set size: 99
Unlabelled set size: 396
	
Mean region size: 82.22222222222186
Incorrectly Classified Instances: 75.15151515151516
Correctly Classified Instances: 24.848484848484848
Weighted Precision: 0.28420824243324794
Weighted AreaUnderROC: 0.7419483726150393
Root mean squared error: 0.2795001734719237
Relative absolute error: 95.38989898989831
Root relative squared error: 97.22428700417929
Weighted TruePositiveRate: 0.24848484848484848
Weighted MatthewsCorrelation: 0.18738037535709132
Weighted FMeasure: 0.24809156362866552
Iteration time: 359.0
Weighted AreaUnderPRC: 0.23953100919297232
Mean absolute error: 0.15766925452875855
Coverage of cases: 96.36363636363636
Instances selection time: 5.0
Test time: 10.0
Accumulative iteration time: 359.0
Weighted Recall: 0.24848484848484848
Weighted FalsePositiveRate: 0.07515151515151516
Kappa statistic: 0.17333333333333334
Training time: 354.0
		
Iteration: 2
Labeled set size: 119
Unlabelled set size: 376
	
Mean region size: 82.0018365472907
Incorrectly Classified Instances: 72.72727272727273
Correctly Classified Instances: 27.272727272727273
Weighted Precision: 0.30416867621783833
Weighted AreaUnderROC: 0.7628035914702581
Root mean squared error: 0.2781453631120622
Relative absolute error: 94.91717171717124
Root relative squared error: 96.75301548535629
Weighted TruePositiveRate: 0.2727272727272727
Weighted MatthewsCorrelation: 0.2111929174523344
Weighted FMeasure: 0.27514525701094683
Iteration time: 359.0
Weighted AreaUnderPRC: 0.24980880879157627
Mean absolute error: 0.15688788713582125
Coverage of cases: 96.76767676767676
Instances selection time: 4.0
Test time: 11.0
Accumulative iteration time: 718.0
Weighted Recall: 0.2727272727272727
Weighted FalsePositiveRate: 0.07272727272727272
Kappa statistic: 0.19999999999999998
Training time: 355.0
		
Iteration: 3
Labeled set size: 139
Unlabelled set size: 356
	
Mean region size: 82.1303948576672
Incorrectly Classified Instances: 72.72727272727273
Correctly Classified Instances: 27.272727272727273
Weighted Precision: 0.31426595729594464
Weighted AreaUnderROC: 0.7766060606060606
Root mean squared error: 0.2777506593020512
Relative absolute error: 94.77575757575708
Root relative squared error: 96.61571755087034
Weighted TruePositiveRate: 0.2727272727272727
Weighted MatthewsCorrelation: 0.21496209733106314
Weighted FMeasure: 0.27636358714284226
Iteration time: 366.0
Weighted AreaUnderPRC: 0.24533359883433192
Mean absolute error: 0.15665414475331849
Coverage of cases: 98.98989898989899
Instances selection time: 4.0
Test time: 11.0
Accumulative iteration time: 1084.0
Weighted Recall: 0.2727272727272727
Weighted FalsePositiveRate: 0.07272727272727272
Kappa statistic: 0.19999999999999998
Training time: 362.0
		
Iteration: 4
Labeled set size: 159
Unlabelled set size: 336
	
Mean region size: 82.14876033057814
Incorrectly Classified Instances: 68.68686868686869
Correctly Classified Instances: 31.31313131313131
Weighted Precision: 0.3564862496617766
Weighted AreaUnderROC: 0.7977306397306397
Root mean squared error: 0.2767271040132247
Relative absolute error: 94.46060606060549
Root relative squared error: 96.259673288252
Weighted TruePositiveRate: 0.31313131313131315
Weighted MatthewsCorrelation: 0.25554904121611244
Weighted FMeasure: 0.30263844516559446
Iteration time: 365.0
Weighted AreaUnderPRC: 0.28942248518533814
Mean absolute error: 0.1561332331580266
Coverage of cases: 98.58585858585859
Instances selection time: 4.0
Test time: 11.0
Accumulative iteration time: 1449.0
Weighted Recall: 0.31313131313131315
Weighted FalsePositiveRate: 0.06868686868686869
Kappa statistic: 0.24444444444444446
Training time: 361.0
		
Iteration: 5
Labeled set size: 179
Unlabelled set size: 316
	
Mean region size: 82.27731864095463
Incorrectly Classified Instances: 63.03030303030303
Correctly Classified Instances: 36.96969696969697
Weighted Precision: 0.3998354049294488
Weighted AreaUnderROC: 0.808902356902357
Root mean squared error: 0.27605516151747966
Relative absolute error: 94.24646464646409
Root relative squared error: 96.02593772649865
Weighted TruePositiveRate: 0.3696969696969697
Weighted MatthewsCorrelation: 0.3121053654568402
Weighted FMeasure: 0.3573424871910461
Iteration time: 372.0
Weighted AreaUnderPRC: 0.31760211547905765
Mean absolute error: 0.15577928040737965
Coverage of cases: 97.97979797979798
Instances selection time: 4.0
Test time: 10.0
Accumulative iteration time: 1821.0
Weighted Recall: 0.3696969696969697
Weighted FalsePositiveRate: 0.06303030303030302
Kappa statistic: 0.3066666666666667
Training time: 368.0
		
Iteration: 6
Labeled set size: 199
Unlabelled set size: 296
	
Mean region size: 82.22222222222184
Incorrectly Classified Instances: 58.98989898989899
Correctly Classified Instances: 41.01010101010101
Weighted Precision: 0.4327254536860003
Weighted AreaUnderROC: 0.8258720538720539
Root mean squared error: 0.2749648035821113
Relative absolute error: 93.89090909090856
Root relative squared error: 95.64665612703236
Weighted TruePositiveRate: 0.4101010101010101
Weighted MatthewsCorrelation: 0.34988551926516426
Weighted FMeasure: 0.3895162663523154
Iteration time: 374.0
Weighted AreaUnderPRC: 0.3437045481418597
Mean absolute error: 0.15519158527423002
Coverage of cases: 97.97979797979798
Instances selection time: 4.0
Test time: 10.0
Accumulative iteration time: 2195.0
Weighted Recall: 0.4101010101010101
Weighted FalsePositiveRate: 0.058989898989898995
Kappa statistic: 0.35111111111111104
Training time: 370.0
		
Iteration: 7
Labeled set size: 219
Unlabelled set size: 276
	
Mean region size: 82.14876033057813
Incorrectly Classified Instances: 58.38383838383838
Correctly Classified Instances: 41.61616161616162
Weighted Precision: 0.43594208224593345
Weighted AreaUnderROC: 0.8328215488215487
Root mean squared error: 0.27415238313036444
Relative absolute error: 93.60808080808019
Root relative squared error: 95.36405523205782
Weighted TruePositiveRate: 0.4161616161616162
Weighted MatthewsCorrelation: 0.35986199048446815
Weighted FMeasure: 0.405485515667368
Iteration time: 383.0
Weighted AreaUnderPRC: 0.36107515825501485
Mean absolute error: 0.15472410050922444
Coverage of cases: 97.97979797979798
Instances selection time: 4.0
Test time: 10.0
Accumulative iteration time: 2578.0
Weighted Recall: 0.4161616161616162
Weighted FalsePositiveRate: 0.05838383838383839
Kappa statistic: 0.35777777777777775
Training time: 379.0
		
Iteration: 8
Labeled set size: 239
Unlabelled set size: 256
	
Mean region size: 82.38751147842021
Incorrectly Classified Instances: 56.56565656565657
Correctly Classified Instances: 43.43434343434343
Weighted Precision: 0.4398660527950503
Weighted AreaUnderROC: 0.8325387205387206
Root mean squared error: 0.27396341722951123
Relative absolute error: 93.56363636363575
Root relative squared error: 95.29832334090965
Weighted TruePositiveRate: 0.43434343434343436
Weighted MatthewsCorrelation: 0.373452571224455
Weighted FMeasure: 0.417014827216541
Iteration time: 386.0
Weighted AreaUnderPRC: 0.36294881149068253
Mean absolute error: 0.15465063861758074
Coverage of cases: 98.18181818181819
Instances selection time: 4.0
Test time: 10.0
Accumulative iteration time: 2964.0
Weighted Recall: 0.43434343434343436
Weighted FalsePositiveRate: 0.05656565656565657
Kappa statistic: 0.37777777777777777
Training time: 382.0
		
Iteration: 9
Labeled set size: 259
Unlabelled set size: 236
	
Mean region size: 82.46097337006394
Incorrectly Classified Instances: 55.15151515151515
Correctly Classified Instances: 44.84848484848485
Weighted Precision: 0.4534013127943454
Weighted AreaUnderROC: 0.8338316498316498
Root mean squared error: 0.27374260732345124
Relative absolute error: 93.49898989898931
Root relative squared error: 95.22151449527112
Weighted TruePositiveRate: 0.4484848484848485
Weighted MatthewsCorrelation: 0.38773273162984284
Weighted FMeasure: 0.4283237810257774
Iteration time: 393.0
Weighted AreaUnderPRC: 0.360903208311733
Mean absolute error: 0.15454378495700813
Coverage of cases: 98.18181818181819
Instances selection time: 3.0
Test time: 10.0
Accumulative iteration time: 3357.0
Weighted Recall: 0.4484848484848485
Weighted FalsePositiveRate: 0.05515151515151515
Kappa statistic: 0.3933333333333333
Training time: 390.0
		
Iteration: 10
Labeled set size: 279
Unlabelled set size: 216
	
Mean region size: 82.60789715335139
Incorrectly Classified Instances: 51.515151515151516
Correctly Classified Instances: 48.484848484848484
Weighted Precision: 0.5083496861512412
Weighted AreaUnderROC: 0.8535084175084175
Root mean squared error: 0.2724583363883451
Relative absolute error: 93.0585858585853
Root relative squared error: 94.77478015362527
Weighted TruePositiveRate: 0.48484848484848486
Weighted MatthewsCorrelation: 0.432839576818578
Weighted FMeasure: 0.46601471827894375
Iteration time: 400.0
Weighted AreaUnderPRC: 0.3952257740829527
Mean absolute error: 0.15381584439435686
Coverage of cases: 98.58585858585859
Instances selection time: 3.0
Test time: 10.0
Accumulative iteration time: 3757.0
Weighted Recall: 0.48484848484848486
Weighted FalsePositiveRate: 0.05151515151515152
Kappa statistic: 0.43333333333333335
Training time: 397.0
		
Iteration: 11
Labeled set size: 299
Unlabelled set size: 196
	
Mean region size: 82.57116620752952
Incorrectly Classified Instances: 52.323232323232325
Correctly Classified Instances: 47.676767676767675
Weighted Precision: 0.4954693984193545
Weighted AreaUnderROC: 0.8547609427609427
Root mean squared error: 0.2724302581691403
Relative absolute error: 93.04646464646412
Root relative squared error: 94.7650131298391
Weighted TruePositiveRate: 0.4767676767676768
Weighted MatthewsCorrelation: 0.4215705995042764
Weighted FMeasure: 0.4549539110543895
Iteration time: 409.0
Weighted AreaUnderPRC: 0.3975164358094732
Mean absolute error: 0.15379580933299952
Coverage of cases: 98.78787878787878
Instances selection time: 3.0
Test time: 10.0
Accumulative iteration time: 4166.0
Weighted Recall: 0.4767676767676768
Weighted FalsePositiveRate: 0.05232323232323233
Kappa statistic: 0.4244444444444445
Training time: 406.0
		
Iteration: 12
Labeled set size: 319
Unlabelled set size: 176
	
Mean region size: 82.64462809917326
Incorrectly Classified Instances: 50.303030303030305
Correctly Classified Instances: 49.696969696969695
Weighted Precision: 0.5244016166105112
Weighted AreaUnderROC: 0.8604130190796857
Root mean squared error: 0.2721392776584443
Relative absolute error: 92.9494949494944
Root relative squared error: 94.6637954013021
Weighted TruePositiveRate: 0.49696969696969695
Weighted MatthewsCorrelation: 0.44227219896108433
Weighted FMeasure: 0.4666589882779836
Iteration time: 411.0
Weighted AreaUnderPRC: 0.4075113954388208
Mean absolute error: 0.1536355288421405
Coverage of cases: 98.98989898989899
Instances selection time: 2.0
Test time: 10.0
Accumulative iteration time: 4577.0
Weighted Recall: 0.49696969696969695
Weighted FalsePositiveRate: 0.05030303030303031
Kappa statistic: 0.4466666666666666
Training time: 409.0
		
Iteration: 13
Labeled set size: 339
Unlabelled set size: 156
	
Mean region size: 82.60789715335137
Incorrectly Classified Instances: 51.111111111111114
Correctly Classified Instances: 48.888888888888886
Weighted Precision: 0.4917507400239083
Weighted AreaUnderROC: 0.8624466891133559
Root mean squared error: 0.27203998325816375
Relative absolute error: 92.90505050505001
Root relative squared error: 94.62925579028553
Weighted TruePositiveRate: 0.4888888888888889
Weighted MatthewsCorrelation: 0.42872658273849606
Weighted FMeasure: 0.4622957457403644
Iteration time: 421.0
Weighted AreaUnderPRC: 0.4029163960004661
Mean absolute error: 0.15356206695049687
Coverage of cases: 98.38383838383838
Instances selection time: 2.0
Test time: 10.0
Accumulative iteration time: 4998.0
Weighted Recall: 0.4888888888888889
Weighted FalsePositiveRate: 0.05111111111111111
Kappa statistic: 0.4377777777777778
Training time: 419.0
		
Iteration: 14
Labeled set size: 359
Unlabelled set size: 136
	
Mean region size: 82.62626262626227
Incorrectly Classified Instances: 48.08080808080808
Correctly Classified Instances: 51.91919191919192
Weighted Precision: 0.5095540523599139
Weighted AreaUnderROC: 0.875364758698092
Root mean squared error: 0.2712293225576517
Relative absolute error: 92.63030303030251
Root relative squared error: 94.3472670257329
Weighted TruePositiveRate: 0.5191919191919192
Weighted MatthewsCorrelation: 0.4574149135459887
Weighted FMeasure: 0.48976551934652546
Iteration time: 431.0
Weighted AreaUnderPRC: 0.43015390557544275
Mean absolute error: 0.15310793889306298
Coverage of cases: 98.58585858585859
Instances selection time: 1.0
Test time: 10.0
Accumulative iteration time: 5429.0
Weighted Recall: 0.5191919191919192
Weighted FalsePositiveRate: 0.048080808080808085
Kappa statistic: 0.4711111111111111
Training time: 430.0
		
Iteration: 15
Labeled set size: 379
Unlabelled set size: 116
	
Mean region size: 82.442607897153
Incorrectly Classified Instances: 47.676767676767675
Correctly Classified Instances: 52.323232323232325
Weighted Precision: 0.5139764581107426
Weighted AreaUnderROC: 0.8762918069584736
Root mean squared error: 0.2708200558392603
Relative absolute error: 92.48484848484804
Root relative squared error: 94.20490337566473
Weighted TruePositiveRate: 0.5232323232323233
Weighted MatthewsCorrelation: 0.46190617403678563
Weighted FMeasure: 0.49367773112497443
Iteration time: 436.0
Weighted AreaUnderPRC: 0.4231744443874279
Mean absolute error: 0.1528675181567746
Coverage of cases: 99.1919191919192
Instances selection time: 1.0
Test time: 10.0
Accumulative iteration time: 5865.0
Weighted Recall: 0.5232323232323233
Weighted FalsePositiveRate: 0.047676767676767685
Kappa statistic: 0.47555555555555556
Training time: 435.0
		
Iteration: 16
Labeled set size: 399
Unlabelled set size: 96
	
Mean region size: 82.31404958677649
Incorrectly Classified Instances: 44.44444444444444
Correctly Classified Instances: 55.55555555555556
Weighted Precision: 0.5463766492105042
Weighted AreaUnderROC: 0.8868035914702581
Root mean squared error: 0.2701988144171498
Relative absolute error: 92.2787878787874
Root relative squared error: 93.98880420988651
Weighted TruePositiveRate: 0.5555555555555556
Weighted MatthewsCorrelation: 0.5008991847285228
Weighted FMeasure: 0.5351827817828338
Iteration time: 442.0
Weighted AreaUnderPRC: 0.4621187551653052
Mean absolute error: 0.15252692211369914
Coverage of cases: 98.98989898989899
Instances selection time: 2.0
Test time: 10.0
Accumulative iteration time: 6307.0
Weighted Recall: 0.5555555555555556
Weighted FalsePositiveRate: 0.044444444444444446
Kappa statistic: 0.5111111111111111
Training time: 440.0
		
Iteration: 17
Labeled set size: 419
Unlabelled set size: 76
	
Mean region size: 82.2589531680437
Incorrectly Classified Instances: 45.85858585858586
Correctly Classified Instances: 54.14141414141414
Weighted Precision: 0.5361687314698399
Weighted AreaUnderROC: 0.8884421997755331
Root mean squared error: 0.27001315260748354
Relative absolute error: 92.21010101010052
Root relative squared error: 93.92422164865056
Weighted TruePositiveRate: 0.5414141414141415
Weighted MatthewsCorrelation: 0.4874547720170361
Weighted FMeasure: 0.5237326122675616
Iteration time: 448.0
Weighted AreaUnderPRC: 0.46545591196847114
Mean absolute error: 0.15241339009934068
Coverage of cases: 98.58585858585859
Instances selection time: 1.0
Test time: 10.0
Accumulative iteration time: 6755.0
Weighted Recall: 0.5414141414141415
Weighted FalsePositiveRate: 0.045858585858585856
Kappa statistic: 0.4955555555555556
Training time: 447.0
		
Iteration: 18
Labeled set size: 439
Unlabelled set size: 56
	
Mean region size: 82.18549127639999
Incorrectly Classified Instances: 46.26262626262626
Correctly Classified Instances: 53.73737373737374
Weighted Precision: 0.5227254030289834
Weighted AreaUnderROC: 0.8950909090909092
Root mean squared error: 0.2697350955248005
Relative absolute error: 92.11717171717122
Root relative squared error: 93.82749934156035
Weighted TruePositiveRate: 0.5373737373737374
Weighted MatthewsCorrelation: 0.4795344347027869
Weighted FMeasure: 0.5177727711232767
Iteration time: 458.0
Weighted AreaUnderPRC: 0.46503283966868986
Mean absolute error: 0.15225978796226744
Coverage of cases: 99.39393939393939
Instances selection time: 1.0
Test time: 9.0
Accumulative iteration time: 7213.0
Weighted Recall: 0.5373737373737374
Weighted FalsePositiveRate: 0.04626262626262627
Kappa statistic: 0.4911111111111111
Training time: 457.0
		
Iteration: 19
Labeled set size: 459
Unlabelled set size: 36
	
Mean region size: 82.14876033057811
Incorrectly Classified Instances: 45.05050505050505
Correctly Classified Instances: 54.94949494949495
Weighted Precision: 0.5328787737377553
Weighted AreaUnderROC: 0.9012727272727272
Root mean squared error: 0.2694252057408775
Relative absolute error: 92.00808080808027
Root relative squared error: 93.71970401207079
Weighted TruePositiveRate: 0.5494949494949495
Weighted MatthewsCorrelation: 0.49199881346051194
Weighted FMeasure: 0.5291889278402797
Iteration time: 466.0
Weighted AreaUnderPRC: 0.4762244434774483
Mean absolute error: 0.152079472410051
Coverage of cases: 99.1919191919192
Instances selection time: 0.0
Test time: 10.0
Accumulative iteration time: 7679.0
Weighted Recall: 0.5494949494949495
Weighted FalsePositiveRate: 0.04505050505050505
Kappa statistic: 0.5044444444444444
Training time: 466.0
		
Iteration: 20
Labeled set size: 479
Unlabelled set size: 16
	
Mean region size: 82.02020202020161
Incorrectly Classified Instances: 44.24242424242424
Correctly Classified Instances: 55.75757575757576
Weighted Precision: 0.5465828920444131
Weighted AreaUnderROC: 0.9044646464646464
Root mean squared error: 0.26922074467498947
Relative absolute error: 91.94343434343371
Root relative squared error: 93.64858211935727
Weighted TruePositiveRate: 0.5575757575757576
Weighted MatthewsCorrelation: 0.5064400701840202
Weighted FMeasure: 0.5479038499612062
Iteration time: 480.0
Weighted AreaUnderPRC: 0.4809906631264627
Mean absolute error: 0.15197261874947818
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 10.0
Accumulative iteration time: 8159.0
Weighted Recall: 0.5575757575757576
Weighted FalsePositiveRate: 0.04424242424242425
Kappa statistic: 0.5133333333333333
Training time: 479.0
		
Time end:Wed Nov 01 14.57.33 EET 2017