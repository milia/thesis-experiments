Wed Nov 01 14.53.16 EET 2017
Dataset: vowel
Test set size: 495
Initial Labelled set size: 99
Initial Unlabelled set size: 396
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Wed Nov 01 14.53.16 EET 2017
		
Iteration: 1
Labeled set size: 99
Unlabelled set size: 396
	
Mean region size: 54.545454545454064
Incorrectly Classified Instances: 64.04040404040404
Correctly Classified Instances: 35.95959595959596
Weighted Precision: 0.4153262343904055
Weighted AreaUnderROC: 0.6477777777777778
Root mean squared error: 0.32499205330398956
Relative absolute error: 73.4000000000004
Root relative squared error: 113.04866208849995
Weighted TruePositiveRate: 0.3595959595959596
Weighted MatthewsCorrelation: 0.31601618541092175
Weighted FMeasure: 0.3662865647559416
Iteration time: 12.0
Weighted AreaUnderPRC: 0.20352876391319646
Mean absolute error: 0.12132231404958824
Coverage of cases: 67.47474747474747
Instances selection time: 11.0
Test time: 10.0
Accumulative iteration time: 12.0
Weighted Recall: 0.3595959595959596
Weighted FalsePositiveRate: 0.06404040404040404
Kappa statistic: 0.2955555555555555
Training time: 1.0
		
Iteration: 2
Labeled set size: 119
Unlabelled set size: 376
	
Mean region size: 45.45454545454594
Incorrectly Classified Instances: 60.0
Correctly Classified Instances: 40.0
Weighted Precision: 0.46408906905582914
Weighted AreaUnderROC: 0.6699999999999999
Root mean squared error: 0.316941407576339
Relative absolute error: 68.87692307692272
Root relative squared error: 110.24824060370722
Weighted TruePositiveRate: 0.4
Weighted MatthewsCorrelation: 0.3616924408848215
Weighted FMeasure: 0.4034955584643086
Iteration time: 14.0
Weighted AreaUnderPRC: 0.2374685619352431
Mean absolute error: 0.11384615384615399
Coverage of cases: 63.03030303030303
Instances selection time: 13.0
Test time: 10.0
Accumulative iteration time: 26.0
Weighted Recall: 0.4
Weighted FalsePositiveRate: 0.060000000000000005
Kappa statistic: 0.34
Training time: 1.0
		
Iteration: 3
Labeled set size: 139
Unlabelled set size: 356
	
Mean region size: 36.36363636363658
Incorrectly Classified Instances: 58.98989898989899
Correctly Classified Instances: 41.01010101010101
Weighted Precision: 0.4808593181458331
Weighted AreaUnderROC: 0.6755555555555555
Root mean squared error: 0.31596437517375403
Relative absolute error: 67.46370370370326
Root relative squared error: 109.90837935231215
Weighted TruePositiveRate: 0.4101010101010101
Weighted MatthewsCorrelation: 0.3743200831422773
Weighted FMeasure: 0.4122890758479909
Iteration time: 14.0
Weighted AreaUnderPRC: 0.24566550872518442
Mean absolute error: 0.1115102540557086
Coverage of cases: 59.39393939393939
Instances selection time: 13.0
Test time: 12.0
Accumulative iteration time: 40.0
Weighted Recall: 0.4101010101010101
Weighted FalsePositiveRate: 0.05898989898989899
Kappa statistic: 0.35111111111111104
Training time: 1.0
		
Iteration: 4
Labeled set size: 159
Unlabelled set size: 336
	
Mean region size: 27.272727272727032
Incorrectly Classified Instances: 54.74747474747475
Correctly Classified Instances: 45.25252525252525
Weighted Precision: 0.5179354742323954
Weighted AreaUnderROC: 0.6988888888888889
Root mean squared error: 0.3056893922863809
Relative absolute error: 62.796078431371804
Root relative squared error: 106.33422097954342
Weighted TruePositiveRate: 0.45252525252525255
Weighted MatthewsCorrelation: 0.4190547311308966
Weighted FMeasure: 0.4549585054885445
Iteration time: 15.0
Weighted AreaUnderPRC: 0.2831221232694918
Mean absolute error: 0.10379517096094579
Coverage of cases: 55.15151515151515
Instances selection time: 14.0
Test time: 12.0
Accumulative iteration time: 55.0
Weighted Recall: 0.45252525252525255
Weighted FalsePositiveRate: 0.05474747474747475
Kappa statistic: 0.3977777777777778
Training time: 1.0
		
Iteration: 5
Labeled set size: 179
Unlabelled set size: 316
	
Mean region size: 18.18181818181829
Incorrectly Classified Instances: 51.91919191919192
Correctly Classified Instances: 48.08080808080808
Weighted Precision: 0.5349251288297179
Weighted AreaUnderROC: 0.7144444444444445
Root mean squared error: 0.2986810355139071
Relative absolute error: 59.59415204678343
Root relative squared error: 103.89636027337421
Weighted TruePositiveRate: 0.4808080808080808
Weighted MatthewsCorrelation: 0.4439044680618257
Weighted FMeasure: 0.4762851936972261
Iteration time: 15.0
Weighted AreaUnderPRC: 0.30951370741988693
Mean absolute error: 0.09850273065584102
Coverage of cases: 52.72727272727273
Instances selection time: 14.0
Test time: 13.0
Accumulative iteration time: 70.0
Weighted Recall: 0.4808080808080808
Weighted FalsePositiveRate: 0.05191919191919192
Kappa statistic: 0.4288888888888889
Training time: 1.0
		
Iteration: 6
Labeled set size: 199
Unlabelled set size: 296
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 48.08080808080808
Correctly Classified Instances: 51.91919191919192
Weighted Precision: 0.5659141389422319
Weighted AreaUnderROC: 0.7355555555555556
Root mean squared error: 0.2882139549822399
Relative absolute error: 55.35661375661369
Root relative squared error: 100.25538063080201
Weighted TruePositiveRate: 0.5191919191919192
Weighted MatthewsCorrelation: 0.4800930493536291
Weighted FMeasure: 0.505015008017074
Iteration time: 15.0
Weighted AreaUnderPRC: 0.33330009749736383
Mean absolute error: 0.09149853513489925
Coverage of cases: 51.91919191919192
Instances selection time: 14.0
Test time: 14.0
Accumulative iteration time: 85.0
Weighted Recall: 0.5191919191919192
Weighted FalsePositiveRate: 0.04808080808080808
Kappa statistic: 0.4711111111111111
Training time: 1.0
		
Iteration: 7
Labeled set size: 219
Unlabelled set size: 276
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 44.44444444444444
Correctly Classified Instances: 55.55555555555556
Weighted Precision: 0.5988284081313121
Weighted AreaUnderROC: 0.7555555555555555
Root mean squared error: 0.2777271726450049
Relative absolute error: 51.33333333333298
Root relative squared error: 96.60754770447251
Weighted TruePositiveRate: 0.5555555555555556
Weighted MatthewsCorrelation: 0.5160177847813546
Weighted FMeasure: 0.5327879195882489
Iteration time: 15.0
Weighted AreaUnderPRC: 0.3630494673603459
Mean absolute error: 0.08484848484848481
Coverage of cases: 55.55555555555556
Instances selection time: 14.0
Test time: 16.0
Accumulative iteration time: 100.0
Weighted Recall: 0.5555555555555556
Weighted FalsePositiveRate: 0.044444444444444446
Kappa statistic: 0.5111111111111111
Training time: 1.0
		
Iteration: 8
Labeled set size: 239
Unlabelled set size: 256
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 42.02020202020202
Correctly Classified Instances: 57.97979797979798
Weighted Precision: 0.6206319483779886
Weighted AreaUnderROC: 0.768888888888889
Root mean squared error: 0.2705526772143316
Relative absolute error: 48.588444444444264
Root relative squared error: 94.1118955758987
Weighted TruePositiveRate: 0.5797979797979798
Weighted MatthewsCorrelation: 0.5391934179581125
Weighted FMeasure: 0.549763875875146
Iteration time: 15.0
Weighted AreaUnderPRC: 0.38470561081000776
Mean absolute error: 0.08031147842056954
Coverage of cases: 57.97979797979798
Instances selection time: 14.0
Test time: 16.0
Accumulative iteration time: 115.0
Weighted Recall: 0.5797979797979798
Weighted FalsePositiveRate: 0.04202020202020202
Kappa statistic: 0.5377777777777777
Training time: 1.0
		
Iteration: 9
Labeled set size: 259
Unlabelled set size: 236
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 39.39393939393939
Correctly Classified Instances: 60.60606060606061
Weighted Precision: 0.6438816065534828
Weighted AreaUnderROC: 0.7833333333333333
Root mean squared error: 0.26238225736342985
Relative absolute error: 45.641975308641626
Root relative squared error: 91.2698105973414
Weighted TruePositiveRate: 0.6060606060606061
Weighted MatthewsCorrelation: 0.5687877386294535
Weighted FMeasure: 0.580150790012286
Iteration time: 15.0
Weighted AreaUnderPRC: 0.4147174942577293
Mean absolute error: 0.07544128150188747
Coverage of cases: 60.60606060606061
Instances selection time: 14.0
Test time: 17.0
Accumulative iteration time: 130.0
Weighted Recall: 0.6060606060606061
Weighted FalsePositiveRate: 0.03939393939393939
Kappa statistic: 0.5666666666666667
Training time: 1.0
		
Iteration: 10
Labeled set size: 279
Unlabelled set size: 216
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 35.75757575757576
Correctly Classified Instances: 64.24242424242425
Weighted Precision: 0.6883617901098986
Weighted AreaUnderROC: 0.8033333333333332
Root mean squared error: 0.25033305906626346
Relative absolute error: 41.63448275862039
Root relative squared error: 87.07849043155392
Weighted TruePositiveRate: 0.6424242424242425
Weighted MatthewsCorrelation: 0.6085234449301088
Weighted FMeasure: 0.6122754006402417
Iteration time: 15.0
Weighted AreaUnderPRC: 0.46204162531049897
Mean absolute error: 0.06881732687375315
Coverage of cases: 64.24242424242425
Instances selection time: 14.0
Test time: 17.0
Accumulative iteration time: 145.0
Weighted Recall: 0.6424242424242425
Weighted FalsePositiveRate: 0.03575757575757576
Kappa statistic: 0.6066666666666667
Training time: 1.0
		
Iteration: 11
Labeled set size: 299
Unlabelled set size: 196
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 32.92929292929293
Correctly Classified Instances: 67.07070707070707
Weighted Precision: 0.697506263923698
Weighted AreaUnderROC: 0.8188888888888889
Root mean squared error: 0.24052240945635195
Relative absolute error: 38.485304659497956
Root relative squared error: 83.66585064130601
Weighted TruePositiveRate: 0.6707070707070707
Weighted MatthewsCorrelation: 0.6348610926982997
Weighted FMeasure: 0.6431333447725521
Iteration time: 13.0
Weighted AreaUnderPRC: 0.49383196458861645
Mean absolute error: 0.0636120738173524
Coverage of cases: 67.07070707070707
Instances selection time: 12.0
Test time: 19.0
Accumulative iteration time: 158.0
Weighted Recall: 0.6707070707070707
Weighted FalsePositiveRate: 0.03292929292929293
Kappa statistic: 0.6377777777777778
Training time: 1.0
		
Iteration: 12
Labeled set size: 319
Unlabelled set size: 176
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 30.90909090909091
Correctly Classified Instances: 69.0909090909091
Weighted Precision: 0.7141044952357467
Weighted AreaUnderROC: 0.83
Root mean squared error: 0.23327429398688115
Relative absolute error: 36.199999999999704
Root relative squared error: 81.1445897422889
Weighted TruePositiveRate: 0.6909090909090909
Weighted MatthewsCorrelation: 0.6559843215039219
Weighted FMeasure: 0.6635288543460613
Iteration time: 13.0
Weighted AreaUnderPRC: 0.5250362505115856
Mean absolute error: 0.059834710743801554
Coverage of cases: 69.0909090909091
Instances selection time: 12.0
Test time: 19.0
Accumulative iteration time: 171.0
Weighted Recall: 0.6909090909090909
Weighted FalsePositiveRate: 0.030909090909090907
Kappa statistic: 0.66
Training time: 1.0
		
Iteration: 13
Labeled set size: 339
Unlabelled set size: 156
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 27.07070707070707
Correctly Classified Instances: 72.92929292929293
Weighted Precision: 0.7760925405835515
Weighted AreaUnderROC: 0.8511111111111112
Root mean squared error: 0.21852734956395983
Relative absolute error: 31.984761904761573
Root relative squared error: 76.01485712281057
Weighted TruePositiveRate: 0.7292929292929293
Weighted MatthewsCorrelation: 0.7048362488793981
Weighted FMeasure: 0.7033115583061221
Iteration time: 13.0
Weighted AreaUnderPRC: 0.5778898394877686
Mean absolute error: 0.052867375049193026
Coverage of cases: 72.92929292929293
Instances selection time: 12.0
Test time: 20.0
Accumulative iteration time: 184.0
Weighted Recall: 0.7292929292929293
Weighted FalsePositiveRate: 0.027070707070707068
Kappa statistic: 0.7022222222222223
Training time: 1.0
		
Iteration: 14
Labeled set size: 359
Unlabelled set size: 136
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 26.060606060606062
Correctly Classified Instances: 73.93939393939394
Weighted Precision: 0.7941512495513514
Weighted AreaUnderROC: 0.8566666666666667
Root mean squared error: 0.21458631065495173
Relative absolute error: 30.787387387387245
Root relative squared error: 74.64396459983139
Weighted TruePositiveRate: 0.7393939393939394
Weighted MatthewsCorrelation: 0.7176096650855539
Weighted FMeasure: 0.7112562708962765
Iteration time: 12.0
Weighted AreaUnderPRC: 0.591372832364701
Mean absolute error: 0.050888243615516436
Coverage of cases: 73.93939393939394
Instances selection time: 11.0
Test time: 21.0
Accumulative iteration time: 196.0
Weighted Recall: 0.7393939393939394
Weighted FalsePositiveRate: 0.026060606060606062
Kappa statistic: 0.7133333333333333
Training time: 1.0
		
Iteration: 15
Labeled set size: 379
Unlabelled set size: 116
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 21.818181818181817
Correctly Classified Instances: 78.18181818181819
Weighted Precision: 0.8177395969261997
Weighted AreaUnderROC: 0.8799999999999999
Root mean squared error: 0.19651027094865675
Relative absolute error: 26.1435897435896
Root relative squared error: 68.35620437960276
Weighted TruePositiveRate: 0.7818181818181819
Weighted MatthewsCorrelation: 0.7622972237628604
Weighted FMeasure: 0.7613903874325522
Iteration time: 11.0
Weighted AreaUnderPRC: 0.652207269496639
Mean absolute error: 0.04321254503072689
Coverage of cases: 78.18181818181819
Instances selection time: 10.0
Test time: 22.0
Accumulative iteration time: 207.0
Weighted Recall: 0.7818181818181819
Weighted FalsePositiveRate: 0.021818181818181816
Kappa statistic: 0.76
Training time: 1.0
		
Iteration: 16
Labeled set size: 399
Unlabelled set size: 96
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 19.7979797979798
Correctly Classified Instances: 80.20202020202021
Weighted Precision: 0.8418643896842358
Weighted AreaUnderROC: 0.8911111111111111
Root mean squared error: 0.18732341069146513
Relative absolute error: 23.876422764227453
Root relative squared error: 65.16055005417813
Weighted TruePositiveRate: 0.802020202020202
Weighted MatthewsCorrelation: 0.7855462920030316
Weighted FMeasure: 0.7818381853625046
Iteration time: 11.0
Weighted AreaUnderPRC: 0.6810104778455099
Mean absolute error: 0.03946516159376464
Coverage of cases: 80.20202020202021
Instances selection time: 9.0
Test time: 23.0
Accumulative iteration time: 218.0
Weighted Recall: 0.802020202020202
Weighted FalsePositiveRate: 0.019797979797979797
Kappa statistic: 0.7822222222222222
Training time: 2.0
		
Iteration: 17
Labeled set size: 419
Unlabelled set size: 76
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 15.353535353535353
Correctly Classified Instances: 84.64646464646465
Weighted Precision: 0.8757484554602958
Weighted AreaUnderROC: 0.9155555555555556
Root mean squared error: 0.16509235048739646
Relative absolute error: 19.014987080103268
Root relative squared error: 57.42746369920785
Weighted TruePositiveRate: 0.8464646464646465
Weighted MatthewsCorrelation: 0.8310128519158408
Weighted FMeasure: 0.8266890518563001
Iteration time: 8.0
Weighted AreaUnderPRC: 0.7420230318057858
Mean absolute error: 0.03142973071091469
Coverage of cases: 84.64646464646465
Instances selection time: 7.0
Test time: 24.0
Accumulative iteration time: 226.0
Weighted Recall: 0.8464646464646465
Weighted FalsePositiveRate: 0.015353535353535354
Kappa statistic: 0.8311111111111111
Training time: 1.0
		
Iteration: 18
Labeled set size: 439
Unlabelled set size: 56
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 12.121212121212121
Correctly Classified Instances: 87.87878787878788
Weighted Precision: 0.899840062587098
Weighted AreaUnderROC: 0.9333333333333333
Root mean squared error: 0.146796558782697
Relative absolute error: 15.451851851851703
Root relative squared error: 51.06326262709271
Weighted TruePositiveRate: 0.8787878787878788
Weighted MatthewsCorrelation: 0.86768462692468
Weighted FMeasure: 0.8670895178512921
Iteration time: 7.0
Weighted AreaUnderPRC: 0.7921785255360135
Mean absolute error: 0.025540250994796367
Coverage of cases: 87.87878787878788
Instances selection time: 6.0
Test time: 24.0
Accumulative iteration time: 233.0
Weighted Recall: 0.8787878787878788
Weighted FalsePositiveRate: 0.012121212121212121
Kappa statistic: 0.8666666666666667
Training time: 1.0
		
Iteration: 19
Labeled set size: 459
Unlabelled set size: 36
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 10.909090909090908
Correctly Classified Instances: 89.0909090909091
Weighted Precision: 0.9102302111590649
Weighted AreaUnderROC: 0.9400000000000001
Root mean squared error: 0.1393404588122936
Relative absolute error: 14.059574468084996
Root relative squared error: 48.46965420656995
Weighted TruePositiveRate: 0.8909090909090909
Weighted MatthewsCorrelation: 0.8808582614505217
Weighted FMeasure: 0.8801252593559332
Iteration time: 5.0
Weighted AreaUnderPRC: 0.8128434603019584
Mean absolute error: 0.023238966062950554
Coverage of cases: 89.0909090909091
Instances selection time: 4.0
Test time: 27.0
Accumulative iteration time: 238.0
Weighted Recall: 0.8909090909090909
Weighted FalsePositiveRate: 0.01090909090909091
Kappa statistic: 0.88
Training time: 1.0
		
Iteration: 20
Labeled set size: 479
Unlabelled set size: 16
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 5.454545454545454
Correctly Classified Instances: 94.54545454545455
Weighted Precision: 0.9494037166985358
Weighted AreaUnderROC: 0.9700000000000001
Root mean squared error: 0.09867304685849283
Relative absolute error: 8.110204081632624
Root relative squared error: 34.32347289154944
Weighted TruePositiveRate: 0.9454545454545454
Weighted MatthewsCorrelation: 0.9411557920625813
Weighted FMeasure: 0.9453447281341428
Iteration time: 4.0
Weighted AreaUnderPRC: 0.9014512272592193
Mean absolute error: 0.013405296002698637
Coverage of cases: 94.54545454545455
Instances selection time: 3.0
Test time: 26.0
Accumulative iteration time: 242.0
Weighted Recall: 0.9454545454545454
Weighted FalsePositiveRate: 0.005454545454545455
Kappa statistic: 0.94
Training time: 1.0
		
Time end:Wed Nov 01 14.53.17 EET 2017