Wed Nov 01 14.50.26 EET 2017
Dataset: vowel
Test set size: 495
Initial Labelled set size: 99
Initial Unlabelled set size: 396
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Nov 01 14.50.26 EET 2017
		
Iteration: 1
Labeled set size: 99
Unlabelled set size: 396
	
Mean region size: 54.545454545454064
Incorrectly Classified Instances: 61.41414141414141
Correctly Classified Instances: 38.58585858585859
Weighted Precision: 0.42787521013862667
Weighted AreaUnderROC: 0.6622222222222222
Root mean squared error: 0.3183116618183918
Relative absolute error: 70.80000000000037
Root relative squared error: 110.72488428533063
Weighted TruePositiveRate: 0.38585858585858585
Weighted MatthewsCorrelation: 0.3362593802728566
Weighted FMeasure: 0.3815403419227148
Iteration time: 12.0
Weighted AreaUnderPRC: 0.214773068348759
Mean absolute error: 0.1170247933884311
Coverage of cases: 67.87878787878788
Instances selection time: 12.0
Test time: 10.0
Accumulative iteration time: 12.0
Weighted Recall: 0.38585858585858585
Weighted FalsePositiveRate: 0.061414141414141414
Kappa statistic: 0.3244444444444445
Training time: 0.0
		
Iteration: 2
Labeled set size: 119
Unlabelled set size: 376
	
Mean region size: 45.45454545454594
Incorrectly Classified Instances: 58.18181818181818
Correctly Classified Instances: 41.81818181818182
Weighted Precision: 0.4499430998020029
Weighted AreaUnderROC: 0.6799999999999999
Root mean squared error: 0.3121310477863815
Relative absolute error: 67.0461538461535
Root relative squared error: 108.57495434058019
Weighted TruePositiveRate: 0.41818181818181815
Weighted MatthewsCorrelation: 0.3688155744177912
Weighted FMeasure: 0.4138101809326732
Iteration time: 6.0
Weighted AreaUnderPRC: 0.2359840478498218
Mean absolute error: 0.11082008900190732
Coverage of cases: 66.46464646464646
Instances selection time: 6.0
Test time: 11.0
Accumulative iteration time: 18.0
Weighted Recall: 0.41818181818181815
Weighted FalsePositiveRate: 0.058181818181818175
Kappa statistic: 0.36
Training time: 0.0
		
Iteration: 3
Labeled set size: 139
Unlabelled set size: 356
	
Mean region size: 36.36363636363658
Incorrectly Classified Instances: 53.93939393939394
Correctly Classified Instances: 46.06060606060606
Weighted Precision: 0.481636639243982
Weighted AreaUnderROC: 0.7033333333333334
Root mean squared error: 0.3021988593573406
Relative absolute error: 62.3155555555551
Root relative squared error: 105.12003720614892
Weighted TruePositiveRate: 0.46060606060606063
Weighted MatthewsCorrelation: 0.41050559761511307
Weighted FMeasure: 0.4519858124899016
Iteration time: 7.0
Weighted AreaUnderPRC: 0.2742593691158346
Mean absolute error: 0.10300091827364545
Coverage of cases: 63.23232323232323
Instances selection time: 7.0
Test time: 12.0
Accumulative iteration time: 25.0
Weighted Recall: 0.46060606060606063
Weighted FalsePositiveRate: 0.05393939393939395
Kappa statistic: 0.40666666666666673
Training time: 0.0
		
Iteration: 4
Labeled set size: 159
Unlabelled set size: 336
	
Mean region size: 27.272727272727032
Incorrectly Classified Instances: 53.333333333333336
Correctly Classified Instances: 46.666666666666664
Weighted Precision: 0.4914293351485422
Weighted AreaUnderROC: 0.7066666666666667
Root mean squared error: 0.3017303563233592
Relative absolute error: 61.34117647058755
Root relative squared error: 104.95706817156007
Weighted TruePositiveRate: 0.4666666666666667
Weighted MatthewsCorrelation: 0.4175922170560113
Weighted FMeasure: 0.4559958701641181
Iteration time: 7.0
Weighted AreaUnderPRC: 0.2792548186350105
Mean absolute error: 0.10139037433155032
Coverage of cases: 60.2020202020202
Instances selection time: 7.0
Test time: 12.0
Accumulative iteration time: 32.0
Weighted Recall: 0.4666666666666667
Weighted FalsePositiveRate: 0.05333333333333334
Kappa statistic: 0.41333333333333333
Training time: 0.0
		
Iteration: 5
Labeled set size: 179
Unlabelled set size: 316
	
Mean region size: 18.18181818181829
Incorrectly Classified Instances: 51.515151515151516
Correctly Classified Instances: 48.484848484848484
Weighted Precision: 0.5123184904150714
Weighted AreaUnderROC: 0.7166666666666667
Root mean squared error: 0.2975202054114461
Relative absolute error: 59.17543859649103
Root relative squared error: 103.49256489234523
Weighted TruePositiveRate: 0.48484848484848486
Weighted MatthewsCorrelation: 0.4370590020600572
Weighted FMeasure: 0.46959855153757424
Iteration time: 8.0
Weighted AreaUnderPRC: 0.2949054278228209
Mean absolute error: 0.09781064230825026
Coverage of cases: 53.93939393939394
Instances selection time: 8.0
Test time: 13.0
Accumulative iteration time: 40.0
Weighted Recall: 0.48484848484848486
Weighted FalsePositiveRate: 0.05151515151515152
Kappa statistic: 0.43333333333333335
Training time: 0.0
		
Iteration: 6
Labeled set size: 199
Unlabelled set size: 296
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 47.474747474747474
Correctly Classified Instances: 52.525252525252526
Weighted Precision: 0.5793468191390256
Weighted AreaUnderROC: 0.7388888888888889
Root mean squared error: 0.2863967100536755
Relative absolute error: 54.72486772486763
Root relative squared error: 99.62325099633014
Weighted TruePositiveRate: 0.5252525252525253
Weighted MatthewsCorrelation: 0.4867104466614872
Weighted FMeasure: 0.5027511468613192
Iteration time: 7.0
Weighted AreaUnderPRC: 0.3342929994632935
Mean absolute error: 0.0904543268179636
Coverage of cases: 52.525252525252526
Instances selection time: 7.0
Test time: 20.0
Accumulative iteration time: 47.0
Weighted Recall: 0.5252525252525253
Weighted FalsePositiveRate: 0.047474747474747475
Kappa statistic: 0.4777777777777778
Training time: 0.0
		
Iteration: 7
Labeled set size: 219
Unlabelled set size: 276
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 44.84848484848485
Correctly Classified Instances: 55.15151515151515
Weighted Precision: 0.5936778874757705
Weighted AreaUnderROC: 0.7533333333333333
Root mean squared error: 0.2789836328418712
Relative absolute error: 51.7565217391301
Root relative squared error: 97.04460806573103
Weighted TruePositiveRate: 0.5515151515151515
Weighted MatthewsCorrelation: 0.5120444278625995
Weighted FMeasure: 0.5314728832391528
Iteration time: 8.0
Weighted AreaUnderPRC: 0.3578466892692394
Mean absolute error: 0.08554796981674452
Coverage of cases: 55.15151515151515
Instances selection time: 8.0
Test time: 15.0
Accumulative iteration time: 55.0
Weighted Recall: 0.5515151515151515
Weighted FalsePositiveRate: 0.04484848484848485
Kappa statistic: 0.5066666666666666
Training time: 0.0
		
Iteration: 8
Labeled set size: 239
Unlabelled set size: 256
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 40.60606060606061
Correctly Classified Instances: 59.39393939393939
Weighted Precision: 0.658050677512836
Weighted AreaUnderROC: 0.7766666666666666
Root mean squared error: 0.2659712696015185
Relative absolute error: 47.10133333333313
Root relative squared error: 92.51825045182504
Weighted TruePositiveRate: 0.593939393939394
Weighted MatthewsCorrelation: 0.5646852947963334
Weighted FMeasure: 0.5727876705208035
Iteration time: 8.0
Weighted AreaUnderPRC: 0.4078610834555844
Mean absolute error: 0.07785344352617096
Coverage of cases: 59.39393939393939
Instances selection time: 8.0
Test time: 15.0
Accumulative iteration time: 63.0
Weighted Recall: 0.593939393939394
Weighted FalsePositiveRate: 0.04060606060606061
Kappa statistic: 0.5533333333333333
Training time: 0.0
		
Iteration: 9
Labeled set size: 259
Unlabelled set size: 236
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 37.37373737373738
Correctly Classified Instances: 62.62626262626262
Weighted Precision: 0.7063228327922322
Weighted AreaUnderROC: 0.7944444444444443
Root mean squared error: 0.2555797311819401
Relative absolute error: 43.5102880658433
Root relative squared error: 88.90354817393349
Weighted TruePositiveRate: 0.6262626262626263
Weighted MatthewsCorrelation: 0.6034539978372713
Weighted FMeasure: 0.6039921368996419
Iteration time: 7.0
Weighted AreaUnderPRC: 0.4533727423587909
Mean absolute error: 0.07191783151379104
Coverage of cases: 62.62626262626262
Instances selection time: 7.0
Test time: 17.0
Accumulative iteration time: 70.0
Weighted Recall: 0.6262626262626263
Weighted FalsePositiveRate: 0.03737373737373737
Kappa statistic: 0.5888888888888889
Training time: 0.0
		
Iteration: 10
Labeled set size: 279
Unlabelled set size: 216
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 34.14141414141414
Correctly Classified Instances: 65.85858585858585
Weighted Precision: 0.7104496283028925
Weighted AreaUnderROC: 0.8122222222222221
Root mean squared error: 0.24462138953396775
Relative absolute error: 39.92413793103417
Root relative squared error: 85.09168308548718
Weighted TruePositiveRate: 0.6585858585858586
Weighted MatthewsCorrelation: 0.6347658527722638
Weighted FMeasure: 0.6440598924360031
Iteration time: 7.0
Weighted AreaUnderPRC: 0.49066933350187775
Mean absolute error: 0.06599031062980897
Coverage of cases: 65.85858585858585
Instances selection time: 7.0
Test time: 25.0
Accumulative iteration time: 77.0
Weighted Recall: 0.6585858585858586
Weighted FalsePositiveRate: 0.034141414141414146
Kappa statistic: 0.6244444444444445
Training time: 0.0
		
Iteration: 11
Labeled set size: 299
Unlabelled set size: 196
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 33.333333333333336
Correctly Classified Instances: 66.66666666666667
Weighted Precision: 0.7143407142428505
Weighted AreaUnderROC: 0.8166666666666665
Root mean squared error: 0.2419908701672128
Relative absolute error: 38.91397849462342
Root relative squared error: 84.17665549639294
Weighted TruePositiveRate: 0.6666666666666666
Weighted MatthewsCorrelation: 0.6417603547628544
Weighted FMeasure: 0.6506837110689655
Iteration time: 7.0
Weighted AreaUnderPRC: 0.5002384471009527
Mean absolute error: 0.06432062561094821
Coverage of cases: 66.66666666666667
Instances selection time: 6.0
Test time: 18.0
Accumulative iteration time: 84.0
Weighted Recall: 0.6666666666666666
Weighted FalsePositiveRate: 0.03333333333333334
Kappa statistic: 0.6333333333333333
Training time: 1.0
		
Iteration: 12
Labeled set size: 319
Unlabelled set size: 176
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 31.11111111111111
Correctly Classified Instances: 68.88888888888889
Weighted Precision: 0.7226173033130883
Weighted AreaUnderROC: 0.8288888888888888
Root mean squared error: 0.23403410444781145
Relative absolute error: 36.41481481481452
Root relative squared error: 81.40889022341074
Weighted TruePositiveRate: 0.6888888888888889
Weighted MatthewsCorrelation: 0.6606306181278366
Weighted FMeasure: 0.6700520633331414
Iteration time: 6.0
Weighted AreaUnderPRC: 0.5243057630659081
Mean absolute error: 0.06018977655341281
Coverage of cases: 68.88888888888889
Instances selection time: 6.0
Test time: 19.0
Accumulative iteration time: 90.0
Weighted Recall: 0.6888888888888889
Weighted FalsePositiveRate: 0.031111111111111114
Kappa statistic: 0.6577777777777778
Training time: 0.0
		
Iteration: 13
Labeled set size: 339
Unlabelled set size: 156
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 24.242424242424242
Correctly Classified Instances: 75.75757575757575
Weighted Precision: 0.7783153919733224
Weighted AreaUnderROC: 0.8666666666666667
Root mean squared error: 0.2068175192144166
Relative absolute error: 28.971428571428255
Root relative squared error: 71.94158628175113
Weighted TruePositiveRate: 0.7575757575757576
Weighted MatthewsCorrelation: 0.7329952187363551
Weighted FMeasure: 0.7413068902740446
Iteration time: 6.0
Weighted AreaUnderPRC: 0.606695911040105
Mean absolute error: 0.047886658795749486
Coverage of cases: 75.75757575757575
Instances selection time: 6.0
Test time: 20.0
Accumulative iteration time: 96.0
Weighted Recall: 0.7575757575757576
Weighted FalsePositiveRate: 0.024242424242424242
Kappa statistic: 0.7333333333333333
Training time: 0.0
		
Iteration: 14
Labeled set size: 359
Unlabelled set size: 136
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 23.434343434343436
Correctly Classified Instances: 76.56565656565657
Weighted Precision: 0.7915386362174719
Weighted AreaUnderROC: 0.8711111111111111
Root mean squared error: 0.20350484491928544
Relative absolute error: 27.984384384384207
Root relative squared error: 70.78927073067132
Weighted TruePositiveRate: 0.7656565656565657
Weighted MatthewsCorrelation: 0.7432095286613838
Weighted FMeasure: 0.7489273399161537
Iteration time: 6.0
Weighted AreaUnderPRC: 0.6182744348810458
Mean absolute error: 0.04625518080063535
Coverage of cases: 76.56565656565657
Instances selection time: 6.0
Test time: 21.0
Accumulative iteration time: 102.0
Weighted Recall: 0.7656565656565657
Weighted FalsePositiveRate: 0.023434343434343436
Kappa statistic: 0.7422222222222222
Training time: 0.0
		
Iteration: 15
Labeled set size: 379
Unlabelled set size: 116
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 21.01010101010101
Correctly Classified Instances: 78.98989898989899
Weighted Precision: 0.8322024444129493
Weighted AreaUnderROC: 0.8844444444444445
Root mean squared error: 0.19284317150700545
Relative absolute error: 25.279772079771856
Root relative squared error: 67.08060184898828
Weighted TruePositiveRate: 0.7898989898989899
Weighted MatthewsCorrelation: 0.774762552337636
Weighted FMeasure: 0.7750440869910455
Iteration time: 5.0
Weighted AreaUnderPRC: 0.6553171224453642
Mean absolute error: 0.0417847472392926
Coverage of cases: 78.98989898989899
Instances selection time: 5.0
Test time: 21.0
Accumulative iteration time: 107.0
Weighted Recall: 0.7898989898989899
Weighted FalsePositiveRate: 0.021010101010101014
Kappa statistic: 0.7688888888888888
Training time: 0.0
		
Iteration: 16
Labeled set size: 399
Unlabelled set size: 96
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 18.181818181818183
Correctly Classified Instances: 81.81818181818181
Weighted Precision: 0.8495475059514469
Weighted AreaUnderROC: 0.9
Root mean squared error: 0.1795283311328402
Relative absolute error: 22.146341463414444
Root relative squared error: 62.4490273999559
Weighted TruePositiveRate: 0.8181818181818182
Weighted MatthewsCorrelation: 0.8068670493987816
Weighted FMeasure: 0.8122615808942513
Iteration time: 4.0
Weighted AreaUnderPRC: 0.6996521283106346
Mean absolute error: 0.036605523080024106
Coverage of cases: 81.81818181818181
Instances selection time: 4.0
Test time: 22.0
Accumulative iteration time: 111.0
Weighted Recall: 0.8181818181818182
Weighted FalsePositiveRate: 0.01818181818181818
Kappa statistic: 0.8
Training time: 0.0
		
Iteration: 17
Labeled set size: 419
Unlabelled set size: 76
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 15.757575757575758
Correctly Classified Instances: 84.24242424242425
Weighted Precision: 0.8646429815344207
Weighted AreaUnderROC: 0.9133333333333332
Root mean squared error: 0.16724625714484118
Relative absolute error: 19.448062015503755
Root relative squared error: 58.17670129874963
Weighted TruePositiveRate: 0.8424242424242424
Weighted MatthewsCorrelation: 0.8308426325006706
Weighted FMeasure: 0.8366213734426744
Iteration time: 4.0
Weighted AreaUnderPRC: 0.7349981191142338
Mean absolute error: 0.03214555705041964
Coverage of cases: 84.24242424242425
Instances selection time: 4.0
Test time: 23.0
Accumulative iteration time: 115.0
Weighted Recall: 0.8424242424242424
Weighted FalsePositiveRate: 0.01575757575757576
Kappa statistic: 0.8266666666666667
Training time: 0.0
		
Iteration: 18
Labeled set size: 439
Unlabelled set size: 56
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 13.737373737373737
Correctly Classified Instances: 86.26262626262626
Weighted Precision: 0.876408679683932
Weighted AreaUnderROC: 0.9244444444444444
Root mean squared error: 0.1562558028715335
Relative absolute error: 17.18617283950601
Root relative squared error: 54.35366581615552
Weighted TruePositiveRate: 0.8626262626262626
Weighted MatthewsCorrelation: 0.8523405462734184
Weighted FMeasure: 0.8608094432354523
Iteration time: 3.0
Weighted AreaUnderPRC: 0.7645767236863895
Mean absolute error: 0.028406897255382018
Coverage of cases: 86.26262626262626
Instances selection time: 3.0
Test time: 25.0
Accumulative iteration time: 118.0
Weighted Recall: 0.8626262626262626
Weighted FalsePositiveRate: 0.013737373737373737
Kappa statistic: 0.8488888888888889
Training time: 0.0
		
Iteration: 19
Labeled set size: 459
Unlabelled set size: 36
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 12.525252525252526
Correctly Classified Instances: 87.47474747474747
Weighted Precision: 0.8872421572293222
Weighted AreaUnderROC: 0.9311111111111111
Root mean squared error: 0.14928317484913867
Relative absolute error: 15.795744680850952
Root relative squared error: 51.928233375088375
Weighted TruePositiveRate: 0.8747474747474747
Weighted MatthewsCorrelation: 0.8654564365973422
Weighted FMeasure: 0.8733462825724855
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7855183075702419
Mean absolute error: 0.026108668893968682
Coverage of cases: 87.47474747474747
Instances selection time: 2.0
Test time: 25.0
Accumulative iteration time: 120.0
Weighted Recall: 0.8747474747474747
Weighted FalsePositiveRate: 0.012525252525252526
Kappa statistic: 0.8622222222222222
Training time: 0.0
		
Iteration: 20
Labeled set size: 479
Unlabelled set size: 16
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 9.292929292929292
Correctly Classified Instances: 90.70707070707071
Weighted Precision: 0.9192111691332788
Weighted AreaUnderROC: 0.9488888888888889
Root mean squared error: 0.12868015058428167
Relative absolute error: 12.237641723355981
Root relative squared error: 44.76146020497532
Weighted TruePositiveRate: 0.907070707070707
Weighted MatthewsCorrelation: 0.9014231746892247
Weighted FMeasure: 0.9070441639786152
Iteration time: 2.0
Weighted AreaUnderPRC: 0.8400962977391895
Mean absolute error: 0.020227506980753818
Coverage of cases: 90.70707070707071
Instances selection time: 2.0
Test time: 26.0
Accumulative iteration time: 122.0
Weighted Recall: 0.907070707070707
Weighted FalsePositiveRate: 0.009292929292929292
Kappa statistic: 0.8977777777777777
Training time: 0.0
		
Time end:Wed Nov 01 14.50.28 EET 2017