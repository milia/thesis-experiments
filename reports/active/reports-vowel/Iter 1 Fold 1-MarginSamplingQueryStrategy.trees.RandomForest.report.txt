Wed Nov 01 14.59.08 EET 2017
Dataset: vowel
Test set size: 495
Initial Labelled set size: 99
Initial Unlabelled set size: 396
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Wed Nov 01 14.59.08 EET 2017
		
Iteration: 1
Labeled set size: 99
Unlabelled set size: 396
	
Mean region size: 41.52433425160715
Incorrectly Classified Instances: 60.2020202020202
Correctly Classified Instances: 39.7979797979798
Weighted Precision: 0.4717215434046595
Weighted AreaUnderROC: 0.782763187429854
Root mean squared error: 0.2646724915232174
Relative absolute error: 77.70109285003097
Root relative squared error: 92.06646979254697
Weighted TruePositiveRate: 0.397979797979798
Weighted MatthewsCorrelation: 0.3567391275068142
Weighted FMeasure: 0.391366362152496
Iteration time: 13.0
Weighted AreaUnderPRC: 0.4582299811061918
Mean absolute error: 0.12843155842980408
Coverage of cases: 77.97979797979798
Instances selection time: 2.0
Test time: 8.0
Accumulative iteration time: 13.0
Weighted Recall: 0.397979797979798
Weighted FalsePositiveRate: 0.06020202020202021
Kappa statistic: 0.3377777777777778
Training time: 11.0
		
Iteration: 2
Labeled set size: 119
Unlabelled set size: 376
	
Mean region size: 44.11386593204786
Incorrectly Classified Instances: 53.93939393939394
Correctly Classified Instances: 46.06060606060606
Weighted Precision: 0.4998975951183329
Weighted AreaUnderROC: 0.8278855218855219
Root mean squared error: 0.25502424098652343
Relative absolute error: 76.21082824533192
Root relative squared error: 88.71032060803854
Weighted TruePositiveRate: 0.46060606060606063
Weighted MatthewsCorrelation: 0.41263975242748174
Weighted FMeasure: 0.4455460686104047
Iteration time: 17.0
Weighted AreaUnderPRC: 0.4911291002095396
Mean absolute error: 0.12596831114930976
Coverage of cases: 81.41414141414141
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 30.0
Weighted Recall: 0.46060606060606063
Weighted FalsePositiveRate: 0.05393939393939395
Kappa statistic: 0.40666666666666673
Training time: 15.0
		
Iteration: 3
Labeled set size: 139
Unlabelled set size: 356
	
Mean region size: 41.1202938475666
Incorrectly Classified Instances: 52.121212121212125
Correctly Classified Instances: 47.878787878787875
Weighted Precision: 0.5185596020301424
Weighted AreaUnderROC: 0.8065993265993265
Root mean squared error: 0.25250557004052865
Relative absolute error: 74.13402710062536
Root relative squared error: 87.83419955279675
Weighted TruePositiveRate: 0.47878787878787876
Weighted MatthewsCorrelation: 0.43625190431504873
Weighted FMeasure: 0.47253995730348086
Iteration time: 19.0
Weighted AreaUnderPRC: 0.5067668860199985
Mean absolute error: 0.12253558198450552
Coverage of cases: 78.18181818181819
Instances selection time: 2.0
Test time: 6.0
Accumulative iteration time: 49.0
Weighted Recall: 0.47878787878787876
Weighted FalsePositiveRate: 0.052121212121212124
Kappa statistic: 0.42666666666666664
Training time: 17.0
		
Iteration: 4
Labeled set size: 159
Unlabelled set size: 336
	
Mean region size: 39.59595959595968
Incorrectly Classified Instances: 50.707070707070706
Correctly Classified Instances: 49.292929292929294
Weighted Precision: 0.5341303932457607
Weighted AreaUnderROC: 0.8376767676767678
Root mean squared error: 0.25285725442320983
Relative absolute error: 73.274034659551
Root relative squared error: 87.95653315614287
Weighted TruePositiveRate: 0.49292929292929294
Weighted MatthewsCorrelation: 0.4530781852511134
Weighted FMeasure: 0.48838825240382167
Iteration time: 21.0
Weighted AreaUnderPRC: 0.5129630866440499
Mean absolute error: 0.1211141068752917
Coverage of cases: 81.01010101010101
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 70.0
Weighted Recall: 0.49292929292929294
Weighted FalsePositiveRate: 0.0507070707070707
Kappa statistic: 0.4422222222222223
Training time: 20.0
		
Iteration: 5
Labeled set size: 179
Unlabelled set size: 316
	
Mean region size: 41.138659320477636
Incorrectly Classified Instances: 49.494949494949495
Correctly Classified Instances: 50.505050505050505
Weighted Precision: 0.5475746468778578
Weighted AreaUnderROC: 0.8286644219977553
Root mean squared error: 0.24961676179401107
Relative absolute error: 72.50642775493907
Root relative squared error: 86.82932603672475
Weighted TruePositiveRate: 0.5050505050505051
Weighted MatthewsCorrelation: 0.467615455923461
Weighted FMeasure: 0.502668174556445
Iteration time: 24.0
Weighted AreaUnderPRC: 0.5284383165072079
Mean absolute error: 0.11984533513213146
Coverage of cases: 82.02020202020202
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 94.0
Weighted Recall: 0.5050505050505051
Weighted FalsePositiveRate: 0.049494949494949494
Kappa statistic: 0.45555555555555555
Training time: 22.0
		
Iteration: 6
Labeled set size: 199
Unlabelled set size: 296
	
Mean region size: 40.77134986225908
Incorrectly Classified Instances: 46.464646464646464
Correctly Classified Instances: 53.535353535353536
Weighted Precision: 0.5816703292197958
Weighted AreaUnderROC: 0.8502042648709314
Root mean squared error: 0.2398998007155921
Relative absolute error: 69.64449901823322
Root relative squared error: 83.44927585299367
Weighted TruePositiveRate: 0.5353535353535354
Weighted MatthewsCorrelation: 0.503617238503598
Weighted FMeasure: 0.5371443961962736
Iteration time: 27.0
Weighted AreaUnderPRC: 0.5739391875620237
Mean absolute error: 0.11511487441030357
Coverage of cases: 83.83838383838383
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 121.0
Weighted Recall: 0.5353535353535354
Weighted FalsePositiveRate: 0.046464646464646465
Kappa statistic: 0.4888888888888889
Training time: 26.0
		
Iteration: 7
Labeled set size: 219
Unlabelled set size: 276
	
Mean region size: 39.48576675849415
Incorrectly Classified Instances: 41.61616161616162
Correctly Classified Instances: 58.38383838383838
Weighted Precision: 0.6100430856724776
Weighted AreaUnderROC: 0.8722244668911336
Root mean squared error: 0.23760795541597063
Relative absolute error: 68.3617682792826
Root relative squared error: 82.65205622192263
Weighted TruePositiveRate: 0.5838383838383838
Weighted MatthewsCorrelation: 0.5502149505141265
Weighted FMeasure: 0.5833248396956036
Iteration time: 30.0
Weighted AreaUnderPRC: 0.5885514802127607
Mean absolute error: 0.11299465831286452
Coverage of cases: 84.84848484848484
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 151.0
Weighted Recall: 0.5838383838383838
Weighted FalsePositiveRate: 0.04161616161616163
Kappa statistic: 0.5422222222222222
Training time: 29.0
		
Iteration: 8
Labeled set size: 239
Unlabelled set size: 256
	
Mean region size: 40.11019283746572
Incorrectly Classified Instances: 43.43434343434343
Correctly Classified Instances: 56.56565656565657
Weighted Precision: 0.6182361382508009
Weighted AreaUnderROC: 0.8656004489337822
Root mean squared error: 0.23505242200945034
Relative absolute error: 66.84398660903275
Root relative squared error: 81.7631125397848
Weighted TruePositiveRate: 0.5656565656565656
Weighted MatthewsCorrelation: 0.5392424596745466
Weighted FMeasure: 0.5677997421470842
Iteration time: 32.0
Weighted AreaUnderPRC: 0.5973982457449725
Mean absolute error: 0.11048592827939369
Coverage of cases: 86.06060606060606
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 183.0
Weighted Recall: 0.5656565656565656
Weighted FalsePositiveRate: 0.043434343434343436
Kappa statistic: 0.5222222222222221
Training time: 31.0
		
Iteration: 9
Labeled set size: 259
Unlabelled set size: 236
	
Mean region size: 38.36547291092758
Incorrectly Classified Instances: 35.35353535353536
Correctly Classified Instances: 64.64646464646465
Weighted Precision: 0.6734487663823034
Weighted AreaUnderROC: 0.9158136924803592
Root mean squared error: 0.22020241050041012
Relative absolute error: 62.39770466650257
Root relative squared error: 76.59752797847388
Weighted TruePositiveRate: 0.6464646464646465
Weighted MatthewsCorrelation: 0.6191069923592672
Weighted FMeasure: 0.6456530022997123
Iteration time: 38.0
Weighted AreaUnderPRC: 0.6892753862491884
Mean absolute error: 0.10313670192810408
Coverage of cases: 92.72727272727273
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 221.0
Weighted Recall: 0.6464646464646465
Weighted FalsePositiveRate: 0.03535353535353535
Kappa statistic: 0.6111111111111112
Training time: 36.0
		
Iteration: 10
Labeled set size: 279
Unlabelled set size: 216
	
Mean region size: 38.07162534435274
Incorrectly Classified Instances: 34.343434343434346
Correctly Classified Instances: 65.65656565656566
Weighted Precision: 0.6819468255349815
Weighted AreaUnderROC: 0.9159977553310887
Root mean squared error: 0.21726152448035424
Relative absolute error: 60.781644553385284
Root relative squared error: 75.57453918061822
Weighted TruePositiveRate: 0.6565656565656566
Weighted MatthewsCorrelation: 0.6312907805048242
Weighted FMeasure: 0.6597610484922581
Iteration time: 39.0
Weighted AreaUnderPRC: 0.6920395039796422
Mean absolute error: 0.10046552818741433
Coverage of cases: 92.72727272727273
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 260.0
Weighted Recall: 0.6565656565656566
Weighted FalsePositiveRate: 0.03434343434343434
Kappa statistic: 0.6222222222222222
Training time: 38.0
		
Iteration: 11
Labeled set size: 299
Unlabelled set size: 196
	
Mean region size: 36.82277318640969
Incorrectly Classified Instances: 33.73737373737374
Correctly Classified Instances: 66.26262626262626
Weighted Precision: 0.7123630980767818
Weighted AreaUnderROC: 0.9156498316498318
Root mean squared error: 0.21593244399236214
Relative absolute error: 59.64577151513355
Root relative squared error: 75.11221781168648
Weighted TruePositiveRate: 0.6626262626262627
Weighted MatthewsCorrelation: 0.6467206996173537
Weighted FMeasure: 0.6695951812822718
Iteration time: 43.0
Weighted AreaUnderPRC: 0.7000129789272543
Mean absolute error: 0.09858805209113047
Coverage of cases: 92.52525252525253
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 303.0
Weighted Recall: 0.6626262626262627
Weighted FalsePositiveRate: 0.033737373737373746
Kappa statistic: 0.6288888888888889
Training time: 42.0
		
Iteration: 12
Labeled set size: 319
Unlabelled set size: 176
	
Mean region size: 35.078053259871545
Incorrectly Classified Instances: 29.8989898989899
Correctly Classified Instances: 70.1010101010101
Weighted Precision: 0.7204679293358759
Weighted AreaUnderROC: 0.938781144781145
Root mean squared error: 0.203034506148119
Relative absolute error: 55.71973371959482
Root relative squared error: 70.62566313390656
Weighted TruePositiveRate: 0.701010101010101
Weighted MatthewsCorrelation: 0.6762173063503046
Weighted FMeasure: 0.698745549972583
Iteration time: 50.0
Weighted AreaUnderPRC: 0.7517942008071049
Mean absolute error: 0.09209873342081847
Coverage of cases: 94.94949494949495
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 353.0
Weighted Recall: 0.701010101010101
Weighted FalsePositiveRate: 0.029898989898989904
Kappa statistic: 0.6711111111111111
Training time: 48.0
		
Iteration: 13
Labeled set size: 339
Unlabelled set size: 156
	
Mean region size: 34.61891643709838
Incorrectly Classified Instances: 25.656565656565657
Correctly Classified Instances: 74.34343434343434
Weighted Precision: 0.7672730930164288
Weighted AreaUnderROC: 0.9536071829405162
Root mean squared error: 0.1951022630749012
Relative absolute error: 53.25090445701837
Root relative squared error: 67.86642807670573
Weighted TruePositiveRate: 0.7434343434343434
Weighted MatthewsCorrelation: 0.726411059337966
Weighted FMeasure: 0.746935488179419
Iteration time: 55.0
Weighted AreaUnderPRC: 0.7982658055186536
Mean absolute error: 0.0880180238958987
Coverage of cases: 97.57575757575758
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 408.0
Weighted Recall: 0.7434343434343434
Weighted FalsePositiveRate: 0.025656565656565655
Kappa statistic: 0.7177777777777777
Training time: 54.0
		
Iteration: 14
Labeled set size: 359
Unlabelled set size: 136
	
Mean region size: 34.765840220385805
Incorrectly Classified Instances: 18.78787878787879
Correctly Classified Instances: 81.21212121212122
Weighted Precision: 0.8209737876387267
Weighted AreaUnderROC: 0.9586891133557799
Root mean squared error: 0.1849909811191839
Relative absolute error: 50.764531912746165
Root relative squared error: 64.34921316184062
Weighted TruePositiveRate: 0.8121212121212121
Weighted MatthewsCorrelation: 0.7964375052780353
Weighted FMeasure: 0.813124964503185
Iteration time: 55.0
Weighted AreaUnderPRC: 0.842061261136916
Mean absolute error: 0.08390831721115122
Coverage of cases: 96.56565656565657
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 463.0
Weighted Recall: 0.8121212121212121
Weighted FalsePositiveRate: 0.01878787878787879
Kappa statistic: 0.7933333333333333
Training time: 53.0
		
Iteration: 15
Labeled set size: 379
Unlabelled set size: 116
	
Mean region size: 33.57208448117552
Incorrectly Classified Instances: 18.383838383838384
Correctly Classified Instances: 81.61616161616162
Weighted Precision: 0.8263987658388726
Weighted AreaUnderROC: 0.9748327721661058
Root mean squared error: 0.1804124693913618
Relative absolute error: 48.38336975498517
Root relative squared error: 62.75657537293244
Weighted TruePositiveRate: 0.8161616161616162
Weighted MatthewsCorrelation: 0.8005927095412941
Weighted FMeasure: 0.8152029410669738
Iteration time: 59.0
Weighted AreaUnderPRC: 0.8470910290257664
Mean absolute error: 0.07997251199171153
Coverage of cases: 98.98989898989899
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 522.0
Weighted Recall: 0.8161616161616162
Weighted FalsePositiveRate: 0.018383838383838384
Kappa statistic: 0.7977777777777778
Training time: 58.0
		
Iteration: 16
Labeled set size: 399
Unlabelled set size: 96
	
Mean region size: 33.20477502295691
Incorrectly Classified Instances: 20.2020202020202
Correctly Classified Instances: 79.79797979797979
Weighted Precision: 0.8104278991943299
Weighted AreaUnderROC: 0.9647609427609428
Root mean squared error: 0.18291493049764526
Relative absolute error: 48.59008372954507
Root relative squared error: 63.62705782663488
Weighted TruePositiveRate: 0.797979797979798
Weighted MatthewsCorrelation: 0.7817662291274102
Weighted FMeasure: 0.7982847766091199
Iteration time: 54.0
Weighted AreaUnderPRC: 0.8320336688037602
Mean absolute error: 0.08031418798271965
Coverage of cases: 97.37373737373737
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 576.0
Weighted Recall: 0.797979797979798
Weighted FalsePositiveRate: 0.020202020202020207
Kappa statistic: 0.7777777777777778
Training time: 53.0
		
Iteration: 17
Labeled set size: 419
Unlabelled set size: 76
	
Mean region size: 32.37832874196518
Incorrectly Classified Instances: 19.19191919191919
Correctly Classified Instances: 80.8080808080808
Weighted Precision: 0.8270548685944898
Weighted AreaUnderROC: 0.9692547699214367
Root mean squared error: 0.180014459135405
Relative absolute error: 47.91517579077446
Root relative squared error: 62.618127289430134
Weighted TruePositiveRate: 0.8080808080808081
Weighted MatthewsCorrelation: 0.7961125079697805
Weighted FMeasure: 0.8116339702502563
Iteration time: 56.0
Weighted AreaUnderPRC: 0.8468226428205154
Mean absolute error: 0.0791986376707021
Coverage of cases: 97.77777777777777
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 632.0
Weighted Recall: 0.8080808080808081
Weighted FalsePositiveRate: 0.019191919191919187
Kappa statistic: 0.7888888888888889
Training time: 55.0
		
Iteration: 18
Labeled set size: 439
Unlabelled set size: 56
	
Mean region size: 33.388429752066166
Incorrectly Classified Instances: 17.97979797979798
Correctly Classified Instances: 82.02020202020202
Weighted Precision: 0.8312564635035251
Weighted AreaUnderROC: 0.9833602693602692
Root mean squared error: 0.17360466709058095
Relative absolute error: 46.585155073021625
Root relative squared error: 60.38847764856621
Weighted TruePositiveRate: 0.8202020202020202
Weighted MatthewsCorrelation: 0.8060245686527215
Weighted FMeasure: 0.8212251287910755
Iteration time: 66.0
Weighted AreaUnderPRC: 0.8809082432882956
Mean absolute error: 0.0770002563190445
Coverage of cases: 99.79797979797979
Instances selection time: 0.0
Test time: 6.0
Accumulative iteration time: 698.0
Weighted Recall: 0.8202020202020202
Weighted FalsePositiveRate: 0.017979797979797985
Kappa statistic: 0.8022222222222222
Training time: 66.0
		
Iteration: 19
Labeled set size: 459
Unlabelled set size: 36
	
Mean region size: 32.121212121212196
Incorrectly Classified Instances: 19.19191919191919
Correctly Classified Instances: 80.8080808080808
Weighted Precision: 0.818598923774747
Weighted AreaUnderROC: 0.9764489337822672
Root mean squared error: 0.1732501186977357
Relative absolute error: 45.99288385990487
Root relative squared error: 60.26514779773047
Weighted TruePositiveRate: 0.8080808080808081
Weighted MatthewsCorrelation: 0.792360790552594
Weighted FMeasure: 0.8087528084966238
Iteration time: 70.0
Weighted AreaUnderPRC: 0.8801680190853735
Mean absolute error: 0.07602129563620688
Coverage of cases: 98.38383838383838
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 768.0
Weighted Recall: 0.8080808080808081
Weighted FalsePositiveRate: 0.019191919191919194
Kappa statistic: 0.7888888888888889
Training time: 69.0
		
Iteration: 20
Labeled set size: 479
Unlabelled set size: 16
	
Mean region size: 31.75390266299368
Incorrectly Classified Instances: 14.94949494949495
Correctly Classified Instances: 85.05050505050505
Weighted Precision: 0.8609779747177729
Weighted AreaUnderROC: 0.9787946127946129
Root mean squared error: 0.16723739304163723
Relative absolute error: 43.92715617896151
Root relative squared error: 58.17361791564031
Weighted TruePositiveRate: 0.8505050505050505
Weighted MatthewsCorrelation: 0.8387584757531994
Weighted FMeasure: 0.8507579329524798
Iteration time: 71.0
Weighted AreaUnderPRC: 0.8860892581369337
Mean absolute error: 0.07260686971729222
Coverage of cases: 98.78787878787878
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 839.0
Weighted Recall: 0.8505050505050505
Weighted FalsePositiveRate: 0.014949494949494952
Kappa statistic: 0.8355555555555556
Training time: 70.0
		
Time end:Wed Nov 01 14.59.10 EET 2017