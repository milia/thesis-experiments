Wed Nov 01 14.58.56 EET 2017
Dataset: vowel
Test set size: 495
Initial Labelled set size: 99
Initial Unlabelled set size: 396
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Wed Nov 01 14.58.56 EET 2017
		
Iteration: 1
Labeled set size: 99
Unlabelled set size: 396
	
Mean region size: 17.704315886134193
Incorrectly Classified Instances: 51.515151515151516
Correctly Classified Instances: 48.484848484848484
Weighted Precision: 0.5497242702871807
Weighted AreaUnderROC: 0.7839483726150394
Root mean squared error: 0.2809097036576782
Relative absolute error: 59.776020892687285
Root relative squared error: 97.71459284413119
Weighted TruePositiveRate: 0.48484848484848486
Weighted MatthewsCorrelation: 0.44768508968948073
Weighted FMeasure: 0.4740190357243295
Iteration time: 8.0
Weighted AreaUnderPRC: 0.3667904785107601
Mean absolute error: 0.09880334031849201
Coverage of cases: 66.06060606060606
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 8.0
Weighted Recall: 0.48484848484848486
Weighted FalsePositiveRate: 0.05151515151515152
Kappa statistic: 0.43333333333333335
Training time: 7.0
		
Iteration: 2
Labeled set size: 119
Unlabelled set size: 376
	
Mean region size: 13.682277318641026
Incorrectly Classified Instances: 54.14141414141414
Correctly Classified Instances: 45.85858585858586
Weighted Precision: 0.5448306767063386
Weighted AreaUnderROC: 0.7358361391694724
Root mean squared error: 0.3003617772366734
Relative absolute error: 61.5185185185181
Root relative squared error: 104.48100719364005
Weighted TruePositiveRate: 0.4585858585858586
Weighted MatthewsCorrelation: 0.4289860664254514
Weighted FMeasure: 0.4566071279729189
Iteration time: 8.0
Weighted AreaUnderPRC: 0.32621360997400506
Mean absolute error: 0.10168350168350164
Coverage of cases: 54.94949494949495
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 16.0
Weighted Recall: 0.4585858585858586
Weighted FalsePositiveRate: 0.05414141414141414
Kappa statistic: 0.4044444444444445
Training time: 7.0
		
Iteration: 3
Labeled set size: 139
Unlabelled set size: 356
	
Mean region size: 13.865932047750281
Incorrectly Classified Instances: 53.333333333333336
Correctly Classified Instances: 46.666666666666664
Weighted Precision: 0.521256906046446
Weighted AreaUnderROC: 0.7388529741863076
Root mean squared error: 0.2890494540438202
Relative absolute error: 59.61600955794463
Root relative squared error: 100.54600943272999
Weighted TruePositiveRate: 0.4666666666666667
Weighted MatthewsCorrelation: 0.4325334167509539
Weighted FMeasure: 0.47180981303751934
Iteration time: 10.0
Weighted AreaUnderPRC: 0.34324637191764934
Mean absolute error: 0.09853885877346283
Coverage of cases: 55.75757575757576
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 26.0
Weighted Recall: 0.4666666666666667
Weighted FalsePositiveRate: 0.05333333333333333
Kappa statistic: 0.41333333333333333
Training time: 9.0
		
Iteration: 4
Labeled set size: 159
Unlabelled set size: 336
	
Mean region size: 14.600550964187384
Incorrectly Classified Instances: 50.90909090909091
Correctly Classified Instances: 49.09090909090909
Weighted Precision: 0.5009026153740141
Weighted AreaUnderROC: 0.7488888888888889
Root mean squared error: 0.2866143806806649
Relative absolute error: 58.58498025690969
Root relative squared error: 99.69896784204052
Weighted TruePositiveRate: 0.4909090909090909
Weighted MatthewsCorrelation: 0.43948494210868616
Weighted FMeasure: 0.4803732661402077
Iteration time: 11.0
Weighted AreaUnderPRC: 0.3509409109490479
Mean absolute error: 0.09683467811059515
Coverage of cases: 58.18181818181818
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 37.0
Weighted Recall: 0.4909090909090909
Weighted FalsePositiveRate: 0.05090909090909091
Kappa statistic: 0.44000000000000006
Training time: 10.0
		
Iteration: 5
Labeled set size: 179
Unlabelled set size: 316
	
Mean region size: 13.516988062442636
Incorrectly Classified Instances: 45.85858585858586
Correctly Classified Instances: 54.14141414141414
Weighted Precision: 0.5470308268932558
Weighted AreaUnderROC: 0.7705364758698092
Root mean squared error: 0.28098333437485007
Relative absolute error: 53.293242365171885
Root relative squared error: 97.74020532905281
Weighted TruePositiveRate: 0.5414141414141415
Weighted MatthewsCorrelation: 0.4943733651684584
Weighted FMeasure: 0.5332417393285024
Iteration time: 13.0
Weighted AreaUnderPRC: 0.36731158103839945
Mean absolute error: 0.08808800390937557
Coverage of cases: 60.60606060606061
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 50.0
Weighted Recall: 0.5414141414141415
Weighted FalsePositiveRate: 0.04585858585858586
Kappa statistic: 0.4955555555555556
Training time: 12.0
		
Iteration: 6
Labeled set size: 199
Unlabelled set size: 296
	
Mean region size: 13.663911845730086
Incorrectly Classified Instances: 44.84848484848485
Correctly Classified Instances: 55.15151515151515
Weighted Precision: 0.5867142214589463
Weighted AreaUnderROC: 0.781405162738496
Root mean squared error: 0.2716370017586928
Relative absolute error: 52.17946343779647
Root relative squared error: 94.48907845702932
Weighted TruePositiveRate: 0.5515151515151515
Weighted MatthewsCorrelation: 0.5148798313474539
Weighted FMeasure: 0.5454561723965013
Iteration time: 16.0
Weighted AreaUnderPRC: 0.39723369042911766
Mean absolute error: 0.08624704700462282
Coverage of cases: 63.23232323232323
Instances selection time: 2.0
Test time: 3.0
Accumulative iteration time: 66.0
Weighted Recall: 0.5515151515151515
Weighted FalsePositiveRate: 0.044848484848484846
Kappa statistic: 0.5066666666666666
Training time: 14.0
		
Iteration: 7
Labeled set size: 219
Unlabelled set size: 276
	
Mean region size: 14.563820018365528
Incorrectly Classified Instances: 43.23232323232323
Correctly Classified Instances: 56.76767676767677
Weighted Precision: 0.5688105499067639
Weighted AreaUnderROC: 0.7907968574635241
Root mean squared error: 0.2623950485346192
Relative absolute error: 50.22499568332871
Root relative squared error: 91.27426001318034
Weighted TruePositiveRate: 0.5676767676767677
Weighted MatthewsCorrelation: 0.5233998138629146
Weighted FMeasure: 0.5637233401662024
Iteration time: 17.0
Weighted AreaUnderPRC: 0.42479748223494673
Mean absolute error: 0.08301652179062649
Coverage of cases: 65.65656565656566
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 83.0
Weighted Recall: 0.5676767676767677
Weighted FalsePositiveRate: 0.04323232323232325
Kappa statistic: 0.5244444444444444
Training time: 16.0
		
Iteration: 8
Labeled set size: 239
Unlabelled set size: 256
	
Mean region size: 11.36822773186412
Incorrectly Classified Instances: 37.57575757575758
Correctly Classified Instances: 62.42424242424242
Weighted Precision: 0.6382363112601047
Weighted AreaUnderROC: 0.8055667789001123
Root mean squared error: 0.2544299757095004
Relative absolute error: 43.30648148148133
Root relative squared error: 88.50360510896672
Weighted TruePositiveRate: 0.6242424242424243
Weighted MatthewsCorrelation: 0.5882844211253303
Weighted FMeasure: 0.6173534229422758
Iteration time: 19.0
Weighted AreaUnderPRC: 0.47030790531511835
Mean absolute error: 0.07158096112641588
Coverage of cases: 65.85858585858585
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 102.0
Weighted Recall: 0.6242424242424243
Weighted FalsePositiveRate: 0.03757575757575758
Kappa statistic: 0.5866666666666667
Training time: 18.0
		
Iteration: 9
Labeled set size: 259
Unlabelled set size: 236
	
Mean region size: 13.590449954086333
Incorrectly Classified Instances: 39.5959595959596
Correctly Classified Instances: 60.4040404040404
Weighted Precision: 0.6134637491367526
Weighted AreaUnderROC: 0.8175510662177329
Root mean squared error: 0.25339679700585915
Relative absolute error: 46.65138356747525
Root relative squared error: 88.14421333628322
Weighted TruePositiveRate: 0.604040404040404
Weighted MatthewsCorrelation: 0.5647990882317255
Weighted FMeasure: 0.5970555451155632
Iteration time: 23.0
Weighted AreaUnderPRC: 0.4621018609649596
Mean absolute error: 0.07710972490491827
Coverage of cases: 68.08080808080808
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 125.0
Weighted Recall: 0.604040404040404
Weighted FalsePositiveRate: 0.0395959595959596
Kappa statistic: 0.5644444444444444
Training time: 22.0
		
Iteration: 10
Labeled set size: 279
Unlabelled set size: 216
	
Mean region size: 13.35169880624429
Incorrectly Classified Instances: 36.76767676767677
Correctly Classified Instances: 63.23232323232323
Weighted Precision: 0.6478360202759532
Weighted AreaUnderROC: 0.8307003367003368
Root mean squared error: 0.24409121889296975
Relative absolute error: 43.4220043572982
Root relative squared error: 84.90726294033561
Weighted TruePositiveRate: 0.6323232323232323
Weighted MatthewsCorrelation: 0.5987091788752488
Weighted FMeasure: 0.6282042784856258
Iteration time: 24.0
Weighted AreaUnderPRC: 0.4991192586880226
Mean absolute error: 0.07177190802859253
Coverage of cases: 70.3030303030303
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 149.0
Weighted Recall: 0.6323232323232323
Weighted FalsePositiveRate: 0.03676767676767677
Kappa statistic: 0.5955555555555555
Training time: 23.0
		
Iteration: 11
Labeled set size: 299
Unlabelled set size: 196
	
Mean region size: 13.461891643709889
Incorrectly Classified Instances: 33.93939393939394
Correctly Classified Instances: 66.06060606060606
Weighted Precision: 0.6669356342197669
Weighted AreaUnderROC: 0.8262177328843996
Root mean squared error: 0.24182396547332524
Relative absolute error: 40.152182539682414
Root relative squared error: 84.11859760805858
Weighted TruePositiveRate: 0.6606060606060606
Weighted MatthewsCorrelation: 0.6263519892763703
Weighted FMeasure: 0.6545805804699891
Iteration time: 27.0
Weighted AreaUnderPRC: 0.506721170495888
Mean absolute error: 0.06636724386724409
Coverage of cases: 69.8989898989899
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 176.0
Weighted Recall: 0.6606060606060606
Weighted FalsePositiveRate: 0.033939393939393936
Kappa statistic: 0.6266666666666667
Training time: 26.0
		
Iteration: 12
Labeled set size: 319
Unlabelled set size: 176
	
Mean region size: 16.951331496786132
Incorrectly Classified Instances: 33.333333333333336
Correctly Classified Instances: 66.66666666666667
Weighted Precision: 0.6976960731878589
Weighted AreaUnderROC: 0.850121212121212
Root mean squared error: 0.23164728442058588
Relative absolute error: 39.77951865857203
Root relative squared error: 80.57863358180758
Weighted TruePositiveRate: 0.6666666666666666
Weighted MatthewsCorrelation: 0.644601271036547
Weighted FMeasure: 0.6708443900234923
Iteration time: 27.0
Weighted AreaUnderPRC: 0.5550454524373057
Mean absolute error: 0.06575127051003683
Coverage of cases: 74.74747474747475
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 203.0
Weighted Recall: 0.6666666666666666
Weighted FalsePositiveRate: 0.03333333333333333
Kappa statistic: 0.6333333333333333
Training time: 26.0
		
Iteration: 13
Labeled set size: 339
Unlabelled set size: 156
	
Mean region size: 13.33333333333338
Incorrectly Classified Instances: 32.323232323232325
Correctly Classified Instances: 67.67676767676768
Weighted Precision: 0.6938279741489414
Weighted AreaUnderROC: 0.8525095398428731
Root mean squared error: 0.232413885130777
Relative absolute error: 37.60137728580699
Root relative squared error: 80.84529605481929
Weighted TruePositiveRate: 0.6767676767676768
Weighted MatthewsCorrelation: 0.6502059146132858
Weighted FMeasure: 0.6779823151034352
Iteration time: 30.0
Weighted AreaUnderPRC: 0.5625664296762521
Mean absolute error: 0.06215103683604501
Coverage of cases: 74.14141414141415
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 233.0
Weighted Recall: 0.6767676767676768
Weighted FalsePositiveRate: 0.03232323232323232
Kappa statistic: 0.6444444444444445
Training time: 29.0
		
Iteration: 14
Labeled set size: 359
Unlabelled set size: 136
	
Mean region size: 13.076216712580388
Incorrectly Classified Instances: 33.93939393939394
Correctly Classified Instances: 66.06060606060606
Weighted Precision: 0.6720448959837436
Weighted AreaUnderROC: 0.8539753086419756
Root mean squared error: 0.2350435484519551
Relative absolute error: 39.8274174493471
Root relative squared error: 81.76002586799513
Weighted TruePositiveRate: 0.6606060606060606
Weighted MatthewsCorrelation: 0.6307297405277792
Weighted FMeasure: 0.6620272632078508
Iteration time: 33.0
Weighted AreaUnderPRC: 0.5488544678448962
Mean absolute error: 0.06583044206503695
Coverage of cases: 74.74747474747475
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 266.0
Weighted Recall: 0.6606060606060606
Weighted FalsePositiveRate: 0.03393939393939394
Kappa statistic: 0.6266666666666667
Training time: 33.0
		
Iteration: 15
Labeled set size: 379
Unlabelled set size: 116
	
Mean region size: 12.63544536271811
Incorrectly Classified Instances: 32.323232323232325
Correctly Classified Instances: 67.67676767676768
Weighted Precision: 0.6854272375641112
Weighted AreaUnderROC: 0.8610909090909091
Root mean squared error: 0.22775330472772487
Relative absolute error: 37.67571702176952
Root relative squared error: 79.22411063270037
Weighted TruePositiveRate: 0.6767676767676768
Weighted MatthewsCorrelation: 0.6467269711517332
Weighted FMeasure: 0.6756482899514933
Iteration time: 34.0
Weighted AreaUnderPRC: 0.5792681507429386
Mean absolute error: 0.06227391243267729
Coverage of cases: 76.36363636363636
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 300.0
Weighted Recall: 0.6767676767676768
Weighted FalsePositiveRate: 0.032323232323232316
Kappa statistic: 0.6444444444444445
Training time: 33.0
		
Iteration: 16
Labeled set size: 399
Unlabelled set size: 96
	
Mean region size: 13.388429752066148
Incorrectly Classified Instances: 36.56565656565657
Correctly Classified Instances: 63.43434343434343
Weighted Precision: 0.6506093302237579
Weighted AreaUnderROC: 0.8552749719416386
Root mean squared error: 0.23881987714962497
Relative absolute error: 41.98714711214694
Root relative squared error: 83.07362385458546
Weighted TruePositiveRate: 0.6343434343434343
Weighted MatthewsCorrelation: 0.6027079315805054
Weighted FMeasure: 0.6338300106896978
Iteration time: 35.0
Weighted AreaUnderPRC: 0.555954348309125
Mean absolute error: 0.0694002431605739
Coverage of cases: 75.75757575757575
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 335.0
Weighted Recall: 0.6343434343434343
Weighted FalsePositiveRate: 0.03656565656565656
Kappa statistic: 0.5977777777777777
Training time: 35.0
		
Iteration: 17
Labeled set size: 419
Unlabelled set size: 76
	
Mean region size: 14.086317722681404
Incorrectly Classified Instances: 33.333333333333336
Correctly Classified Instances: 66.66666666666667
Weighted Precision: 0.665692221803804
Weighted AreaUnderROC: 0.8600718294051626
Root mean squared error: 0.23298972889484118
Relative absolute error: 39.31718246329845
Root relative squared error: 81.04560362061251
Weighted TruePositiveRate: 0.6666666666666666
Weighted MatthewsCorrelation: 0.630484276917129
Weighted FMeasure: 0.6595878191712697
Iteration time: 38.0
Weighted AreaUnderPRC: 0.5500178316549564
Mean absolute error: 0.06498707845173339
Coverage of cases: 76.36363636363636
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 373.0
Weighted Recall: 0.6666666666666666
Weighted FalsePositiveRate: 0.03333333333333333
Kappa statistic: 0.6333333333333333
Training time: 37.0
		
Iteration: 18
Labeled set size: 439
Unlabelled set size: 56
	
Mean region size: 12.231404958677723
Incorrectly Classified Instances: 31.91919191919192
Correctly Classified Instances: 68.08080808080808
Weighted Precision: 0.6876606731890302
Weighted AreaUnderROC: 0.870395061728395
Root mean squared error: 0.2262661841132518
Relative absolute error: 36.91752827732851
Root relative squared error: 78.70681492001673
Weighted TruePositiveRate: 0.6808080808080809
Weighted MatthewsCorrelation: 0.6508724501127726
Weighted FMeasure: 0.6803637858743491
Iteration time: 44.0
Weighted AreaUnderPRC: 0.5808478498740275
Mean absolute error: 0.06102070789641115
Coverage of cases: 76.36363636363636
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 417.0
Weighted Recall: 0.6808080808080809
Weighted FalsePositiveRate: 0.031919191919191924
Kappa statistic: 0.648888888888889
Training time: 43.0
		
Iteration: 19
Labeled set size: 459
Unlabelled set size: 36
	
Mean region size: 13.00275482093667
Incorrectly Classified Instances: 33.333333333333336
Correctly Classified Instances: 66.66666666666667
Weighted Precision: 0.6740616550816925
Weighted AreaUnderROC: 0.8825454545454545
Root mean squared error: 0.22377295383133808
Relative absolute error: 37.616311346111516
Root relative squared error: 77.83954341358012
Weighted TruePositiveRate: 0.6666666666666666
Weighted MatthewsCorrelation: 0.6358104858845091
Weighted FMeasure: 0.6670758200201586
Iteration time: 44.0
Weighted AreaUnderPRC: 0.6003911104622467
Mean absolute error: 0.06217572123324257
Coverage of cases: 78.98989898989899
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 461.0
Weighted Recall: 0.6666666666666666
Weighted FalsePositiveRate: 0.03333333333333333
Kappa statistic: 0.6333333333333333
Training time: 44.0
		
Iteration: 20
Labeled set size: 479
Unlabelled set size: 16
	
Mean region size: 12.121212121212164
Incorrectly Classified Instances: 32.72727272727273
Correctly Classified Instances: 67.27272727272727
Weighted Precision: 0.6779039539593388
Weighted AreaUnderROC: 0.8826913580246916
Root mean squared error: 0.22426822820460707
Relative absolute error: 37.348462401795594
Root relative squared error: 78.01182487306677
Weighted TruePositiveRate: 0.6727272727272727
Weighted MatthewsCorrelation: 0.6407904022001756
Weighted FMeasure: 0.6704486713034521
Iteration time: 45.0
Weighted AreaUnderPRC: 0.6075811147001684
Mean absolute error: 0.061732995705447664
Coverage of cases: 77.77777777777777
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 506.0
Weighted Recall: 0.6727272727272727
Weighted FalsePositiveRate: 0.03272727272727272
Kappa statistic: 0.64
Training time: 45.0
		
Time end:Wed Nov 01 14.58.57 EET 2017