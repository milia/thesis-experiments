Wed Nov 01 14.50.44 EET 2017
Dataset: vowel
Test set size: 495
Initial Labelled set size: 99
Initial Unlabelled set size: 396
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Nov 01 14.50.44 EET 2017
		
Iteration: 1
Labeled set size: 99
Unlabelled set size: 396
	
Mean region size: 25.69329660238762
Incorrectly Classified Instances: 62.22222222222222
Correctly Classified Instances: 37.77777777777778
Weighted Precision: 0.3594693717449091
Weighted AreaUnderROC: 0.7669988776655443
Root mean squared error: 0.28302694975920656
Relative absolute error: 70.41399176954656
Root relative squared error: 98.45107804940477
Weighted TruePositiveRate: 0.37777777777777777
Weighted MatthewsCorrelation: 0.30874320289311463
Weighted FMeasure: 0.3437289943075507
Iteration time: 13.0
Weighted AreaUnderPRC: 0.30391152279878497
Mean absolute error: 0.1163867632554496
Coverage of cases: 72.12121212121212
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 13.0
Weighted Recall: 0.37777777777777777
Weighted FalsePositiveRate: 0.06222222222222223
Kappa statistic: 0.31555555555555553
Training time: 12.0
		
Iteration: 2
Labeled set size: 119
Unlabelled set size: 376
	
Mean region size: 20.550964187327917
Incorrectly Classified Instances: 51.313131313131315
Correctly Classified Instances: 48.686868686868685
Weighted Precision: 0.4927192358608288
Weighted AreaUnderROC: 0.8064332210998877
Root mean squared error: 0.2681142329946845
Relative absolute error: 61.757575757575445
Root relative squared error: 93.26368143094946
Weighted TruePositiveRate: 0.4868686868686869
Weighted MatthewsCorrelation: 0.4293331830615941
Weighted FMeasure: 0.45175073003118776
Iteration time: 8.0
Weighted AreaUnderPRC: 0.3781451164939204
Mean absolute error: 0.10207863761582783
Coverage of cases: 70.5050505050505
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 21.0
Weighted Recall: 0.4868686868686869
Weighted FalsePositiveRate: 0.05131313131313131
Kappa statistic: 0.4355555555555556
Training time: 7.0
		
Iteration: 3
Labeled set size: 139
Unlabelled set size: 356
	
Mean region size: 15.096418732782475
Incorrectly Classified Instances: 49.09090909090909
Correctly Classified Instances: 50.90909090909091
Weighted Precision: 0.5220962515446831
Weighted AreaUnderROC: 0.789337822671156
Root mean squared error: 0.27676260024455496
Relative absolute error: 56.88191438191417
Root relative squared error: 96.27202067158115
Weighted TruePositiveRate: 0.509090909090909
Weighted MatthewsCorrelation: 0.45664235257274655
Weighted FMeasure: 0.48912131386978897
Iteration time: 10.0
Weighted AreaUnderPRC: 0.38096670028912266
Mean absolute error: 0.09401969319324717
Coverage of cases: 65.85858585858585
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 31.0
Weighted Recall: 0.509090909090909
Weighted FalsePositiveRate: 0.049090909090909095
Kappa statistic: 0.4599999999999999
Training time: 9.0
		
Iteration: 4
Labeled set size: 159
Unlabelled set size: 336
	
Mean region size: 15.04132231404967
Incorrectly Classified Instances: 46.06060606060606
Correctly Classified Instances: 53.93939393939394
Weighted Precision: 0.556438138936756
Weighted AreaUnderROC: 0.7829584736251403
Root mean squared error: 0.27580384247807704
Relative absolute error: 52.69012345678987
Root relative squared error: 95.93851626227207
Weighted TruePositiveRate: 0.5393939393939394
Weighted MatthewsCorrelation: 0.4951277736499949
Weighted FMeasure: 0.5297278397887044
Iteration time: 12.0
Weighted AreaUnderPRC: 0.4037125482311459
Mean absolute error: 0.08709111315171936
Coverage of cases: 62.42424242424242
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 43.0
Weighted Recall: 0.5393939393939394
Weighted FalsePositiveRate: 0.046060606060606066
Kappa statistic: 0.49333333333333335
Training time: 11.0
		
Iteration: 5
Labeled set size: 179
Unlabelled set size: 316
	
Mean region size: 18.494031221304017
Incorrectly Classified Instances: 49.898989898989896
Correctly Classified Instances: 50.101010101010104
Weighted Precision: 0.5355140369717963
Weighted AreaUnderROC: 0.812013468013468
Root mean squared error: 0.2667285158062424
Relative absolute error: 58.21050884291326
Root relative squared error: 92.78165895503406
Weighted TruePositiveRate: 0.501010101010101
Weighted MatthewsCorrelation: 0.4557175092493803
Weighted FMeasure: 0.48543355006030675
Iteration time: 14.0
Weighted AreaUnderPRC: 0.39889311850220605
Mean absolute error: 0.09621571709572502
Coverage of cases: 72.52525252525253
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 57.0
Weighted Recall: 0.501010101010101
Weighted FalsePositiveRate: 0.049898989898989894
Kappa statistic: 0.4511111111111111
Training time: 13.0
		
Iteration: 6
Labeled set size: 199
Unlabelled set size: 296
	
Mean region size: 14.80257116620757
Incorrectly Classified Instances: 54.94949494949495
Correctly Classified Instances: 45.05050505050505
Weighted Precision: 0.47931976256611036
Weighted AreaUnderROC: 0.7713243546576881
Root mean squared error: 0.2835286112674739
Relative absolute error: 60.48822759324863
Root relative squared error: 98.62558127726639
Weighted TruePositiveRate: 0.4505050505050505
Weighted MatthewsCorrelation: 0.39782260258243346
Weighted FMeasure: 0.4313004304044274
Iteration time: 14.0
Weighted AreaUnderPRC: 0.34922516934599174
Mean absolute error: 0.09998054147644465
Coverage of cases: 62.02020202020202
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 71.0
Weighted Recall: 0.4505050505050505
Weighted FalsePositiveRate: 0.05494949494949495
Kappa statistic: 0.39555555555555555
Training time: 13.0
		
Iteration: 7
Labeled set size: 219
Unlabelled set size: 276
	
Mean region size: 14.894398530762222
Incorrectly Classified Instances: 53.73737373737374
Correctly Classified Instances: 46.26262626262626
Weighted Precision: 0.46539977998411197
Weighted AreaUnderROC: 0.7726419753086422
Root mean squared error: 0.28307558401512545
Relative absolute error: 59.93844797178093
Root relative squared error: 98.467995501716
Weighted TruePositiveRate: 0.4626262626262626
Weighted MatthewsCorrelation: 0.4011685441303539
Weighted FMeasure: 0.44116838591969526
Iteration time: 16.0
Weighted AreaUnderPRC: 0.36190227574399253
Mean absolute error: 0.0990718148293906
Coverage of cases: 62.82828282828283
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 87.0
Weighted Recall: 0.4626262626262626
Weighted FalsePositiveRate: 0.05373737373737374
Kappa statistic: 0.40888888888888886
Training time: 15.0
		
Iteration: 8
Labeled set size: 239
Unlabelled set size: 256
	
Mean region size: 13.755739210284677
Incorrectly Classified Instances: 45.656565656565654
Correctly Classified Instances: 54.343434343434346
Weighted Precision: 0.5448827935843852
Weighted AreaUnderROC: 0.7774837261503929
Root mean squared error: 0.27732012767719333
Relative absolute error: 53.351675485008535
Root relative squared error: 96.46595689154911
Weighted TruePositiveRate: 0.5434343434343434
Weighted MatthewsCorrelation: 0.4930813732493836
Weighted FMeasure: 0.5294133086793242
Iteration time: 19.0
Weighted AreaUnderPRC: 0.38318843444675904
Mean absolute error: 0.08818458757852707
Coverage of cases: 62.62626262626262
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 106.0
Weighted Recall: 0.5434343434343434
Weighted FalsePositiveRate: 0.045656565656565666
Kappa statistic: 0.4977777777777777
Training time: 18.0
		
Iteration: 9
Labeled set size: 259
Unlabelled set size: 236
	
Mean region size: 14.141414141414181
Incorrectly Classified Instances: 42.22222222222222
Correctly Classified Instances: 57.77777777777778
Weighted Precision: 0.5897937479152126
Weighted AreaUnderROC: 0.8222469135802469
Root mean squared error: 0.26303565377785937
Relative absolute error: 48.657162590495616
Root relative squared error: 91.4970948946457
Weighted TruePositiveRate: 0.5777777777777777
Weighted MatthewsCorrelation: 0.5362473938469102
Weighted FMeasure: 0.5696815008745353
Iteration time: 22.0
Weighted AreaUnderPRC: 0.45474936173137387
Mean absolute error: 0.08042506213305112
Coverage of cases: 70.1010101010101
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 128.0
Weighted Recall: 0.5777777777777777
Weighted FalsePositiveRate: 0.042222222222222223
Kappa statistic: 0.5355555555555555
Training time: 21.0
		
Iteration: 10
Labeled set size: 279
Unlabelled set size: 216
	
Mean region size: 17.685950413223164
Incorrectly Classified Instances: 44.04040404040404
Correctly Classified Instances: 55.95959595959596
Weighted Precision: 0.6037661632120987
Weighted AreaUnderROC: 0.8481997755331089
Root mean squared error: 0.2515057623340846
Relative absolute error: 52.37649843714547
Root relative squared error: 87.48641589959601
Weighted TruePositiveRate: 0.5595959595959596
Weighted MatthewsCorrelation: 0.5204566488568805
Weighted FMeasure: 0.5384137050109258
Iteration time: 21.0
Weighted AreaUnderPRC: 0.4582844750024328
Mean absolute error: 0.08657272468949721
Coverage of cases: 77.97979797979798
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 149.0
Weighted Recall: 0.5595959595959596
Weighted FalsePositiveRate: 0.04404040404040404
Kappa statistic: 0.5155555555555555
Training time: 20.0
		
Iteration: 11
Labeled set size: 299
Unlabelled set size: 196
	
Mean region size: 15.114784205693379
Incorrectly Classified Instances: 43.83838383838384
Correctly Classified Instances: 56.16161616161616
Weighted Precision: 0.5485126983938525
Weighted AreaUnderROC: 0.8296969696969697
Root mean squared error: 0.26514930480086907
Relative absolute error: 51.03377813063219
Root relative squared error: 92.23232954990581
Weighted TruePositiveRate: 0.5616161616161616
Weighted MatthewsCorrelation: 0.5071224724939019
Weighted FMeasure: 0.5428377784384419
Iteration time: 23.0
Weighted AreaUnderPRC: 0.44957033493322157
Mean absolute error: 0.08435335228203722
Coverage of cases: 70.3030303030303
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 172.0
Weighted Recall: 0.5616161616161616
Weighted FalsePositiveRate: 0.043838383838383836
Kappa statistic: 0.5177777777777777
Training time: 22.0
		
Iteration: 12
Labeled set size: 319
Unlabelled set size: 176
	
Mean region size: 14.692378328742047
Incorrectly Classified Instances: 41.01010101010101
Correctly Classified Instances: 58.98989898989899
Weighted Precision: 0.594399329409855
Weighted AreaUnderROC: 0.8264040404040404
Root mean squared error: 0.25893022157338175
Relative absolute error: 47.96419168315699
Root relative squared error: 90.0690180746346
Weighted TruePositiveRate: 0.5898989898989899
Weighted MatthewsCorrelation: 0.5474966905549572
Weighted FMeasure: 0.5819544448447979
Iteration time: 24.0
Weighted AreaUnderPRC: 0.4676125588737877
Mean absolute error: 0.07927965567464017
Coverage of cases: 69.6969696969697
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 196.0
Weighted Recall: 0.5898989898989899
Weighted FalsePositiveRate: 0.04101010101010101
Kappa statistic: 0.5488888888888889
Training time: 23.0
		
Iteration: 13
Labeled set size: 339
Unlabelled set size: 156
	
Mean region size: 13.406795224977094
Incorrectly Classified Instances: 40.0
Correctly Classified Instances: 60.0
Weighted Precision: 0.6086632000685036
Weighted AreaUnderROC: 0.8321616161616161
Root mean squared error: 0.2586098752417459
Relative absolute error: 46.219119347189256
Root relative squared error: 89.95758542934922
Weighted TruePositiveRate: 0.6
Weighted MatthewsCorrelation: 0.5594567988973437
Weighted FMeasure: 0.5909485803271325
Iteration time: 26.0
Weighted AreaUnderPRC: 0.4848765739018481
Mean absolute error: 0.07639523859039596
Coverage of cases: 70.1010101010101
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 222.0
Weighted Recall: 0.6
Weighted FalsePositiveRate: 0.039999999999999994
Kappa statistic: 0.5599999999999999
Training time: 25.0
		
Iteration: 14
Labeled set size: 359
Unlabelled set size: 136
	
Mean region size: 12.561983471074443
Incorrectly Classified Instances: 36.56565656565657
Correctly Classified Instances: 63.43434343434343
Weighted Precision: 0.6444606983954575
Weighted AreaUnderROC: 0.8434680134680135
Root mean squared error: 0.2471985519617189
Relative absolute error: 41.997537030870156
Root relative squared error: 85.98815043439647
Weighted TruePositiveRate: 0.6343434343434343
Weighted MatthewsCorrelation: 0.5999875801652598
Weighted FMeasure: 0.6316596169328362
Iteration time: 30.0
Weighted AreaUnderPRC: 0.5185206085251596
Mean absolute error: 0.06941741657995111
Coverage of cases: 71.11111111111111
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 252.0
Weighted Recall: 0.6343434343434343
Weighted FalsePositiveRate: 0.036565656565656565
Kappa statistic: 0.5977777777777777
Training time: 29.0
		
Iteration: 15
Labeled set size: 379
Unlabelled set size: 116
	
Mean region size: 14.233241505968827
Incorrectly Classified Instances: 36.36363636363637
Correctly Classified Instances: 63.63636363636363
Weighted Precision: 0.656629589301204
Weighted AreaUnderROC: 0.8461773288439954
Root mean squared error: 0.2373679815463207
Relative absolute error: 42.03627272335266
Root relative squared error: 82.56858118115063
Weighted TruePositiveRate: 0.6363636363636364
Weighted MatthewsCorrelation: 0.6055219986670305
Weighted FMeasure: 0.6338311683334266
Iteration time: 30.0
Weighted AreaUnderPRC: 0.5326002782169786
Mean absolute error: 0.06948144251793872
Coverage of cases: 74.54545454545455
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 282.0
Weighted Recall: 0.6363636363636364
Weighted FalsePositiveRate: 0.03636363636363636
Kappa statistic: 0.6
Training time: 29.0
		
Iteration: 16
Labeled set size: 399
Unlabelled set size: 96
	
Mean region size: 13.93939393939399
Incorrectly Classified Instances: 33.93939393939394
Correctly Classified Instances: 66.06060606060606
Weighted Precision: 0.6695977876023301
Weighted AreaUnderROC: 0.8540314253647587
Root mean squared error: 0.23520469807604183
Relative absolute error: 40.89689748953305
Root relative squared error: 81.81608185217645
Weighted TruePositiveRate: 0.6606060606060606
Weighted MatthewsCorrelation: 0.6284410489881902
Weighted FMeasure: 0.6578104803117217
Iteration time: 33.0
Weighted AreaUnderPRC: 0.5317950052974015
Mean absolute error: 0.0675981776686501
Coverage of cases: 75.15151515151516
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 315.0
Weighted Recall: 0.6606060606060606
Weighted FalsePositiveRate: 0.03393939393939394
Kappa statistic: 0.6266666666666667
Training time: 33.0
		
Iteration: 17
Labeled set size: 419
Unlabelled set size: 76
	
Mean region size: 13.131313131313197
Incorrectly Classified Instances: 32.121212121212125
Correctly Classified Instances: 67.87878787878788
Weighted Precision: 0.7009690004663296
Weighted AreaUnderROC: 0.8576296296296296
Root mean squared error: 0.22974133134942457
Relative absolute error: 38.3542320414875
Root relative squared error: 79.91564677179866
Weighted TruePositiveRate: 0.6787878787878788
Weighted MatthewsCorrelation: 0.6540014124143089
Weighted FMeasure: 0.6801592988401572
Iteration time: 34.0
Weighted AreaUnderPRC: 0.5532706548989946
Mean absolute error: 0.06339542486196321
Coverage of cases: 76.16161616161617
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 349.0
Weighted Recall: 0.6787878787878788
Weighted FalsePositiveRate: 0.03212121212121212
Kappa statistic: 0.6466666666666667
Training time: 33.0
		
Iteration: 18
Labeled set size: 439
Unlabelled set size: 56
	
Mean region size: 14.563820018365538
Incorrectly Classified Instances: 32.323232323232325
Correctly Classified Instances: 67.67676767676768
Weighted Precision: 0.6878412247089146
Weighted AreaUnderROC: 0.8712166105499439
Root mean squared error: 0.23147435663645852
Relative absolute error: 38.90488602257598
Root relative squared error: 80.51848055826505
Weighted TruePositiveRate: 0.6767676767676768
Weighted MatthewsCorrelation: 0.6483998137963023
Weighted FMeasure: 0.6780756060491286
Iteration time: 36.0
Weighted AreaUnderPRC: 0.5603754431580663
Mean absolute error: 0.06430559673153095
Coverage of cases: 78.18181818181819
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 385.0
Weighted Recall: 0.6767676767676768
Weighted FalsePositiveRate: 0.03232323232323232
Kappa statistic: 0.6444444444444445
Training time: 35.0
		
Iteration: 19
Labeled set size: 459
Unlabelled set size: 36
	
Mean region size: 14.214876033057937
Incorrectly Classified Instances: 35.55555555555556
Correctly Classified Instances: 64.44444444444444
Weighted Precision: 0.6536367138134317
Weighted AreaUnderROC: 0.8628574635241302
Root mean squared error: 0.23583219169808758
Relative absolute error: 41.45770945655417
Root relative squared error: 82.03435584909474
Weighted TruePositiveRate: 0.6444444444444445
Weighted MatthewsCorrelation: 0.6102235164440446
Weighted FMeasure: 0.6401573602823093
Iteration time: 41.0
Weighted AreaUnderPRC: 0.5375784613734933
Mean absolute error: 0.06852513959761064
Coverage of cases: 76.76767676767676
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 426.0
Weighted Recall: 0.6444444444444445
Weighted FalsePositiveRate: 0.03555555555555555
Kappa statistic: 0.6088888888888889
Training time: 40.0
		
Iteration: 20
Labeled set size: 479
Unlabelled set size: 16
	
Mean region size: 15.188246097337029
Incorrectly Classified Instances: 33.93939393939394
Correctly Classified Instances: 66.06060606060606
Weighted Precision: 0.6850763432116576
Weighted AreaUnderROC: 0.8631604938271604
Root mean squared error: 0.2315368260783186
Relative absolute error: 41.84648320706786
Root relative squared error: 80.54021058751316
Weighted TruePositiveRate: 0.6606060606060606
Weighted MatthewsCorrelation: 0.6331506043993502
Weighted FMeasure: 0.6575170176726306
Iteration time: 42.0
Weighted AreaUnderPRC: 0.5347340439046452
Mean absolute error: 0.06916774083812914
Coverage of cases: 75.95959595959596
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 468.0
Weighted Recall: 0.6606060606060606
Weighted FalsePositiveRate: 0.03393939393939394
Kappa statistic: 0.6266666666666667
Training time: 42.0
		
Time end:Wed Nov 01 14.50.45 EET 2017