Wed Nov 01 14.53.17 EET 2017
Dataset: vowel
Test set size: 495
Initial Labelled set size: 99
Initial Unlabelled set size: 396
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Wed Nov 01 14.53.17 EET 2017
		
Iteration: 1
Labeled set size: 99
Unlabelled set size: 396
	
Mean region size: 54.545454545454064
Incorrectly Classified Instances: 61.41414141414141
Correctly Classified Instances: 38.58585858585859
Weighted Precision: 0.4424247246763103
Weighted AreaUnderROC: 0.6622222222222223
Root mean squared error: 0.318311661818392
Relative absolute error: 70.80000000000038
Root relative squared error: 110.72488428533067
Weighted TruePositiveRate: 0.38585858585858585
Weighted MatthewsCorrelation: 0.34188144926192887
Weighted FMeasure: 0.38670461512195903
Iteration time: 15.0
Weighted AreaUnderPRC: 0.22723401717693475
Mean absolute error: 0.11702479338843114
Coverage of cases: 72.32323232323232
Instances selection time: 12.0
Test time: 10.0
Accumulative iteration time: 15.0
Weighted Recall: 0.38585858585858585
Weighted FalsePositiveRate: 0.061414141414141414
Kappa statistic: 0.3244444444444445
Training time: 3.0
		
Iteration: 2
Labeled set size: 119
Unlabelled set size: 376
	
Mean region size: 45.45454545454594
Incorrectly Classified Instances: 59.5959595959596
Correctly Classified Instances: 40.4040404040404
Weighted Precision: 0.459457289402872
Weighted AreaUnderROC: 0.672222222222222
Root mean squared error: 0.3158787694388369
Relative absolute error: 68.47008547008511
Root relative squared error: 109.87860135096999
Weighted TruePositiveRate: 0.40404040404040403
Weighted MatthewsCorrelation: 0.3604878220612183
Weighted FMeasure: 0.4020462419754123
Iteration time: 14.0
Weighted AreaUnderPRC: 0.2383484738574367
Mean absolute error: 0.11317369499187693
Coverage of cases: 66.66666666666667
Instances selection time: 13.0
Test time: 10.0
Accumulative iteration time: 29.0
Weighted Recall: 0.40404040404040403
Weighted FalsePositiveRate: 0.0595959595959596
Kappa statistic: 0.3444444444444445
Training time: 1.0
		
Iteration: 3
Labeled set size: 139
Unlabelled set size: 356
	
Mean region size: 36.36363636363658
Incorrectly Classified Instances: 53.93939393939394
Correctly Classified Instances: 46.06060606060606
Weighted Precision: 0.5048074969197964
Weighted AreaUnderROC: 0.7033333333333334
Root mean squared error: 0.3021988593573405
Relative absolute error: 62.315555555555044
Root relative squared error: 105.12003720614888
Weighted TruePositiveRate: 0.46060606060606063
Weighted MatthewsCorrelation: 0.4173653035095564
Weighted FMeasure: 0.45307233703284977
Iteration time: 14.0
Weighted AreaUnderPRC: 0.28258088981945184
Mean absolute error: 0.10300091827364537
Coverage of cases: 64.04040404040404
Instances selection time: 13.0
Test time: 12.0
Accumulative iteration time: 43.0
Weighted Recall: 0.46060606060606063
Weighted FalsePositiveRate: 0.05393939393939394
Kappa statistic: 0.40666666666666673
Training time: 1.0
		
Iteration: 4
Labeled set size: 159
Unlabelled set size: 336
	
Mean region size: 27.272727272727032
Incorrectly Classified Instances: 50.101010101010104
Correctly Classified Instances: 49.898989898989896
Weighted Precision: 0.5408567136328972
Weighted AreaUnderROC: 0.7244444444444444
Root mean squared error: 0.29247996498388446
Relative absolute error: 58.0156862745092
Root relative squared error: 101.73931452469012
Weighted TruePositiveRate: 0.498989898989899
Weighted MatthewsCorrelation: 0.4541944885922142
Weighted FMeasure: 0.4800737339553537
Iteration time: 15.0
Weighted AreaUnderPRC: 0.3132340804171377
Mean absolute error: 0.09589369632150342
Coverage of cases: 59.5959595959596
Instances selection time: 14.0
Test time: 12.0
Accumulative iteration time: 58.0
Weighted Recall: 0.498989898989899
Weighted FalsePositiveRate: 0.050101010101010104
Kappa statistic: 0.44888888888888884
Training time: 1.0
		
Iteration: 5
Labeled set size: 179
Unlabelled set size: 316
	
Mean region size: 18.18181818181829
Incorrectly Classified Instances: 50.101010101010104
Correctly Classified Instances: 49.898989898989896
Weighted Precision: 0.5445438929363984
Weighted AreaUnderROC: 0.7244444444444444
Root mean squared error: 0.29342113661340335
Relative absolute error: 57.70994152046763
Root relative squared error: 102.06670158671541
Weighted TruePositiveRate: 0.498989898989899
Weighted MatthewsCorrelation: 0.45539179472575436
Weighted FMeasure: 0.48000598728213667
Iteration time: 14.0
Weighted AreaUnderPRC: 0.3127501819151983
Mean absolute error: 0.09538833309168265
Coverage of cases: 56.16161616161616
Instances selection time: 14.0
Test time: 14.0
Accumulative iteration time: 72.0
Weighted Recall: 0.498989898989899
Weighted FalsePositiveRate: 0.0501010101010101
Kappa statistic: 0.44888888888888884
Training time: 0.0
		
Iteration: 6
Labeled set size: 199
Unlabelled set size: 296
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 48.484848484848484
Correctly Classified Instances: 51.515151515151516
Weighted Precision: 0.5870217266175292
Weighted AreaUnderROC: 0.7333333333333333
Root mean squared error: 0.2894191125763149
Relative absolute error: 55.777777777777715
Root relative squared error: 100.67459535384185
Weighted TruePositiveRate: 0.5151515151515151
Weighted MatthewsCorrelation: 0.4819819562861165
Weighted FMeasure: 0.4971936646649587
Iteration time: 15.0
Weighted AreaUnderPRC: 0.3395639975582127
Mean absolute error: 0.09219467401285632
Coverage of cases: 51.515151515151516
Instances selection time: 15.0
Test time: 14.0
Accumulative iteration time: 87.0
Weighted Recall: 0.5151515151515151
Weighted FalsePositiveRate: 0.048484848484848485
Kappa statistic: 0.4666666666666666
Training time: 0.0
		
Iteration: 7
Labeled set size: 219
Unlabelled set size: 276
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 44.44444444444444
Correctly Classified Instances: 55.55555555555556
Weighted Precision: 0.6538056516552949
Weighted AreaUnderROC: 0.7555555555555556
Root mean squared error: 0.27772717264500485
Relative absolute error: 51.33333333333299
Root relative squared error: 96.6075477044725
Weighted TruePositiveRate: 0.5555555555555556
Weighted MatthewsCorrelation: 0.5321103525647958
Weighted FMeasure: 0.5346582829631592
Iteration time: 16.0
Weighted AreaUnderPRC: 0.38121078614280934
Mean absolute error: 0.08484848484848483
Coverage of cases: 55.55555555555556
Instances selection time: 14.0
Test time: 15.0
Accumulative iteration time: 103.0
Weighted Recall: 0.5555555555555556
Weighted FalsePositiveRate: 0.044444444444444446
Kappa statistic: 0.5111111111111111
Training time: 2.0
		
Iteration: 8
Labeled set size: 239
Unlabelled set size: 256
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 42.02020202020202
Correctly Classified Instances: 57.97979797979798
Weighted Precision: 0.6736141251176491
Weighted AreaUnderROC: 0.768888888888889
Root mean squared error: 0.27055267721433157
Relative absolute error: 48.588444444444264
Root relative squared error: 94.11189557589869
Weighted TruePositiveRate: 0.5797979797979798
Weighted MatthewsCorrelation: 0.5546359693531903
Weighted FMeasure: 0.5518876118656754
Iteration time: 15.0
Weighted AreaUnderPRC: 0.4015103632133991
Mean absolute error: 0.08031147842056954
Coverage of cases: 57.97979797979798
Instances selection time: 14.0
Test time: 17.0
Accumulative iteration time: 118.0
Weighted Recall: 0.5797979797979798
Weighted FalsePositiveRate: 0.04202020202020202
Kappa statistic: 0.5377777777777777
Training time: 1.0
		
Iteration: 9
Labeled set size: 259
Unlabelled set size: 236
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 38.38383838383838
Correctly Classified Instances: 61.61616161616162
Weighted Precision: 0.6800182767164492
Weighted AreaUnderROC: 0.7888888888888889
Root mean squared error: 0.25900332813514376
Relative absolute error: 44.57613168724245
Root relative squared error: 90.09444823181249
Weighted TruePositiveRate: 0.6161616161616161
Weighted MatthewsCorrelation: 0.5855039133023912
Weighted FMeasure: 0.5872839282539877
Iteration time: 15.0
Weighted AreaUnderPRC: 0.4407711652514612
Mean absolute error: 0.07367955650783924
Coverage of cases: 61.61616161616162
Instances selection time: 14.0
Test time: 17.0
Accumulative iteration time: 133.0
Weighted Recall: 0.6161616161616161
Weighted FalsePositiveRate: 0.03838383838383838
Kappa statistic: 0.5777777777777777
Training time: 1.0
		
Iteration: 10
Labeled set size: 279
Unlabelled set size: 216
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 36.36363636363637
Correctly Classified Instances: 63.63636363636363
Weighted Precision: 0.691105633632843
Weighted AreaUnderROC: 0.8
Root mean squared error: 0.2524416200885908
Relative absolute error: 42.275862068965225
Root relative squared error: 87.81195452731474
Weighted TruePositiveRate: 0.6363636363636364
Weighted MatthewsCorrelation: 0.6026465071813443
Weighted FMeasure: 0.6028314848401284
Iteration time: 14.0
Weighted AreaUnderPRC: 0.4633538615921077
Mean absolute error: 0.06987745796523222
Coverage of cases: 63.63636363636363
Instances selection time: 13.0
Test time: 18.0
Accumulative iteration time: 147.0
Weighted Recall: 0.6363636363636364
Weighted FalsePositiveRate: 0.03636363636363636
Kappa statistic: 0.6
Training time: 1.0
		
Iteration: 11
Labeled set size: 299
Unlabelled set size: 196
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 33.93939393939394
Correctly Classified Instances: 66.06060606060606
Weighted Precision: 0.713639541607055
Weighted AreaUnderROC: 0.8133333333333334
Root mean squared error: 0.2441770032879396
Relative absolute error: 39.55698924731153
Root relative squared error: 84.93710308867408
Weighted TruePositiveRate: 0.6606060606060606
Weighted MatthewsCorrelation: 0.6326467870931965
Weighted FMeasure: 0.6374256129373438
Iteration time: 14.0
Weighted AreaUnderPRC: 0.4950709422311734
Mean absolute error: 0.06538345330134179
Coverage of cases: 66.06060606060606
Instances selection time: 13.0
Test time: 18.0
Accumulative iteration time: 161.0
Weighted Recall: 0.6606060606060606
Weighted FalsePositiveRate: 0.03393939393939394
Kappa statistic: 0.6266666666666667
Training time: 1.0
		
Iteration: 12
Labeled set size: 319
Unlabelled set size: 176
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 31.91919191919192
Correctly Classified Instances: 68.08080808080808
Weighted Precision: 0.7315840984188064
Weighted AreaUnderROC: 0.8244444444444445
Root mean squared error: 0.23704899342358357
Relative absolute error: 37.27407407407377
Root relative squared error: 82.45762098956766
Weighted TruePositiveRate: 0.6808080808080809
Weighted MatthewsCorrelation: 0.6540364263109794
Weighted FMeasure: 0.6574568258057027
Iteration time: 13.0
Weighted AreaUnderPRC: 0.5227041184738894
Mean absolute error: 0.061610039791857876
Coverage of cases: 68.08080808080808
Instances selection time: 12.0
Test time: 20.0
Accumulative iteration time: 174.0
Weighted Recall: 0.6808080808080809
Weighted FalsePositiveRate: 0.031919191919191924
Kappa statistic: 0.648888888888889
Training time: 1.0
		
Iteration: 13
Labeled set size: 339
Unlabelled set size: 156
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 29.494949494949495
Correctly Classified Instances: 70.5050505050505
Weighted Precision: 0.7664524163959511
Weighted AreaUnderROC: 0.8377777777777777
Root mean squared error: 0.22808636054692868
Relative absolute error: 34.56761904761873
Root relative squared error: 79.33996428013268
Weighted TruePositiveRate: 0.705050505050505
Weighted MatthewsCorrelation: 0.6817438384787773
Weighted FMeasure: 0.6776311324076978
Iteration time: 13.0
Weighted AreaUnderPRC: 0.550637466415746
Mean absolute error: 0.057136560409287517
Coverage of cases: 70.5050505050505
Instances selection time: 12.0
Test time: 20.0
Accumulative iteration time: 187.0
Weighted Recall: 0.705050505050505
Weighted FalsePositiveRate: 0.029494949494949494
Kappa statistic: 0.6755555555555556
Training time: 1.0
		
Iteration: 14
Labeled set size: 359
Unlabelled set size: 136
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 26.262626262626263
Correctly Classified Instances: 73.73737373737374
Weighted Precision: 0.7924686054598852
Weighted AreaUnderROC: 0.8555555555555555
Root mean squared error: 0.21541512056446274
Relative absolute error: 31.003003003002817
Root relative squared error: 74.93226657658239
Weighted TruePositiveRate: 0.7373737373737373
Weighted MatthewsCorrelation: 0.715980255083534
Weighted FMeasure: 0.7113816628715204
Iteration time: 12.0
Weighted AreaUnderPRC: 0.592686423734907
Mean absolute error: 0.051244633062814904
Coverage of cases: 73.73737373737374
Instances selection time: 11.0
Test time: 21.0
Accumulative iteration time: 199.0
Weighted Recall: 0.7373737373737373
Weighted FalsePositiveRate: 0.026262626262626262
Kappa statistic: 0.711111111111111
Training time: 1.0
		
Iteration: 15
Labeled set size: 379
Unlabelled set size: 116
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 24.04040404040404
Correctly Classified Instances: 75.95959595959596
Weighted Precision: 0.8086642019444991
Weighted AreaUnderROC: 0.8677777777777778
Root mean squared error: 0.20625889196531247
Relative absolute error: 28.51908831908816
Root relative squared error: 71.74726749002876
Weighted TruePositiveRate: 0.7595959595959596
Weighted MatthewsCorrelation: 0.7383542788937475
Weighted FMeasure: 0.7331266307438904
Iteration time: 11.0
Weighted AreaUnderPRC: 0.6249168669490786
Mean absolute error: 0.047138988957170815
Coverage of cases: 75.95959595959596
Instances selection time: 10.0
Test time: 23.0
Accumulative iteration time: 210.0
Weighted Recall: 0.7595959595959596
Weighted FalsePositiveRate: 0.024040404040404046
Kappa statistic: 0.7355555555555555
Training time: 1.0
		
Iteration: 16
Labeled set size: 399
Unlabelled set size: 96
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 19.393939393939394
Correctly Classified Instances: 80.60606060606061
Weighted Precision: 0.8456721731770692
Weighted AreaUnderROC: 0.8933333333333334
Root mean squared error: 0.18540536821960732
Relative absolute error: 23.44390243902426
Root relative squared error: 64.49335793957702
Weighted TruePositiveRate: 0.806060606060606
Weighted MatthewsCorrelation: 0.7889981274065448
Weighted FMeasure: 0.7855367326822414
Iteration time: 10.0
Weighted AreaUnderPRC: 0.6837319081170609
Mean absolute error: 0.03875025196532961
Coverage of cases: 80.60606060606061
Instances selection time: 9.0
Test time: 23.0
Accumulative iteration time: 220.0
Weighted Recall: 0.806060606060606
Weighted FalsePositiveRate: 0.019393939393939394
Kappa statistic: 0.7866666666666666
Training time: 1.0
		
Iteration: 17
Labeled set size: 419
Unlabelled set size: 76
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 16.96969696969697
Correctly Classified Instances: 83.03030303030303
Weighted Precision: 0.8538336108282107
Weighted AreaUnderROC: 0.9066666666666668
Root mean squared error: 0.17354765785648998
Relative absolute error: 20.74728682170527
Root relative squared error: 60.368646955553395
Weighted TruePositiveRate: 0.8303030303030303
Weighted MatthewsCorrelation: 0.8115720546527597
Weighted FMeasure: 0.8104760633506298
Iteration time: 8.0
Weighted AreaUnderPRC: 0.7160545578058186
Mean absolute error: 0.03429303606893455
Coverage of cases: 83.03030303030303
Instances selection time: 7.0
Test time: 24.0
Accumulative iteration time: 228.0
Weighted Recall: 0.8303030303030303
Weighted FalsePositiveRate: 0.01696969696969697
Kappa statistic: 0.8133333333333334
Training time: 1.0
		
Iteration: 18
Labeled set size: 439
Unlabelled set size: 56
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 13.131313131313131
Correctly Classified Instances: 86.86868686868686
Weighted Precision: 0.8878728395570565
Weighted AreaUnderROC: 0.9277777777777779
Root mean squared error: 0.15277723516056932
Relative absolute error: 16.5358024691356
Root relative squared error: 53.14364415036136
Weighted TruePositiveRate: 0.8686868686868687
Weighted MatthewsCorrelation: 0.8559343137406854
Weighted FMeasure: 0.8571640401184091
Iteration time: 7.0
Weighted AreaUnderPRC: 0.7762603014201367
Mean absolute error: 0.02733190490766232
Coverage of cases: 86.86868686868686
Instances selection time: 6.0
Test time: 25.0
Accumulative iteration time: 235.0
Weighted Recall: 0.8686868686868687
Weighted FalsePositiveRate: 0.013131313131313131
Kappa statistic: 0.8555555555555556
Training time: 1.0
		
Iteration: 19
Labeled set size: 459
Unlabelled set size: 36
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 11.515151515151516
Correctly Classified Instances: 88.48484848484848
Weighted Precision: 0.8997614987337734
Weighted AreaUnderROC: 0.9366666666666666
Root mean squared error: 0.14314992847938204
Relative absolute error: 14.710638297872215
Root relative squared error: 49.79478029735546
Weighted TruePositiveRate: 0.8848484848484849
Weighted MatthewsCorrelation: 0.8754319257670031
Weighted FMeasure: 0.8794235437673412
Iteration time: 5.0
Weighted AreaUnderPRC: 0.8022023085132478
Mean absolute error: 0.024315104624582327
Coverage of cases: 88.48484848484848
Instances selection time: 4.0
Test time: 26.0
Accumulative iteration time: 240.0
Weighted Recall: 0.8848484848484849
Weighted FalsePositiveRate: 0.011515151515151515
Kappa statistic: 0.8733333333333334
Training time: 1.0
		
Iteration: 20
Labeled set size: 479
Unlabelled set size: 16
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 9.292929292929292
Correctly Classified Instances: 90.70707070707071
Weighted Precision: 0.9137302602354295
Weighted AreaUnderROC: 0.9488888888888889
Root mean squared error: 0.12868015058428178
Relative absolute error: 12.237641723355981
Root relative squared error: 44.76146020497536
Weighted TruePositiveRate: 0.907070707070707
Weighted MatthewsCorrelation: 0.8994304477682664
Weighted FMeasure: 0.9061795575027852
Iteration time: 3.0
Weighted AreaUnderPRC: 0.8359651648089986
Mean absolute error: 0.020227506980753818
Coverage of cases: 90.70707070707071
Instances selection time: 2.0
Test time: 26.0
Accumulative iteration time: 243.0
Weighted Recall: 0.907070707070707
Weighted FalsePositiveRate: 0.009292929292929292
Kappa statistic: 0.8977777777777777
Training time: 1.0
		
Time end:Wed Nov 01 14.53.18 EET 2017