Wed Nov 01 14.59.37 EET 2017
Dataset: vowel
Test set size: 495
Initial Labelled set size: 99
Initial Unlabelled set size: 396
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Wed Nov 01 14.59.37 EET 2017
		
Iteration: 1
Labeled set size: 99
Unlabelled set size: 396
	
Mean region size: 18.80624426078978
Incorrectly Classified Instances: 51.515151515151516
Correctly Classified Instances: 48.484848484848484
Weighted Precision: 0.5154252450668442
Weighted AreaUnderROC: 0.897818181818182
Root mean squared error: 0.2699262253570057
Relative absolute error: 58.926628057708484
Root relative squared error: 93.89398395740352
Weighted TruePositiveRate: 0.48484848484848486
Weighted MatthewsCorrelation: 0.44411126161985576
Weighted FMeasure: 0.4880910151529658
Iteration time: 1.0
Weighted AreaUnderPRC: 0.5267875432256933
Mean absolute error: 0.09739938521935348
Coverage of cases: 72.92929292929293
Instances selection time: 1.0
Test time: 34.0
Accumulative iteration time: 1.0
Weighted Recall: 0.48484848484848486
Weighted FalsePositiveRate: 0.05151515151515151
Kappa statistic: 0.43333333333333335
Training time: 0.0
		
Iteration: 2
Labeled set size: 119
Unlabelled set size: 376
	
Mean region size: 19.779614325068962
Incorrectly Classified Instances: 49.292929292929294
Correctly Classified Instances: 50.707070707070706
Weighted Precision: 0.5478514558868031
Weighted AreaUnderROC: 0.9056475869809202
Root mean squared error: 0.263474051165353
Relative absolute error: 57.888995180877615
Root relative squared error: 91.6495916637679
Weighted TruePositiveRate: 0.5070707070707071
Weighted MatthewsCorrelation: 0.4727412785016806
Weighted FMeasure: 0.5133269905423791
Iteration time: 2.0
Weighted AreaUnderPRC: 0.541896669134253
Mean absolute error: 0.09568428955517022
Coverage of cases: 75.55555555555556
Instances selection time: 1.0
Test time: 33.0
Accumulative iteration time: 3.0
Weighted Recall: 0.5070707070707071
Weighted FalsePositiveRate: 0.049292929292929305
Kappa statistic: 0.4577777777777778
Training time: 1.0
		
Iteration: 3
Labeled set size: 139
Unlabelled set size: 356
	
Mean region size: 20.88154269972461
Incorrectly Classified Instances: 48.888888888888886
Correctly Classified Instances: 51.111111111111114
Weighted Precision: 0.54654912870896
Weighted AreaUnderROC: 0.9152996632996633
Root mean squared error: 0.25781309441838096
Relative absolute error: 57.26275261879409
Root relative squared error: 89.68042478759371
Weighted TruePositiveRate: 0.5111111111111111
Weighted MatthewsCorrelation: 0.4747977117138776
Weighted FMeasure: 0.5158711411808348
Iteration time: 1.0
Weighted AreaUnderPRC: 0.5568561604774112
Mean absolute error: 0.09464917788230488
Coverage of cases: 78.98989898989899
Instances selection time: 1.0
Test time: 34.0
Accumulative iteration time: 4.0
Weighted Recall: 0.5111111111111111
Weighted FalsePositiveRate: 0.048888888888888885
Kappa statistic: 0.46222222222222215
Training time: 0.0
		
Iteration: 4
Labeled set size: 159
Unlabelled set size: 336
	
Mean region size: 20.53259871441699
Incorrectly Classified Instances: 47.474747474747474
Correctly Classified Instances: 52.525252525252526
Weighted Precision: 0.5534027863718323
Weighted AreaUnderROC: 0.9136655443322111
Root mean squared error: 0.25846635800098167
Relative absolute error: 56.72800165161886
Root relative squared error: 89.90766287927427
Weighted TruePositiveRate: 0.5252525252525253
Weighted MatthewsCorrelation: 0.4893506741013512
Weighted FMeasure: 0.5327213373536644
Iteration time: 1.0
Weighted AreaUnderPRC: 0.5590446329587492
Mean absolute error: 0.09376529198614747
Coverage of cases: 78.38383838383838
Instances selection time: 1.0
Test time: 33.0
Accumulative iteration time: 5.0
Weighted Recall: 0.5252525252525253
Weighted FalsePositiveRate: 0.047474747474747475
Kappa statistic: 0.4777777777777778
Training time: 0.0
		
Iteration: 5
Labeled set size: 179
Unlabelled set size: 316
	
Mean region size: 21.634527089072613
Incorrectly Classified Instances: 47.676767676767675
Correctly Classified Instances: 52.323232323232325
Weighted Precision: 0.5377257133810118
Weighted AreaUnderROC: 0.9156677890011224
Root mean squared error: 0.2552619968934807
Relative absolute error: 56.376126506249754
Root relative squared error: 88.79302412928439
Weighted TruePositiveRate: 0.5232323232323233
Weighted MatthewsCorrelation: 0.48013996248869356
Weighted FMeasure: 0.5232051866904388
Iteration time: 2.0
Weighted AreaUnderPRC: 0.5575301887019233
Mean absolute error: 0.09318368017562004
Coverage of cases: 81.01010101010101
Instances selection time: 1.0
Test time: 33.0
Accumulative iteration time: 7.0
Weighted Recall: 0.5232323232323233
Weighted FalsePositiveRate: 0.04767676767676768
Kappa statistic: 0.47555555555555556
Training time: 1.0
		
Iteration: 6
Labeled set size: 199
Unlabelled set size: 296
	
Mean region size: 22.571166207529906
Incorrectly Classified Instances: 48.888888888888886
Correctly Classified Instances: 51.111111111111114
Weighted Precision: 0.5426602675720399
Weighted AreaUnderROC: 0.9185499438832773
Root mean squared error: 0.25436860838647785
Relative absolute error: 57.81797066989154
Root relative squared error: 88.48225845235429
Weighted TruePositiveRate: 0.5111111111111111
Weighted MatthewsCorrelation: 0.4740830151386055
Weighted FMeasure: 0.5165267567389483
Iteration time: 1.0
Weighted AreaUnderPRC: 0.5650663675779667
Mean absolute error: 0.09556689366924281
Coverage of cases: 83.43434343434343
Instances selection time: 0.0
Test time: 34.0
Accumulative iteration time: 8.0
Weighted Recall: 0.5111111111111111
Weighted FalsePositiveRate: 0.048888888888888885
Kappa statistic: 0.46222222222222215
Training time: 1.0
		
Iteration: 7
Labeled set size: 219
Unlabelled set size: 276
	
Mean region size: 22.773186409550107
Incorrectly Classified Instances: 49.292929292929294
Correctly Classified Instances: 50.707070707070706
Weighted Precision: 0.540860360111997
Weighted AreaUnderROC: 0.9173423120089786
Root mean squared error: 0.2546300130941092
Relative absolute error: 58.411556404425
Root relative squared error: 88.57318822174689
Weighted TruePositiveRate: 0.5070707070707071
Weighted MatthewsCorrelation: 0.47107971707311636
Weighted FMeasure: 0.5145533066954068
Iteration time: 1.0
Weighted AreaUnderPRC: 0.5586775625607538
Mean absolute error: 0.09654802711475269
Coverage of cases: 84.04040404040404
Instances selection time: 0.0
Test time: 33.0
Accumulative iteration time: 9.0
Weighted Recall: 0.5070707070707071
Weighted FalsePositiveRate: 0.04929292929292929
Kappa statistic: 0.4577777777777778
Training time: 1.0
		
Iteration: 8
Labeled set size: 239
Unlabelled set size: 256
	
Mean region size: 23.783287419651096
Incorrectly Classified Instances: 45.85858585858586
Correctly Classified Instances: 54.14141414141414
Weighted Precision: 0.5553325733767089
Weighted AreaUnderROC: 0.9249966329966332
Root mean squared error: 0.2453071314809038
Relative absolute error: 56.83947536600491
Root relative squared error: 85.33021879382517
Weighted TruePositiveRate: 0.5414141414141415
Weighted MatthewsCorrelation: 0.5015454389669707
Weighted FMeasure: 0.5458329133885466
Iteration time: 1.0
Weighted AreaUnderPRC: 0.5863580080963635
Mean absolute error: 0.09394954605951286
Coverage of cases: 86.46464646464646
Instances selection time: 1.0
Test time: 33.0
Accumulative iteration time: 10.0
Weighted Recall: 0.5414141414141415
Weighted FalsePositiveRate: 0.04585858585858585
Kappa statistic: 0.4955555555555556
Training time: 0.0
		
Iteration: 9
Labeled set size: 259
Unlabelled set size: 236
	
Mean region size: 24.903581267217632
Incorrectly Classified Instances: 41.41414141414141
Correctly Classified Instances: 58.58585858585859
Weighted Precision: 0.5927239280750725
Weighted AreaUnderROC: 0.9373198653198652
Root mean squared error: 0.2358800939764859
Relative absolute error: 54.68066031788728
Root relative squared error: 82.05101868262825
Weighted TruePositiveRate: 0.5858585858585859
Weighted MatthewsCorrelation: 0.5473371502403491
Weighted FMeasure: 0.5879068989645523
Iteration time: 1.0
Weighted AreaUnderPRC: 0.6030904463266472
Mean absolute error: 0.09038125672378121
Coverage of cases: 90.9090909090909
Instances selection time: 0.0
Test time: 34.0
Accumulative iteration time: 11.0
Weighted Recall: 0.5858585858585859
Weighted FalsePositiveRate: 0.04141414141414142
Kappa statistic: 0.5444444444444444
Training time: 1.0
		
Iteration: 10
Labeled set size: 279
Unlabelled set size: 216
	
Mean region size: 24.536271808999093
Incorrectly Classified Instances: 43.83838383838384
Correctly Classified Instances: 56.16161616161616
Weighted Precision: 0.569962807782071
Weighted AreaUnderROC: 0.9345634118967452
Root mean squared error: 0.24107698050489992
Relative absolute error: 55.7584159618198
Root relative squared error: 83.85875848146395
Weighted TruePositiveRate: 0.5616161616161616
Weighted MatthewsCorrelation: 0.5215651217340246
Weighted FMeasure: 0.5648268215912962
Iteration time: 2.0
Weighted AreaUnderPRC: 0.5888736660326845
Mean absolute error: 0.09216267101127298
Coverage of cases: 89.8989898989899
Instances selection time: 1.0
Test time: 32.0
Accumulative iteration time: 13.0
Weighted Recall: 0.5616161616161616
Weighted FalsePositiveRate: 0.043838383838383836
Kappa statistic: 0.5177777777777777
Training time: 1.0
		
Iteration: 11
Labeled set size: 299
Unlabelled set size: 196
	
Mean region size: 24.426078971533503
Incorrectly Classified Instances: 44.64646464646464
Correctly Classified Instances: 55.35353535353536
Weighted Precision: 0.5653628464192542
Weighted AreaUnderROC: 0.9351604938271604
Root mean squared error: 0.24012172911541513
Relative absolute error: 55.681601906464444
Root relative squared error: 83.52647376729456
Weighted TruePositiveRate: 0.5535353535353535
Weighted MatthewsCorrelation: 0.5142090838094513
Weighted FMeasure: 0.557921490941686
Iteration time: 2.0
Weighted AreaUnderPRC: 0.6006477413486259
Mean absolute error: 0.09203570563052033
Coverage of cases: 90.1010101010101
Instances selection time: 1.0
Test time: 33.0
Accumulative iteration time: 15.0
Weighted Recall: 0.5535353535353535
Weighted FalsePositiveRate: 0.04464646464646465
Kappa statistic: 0.5088888888888888
Training time: 1.0
		
Iteration: 12
Labeled set size: 319
Unlabelled set size: 176
	
Mean region size: 24.242424242424214
Incorrectly Classified Instances: 45.45454545454545
Correctly Classified Instances: 54.54545454545455
Weighted Precision: 0.5612767218858906
Weighted AreaUnderROC: 0.9320628507295173
Root mean squared error: 0.24443927683324237
Relative absolute error: 56.22150246300389
Root relative squared error: 85.02833508372213
Weighted TruePositiveRate: 0.5454545454545454
Weighted MatthewsCorrelation: 0.506614247510077
Weighted FMeasure: 0.5498762557143935
Iteration time: 1.0
Weighted AreaUnderPRC: 0.5937536095895848
Mean absolute error: 0.09292810324463512
Coverage of cases: 89.4949494949495
Instances selection time: 1.0
Test time: 33.0
Accumulative iteration time: 16.0
Weighted Recall: 0.5454545454545454
Weighted FalsePositiveRate: 0.04545454545454545
Kappa statistic: 0.49999999999999994
Training time: 0.0
		
Iteration: 13
Labeled set size: 339
Unlabelled set size: 156
	
Mean region size: 24.738292011019272
Incorrectly Classified Instances: 44.64646464646464
Correctly Classified Instances: 55.35353535353536
Weighted Precision: 0.5732479000484121
Weighted AreaUnderROC: 0.9329562289562291
Root mean squared error: 0.2410605641059829
Relative absolute error: 55.72298344091447
Root relative squared error: 83.85304802819282
Weighted TruePositiveRate: 0.5535353535353535
Weighted MatthewsCorrelation: 0.5166153862190175
Weighted FMeasure: 0.5578394050018113
Iteration time: 1.0
Weighted AreaUnderPRC: 0.5921784110104215
Mean absolute error: 0.09210410486101624
Coverage of cases: 89.0909090909091
Instances selection time: 0.0
Test time: 33.0
Accumulative iteration time: 17.0
Weighted Recall: 0.5535353535353535
Weighted FalsePositiveRate: 0.04464646464646464
Kappa statistic: 0.5088888888888888
Training time: 1.0
		
Iteration: 14
Labeled set size: 359
Unlabelled set size: 136
	
Mean region size: 25.179063360881525
Incorrectly Classified Instances: 41.61616161616162
Correctly Classified Instances: 58.38383838383838
Weighted Precision: 0.5925228093308705
Weighted AreaUnderROC: 0.9352323232323232
Root mean squared error: 0.23723923814468392
Relative absolute error: 55.49104719695029
Root relative squared error: 82.52379771903273
Weighted TruePositiveRate: 0.5838383838383838
Weighted MatthewsCorrelation: 0.5460662670881455
Weighted FMeasure: 0.586940631303134
Iteration time: 1.0
Weighted AreaUnderPRC: 0.5939303827596087
Mean absolute error: 0.09172073916851346
Coverage of cases: 89.6969696969697
Instances selection time: 0.0
Test time: 33.0
Accumulative iteration time: 18.0
Weighted Recall: 0.5838383838383838
Weighted FalsePositiveRate: 0.04161616161616162
Kappa statistic: 0.5422222222222222
Training time: 1.0
		
Iteration: 15
Labeled set size: 379
Unlabelled set size: 116
	
Mean region size: 25.638200183654675
Incorrectly Classified Instances: 41.01010101010101
Correctly Classified Instances: 58.98989898989899
Weighted Precision: 0.5968938323511259
Weighted AreaUnderROC: 0.9393131313131313
Root mean squared error: 0.2300621276667383
Relative absolute error: 54.09589897699281
Root relative squared error: 80.02723594484628
Weighted TruePositiveRate: 0.5898989898989899
Weighted MatthewsCorrelation: 0.5517244685805668
Weighted FMeasure: 0.5915820206976677
Iteration time: 1.0
Weighted AreaUnderPRC: 0.616265857523854
Mean absolute error: 0.08941470905288125
Coverage of cases: 92.92929292929293
Instances selection time: 0.0
Test time: 33.0
Accumulative iteration time: 19.0
Weighted Recall: 0.5898989898989899
Weighted FalsePositiveRate: 0.041010101010101004
Kappa statistic: 0.5488888888888889
Training time: 1.0
		
Iteration: 16
Labeled set size: 399
Unlabelled set size: 96
	
Mean region size: 26.023875114784165
Incorrectly Classified Instances: 42.82828282828283
Correctly Classified Instances: 57.17171717171717
Weighted Precision: 0.5827218275553441
Weighted AreaUnderROC: 0.9402558922558923
Root mean squared error: 0.22967133916579358
Relative absolute error: 54.0648637433119
Root relative squared error: 79.89129995274358
Weighted TruePositiveRate: 0.5717171717171717
Weighted MatthewsCorrelation: 0.5335624700135501
Weighted FMeasure: 0.5749869910241382
Iteration time: 1.0
Weighted AreaUnderPRC: 0.6182772930258025
Mean absolute error: 0.08936341114597067
Coverage of cases: 93.13131313131314
Instances selection time: 0.0
Test time: 33.0
Accumulative iteration time: 20.0
Weighted Recall: 0.5717171717171717
Weighted FalsePositiveRate: 0.042828282828282827
Kappa statistic: 0.5288888888888889
Training time: 1.0
		
Iteration: 17
Labeled set size: 419
Unlabelled set size: 76
	
Mean region size: 27.08907254361797
Incorrectly Classified Instances: 44.04040404040404
Correctly Classified Instances: 55.95959595959596
Weighted Precision: 0.569103700098945
Weighted AreaUnderROC: 0.9401346801346802
Root mean squared error: 0.23273791627618107
Relative absolute error: 55.615762451645004
Root relative squared error: 80.9580104645734
Weighted TruePositiveRate: 0.5595959595959596
Weighted MatthewsCorrelation: 0.5193015092165051
Weighted FMeasure: 0.5615250842467068
Iteration time: 2.0
Weighted AreaUnderPRC: 0.6144815226524549
Mean absolute error: 0.09192688008536423
Coverage of cases: 94.34343434343434
Instances selection time: 1.0
Test time: 32.0
Accumulative iteration time: 22.0
Weighted Recall: 0.5595959595959596
Weighted FalsePositiveRate: 0.04404040404040404
Kappa statistic: 0.5155555555555555
Training time: 1.0
		
Iteration: 18
Labeled set size: 439
Unlabelled set size: 56
	
Mean region size: 26.923783287419624
Incorrectly Classified Instances: 44.04040404040404
Correctly Classified Instances: 55.95959595959596
Weighted Precision: 0.5693594051523659
Weighted AreaUnderROC: 0.9412255892255892
Root mean squared error: 0.23172440855449128
Relative absolute error: 55.51885554227458
Root relative squared error: 80.60546125363555
Weighted TruePositiveRate: 0.5595959595959596
Weighted MatthewsCorrelation: 0.5194816620101804
Weighted FMeasure: 0.5618299762139531
Iteration time: 1.0
Weighted AreaUnderPRC: 0.6131434245770294
Mean absolute error: 0.09176670337566105
Coverage of cases: 94.14141414141415
Instances selection time: 0.0
Test time: 33.0
Accumulative iteration time: 23.0
Weighted Recall: 0.5595959595959596
Weighted FalsePositiveRate: 0.04404040404040403
Kappa statistic: 0.5155555555555555
Training time: 1.0
		
Iteration: 19
Labeled set size: 459
Unlabelled set size: 36
	
Mean region size: 27.1808999081726
Incorrectly Classified Instances: 43.83838383838384
Correctly Classified Instances: 56.16161616161616
Weighted Precision: 0.567924766137677
Weighted AreaUnderROC: 0.9449427609427611
Root mean squared error: 0.22700593841813435
Relative absolute error: 54.62467502273893
Root relative squared error: 78.9641388563745
Weighted TruePositiveRate: 0.5616161616161616
Weighted MatthewsCorrelation: 0.5199944785283429
Weighted FMeasure: 0.5622467279429231
Iteration time: 2.0
Weighted AreaUnderPRC: 0.6309857338183913
Mean absolute error: 0.09028871904585005
Coverage of cases: 95.75757575757575
Instances selection time: 1.0
Test time: 32.0
Accumulative iteration time: 25.0
Weighted Recall: 0.5616161616161616
Weighted FalsePositiveRate: 0.04383838383838384
Kappa statistic: 0.5177777777777777
Training time: 1.0
		
Iteration: 20
Labeled set size: 479
Unlabelled set size: 16
	
Mean region size: 26.960514233241472
Incorrectly Classified Instances: 44.44444444444444
Correctly Classified Instances: 55.55555555555556
Weighted Precision: 0.561124767248483
Weighted AreaUnderROC: 0.9430662177328843
Root mean squared error: 0.22944939422764984
Relative absolute error: 55.2889347294035
Root relative squared error: 79.81409628557886
Weighted TruePositiveRate: 0.5555555555555556
Weighted MatthewsCorrelation: 0.5131226964970534
Weighted FMeasure: 0.5562784711023796
Iteration time: 1.0
Weighted AreaUnderPRC: 0.6279906083823827
Mean absolute error: 0.09138666897422125
Coverage of cases: 95.55555555555556
Instances selection time: 0.0
Test time: 33.0
Accumulative iteration time: 26.0
Weighted Recall: 0.5555555555555556
Weighted FalsePositiveRate: 0.04444444444444445
Kappa statistic: 0.5111111111111111
Training time: 1.0
		
Time end:Wed Nov 01 14.59.39 EET 2017