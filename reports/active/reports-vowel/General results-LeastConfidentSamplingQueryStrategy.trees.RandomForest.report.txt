Wed Nov 01 14.56.29 EET 2017
Dataset: vowel
Test set size: 495
Initial Labelled set size: 99
Initial Unlabelled set size: 396
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Wed Nov 01 14.56.29 EET 2017
		
Iteration: 1
Labeled set size: 99
Unlabelled set size: 396
	
Mean region size: 43.305785123967084
Incorrectly Classified Instances: 63.39393939393939
Correctly Classified Instances: 36.60606060606061
Weighted Precision: 0.3993317446955009
Weighted AreaUnderROC: 0.7632552188552189
Root mean squared error: 0.2707779589785887
Relative absolute error: 80.4947323714975
Root relative squared error: 94.1902599598376
Weighted TruePositiveRate: 0.36606060606060603
Weighted MatthewsCorrelation: 0.3127733864157014
Weighted FMeasure: 0.36503217212532063
Iteration time: 15.3
Weighted AreaUnderPRC: 0.39142429984086957
Mean absolute error: 0.1330491444156992
Coverage of cases: 77.13131313131314
Instances selection time: 2.4
Test time: 5.0
Accumulative iteration time: 15.3
Weighted Recall: 0.36606060606060603
Weighted FalsePositiveRate: 0.06339393939393939
Kappa statistic: 0.30266666666666675
Training time: 12.9
		
Iteration: 2
Labeled set size: 119
Unlabelled set size: 376
	
Mean region size: 42.43526170798911
Incorrectly Classified Instances: 60.4040404040404
Correctly Classified Instances: 39.59595959595959
Weighted Precision: 0.427595297961215
Weighted AreaUnderROC: 0.7786909090909091
Root mean squared error: 0.2658171196532486
Relative absolute error: 78.25530400297801
Root relative squared error: 92.46462930867474
Weighted TruePositiveRate: 0.39595959595959596
Weighted MatthewsCorrelation: 0.3447708195446916
Weighted FMeasure: 0.3951957457211124
Iteration time: 16.1
Weighted AreaUnderPRC: 0.4236255615149186
Mean absolute error: 0.12934760992227856
Coverage of cases: 78.82828282828284
Instances selection time: 2.0
Test time: 5.9
Accumulative iteration time: 31.4
Weighted Recall: 0.39595959595959596
Weighted FalsePositiveRate: 0.06040404040404039
Kappa statistic: 0.33555555555555555
Training time: 14.1
		
Iteration: 3
Labeled set size: 139
Unlabelled set size: 356
	
Mean region size: 40.60606060606075
Incorrectly Classified Instances: 56.141414141414145
Correctly Classified Instances: 43.858585858585855
Weighted Precision: 0.4685721059178832
Weighted AreaUnderROC: 0.7920401795735129
Root mean squared error: 0.26040401995103585
Relative absolute error: 75.90525223763385
Root relative squared error: 90.58167964001191
Weighted TruePositiveRate: 0.4385858585858585
Weighted MatthewsCorrelation: 0.39191045936629026
Weighted FMeasure: 0.438651753075393
Iteration time: 21.9
Weighted AreaUnderPRC: 0.45429067831645353
Mean absolute error: 0.12546322683906502
Coverage of cases: 78.12121212121212
Instances selection time: 2.1
Test time: 5.0
Accumulative iteration time: 53.3
Weighted Recall: 0.4385858585858585
Weighted FalsePositiveRate: 0.05614141414141414
Kappa statistic: 0.3824444444444445
Training time: 19.8
		
Iteration: 4
Labeled set size: 159
Unlabelled set size: 336
	
Mean region size: 40.39853076216726
Incorrectly Classified Instances: 50.121212121212125
Correctly Classified Instances: 49.878787878787875
Weighted Precision: 0.5234157237272166
Weighted AreaUnderROC: 0.8272143658810325
Root mean squared error: 0.24886632230582711
Relative absolute error: 72.03990477899922
Root relative squared error: 86.56828525355758
Weighted TruePositiveRate: 0.4987878787878787
Weighted MatthewsCorrelation: 0.4555119920841655
Weighted FMeasure: 0.49643411026638445
Iteration time: 21.7
Weighted AreaUnderPRC: 0.5258648577019176
Mean absolute error: 0.11907422277520607
Coverage of cases: 81.95959595959596
Instances selection time: 1.7
Test time: 5.3
Accumulative iteration time: 75.0
Weighted Recall: 0.4987878787878787
Weighted FalsePositiveRate: 0.05012121212121212
Kappa statistic: 0.44866666666666666
Training time: 20.0
		
Iteration: 5
Labeled set size: 179
Unlabelled set size: 316
	
Mean region size: 39.16620752984404
Incorrectly Classified Instances: 46.161616161616166
Correctly Classified Instances: 53.838383838383834
Weighted Precision: 0.5577801084026752
Weighted AreaUnderROC: 0.8454826038159373
Root mean squared error: 0.2427712009798082
Relative absolute error: 69.64381501866225
Root relative squared error: 84.4480939929762
Weighted TruePositiveRate: 0.5383838383838384
Weighted MatthewsCorrelation: 0.4980521020183075
Weighted FMeasure: 0.5377071469294233
Iteration time: 25.1
Weighted AreaUnderPRC: 0.5578619002675584
Mean absolute error: 0.11511374383250031
Coverage of cases: 84.00000000000003
Instances selection time: 1.6
Test time: 5.0
Accumulative iteration time: 100.1
Weighted Recall: 0.5383838383838384
Weighted FalsePositiveRate: 0.04616161616161617
Kappa statistic: 0.49222222222222217
Training time: 23.5
		
Iteration: 6
Labeled set size: 199
Unlabelled set size: 296
	
Mean region size: 38.70156106519758
Incorrectly Classified Instances: 42.86868686868687
Correctly Classified Instances: 57.13131313131314
Weighted Precision: 0.5900763388748729
Weighted AreaUnderROC: 0.8573375982042648
Root mean squared error: 0.23708863760052376
Relative absolute error: 67.453887754735
Root relative squared error: 82.47141123802797
Weighted TruePositiveRate: 0.5713131313131312
Weighted MatthewsCorrelation: 0.5336124460849593
Weighted FMeasure: 0.5695127142030456
Iteration time: 27.1
Weighted AreaUnderPRC: 0.5878277256943847
Mean absolute error: 0.11149402934667015
Coverage of cases: 84.58585858585859
Instances selection time: 1.6
Test time: 5.0
Accumulative iteration time: 127.2
Weighted Recall: 0.5713131313131312
Weighted FalsePositiveRate: 0.04286868686868687
Kappa statistic: 0.5284444444444445
Training time: 25.5
		
Iteration: 7
Labeled set size: 219
Unlabelled set size: 276
	
Mean region size: 37.95775941230499
Incorrectly Classified Instances: 39.31313131313131
Correctly Classified Instances: 60.686868686868685
Weighted Precision: 0.6300543668588043
Weighted AreaUnderROC: 0.8748951739618406
Root mean squared error: 0.22898679746809938
Relative absolute error: 64.59178897935871
Root relative squared error: 79.65318175175564
Weighted TruePositiveRate: 0.606868686868687
Weighted MatthewsCorrelation: 0.5748446459184204
Weighted FMeasure: 0.6071815747620111
Iteration time: 30.0
Weighted AreaUnderPRC: 0.6324126193004044
Mean absolute error: 0.10676328756918863
Coverage of cases: 86.82828282828282
Instances selection time: 1.6
Test time: 5.0
Accumulative iteration time: 157.2
Weighted Recall: 0.606868686868687
Weighted FalsePositiveRate: 0.039313131313131314
Kappa statistic: 0.5675555555555556
Training time: 28.4
		
Iteration: 8
Labeled set size: 239
Unlabelled set size: 256
	
Mean region size: 37.80899908172647
Incorrectly Classified Instances: 36.64646464646465
Correctly Classified Instances: 63.35353535353536
Weighted Precision: 0.6488053497415402
Weighted AreaUnderROC: 0.8940895622895623
Root mean squared error: 0.2230785746333525
Relative absolute error: 62.78894185878555
Root relative squared error: 77.59800323277781
Weighted TruePositiveRate: 0.6335353535353537
Weighted MatthewsCorrelation: 0.6011758560769305
Weighted FMeasure: 0.6322594881619253
Iteration time: 33.9
Weighted AreaUnderPRC: 0.6627975150574004
Mean absolute error: 0.10378337497319992
Coverage of cases: 89.55555555555556
Instances selection time: 1.5
Test time: 5.1
Accumulative iteration time: 191.1
Weighted Recall: 0.6335353535353537
Weighted FalsePositiveRate: 0.03664646464646465
Kappa statistic: 0.5968888888888889
Training time: 32.4
		
Iteration: 9
Labeled set size: 259
Unlabelled set size: 236
	
Mean region size: 37.10743801652903
Incorrectly Classified Instances: 32.868686868686865
Correctly Classified Instances: 67.13131313131312
Weighted Precision: 0.684760912779489
Weighted AreaUnderROC: 0.9154796857463523
Root mean squared error: 0.21389982264218563
Relative absolute error: 59.77003122404727
Root relative squared error: 74.4051693720896
Weighted TruePositiveRate: 0.6713131313131313
Weighted MatthewsCorrelation: 0.6412907916717079
Weighted FMeasure: 0.6680289932842574
Iteration time: 37.6
Weighted AreaUnderPRC: 0.7093753687496753
Mean absolute error: 0.09879344003974819
Coverage of cases: 91.31313131313132
Instances selection time: 1.3
Test time: 5.3
Accumulative iteration time: 228.7
Weighted Recall: 0.6713131313131313
Weighted FalsePositiveRate: 0.03286868686868687
Kappa statistic: 0.6384444444444445
Training time: 36.3
		
Iteration: 10
Labeled set size: 279
Unlabelled set size: 216
	
Mean region size: 36.34710743801666
Incorrectly Classified Instances: 30.383838383838384
Correctly Classified Instances: 69.61616161616162
Weighted Precision: 0.714192795225993
Weighted AreaUnderROC: 0.9211319865319865
Root mean squared error: 0.20902447005352287
Relative absolute error: 58.01626328705121
Root relative squared error: 72.70927532866662
Weighted TruePositiveRate: 0.6961616161616162
Weighted MatthewsCorrelation: 0.6708443338987036
Weighted FMeasure: 0.6950525231404998
Iteration time: 40.3
Weighted AreaUnderPRC: 0.7348159420326946
Mean absolute error: 0.09589465006124231
Coverage of cases: 91.95959595959596
Instances selection time: 1.2
Test time: 5.4
Accumulative iteration time: 269.0
Weighted Recall: 0.6961616161616162
Weighted FalsePositiveRate: 0.030383838383838385
Kappa statistic: 0.6657777777777778
Training time: 39.1
		
Iteration: 11
Labeled set size: 299
Unlabelled set size: 196
	
Mean region size: 36.24793388429766
Incorrectly Classified Instances: 27.67676767676768
Correctly Classified Instances: 72.32323232323233
Weighted Precision: 0.7380706012803928
Weighted AreaUnderROC: 0.9397506172839506
Root mean squared error: 0.20251085608505134
Relative absolute error: 56.2307626818102
Root relative squared error: 70.44351117532628
Weighted TruePositiveRate: 0.7232323232323232
Weighted MatthewsCorrelation: 0.6996634964243515
Weighted FMeasure: 0.722068219626922
Iteration time: 44.2
Weighted AreaUnderPRC: 0.7677214802713067
Mean absolute error: 0.0929434093914224
Coverage of cases: 94.42424242424242
Instances selection time: 1.2
Test time: 5.2
Accumulative iteration time: 313.2
Weighted Recall: 0.7232323232323232
Weighted FalsePositiveRate: 0.027676767676767678
Kappa statistic: 0.6955555555555556
Training time: 43.0
		
Iteration: 12
Labeled set size: 319
Unlabelled set size: 176
	
Mean region size: 35.184573002754924
Incorrectly Classified Instances: 24.54545454545454
Correctly Classified Instances: 75.45454545454545
Weighted Precision: 0.7641258390291144
Weighted AreaUnderROC: 0.9478136924803591
Root mean squared error: 0.1952438880270642
Relative absolute error: 53.78753854453767
Root relative squared error: 67.91569239316395
Weighted TruePositiveRate: 0.7545454545454546
Weighted MatthewsCorrelation: 0.7329183991022287
Weighted FMeasure: 0.7545065969578072
Iteration time: 47.3
Weighted AreaUnderPRC: 0.7985415172910522
Mean absolute error: 0.08890502238766611
Coverage of cases: 95.19191919191918
Instances selection time: 1.2
Test time: 5.4
Accumulative iteration time: 360.5
Weighted Recall: 0.7545454545454546
Weighted FalsePositiveRate: 0.024545454545454544
Kappa statistic: 0.73
Training time: 46.1
		
Iteration: 13
Labeled set size: 339
Unlabelled set size: 156
	
Mean region size: 34.470156106519845
Incorrectly Classified Instances: 22.68686868686869
Correctly Classified Instances: 77.31313131313131
Weighted Precision: 0.7832474561790438
Weighted AreaUnderROC: 0.9589223344556679
Root mean squared error: 0.18937787838861309
Relative absolute error: 51.770071688481906
Root relative squared error: 65.87519775742327
Weighted TruePositiveRate: 0.7731313131313131
Weighted MatthewsCorrelation: 0.7533056723824499
Weighted FMeasure: 0.7724705927558048
Iteration time: 50.5
Weighted AreaUnderPRC: 0.8202229926370104
Mean absolute error: 0.08557036642724337
Coverage of cases: 96.6060606060606
Instances selection time: 1.5
Test time: 5.3
Accumulative iteration time: 411.0
Weighted Recall: 0.7731313131313131
Weighted FalsePositiveRate: 0.02268686868686869
Kappa statistic: 0.7504444444444445
Training time: 49.0
		
Iteration: 14
Labeled set size: 359
Unlabelled set size: 136
	
Mean region size: 33.54637281910018
Incorrectly Classified Instances: 18.98989898989899
Correctly Classified Instances: 81.01010101010101
Weighted Precision: 0.8167916707991255
Weighted AreaUnderROC: 0.9636826038159372
Root mean squared error: 0.1814257454636676
Relative absolute error: 49.14814446146759
Root relative squared error: 63.10904400450637
Weighted TruePositiveRate: 0.81010101010101
Weighted MatthewsCorrelation: 0.7927632471621378
Weighted FMeasure: 0.809051521356365
Iteration time: 53.9
Weighted AreaUnderPRC: 0.851581511553543
Mean absolute error: 0.08123660241564942
Coverage of cases: 96.68686868686868
Instances selection time: 1.2
Test time: 5.8
Accumulative iteration time: 464.9
Weighted Recall: 0.81010101010101
Weighted FalsePositiveRate: 0.01898989898989899
Kappa statistic: 0.7911111111111111
Training time: 52.7
		
Iteration: 15
Labeled set size: 379
Unlabelled set size: 116
	
Mean region size: 33.98530762167135
Incorrectly Classified Instances: 19.03030303030303
Correctly Classified Instances: 80.96969696969697
Weighted Precision: 0.8157087919137187
Weighted AreaUnderROC: 0.9707250280583614
Root mean squared error: 0.1803566566483155
Relative absolute error: 49.11677066398566
Root relative squared error: 62.73716087997875
Weighted TruePositiveRate: 0.8096969696969698
Weighted MatthewsCorrelation: 0.7923185743123042
Weighted FMeasure: 0.8091745024200232
Iteration time: 60.6
Weighted AreaUnderPRC: 0.8575079306501772
Mean absolute error: 0.08118474489915037
Coverage of cases: 97.85858585858585
Instances selection time: 0.9
Test time: 5.6
Accumulative iteration time: 525.5
Weighted Recall: 0.8096969696969698
Weighted FalsePositiveRate: 0.019030303030303033
Kappa statistic: 0.7906666666666665
Training time: 59.7
		
Iteration: 16
Labeled set size: 399
Unlabelled set size: 96
	
Mean region size: 33.471074380165376
Incorrectly Classified Instances: 17.7979797979798
Correctly Classified Instances: 82.20202020202022
Weighted Precision: 0.8298231048060833
Weighted AreaUnderROC: 0.9747712682379349
Root mean squared error: 0.1747222607901257
Relative absolute error: 47.24601128114317
Root relative squared error: 60.777233223379895
Weighted TruePositiveRate: 0.822020202020202
Weighted MatthewsCorrelation: 0.806505449682135
Weighted FMeasure: 0.8217692174464111
Iteration time: 61.5
Weighted AreaUnderPRC: 0.8737496808532385
Mean absolute error: 0.07809258062998922
Coverage of cases: 98.3030303030303
Instances selection time: 0.8
Test time: 5.5
Accumulative iteration time: 587.0
Weighted Recall: 0.822020202020202
Weighted FalsePositiveRate: 0.017797979797979802
Kappa statistic: 0.8042222222222222
Training time: 60.7
		
Iteration: 17
Labeled set size: 419
Unlabelled set size: 76
	
Mean region size: 32.38567493112956
Incorrectly Classified Instances: 15.959595959595958
Correctly Classified Instances: 84.04040404040403
Weighted Precision: 0.8460142212545595
Weighted AreaUnderROC: 0.9754574635241303
Root mean squared error: 0.17100311928105058
Relative absolute error: 45.702981696748076
Root relative squared error: 59.483527831373046
Weighted TruePositiveRate: 0.8404040404040405
Weighted MatthewsCorrelation: 0.8261294244912637
Weighted FMeasure: 0.8403171322816458
Iteration time: 64.4
Weighted AreaUnderPRC: 0.8822940045428431
Mean absolute error: 0.07554211850702208
Coverage of cases: 98.08080808080808
Instances selection time: 0.8
Test time: 5.6
Accumulative iteration time: 651.4
Weighted Recall: 0.8404040404040405
Weighted FalsePositiveRate: 0.01595959595959596
Kappa statistic: 0.8244444444444445
Training time: 63.6
		
Iteration: 18
Labeled set size: 439
Unlabelled set size: 56
	
Mean region size: 32.12672176308547
Incorrectly Classified Instances: 15.232323232323234
Correctly Classified Instances: 84.76767676767676
Weighted Precision: 0.8535821443364282
Weighted AreaUnderROC: 0.9745164983164983
Root mean squared error: 0.16896611797944389
Relative absolute error: 44.94766733282616
Root relative squared error: 58.77495582329453
Weighted TruePositiveRate: 0.8476767676767676
Weighted MatthewsCorrelation: 0.8342778075288079
Weighted FMeasure: 0.8477531583195702
Iteration time: 69.2
Weighted AreaUnderPRC: 0.8870423112108382
Mean absolute error: 0.07429366501293629
Coverage of cases: 97.83838383838383
Instances selection time: 0.7
Test time: 5.4
Accumulative iteration time: 720.6
Weighted Recall: 0.8476767676767676
Weighted FalsePositiveRate: 0.015232323232323236
Kappa statistic: 0.8324444444444443
Training time: 68.5
		
Iteration: 19
Labeled set size: 459
Unlabelled set size: 36
	
Mean region size: 31.968778696051494
Incorrectly Classified Instances: 16.525252525252522
Correctly Classified Instances: 83.47474747474746
Weighted Precision: 0.8408231491742526
Weighted AreaUnderROC: 0.9761396184062849
Root mean squared error: 0.1705775918348206
Relative absolute error: 45.267209948305954
Root relative squared error: 59.33550787830287
Weighted TruePositiveRate: 0.8347474747474747
Weighted MatthewsCorrelation: 0.8200505841762211
Weighted FMeasure: 0.8346548523783299
Iteration time: 68.7
Weighted AreaUnderPRC: 0.8832970212639916
Mean absolute error: 0.07482183462529957
Coverage of cases: 98.22222222222221
Instances selection time: 0.8
Test time: 5.5
Accumulative iteration time: 789.3
Weighted Recall: 0.8347474747474747
Weighted FalsePositiveRate: 0.016525252525252523
Kappa statistic: 0.8182222222222222
Training time: 67.9
		
Iteration: 20
Labeled set size: 479
Unlabelled set size: 16
	
Mean region size: 31.51331496786048
Incorrectly Classified Instances: 15.414141414141412
Correctly Classified Instances: 84.58585858585859
Weighted Precision: 0.8502697974828892
Weighted AreaUnderROC: 0.9778282828282828
Root mean squared error: 0.16707596389458587
Relative absolute error: 43.94241239358012
Root relative squared error: 58.11746469924405
Weighted TruePositiveRate: 0.845858585858586
Weighted MatthewsCorrelation: 0.8317456955593354
Weighted FMeasure: 0.8457246822177217
Iteration time: 72.0
Weighted AreaUnderPRC: 0.8891812635239471
Mean absolute error: 0.07263208660095934
Coverage of cases: 98.40404040404039
Instances selection time: 0.4
Test time: 5.6
Accumulative iteration time: 861.3
Weighted Recall: 0.845858585858586
Weighted FalsePositiveRate: 0.015414141414141413
Kappa statistic: 0.8304444444444445
Training time: 71.6
		
Time end:Wed Nov 01 14.56.48 EET 2017