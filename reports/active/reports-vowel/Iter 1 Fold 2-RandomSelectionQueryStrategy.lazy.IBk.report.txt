Wed Nov 01 15.01.26 EET 2017
Dataset: vowel
Test set size: 495
Initial Labelled set size: 99
Initial Unlabelled set size: 396
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Wed Nov 01 15.01.26 EET 2017
		
Iteration: 1
Labeled set size: 99
Unlabelled set size: 396
	
Mean region size: 54.545454545454064
Incorrectly Classified Instances: 61.41414141414141
Correctly Classified Instances: 38.58585858585859
Weighted Precision: 0.4169439407683922
Weighted AreaUnderROC: 0.6622222222222223
Root mean squared error: 0.31831166181839166
Relative absolute error: 70.80000000000042
Root relative squared error: 110.72488428533056
Weighted TruePositiveRate: 0.38585858585858585
Weighted MatthewsCorrelation: 0.3357719501096009
Weighted FMeasure: 0.3892152585966697
Iteration time: 1.0
Weighted AreaUnderPRC: 0.2230939966415411
Mean absolute error: 0.1170247933884312
Coverage of cases: 70.70707070707071
Instances selection time: 1.0
Test time: 10.0
Accumulative iteration time: 1.0
Weighted Recall: 0.38585858585858585
Weighted FalsePositiveRate: 0.06141414141414143
Kappa statistic: 0.3244444444444445
Training time: 0.0
		
Iteration: 2
Labeled set size: 119
Unlabelled set size: 376
	
Mean region size: 45.45454545454594
Incorrectly Classified Instances: 55.95959595959596
Correctly Classified Instances: 44.04040404040404
Weighted Precision: 0.4654064062781948
Weighted AreaUnderROC: 0.6922222222222223
Root mean squared error: 0.30614909324331
Relative absolute error: 64.80854700854663
Root relative squared error: 106.49412820685345
Weighted TruePositiveRate: 0.4404040404040404
Weighted MatthewsCorrelation: 0.390985110871562
Weighted FMeasure: 0.43682730793727886
Iteration time: 1.0
Weighted AreaUnderPRC: 0.2620171370477865
Mean absolute error: 0.10712156530338353
Coverage of cases: 68.48484848484848
Instances selection time: 1.0
Test time: 10.0
Accumulative iteration time: 2.0
Weighted Recall: 0.4404040404040404
Weighted FalsePositiveRate: 0.055959595959595966
Kappa statistic: 0.3844444444444445
Training time: 0.0
		
Iteration: 3
Labeled set size: 139
Unlabelled set size: 356
	
Mean region size: 36.36363636363658
Incorrectly Classified Instances: 51.111111111111114
Correctly Classified Instances: 48.888888888888886
Weighted Precision: 0.5215548482100479
Weighted AreaUnderROC: 0.7188888888888889
Root mean squared error: 0.2942089776993943
Relative absolute error: 59.4325925925924
Root relative squared error: 102.34075253597453
Weighted TruePositiveRate: 0.4888888888888889
Weighted MatthewsCorrelation: 0.4474179436031342
Weighted FMeasure: 0.487251267992322
Iteration time: 1.0
Weighted AreaUnderPRC: 0.3020286905913625
Mean absolute error: 0.09823569023569055
Coverage of cases: 66.46464646464646
Instances selection time: 1.0
Test time: 11.0
Accumulative iteration time: 3.0
Weighted Recall: 0.4888888888888889
Weighted FalsePositiveRate: 0.051111111111111114
Kappa statistic: 0.4377777777777778
Training time: 0.0
		
Iteration: 4
Labeled set size: 159
Unlabelled set size: 336
	
Mean region size: 27.272727272727032
Incorrectly Classified Instances: 46.666666666666664
Correctly Classified Instances: 53.333333333333336
Weighted Precision: 0.5639765871838932
Weighted AreaUnderROC: 0.7433333333333333
Root mean squared error: 0.28231951673821615
Relative absolute error: 54.48235294117564
Root relative squared error: 98.204997089187
Weighted TruePositiveRate: 0.5333333333333333
Weighted MatthewsCorrelation: 0.4942465012002776
Weighted FMeasure: 0.5284685704865187
Iteration time: 0.0
Weighted AreaUnderPRC: 0.3429068237973596
Mean absolute error: 0.09005347593582809
Coverage of cases: 65.65656565656566
Instances selection time: 0.0
Test time: 13.0
Accumulative iteration time: 3.0
Weighted Recall: 0.5333333333333333
Weighted FalsePositiveRate: 0.04666666666666666
Kappa statistic: 0.48666666666666664
Training time: 0.0
		
Iteration: 5
Labeled set size: 179
Unlabelled set size: 316
	
Mean region size: 18.18181818181829
Incorrectly Classified Instances: 41.61616161616162
Correctly Classified Instances: 58.38383838383838
Weighted Precision: 0.6044120329807366
Weighted AreaUnderROC: 0.7711111111111111
Root mean squared error: 0.26751094951813786
Relative absolute error: 48.91695906432726
Root relative squared error: 93.05382894627994
Weighted TruePositiveRate: 0.5838383838383838
Weighted MatthewsCorrelation: 0.5470763389621177
Weighted FMeasure: 0.5798438683605374
Iteration time: 1.0
Weighted AreaUnderPRC: 0.3952577544791426
Mean absolute error: 0.08085447779227697
Coverage of cases: 62.22222222222222
Instances selection time: 1.0
Test time: 12.0
Accumulative iteration time: 4.0
Weighted Recall: 0.5838383838383838
Weighted FalsePositiveRate: 0.04161616161616162
Kappa statistic: 0.5422222222222222
Training time: 0.0
		
Iteration: 6
Labeled set size: 199
Unlabelled set size: 296
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 38.98989898989899
Correctly Classified Instances: 61.01010101010101
Weighted Precision: 0.6370865911404332
Weighted AreaUnderROC: 0.7855555555555556
Root mean squared error: 0.259623109704184
Relative absolute error: 45.880423280423315
Root relative squared error: 90.3100395869081
Weighted TruePositiveRate: 0.6101010101010101
Weighted MatthewsCorrelation: 0.5780272931399237
Weighted FMeasure: 0.6066845697616827
Iteration time: 0.0
Weighted AreaUnderPRC: 0.4271707857055688
Mean absolute error: 0.07583541038086547
Coverage of cases: 61.01010101010101
Instances selection time: 0.0
Test time: 15.0
Accumulative iteration time: 4.0
Weighted Recall: 0.6101010101010101
Weighted FalsePositiveRate: 0.038989898989898984
Kappa statistic: 0.5711111111111111
Training time: 0.0
		
Iteration: 7
Labeled set size: 219
Unlabelled set size: 276
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 36.56565656565657
Correctly Classified Instances: 63.43434343434343
Weighted Precision: 0.6626373020065883
Weighted AreaUnderROC: 0.798888888888889
Root mean squared error: 0.251977033763645
Relative absolute error: 43.08115942028973
Root relative squared error: 87.6503479220892
Weighted TruePositiveRate: 0.6343434343434343
Weighted MatthewsCorrelation: 0.6052743984211326
Weighted FMeasure: 0.6315540347233248
Iteration time: 1.0
Weighted AreaUnderPRC: 0.4538976859726221
Mean absolute error: 0.0712085279674215
Coverage of cases: 63.43434343434343
Instances selection time: 1.0
Test time: 16.0
Accumulative iteration time: 5.0
Weighted Recall: 0.6343434343434343
Weighted FalsePositiveRate: 0.036565656565656565
Kappa statistic: 0.5977777777777777
Training time: 0.0
		
Iteration: 8
Labeled set size: 239
Unlabelled set size: 256
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 32.323232323232325
Correctly Classified Instances: 67.67676767676768
Weighted Precision: 0.7088898258532126
Weighted AreaUnderROC: 0.8222222222222223
Root mean squared error: 0.2373681839023707
Relative absolute error: 38.39111111111092
Root relative squared error: 82.56865157081243
Weighted TruePositiveRate: 0.6767676767676768
Weighted MatthewsCorrelation: 0.6535832305602525
Weighted FMeasure: 0.6753102980745046
Iteration time: 1.0
Weighted AreaUnderPRC: 0.5066868005193401
Mean absolute error: 0.06345638200183663
Coverage of cases: 67.67676767676768
Instances selection time: 1.0
Test time: 17.0
Accumulative iteration time: 6.0
Weighted Recall: 0.6767676767676768
Weighted FalsePositiveRate: 0.03232323232323233
Kappa statistic: 0.6444444444444445
Training time: 0.0
		
Iteration: 9
Labeled set size: 259
Unlabelled set size: 236
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 28.484848484848484
Correctly Classified Instances: 71.51515151515152
Weighted Precision: 0.741308802260284
Weighted AreaUnderROC: 0.8433333333333333
Root mean squared error: 0.22319905699488946
Relative absolute error: 34.13086419753066
Root relative squared error: 77.6399130876144
Weighted TruePositiveRate: 0.7151515151515152
Weighted MatthewsCorrelation: 0.6943577308514339
Weighted FMeasure: 0.7147558205744388
Iteration time: 1.0
Weighted AreaUnderPRC: 0.5573886001083107
Mean absolute error: 0.05641465156616674
Coverage of cases: 71.51515151515152
Instances selection time: 1.0
Test time: 18.0
Accumulative iteration time: 7.0
Weighted Recall: 0.7151515151515152
Weighted FalsePositiveRate: 0.028484848484848488
Kappa statistic: 0.6866666666666668
Training time: 0.0
		
Iteration: 10
Labeled set size: 279
Unlabelled set size: 216
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 24.242424242424242
Correctly Classified Instances: 75.75757575757575
Weighted Precision: 0.7716437573516879
Weighted AreaUnderROC: 0.8666666666666666
Root mean squared error: 0.20621384464524015
Relative absolute error: 29.448275862068677
Root relative squared error: 71.73159775529808
Weighted TruePositiveRate: 0.7575757575757576
Weighted MatthewsCorrelation: 0.7363045879571612
Weighted FMeasure: 0.7544112086441154
Iteration time: 0.0
Weighted AreaUnderPRC: 0.611954548049853
Mean absolute error: 0.04867483613565102
Coverage of cases: 75.75757575757575
Instances selection time: 0.0
Test time: 18.0
Accumulative iteration time: 7.0
Weighted Recall: 0.7575757575757576
Weighted FalsePositiveRate: 0.024242424242424242
Kappa statistic: 0.7333333333333333
Training time: 0.0
		
Iteration: 11
Labeled set size: 299
Unlabelled set size: 196
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 22.626262626262626
Correctly Classified Instances: 77.37373737373737
Weighted Precision: 0.785370948009607
Weighted AreaUnderROC: 0.8755555555555556
Root mean squared error: 0.19945665873566795
Relative absolute error: 27.55412186379945
Root relative squared error: 69.38110697007919
Weighted TruePositiveRate: 0.7737373737373737
Weighted MatthewsCorrelation: 0.7529156717734777
Weighted FMeasure: 0.7694873972714593
Iteration time: 1.0
Weighted AreaUnderPRC: 0.6353323300417183
Mean absolute error: 0.04554400308066053
Coverage of cases: 77.37373737373737
Instances selection time: 1.0
Test time: 18.0
Accumulative iteration time: 8.0
Weighted Recall: 0.7737373737373737
Weighted FalsePositiveRate: 0.022626262626262633
Kappa statistic: 0.7511111111111111
Training time: 0.0
		
Iteration: 12
Labeled set size: 319
Unlabelled set size: 176
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 19.595959595959595
Correctly Classified Instances: 80.4040404040404
Weighted Precision: 0.8115311300014367
Weighted AreaUnderROC: 0.8922222222222222
Root mean squared error: 0.18583113543442384
Relative absolute error: 24.17037037037013
Root relative squared error: 64.64146129628013
Weighted TruePositiveRate: 0.804040404040404
Weighted MatthewsCorrelation: 0.7854904130998637
Weighted FMeasure: 0.8008873113130938
Iteration time: 0.0
Weighted AreaUnderPRC: 0.6809022721969527
Mean absolute error: 0.03995102540557072
Coverage of cases: 80.4040404040404
Instances selection time: 0.0
Test time: 19.0
Accumulative iteration time: 8.0
Weighted Recall: 0.804040404040404
Weighted FalsePositiveRate: 0.019595959595959597
Kappa statistic: 0.7844444444444444
Training time: 0.0
		
Iteration: 13
Labeled set size: 339
Unlabelled set size: 156
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 16.767676767676768
Correctly Classified Instances: 83.23232323232324
Weighted Precision: 0.8370438707194882
Weighted AreaUnderROC: 0.9077777777777777
Root mean squared error: 0.17207604344314578
Relative absolute error: 21.007619047618682
Root relative squared error: 59.85674508334638
Weighted TruePositiveRate: 0.8323232323232324
Weighted MatthewsCorrelation: 0.8162302128209865
Weighted FMeasure: 0.830360054603336
Iteration time: 0.0
Weighted AreaUnderPRC: 0.7186779423911064
Mean absolute error: 0.034723337268791434
Coverage of cases: 83.23232323232324
Instances selection time: 0.0
Test time: 20.0
Accumulative iteration time: 8.0
Weighted Recall: 0.8323232323232324
Weighted FalsePositiveRate: 0.016767676767676765
Kappa statistic: 0.8155555555555556
Training time: 0.0
		
Iteration: 14
Labeled set size: 359
Unlabelled set size: 136
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 14.343434343434344
Correctly Classified Instances: 85.65656565656566
Weighted Precision: 0.8629839230514111
Weighted AreaUnderROC: 0.9211111111111111
Root mean squared error: 0.15930064901684507
Relative absolute error: 18.281681681681473
Root relative squared error: 55.412817199992055
Weighted TruePositiveRate: 0.8565656565656565
Weighted MatthewsCorrelation: 0.8436003221617658
Weighted FMeasure: 0.8551147047264289
Iteration time: 0.0
Weighted AreaUnderPRC: 0.754883454869789
Mean absolute error: 0.030217655672200975
Coverage of cases: 85.65656565656566
Instances selection time: 0.0
Test time: 21.0
Accumulative iteration time: 8.0
Weighted Recall: 0.8565656565656565
Weighted FalsePositiveRate: 0.014343434343434342
Kappa statistic: 0.8422222222222222
Training time: 0.0
		
Iteration: 15
Labeled set size: 379
Unlabelled set size: 116
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 12.323232323232324
Correctly Classified Instances: 87.67676767676768
Weighted Precision: 0.8789769501608361
Weighted AreaUnderROC: 0.9322222222222223
Root mean squared error: 0.14778248387059634
Relative absolute error: 15.993732193731867
Root relative squared error: 51.40621720389971
Weighted TruePositiveRate: 0.8767676767676768
Weighted MatthewsCorrelation: 0.8644099005201678
Weighted FMeasure: 0.8749732531626199
Iteration time: 1.0
Weighted AreaUnderPRC: 0.7858138900478933
Mean absolute error: 0.02643592098137516
Coverage of cases: 87.67676767676768
Instances selection time: 1.0
Test time: 21.0
Accumulative iteration time: 9.0
Weighted Recall: 0.8767676767676768
Weighted FalsePositiveRate: 0.012323232323232321
Kappa statistic: 0.8644444444444445
Training time: 0.0
		
Iteration: 16
Labeled set size: 399
Unlabelled set size: 96
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 11.313131313131313
Correctly Classified Instances: 88.68686868686869
Weighted Precision: 0.8880676469874196
Weighted AreaUnderROC: 0.9377777777777776
Root mean squared error: 0.14169318260221786
Relative absolute error: 14.793495934959198
Root relative squared error: 49.28805045352661
Weighted TruePositiveRate: 0.8868686868686869
Weighted MatthewsCorrelation: 0.8755254143301633
Weighted FMeasure: 0.885832526190928
Iteration time: 1.0
Weighted AreaUnderPRC: 0.8026833578958694
Mean absolute error: 0.02445205939662693
Coverage of cases: 88.68686868686869
Instances selection time: 1.0
Test time: 59.0
Accumulative iteration time: 10.0
Weighted Recall: 0.8868686868686869
Weighted FalsePositiveRate: 0.011313131313131315
Kappa statistic: 0.8755555555555556
Training time: 0.0
		
Iteration: 17
Labeled set size: 419
Unlabelled set size: 76
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 9.292929292929292
Correctly Classified Instances: 90.70707070707071
Weighted Precision: 0.9109346412028859
Weighted AreaUnderROC: 0.9488888888888888
Root mean squared error: 0.12852271821307917
Relative absolute error: 12.518863049095504
Root relative squared error: 44.706697269226815
Weighted TruePositiveRate: 0.907070707070707
Weighted MatthewsCorrelation: 0.8991420244496379
Weighted FMeasure: 0.907555663283336
Iteration time: 1.0
Weighted AreaUnderPRC: 0.8362901271213561
Mean absolute error: 0.02069233561833981
Coverage of cases: 90.70707070707071
Instances selection time: 1.0
Test time: 61.0
Accumulative iteration time: 11.0
Weighted Recall: 0.907070707070707
Weighted FalsePositiveRate: 0.009292929292929296
Kappa statistic: 0.8977777777777777
Training time: 0.0
		
Iteration: 18
Labeled set size: 439
Unlabelled set size: 56
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 9.090909090909092
Correctly Classified Instances: 90.9090909090909
Weighted Precision: 0.9126033664188663
Weighted AreaUnderROC: 0.9499999999999998
Root mean squared error: 0.12717809533030391
Relative absolute error: 12.19999999999987
Root relative squared error: 44.23896946983616
Weighted TruePositiveRate: 0.9090909090909091
Weighted MatthewsCorrelation: 0.9011628136154146
Weighted FMeasure: 0.909329294563237
Iteration time: 1.0
Weighted AreaUnderPRC: 0.8387908916471771
Mean absolute error: 0.020165289256198263
Coverage of cases: 90.9090909090909
Instances selection time: 1.0
Test time: 39.0
Accumulative iteration time: 12.0
Weighted Recall: 0.9090909090909091
Weighted FalsePositiveRate: 0.009090909090909092
Kappa statistic: 0.8999999999999999
Training time: 0.0
		
Iteration: 19
Labeled set size: 459
Unlabelled set size: 36
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 7.6767676767676765
Correctly Classified Instances: 92.32323232323232
Weighted Precision: 0.9277760465999969
Weighted AreaUnderROC: 0.9577777777777776
Root mean squared error: 0.11694596102467221
Relative absolute error: 10.587234042553142
Root relative squared error: 40.67971599947659
Weighted TruePositiveRate: 0.9232323232323232
Weighted MatthewsCorrelation: 0.9171187625028103
Weighted FMeasure: 0.9236947279422313
Iteration time: 1.0
Weighted AreaUnderPRC: 0.8642198866135844
Mean absolute error: 0.017499560400914396
Coverage of cases: 92.32323232323232
Instances selection time: 1.0
Test time: 24.0
Accumulative iteration time: 13.0
Weighted Recall: 0.9232323232323232
Weighted FalsePositiveRate: 0.0076767676767676785
Kappa statistic: 0.9155555555555555
Training time: 0.0
		
Iteration: 20
Labeled set size: 479
Unlabelled set size: 16
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 8.080808080808081
Correctly Classified Instances: 91.91919191919192
Weighted Precision: 0.9240084789000942
Weighted AreaUnderROC: 0.9555555555555554
Root mean squared error: 0.12001749347603761
Relative absolute error: 10.934240362811796
Root relative squared error: 41.74815022935444
Weighted TruePositiveRate: 0.9191919191919192
Weighted MatthewsCorrelation: 0.9127787805196522
Weighted FMeasure: 0.919716116485775
Iteration time: 0.0
Weighted AreaUnderPRC: 0.8576412498287238
Mean absolute error: 0.018073124566631182
Coverage of cases: 91.91919191919192
Instances selection time: 0.0
Test time: 26.0
Accumulative iteration time: 13.0
Weighted Recall: 0.9191919191919192
Weighted FalsePositiveRate: 0.00808080808080808
Kappa statistic: 0.9111111111111111
Training time: 0.0
		
Time end:Wed Nov 01 15.01.27 EET 2017