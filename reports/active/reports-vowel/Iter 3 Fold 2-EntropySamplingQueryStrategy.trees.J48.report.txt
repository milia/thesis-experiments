Wed Nov 01 14.50.38 EET 2017
Dataset: vowel
Test set size: 495
Initial Labelled set size: 99
Initial Unlabelled set size: 396
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Nov 01 14.50.38 EET 2017
		
Iteration: 1
Labeled set size: 99
Unlabelled set size: 396
	
Mean region size: 18.84297520661166
Incorrectly Classified Instances: 54.94949494949495
Correctly Classified Instances: 45.05050505050505
Weighted Precision: 0.4667365870107907
Weighted AreaUnderROC: 0.7424354657687992
Root mean squared error: 0.2862471917569525
Relative absolute error: 64.70841455490537
Root relative squared error: 99.57124097568365
Weighted TruePositiveRate: 0.4505050505050505
Weighted MatthewsCorrelation: 0.40040749251603014
Weighted FMeasure: 0.4498429492731039
Iteration time: 20.0
Weighted AreaUnderPRC: 0.31221448832515514
Mean absolute error: 0.10695605711554677
Coverage of cases: 61.21212121212121
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 20.0
Weighted Recall: 0.4505050505050505
Weighted FalsePositiveRate: 0.054949494949494956
Kappa statistic: 0.39555555555555555
Training time: 18.0
		
Iteration: 2
Labeled set size: 119
Unlabelled set size: 376
	
Mean region size: 17.079889807162637
Incorrectly Classified Instances: 53.93939393939394
Correctly Classified Instances: 46.06060606060606
Weighted Precision: 0.45257015662065786
Weighted AreaUnderROC: 0.7495802469135803
Root mean squared error: 0.2846336962836831
Relative absolute error: 62.313315696648594
Root relative squared error: 99.00998569979437
Weighted TruePositiveRate: 0.46060606060606063
Weighted MatthewsCorrelation: 0.39880335608697864
Weighted FMeasure: 0.44386279316419197
Iteration time: 10.0
Weighted AreaUnderPRC: 0.32155646987585634
Mean absolute error: 0.102997216027519
Coverage of cases: 61.01010101010101
Instances selection time: 3.0
Test time: 4.0
Accumulative iteration time: 30.0
Weighted Recall: 0.46060606060606063
Weighted FalsePositiveRate: 0.05393939393939395
Kappa statistic: 0.40666666666666673
Training time: 7.0
		
Iteration: 3
Labeled set size: 139
Unlabelled set size: 356
	
Mean region size: 14.416896235078104
Incorrectly Classified Instances: 51.717171717171716
Correctly Classified Instances: 48.282828282828284
Weighted Precision: 0.50049013255776
Weighted AreaUnderROC: 0.7593804713804714
Root mean squared error: 0.28317337215646216
Relative absolute error: 57.87654320987621
Root relative squared error: 98.50201115974163
Weighted TruePositiveRate: 0.48282828282828283
Weighted MatthewsCorrelation: 0.4302291283548549
Weighted FMeasure: 0.46420317626063773
Iteration time: 10.0
Weighted AreaUnderPRC: 0.344838750774021
Mean absolute error: 0.09566370778491996
Coverage of cases: 60.2020202020202
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 40.0
Weighted Recall: 0.48282828282828283
Weighted FalsePositiveRate: 0.05171717171717171
Kappa statistic: 0.4311111111111111
Training time: 9.0
		
Iteration: 4
Labeled set size: 159
Unlabelled set size: 336
	
Mean region size: 16.106519742883442
Incorrectly Classified Instances: 52.121212121212125
Correctly Classified Instances: 47.878787878787875
Weighted Precision: 0.5078900833234515
Weighted AreaUnderROC: 0.7569472502805838
Root mean squared error: 0.28550188413420596
Relative absolute error: 58.68609574884057
Root relative squared error: 99.31198531469354
Weighted TruePositiveRate: 0.47878787878787876
Weighted MatthewsCorrelation: 0.43397790176205264
Weighted FMeasure: 0.47266490842083086
Iteration time: 11.0
Weighted AreaUnderPRC: 0.34744320529459943
Mean absolute error: 0.09700181115510902
Coverage of cases: 60.80808080808081
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 51.0
Weighted Recall: 0.47878787878787876
Weighted FalsePositiveRate: 0.052121212121212124
Kappa statistic: 0.42666666666666664
Training time: 10.0
		
Iteration: 5
Labeled set size: 179
Unlabelled set size: 316
	
Mean region size: 19.449035812672292
Incorrectly Classified Instances: 50.707070707070706
Correctly Classified Instances: 49.292929292929294
Weighted Precision: 0.5347683806687208
Weighted AreaUnderROC: 0.7700493827160494
Root mean squared error: 0.27979022711078577
Relative absolute error: 58.00472983806284
Root relative squared error: 97.32518231984595
Weighted TruePositiveRate: 0.49292929292929294
Weighted MatthewsCorrelation: 0.45557428223831087
Weighted FMeasure: 0.4922896428589191
Iteration time: 13.0
Weighted AreaUnderPRC: 0.35613108303565366
Mean absolute error: 0.0958755865091954
Coverage of cases: 64.24242424242425
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 64.0
Weighted Recall: 0.49292929292929294
Weighted FalsePositiveRate: 0.0507070707070707
Kappa statistic: 0.4422222222222223
Training time: 12.0
		
Iteration: 6
Labeled set size: 199
Unlabelled set size: 296
	
Mean region size: 15.059687786960575
Incorrectly Classified Instances: 49.696969696969695
Correctly Classified Instances: 50.303030303030305
Weighted Precision: 0.5215289150033727
Weighted AreaUnderROC: 0.7891604938271604
Root mean squared error: 0.28007839684619623
Relative absolute error: 56.37336860670171
Root relative squared error: 97.4254223186747
Weighted TruePositiveRate: 0.503030303030303
Weighted MatthewsCorrelation: 0.4564749987737096
Weighted FMeasure: 0.4957438243580507
Iteration time: 17.0
Weighted AreaUnderPRC: 0.3858920559312647
Mean absolute error: 0.09317912166397037
Coverage of cases: 63.63636363636363
Instances selection time: 2.0
Test time: 3.0
Accumulative iteration time: 81.0
Weighted Recall: 0.503030303030303
Weighted FalsePositiveRate: 0.0496969696969697
Kappa statistic: 0.4533333333333333
Training time: 15.0
		
Iteration: 7
Labeled set size: 219
Unlabelled set size: 276
	
Mean region size: 18.640955004591437
Incorrectly Classified Instances: 48.282828282828284
Correctly Classified Instances: 51.717171717171716
Weighted Precision: 0.5148599923482589
Weighted AreaUnderROC: 0.8101099887766555
Root mean squared error: 0.26879368933141673
Relative absolute error: 56.57869692036306
Root relative squared error: 93.50003068636737
Weighted TruePositiveRate: 0.5171717171717172
Weighted MatthewsCorrelation: 0.46076191556024365
Weighted FMeasure: 0.4845902000154834
Iteration time: 18.0
Weighted AreaUnderPRC: 0.4063198468040891
Mean absolute error: 0.09351850730638582
Coverage of cases: 71.91919191919192
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 99.0
Weighted Recall: 0.5171717171717172
Weighted FalsePositiveRate: 0.048282828282828275
Kappa statistic: 0.46888888888888886
Training time: 17.0
		
Iteration: 8
Labeled set size: 239
Unlabelled set size: 256
	
Mean region size: 13.314967860422437
Incorrectly Classified Instances: 42.82828282828283
Correctly Classified Instances: 57.17171717171717
Weighted Precision: 0.5381969733192119
Weighted AreaUnderROC: 0.8147991021324354
Root mean squared error: 0.2623100145156558
Relative absolute error: 48.76876888543534
Root relative squared error: 91.24468088354288
Weighted TruePositiveRate: 0.5717171717171717
Weighted MatthewsCorrelation: 0.5129122525650197
Weighted FMeasure: 0.5445995295345265
Iteration time: 19.0
Weighted AreaUnderPRC: 0.4492788398912675
Mean absolute error: 0.08060953534782753
Coverage of cases: 69.0909090909091
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 118.0
Weighted Recall: 0.5717171717171717
Weighted FalsePositiveRate: 0.04282828282828283
Kappa statistic: 0.5288888888888889
Training time: 18.0
		
Iteration: 9
Labeled set size: 259
Unlabelled set size: 236
	
Mean region size: 13.057851239669478
Incorrectly Classified Instances: 44.44444444444444
Correctly Classified Instances: 55.55555555555556
Weighted Precision: 0.5717152952958507
Weighted AreaUnderROC: 0.8020179573512907
Root mean squared error: 0.26831689534067854
Relative absolute error: 50.186000881834055
Root relative squared error: 93.33417763797183
Weighted TruePositiveRate: 0.5555555555555556
Weighted MatthewsCorrelation: 0.503622550072572
Weighted FMeasure: 0.5274716668636273
Iteration time: 23.0
Weighted AreaUnderPRC: 0.4131021635493872
Mean absolute error: 0.08295206757327997
Coverage of cases: 64.84848484848484
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 141.0
Weighted Recall: 0.5555555555555556
Weighted FalsePositiveRate: 0.044444444444444446
Kappa statistic: 0.5111111111111111
Training time: 22.0
		
Iteration: 10
Labeled set size: 279
Unlabelled set size: 216
	
Mean region size: 11.42332415059695
Incorrectly Classified Instances: 42.22222222222222
Correctly Classified Instances: 57.77777777777778
Weighted Precision: 0.5932425235773294
Weighted AreaUnderROC: 0.8073737373737372
Root mean squared error: 0.26385513357970014
Relative absolute error: 47.034154351395614
Root relative squared error: 91.78215138838092
Weighted TruePositiveRate: 0.5777777777777777
Weighted MatthewsCorrelation: 0.5325284169226898
Weighted FMeasure: 0.5573071088629568
Iteration time: 25.0
Weighted AreaUnderPRC: 0.427231048321895
Mean absolute error: 0.07774240388660482
Coverage of cases: 67.07070707070707
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 166.0
Weighted Recall: 0.5777777777777777
Weighted FalsePositiveRate: 0.042222222222222223
Kappa statistic: 0.5355555555555555
Training time: 24.0
		
Iteration: 11
Labeled set size: 299
Unlabelled set size: 196
	
Mean region size: 13.645546372819167
Incorrectly Classified Instances: 47.474747474747474
Correctly Classified Instances: 52.525252525252526
Weighted Precision: 0.5421874583071868
Weighted AreaUnderROC: 0.7873333333333333
Root mean squared error: 0.2781391071414268
Relative absolute error: 54.073094035593634
Root relative squared error: 96.75083934257616
Weighted TruePositiveRate: 0.5252525252525253
Weighted MatthewsCorrelation: 0.47986815122010884
Weighted FMeasure: 0.5165214273975225
Iteration time: 26.0
Weighted AreaUnderPRC: 0.39336268042380446
Mean absolute error: 0.08937701493486609
Coverage of cases: 64.64646464646465
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 192.0
Weighted Recall: 0.5252525252525253
Weighted FalsePositiveRate: 0.047474747474747475
Kappa statistic: 0.4777777777777778
Training time: 25.0
		
Iteration: 12
Labeled set size: 319
Unlabelled set size: 176
	
Mean region size: 15.022956841138722
Incorrectly Classified Instances: 41.01010101010101
Correctly Classified Instances: 58.98989898989899
Weighted Precision: 0.6231278413302007
Weighted AreaUnderROC: 0.8462514029180697
Root mean squared error: 0.24831083031185408
Relative absolute error: 46.00606060606034
Root relative squared error: 86.37505706203383
Weighted TruePositiveRate: 0.5898989898989899
Weighted MatthewsCorrelation: 0.556380246772901
Weighted FMeasure: 0.5830270255612692
Iteration time: 25.0
Weighted AreaUnderPRC: 0.5212382308344387
Mean absolute error: 0.0760430753819184
Coverage of cases: 75.55555555555556
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 217.0
Weighted Recall: 0.5898989898989899
Weighted FalsePositiveRate: 0.04101010101010102
Kappa statistic: 0.5488888888888889
Training time: 25.0
		
Iteration: 13
Labeled set size: 339
Unlabelled set size: 156
	
Mean region size: 16.19834710743811
Incorrectly Classified Instances: 41.61616161616162
Correctly Classified Instances: 58.38383838383838
Weighted Precision: 0.6074155471946318
Weighted AreaUnderROC: 0.8321683501683502
Root mean squared error: 0.25784121204430405
Relative absolute error: 50.44857243479164
Root relative squared error: 89.6902055190282
Weighted TruePositiveRate: 0.5838383838383838
Weighted MatthewsCorrelation: 0.5469628111911248
Weighted FMeasure: 0.5767272727896173
Iteration time: 27.0
Weighted AreaUnderPRC: 0.4767004253144445
Mean absolute error: 0.08338607014015201
Coverage of cases: 73.73737373737374
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 244.0
Weighted Recall: 0.5838383838383838
Weighted FalsePositiveRate: 0.041616161616161614
Kappa statistic: 0.5422222222222222
Training time: 26.0
		
Iteration: 14
Labeled set size: 359
Unlabelled set size: 136
	
Mean region size: 13.774104683195665
Incorrectly Classified Instances: 39.39393939393939
Correctly Classified Instances: 60.60606060606061
Weighted Precision: 0.6357938724740412
Weighted AreaUnderROC: 0.8395847362514027
Root mean squared error: 0.2530106118831109
Relative absolute error: 44.88735042735019
Root relative squared error: 88.00987863178405
Weighted TruePositiveRate: 0.6060606060606061
Weighted MatthewsCorrelation: 0.5747865132103182
Weighted FMeasure: 0.6029043988570074
Iteration time: 29.0
Weighted AreaUnderPRC: 0.5015788692246655
Mean absolute error: 0.07419396764851319
Coverage of cases: 73.73737373737374
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 273.0
Weighted Recall: 0.6060606060606061
Weighted FalsePositiveRate: 0.0393939393939394
Kappa statistic: 0.5666666666666667
Training time: 28.0
		
Iteration: 15
Labeled set size: 379
Unlabelled set size: 116
	
Mean region size: 16.7676767676768
Incorrectly Classified Instances: 39.5959595959596
Correctly Classified Instances: 60.4040404040404
Weighted Precision: 0.642236407811158
Weighted AreaUnderROC: 0.8554612794612794
Root mean squared error: 0.24741482355931524
Relative absolute error: 46.451139674427544
Root relative squared error: 86.06338062697334
Weighted TruePositiveRate: 0.604040404040404
Weighted MatthewsCorrelation: 0.5764130512092597
Weighted FMeasure: 0.6045083244298274
Iteration time: 31.0
Weighted AreaUnderPRC: 0.5329771685314596
Mean absolute error: 0.0767787432635171
Coverage of cases: 78.38383838383838
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 304.0
Weighted Recall: 0.604040404040404
Weighted FalsePositiveRate: 0.03959595959595961
Kappa statistic: 0.5644444444444444
Training time: 30.0
		
Iteration: 16
Labeled set size: 399
Unlabelled set size: 96
	
Mean region size: 14.69237832874206
Incorrectly Classified Instances: 37.57575757575758
Correctly Classified Instances: 62.42424242424242
Weighted Precision: 0.6454845784305331
Weighted AreaUnderROC: 0.8389360269360269
Root mean squared error: 0.2438514561963784
Relative absolute error: 44.07808433732299
Root relative squared error: 84.82386135622663
Weighted TruePositiveRate: 0.6242424242424243
Weighted MatthewsCorrelation: 0.5926610322528545
Weighted FMeasure: 0.6228740173899204
Iteration time: 32.0
Weighted AreaUnderPRC: 0.5119997475170804
Mean absolute error: 0.07285633774764178
Coverage of cases: 71.71717171717172
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 336.0
Weighted Recall: 0.6242424242424243
Weighted FalsePositiveRate: 0.03757575757575758
Kappa statistic: 0.5866666666666667
Training time: 31.0
		
Iteration: 17
Labeled set size: 419
Unlabelled set size: 76
	
Mean region size: 16.418732782369204
Incorrectly Classified Instances: 37.17171717171717
Correctly Classified Instances: 62.82828282828283
Weighted Precision: 0.6390520628121982
Weighted AreaUnderROC: 0.8622603815937149
Root mean squared error: 0.23832925857496928
Relative absolute error: 45.188726088725836
Root relative squared error: 82.90296191717277
Weighted TruePositiveRate: 0.6282828282828283
Weighted MatthewsCorrelation: 0.5926246770737018
Weighted FMeasure: 0.6233329436926539
Iteration time: 34.0
Weighted AreaUnderPRC: 0.5435591791987857
Mean absolute error: 0.07469210923756385
Coverage of cases: 78.78787878787878
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 370.0
Weighted Recall: 0.6282828282828283
Weighted FalsePositiveRate: 0.03717171717171717
Kappa statistic: 0.5911111111111111
Training time: 34.0
		
Iteration: 18
Labeled set size: 439
Unlabelled set size: 56
	
Mean region size: 13.645546372819178
Incorrectly Classified Instances: 35.55555555555556
Correctly Classified Instances: 64.44444444444444
Weighted Precision: 0.6700610320689123
Weighted AreaUnderROC: 0.859611672278339
Root mean squared error: 0.23344787641992462
Relative absolute error: 39.74583342583326
Root relative squared error: 81.20497048581211
Weighted TruePositiveRate: 0.6444444444444445
Weighted MatthewsCorrelation: 0.6180521279408312
Weighted FMeasure: 0.647718202187378
Iteration time: 36.0
Weighted AreaUnderPRC: 0.5635479635025296
Mean absolute error: 0.06569559243939424
Coverage of cases: 74.14141414141415
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 406.0
Weighted Recall: 0.6444444444444445
Weighted FalsePositiveRate: 0.035555555555555556
Kappa statistic: 0.6088888888888889
Training time: 35.0
		
Iteration: 19
Labeled set size: 459
Unlabelled set size: 36
	
Mean region size: 16.510560146923844
Incorrectly Classified Instances: 30.1010101010101
Correctly Classified Instances: 69.8989898989899
Weighted Precision: 0.7151000961593695
Weighted AreaUnderROC: 0.887946127946128
Root mean squared error: 0.21354347100806728
Relative absolute error: 38.48706916059138
Root relative squared error: 74.28121226279852
Weighted TruePositiveRate: 0.6989898989898989
Weighted MatthewsCorrelation: 0.6742594870484535
Weighted FMeasure: 0.7000187418722535
Iteration time: 36.0
Weighted AreaUnderPRC: 0.6149602195403497
Mean absolute error: 0.06361499034808533
Coverage of cases: 83.23232323232324
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 442.0
Weighted Recall: 0.6989898989898989
Weighted FalsePositiveRate: 0.030101010101010104
Kappa statistic: 0.6688888888888889
Training time: 36.0
		
Iteration: 20
Labeled set size: 479
Unlabelled set size: 16
	
Mean region size: 12.121212121212169
Incorrectly Classified Instances: 31.31313131313131
Correctly Classified Instances: 68.68686868686869
Weighted Precision: 0.6934531098003393
Weighted AreaUnderROC: 0.8624826038159371
Root mean squared error: 0.22498533794901932
Relative absolute error: 36.31909510242829
Root relative squared error: 78.2612718867776
Weighted TruePositiveRate: 0.6868686868686869
Weighted MatthewsCorrelation: 0.656180096144353
Weighted FMeasure: 0.6829443072344599
Iteration time: 39.0
Weighted AreaUnderPRC: 0.5795002060399574
Mean absolute error: 0.060031562152774415
Coverage of cases: 75.95959595959596
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 481.0
Weighted Recall: 0.6868686868686869
Weighted FalsePositiveRate: 0.031313131313131314
Kappa statistic: 0.6555555555555556
Training time: 38.0
		
Time end:Wed Nov 01 14.50.40 EET 2017