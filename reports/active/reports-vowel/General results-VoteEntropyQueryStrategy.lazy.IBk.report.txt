Wed Nov 01 15.04.11 EET 2017
Dataset: vowel
Test set size: 495
Initial Labelled set size: 99
Initial Unlabelled set size: 396
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Wed Nov 01 15.04.11 EET 2017
		
Iteration: 1
Labeled set size: 99
Unlabelled set size: 396
	
Mean region size: 54.545454545454064
Incorrectly Classified Instances: 60.989898989899
Correctly Classified Instances: 39.01010101010101
Weighted Precision: 0.433316987970865
Weighted AreaUnderROC: 0.6645555555555556
Root mean squared error: 0.3171465242850592
Relative absolute error: 70.38000000000036
Root relative squared error: 110.31959056213566
Weighted TruePositiveRate: 0.39010101010101017
Weighted MatthewsCorrelation: 0.34349935415486543
Weighted FMeasure: 0.3922319931570696
Iteration time: 7.4
Weighted AreaUnderPRC: 0.22580134965911994
Mean absolute error: 0.11633057851239806
Coverage of cases: 69.69696969696969
Instances selection time: 7.0
Test time: 10.8
Accumulative iteration time: 7.4
Weighted Recall: 0.39010101010101017
Weighted FalsePositiveRate: 0.060989898989899
Kappa statistic: 0.32911111111111113
Training time: 0.4
		
Iteration: 2
Labeled set size: 119
Unlabelled set size: 376
	
Mean region size: 45.45454545454594
Incorrectly Classified Instances: 57.71717171717171
Correctly Classified Instances: 42.28282828282828
Weighted Precision: 0.46795854861085473
Weighted AreaUnderROC: 0.6825555555555555
Root mean squared error: 0.31080046456091404
Relative absolute error: 66.57829059829024
Root relative squared error: 108.11211024360223
Weighted TruePositiveRate: 0.4228282828282829
Weighted MatthewsCorrelation: 0.3790671514023234
Weighted FMeasure: 0.421507464569096
Iteration time: 7.6
Weighted AreaUnderPRC: 0.250272352197721
Mean absolute error: 0.11004676131948869
Coverage of cases: 65.87878787878788
Instances selection time: 7.0
Test time: 10.7
Accumulative iteration time: 15.0
Weighted Recall: 0.4228282828282829
Weighted FalsePositiveRate: 0.05771717171717172
Kappa statistic: 0.36511111111111105
Training time: 0.6
		
Iteration: 3
Labeled set size: 139
Unlabelled set size: 356
	
Mean region size: 36.36363636363659
Incorrectly Classified Instances: 54.80808080808081
Correctly Classified Instances: 45.19191919191919
Weighted Precision: 0.5004242157186642
Weighted AreaUnderROC: 0.6985555555555555
Root mean squared error: 0.3045189053296209
Relative absolute error: 63.2010370370366
Root relative squared error: 105.92706645650655
Weighted TruePositiveRate: 0.451919191919192
Weighted MatthewsCorrelation: 0.41097586330383873
Weighted FMeasure: 0.4481202457016248
Iteration time: 7.3
Weighted AreaUnderPRC: 0.2736873324987311
Mean absolute error: 0.10446452402816031
Coverage of cases: 61.63636363636364
Instances selection time: 6.8
Test time: 11.4
Accumulative iteration time: 22.3
Weighted Recall: 0.451919191919192
Weighted FalsePositiveRate: 0.05480808080808082
Kappa statistic: 0.39711111111111114
Training time: 0.5
		
Iteration: 4
Labeled set size: 159
Unlabelled set size: 336
	
Mean region size: 27.272727272727032
Incorrectly Classified Instances: 51.13131313131314
Correctly Classified Instances: 48.868686868686865
Weighted Precision: 0.5393765124353564
Weighted AreaUnderROC: 0.7187777777777778
Root mean squared error: 0.2953471234950311
Relative absolute error: 59.07568627450915
Root relative squared error: 102.73665716856578
Weighted TruePositiveRate: 0.4886868686868687
Weighted MatthewsCorrelation: 0.4488444806822133
Weighted FMeasure: 0.47684843402321986
Iteration time: 8.4
Weighted AreaUnderPRC: 0.3044175250661437
Mean absolute error: 0.09764576243720584
Coverage of cases: 59.191919191919204
Instances selection time: 7.9
Test time: 12.3
Accumulative iteration time: 30.7
Weighted Recall: 0.4886868686868687
Weighted FalsePositiveRate: 0.051131313131313125
Kappa statistic: 0.4375555555555556
Training time: 0.5
		
Iteration: 5
Labeled set size: 179
Unlabelled set size: 316
	
Mean region size: 18.181818181818294
Incorrectly Classified Instances: 49.25252525252525
Correctly Classified Instances: 50.74747474747475
Weighted Precision: 0.5586654992798887
Weighted AreaUnderROC: 0.7291111111111113
Root mean squared error: 0.29083325177230523
Relative absolute error: 56.830643274853585
Root relative squared error: 101.16650444050518
Weighted TruePositiveRate: 0.5074747474747474
Weighted MatthewsCorrelation: 0.46914012184876563
Weighted FMeasure: 0.4946580995175509
Iteration time: 7.8
Weighted AreaUnderPRC: 0.32401344992340947
Mean absolute error: 0.09393494756174209
Coverage of cases: 55.17171717171717
Instances selection time: 7.0
Test time: 13.8
Accumulative iteration time: 38.5
Weighted Recall: 0.5074747474747474
Weighted FalsePositiveRate: 0.04925252525252525
Kappa statistic: 0.4582222222222221
Training time: 0.8
		
Iteration: 6
Labeled set size: 199
Unlabelled set size: 296
	
Mean region size: 9.090909090909147
Incorrectly Classified Instances: 46.38383838383839
Correctly Classified Instances: 53.61616161616162
Weighted Precision: 0.5930029440587098
Weighted AreaUnderROC: 0.7448888888888889
Root mean squared error: 0.28300306693339417
Relative absolute error: 53.58772486772477
Root relative squared error: 98.44277039548669
Weighted TruePositiveRate: 0.5361616161616161
Weighted MatthewsCorrelation: 0.5006172662748207
Weighted FMeasure: 0.5187407098016481
Iteration time: 7.7
Weighted AreaUnderPRC: 0.3540083692553496
Mean absolute error: 0.08857475184747954
Coverage of cases: 53.61616161616162
Instances selection time: 7.2
Test time: 14.3
Accumulative iteration time: 46.2
Weighted Recall: 0.5361616161616161
Weighted FalsePositiveRate: 0.04638383838383839
Kappa statistic: 0.48977777777777776
Training time: 0.5
		
Iteration: 7
Labeled set size: 219
Unlabelled set size: 276
	
Mean region size: 9.090909090909147
Incorrectly Classified Instances: 43.25252525252525
Correctly Classified Instances: 56.74747474747475
Weighted Precision: 0.6216727732098433
Weighted AreaUnderROC: 0.7621111111111111
Root mean squared error: 0.27387095106380926
Relative absolute error: 50.08492753623155
Root relative squared error: 95.26615893499635
Weighted TruePositiveRate: 0.5674747474747475
Weighted MatthewsCorrelation: 0.5331268944708709
Weighted FMeasure: 0.5480781337158335
Iteration time: 7.8
Weighted AreaUnderPRC: 0.38509556447817506
Mean absolute error: 0.08278500419211879
Coverage of cases: 56.74747474747475
Instances selection time: 7.4
Test time: 15.0
Accumulative iteration time: 54.0
Weighted Recall: 0.5674747474747475
Weighted FalsePositiveRate: 0.04325252525252525
Kappa statistic: 0.5242222222222221
Training time: 0.4
		
Iteration: 8
Labeled set size: 239
Unlabelled set size: 256
	
Mean region size: 9.090909090909147
Incorrectly Classified Instances: 40.92929292929293
Correctly Classified Instances: 59.07070707070708
Weighted Precision: 0.6449977965773595
Weighted AreaUnderROC: 0.7748888888888891
Root mean squared error: 0.2669177043568455
Relative absolute error: 47.44124444444425
Root relative squared error: 92.84746829501854
Weighted TruePositiveRate: 0.5907070707070707
Weighted MatthewsCorrelation: 0.5569268487128349
Weighted FMeasure: 0.5668204784542485
Iteration time: 7.8
Weighted AreaUnderPRC: 0.40930910073987603
Mean absolute error: 0.07841528007346207
Coverage of cases: 59.07070707070708
Instances selection time: 7.1
Test time: 16.0
Accumulative iteration time: 61.8
Weighted Recall: 0.5907070707070707
Weighted FalsePositiveRate: 0.040929292929292926
Kappa statistic: 0.5497777777777777
Training time: 0.7
		
Iteration: 9
Labeled set size: 259
Unlabelled set size: 236
	
Mean region size: 9.090909090909147
Incorrectly Classified Instances: 37.57575757575758
Correctly Classified Instances: 62.424242424242436
Weighted Precision: 0.6775442916865458
Weighted AreaUnderROC: 0.7933333333333332
Root mean squared error: 0.2561785532677189
Relative absolute error: 43.72345679012314
Root relative squared error: 89.11184876140359
Weighted TruePositiveRate: 0.6242424242424243
Weighted MatthewsCorrelation: 0.593116219872888
Weighted FMeasure: 0.6002632213520833
Iteration time: 7.6
Weighted AreaUnderPRC: 0.44845257151620166
Mean absolute error: 0.07227017651260069
Coverage of cases: 62.424242424242436
Instances selection time: 7.2
Test time: 16.5
Accumulative iteration time: 69.4
Weighted Recall: 0.6242424242424243
Weighted FalsePositiveRate: 0.03757575757575758
Kappa statistic: 0.5866666666666667
Training time: 0.4
		
Iteration: 10
Labeled set size: 279
Unlabelled set size: 216
	
Mean region size: 9.090909090909147
Incorrectly Classified Instances: 35.535353535353536
Correctly Classified Instances: 64.46464646464645
Weighted Precision: 0.7001158452530027
Weighted AreaUnderROC: 0.8045555555555556
Root mean squared error: 0.24948909361013497
Relative absolute error: 41.39931034482728
Root relative squared error: 86.78491658968835
Weighted TruePositiveRate: 0.6446464646464647
Weighted MatthewsCorrelation: 0.6149495449951867
Weighted FMeasure: 0.6183763017156189
Iteration time: 7.6
Weighted AreaUnderPRC: 0.472879841925588
Mean absolute error: 0.0684286121402108
Coverage of cases: 64.46464646464645
Instances selection time: 7.1
Test time: 17.8
Accumulative iteration time: 77.0
Weighted Recall: 0.6446464646464647
Weighted FalsePositiveRate: 0.03553535353535354
Kappa statistic: 0.6091111111111112
Training time: 0.5
		
Iteration: 11
Labeled set size: 299
Unlabelled set size: 196
	
Mean region size: 9.090909090909147
Incorrectly Classified Instances: 32.62626262626262
Correctly Classified Instances: 67.37373737373738
Weighted Precision: 0.7282958165680606
Weighted AreaUnderROC: 0.8205555555555556
Root mean squared error: 0.23929682851823805
Relative absolute error: 38.16379928315385
Root relative squared error: 83.2395316469602
Weighted TruePositiveRate: 0.6737373737373737
Weighted MatthewsCorrelation: 0.6467541512093
Weighted FMeasure: 0.6483052742200692
Iteration time: 7.3
Weighted AreaUnderPRC: 0.5083669334669084
Mean absolute error: 0.06308065997215553
Coverage of cases: 67.37373737373738
Instances selection time: 6.7
Test time: 18.7
Accumulative iteration time: 84.3
Weighted Recall: 0.6737373737373737
Weighted FalsePositiveRate: 0.03262626262626263
Kappa statistic: 0.6411111111111112
Training time: 0.6
		
Iteration: 12
Labeled set size: 319
Unlabelled set size: 176
	
Mean region size: 9.090909090909147
Incorrectly Classified Instances: 30.444444444444446
Correctly Classified Instances: 69.55555555555556
Weighted Precision: 0.7472984361711628
Weighted AreaUnderROC: 0.8325555555555555
Root mean squared error: 0.2314123720155307
Relative absolute error: 35.705925925925634
Root relative squared error: 80.4969191742413
Weighted TruePositiveRate: 0.6955555555555555
Weighted MatthewsCorrelation: 0.6692572648377567
Weighted FMeasure: 0.6688484388753974
Iteration time: 6.7
Weighted AreaUnderPRC: 0.5358252790754638
Mean absolute error: 0.059018059381695645
Coverage of cases: 69.55555555555556
Instances selection time: 6.4
Test time: 19.3
Accumulative iteration time: 91.0
Weighted Recall: 0.6955555555555555
Weighted FalsePositiveRate: 0.03044444444444444
Kappa statistic: 0.6651111111111111
Training time: 0.3
		
Iteration: 13
Labeled set size: 339
Unlabelled set size: 156
	
Mean region size: 9.090909090909147
Incorrectly Classified Instances: 27.454545454545457
Correctly Classified Instances: 72.54545454545455
Weighted Precision: 0.7778054020477819
Weighted AreaUnderROC: 0.849
Root mean squared error: 0.21991441382709245
Relative absolute error: 32.39371428571396
Root relative squared error: 76.49734817938798
Weighted TruePositiveRate: 0.7254545454545454
Weighted MatthewsCorrelation: 0.7013112745578284
Weighted FMeasure: 0.6972777862897146
Iteration time: 6.4
Weighted AreaUnderPRC: 0.5756937883216706
Mean absolute error: 0.05354332939787467
Coverage of cases: 72.54545454545455
Instances selection time: 5.9
Test time: 20.0
Accumulative iteration time: 97.4
Weighted Recall: 0.7254545454545454
Weighted FalsePositiveRate: 0.027454545454545454
Kappa statistic: 0.6980000000000001
Training time: 0.5
		
Iteration: 14
Labeled set size: 359
Unlabelled set size: 136
	
Mean region size: 9.090909090909147
Incorrectly Classified Instances: 25.454545454545457
Correctly Classified Instances: 74.54545454545453
Weighted Precision: 0.7916966802087042
Weighted AreaUnderROC: 0.86
Root mean squared error: 0.21196547253656664
Relative absolute error: 30.14054054054038
Root relative squared error: 73.73230463823583
Weighted TruePositiveRate: 0.7454545454545454
Weighted MatthewsCorrelation: 0.7224755832224652
Weighted FMeasure: 0.7194297538420119
Iteration time: 6.1
Weighted AreaUnderPRC: 0.6026627403113499
Mean absolute error: 0.04981907527362077
Coverage of cases: 74.54545454545453
Instances selection time: 5.6
Test time: 21.0
Accumulative iteration time: 103.5
Weighted Recall: 0.7454545454545454
Weighted FalsePositiveRate: 0.02545454545454546
Kappa statistic: 0.72
Training time: 0.5
		
Iteration: 15
Labeled set size: 379
Unlabelled set size: 116
	
Mean region size: 9.090909090909147
Incorrectly Classified Instances: 22.888888888888893
Correctly Classified Instances: 77.11111111111111
Weighted Precision: 0.8064660030042434
Weighted AreaUnderROC: 0.8741111111111112
Root mean squared error: 0.20112483086672245
Relative absolute error: 27.288148148147986
Root relative squared error: 69.96138155104758
Weighted TruePositiveRate: 0.7711111111111111
Weighted MatthewsCorrelation: 0.7478921782625995
Weighted FMeasure: 0.7461594174065407
Iteration time: 5.5
Weighted AreaUnderPRC: 0.6378347145524064
Mean absolute error: 0.04510437710437713
Coverage of cases: 77.11111111111111
Instances selection time: 5.0
Test time: 21.9
Accumulative iteration time: 109.0
Weighted Recall: 0.7711111111111111
Weighted FalsePositiveRate: 0.02288888888888889
Kappa statistic: 0.748222222222222
Training time: 0.5
		
Iteration: 16
Labeled set size: 399
Unlabelled set size: 96
	
Mean region size: 9.090909090909147
Incorrectly Classified Instances: 19.313131313131315
Correctly Classified Instances: 80.68686868686869
Weighted Precision: 0.8360057525143978
Weighted AreaUnderROC: 0.8937777777777777
Root mean squared error: 0.18476476685157733
Relative absolute error: 23.3573983739836
Root relative squared error: 64.27052440610562
Weighted TruePositiveRate: 0.8068686868686867
Weighted MatthewsCorrelation: 0.7876727812635039
Weighted FMeasure: 0.7873109148571253
Iteration time: 5.1
Weighted AreaUnderPRC: 0.6909600870709368
Mean absolute error: 0.03860727003964256
Coverage of cases: 80.68686868686869
Instances selection time: 4.2
Test time: 22.7
Accumulative iteration time: 114.1
Weighted Recall: 0.8068686868686867
Weighted FalsePositiveRate: 0.019313131313131317
Kappa statistic: 0.7875555555555556
Training time: 0.9
		
Iteration: 17
Labeled set size: 419
Unlabelled set size: 76
	
Mean region size: 9.090909090909147
Incorrectly Classified Instances: 16.80808080808081
Correctly Classified Instances: 83.1919191919192
Weighted Precision: 0.8539361071166466
Weighted AreaUnderROC: 0.9075555555555559
Root mean squared error: 0.17260952686957892
Relative absolute error: 20.574056847545087
Root relative squared error: 60.042317582709146
Weighted TruePositiveRate: 0.831919191919192
Weighted MatthewsCorrelation: 0.8131211966641395
Weighted FMeasure: 0.8125304713229899
Iteration time: 4.3
Weighted AreaUnderPRC: 0.7270058301353115
Mean absolute error: 0.03400670553313259
Coverage of cases: 83.1919191919192
Instances selection time: 3.9
Test time: 23.3
Accumulative iteration time: 118.4
Weighted Recall: 0.831919191919192
Weighted FalsePositiveRate: 0.016808080808080807
Kappa statistic: 0.8151111111111111
Training time: 0.4
		
Iteration: 18
Labeled set size: 439
Unlabelled set size: 56
	
Mean region size: 9.090909090909147
Incorrectly Classified Instances: 13.191919191919192
Correctly Classified Instances: 86.8080808080808
Weighted Precision: 0.8830832714304613
Weighted AreaUnderROC: 0.9274444444444445
Root mean squared error: 0.15295878163894547
Relative absolute error: 16.60083950617265
Root relative squared error: 53.20679519137499
Weighted TruePositiveRate: 0.8680808080808081
Weighted MatthewsCorrelation: 0.8536734914542375
Weighted FMeasure: 0.8555106449601466
Iteration time: 3.2
Weighted AreaUnderPRC: 0.7789455463801197
Mean absolute error: 0.027439404142434308
Coverage of cases: 86.8080808080808
Instances selection time: 2.7
Test time: 24.5
Accumulative iteration time: 121.6
Weighted Recall: 0.8680808080808081
Weighted FalsePositiveRate: 0.01319191919191919
Kappa statistic: 0.8548888888888889
Training time: 0.5
		
Iteration: 19
Labeled set size: 459
Unlabelled set size: 36
	
Mean region size: 9.090909090909147
Incorrectly Classified Instances: 10.929292929292929
Correctly Classified Instances: 89.07070707070707
Weighted Precision: 0.9018436612506167
Weighted AreaUnderROC: 0.9398888888888891
Root mean squared error: 0.13929709945041113
Relative absolute error: 14.081276595744546
Root relative squared error: 48.45457162901154
Weighted TruePositiveRate: 0.8907070707070709
Weighted MatthewsCorrelation: 0.8792058992676255
Weighted FMeasure: 0.8817067646586969
Iteration time: 2.4
Weighted AreaUnderPRC: 0.8140786296045895
Mean absolute error: 0.023274837348338247
Coverage of cases: 89.07070707070707
Instances selection time: 2.0
Test time: 25.5
Accumulative iteration time: 124.0
Weighted Recall: 0.8907070707070709
Weighted FalsePositiveRate: 0.010929292929292931
Kappa statistic: 0.8797777777777777
Training time: 0.4
		
Iteration: 20
Labeled set size: 479
Unlabelled set size: 16
	
Mean region size: 9.090909090909147
Incorrectly Classified Instances: 7.6969696969696955
Correctly Classified Instances: 92.30303030303031
Weighted Precision: 0.9286449516179832
Weighted AreaUnderROC: 0.9576666666666668
Root mean squared error: 0.11653459706263258
Relative absolute error: 10.521496598639432
Root relative squared error: 40.5366228220674
Weighted TruePositiveRate: 0.9230303030303029
Weighted MatthewsCorrelation: 0.9166471843178835
Weighted FMeasure: 0.9220557256801568
Iteration time: 1.7
Weighted AreaUnderPRC: 0.8649188860186203
Mean absolute error: 0.01739090346882562
Coverage of cases: 92.30303030303031
Instances selection time: 1.0
Test time: 25.9
Accumulative iteration time: 125.7
Weighted Recall: 0.9230303030303029
Weighted FalsePositiveRate: 0.007696969696969698
Kappa statistic: 0.9153333333333332
Training time: 0.7
		
Time end:Wed Nov 01 15.04.23 EET 2017