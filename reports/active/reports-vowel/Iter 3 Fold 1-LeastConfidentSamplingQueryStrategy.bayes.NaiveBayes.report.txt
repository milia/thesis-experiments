Wed Nov 01 14.54.09 EET 2017
Dataset: vowel
Test set size: 495
Initial Labelled set size: 99
Initial Unlabelled set size: 396
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Wed Nov 01 14.54.09 EET 2017
		
Iteration: 1
Labeled set size: 99
Unlabelled set size: 396
	
Mean region size: 19.081726354453654
Incorrectly Classified Instances: 47.07070707070707
Correctly Classified Instances: 52.92929292929293
Weighted Precision: 0.5602796557550043
Weighted AreaUnderROC: 0.9002648709315376
Root mean squared error: 0.2631944997759783
Relative absolute error: 56.34183919412489
Root relative squared error: 91.55234956128416
Weighted TruePositiveRate: 0.5292929292929293
Weighted MatthewsCorrelation: 0.48859165914301705
Weighted FMeasure: 0.5197170019181252
Iteration time: 37.0
Weighted AreaUnderPRC: 0.547242637604923
Mean absolute error: 0.09312700693243843
Coverage of cases: 73.93939393939394
Instances selection time: 37.0
Test time: 37.0
Accumulative iteration time: 37.0
Weighted Recall: 0.5292929292929293
Weighted FalsePositiveRate: 0.047070707070707055
Kappa statistic: 0.48222222222222216
Training time: 0.0
		
Iteration: 2
Labeled set size: 119
Unlabelled set size: 376
	
Mean region size: 19.834710743801683
Incorrectly Classified Instances: 43.23232323232323
Correctly Classified Instances: 56.76767676767677
Weighted Precision: 0.6011381112222746
Weighted AreaUnderROC: 0.9235286195286196
Root mean squared error: 0.2498904406764048
Relative absolute error: 53.143628517450246
Root relative squared error: 86.92452538446865
Weighted TruePositiveRate: 0.5676767676767677
Weighted MatthewsCorrelation: 0.5345654645147412
Weighted FMeasure: 0.5660529070470393
Iteration time: 23.0
Weighted AreaUnderPRC: 0.5924408519619699
Mean absolute error: 0.08784070829330676
Coverage of cases: 78.18181818181819
Instances selection time: 22.0
Test time: 37.0
Accumulative iteration time: 60.0
Weighted Recall: 0.5676767676767677
Weighted FalsePositiveRate: 0.043232323232323226
Kappa statistic: 0.5244444444444444
Training time: 1.0
		
Iteration: 3
Labeled set size: 139
Unlabelled set size: 356
	
Mean region size: 21.68962350780535
Incorrectly Classified Instances: 37.97979797979798
Correctly Classified Instances: 62.02020202020202
Weighted Precision: 0.6564808864640225
Weighted AreaUnderROC: 0.9366105499438834
Root mean squared error: 0.23165713951597175
Relative absolute error: 49.755525913378264
Root relative squared error: 80.58206168208511
Weighted TruePositiveRate: 0.6202020202020202
Weighted MatthewsCorrelation: 0.594962987976525
Weighted FMeasure: 0.6235740779839921
Iteration time: 22.0
Weighted AreaUnderPRC: 0.6235950577958862
Mean absolute error: 0.08224053869979932
Coverage of cases: 84.84848484848484
Instances selection time: 21.0
Test time: 36.0
Accumulative iteration time: 82.0
Weighted Recall: 0.6202020202020202
Weighted FalsePositiveRate: 0.03797979797979799
Kappa statistic: 0.5822222222222222
Training time: 1.0
		
Iteration: 4
Labeled set size: 159
Unlabelled set size: 336
	
Mean region size: 21.414141414141433
Incorrectly Classified Instances: 39.7979797979798
Correctly Classified Instances: 60.2020202020202
Weighted Precision: 0.6273938468621145
Weighted AreaUnderROC: 0.9350123456790123
Root mean squared error: 0.23313920396588336
Relative absolute error: 49.949449458706546
Root relative squared error: 81.09759860518248
Weighted TruePositiveRate: 0.602020202020202
Weighted MatthewsCorrelation: 0.5698189957264711
Weighted FMeasure: 0.6008274550268438
Iteration time: 19.0
Weighted AreaUnderPRC: 0.6330608985149172
Mean absolute error: 0.08256107348546589
Coverage of cases: 84.84848484848484
Instances selection time: 19.0
Test time: 37.0
Accumulative iteration time: 101.0
Weighted Recall: 0.602020202020202
Weighted FalsePositiveRate: 0.0397979797979798
Kappa statistic: 0.5622222222222222
Training time: 0.0
		
Iteration: 5
Labeled set size: 179
Unlabelled set size: 316
	
Mean region size: 22.589531680440803
Incorrectly Classified Instances: 39.19191919191919
Correctly Classified Instances: 60.80808080808081
Weighted Precision: 0.6357967388127845
Weighted AreaUnderROC: 0.9372076318742986
Root mean squared error: 0.23136192342936443
Relative absolute error: 50.99725446405451
Root relative squared error: 80.47937060616903
Weighted TruePositiveRate: 0.6080808080808081
Weighted MatthewsCorrelation: 0.5782663078495621
Weighted FMeasure: 0.6098419032159267
Iteration time: 18.0
Weighted AreaUnderPRC: 0.6403011134902588
Mean absolute error: 0.08429298258521459
Coverage of cases: 87.47474747474747
Instances selection time: 17.0
Test time: 37.0
Accumulative iteration time: 119.0
Weighted Recall: 0.6080808080808081
Weighted FalsePositiveRate: 0.03919191919191919
Kappa statistic: 0.5688888888888889
Training time: 1.0
		
Iteration: 6
Labeled set size: 199
Unlabelled set size: 296
	
Mean region size: 23.41597796143255
Incorrectly Classified Instances: 38.18181818181818
Correctly Classified Instances: 61.81818181818182
Weighted Precision: 0.6385795277232909
Weighted AreaUnderROC: 0.9388866442199775
Root mean squared error: 0.22646575105310465
Relative absolute error: 50.39103727873011
Root relative squared error: 78.77623438833325
Weighted TruePositiveRate: 0.6181818181818182
Weighted MatthewsCorrelation: 0.5884489765390509
Weighted FMeasure: 0.6238531626217142
Iteration time: 17.0
Weighted AreaUnderPRC: 0.6476487431137874
Mean absolute error: 0.08329097070864534
Coverage of cases: 88.08080808080808
Instances selection time: 16.0
Test time: 37.0
Accumulative iteration time: 136.0
Weighted Recall: 0.6181818181818182
Weighted FalsePositiveRate: 0.038181818181818185
Kappa statistic: 0.58
Training time: 1.0
		
Iteration: 7
Labeled set size: 219
Unlabelled set size: 276
	
Mean region size: 23.581267217630888
Incorrectly Classified Instances: 39.19191919191919
Correctly Classified Instances: 60.80808080808081
Weighted Precision: 0.6362056108092048
Weighted AreaUnderROC: 0.9359461279461279
Root mean squared error: 0.23277771411633533
Relative absolute error: 52.07705658937643
Root relative squared error: 80.97185416486612
Weighted TruePositiveRate: 0.6080808080808081
Weighted MatthewsCorrelation: 0.5804784641693975
Weighted FMeasure: 0.6154353294440013
Iteration time: 39.0
Weighted AreaUnderPRC: 0.6249792324214805
Mean absolute error: 0.08607777948657316
Coverage of cases: 88.28282828282828
Instances selection time: 38.0
Test time: 89.0
Accumulative iteration time: 175.0
Weighted Recall: 0.6080808080808081
Weighted FalsePositiveRate: 0.039191919191919194
Kappa statistic: 0.5688888888888889
Training time: 1.0
		
Iteration: 8
Labeled set size: 239
Unlabelled set size: 256
	
Mean region size: 23.691460055096435
Incorrectly Classified Instances: 37.37373737373738
Correctly Classified Instances: 62.62626262626262
Weighted Precision: 0.6437254358979543
Weighted AreaUnderROC: 0.9345274971941638
Root mean squared error: 0.23105444494248267
Relative absolute error: 51.29728394532995
Root relative squared error: 80.3724140476637
Weighted TruePositiveRate: 0.6262626262626263
Weighted MatthewsCorrelation: 0.5955618841162866
Weighted FMeasure: 0.6293051348803821
Iteration time: 38.0
Weighted AreaUnderPRC: 0.6199916521710156
Mean absolute error: 0.08478889908319054
Coverage of cases: 86.86868686868686
Instances selection time: 36.0
Test time: 89.0
Accumulative iteration time: 213.0
Weighted Recall: 0.6262626262626263
Weighted FalsePositiveRate: 0.03737373737373737
Kappa statistic: 0.5888888888888889
Training time: 2.0
		
Iteration: 9
Labeled set size: 259
Unlabelled set size: 236
	
Mean region size: 24.315886134067977
Incorrectly Classified Instances: 34.54545454545455
Correctly Classified Instances: 65.45454545454545
Weighted Precision: 0.6685246673154527
Weighted AreaUnderROC: 0.9450370370370372
Root mean squared error: 0.22060317633730878
Relative absolute error: 49.50633651075696
Root relative squared error: 76.73693459230205
Weighted TruePositiveRate: 0.6545454545454545
Weighted MatthewsCorrelation: 0.6252368144743857
Weighted FMeasure: 0.6567148933754043
Iteration time: 13.0
Weighted AreaUnderPRC: 0.6604216306176225
Mean absolute error: 0.08182865538968144
Coverage of cases: 92.12121212121212
Instances selection time: 13.0
Test time: 36.0
Accumulative iteration time: 226.0
Weighted Recall: 0.6545454545454545
Weighted FalsePositiveRate: 0.034545454545454546
Kappa statistic: 0.62
Training time: 0.0
		
Iteration: 10
Labeled set size: 279
Unlabelled set size: 216
	
Mean region size: 25.362718089990846
Incorrectly Classified Instances: 32.525252525252526
Correctly Classified Instances: 67.47474747474747
Weighted Precision: 0.6855321485027394
Weighted AreaUnderROC: 0.9436812570145904
Root mean squared error: 0.2179242737202005
Relative absolute error: 49.876442710218726
Root relative squared error: 75.80507686331876
Weighted TruePositiveRate: 0.6747474747474748
Weighted MatthewsCorrelation: 0.6461719950226423
Weighted FMeasure: 0.676223458235068
Iteration time: 13.0
Weighted AreaUnderPRC: 0.6528711609440196
Mean absolute error: 0.08244040117391578
Coverage of cases: 91.11111111111111
Instances selection time: 12.0
Test time: 36.0
Accumulative iteration time: 239.0
Weighted Recall: 0.6747474747474748
Weighted FalsePositiveRate: 0.032525252525252527
Kappa statistic: 0.6422222222222222
Training time: 1.0
		
Iteration: 11
Labeled set size: 299
Unlabelled set size: 196
	
Mean region size: 25.858585858585876
Incorrectly Classified Instances: 30.90909090909091
Correctly Classified Instances: 69.0909090909091
Weighted Precision: 0.6989934457616322
Weighted AreaUnderROC: 0.9463883277216611
Root mean squared error: 0.21337240670913707
Relative absolute error: 49.30641593036046
Root relative squared error: 74.221707453593
Weighted TruePositiveRate: 0.6909090909090909
Weighted MatthewsCorrelation: 0.663210913517852
Weighted FMeasure: 0.6927203797639684
Iteration time: 11.0
Weighted AreaUnderPRC: 0.6671439106824627
Mean absolute error: 0.08149820814935665
Coverage of cases: 91.31313131313131
Instances selection time: 11.0
Test time: 36.0
Accumulative iteration time: 250.0
Weighted Recall: 0.6909090909090909
Weighted FalsePositiveRate: 0.03090909090909091
Kappa statistic: 0.66
Training time: 0.0
		
Iteration: 12
Labeled set size: 319
Unlabelled set size: 176
	
Mean region size: 26.40955004591367
Incorrectly Classified Instances: 30.707070707070706
Correctly Classified Instances: 69.29292929292929
Weighted Precision: 0.7094971505903563
Weighted AreaUnderROC: 0.9500381593714927
Root mean squared error: 0.21157680334521117
Relative absolute error: 49.38725857385903
Root relative squared error: 73.59710584912374
Weighted TruePositiveRate: 0.692929292929293
Weighted MatthewsCorrelation: 0.668245381744171
Weighted FMeasure: 0.6953181880622398
Iteration time: 11.0
Weighted AreaUnderPRC: 0.6655762315147302
Mean absolute error: 0.08163183235348653
Coverage of cases: 93.33333333333333
Instances selection time: 10.0
Test time: 35.0
Accumulative iteration time: 261.0
Weighted Recall: 0.692929292929293
Weighted FalsePositiveRate: 0.03070707070707071
Kappa statistic: 0.6622222222222223
Training time: 1.0
		
Iteration: 13
Labeled set size: 339
Unlabelled set size: 156
	
Mean region size: 26.64830119375575
Incorrectly Classified Instances: 30.90909090909091
Correctly Classified Instances: 69.0909090909091
Weighted Precision: 0.7053388815095789
Weighted AreaUnderROC: 0.9526285072951741
Root mean squared error: 0.20734934233689795
Relative absolute error: 48.660457250172485
Root relative squared error: 72.12658124348334
Weighted TruePositiveRate: 0.6909090909090909
Weighted MatthewsCorrelation: 0.6651135677369199
Weighted FMeasure: 0.6924275840942508
Iteration time: 10.0
Weighted AreaUnderPRC: 0.6905770022453224
Mean absolute error: 0.08043050785152529
Coverage of cases: 93.53535353535354
Instances selection time: 9.0
Test time: 35.0
Accumulative iteration time: 271.0
Weighted Recall: 0.6909090909090909
Weighted FalsePositiveRate: 0.030909090909090914
Kappa statistic: 0.66
Training time: 1.0
		
Iteration: 14
Labeled set size: 359
Unlabelled set size: 136
	
Mean region size: 26.336088154269977
Incorrectly Classified Instances: 31.11111111111111
Correctly Classified Instances: 68.88888888888889
Weighted Precision: 0.6991554873375105
Weighted AreaUnderROC: 0.9500740740740742
Root mean squared error: 0.20925150202162826
Relative absolute error: 48.78913628278992
Root relative squared error: 72.78824852196385
Weighted TruePositiveRate: 0.6888888888888889
Weighted MatthewsCorrelation: 0.6614156597287252
Weighted FMeasure: 0.6899914839472039
Iteration time: 9.0
Weighted AreaUnderPRC: 0.6747919020167318
Mean absolute error: 0.08064320046742188
Coverage of cases: 92.92929292929293
Instances selection time: 8.0
Test time: 34.0
Accumulative iteration time: 280.0
Weighted Recall: 0.6888888888888889
Weighted FalsePositiveRate: 0.031111111111111107
Kappa statistic: 0.6577777777777778
Training time: 1.0
		
Iteration: 15
Labeled set size: 379
Unlabelled set size: 116
	
Mean region size: 26.464646464646464
Incorrectly Classified Instances: 31.717171717171716
Correctly Classified Instances: 68.28282828282828
Weighted Precision: 0.6915318521499016
Weighted AreaUnderROC: 0.951860830527497
Root mean squared error: 0.20756796151679904
Relative absolute error: 48.274664705542584
Root relative squared error: 72.20262804383874
Weighted TruePositiveRate: 0.6828282828282828
Weighted MatthewsCorrelation: 0.6542520849941864
Weighted FMeasure: 0.6838664729940251
Iteration time: 7.0
Weighted AreaUnderPRC: 0.6889523087635149
Mean absolute error: 0.07979283422403784
Coverage of cases: 92.72727272727273
Instances selection time: 6.0
Test time: 34.0
Accumulative iteration time: 287.0
Weighted Recall: 0.6828282828282828
Weighted FalsePositiveRate: 0.03171717171717172
Kappa statistic: 0.6511111111111111
Training time: 1.0
		
Iteration: 16
Labeled set size: 399
Unlabelled set size: 96
	
Mean region size: 26.40955004591368
Incorrectly Classified Instances: 30.90909090909091
Correctly Classified Instances: 69.0909090909091
Weighted Precision: 0.6970958058966837
Weighted AreaUnderROC: 0.9509270482603817
Root mean squared error: 0.20743384549242233
Relative absolute error: 48.886287787147026
Root relative squared error: 72.15597571198548
Weighted TruePositiveRate: 0.6909090909090909
Weighted MatthewsCorrelation: 0.662272255282351
Weighted FMeasure: 0.6917654732072158
Iteration time: 7.0
Weighted AreaUnderPRC: 0.6847239996313299
Mean absolute error: 0.08080378146635925
Coverage of cases: 93.13131313131314
Instances selection time: 6.0
Test time: 34.0
Accumulative iteration time: 294.0
Weighted Recall: 0.6909090909090909
Weighted FalsePositiveRate: 0.030909090909090914
Kappa statistic: 0.66
Training time: 1.0
		
Iteration: 17
Labeled set size: 419
Unlabelled set size: 76
	
Mean region size: 26.86868686868687
Incorrectly Classified Instances: 31.11111111111111
Correctly Classified Instances: 68.88888888888889
Weighted Precision: 0.6943883400977955
Weighted AreaUnderROC: 0.9520179573512907
Root mean squared error: 0.2096054541043434
Relative absolute error: 49.491034886263265
Root relative squared error: 72.91137094599726
Weighted TruePositiveRate: 0.6888888888888889
Weighted MatthewsCorrelation: 0.659682890999361
Weighted FMeasure: 0.6893151587563586
Iteration time: 6.0
Weighted AreaUnderPRC: 0.6835954966662107
Mean absolute error: 0.08180336344836955
Coverage of cases: 93.73737373737374
Instances selection time: 5.0
Test time: 34.0
Accumulative iteration time: 300.0
Weighted Recall: 0.6888888888888889
Weighted FalsePositiveRate: 0.031111111111111107
Kappa statistic: 0.6577777777777778
Training time: 1.0
		
Iteration: 18
Labeled set size: 439
Unlabelled set size: 56
	
Mean region size: 26.42791551882461
Incorrectly Classified Instances: 32.121212121212125
Correctly Classified Instances: 67.87878787878788
Weighted Precision: 0.6821390072653976
Weighted AreaUnderROC: 0.9532480359147025
Root mean squared error: 0.20830598831301259
Relative absolute error: 49.10216883615691
Root relative squared error: 72.45935106536864
Weighted TruePositiveRate: 0.6787878787878788
Weighted MatthewsCorrelation: 0.6471715054316036
Weighted FMeasure: 0.6772926884189883
Iteration time: 4.0
Weighted AreaUnderPRC: 0.694078754676132
Mean absolute error: 0.08116060964654087
Coverage of cases: 93.73737373737374
Instances selection time: 3.0
Test time: 34.0
Accumulative iteration time: 304.0
Weighted Recall: 0.6787878787878788
Weighted FalsePositiveRate: 0.03212121212121212
Kappa statistic: 0.6466666666666667
Training time: 1.0
		
Iteration: 19
Labeled set size: 459
Unlabelled set size: 36
	
Mean region size: 26.372819100091828
Incorrectly Classified Instances: 34.14141414141414
Correctly Classified Instances: 65.85858585858585
Weighted Precision: 0.6605164220324948
Weighted AreaUnderROC: 0.95403367003367
Root mean squared error: 0.20954655884204562
Relative absolute error: 49.61901600750833
Root relative squared error: 72.89088419704933
Weighted TruePositiveRate: 0.6585858585858586
Weighted MatthewsCorrelation: 0.6243207731927944
Weighted FMeasure: 0.6565815649148682
Iteration time: 4.0
Weighted AreaUnderPRC: 0.6947742235432195
Mean absolute error: 0.08201490249174982
Coverage of cases: 93.73737373737374
Instances selection time: 2.0
Test time: 34.0
Accumulative iteration time: 308.0
Weighted Recall: 0.6585858585858586
Weighted FalsePositiveRate: 0.034141414141414146
Kappa statistic: 0.6244444444444445
Training time: 2.0
		
Iteration: 20
Labeled set size: 479
Unlabelled set size: 16
	
Mean region size: 26.207529843893486
Incorrectly Classified Instances: 33.73737373737374
Correctly Classified Instances: 66.26262626262626
Weighted Precision: 0.665229528524146
Weighted AreaUnderROC: 0.9565432098765431
Root mean squared error: 0.20872069402332027
Relative absolute error: 49.4892116822532
Root relative squared error: 72.60360667172617
Weighted TruePositiveRate: 0.6626262626262627
Weighted MatthewsCorrelation: 0.6289526503446814
Weighted FMeasure: 0.6605269259316707
Iteration time: 3.0
Weighted AreaUnderPRC: 0.7077961677965159
Mean absolute error: 0.08180034988802234
Coverage of cases: 93.93939393939394
Instances selection time: 1.0
Test time: 34.0
Accumulative iteration time: 311.0
Weighted Recall: 0.6626262626262627
Weighted FalsePositiveRate: 0.03373737373737373
Kappa statistic: 0.6288888888888889
Training time: 2.0
		
Time end:Wed Nov 01 14.54.11 EET 2017