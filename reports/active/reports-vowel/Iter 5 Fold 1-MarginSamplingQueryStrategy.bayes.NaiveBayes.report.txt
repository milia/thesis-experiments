Wed Nov 01 14.57.02 EET 2017
Dataset: vowel
Test set size: 495
Initial Labelled set size: 99
Initial Unlabelled set size: 396
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Wed Nov 01 14.57.02 EET 2017
		
Iteration: 1
Labeled set size: 99
Unlabelled set size: 396
	
Mean region size: 18.34710743801659
Incorrectly Classified Instances: 55.55555555555556
Correctly Classified Instances: 44.44444444444444
Weighted Precision: 0.47351273428716434
Weighted AreaUnderROC: 0.8730370370370371
Root mean squared error: 0.281789259835505
Relative absolute error: 63.015571446280354
Root relative squared error: 98.02054693785172
Weighted TruePositiveRate: 0.4444444444444444
Weighted MatthewsCorrelation: 0.3960209069922392
Weighted FMeasure: 0.4391600758116458
Iteration time: 23.0
Weighted AreaUnderPRC: 0.47946819640785115
Mean absolute error: 0.10415796933269547
Coverage of cases: 69.29292929292929
Instances selection time: 23.0
Test time: 34.0
Accumulative iteration time: 23.0
Weighted Recall: 0.4444444444444444
Weighted FalsePositiveRate: 0.055555555555555546
Kappa statistic: 0.38888888888888884
Training time: 0.0
		
Iteration: 2
Labeled set size: 119
Unlabelled set size: 376
	
Mean region size: 19.191919191919236
Incorrectly Classified Instances: 49.09090909090909
Correctly Classified Instances: 50.90909090909091
Weighted Precision: 0.5419076999804655
Weighted AreaUnderROC: 0.8874747474747474
Root mean squared error: 0.26661450980341017
Relative absolute error: 58.439878264204616
Root relative squared error: 92.74200190508711
Weighted TruePositiveRate: 0.509090909090909
Weighted MatthewsCorrelation: 0.4714176867704453
Weighted FMeasure: 0.5121179352811495
Iteration time: 21.0
Weighted AreaUnderPRC: 0.5218334625726511
Mean absolute error: 0.09659484010612396
Coverage of cases: 73.33333333333333
Instances selection time: 21.0
Test time: 33.0
Accumulative iteration time: 44.0
Weighted Recall: 0.509090909090909
Weighted FalsePositiveRate: 0.049090909090909095
Kappa statistic: 0.4599999999999999
Training time: 0.0
		
Iteration: 3
Labeled set size: 139
Unlabelled set size: 356
	
Mean region size: 19.044995408631838
Incorrectly Classified Instances: 46.666666666666664
Correctly Classified Instances: 53.333333333333336
Weighted Precision: 0.5752862272935504
Weighted AreaUnderROC: 0.8954837261503928
Root mean squared error: 0.2595876057328048
Relative absolute error: 56.360615932255705
Root relative squared error: 90.29768951119874
Weighted TruePositiveRate: 0.5333333333333333
Weighted MatthewsCorrelation: 0.500592401361138
Weighted FMeasure: 0.5355199845777634
Iteration time: 20.0
Weighted AreaUnderPRC: 0.5478503656935727
Mean absolute error: 0.09315804286323318
Coverage of cases: 73.13131313131314
Instances selection time: 20.0
Test time: 33.0
Accumulative iteration time: 64.0
Weighted Recall: 0.5333333333333333
Weighted FalsePositiveRate: 0.04666666666666666
Kappa statistic: 0.48666666666666664
Training time: 0.0
		
Iteration: 4
Labeled set size: 159
Unlabelled set size: 336
	
Mean region size: 20.404040404040447
Incorrectly Classified Instances: 42.02020202020202
Correctly Classified Instances: 57.97979797979798
Weighted Precision: 0.6455834907238497
Weighted AreaUnderROC: 0.9043097643097642
Root mean squared error: 0.24823574414238436
Relative absolute error: 53.89028838514523
Root relative squared error: 86.34893829724065
Weighted TruePositiveRate: 0.5797979797979798
Weighted MatthewsCorrelation: 0.5611452802424863
Weighted FMeasure: 0.5882785765383967
Iteration time: 18.0
Weighted AreaUnderPRC: 0.5934749493181593
Mean absolute error: 0.08907485683495138
Coverage of cases: 75.55555555555556
Instances selection time: 17.0
Test time: 35.0
Accumulative iteration time: 82.0
Weighted Recall: 0.5797979797979798
Weighted FalsePositiveRate: 0.04202020202020202
Kappa statistic: 0.5377777777777777
Training time: 1.0
		
Iteration: 5
Labeled set size: 179
Unlabelled set size: 316
	
Mean region size: 21.101928374655675
Incorrectly Classified Instances: 39.39393939393939
Correctly Classified Instances: 60.60606060606061
Weighted Precision: 0.6635088308910917
Weighted AreaUnderROC: 0.9098540965207632
Root mean squared error: 0.2417145415763396
Relative absolute error: 52.82466662269179
Root relative squared error: 84.08053444611669
Weighted TruePositiveRate: 0.6060606060606061
Weighted MatthewsCorrelation: 0.5849288434121789
Weighted FMeasure: 0.6083360364462167
Iteration time: 17.0
Weighted AreaUnderPRC: 0.6113811237340739
Mean absolute error: 0.08731349854990433
Coverage of cases: 78.38383838383838
Instances selection time: 17.0
Test time: 35.0
Accumulative iteration time: 99.0
Weighted Recall: 0.6060606060606061
Weighted FalsePositiveRate: 0.03939393939393939
Kappa statistic: 0.5666666666666667
Training time: 0.0
		
Iteration: 6
Labeled set size: 199
Unlabelled set size: 296
	
Mean region size: 21.175390266299342
Incorrectly Classified Instances: 40.0
Correctly Classified Instances: 60.0
Weighted Precision: 0.6729802630207103
Weighted AreaUnderROC: 0.9105364758698092
Root mean squared error: 0.2419149113760935
Relative absolute error: 52.984976032070136
Root relative squared error: 84.15023318968545
Weighted TruePositiveRate: 0.6
Weighted MatthewsCorrelation: 0.5822535850969258
Weighted FMeasure: 0.6013049611100458
Iteration time: 16.0
Weighted AreaUnderPRC: 0.6009996489894559
Mean absolute error: 0.08757847278028177
Coverage of cases: 77.17171717171718
Instances selection time: 16.0
Test time: 34.0
Accumulative iteration time: 115.0
Weighted Recall: 0.6
Weighted FalsePositiveRate: 0.039999999999999994
Kappa statistic: 0.5599999999999999
Training time: 0.0
		
Iteration: 7
Labeled set size: 219
Unlabelled set size: 276
	
Mean region size: 22.31404958677686
Incorrectly Classified Instances: 38.38383838383838
Correctly Classified Instances: 61.61616161616162
Weighted Precision: 0.6817714532378926
Weighted AreaUnderROC: 0.9173916947250281
Root mean squared error: 0.23852580264443654
Relative absolute error: 52.87263013380845
Root relative squared error: 82.9713298783854
Weighted TruePositiveRate: 0.6161616161616161
Weighted MatthewsCorrelation: 0.5987914812407835
Weighted FMeasure: 0.6194126813371104
Iteration time: 16.0
Weighted AreaUnderPRC: 0.6268737650522105
Mean absolute error: 0.0873927770806757
Coverage of cases: 80.20202020202021
Instances selection time: 15.0
Test time: 35.0
Accumulative iteration time: 131.0
Weighted Recall: 0.6161616161616161
Weighted FalsePositiveRate: 0.03838383838383839
Kappa statistic: 0.5777777777777777
Training time: 1.0
		
Iteration: 8
Labeled set size: 239
Unlabelled set size: 256
	
Mean region size: 23.08539944903581
Incorrectly Classified Instances: 40.4040404040404
Correctly Classified Instances: 59.5959595959596
Weighted Precision: 0.6666389017819581
Weighted AreaUnderROC: 0.9188013468013468
Root mean squared error: 0.23680034650192436
Relative absolute error: 53.73155959360084
Root relative squared error: 82.37112902294808
Weighted TruePositiveRate: 0.5959595959595959
Weighted MatthewsCorrelation: 0.5788165143343987
Weighted FMeasure: 0.6010307355011727
Iteration time: 15.0
Weighted AreaUnderPRC: 0.6298673051337242
Mean absolute error: 0.08881249519603503
Coverage of cases: 81.41414141414141
Instances selection time: 14.0
Test time: 34.0
Accumulative iteration time: 146.0
Weighted Recall: 0.5959595959595959
Weighted FalsePositiveRate: 0.040404040404040394
Kappa statistic: 0.5555555555555555
Training time: 1.0
		
Iteration: 9
Labeled set size: 259
Unlabelled set size: 236
	
Mean region size: 23.562901744719916
Incorrectly Classified Instances: 39.7979797979798
Correctly Classified Instances: 60.2020202020202
Weighted Precision: 0.6577346080993575
Weighted AreaUnderROC: 0.9196632996632997
Root mean squared error: 0.2348823503353122
Relative absolute error: 53.91506044123493
Root relative squared error: 81.7039530156518
Weighted TruePositiveRate: 0.602020202020202
Weighted MatthewsCorrelation: 0.5800056630929808
Weighted FMeasure: 0.6041368543500582
Iteration time: 13.0
Weighted AreaUnderPRC: 0.6373358607598579
Mean absolute error: 0.08911580238220707
Coverage of cases: 83.23232323232324
Instances selection time: 12.0
Test time: 34.0
Accumulative iteration time: 159.0
Weighted Recall: 0.602020202020202
Weighted FalsePositiveRate: 0.039797979797979804
Kappa statistic: 0.5622222222222222
Training time: 1.0
		
Iteration: 10
Labeled set size: 279
Unlabelled set size: 216
	
Mean region size: 24.00367309458217
Incorrectly Classified Instances: 39.39393939393939
Correctly Classified Instances: 60.60606060606061
Weighted Precision: 0.6696612351708647
Weighted AreaUnderROC: 0.9203456790123455
Root mean squared error: 0.23508495485728134
Relative absolute error: 53.877509868984646
Root relative squared error: 81.77442910855574
Weighted TruePositiveRate: 0.6060606060606061
Weighted MatthewsCorrelation: 0.5859828885313517
Weighted FMeasure: 0.6068593788171047
Iteration time: 12.0
Weighted AreaUnderPRC: 0.6255374754834011
Mean absolute error: 0.08905373532063635
Coverage of cases: 83.23232323232324
Instances selection time: 12.0
Test time: 34.0
Accumulative iteration time: 171.0
Weighted Recall: 0.6060606060606061
Weighted FalsePositiveRate: 0.03939393939393939
Kappa statistic: 0.5666666666666667
Training time: 0.0
		
Iteration: 11
Labeled set size: 299
Unlabelled set size: 196
	
Mean region size: 24.775022956841124
Incorrectly Classified Instances: 40.80808080808081
Correctly Classified Instances: 59.19191919191919
Weighted Precision: 0.6331591625055848
Weighted AreaUnderROC: 0.9279551066217733
Root mean squared error: 0.23343901261252328
Relative absolute error: 53.64473295419148
Root relative squared error: 81.2018872055979
Weighted TruePositiveRate: 0.591919191919192
Weighted MatthewsCorrelation: 0.5618726348053944
Weighted FMeasure: 0.5872369169961482
Iteration time: 11.0
Weighted AreaUnderPRC: 0.6337690078681777
Mean absolute error: 0.08866898008957326
Coverage of cases: 85.25252525252525
Instances selection time: 10.0
Test time: 35.0
Accumulative iteration time: 182.0
Weighted Recall: 0.591919191919192
Weighted FalsePositiveRate: 0.04080808080808081
Kappa statistic: 0.5511111111111111
Training time: 1.0
		
Iteration: 12
Labeled set size: 319
Unlabelled set size: 176
	
Mean region size: 25.087235996326854
Incorrectly Classified Instances: 39.5959595959596
Correctly Classified Instances: 60.4040404040404
Weighted Precision: 0.6470625642173353
Weighted AreaUnderROC: 0.9272861952861954
Root mean squared error: 0.23089321817795808
Relative absolute error: 54.12721651142676
Root relative squared error: 80.31633123013917
Weighted TruePositiveRate: 0.604040404040404
Weighted MatthewsCorrelation: 0.5766195695013107
Weighted FMeasure: 0.6019480877691784
Iteration time: 10.0
Weighted AreaUnderPRC: 0.6245110368578245
Mean absolute error: 0.08946647357260679
Coverage of cases: 86.06060606060606
Instances selection time: 9.0
Test time: 34.0
Accumulative iteration time: 192.0
Weighted Recall: 0.604040404040404
Weighted FalsePositiveRate: 0.0395959595959596
Kappa statistic: 0.5644444444444444
Training time: 1.0
		
Iteration: 13
Labeled set size: 339
Unlabelled set size: 156
	
Mean region size: 26.005509641873267
Incorrectly Classified Instances: 37.17171717171717
Correctly Classified Instances: 62.82828282828283
Weighted Precision: 0.6732615587420311
Weighted AreaUnderROC: 0.935537598204265
Root mean squared error: 0.2274266451361477
Relative absolute error: 53.853976447776475
Root relative squared error: 79.11048191651871
Weighted TruePositiveRate: 0.6282828282828283
Weighted MatthewsCorrelation: 0.6021678114393683
Weighted FMeasure: 0.6226285128250958
Iteration time: 10.0
Weighted AreaUnderPRC: 0.6392632246435541
Mean absolute error: 0.08901483710376334
Coverage of cases: 88.68686868686869
Instances selection time: 9.0
Test time: 34.0
Accumulative iteration time: 202.0
Weighted Recall: 0.6282828282828283
Weighted FalsePositiveRate: 0.03717171717171717
Kappa statistic: 0.5911111111111111
Training time: 1.0
		
Iteration: 14
Labeled set size: 359
Unlabelled set size: 136
	
Mean region size: 25.78512396694213
Incorrectly Classified Instances: 37.57575757575758
Correctly Classified Instances: 62.42424242424242
Weighted Precision: 0.6661717615093851
Weighted AreaUnderROC: 0.9333692480359148
Root mean squared error: 0.22870970472431948
Relative absolute error: 54.166210109760506
Root relative squared error: 79.55679489047616
Weighted TruePositiveRate: 0.6242424242424243
Weighted MatthewsCorrelation: 0.5976237037566161
Weighted FMeasure: 0.6204022187291116
Iteration time: 8.0
Weighted AreaUnderPRC: 0.6289082170092476
Mean absolute error: 0.08953092580125761
Coverage of cases: 87.87878787878788
Instances selection time: 7.0
Test time: 33.0
Accumulative iteration time: 210.0
Weighted Recall: 0.6242424242424243
Weighted FalsePositiveRate: 0.03757575757575758
Kappa statistic: 0.5866666666666667
Training time: 1.0
		
Iteration: 15
Labeled set size: 379
Unlabelled set size: 116
	
Mean region size: 26.262626262626245
Incorrectly Classified Instances: 36.96969696969697
Correctly Classified Instances: 63.03030303030303
Weighted Precision: 0.6751870959727504
Weighted AreaUnderROC: 0.9400808080808081
Root mean squared error: 0.2225201989962216
Relative absolute error: 53.489391027608555
Root relative squared error: 77.40377196441688
Weighted TruePositiveRate: 0.6303030303030303
Weighted MatthewsCorrelation: 0.607091693286
Weighted FMeasure: 0.6309815587889409
Iteration time: 7.0
Weighted AreaUnderPRC: 0.6524206232282905
Mean absolute error: 0.08841221657456015
Coverage of cases: 90.70707070707071
Instances selection time: 6.0
Test time: 33.0
Accumulative iteration time: 217.0
Weighted Recall: 0.6303030303030303
Weighted FalsePositiveRate: 0.03696969696969697
Kappa statistic: 0.5933333333333333
Training time: 1.0
		
Iteration: 16
Labeled set size: 399
Unlabelled set size: 96
	
Mean region size: 27.23599632690542
Incorrectly Classified Instances: 34.94949494949495
Correctly Classified Instances: 65.05050505050505
Weighted Precision: 0.6913081039289768
Weighted AreaUnderROC: 0.9493961840628508
Root mean squared error: 0.21724272453371568
Relative absolute error: 52.90493651625701
Root relative squared error: 75.5679996089788
Weighted TruePositiveRate: 0.6505050505050505
Weighted MatthewsCorrelation: 0.6291360500339749
Weighted FMeasure: 0.6534071571672574
Iteration time: 7.0
Weighted AreaUnderPRC: 0.6669960385296134
Mean absolute error: 0.08744617605992949
Coverage of cases: 93.13131313131314
Instances selection time: 5.0
Test time: 33.0
Accumulative iteration time: 224.0
Weighted Recall: 0.6505050505050505
Weighted FalsePositiveRate: 0.03494949494949495
Kappa statistic: 0.6155555555555555
Training time: 2.0
		
Iteration: 17
Labeled set size: 419
Unlabelled set size: 76
	
Mean region size: 26.593204775022933
Incorrectly Classified Instances: 36.76767676767677
Correctly Classified Instances: 63.23232323232323
Weighted Precision: 0.6633616970259486
Weighted AreaUnderROC: 0.9478967452300785
Root mean squared error: 0.21815006586545305
Relative absolute error: 52.59220345865522
Root relative squared error: 75.88361878356385
Weighted TruePositiveRate: 0.6323232323232323
Weighted MatthewsCorrelation: 0.6057216655910155
Weighted FMeasure: 0.634055136724082
Iteration time: 5.0
Weighted AreaUnderPRC: 0.664185351481161
Mean absolute error: 0.08692926191513316
Coverage of cases: 93.13131313131314
Instances selection time: 4.0
Test time: 33.0
Accumulative iteration time: 229.0
Weighted Recall: 0.6323232323232323
Weighted FalsePositiveRate: 0.03676767676767677
Kappa statistic: 0.5955555555555555
Training time: 1.0
		
Iteration: 18
Labeled set size: 439
Unlabelled set size: 56
	
Mean region size: 26.33608815426996
Incorrectly Classified Instances: 37.77777777777778
Correctly Classified Instances: 62.22222222222222
Weighted Precision: 0.6395883490693616
Weighted AreaUnderROC: 0.9480763187429855
Root mean squared error: 0.2182435769288597
Relative absolute error: 52.505434434728485
Root relative squared error: 75.9161466577107
Weighted TruePositiveRate: 0.6222222222222222
Weighted MatthewsCorrelation: 0.589882950991353
Weighted FMeasure: 0.6224046177688487
Iteration time: 4.0
Weighted AreaUnderPRC: 0.656041697043223
Mean absolute error: 0.08678584204087408
Coverage of cases: 92.52525252525253
Instances selection time: 3.0
Test time: 33.0
Accumulative iteration time: 233.0
Weighted Recall: 0.6222222222222222
Weighted FalsePositiveRate: 0.03777777777777778
Kappa statistic: 0.5844444444444444
Training time: 1.0
		
Iteration: 19
Labeled set size: 459
Unlabelled set size: 36
	
Mean region size: 26.795224977043155
Incorrectly Classified Instances: 39.39393939393939
Correctly Classified Instances: 60.60606060606061
Weighted Precision: 0.6164171021279418
Weighted AreaUnderROC: 0.9432727272727273
Root mean squared error: 0.22066514151657027
Relative absolute error: 53.63662506379711
Root relative squared error: 76.7584892135316
Weighted TruePositiveRate: 0.6060606060606061
Weighted MatthewsCorrelation: 0.5684156102986107
Weighted FMeasure: 0.6021192121606895
Iteration time: 4.0
Weighted AreaUnderPRC: 0.6488008341403481
Mean absolute error: 0.08865557861784702
Coverage of cases: 92.72727272727273
Instances selection time: 2.0
Test time: 33.0
Accumulative iteration time: 237.0
Weighted Recall: 0.6060606060606061
Weighted FalsePositiveRate: 0.03939393939393939
Kappa statistic: 0.5666666666666667
Training time: 2.0
		
Iteration: 20
Labeled set size: 479
Unlabelled set size: 16
	
Mean region size: 26.44628099173554
Incorrectly Classified Instances: 39.5959595959596
Correctly Classified Instances: 60.4040404040404
Weighted Precision: 0.6108855018613643
Weighted AreaUnderROC: 0.9420426487093152
Root mean squared error: 0.22143205334555438
Relative absolute error: 53.40930499685061
Root relative squared error: 77.02525990938429
Weighted TruePositiveRate: 0.604040404040404
Weighted MatthewsCorrelation: 0.5651217688425249
Weighted FMeasure: 0.599859855326512
Iteration time: 3.0
Weighted AreaUnderPRC: 0.6480237392684185
Mean absolute error: 0.08827984297000156
Coverage of cases: 92.92929292929293
Instances selection time: 1.0
Test time: 33.0
Accumulative iteration time: 240.0
Weighted Recall: 0.604040404040404
Weighted FalsePositiveRate: 0.0395959595959596
Kappa statistic: 0.5644444444444444
Training time: 2.0
		
Time end:Wed Nov 01 14.57.03 EET 2017