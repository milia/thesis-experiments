Wed Nov 01 15.01.33 EET 2017
Dataset: vowel
Test set size: 495
Initial Labelled set size: 99
Initial Unlabelled set size: 396
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Wed Nov 01 15.01.33 EET 2017
		
Iteration: 1
Labeled set size: 99
Unlabelled set size: 396
	
Mean region size: 54.545454545454064
Incorrectly Classified Instances: 62.62626262626262
Correctly Classified Instances: 37.37373737373738
Weighted Precision: 0.4192280442577877
Weighted AreaUnderROC: 0.6555555555555557
Root mean squared error: 0.32141217326661164
Relative absolute error: 72.00000000000041
Root relative squared error: 111.80339887498882
Weighted TruePositiveRate: 0.37373737373737376
Weighted MatthewsCorrelation: 0.32732926767405135
Weighted FMeasure: 0.37933050814080854
Iteration time: 1.0
Weighted AreaUnderPRC: 0.2117599172509516
Mean absolute error: 0.11900826446281136
Coverage of cases: 68.28282828282828
Instances selection time: 1.0
Test time: 10.0
Accumulative iteration time: 1.0
Weighted Recall: 0.37373737373737376
Weighted FalsePositiveRate: 0.06262626262626263
Kappa statistic: 0.31111111111111117
Training time: 0.0
		
Iteration: 2
Labeled set size: 119
Unlabelled set size: 376
	
Mean region size: 45.45454545454594
Incorrectly Classified Instances: 54.74747474747475
Correctly Classified Instances: 45.25252525252525
Weighted Precision: 0.4950992432522536
Weighted AreaUnderROC: 0.6988888888888889
Root mean squared error: 0.30283640918963106
Relative absolute error: 63.58803418803381
Root relative squared error: 105.3418092612575
Weighted TruePositiveRate: 0.45252525252525255
Weighted MatthewsCorrelation: 0.4136039964437731
Weighted FMeasure: 0.4593768013339443
Iteration time: 1.0
Weighted AreaUnderPRC: 0.2710794022225
Mean absolute error: 0.10510418874055243
Coverage of cases: 67.27272727272727
Instances selection time: 1.0
Test time: 10.0
Accumulative iteration time: 2.0
Weighted Recall: 0.45252525252525255
Weighted FalsePositiveRate: 0.05474747474747475
Kappa statistic: 0.3977777777777778
Training time: 0.0
		
Iteration: 3
Labeled set size: 139
Unlabelled set size: 356
	
Mean region size: 36.36363636363658
Incorrectly Classified Instances: 48.484848484848484
Correctly Classified Instances: 51.515151515151516
Weighted Precision: 0.542047288500848
Weighted AreaUnderROC: 0.7333333333333335
Root mean squared error: 0.28659041845855504
Relative absolute error: 56.75555555555531
Root relative squared error: 99.69063257007726
Weighted TruePositiveRate: 0.5151515151515151
Weighted MatthewsCorrelation: 0.4758492289816112
Weighted FMeasure: 0.5179878863907563
Iteration time: 1.0
Weighted AreaUnderPRC: 0.3238809934540346
Mean absolute error: 0.09381083562901765
Coverage of cases: 66.06060606060606
Instances selection time: 1.0
Test time: 11.0
Accumulative iteration time: 3.0
Weighted Recall: 0.5151515151515151
Weighted FalsePositiveRate: 0.048484848484848485
Kappa statistic: 0.4666666666666666
Training time: 0.0
		
Iteration: 4
Labeled set size: 159
Unlabelled set size: 336
	
Mean region size: 27.272727272727032
Incorrectly Classified Instances: 43.63636363636363
Correctly Classified Instances: 56.36363636363637
Weighted Precision: 0.5791895847349019
Weighted AreaUnderROC: 0.76
Root mean squared error: 0.2730406085125076
Relative absolute error: 51.36470588235232
Root relative squared error: 94.97732382796683
Weighted TruePositiveRate: 0.5636363636363636
Weighted MatthewsCorrelation: 0.5244408676107813
Weighted FMeasure: 0.5631240593351238
Iteration time: 1.0
Weighted AreaUnderPRC: 0.36885060485131693
Mean absolute error: 0.08490034030140933
Coverage of cases: 66.26262626262626
Instances selection time: 1.0
Test time: 12.0
Accumulative iteration time: 4.0
Weighted Recall: 0.5636363636363636
Weighted FalsePositiveRate: 0.04363636363636364
Kappa statistic: 0.5199999999999999
Training time: 0.0
		
Iteration: 5
Labeled set size: 179
Unlabelled set size: 316
	
Mean region size: 18.18181818181829
Incorrectly Classified Instances: 40.0
Correctly Classified Instances: 60.0
Weighted Precision: 0.6232332163852198
Weighted AreaUnderROC: 0.7799999999999999
Root mean squared error: 0.2622856357518131
Relative absolute error: 47.24210526315767
Root relative squared error: 91.23620071731183
Weighted TruePositiveRate: 0.6
Weighted MatthewsCorrelation: 0.5660970452487031
Weighted FMeasure: 0.5981609568992241
Iteration time: 1.0
Weighted AreaUnderPRC: 0.4075611282442336
Mean absolute error: 0.07808612440191401
Coverage of cases: 66.06060606060606
Instances selection time: 1.0
Test time: 13.0
Accumulative iteration time: 5.0
Weighted Recall: 0.6
Weighted FalsePositiveRate: 0.04
Kappa statistic: 0.5599999999999999
Training time: 0.0
		
Iteration: 6
Labeled set size: 199
Unlabelled set size: 296
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 36.56565656565657
Correctly Classified Instances: 63.43434343434343
Weighted Precision: 0.6553658471528371
Weighted AreaUnderROC: 0.7988888888888889
Root mean squared error: 0.25145044407343636
Relative absolute error: 43.353439153439126
Root relative squared error: 87.46717341261281
Weighted TruePositiveRate: 0.6343434343434343
Weighted MatthewsCorrelation: 0.6031332603069836
Weighted FMeasure: 0.6325080558262713
Iteration time: 1.0
Weighted AreaUnderPRC: 0.44888704099707877
Mean absolute error: 0.07165857711312298
Coverage of cases: 63.43434343434343
Instances selection time: 1.0
Test time: 15.0
Accumulative iteration time: 6.0
Weighted Recall: 0.6343434343434343
Weighted FalsePositiveRate: 0.03656565656565656
Kappa statistic: 0.5977777777777777
Training time: 0.0
		
Iteration: 7
Labeled set size: 219
Unlabelled set size: 276
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 31.11111111111111
Correctly Classified Instances: 68.88888888888889
Weighted Precision: 0.7113090538573691
Weighted AreaUnderROC: 0.8288888888888889
Root mean squared error: 0.23248522205254124
Relative absolute error: 37.36811594202891
Root relative squared error: 80.87011064176372
Weighted TruePositiveRate: 0.6888888888888889
Weighted MatthewsCorrelation: 0.6641994302339802
Weighted FMeasure: 0.6887270445003195
Iteration time: 1.0
Weighted AreaUnderPRC: 0.5176092508907879
Mean absolute error: 0.06176548089591595
Coverage of cases: 68.88888888888889
Instances selection time: 1.0
Test time: 16.0
Accumulative iteration time: 7.0
Weighted Recall: 0.6888888888888889
Weighted FalsePositiveRate: 0.031111111111111107
Kappa statistic: 0.6577777777777778
Training time: 0.0
		
Iteration: 8
Labeled set size: 239
Unlabelled set size: 256
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 26.262626262626263
Correctly Classified Instances: 73.73737373737374
Weighted Precision: 0.7526968666225315
Weighted AreaUnderROC: 0.8555555555555555
Root mean squared error: 0.21403088437746873
Relative absolute error: 32.01777777777765
Root relative squared error: 74.45075926782434
Weighted TruePositiveRate: 0.7373737373737373
Weighted MatthewsCorrelation: 0.7142599846300975
Weighted FMeasure: 0.73429615981831
Iteration time: 0.0
Weighted AreaUnderPRC: 0.5790589930767747
Mean absolute error: 0.05292194674012869
Coverage of cases: 73.73737373737374
Instances selection time: 0.0
Test time: 17.0
Accumulative iteration time: 7.0
Weighted Recall: 0.7373737373737373
Weighted FalsePositiveRate: 0.02626262626262626
Kappa statistic: 0.711111111111111
Training time: 0.0
		
Iteration: 9
Labeled set size: 259
Unlabelled set size: 236
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 24.646464646464647
Correctly Classified Instances: 75.35353535353535
Weighted Precision: 0.7686581516563353
Weighted AreaUnderROC: 0.8644444444444443
Root mean squared error: 0.2076614168930397
Relative absolute error: 30.08065843621383
Root relative squared error: 72.23513654717469
Weighted TruePositiveRate: 0.7535353535353535
Weighted MatthewsCorrelation: 0.7322075748703114
Weighted FMeasure: 0.7506477629904726
Iteration time: 0.0
Weighted AreaUnderPRC: 0.5992135305117096
Mean absolute error: 0.04972009658878351
Coverage of cases: 75.35353535353535
Instances selection time: 0.0
Test time: 18.0
Accumulative iteration time: 7.0
Weighted Recall: 0.7535353535353535
Weighted FalsePositiveRate: 0.02464646464646465
Kappa statistic: 0.7288888888888888
Training time: 0.0
		
Iteration: 10
Labeled set size: 279
Unlabelled set size: 216
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 22.22222222222222
Correctly Classified Instances: 77.77777777777777
Weighted Precision: 0.7932120467254491
Weighted AreaUnderROC: 0.8777777777777778
Root mean squared error: 0.1974598172247738
Relative absolute error: 27.310344827585936
Root relative squared error: 68.68650456699146
Weighted TruePositiveRate: 0.7777777777777778
Weighted MatthewsCorrelation: 0.760281991531339
Weighted FMeasure: 0.7778644732560157
Iteration time: 0.0
Weighted AreaUnderPRC: 0.6341387808109349
Mean absolute error: 0.04514106583072085
Coverage of cases: 77.77777777777777
Instances selection time: 0.0
Test time: 18.0
Accumulative iteration time: 7.0
Weighted Recall: 0.7777777777777778
Weighted FalsePositiveRate: 0.022222222222222223
Kappa statistic: 0.7555555555555555
Training time: 0.0
		
Iteration: 11
Labeled set size: 299
Unlabelled set size: 196
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 19.7979797979798
Correctly Classified Instances: 80.20202020202021
Weighted Precision: 0.8164326884997513
Weighted AreaUnderROC: 0.8911111111111111
Root mean squared error: 0.1866094749974561
Relative absolute error: 24.55340501792115
Root relative squared error: 64.91220713562235
Weighted TruePositiveRate: 0.802020202020202
Weighted MatthewsCorrelation: 0.7863302744677446
Weighted FMeasure: 0.8013168480365095
Iteration time: 1.0
Weighted AreaUnderPRC: 0.6701964804192886
Mean absolute error: 0.04058414052548977
Coverage of cases: 80.20202020202021
Instances selection time: 1.0
Test time: 18.0
Accumulative iteration time: 8.0
Weighted Recall: 0.802020202020202
Weighted FalsePositiveRate: 0.0197979797979798
Kappa statistic: 0.7822222222222222
Training time: 0.0
		
Iteration: 12
Labeled set size: 319
Unlabelled set size: 176
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 17.97979797979798
Correctly Classified Instances: 82.02020202020202
Weighted Precision: 0.8316693603048528
Weighted AreaUnderROC: 0.901111111111111
Root mean squared error: 0.1780243927667143
Relative absolute error: 22.451851851851625
Root relative squared error: 61.92588162323421
Weighted TruePositiveRate: 0.8202020202020202
Weighted MatthewsCorrelation: 0.8056635460058672
Weighted FMeasure: 0.8201557517085645
Iteration time: 1.0
Weighted AreaUnderPRC: 0.6971948649534051
Mean absolute error: 0.03711049892868061
Coverage of cases: 82.02020202020202
Instances selection time: 1.0
Test time: 19.0
Accumulative iteration time: 9.0
Weighted Recall: 0.8202020202020202
Weighted FalsePositiveRate: 0.01797979797979798
Kappa statistic: 0.8022222222222222
Training time: 0.0
		
Iteration: 13
Labeled set size: 339
Unlabelled set size: 156
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 15.757575757575758
Correctly Classified Instances: 84.24242424242425
Weighted Precision: 0.8504115253160385
Weighted AreaUnderROC: 0.9133333333333333
Root mean squared error: 0.16682726843557322
Relative absolute error: 19.931428571428246
Root relative squared error: 58.03095584887974
Weighted TruePositiveRate: 0.8424242424242424
Weighted MatthewsCorrelation: 0.8290428529906316
Weighted FMeasure: 0.8422910965933513
Iteration time: 0.0
Weighted AreaUnderPRC: 0.7313567529978561
Mean absolute error: 0.032944510035418796
Coverage of cases: 84.24242424242425
Instances selection time: 0.0
Test time: 20.0
Accumulative iteration time: 9.0
Weighted Recall: 0.8424242424242424
Weighted FalsePositiveRate: 0.01575757575757576
Kappa statistic: 0.8266666666666667
Training time: 0.0
		
Iteration: 14
Labeled set size: 359
Unlabelled set size: 136
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 15.151515151515152
Correctly Classified Instances: 84.84848484848484
Weighted Precision: 0.8541486015247247
Weighted AreaUnderROC: 0.9166666666666666
Root mean squared error: 0.16371394127068775
Relative absolute error: 19.144144144143915
Root relative squared error: 56.94798330522535
Weighted TruePositiveRate: 0.8484848484848485
Weighted MatthewsCorrelation: 0.835100226146811
Weighted FMeasure: 0.8485591866931473
Iteration time: 1.0
Weighted AreaUnderPRC: 0.7408511816369237
Mean absolute error: 0.031643213461395106
Coverage of cases: 84.84848484848484
Instances selection time: 1.0
Test time: 21.0
Accumulative iteration time: 10.0
Weighted Recall: 0.8484848484848485
Weighted FalsePositiveRate: 0.015151515151515152
Kappa statistic: 0.8333333333333334
Training time: 0.0
		
Iteration: 15
Labeled set size: 379
Unlabelled set size: 116
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 13.333333333333334
Correctly Classified Instances: 86.66666666666667
Weighted Precision: 0.8699941135369124
Weighted AreaUnderROC: 0.9266666666666665
Root mean squared error: 0.15370234148592446
Relative absolute error: 17.073504273504003
Root relative squared error: 53.46544288761598
Weighted TruePositiveRate: 0.8666666666666667
Weighted MatthewsCorrelation: 0.8544767819002761
Weighted FMeasure: 0.8670090837922133
Iteration time: 0.0
Weighted AreaUnderPRC: 0.7693633286845936
Mean absolute error: 0.028220668220667957
Coverage of cases: 86.66666666666667
Instances selection time: 0.0
Test time: 22.0
Accumulative iteration time: 10.0
Weighted Recall: 0.8666666666666667
Weighted FalsePositiveRate: 0.013333333333333332
Kappa statistic: 0.8533333333333334
Training time: 0.0
		
Iteration: 16
Labeled set size: 399
Unlabelled set size: 96
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 11.919191919191919
Correctly Classified Instances: 88.08080808080808
Weighted Precision: 0.8837801899461979
Weighted AreaUnderROC: 0.9344444444444443
Root mean squared error: 0.14542806619974755
Relative absolute error: 15.44227642276409
Root relative squared error: 50.58723173954432
Weighted TruePositiveRate: 0.8808080808080808
Weighted MatthewsCorrelation: 0.8698630653892073
Weighted FMeasure: 0.8809891146811918
Iteration time: 0.0
Weighted AreaUnderPRC: 0.7925285717064373
Mean absolute error: 0.02552442383927965
Coverage of cases: 88.08080808080808
Instances selection time: 0.0
Test time: 23.0
Accumulative iteration time: 10.0
Weighted Recall: 0.8808080808080808
Weighted FalsePositiveRate: 0.01191919191919192
Kappa statistic: 0.8688888888888889
Training time: 0.0
		
Iteration: 17
Labeled set size: 419
Unlabelled set size: 76
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 10.303030303030303
Correctly Classified Instances: 89.6969696969697
Weighted Precision: 0.8989878124749409
Weighted AreaUnderROC: 0.9433333333333334
Root mean squared error: 0.13530578311972116
Relative absolute error: 13.601550387596843
Root relative squared error: 47.06619007761887
Weighted TruePositiveRate: 0.896969696969697
Weighted MatthewsCorrelation: 0.8871256011324443
Weighted FMeasure: 0.8965534749416517
Iteration time: 0.0
Weighted AreaUnderPRC: 0.8180218108676426
Mean absolute error: 0.022481901467102366
Coverage of cases: 89.6969696969697
Instances selection time: 0.0
Test time: 24.0
Accumulative iteration time: 10.0
Weighted Recall: 0.896969696969697
Weighted FalsePositiveRate: 0.010303030303030302
Kappa statistic: 0.8866666666666667
Training time: 0.0
		
Iteration: 18
Labeled set size: 439
Unlabelled set size: 56
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 10.303030303030303
Correctly Classified Instances: 89.6969696969697
Weighted Precision: 0.8989878124749409
Weighted AreaUnderROC: 0.9433333333333334
Root mean squared error: 0.1353671031945467
Relative absolute error: 13.500740740740651
Root relative squared error: 47.087520298920346
Weighted TruePositiveRate: 0.896969696969697
Weighted MatthewsCorrelation: 0.8871256011324443
Weighted FMeasure: 0.8965534749416517
Iteration time: 1.0
Weighted AreaUnderPRC: 0.8180218108676426
Mean absolute error: 0.022315273951637585
Coverage of cases: 89.6969696969697
Instances selection time: 1.0
Test time: 24.0
Accumulative iteration time: 11.0
Weighted Recall: 0.896969696969697
Weighted FalsePositiveRate: 0.010303030303030302
Kappa statistic: 0.8866666666666667
Training time: 0.0
		
Iteration: 19
Labeled set size: 459
Unlabelled set size: 36
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 9.494949494949495
Correctly Classified Instances: 90.5050505050505
Weighted Precision: 0.9070811513203826
Weighted AreaUnderROC: 0.9477777777777777
Root mean squared error: 0.1300183582609763
Relative absolute error: 12.540425531914895
Root relative squared error: 45.22695647144981
Weighted TruePositiveRate: 0.9050505050505051
Weighted MatthewsCorrelation: 0.8959015335595143
Weighted FMeasure: 0.9043353578965098
Iteration time: 0.0
Weighted AreaUnderPRC: 0.8317493873205425
Mean absolute error: 0.02072797608580988
Coverage of cases: 90.5050505050505
Instances selection time: 0.0
Test time: 25.0
Accumulative iteration time: 11.0
Weighted Recall: 0.9050505050505051
Weighted FalsePositiveRate: 0.009494949494949497
Kappa statistic: 0.8955555555555557
Training time: 0.0
		
Iteration: 20
Labeled set size: 479
Unlabelled set size: 16
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 9.494949494949495
Correctly Classified Instances: 90.5050505050505
Weighted Precision: 0.9079446318861034
Weighted AreaUnderROC: 0.9477777777777777
Root mean squared error: 0.13006784725703927
Relative absolute error: 12.454875283446665
Root relative squared error: 45.24417124558396
Weighted TruePositiveRate: 0.9050505050505051
Weighted MatthewsCorrelation: 0.8962358178706846
Weighted FMeasure: 0.9045198678351314
Iteration time: 0.0
Weighted AreaUnderPRC: 0.8318560293915813
Mean absolute error: 0.0205865707164409
Coverage of cases: 90.5050505050505
Instances selection time: 0.0
Test time: 26.0
Accumulative iteration time: 11.0
Weighted Recall: 0.9050505050505051
Weighted FalsePositiveRate: 0.009494949494949497
Kappa statistic: 0.8955555555555557
Training time: 0.0
		
Time end:Wed Nov 01 15.01.34 EET 2017