Wed Nov 01 14.52.01 EET 2017
Dataset: vowel
Test set size: 495
Initial Labelled set size: 99
Initial Unlabelled set size: 396
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Wed Nov 01 14.52.01 EET 2017
		
Iteration: 1
Labeled set size: 99
Unlabelled set size: 396
	
Mean region size: 82.03856749311254
Incorrectly Classified Instances: 75.15151515151516
Correctly Classified Instances: 24.848484848484848
Weighted Precision: 0.23522560188570388
Weighted AreaUnderROC: 0.7086734006734007
Root mean squared error: 0.28109240350898845
Relative absolute error: 95.92323232323182
Root relative squared error: 97.77814508654578
Weighted TruePositiveRate: 0.24848484848484848
Weighted MatthewsCorrelation: 0.16237264467795706
Weighted FMeasure: 0.2189740743389824
Iteration time: 417.0
Weighted AreaUnderPRC: 0.21252594320416235
Mean absolute error: 0.15855079722848336
Coverage of cases: 95.75757575757575
Instances selection time: 11.0
Test time: 10.0
Accumulative iteration time: 417.0
Weighted Recall: 0.24848484848484848
Weighted FalsePositiveRate: 0.07515151515151515
Kappa statistic: 0.17333333333333334
Training time: 406.0
		
Iteration: 2
Labeled set size: 119
Unlabelled set size: 376
	
Mean region size: 82.18549127639996
Incorrectly Classified Instances: 71.31313131313131
Correctly Classified Instances: 28.68686868686869
Weighted Precision: 0.2959235771314831
Weighted AreaUnderROC: 0.7037283950617286
Root mean squared error: 0.28098654994404865
Relative absolute error: 95.88686868686818
Root relative squared error: 97.74132386654335
Weighted TruePositiveRate: 0.2868686868686869
Weighted MatthewsCorrelation: 0.205582186543367
Weighted FMeasure: 0.24470643957224497
Iteration time: 415.0
Weighted AreaUnderPRC: 0.22119705617733315
Mean absolute error: 0.15849069204441124
Coverage of cases: 95.35353535353535
Instances selection time: 11.0
Test time: 9.0
Accumulative iteration time: 832.0
Weighted Recall: 0.2868686868686869
Weighted FalsePositiveRate: 0.0713131313131313
Kappa statistic: 0.21555555555555558
Training time: 404.0
		
Iteration: 3
Labeled set size: 139
Unlabelled set size: 356
	
Mean region size: 82.46097337006395
Incorrectly Classified Instances: 68.48484848484848
Correctly Classified Instances: 31.515151515151516
Weighted Precision: 0.26673886104433175
Weighted AreaUnderROC: 0.7315308641975308
Root mean squared error: 0.2798058484516572
Relative absolute error: 95.51515151515108
Root relative squared error: 97.3306162117445
Weighted TruePositiveRate: 0.3151515151515151
Weighted MatthewsCorrelation: 0.2176876259697792
Weighted FMeasure: 0.2597900246787899
Iteration time: 424.0
Weighted AreaUnderPRC: 0.24304401622557623
Mean absolute error: 0.15787628349611849
Coverage of cases: 96.36363636363636
Instances selection time: 10.0
Test time: 9.0
Accumulative iteration time: 1256.0
Weighted Recall: 0.3151515151515151
Weighted FalsePositiveRate: 0.06848484848484848
Kappa statistic: 0.24666666666666665
Training time: 414.0
		
Iteration: 4
Labeled set size: 159
Unlabelled set size: 336
	
Mean region size: 82.35078053259836
Incorrectly Classified Instances: 67.67676767676768
Correctly Classified Instances: 32.323232323232325
Weighted Precision: 0.2458179243034655
Weighted AreaUnderROC: 0.7317485970819304
Root mean squared error: 0.2799260296293769
Relative absolute error: 95.54343434343384
Root relative squared error: 97.37242129962684
Weighted TruePositiveRate: 0.32323232323232326
Weighted MatthewsCorrelation: 0.21349174853846037
Weighted FMeasure: 0.2543223673044964
Iteration time: 418.0
Weighted AreaUnderPRC: 0.24116457817341092
Mean absolute error: 0.15792303197261895
Coverage of cases: 95.95959595959596
Instances selection time: 10.0
Test time: 9.0
Accumulative iteration time: 1674.0
Weighted Recall: 0.32323232323232326
Weighted FalsePositiveRate: 0.06767676767676768
Kappa statistic: 0.2555555555555556
Training time: 408.0
		
Iteration: 5
Labeled set size: 179
Unlabelled set size: 316
	
Mean region size: 82.29568411386556
Incorrectly Classified Instances: 67.87878787878788
Correctly Classified Instances: 32.121212121212125
Weighted Precision: 0.25698097101259154
Weighted AreaUnderROC: 0.7279753086419751
Root mean squared error: 0.28033413030305354
Relative absolute error: 95.69292929292882
Root relative squared error: 97.51437934040824
Weighted TruePositiveRate: 0.3212121212121212
Weighted MatthewsCorrelation: 0.21452930418528598
Weighted FMeasure: 0.2473326730104224
Iteration time: 423.0
Weighted AreaUnderPRC: 0.237917935807649
Mean absolute error: 0.1581701310626933
Coverage of cases: 94.74747474747475
Instances selection time: 9.0
Test time: 14.0
Accumulative iteration time: 2097.0
Weighted Recall: 0.3212121212121212
Weighted FalsePositiveRate: 0.06787878787878789
Kappa statistic: 0.25333333333333335
Training time: 414.0
		
Iteration: 6
Labeled set size: 199
Unlabelled set size: 296
	
Mean region size: 82.24058769513277
Incorrectly Classified Instances: 67.67676767676768
Correctly Classified Instances: 32.323232323232325
Weighted Precision: 0.2417764944502912
Weighted AreaUnderROC: 0.7264130190796858
Root mean squared error: 0.2807619651505794
Relative absolute error: 95.83434343434294
Root relative squared error: 97.6632019242712
Weighted TruePositiveRate: 0.32323232323232326
Weighted MatthewsCorrelation: 0.21129133570067496
Weighted FMeasure: 0.24312965737767384
Iteration time: 429.0
Weighted AreaUnderPRC: 0.2375649041358262
Mean absolute error: 0.15840387344519596
Coverage of cases: 94.94949494949495
Instances selection time: 9.0
Test time: 15.0
Accumulative iteration time: 2526.0
Weighted Recall: 0.32323232323232326
Weighted FalsePositiveRate: 0.06767676767676768
Kappa statistic: 0.2555555555555556
Training time: 420.0
		
Iteration: 7
Labeled set size: 219
Unlabelled set size: 276
	
Mean region size: 82.20385674931092
Incorrectly Classified Instances: 62.42424242424242
Correctly Classified Instances: 37.57575757575758
Weighted Precision: 0.29368522096383587
Weighted AreaUnderROC: 0.7615892255892256
Root mean squared error: 0.2793446025020097
Relative absolute error: 95.34545454545398
Root relative squared error: 97.17017155787903
Weighted TruePositiveRate: 0.37575757575757573
Weighted MatthewsCorrelation: 0.2685917022728126
Weighted FMeasure: 0.29335828928556235
Iteration time: 470.0
Weighted AreaUnderPRC: 0.2790436373132088
Mean absolute error: 0.157595792637115
Coverage of cases: 96.56565656565657
Instances selection time: 8.0
Test time: 15.0
Accumulative iteration time: 2996.0
Weighted Recall: 0.37575757575757573
Weighted FalsePositiveRate: 0.062424242424242424
Kappa statistic: 0.3133333333333333
Training time: 462.0
		
Iteration: 8
Labeled set size: 239
Unlabelled set size: 256
	
Mean region size: 82.18549127639999
Incorrectly Classified Instances: 60.4040404040404
Correctly Classified Instances: 39.5959595959596
Weighted Precision: 0.3561216858008302
Weighted AreaUnderROC: 0.7753490460157126
Root mean squared error: 0.2792017746638067
Relative absolute error: 95.2888888888884
Root relative squared error: 97.12048881685907
Weighted TruePositiveRate: 0.39595959595959596
Weighted MatthewsCorrelation: 0.2990536903461497
Weighted FMeasure: 0.30546543984605995
Iteration time: 460.0
Weighted AreaUnderPRC: 0.28827867681604147
Mean absolute error: 0.15750229568411406
Coverage of cases: 96.76767676767676
Instances selection time: 7.0
Test time: 15.0
Accumulative iteration time: 3456.0
Weighted Recall: 0.39595959595959596
Weighted FalsePositiveRate: 0.060404040404040404
Kappa statistic: 0.33555555555555555
Training time: 453.0
		
Iteration: 9
Labeled set size: 259
Unlabelled set size: 236
	
Mean region size: 82.57116620752953
Incorrectly Classified Instances: 58.58585858585859
Correctly Classified Instances: 41.41414141414141
Weighted Precision: 0.34504735714606405
Weighted AreaUnderROC: 0.7845566778900113
Root mean squared error: 0.2788183652578238
Relative absolute error: 95.17575757575705
Root relative squared error: 96.98711964694287
Weighted TruePositiveRate: 0.41414141414141414
Weighted MatthewsCorrelation: 0.30617736496766956
Weighted FMeasure: 0.3133523291029899
Iteration time: 482.0
Weighted AreaUnderPRC: 0.302039747410677
Mean absolute error: 0.15731530177811182
Coverage of cases: 95.55555555555556
Instances selection time: 6.0
Test time: 11.0
Accumulative iteration time: 3938.0
Weighted Recall: 0.41414141414141414
Weighted FalsePositiveRate: 0.05858585858585859
Kappa statistic: 0.3555555555555555
Training time: 476.0
		
Iteration: 10
Labeled set size: 279
Unlabelled set size: 216
	
Mean region size: 82.58953168044046
Incorrectly Classified Instances: 58.38383838383838
Correctly Classified Instances: 41.61616161616162
Weighted Precision: 0.3791411688435779
Weighted AreaUnderROC: 0.7888148148148147
Root mean squared error: 0.2787009740527371
Relative absolute error: 95.13131313131252
Root relative squared error: 96.94628505255483
Weighted TruePositiveRate: 0.4161616161616162
Weighted MatthewsCorrelation: 0.309960030834794
Weighted FMeasure: 0.30765739235083434
Iteration time: 449.0
Weighted AreaUnderPRC: 0.29955054893086225
Mean absolute error: 0.15724183988646798
Coverage of cases: 96.56565656565657
Instances selection time: 5.0
Test time: 11.0
Accumulative iteration time: 4387.0
Weighted Recall: 0.4161616161616162
Weighted FalsePositiveRate: 0.058383838383838385
Kappa statistic: 0.35777777777777775
Training time: 444.0
		
Iteration: 11
Labeled set size: 299
Unlabelled set size: 196
	
Mean region size: 82.40587695133117
Incorrectly Classified Instances: 57.37373737373738
Correctly Classified Instances: 42.62626262626262
Weighted Precision: 0.4247470068492612
Weighted AreaUnderROC: 0.8028754208754207
Root mean squared error: 0.27771612062260026
Relative absolute error: 94.77979797979737
Root relative squared error: 96.60370325248202
Weighted TruePositiveRate: 0.4262626262626263
Weighted MatthewsCorrelation: 0.3353879659637778
Weighted FMeasure: 0.33827225070779277
Iteration time: 447.0
Weighted AreaUnderPRC: 0.3216187544278222
Mean absolute error: 0.15666082310710408
Coverage of cases: 95.95959595959596
Instances selection time: 5.0
Test time: 10.0
Accumulative iteration time: 4834.0
Weighted Recall: 0.4262626262626263
Weighted FalsePositiveRate: 0.05737373737373737
Kappa statistic: 0.36888888888888893
Training time: 442.0
		
Iteration: 12
Labeled set size: 319
Unlabelled set size: 176
	
Mean region size: 82.2405876951328
Incorrectly Classified Instances: 54.94949494949495
Correctly Classified Instances: 45.05050505050505
Weighted Precision: 0.4206721718601333
Weighted AreaUnderROC: 0.8218451178451178
Root mean squared error: 0.2768812951417808
Relative absolute error: 94.5010101010094
Root relative squared error: 96.31330875598721
Weighted TruePositiveRate: 0.4505050505050505
Weighted MatthewsCorrelation: 0.36055552892474846
Weighted FMeasure: 0.37654900085969073
Iteration time: 454.0
Weighted AreaUnderPRC: 0.34038293527319985
Mean absolute error: 0.15620001669588432
Coverage of cases: 95.15151515151516
Instances selection time: 4.0
Test time: 11.0
Accumulative iteration time: 5288.0
Weighted Recall: 0.4505050505050505
Weighted FalsePositiveRate: 0.05494949494949494
Kappa statistic: 0.39555555555555555
Training time: 450.0
		
Iteration: 13
Labeled set size: 339
Unlabelled set size: 156
	
Mean region size: 82.2405876951328
Incorrectly Classified Instances: 53.13131313131313
Correctly Classified Instances: 46.86868686868687
Weighted Precision: 0.4310235318041642
Weighted AreaUnderROC: 0.8394971941638609
Root mean squared error: 0.2758813635517175
Relative absolute error: 94.16969696969633
Root relative squared error: 95.96548200980227
Weighted TruePositiveRate: 0.4686868686868687
Weighted MatthewsCorrelation: 0.3684313849554498
Weighted FMeasure: 0.38293587337785845
Iteration time: 466.0
Weighted AreaUnderPRC: 0.3616951798321014
Mean absolute error: 0.15565239168544948
Coverage of cases: 96.36363636363636
Instances selection time: 4.0
Test time: 11.0
Accumulative iteration time: 5754.0
Weighted Recall: 0.4686868686868687
Weighted FalsePositiveRate: 0.053131313131313126
Kappa statistic: 0.41555555555555557
Training time: 462.0
		
Iteration: 14
Labeled set size: 359
Unlabelled set size: 136
	
Mean region size: 82.20385674931093
Incorrectly Classified Instances: 52.92929292929293
Correctly Classified Instances: 47.07070707070707
Weighted Precision: 0.39422235273782774
Weighted AreaUnderROC: 0.852
Root mean squared error: 0.27509327933350936
Relative absolute error: 93.91111111111044
Root relative squared error: 95.69134648686949
Weighted TruePositiveRate: 0.4707070707070707
Weighted MatthewsCorrelation: 0.38134264831974707
Weighted FMeasure: 0.41232729190483924
Iteration time: 468.0
Weighted AreaUnderPRC: 0.3860502354686794
Mean absolute error: 0.15522497704315874
Coverage of cases: 96.96969696969697
Instances selection time: 4.0
Test time: 10.0
Accumulative iteration time: 6222.0
Weighted Recall: 0.4707070707070707
Weighted FalsePositiveRate: 0.05292929292929295
Kappa statistic: 0.4177777777777778
Training time: 464.0
		
Iteration: 15
Labeled set size: 379
Unlabelled set size: 116
	
Mean region size: 82.07529843893444
Incorrectly Classified Instances: 51.313131313131315
Correctly Classified Instances: 48.686868686868685
Weighted Precision: 0.40700319882586833
Weighted AreaUnderROC: 0.8715106621773289
Root mean squared error: 0.2737876262012979
Relative absolute error: 93.4545454545448
Root relative squared error: 95.23717433635815
Weighted TruePositiveRate: 0.4868686868686869
Weighted MatthewsCorrelation: 0.40120283951073904
Weighted FMeasure: 0.43760663183241905
Iteration time: 474.0
Weighted AreaUnderPRC: 0.4077350377717931
Mean absolute error: 0.1544703230653643
Coverage of cases: 96.36363636363636
Instances selection time: 3.0
Test time: 10.0
Accumulative iteration time: 6696.0
Weighted Recall: 0.4868686868686869
Weighted FalsePositiveRate: 0.051313131313131324
Kappa statistic: 0.4355555555555556
Training time: 471.0
		
Iteration: 16
Labeled set size: 399
Unlabelled set size: 96
	
Mean region size: 82.07529843893444
Incorrectly Classified Instances: 49.494949494949495
Correctly Classified Instances: 50.505050505050505
Weighted Precision: 0.4249258364365578
Weighted AreaUnderROC: 0.8806150392817059
Root mean squared error: 0.27302596549401087
Relative absolute error: 93.20404040403979
Root relative squared error: 94.97223024603717
Weighted TruePositiveRate: 0.5050505050505051
Weighted MatthewsCorrelation: 0.4210618493431354
Weighted FMeasure: 0.4556755138987849
Iteration time: 485.0
Weighted AreaUnderPRC: 0.42283748513031644
Mean absolute error: 0.15405626513064527
Coverage of cases: 96.76767676767676
Instances selection time: 2.0
Test time: 10.0
Accumulative iteration time: 7181.0
Weighted Recall: 0.5050505050505051
Weighted FalsePositiveRate: 0.04949494949494951
Kappa statistic: 0.45555555555555555
Training time: 483.0
		
Iteration: 17
Labeled set size: 419
Unlabelled set size: 76
	
Mean region size: 82.13039485766723
Incorrectly Classified Instances: 48.08080808080808
Correctly Classified Instances: 51.91919191919192
Weighted Precision: 0.47591116563964225
Weighted AreaUnderROC: 0.890246913580247
Root mean squared error: 0.27188438633901707
Relative absolute error: 92.81616161616091
Root relative squared error: 94.57513131753058
Weighted TruePositiveRate: 0.5191919191919192
Weighted MatthewsCorrelation: 0.4501634101858896
Weighted FMeasure: 0.4867095899909347
Iteration time: 494.0
Weighted AreaUnderPRC: 0.43705544933703505
Mean absolute error: 0.1534151431672091
Coverage of cases: 96.96969696969697
Instances selection time: 2.0
Test time: 10.0
Accumulative iteration time: 7675.0
Weighted Recall: 0.5191919191919192
Weighted FalsePositiveRate: 0.04808080808080808
Kappa statistic: 0.4711111111111111
Training time: 492.0
		
Iteration: 18
Labeled set size: 439
Unlabelled set size: 56
	
Mean region size: 82.20385674931093
Incorrectly Classified Instances: 46.86868686868687
Correctly Classified Instances: 53.13131313131313
Weighted Precision: 0.5822207813328321
Weighted AreaUnderROC: 0.9051380471380471
Root mean squared error: 0.26968962528374596
Relative absolute error: 92.0929292929287
Root relative squared error: 93.81168249353652
Weighted TruePositiveRate: 0.5313131313131313
Weighted MatthewsCorrelation: 0.4888411339814432
Weighted FMeasure: 0.5146937026433013
Iteration time: 493.0
Weighted AreaUnderPRC: 0.4711042144850009
Mean absolute error: 0.15221971783955254
Coverage of cases: 99.79797979797979
Instances selection time: 1.0
Test time: 11.0
Accumulative iteration time: 8168.0
Weighted Recall: 0.5313131313131313
Weighted FalsePositiveRate: 0.046868686868686865
Kappa statistic: 0.4844444444444444
Training time: 492.0
		
Iteration: 19
Labeled set size: 459
Unlabelled set size: 36
	
Mean region size: 82.0569329660235
Incorrectly Classified Instances: 46.06060606060606
Correctly Classified Instances: 53.93939393939394
Weighted Precision: 0.5861514070596566
Weighted AreaUnderROC: 0.9129113355780024
Root mean squared error: 0.2692408144283243
Relative absolute error: 91.94343434343384
Root relative squared error: 93.65556339394502
Weighted TruePositiveRate: 0.5393939393939394
Weighted MatthewsCorrelation: 0.49659892413838697
Weighted FMeasure: 0.523373313100493
Iteration time: 502.0
Weighted AreaUnderPRC: 0.482279395899731
Mean absolute error: 0.1519726187494784
Coverage of cases: 99.5959595959596
Instances selection time: 1.0
Test time: 10.0
Accumulative iteration time: 8670.0
Weighted Recall: 0.5393939393939394
Weighted FalsePositiveRate: 0.04606060606060605
Kappa statistic: 0.49333333333333335
Training time: 501.0
		
Iteration: 20
Labeled set size: 479
Unlabelled set size: 16
	
Mean region size: 81.92837465564699
Incorrectly Classified Instances: 42.82828282828283
Correctly Classified Instances: 57.17171717171717
Weighted Precision: 0.5849726892315342
Weighted AreaUnderROC: 0.9163322109988776
Root mean squared error: 0.2686914003887221
Relative absolute error: 91.76969696969645
Root relative squared error: 93.46444942214714
Weighted TruePositiveRate: 0.5717171717171717
Weighted MatthewsCorrelation: 0.5324977329319908
Weighted FMeasure: 0.5701461989704513
Iteration time: 509.0
Weighted AreaUnderPRC: 0.5082751557755267
Mean absolute error: 0.15168544953668933
Coverage of cases: 99.79797979797979
Instances selection time: 0.0
Test time: 11.0
Accumulative iteration time: 9179.0
Weighted Recall: 0.5717171717171717
Weighted FalsePositiveRate: 0.04282828282828283
Kappa statistic: 0.5288888888888889
Training time: 509.0
		
Time end:Wed Nov 01 14.52.12 EET 2017