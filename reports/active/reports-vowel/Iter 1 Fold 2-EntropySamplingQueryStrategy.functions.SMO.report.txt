Wed Nov 01 14.48.36 EET 2017
Dataset: vowel
Test set size: 495
Initial Labelled set size: 99
Initial Unlabelled set size: 396
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Nov 01 14.48.36 EET 2017
		
Iteration: 1
Labeled set size: 99
Unlabelled set size: 396
	
Mean region size: 82.38751147842021
Incorrectly Classified Instances: 74.74747474747475
Correctly Classified Instances: 25.252525252525253
Weighted Precision: 0.27880355401317025
Weighted AreaUnderROC: 0.7017441077441079
Root mean squared error: 0.2816756113739316
Relative absolute error: 96.12929292929222
Root relative squared error: 97.98101425882564
Weighted TruePositiveRate: 0.25252525252525254
Weighted MatthewsCorrelation: 0.18398292769354094
Weighted FMeasure: 0.24449800867430038
Iteration time: 768.0
Weighted AreaUnderPRC: 0.2020098274470678
Mean absolute error: 0.1588913932715584
Coverage of cases: 95.35353535353535
Instances selection time: 21.0
Test time: 25.0
Accumulative iteration time: 768.0
Weighted Recall: 0.25252525252525254
Weighted FalsePositiveRate: 0.07474747474747474
Kappa statistic: 0.1777777777777778
Training time: 747.0
		
Iteration: 2
Labeled set size: 119
Unlabelled set size: 376
	
Mean region size: 82.13039485766718
Incorrectly Classified Instances: 74.54545454545455
Correctly Classified Instances: 25.454545454545453
Weighted Precision: 0.27661082125656894
Weighted AreaUnderROC: 0.737665544332211
Root mean squared error: 0.27954252766014237
Relative absolute error: 95.39393939393877
Root relative squared error: 97.23901993153332
Weighted TruePositiveRate: 0.2545454545454545
Weighted MatthewsCorrelation: 0.18783971294673743
Weighted FMeasure: 0.2557078569206841
Iteration time: 721.0
Weighted AreaUnderPRC: 0.22404491841909108
Mean absolute error: 0.15767593288254442
Coverage of cases: 97.37373737373737
Instances selection time: 22.0
Test time: 24.0
Accumulative iteration time: 1489.0
Weighted Recall: 0.2545454545454545
Weighted FalsePositiveRate: 0.07454545454545454
Kappa statistic: 0.17999999999999997
Training time: 699.0
		
Iteration: 3
Labeled set size: 139
Unlabelled set size: 356
	
Mean region size: 82.09366391184535
Incorrectly Classified Instances: 73.73737373737374
Correctly Classified Instances: 26.262626262626263
Weighted Precision: 0.3052043624804485
Weighted AreaUnderROC: 0.742502805836139
Root mean squared error: 0.27932438930010595
Relative absolute error: 95.32121212121142
Root relative squared error: 97.16314038462875
Weighted TruePositiveRate: 0.26262626262626265
Weighted MatthewsCorrelation: 0.20511708019262762
Weighted FMeasure: 0.2700899344960888
Iteration time: 723.0
Weighted AreaUnderPRC: 0.23712734777948183
Mean absolute error: 0.15755572251440003
Coverage of cases: 96.96969696969697
Instances selection time: 6.0
Test time: 12.0
Accumulative iteration time: 2212.0
Weighted Recall: 0.26262626262626265
Weighted FalsePositiveRate: 0.07373737373737374
Kappa statistic: 0.1888888888888889
Training time: 717.0
		
Iteration: 4
Labeled set size: 159
Unlabelled set size: 336
	
Mean region size: 82.13039485766718
Incorrectly Classified Instances: 74.34343434343434
Correctly Classified Instances: 25.656565656565657
Weighted Precision: 0.327069251440535
Weighted AreaUnderROC: 0.7574567901234567
Root mean squared error: 0.27861426011864193
Relative absolute error: 95.08686868686806
Root relative squared error: 96.91612156352724
Weighted TruePositiveRate: 0.25656565656565655
Weighted MatthewsCorrelation: 0.20130662557798334
Weighted FMeasure: 0.2596514341100476
Iteration time: 642.0
Weighted AreaUnderPRC: 0.24060681448710056
Mean absolute error: 0.15716837799482425
Coverage of cases: 97.37373737373737
Instances selection time: 5.0
Test time: 13.0
Accumulative iteration time: 2854.0
Weighted Recall: 0.25656565656565655
Weighted FalsePositiveRate: 0.07434343434343434
Kappa statistic: 0.1822222222222222
Training time: 637.0
		
Iteration: 5
Labeled set size: 179
Unlabelled set size: 316
	
Mean region size: 82.05693296602345
Incorrectly Classified Instances: 70.1010101010101
Correctly Classified Instances: 29.8989898989899
Weighted Precision: 0.32929533730888727
Weighted AreaUnderROC: 0.7880224466891134
Root mean squared error: 0.27714933502114536
Relative absolute error: 94.57777777777711
Root relative squared error: 96.40654657346757
Weighted TruePositiveRate: 0.298989898989899
Weighted MatthewsCorrelation: 0.23868200385839378
Weighted FMeasure: 0.2971178378310569
Iteration time: 640.0
Weighted AreaUnderPRC: 0.2777150268646247
Mean absolute error: 0.1563269054178144
Coverage of cases: 97.97979797979798
Instances selection time: 11.0
Test time: 11.0
Accumulative iteration time: 3494.0
Weighted Recall: 0.298989898989899
Weighted FalsePositiveRate: 0.07010101010101011
Kappa statistic: 0.22888888888888886
Training time: 629.0
		
Iteration: 6
Labeled set size: 199
Unlabelled set size: 296
	
Mean region size: 81.92837465564698
Incorrectly Classified Instances: 69.8989898989899
Correctly Classified Instances: 30.1010101010101
Weighted Precision: 0.32730465247502183
Weighted AreaUnderROC: 0.8017014590347924
Root mean squared error: 0.2761576290286032
Relative absolute error: 94.25454545454484
Root relative squared error: 96.06158110584373
Weighted TruePositiveRate: 0.301010101010101
Weighted MatthewsCorrelation: 0.2398891864286456
Weighted FMeasure: 0.3001841427917123
Iteration time: 696.0
Weighted AreaUnderPRC: 0.2934271933287969
Mean absolute error: 0.15579263711495114
Coverage of cases: 97.97979797979798
Instances selection time: 9.0
Test time: 15.0
Accumulative iteration time: 4190.0
Weighted Recall: 0.301010101010101
Weighted FalsePositiveRate: 0.0698989898989899
Kappa statistic: 0.2311111111111111
Training time: 687.0
		
Iteration: 7
Labeled set size: 219
Unlabelled set size: 276
	
Mean region size: 82.02020202020161
Incorrectly Classified Instances: 69.4949494949495
Correctly Classified Instances: 30.505050505050505
Weighted Precision: 0.34180481546560176
Weighted AreaUnderROC: 0.8170258136924804
Root mean squared error: 0.27504450093522453
Relative absolute error: 93.87070707070657
Root relative squared error: 95.67437889455805
Weighted TruePositiveRate: 0.30505050505050507
Weighted MatthewsCorrelation: 0.250513146273976
Weighted FMeasure: 0.3134744863928941
Iteration time: 697.0
Weighted AreaUnderPRC: 0.31511062335820705
Mean absolute error: 0.1551581935053011
Coverage of cases: 97.97979797979798
Instances selection time: 5.0
Test time: 10.0
Accumulative iteration time: 4887.0
Weighted Recall: 0.30505050505050507
Weighted FalsePositiveRate: 0.06949494949494951
Kappa statistic: 0.23555555555555557
Training time: 692.0
		
Iteration: 8
Labeled set size: 239
Unlabelled set size: 256
	
Mean region size: 82.02020202020162
Incorrectly Classified Instances: 64.84848484848484
Correctly Classified Instances: 35.15151515151515
Weighted Precision: 0.3779650779488357
Weighted AreaUnderROC: 0.8260965207631875
Root mean squared error: 0.2745779100898913
Relative absolute error: 93.71313131313077
Root relative squared error: 95.51207501582805
Weighted TruePositiveRate: 0.3515151515151515
Weighted MatthewsCorrelation: 0.296846113990188
Weighted FMeasure: 0.3555733237554041
Iteration time: 662.0
Weighted AreaUnderPRC: 0.34365845717785803
Mean absolute error: 0.15489773770765516
Coverage of cases: 96.56565656565657
Instances selection time: 4.0
Test time: 11.0
Accumulative iteration time: 5549.0
Weighted Recall: 0.3515151515151515
Weighted FalsePositiveRate: 0.06484848484848484
Kappa statistic: 0.2866666666666667
Training time: 658.0
		
Iteration: 9
Labeled set size: 259
Unlabelled set size: 236
	
Mean region size: 82.11202938475627
Incorrectly Classified Instances: 63.43434343434343
Correctly Classified Instances: 36.56565656565657
Weighted Precision: 0.4185311148180239
Weighted AreaUnderROC: 0.8417328843995511
Root mean squared error: 0.273655209645664
Relative absolute error: 93.40202020201968
Root relative squared error: 95.19111316562922
Weighted TruePositiveRate: 0.3656565656565657
Weighted MatthewsCorrelation: 0.32326849918990413
Weighted FMeasure: 0.3771438872347803
Iteration time: 462.0
Weighted AreaUnderPRC: 0.36429327666379785
Mean absolute error: 0.15438350446614924
Coverage of cases: 97.77777777777777
Instances selection time: 4.0
Test time: 10.0
Accumulative iteration time: 6011.0
Weighted Recall: 0.3656565656565657
Weighted FalsePositiveRate: 0.06343434343434343
Kappa statistic: 0.3022222222222222
Training time: 458.0
		
Iteration: 10
Labeled set size: 279
Unlabelled set size: 216
	
Mean region size: 82.2405876951328
Incorrectly Classified Instances: 64.24242424242425
Correctly Classified Instances: 35.75757575757576
Weighted Precision: 0.39199945191586527
Weighted AreaUnderROC: 0.8474186307519641
Root mean squared error: 0.27335509449742473
Relative absolute error: 93.31717171717123
Root relative squared error: 95.08671794846617
Weighted TruePositiveRate: 0.3575757575757576
Weighted MatthewsCorrelation: 0.3083757702258103
Weighted FMeasure: 0.36768280943959836
Iteration time: 495.0
Weighted AreaUnderPRC: 0.3571376414952523
Mean absolute error: 0.15424325903664765
Coverage of cases: 99.5959595959596
Instances selection time: 3.0
Test time: 11.0
Accumulative iteration time: 6506.0
Weighted Recall: 0.3575757575757576
Weighted FalsePositiveRate: 0.06424242424242424
Kappa statistic: 0.2933333333333334
Training time: 492.0
		
Iteration: 11
Labeled set size: 299
Unlabelled set size: 196
	
Mean region size: 82.0569329660235
Incorrectly Classified Instances: 60.4040404040404
Correctly Classified Instances: 39.5959595959596
Weighted Precision: 0.4268033406687635
Weighted AreaUnderROC: 0.8589517396184063
Root mean squared error: 0.2726443372242203
Relative absolute error: 93.06262626262583
Root relative squared error: 94.83948064531197
Weighted TruePositiveRate: 0.39595959595959596
Weighted MatthewsCorrelation: 0.3486174786730654
Weighted FMeasure: 0.4038135793770905
Iteration time: 530.0
Weighted AreaUnderPRC: 0.378527680098316
Mean absolute error: 0.15382252274814284
Coverage of cases: 99.5959595959596
Instances selection time: 4.0
Test time: 10.0
Accumulative iteration time: 7036.0
Weighted Recall: 0.39595959595959596
Weighted FalsePositiveRate: 0.06040404040404041
Kappa statistic: 0.33555555555555555
Training time: 526.0
		
Iteration: 12
Labeled set size: 319
Unlabelled set size: 176
	
Mean region size: 82.09366391184535
Incorrectly Classified Instances: 57.97979797979798
Correctly Classified Instances: 42.02020202020202
Weighted Precision: 0.45069908169664163
Weighted AreaUnderROC: 0.8702491582491582
Root mean squared error: 0.2720393137347231
Relative absolute error: 92.8606060606056
Root relative squared error: 94.6290228961934
Weighted TruePositiveRate: 0.4202020202020202
Weighted MatthewsCorrelation: 0.37399190198451926
Weighted FMeasure: 0.42450263812337125
Iteration time: 617.0
Weighted AreaUnderPRC: 0.4011314253051241
Mean absolute error: 0.15348860505885323
Coverage of cases: 99.79797979797979
Instances selection time: 7.0
Test time: 19.0
Accumulative iteration time: 7653.0
Weighted Recall: 0.4202020202020202
Weighted FalsePositiveRate: 0.05797979797979798
Kappa statistic: 0.3622222222222222
Training time: 610.0
		
Iteration: 13
Labeled set size: 339
Unlabelled set size: 156
	
Mean region size: 82.05693296602351
Incorrectly Classified Instances: 57.37373737373738
Correctly Classified Instances: 42.62626262626262
Weighted Precision: 0.4665549581981281
Weighted AreaUnderROC: 0.8753468013468012
Root mean squared error: 0.2716346200618965
Relative absolute error: 92.73131313131269
Root relative squared error: 94.48824998250637
Weighted TruePositiveRate: 0.4262626262626263
Weighted MatthewsCorrelation: 0.38538139679648087
Weighted FMeasure: 0.4359729961921669
Iteration time: 654.0
Weighted AreaUnderPRC: 0.4110413428177724
Mean absolute error: 0.1532748977377079
Coverage of cases: 100.0
Instances selection time: 3.0
Test time: 10.0
Accumulative iteration time: 8307.0
Weighted Recall: 0.4262626262626263
Weighted FalsePositiveRate: 0.05737373737373738
Kappa statistic: 0.36888888888888893
Training time: 651.0
		
Iteration: 14
Labeled set size: 359
Unlabelled set size: 136
	
Mean region size: 82.07529843893442
Incorrectly Classified Instances: 54.343434343434346
Correctly Classified Instances: 45.656565656565654
Weighted Precision: 0.49864840299287444
Weighted AreaUnderROC: 0.8841818181818182
Root mean squared error: 0.2711534297604205
Relative absolute error: 92.57373737373688
Root relative squared error: 94.32086767503519
Weighted TruePositiveRate: 0.45656565656565656
Weighted MatthewsCorrelation: 0.4188392459106113
Weighted FMeasure: 0.4646391765259438
Iteration time: 635.0
Weighted AreaUnderPRC: 0.4313702196094463
Mean absolute error: 0.15301444194006195
Coverage of cases: 100.0
Instances selection time: 10.0
Test time: 18.0
Accumulative iteration time: 8942.0
Weighted Recall: 0.45656565656565656
Weighted FalsePositiveRate: 0.054343434343434346
Kappa statistic: 0.40222222222222226
Training time: 625.0
		
Iteration: 15
Labeled set size: 379
Unlabelled set size: 116
	
Mean region size: 82.1854912764
Incorrectly Classified Instances: 52.92929292929293
Correctly Classified Instances: 47.07070707070707
Weighted Precision: 0.5044499439934775
Weighted AreaUnderROC: 0.8886823793490459
Root mean squared error: 0.27075817522511997
Relative absolute error: 92.44444444444389
Root relative squared error: 94.18337817045847
Weighted TruePositiveRate: 0.4707070707070707
Weighted MatthewsCorrelation: 0.43066431336919747
Weighted FMeasure: 0.47590930449122254
Iteration time: 914.0
Weighted AreaUnderPRC: 0.441112998977365
Mean absolute error: 0.1528007346189165
Coverage of cases: 99.39393939393939
Instances selection time: 8.0
Test time: 10.0
Accumulative iteration time: 9856.0
Weighted Recall: 0.4707070707070707
Weighted FalsePositiveRate: 0.05292929292929293
Kappa statistic: 0.4177777777777778
Training time: 906.0
		
Iteration: 16
Labeled set size: 399
Unlabelled set size: 96
	
Mean region size: 82.24058769513277
Incorrectly Classified Instances: 51.515151515151516
Correctly Classified Instances: 48.484848484848484
Weighted Precision: 0.5109783843526148
Weighted AreaUnderROC: 0.8915061728395063
Root mean squared error: 0.27052307933975595
Relative absolute error: 92.36767676767619
Root relative squared error: 94.1015999391672
Weighted TruePositiveRate: 0.48484848484848486
Weighted MatthewsCorrelation: 0.44328153107114965
Weighted FMeasure: 0.48885289078545524
Iteration time: 937.0
Weighted AreaUnderPRC: 0.4581039050281918
Mean absolute error: 0.1526738458969864
Coverage of cases: 99.79797979797979
Instances selection time: 2.0
Test time: 16.0
Accumulative iteration time: 10793.0
Weighted Recall: 0.48484848484848486
Weighted FalsePositiveRate: 0.05151515151515152
Kappa statistic: 0.43333333333333335
Training time: 935.0
		
Iteration: 17
Labeled set size: 419
Unlabelled set size: 76
	
Mean region size: 82.22222222222186
Incorrectly Classified Instances: 45.85858585858586
Correctly Classified Instances: 54.14141414141414
Weighted Precision: 0.5589992108436114
Weighted AreaUnderROC: 0.9028193041526376
Root mean squared error: 0.2698863077271134
Relative absolute error: 92.15757575757507
Root relative squared error: 93.88009858818543
Weighted TruePositiveRate: 0.5414141414141415
Weighted MatthewsCorrelation: 0.5014707733777011
Weighted FMeasure: 0.5414408581176406
Iteration time: 965.0
Weighted AreaUnderPRC: 0.4888793163869407
Mean absolute error: 0.15232657150012507
Coverage of cases: 100.0
Instances selection time: 8.0
Test time: 14.0
Accumulative iteration time: 11758.0
Weighted Recall: 0.5414141414141415
Weighted FalsePositiveRate: 0.045858585858585856
Kappa statistic: 0.4955555555555556
Training time: 957.0
		
Iteration: 18
Labeled set size: 439
Unlabelled set size: 56
	
Mean region size: 82.22222222222186
Incorrectly Classified Instances: 45.85858585858586
Correctly Classified Instances: 54.14141414141414
Weighted Precision: 0.5638435948406212
Weighted AreaUnderROC: 0.9063748597081931
Root mean squared error: 0.26967836908987136
Relative absolute error: 92.08888888888819
Root relative squared error: 93.80776702038942
Weighted TruePositiveRate: 0.5414141414141415
Weighted MatthewsCorrelation: 0.5032564217259687
Weighted FMeasure: 0.542223506975328
Iteration time: 950.0
Weighted AreaUnderPRC: 0.4976929229219469
Mean absolute error: 0.1522130394857666
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 20.0
Accumulative iteration time: 12708.0
Weighted Recall: 0.5414141414141415
Weighted FalsePositiveRate: 0.045858585858585856
Kappa statistic: 0.4955555555555556
Training time: 949.0
		
Iteration: 19
Labeled set size: 459
Unlabelled set size: 36
	
Mean region size: 82.33241505968742
Incorrectly Classified Instances: 44.64646464646464
Correctly Classified Instances: 55.35353535353536
Weighted Precision: 0.5787442814829595
Weighted AreaUnderROC: 0.906628507295174
Root mean squared error: 0.2695817716305087
Relative absolute error: 92.06060606060537
Root relative squared error: 93.77416554173456
Weighted TruePositiveRate: 0.5535353535353535
Weighted MatthewsCorrelation: 0.5183160033857296
Weighted FMeasure: 0.556475437121396
Iteration time: 953.0
Weighted AreaUnderPRC: 0.509103402912066
Mean absolute error: 0.15216629100926604
Coverage of cases: 100.0
Instances selection time: 5.0
Test time: 14.0
Accumulative iteration time: 13661.0
Weighted Recall: 0.5535353535353535
Weighted FalsePositiveRate: 0.04464646464646464
Kappa statistic: 0.5088888888888888
Training time: 948.0
		
Iteration: 20
Labeled set size: 479
Unlabelled set size: 16
	
Mean region size: 82.33241505968743
Incorrectly Classified Instances: 41.81818181818182
Correctly Classified Instances: 58.18181818181818
Weighted Precision: 0.6051000059004733
Weighted AreaUnderROC: 0.9121099887766554
Root mean squared error: 0.2692635884037616
Relative absolute error: 91.95959595959525
Root relative squared error: 93.66348533365846
Weighted TruePositiveRate: 0.5818181818181818
Weighted MatthewsCorrelation: 0.5475954362982564
Weighted FMeasure: 0.5819453389477074
Iteration time: 988.0
Weighted AreaUnderPRC: 0.5321217293449753
Mean absolute error: 0.15199933216462122
Coverage of cases: 99.39393939393939
Instances selection time: 4.0
Test time: 14.0
Accumulative iteration time: 14649.0
Weighted Recall: 0.5818181818181818
Weighted FalsePositiveRate: 0.041818181818181824
Kappa statistic: 0.5399999999999999
Training time: 984.0
		
Time end:Wed Nov 01 14.48.52 EET 2017