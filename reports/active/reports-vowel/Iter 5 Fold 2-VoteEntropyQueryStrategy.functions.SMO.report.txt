Wed Nov 01 15.04.01 EET 2017
Dataset: vowel
Test set size: 495
Initial Labelled set size: 99
Initial Unlabelled set size: 396
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Wed Nov 01 15.04.01 EET 2017
		
Iteration: 1
Labeled set size: 99
Unlabelled set size: 396
	
Mean region size: 82.25895316804372
Incorrectly Classified Instances: 74.74747474747475
Correctly Classified Instances: 25.252525252525253
Weighted Precision: 0.24885222862057882
Weighted AreaUnderROC: 0.6890258136924803
Root mean squared error: 0.28164370967583074
Relative absolute error: 96.12929292929223
Root relative squared error: 97.9699172358308
Weighted TruePositiveRate: 0.25252525252525254
Weighted MatthewsCorrelation: 0.17535621221222875
Weighted FMeasure: 0.24287967044534706
Iteration time: 368.0
Weighted AreaUnderPRC: 0.20556606493909127
Mean absolute error: 0.15889139327155843
Coverage of cases: 94.14141414141415
Instances selection time: 5.0
Test time: 10.0
Accumulative iteration time: 368.0
Weighted Recall: 0.25252525252525254
Weighted FalsePositiveRate: 0.07474747474747474
Kappa statistic: 0.1777777777777778
Training time: 363.0
		
Iteration: 2
Labeled set size: 119
Unlabelled set size: 376
	
Mean region size: 82.55280073461859
Incorrectly Classified Instances: 73.73737373737374
Correctly Classified Instances: 26.262626262626263
Weighted Precision: 0.241364696975196
Weighted AreaUnderROC: 0.6882895622895624
Root mean squared error: 0.28144100636284985
Relative absolute error: 96.04848484848415
Root relative squared error: 97.89940677841983
Weighted TruePositiveRate: 0.26262626262626265
Weighted MatthewsCorrelation: 0.176153738088609
Weighted FMeasure: 0.2368203279474901
Iteration time: 374.0
Weighted AreaUnderPRC: 0.19762492117731548
Mean absolute error: 0.15875782619584258
Coverage of cases: 96.56565656565657
Instances selection time: 5.0
Test time: 11.0
Accumulative iteration time: 742.0
Weighted Recall: 0.26262626262626265
Weighted FalsePositiveRate: 0.07373737373737374
Kappa statistic: 0.1888888888888889
Training time: 369.0
		
Iteration: 3
Labeled set size: 139
Unlabelled set size: 356
	
Mean region size: 82.6078971533514
Incorrectly Classified Instances: 71.51515151515152
Correctly Classified Instances: 28.484848484848484
Weighted Precision: 0.29540382355037526
Weighted AreaUnderROC: 0.7054298540965207
Root mean squared error: 0.28060168466356056
Relative absolute error: 95.7818181818175
Root relative squared error: 97.60744826989055
Weighted TruePositiveRate: 0.28484848484848485
Weighted MatthewsCorrelation: 0.21105803368623277
Weighted FMeasure: 0.26512111512860903
Iteration time: 369.0
Weighted AreaUnderPRC: 0.21958019587241562
Mean absolute error: 0.15831705484598035
Coverage of cases: 95.75757575757575
Instances selection time: 5.0
Test time: 11.0
Accumulative iteration time: 1111.0
Weighted Recall: 0.28484848484848485
Weighted FalsePositiveRate: 0.07151515151515152
Kappa statistic: 0.21333333333333335
Training time: 364.0
		
Iteration: 4
Labeled set size: 159
Unlabelled set size: 336
	
Mean region size: 82.2956841138656
Incorrectly Classified Instances: 70.9090909090909
Correctly Classified Instances: 29.09090909090909
Weighted Precision: 0.34126184149605404
Weighted AreaUnderROC: 0.7141907968574635
Root mean squared error: 0.28049261564331346
Relative absolute error: 95.73333333333271
Root relative squared error: 97.56950855201472
Weighted TruePositiveRate: 0.2909090909090909
Weighted MatthewsCorrelation: 0.22340981266496118
Weighted FMeasure: 0.2537966642248591
Iteration time: 376.0
Weighted AreaUnderPRC: 0.2327397086155491
Mean absolute error: 0.15823691460055095
Coverage of cases: 94.94949494949495
Instances selection time: 5.0
Test time: 10.0
Accumulative iteration time: 1487.0
Weighted Recall: 0.2909090909090909
Weighted FalsePositiveRate: 0.07090909090909091
Kappa statistic: 0.22
Training time: 371.0
		
Iteration: 5
Labeled set size: 179
Unlabelled set size: 316
	
Mean region size: 82.33241505968745
Incorrectly Classified Instances: 69.29292929292929
Correctly Classified Instances: 30.707070707070706
Weighted Precision: 0.42190550404629473
Weighted AreaUnderROC: 0.7201616161616162
Root mean squared error: 0.2802968775764582
Relative absolute error: 95.68080808080748
Root relative squared error: 97.50142095924802
Weighted TruePositiveRate: 0.30707070707070705
Weighted MatthewsCorrelation: 0.25338170673416105
Weighted FMeasure: 0.26768588056547377
Iteration time: 374.0
Weighted AreaUnderPRC: 0.2475012016926613
Mean absolute error: 0.1581500960013357
Coverage of cases: 95.15151515151516
Instances selection time: 4.0
Test time: 11.0
Accumulative iteration time: 1861.0
Weighted Recall: 0.30707070707070705
Weighted FalsePositiveRate: 0.06929292929292928
Kappa statistic: 0.23777777777777775
Training time: 370.0
		
Iteration: 6
Labeled set size: 199
Unlabelled set size: 296
	
Mean region size: 82.22222222222187
Incorrectly Classified Instances: 68.88888888888889
Correctly Classified Instances: 31.11111111111111
Weighted Precision: 0.33658555251623556
Weighted AreaUnderROC: 0.7509203142536476
Root mean squared error: 0.280078893201789
Relative absolute error: 95.58787878787827
Root relative squared error: 97.425594976237
Weighted TruePositiveRate: 0.3111111111111111
Weighted MatthewsCorrelation: 0.21970189254002667
Weighted FMeasure: 0.22834700394065877
Iteration time: 386.0
Weighted AreaUnderPRC: 0.26066827860620734
Mean absolute error: 0.15799649386426262
Coverage of cases: 95.15151515151516
Instances selection time: 4.0
Test time: 11.0
Accumulative iteration time: 2247.0
Weighted Recall: 0.3111111111111111
Weighted FalsePositiveRate: 0.06888888888888889
Kappa statistic: 0.24222222222222223
Training time: 382.0
		
Iteration: 7
Labeled set size: 219
Unlabelled set size: 276
	
Mean region size: 82.1303948576672
Incorrectly Classified Instances: 64.84848484848484
Correctly Classified Instances: 35.15151515151515
Weighted Precision: 0.2998873780056959
Weighted AreaUnderROC: 0.7793153759820426
Root mean squared error: 0.27862471951164713
Relative absolute error: 95.09494949494896
Root relative squared error: 96.91975986905956
Weighted TruePositiveRate: 0.3515151515151515
Weighted MatthewsCorrelation: 0.25582822406926264
Weighted FMeasure: 0.27789505686044175
Iteration time: 390.0
Weighted AreaUnderPRC: 0.2907278126061245
Mean absolute error: 0.157181734702396
Coverage of cases: 96.36363636363636
Instances selection time: 3.0
Test time: 11.0
Accumulative iteration time: 2637.0
Weighted Recall: 0.3515151515151515
Weighted FalsePositiveRate: 0.06484848484848484
Kappa statistic: 0.2866666666666667
Training time: 387.0
		
Iteration: 8
Labeled set size: 239
Unlabelled set size: 256
	
Mean region size: 82.07529843893444
Incorrectly Classified Instances: 60.80808080808081
Correctly Classified Instances: 39.19191919191919
Weighted Precision: 0.3182083172500175
Weighted AreaUnderROC: 0.7894904601571269
Root mean squared error: 0.2784597204084077
Relative absolute error: 95.03434343434282
Root relative squared error: 96.86236484146616
Weighted TruePositiveRate: 0.39191919191919194
Weighted MatthewsCorrelation: 0.2844199199604197
Weighted FMeasure: 0.29854221393129166
Iteration time: 395.0
Weighted AreaUnderPRC: 0.30514271861598435
Mean absolute error: 0.15708155939560897
Coverage of cases: 96.16161616161617
Instances selection time: 4.0
Test time: 10.0
Accumulative iteration time: 3032.0
Weighted Recall: 0.39191919191919194
Weighted FalsePositiveRate: 0.06080808080808082
Kappa statistic: 0.3311111111111112
Training time: 391.0
		
Iteration: 9
Labeled set size: 259
Unlabelled set size: 236
	
Mean region size: 82.09366391184535
Incorrectly Classified Instances: 60.4040404040404
Correctly Classified Instances: 39.5959595959596
Weighted Precision: 0.31160917797894844
Weighted AreaUnderROC: 0.8090953984287317
Root mean squared error: 0.27776049548192944
Relative absolute error: 94.79999999999936
Root relative squared error: 96.6191390713783
Weighted TruePositiveRate: 0.39595959595959596
Weighted MatthewsCorrelation: 0.28595616133879637
Weighted FMeasure: 0.29961204618063325
Iteration time: 400.0
Weighted AreaUnderPRC: 0.32385469403647843
Mean absolute error: 0.156694214876033
Coverage of cases: 96.76767676767676
Instances selection time: 3.0
Test time: 10.0
Accumulative iteration time: 3432.0
Weighted Recall: 0.39595959595959596
Weighted FalsePositiveRate: 0.060404040404040404
Kappa statistic: 0.33555555555555555
Training time: 397.0
		
Iteration: 10
Labeled set size: 279
Unlabelled set size: 216
	
Mean region size: 82.2405876951328
Incorrectly Classified Instances: 58.58585858585859
Correctly Classified Instances: 41.41414141414141
Weighted Precision: 0.33324786668439915
Weighted AreaUnderROC: 0.8208843995510662
Root mean squared error: 0.27726628833432376
Relative absolute error: 94.63030303030237
Root relative squared error: 96.44722884691771
Weighted TruePositiveRate: 0.41414141414141414
Weighted MatthewsCorrelation: 0.31188836824216115
Weighted FMeasure: 0.33080001446065416
Iteration time: 403.0
Weighted AreaUnderPRC: 0.3379382535520295
Mean absolute error: 0.15641372401702971
Coverage of cases: 96.96969696969697
Instances selection time: 2.0
Test time: 11.0
Accumulative iteration time: 3835.0
Weighted Recall: 0.41414141414141414
Weighted FalsePositiveRate: 0.05858585858585858
Kappa statistic: 0.3555555555555555
Training time: 401.0
		
Iteration: 11
Labeled set size: 299
Unlabelled set size: 196
	
Mean region size: 81.91000918273606
Incorrectly Classified Instances: 58.98989898989899
Correctly Classified Instances: 41.01010101010101
Weighted Precision: 0.36265926306488777
Weighted AreaUnderROC: 0.835746352413019
Root mean squared error: 0.2761593877955515
Relative absolute error: 94.24646464646406
Root relative squared error: 96.06219289388105
Weighted TruePositiveRate: 0.4101010101010101
Weighted MatthewsCorrelation: 0.31957570773425337
Weighted FMeasure: 0.34529715749465634
Iteration time: 412.0
Weighted AreaUnderPRC: 0.3572402471490963
Mean absolute error: 0.15577928040737962
Coverage of cases: 97.57575757575758
Instances selection time: 2.0
Test time: 10.0
Accumulative iteration time: 4247.0
Weighted Recall: 0.4101010101010101
Weighted FalsePositiveRate: 0.05898989898989899
Kappa statistic: 0.35111111111111104
Training time: 410.0
		
Iteration: 12
Labeled set size: 319
Unlabelled set size: 176
	
Mean region size: 81.91000918273606
Incorrectly Classified Instances: 59.39393939393939
Correctly Classified Instances: 40.60606060606061
Weighted Precision: 0.410962791182918
Weighted AreaUnderROC: 0.8359124579124579
Root mean squared error: 0.276168621138271
Relative absolute error: 94.24646464646406
Root relative squared error: 96.06540471715621
Weighted TruePositiveRate: 0.40606060606060607
Weighted MatthewsCorrelation: 0.3197061046368324
Weighted FMeasure: 0.33517127841046035
Iteration time: 416.0
Weighted AreaUnderPRC: 0.36552947918156337
Mean absolute error: 0.15577928040737962
Coverage of cases: 97.37373737373737
Instances selection time: 2.0
Test time: 10.0
Accumulative iteration time: 4663.0
Weighted Recall: 0.40606060606060607
Weighted FalsePositiveRate: 0.05939393939393939
Kappa statistic: 0.34666666666666673
Training time: 414.0
		
Iteration: 13
Labeled set size: 339
Unlabelled set size: 156
	
Mean region size: 82.2405876951328
Incorrectly Classified Instances: 56.76767676767677
Correctly Classified Instances: 43.23232323232323
Weighted Precision: 0.38545141830217083
Weighted AreaUnderROC: 0.8358114478114479
Root mean squared error: 0.27589632768292943
Relative absolute error: 94.165656565656
Root relative squared error: 95.97068729096416
Weighted TruePositiveRate: 0.43232323232323233
Weighted MatthewsCorrelation: 0.3480210185606075
Weighted FMeasure: 0.3741647135418386
Iteration time: 433.0
Weighted AreaUnderPRC: 0.37440678093350643
Mean absolute error: 0.1556457133316638
Coverage of cases: 97.17171717171718
Instances selection time: 2.0
Test time: 10.0
Accumulative iteration time: 5096.0
Weighted Recall: 0.43232323232323233
Weighted FalsePositiveRate: 0.05676767676767677
Kappa statistic: 0.37555555555555553
Training time: 431.0
		
Iteration: 14
Labeled set size: 359
Unlabelled set size: 136
	
Mean region size: 82.09366391184534
Incorrectly Classified Instances: 54.54545454545455
Correctly Classified Instances: 45.45454545454545
Weighted Precision: 0.38462651742548437
Weighted AreaUnderROC: 0.8381952861952862
Root mean squared error: 0.2753480665702337
Relative absolute error: 93.98383838383792
Root relative squared error: 95.77997436541632
Weighted TruePositiveRate: 0.45454545454545453
Weighted MatthewsCorrelation: 0.36286784544398026
Weighted FMeasure: 0.3895564210877788
Iteration time: 426.0
Weighted AreaUnderPRC: 0.39055181669627254
Mean absolute error: 0.15534518741130335
Coverage of cases: 97.37373737373737
Instances selection time: 1.0
Test time: 11.0
Accumulative iteration time: 5522.0
Weighted Recall: 0.45454545454545453
Weighted FalsePositiveRate: 0.05454545454545454
Kappa statistic: 0.4
Training time: 425.0
		
Iteration: 15
Labeled set size: 379
Unlabelled set size: 116
	
Mean region size: 82.11202938475628
Incorrectly Classified Instances: 54.54545454545455
Correctly Classified Instances: 45.45454545454545
Weighted Precision: 0.41725523137662224
Weighted AreaUnderROC: 0.8523995510662178
Root mean squared error: 0.27414795400252856
Relative absolute error: 93.57171717171671
Root relative squared error: 95.36251455753678
Weighted TruePositiveRate: 0.45454545454545453
Weighted MatthewsCorrelation: 0.37275014566336695
Weighted FMeasure: 0.40263717972488505
Iteration time: 439.0
Weighted AreaUnderPRC: 0.39907775964723696
Mean absolute error: 0.15466399532515257
Coverage of cases: 98.18181818181819
Instances selection time: 1.0
Test time: 10.0
Accumulative iteration time: 5961.0
Weighted Recall: 0.45454545454545453
Weighted FalsePositiveRate: 0.054545454545454536
Kappa statistic: 0.4
Training time: 438.0
		
Iteration: 16
Labeled set size: 399
Unlabelled set size: 96
	
Mean region size: 82.14876033057814
Incorrectly Classified Instances: 52.525252525252526
Correctly Classified Instances: 47.474747474747474
Weighted Precision: 0.4205632227207617
Weighted AreaUnderROC: 0.8666531986531987
Root mean squared error: 0.27338218941737796
Relative absolute error: 93.32121212121159
Root relative squared error: 95.09614293107411
Weighted TruePositiveRate: 0.47474747474747475
Weighted MatthewsCorrelation: 0.39492085720926556
Weighted FMeasure: 0.43346835724256255
Iteration time: 442.0
Weighted AreaUnderPRC: 0.41552080296933086
Mean absolute error: 0.15424993739043336
Coverage of cases: 98.58585858585859
Instances selection time: 1.0
Test time: 10.0
Accumulative iteration time: 6403.0
Weighted Recall: 0.47474747474747475
Weighted FalsePositiveRate: 0.052525252525252523
Kappa statistic: 0.4222222222222223
Training time: 441.0
		
Iteration: 17
Labeled set size: 419
Unlabelled set size: 76
	
Mean region size: 82.31404958677652
Incorrectly Classified Instances: 48.888888888888886
Correctly Classified Instances: 51.111111111111114
Weighted Precision: 0.4520037276053319
Weighted AreaUnderROC: 0.8728372615039283
Root mean squared error: 0.2727774670675312
Relative absolute error: 93.12727272727217
Root relative squared error: 94.88578993354636
Weighted TruePositiveRate: 0.5111111111111111
Weighted MatthewsCorrelation: 0.43387680218954966
Weighted FMeasure: 0.4715762709627162
Iteration time: 457.0
Weighted AreaUnderPRC: 0.44313616485352403
Mean absolute error: 0.15392937640871532
Coverage of cases: 98.58585858585859
Instances selection time: 2.0
Test time: 10.0
Accumulative iteration time: 6860.0
Weighted Recall: 0.5111111111111111
Weighted FalsePositiveRate: 0.048888888888888885
Kappa statistic: 0.46222222222222215
Training time: 455.0
		
Iteration: 18
Labeled set size: 439
Unlabelled set size: 56
	
Mean region size: 82.11202938475627
Incorrectly Classified Instances: 45.85858585858586
Correctly Classified Instances: 54.14141414141414
Weighted Precision: 0.5843163879614218
Weighted AreaUnderROC: 0.8994163860830527
Root mean squared error: 0.27072319296914343
Relative absolute error: 92.44444444444402
Root relative squared error: 94.17120957373501
Weighted TruePositiveRate: 0.5414141414141415
Weighted MatthewsCorrelation: 0.4873047467358956
Weighted FMeasure: 0.5109330413477754
Iteration time: 459.0
Weighted AreaUnderPRC: 0.4775708257756887
Mean absolute error: 0.15280073461891672
Coverage of cases: 99.39393939393939
Instances selection time: 1.0
Test time: 10.0
Accumulative iteration time: 7319.0
Weighted Recall: 0.5414141414141415
Weighted FalsePositiveRate: 0.04585858585858586
Kappa statistic: 0.4955555555555556
Training time: 458.0
		
Iteration: 19
Labeled set size: 459
Unlabelled set size: 36
	
Mean region size: 82.14876033057813
Incorrectly Classified Instances: 44.44444444444444
Correctly Classified Instances: 55.55555555555556
Weighted Precision: 0.6000382737453861
Weighted AreaUnderROC: 0.9013243546576879
Root mean squared error: 0.27033808938677395
Relative absolute error: 92.315151515151
Root relative squared error: 94.03725108364344
Weighted TruePositiveRate: 0.5555555555555556
Weighted MatthewsCorrelation: 0.5068943662608569
Weighted FMeasure: 0.5340082960122867
Iteration time: 486.0
Weighted AreaUnderPRC: 0.48869408809756104
Mean absolute error: 0.15258702729777124
Coverage of cases: 99.39393939393939
Instances selection time: 0.0
Test time: 10.0
Accumulative iteration time: 7805.0
Weighted Recall: 0.5555555555555556
Weighted FalsePositiveRate: 0.04444444444444445
Kappa statistic: 0.5111111111111111
Training time: 486.0
		
Iteration: 20
Labeled set size: 479
Unlabelled set size: 16
	
Mean region size: 82.16712580348904
Incorrectly Classified Instances: 43.63636363636363
Correctly Classified Instances: 56.36363636363637
Weighted Precision: 0.5623936862772094
Weighted AreaUnderROC: 0.9152502805836139
Root mean squared error: 0.26864530150653654
Relative absolute error: 91.75353535353486
Root relative squared error: 93.44841390096481
Weighted TruePositiveRate: 0.5636363636363636
Weighted MatthewsCorrelation: 0.5178778847124046
Weighted FMeasure: 0.5586252305811786
Iteration time: 487.0
Weighted AreaUnderPRC: 0.508678985759847
Mean absolute error: 0.15165873612154618
Coverage of cases: 99.79797979797979
Instances selection time: 0.0
Test time: 10.0
Accumulative iteration time: 8292.0
Weighted Recall: 0.5636363636363636
Weighted FalsePositiveRate: 0.04363636363636364
Kappa statistic: 0.5199999999999999
Training time: 487.0
		
Time end:Wed Nov 01 15.04.10 EET 2017