Wed Nov 01 14.50.20 EET 2017
Dataset: vowel
Test set size: 495
Initial Labelled set size: 99
Initial Unlabelled set size: 396
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Nov 01 14.50.20 EET 2017
		
Iteration: 1
Labeled set size: 99
Unlabelled set size: 396
	
Mean region size: 54.545454545454064
Incorrectly Classified Instances: 61.41414141414141
Correctly Classified Instances: 38.58585858585859
Weighted Precision: 0.4424247246763103
Weighted AreaUnderROC: 0.6622222222222223
Root mean squared error: 0.318311661818392
Relative absolute error: 70.80000000000038
Root relative squared error: 110.72488428533067
Weighted TruePositiveRate: 0.38585858585858585
Weighted MatthewsCorrelation: 0.34188144926192887
Weighted FMeasure: 0.38670461512195903
Iteration time: 14.0
Weighted AreaUnderPRC: 0.22723401717693475
Mean absolute error: 0.11702479338843114
Coverage of cases: 72.32323232323232
Instances selection time: 13.0
Test time: 10.0
Accumulative iteration time: 14.0
Weighted Recall: 0.38585858585858585
Weighted FalsePositiveRate: 0.061414141414141414
Kappa statistic: 0.3244444444444445
Training time: 1.0
		
Iteration: 2
Labeled set size: 119
Unlabelled set size: 376
	
Mean region size: 45.45454545454594
Incorrectly Classified Instances: 60.2020202020202
Correctly Classified Instances: 39.7979797979798
Weighted Precision: 0.45734060564887624
Weighted AreaUnderROC: 0.6688888888888889
Root mean squared error: 0.3174713928271482
Relative absolute error: 69.0803418803415
Root relative squared error: 110.43259626078101
Weighted TruePositiveRate: 0.397979797979798
Weighted MatthewsCorrelation: 0.3566357530143453
Weighted FMeasure: 0.4001906260505498
Iteration time: 7.0
Weighted AreaUnderPRC: 0.23594464410329832
Mean absolute error: 0.11418238327329248
Coverage of cases: 66.66666666666667
Instances selection time: 7.0
Test time: 11.0
Accumulative iteration time: 21.0
Weighted Recall: 0.397979797979798
Weighted FalsePositiveRate: 0.060202020202020194
Kappa statistic: 0.3377777777777778
Training time: 0.0
		
Iteration: 3
Labeled set size: 139
Unlabelled set size: 356
	
Mean region size: 36.36363636363658
Incorrectly Classified Instances: 57.57575757575758
Correctly Classified Instances: 42.42424242424242
Weighted Precision: 0.4783175031887112
Weighted AreaUnderROC: 0.6833333333333333
Root mean squared error: 0.3121712228248514
Relative absolute error: 66.02222222222176
Root relative squared error: 108.58892924951166
Weighted TruePositiveRate: 0.42424242424242425
Weighted MatthewsCorrelation: 0.381652838621351
Weighted FMeasure: 0.4205285767212197
Iteration time: 7.0
Weighted AreaUnderPRC: 0.25325535775560204
Mean absolute error: 0.10912764003673088
Coverage of cases: 62.82828282828283
Instances selection time: 7.0
Test time: 12.0
Accumulative iteration time: 28.0
Weighted Recall: 0.42424242424242425
Weighted FalsePositiveRate: 0.057575757575757586
Kappa statistic: 0.3666666666666667
Training time: 0.0
		
Iteration: 4
Labeled set size: 159
Unlabelled set size: 336
	
Mean region size: 27.272727272727032
Incorrectly Classified Instances: 52.121212121212125
Correctly Classified Instances: 47.878787878787875
Weighted Precision: 0.5247080797037915
Weighted AreaUnderROC: 0.7133333333333334
Root mean squared error: 0.2982950781914674
Relative absolute error: 60.094117647058205
Root relative squared error: 103.76210480933595
Weighted TruePositiveRate: 0.47878787878787876
Weighted MatthewsCorrelation: 0.43612326209324803
Weighted FMeasure: 0.4673592878924139
Iteration time: 8.0
Weighted AreaUnderPRC: 0.29796432550148955
Mean absolute error: 0.09932912007778279
Coverage of cases: 58.58585858585859
Instances selection time: 8.0
Test time: 12.0
Accumulative iteration time: 36.0
Weighted Recall: 0.47878787878787876
Weighted FalsePositiveRate: 0.05212121212121212
Kappa statistic: 0.42666666666666664
Training time: 0.0
		
Iteration: 5
Labeled set size: 179
Unlabelled set size: 316
	
Mean region size: 18.18181818181829
Incorrectly Classified Instances: 50.101010101010104
Correctly Classified Instances: 49.898989898989896
Weighted Precision: 0.545787771507576
Weighted AreaUnderROC: 0.7244444444444443
Root mean squared error: 0.29342113661340335
Relative absolute error: 57.70994152046763
Root relative squared error: 102.06670158671541
Weighted TruePositiveRate: 0.498989898989899
Weighted MatthewsCorrelation: 0.4559375862574042
Weighted FMeasure: 0.48056121441790633
Iteration time: 8.0
Weighted AreaUnderPRC: 0.31389783993157977
Mean absolute error: 0.09538833309168265
Coverage of cases: 56.16161616161616
Instances selection time: 8.0
Test time: 13.0
Accumulative iteration time: 44.0
Weighted Recall: 0.498989898989899
Weighted FalsePositiveRate: 0.05010101010101009
Kappa statistic: 0.44888888888888884
Training time: 0.0
		
Iteration: 6
Labeled set size: 199
Unlabelled set size: 296
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 49.292929292929294
Correctly Classified Instances: 50.707070707070706
Weighted Precision: 0.5719046315811478
Weighted AreaUnderROC: 0.7288888888888888
Root mean squared error: 0.2918144966933202
Relative absolute error: 56.62010582010577
Root relative squared error: 101.50783101872192
Weighted TruePositiveRate: 0.5070707070707071
Weighted MatthewsCorrelation: 0.4715765792685413
Weighted FMeasure: 0.49163588995453344
Iteration time: 8.0
Weighted AreaUnderPRC: 0.3324402112688851
Mean absolute error: 0.09358695176877048
Coverage of cases: 50.707070707070706
Instances selection time: 8.0
Test time: 19.0
Accumulative iteration time: 52.0
Weighted Recall: 0.5070707070707071
Weighted FalsePositiveRate: 0.04929292929292929
Kappa statistic: 0.4577777777777778
Training time: 0.0
		
Iteration: 7
Labeled set size: 219
Unlabelled set size: 276
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 46.86868686868687
Correctly Classified Instances: 53.13131313131313
Weighted Precision: 0.5929937276702439
Weighted AreaUnderROC: 0.7422222222222222
Root mean squared error: 0.2851829101382943
Relative absolute error: 53.872463768115594
Root relative squared error: 99.20103003713446
Weighted TruePositiveRate: 0.5313131313131313
Weighted MatthewsCorrelation: 0.4995372690615027
Weighted FMeasure: 0.5204650190793971
Iteration time: 10.0
Weighted AreaUnderPRC: 0.3558367417866277
Mean absolute error: 0.08904539465804287
Coverage of cases: 53.13131313131313
Instances selection time: 10.0
Test time: 15.0
Accumulative iteration time: 62.0
Weighted Recall: 0.5313131313131313
Weighted FalsePositiveRate: 0.046868686868686865
Kappa statistic: 0.4844444444444444
Training time: 0.0
		
Iteration: 8
Labeled set size: 239
Unlabelled set size: 256
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 43.83838383838384
Correctly Classified Instances: 56.16161616161616
Weighted Precision: 0.6403399573863046
Weighted AreaUnderROC: 0.7588888888888891
Root mean squared error: 0.2763314707490893
Relative absolute error: 50.500444444444284
Root relative squared error: 96.12205204264457
Weighted TruePositiveRate: 0.5616161616161616
Weighted MatthewsCorrelation: 0.5343325170371173
Weighted FMeasure: 0.5435906337558147
Iteration time: 8.0
Weighted AreaUnderPRC: 0.38544985204827936
Mean absolute error: 0.08347180899908199
Coverage of cases: 56.16161616161616
Instances selection time: 8.0
Test time: 16.0
Accumulative iteration time: 70.0
Weighted Recall: 0.5616161616161616
Weighted FalsePositiveRate: 0.043838383838383836
Kappa statistic: 0.5177777777777777
Training time: 0.0
		
Iteration: 9
Labeled set size: 259
Unlabelled set size: 236
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 38.58585858585859
Correctly Classified Instances: 61.41414141414141
Weighted Precision: 0.6788308059932041
Weighted AreaUnderROC: 0.7877777777777779
Root mean squared error: 0.25968263127111646
Relative absolute error: 44.78930041152229
Root relative squared error: 90.33074419626307
Weighted TruePositiveRate: 0.6141414141414141
Weighted MatthewsCorrelation: 0.584266496395922
Weighted FMeasure: 0.5870924753376889
Iteration time: 8.0
Weighted AreaUnderPRC: 0.43657623620037134
Mean absolute error: 0.07403190150664889
Coverage of cases: 61.41414141414141
Instances selection time: 8.0
Test time: 17.0
Accumulative iteration time: 78.0
Weighted Recall: 0.6141414141414141
Weighted FalsePositiveRate: 0.03858585858585859
Kappa statistic: 0.5755555555555555
Training time: 0.0
		
Iteration: 10
Labeled set size: 279
Unlabelled set size: 216
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 32.121212121212125
Correctly Classified Instances: 67.87878787878788
Weighted Precision: 0.7387192100591692
Weighted AreaUnderROC: 0.8233333333333334
Root mean squared error: 0.237288545683518
Relative absolute error: 37.78620689655141
Root relative squared error: 82.54094937317134
Weighted TruePositiveRate: 0.6787878787878788
Weighted MatthewsCorrelation: 0.6544359765590856
Weighted FMeasure: 0.6556549467107441
Iteration time: 7.0
Weighted AreaUnderPRC: 0.5186653596237107
Mean absolute error: 0.062456540324878775
Coverage of cases: 67.87878787878788
Instances selection time: 7.0
Test time: 29.0
Accumulative iteration time: 85.0
Weighted Recall: 0.6787878787878788
Weighted FalsePositiveRate: 0.03212121212121212
Kappa statistic: 0.6466666666666667
Training time: 0.0
		
Iteration: 11
Labeled set size: 299
Unlabelled set size: 196
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 30.505050505050505
Correctly Classified Instances: 69.4949494949495
Weighted Precision: 0.7495515068866505
Weighted AreaUnderROC: 0.8322222222222222
Root mean squared error: 0.23151613051602746
Relative absolute error: 35.913261648745255
Root relative squared error: 80.53301162494039
Weighted TruePositiveRate: 0.694949494949495
Weighted MatthewsCorrelation: 0.6689160014164868
Weighted FMeasure: 0.6685565588032244
Iteration time: 7.0
Weighted AreaUnderPRC: 0.5379844414314836
Mean absolute error: 0.05936076305577766
Coverage of cases: 69.4949494949495
Instances selection time: 7.0
Test time: 19.0
Accumulative iteration time: 92.0
Weighted Recall: 0.694949494949495
Weighted FalsePositiveRate: 0.030505050505050504
Kappa statistic: 0.6644444444444445
Training time: 0.0
		
Iteration: 12
Labeled set size: 319
Unlabelled set size: 176
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 29.494949494949495
Correctly Classified Instances: 70.5050505050505
Weighted Precision: 0.7596386373191325
Weighted AreaUnderROC: 0.8377777777777777
Root mean squared error: 0.2278846979676324
Relative absolute error: 34.69629629629602
Root relative squared error: 79.2698158424986
Weighted TruePositiveRate: 0.705050505050505
Weighted MatthewsCorrelation: 0.6801042658186938
Weighted FMeasure: 0.6788872799009323
Iteration time: 7.0
Weighted AreaUnderPRC: 0.5534108683603021
Mean absolute error: 0.05734925007652271
Coverage of cases: 70.5050505050505
Instances selection time: 7.0
Test time: 19.0
Accumulative iteration time: 99.0
Weighted Recall: 0.705050505050505
Weighted FalsePositiveRate: 0.029494949494949494
Kappa statistic: 0.6755555555555556
Training time: 0.0
		
Iteration: 13
Labeled set size: 339
Unlabelled set size: 156
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 28.484848484848484
Correctly Classified Instances: 71.51515151515152
Weighted Precision: 0.7758708185172961
Weighted AreaUnderROC: 0.8433333333333333
Root mean squared error: 0.22415298488793514
Relative absolute error: 33.491428571428266
Root relative squared error: 77.97173742282929
Weighted TruePositiveRate: 0.7151515151515152
Weighted MatthewsCorrelation: 0.6919394099091127
Weighted FMeasure: 0.6858811525321387
Iteration time: 7.0
Weighted AreaUnderPRC: 0.5626500395345237
Mean absolute error: 0.055357733175914844
Coverage of cases: 71.51515151515152
Instances selection time: 6.0
Test time: 20.0
Accumulative iteration time: 106.0
Weighted Recall: 0.7151515151515152
Weighted FalsePositiveRate: 0.028484848484848488
Kappa statistic: 0.6866666666666668
Training time: 1.0
		
Iteration: 14
Labeled set size: 359
Unlabelled set size: 136
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 26.262626262626263
Correctly Classified Instances: 73.73737373737374
Weighted Precision: 0.7924686054598852
Weighted AreaUnderROC: 0.8555555555555555
Root mean squared error: 0.21541512056446274
Relative absolute error: 31.003003003002817
Root relative squared error: 74.93226657658239
Weighted TruePositiveRate: 0.7373737373737373
Weighted MatthewsCorrelation: 0.715980255083534
Weighted FMeasure: 0.7113816628715204
Iteration time: 8.0
Weighted AreaUnderPRC: 0.592686423734907
Mean absolute error: 0.051244633062814904
Coverage of cases: 73.73737373737374
Instances selection time: 8.0
Test time: 21.0
Accumulative iteration time: 114.0
Weighted Recall: 0.7373737373737373
Weighted FalsePositiveRate: 0.026262626262626262
Kappa statistic: 0.711111111111111
Training time: 0.0
		
Iteration: 15
Labeled set size: 379
Unlabelled set size: 116
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 20.80808080808081
Correctly Classified Instances: 79.1919191919192
Weighted Precision: 0.8305121128802093
Weighted AreaUnderROC: 0.8855555555555555
Root mean squared error: 0.19191544843764352
Relative absolute error: 25.063817663817513
Root relative squared error: 66.75789287591101
Weighted TruePositiveRate: 0.7919191919191919
Weighted MatthewsCorrelation: 0.7768687650305591
Weighted FMeasure: 0.7786623216469604
Iteration time: 5.0
Weighted AreaUnderPRC: 0.6660858561269566
Mean absolute error: 0.04142779779143418
Coverage of cases: 79.1919191919192
Instances selection time: 5.0
Test time: 22.0
Accumulative iteration time: 119.0
Weighted Recall: 0.7919191919191919
Weighted FalsePositiveRate: 0.020808080808080807
Kappa statistic: 0.7711111111111111
Training time: 0.0
		
Iteration: 16
Labeled set size: 399
Unlabelled set size: 96
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 20.0
Correctly Classified Instances: 80.0
Weighted Precision: 0.8364404363929998
Weighted AreaUnderROC: 0.8899999999999999
Root mean squared error: 0.1882751045871463
Relative absolute error: 24.092682926829102
Root relative squared error: 65.49159729219755
Weighted TruePositiveRate: 0.8
Weighted MatthewsCorrelation: 0.784959360307118
Weighted FMeasure: 0.7863989644877395
Iteration time: 5.0
Weighted AreaUnderPRC: 0.6783825453806279
Mean absolute error: 0.03982261640798224
Coverage of cases: 80.0
Instances selection time: 5.0
Test time: 23.0
Accumulative iteration time: 124.0
Weighted Recall: 0.8
Weighted FalsePositiveRate: 0.019999999999999997
Kappa statistic: 0.78
Training time: 0.0
		
Iteration: 17
Labeled set size: 419
Unlabelled set size: 76
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 15.151515151515152
Correctly Classified Instances: 84.84848484848484
Weighted Precision: 0.8766196878725186
Weighted AreaUnderROC: 0.9166666666666665
Root mean squared error: 0.16400478962426943
Relative absolute error: 18.798449612402965
Root relative squared error: 57.04915506283843
Weighted TruePositiveRate: 0.8484848484848485
Weighted MatthewsCorrelation: 0.8383159651986188
Weighted FMeasure: 0.8405930402758229
Iteration time: 4.0
Weighted AreaUnderPRC: 0.7474020645985974
Mean absolute error: 0.031071817541162124
Coverage of cases: 84.84848484848484
Instances selection time: 4.0
Test time: 24.0
Accumulative iteration time: 128.0
Weighted Recall: 0.8484848484848485
Weighted FalsePositiveRate: 0.015151515151515152
Kappa statistic: 0.8333333333333334
Training time: 0.0
		
Iteration: 18
Labeled set size: 439
Unlabelled set size: 56
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 12.727272727272727
Correctly Classified Instances: 87.27272727272727
Weighted Precision: 0.8900134263122672
Weighted AreaUnderROC: 0.93
Root mean squared error: 0.15041350344354554
Relative absolute error: 16.10222222222203
Root relative squared error: 52.321418789990034
Weighted TruePositiveRate: 0.8727272727272727
Weighted MatthewsCorrelation: 0.8616897745146791
Weighted FMeasure: 0.8647966548237458
Iteration time: 7.0
Weighted AreaUnderPRC: 0.7820181062491071
Mean absolute error: 0.026615243342515922
Coverage of cases: 87.27272727272727
Instances selection time: 7.0
Test time: 31.0
Accumulative iteration time: 135.0
Weighted Recall: 0.8727272727272727
Weighted FalsePositiveRate: 0.012727272727272728
Kappa statistic: 0.86
Training time: 0.0
		
Iteration: 19
Labeled set size: 459
Unlabelled set size: 36
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 11.919191919191919
Correctly Classified Instances: 88.08080808080808
Weighted Precision: 0.8959983638051288
Weighted AreaUnderROC: 0.9344444444444445
Root mean squared error: 0.14563422582417476
Relative absolute error: 15.1446808510637
Root relative squared error: 50.65894447676736
Weighted TruePositiveRate: 0.8808080808080808
Weighted MatthewsCorrelation: 0.8701861153611105
Weighted FMeasure: 0.8735056570916128
Iteration time: 3.0
Weighted AreaUnderPRC: 0.7949013352940496
Mean absolute error: 0.025032530332336853
Coverage of cases: 88.08080808080808
Instances selection time: 3.0
Test time: 25.0
Accumulative iteration time: 138.0
Weighted Recall: 0.8808080808080808
Weighted FalsePositiveRate: 0.01191919191919192
Kappa statistic: 0.8688888888888889
Training time: 0.0
		
Iteration: 20
Labeled set size: 479
Unlabelled set size: 16
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 9.292929292929292
Correctly Classified Instances: 90.70707070707071
Weighted Precision: 0.9137302602354295
Weighted AreaUnderROC: 0.9488888888888889
Root mean squared error: 0.12868015058428178
Relative absolute error: 12.237641723355981
Root relative squared error: 44.76146020497536
Weighted TruePositiveRate: 0.907070707070707
Weighted MatthewsCorrelation: 0.8994304477682664
Weighted FMeasure: 0.9061795575027852
Iteration time: 1.0
Weighted AreaUnderPRC: 0.8359651648089986
Mean absolute error: 0.020227506980753818
Coverage of cases: 90.70707070707071
Instances selection time: 1.0
Test time: 27.0
Accumulative iteration time: 139.0
Weighted Recall: 0.907070707070707
Weighted FalsePositiveRate: 0.009292929292929292
Kappa statistic: 0.8977777777777777
Training time: 0.0
		
Time end:Wed Nov 01 14.50.21 EET 2017