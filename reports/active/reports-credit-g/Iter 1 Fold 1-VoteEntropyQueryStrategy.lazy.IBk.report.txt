Tue Oct 31 11.35.19 EET 2017
Dataset: german_credit
Test set size: 500
Initial Labelled set size: 100
Initial Unlabelled set size: 400
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Tue Oct 31 11.35.19 EET 2017
		
Iteration: 1
Labeled set size: 100
Unlabelled set size: 400
	
Mean region size: 50.0
Incorrectly Classified Instances: 33.2
Correctly Classified Instances: 66.8
Weighted Precision: 0.6233333333333333
Weighted AreaUnderROC: 0.5380952380952381
Root mean squared error: 0.5706017113158316
Relative absolute error: 67.05882352941175
Root relative squared error: 114.12034226316632
Weighted TruePositiveRate: 0.668
Weighted MatthewsCorrelation: 0.09523809523809523
Weighted FMeasure: 0.6325691699604744
Iteration time: 29.0
Weighted AreaUnderPRC: 0.5979047619047619
Mean absolute error: 0.33529411764705874
Coverage of cases: 66.8
Instances selection time: 28.0
Test time: 22.0
Accumulative iteration time: 29.0
Weighted Recall: 0.668
Weighted FalsePositiveRate: 0.5918095238095238
Kappa statistic: 0.08791208791208799
Training time: 1.0
		
Iteration: 2
Labeled set size: 120
Unlabelled set size: 380
	
Mean region size: 50.0
Incorrectly Classified Instances: 32.6
Correctly Classified Instances: 67.4
Weighted Precision: 0.6220502567991452
Weighted AreaUnderROC: 0.5328571428571428
Root mean squared error: 0.5663240450000124
Relative absolute error: 65.77049180327877
Root relative squared error: 113.26480900000249
Weighted TruePositiveRate: 0.674
Weighted MatthewsCorrelation: 0.08840118645772048
Weighted FMeasure: 0.6289327942275663
Iteration time: 17.0
Weighted AreaUnderPRC: 0.5954411016510979
Mean absolute error: 0.32885245901639387
Coverage of cases: 67.4
Instances selection time: 16.0
Test time: 21.0
Accumulative iteration time: 46.0
Weighted Recall: 0.674
Weighted FalsePositiveRate: 0.6082857142857143
Kappa statistic: 0.07805429864253413
Training time: 1.0
		
Iteration: 3
Labeled set size: 140
Unlabelled set size: 360
	
Mean region size: 50.0
Incorrectly Classified Instances: 31.6
Correctly Classified Instances: 68.4
Weighted Precision: 0.6269230769230769
Weighted AreaUnderROC: 0.5304761904761904
Root mean squared error: 0.558210434432484
Relative absolute error: 63.718309859155156
Root relative squared error: 111.6420868864968
Weighted TruePositiveRate: 0.684
Weighted MatthewsCorrelation: 0.09150180217433553
Weighted FMeasure: 0.6267500434253951
Iteration time: 18.0
Weighted AreaUnderPRC: 0.5945582417582418
Mean absolute error: 0.31859154929577577
Coverage of cases: 68.4
Instances selection time: 17.0
Test time: 23.0
Accumulative iteration time: 64.0
Weighted Recall: 0.684
Weighted FalsePositiveRate: 0.6230476190476191
Kappa statistic: 0.07494145199063251
Training time: 1.0
		
Iteration: 4
Labeled set size: 160
Unlabelled set size: 340
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.2
Correctly Classified Instances: 69.8
Weighted Precision: 0.6472384448523278
Weighted AreaUnderROC: 0.5347619047619048
Root mean squared error: 0.5461773602831232
Relative absolute error: 60.88888888888877
Root relative squared error: 109.23547205662464
Weighted TruePositiveRate: 0.698
Weighted MatthewsCorrelation: 0.11880383133530194
Weighted FMeasure: 0.6299845379993346
Iteration time: 19.0
Weighted AreaUnderPRC: 0.597564013571389
Mean absolute error: 0.30444444444444385
Coverage of cases: 69.8
Instances selection time: 19.0
Test time: 26.0
Accumulative iteration time: 83.0
Weighted Recall: 0.698
Weighted FalsePositiveRate: 0.6284761904761904
Kappa statistic: 0.08816425120772947
Training time: 0.0
		
Iteration: 5
Labeled set size: 180
Unlabelled set size: 320
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.6
Correctly Classified Instances: 69.4
Weighted Precision: 0.6367579242309264
Weighted AreaUnderROC: 0.5280952380952381
Root mean squared error: 0.5501522991208816
Relative absolute error: 61.626373626373535
Root relative squared error: 110.03045982417632
Weighted TruePositiveRate: 0.694
Weighted MatthewsCorrelation: 0.09836735792905449
Weighted FMeasure: 0.6228111372022811
Iteration time: 20.0
Weighted AreaUnderPRC: 0.593831988792248
Mean absolute error: 0.30813186813186766
Coverage of cases: 69.4
Instances selection time: 19.0
Test time: 31.0
Accumulative iteration time: 103.0
Weighted Recall: 0.694
Weighted FalsePositiveRate: 0.6378095238095238
Kappa statistic: 0.07160194174757267
Training time: 1.0
		
Iteration: 6
Labeled set size: 200
Unlabelled set size: 300
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.0
Correctly Classified Instances: 70.0
Weighted Precision: 0.6502145922746781
Weighted AreaUnderROC: 0.5323809523809524
Root mean squared error: 0.5450267978471631
Relative absolute error: 60.39603960396048
Root relative squared error: 109.00535956943263
Weighted TruePositiveRate: 0.7
Weighted MatthewsCorrelation: 0.11788725315039099
Weighted FMeasure: 0.6267583120204603
Iteration time: 20.0
Weighted AreaUnderPRC: 0.5965184549356223
Mean absolute error: 0.3019801980198024
Coverage of cases: 70.0
Instances selection time: 19.0
Test time: 32.0
Accumulative iteration time: 123.0
Weighted Recall: 0.7
Weighted FalsePositiveRate: 0.6352380952380953
Kappa statistic: 0.08312958435207823
Training time: 1.0
		
Iteration: 7
Labeled set size: 220
Unlabelled set size: 280
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.8
Correctly Classified Instances: 70.2
Weighted Precision: 0.6551878528323924
Weighted AreaUnderROC: 0.5338095238095238
Root mean squared error: 0.5434478869920709
Relative absolute error: 59.963963963963906
Root relative squared error: 108.68957739841419
Weighted TruePositiveRate: 0.702
Weighted MatthewsCorrelation: 0.124805375458773
Weighted FMeasure: 0.6280755262154625
Iteration time: 20.0
Weighted AreaUnderPRC: 0.5974710401661152
Mean absolute error: 0.29981981981981953
Coverage of cases: 70.2
Instances selection time: 19.0
Test time: 34.0
Accumulative iteration time: 143.0
Weighted Recall: 0.702
Weighted FalsePositiveRate: 0.6343809523809524
Kappa statistic: 0.08700980392156857
Training time: 1.0
		
Iteration: 8
Labeled set size: 240
Unlabelled set size: 260
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.0
Correctly Classified Instances: 70.0
Weighted Precision: 0.6495726495726497
Weighted AreaUnderROC: 0.5304761904761904
Root mean squared error: 0.5454701976216564
Relative absolute error: 60.33057851239689
Root relative squared error: 109.09403952433128
Weighted TruePositiveRate: 0.7
Weighted MatthewsCorrelation: 0.11412279800291011
Weighted FMeasure: 0.6243853945565437
Iteration time: 20.0
Weighted AreaUnderPRC: 0.5955350427350428
Mean absolute error: 0.30165289256198446
Coverage of cases: 70.0
Instances selection time: 19.0
Test time: 37.0
Accumulative iteration time: 163.0
Weighted Recall: 0.7
Weighted FalsePositiveRate: 0.6390476190476191
Kappa statistic: 0.07862407862407848
Training time: 1.0
		
Iteration: 9
Labeled set size: 260
Unlabelled set size: 240
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.6
Correctly Classified Instances: 70.4
Weighted Precision: 0.6604255319148936
Weighted AreaUnderROC: 0.5333333333333332
Root mean squared error: 0.5419917212752332
Relative absolute error: 59.511450381679275
Root relative squared error: 108.39834425504664
Weighted TruePositiveRate: 0.704
Weighted MatthewsCorrelation: 0.12864083083132663
Weighted FMeasure: 0.6269918699186992
Iteration time: 20.0
Weighted AreaUnderPRC: 0.5974751773049645
Mean absolute error: 0.2975572519083964
Coverage of cases: 70.4
Instances selection time: 19.0
Test time: 39.0
Accumulative iteration time: 183.0
Weighted Recall: 0.704
Weighted FalsePositiveRate: 0.6373333333333334
Kappa statistic: 0.0864197530864195
Training time: 1.0
		
Iteration: 10
Labeled set size: 280
Unlabelled set size: 220
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.6
Correctly Classified Instances: 70.4
Weighted Precision: 0.6604255319148936
Weighted AreaUnderROC: 0.5333333333333332
Root mean squared error: 0.5421376984125326
Relative absolute error: 59.489361702127454
Root relative squared error: 108.4275396825065
Weighted TruePositiveRate: 0.704
Weighted MatthewsCorrelation: 0.12864083083132663
Weighted FMeasure: 0.6269918699186992
Iteration time: 20.0
Weighted AreaUnderPRC: 0.5974751773049645
Mean absolute error: 0.29744680851063726
Coverage of cases: 70.4
Instances selection time: 19.0
Test time: 42.0
Accumulative iteration time: 203.0
Weighted Recall: 0.704
Weighted FalsePositiveRate: 0.6373333333333334
Kappa statistic: 0.0864197530864195
Training time: 1.0
		
Iteration: 11
Labeled set size: 300
Unlabelled set size: 200
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.8
Correctly Classified Instances: 70.2
Weighted Precision: 0.6545354711179442
Weighted AreaUnderROC: 0.53
Root mean squared error: 0.5440932406284881
Relative absolute error: 59.86754966887426
Root relative squared error: 108.81864812569762
Weighted TruePositiveRate: 0.702
Weighted MatthewsCorrelation: 0.11763090805277893
Weighted FMeasure: 0.6232391347246512
Iteration time: 19.0
Weighted AreaUnderPRC: 0.5955057764111575
Mean absolute error: 0.2993377483443713
Coverage of cases: 70.2
Instances selection time: 18.0
Test time: 44.0
Accumulative iteration time: 222.0
Weighted Recall: 0.702
Weighted FalsePositiveRate: 0.642
Kappa statistic: 0.07797029702970296
Training time: 1.0
		
Iteration: 12
Labeled set size: 320
Unlabelled set size: 180
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.8
Correctly Classified Instances: 70.2
Weighted Precision: 0.6545354711179442
Weighted AreaUnderROC: 0.53
Root mean squared error: 0.5442046609618624
Relative absolute error: 59.850931677018714
Root relative squared error: 108.84093219237248
Weighted TruePositiveRate: 0.702
Weighted MatthewsCorrelation: 0.11763090805277893
Weighted FMeasure: 0.6232391347246512
Iteration time: 18.0
Weighted AreaUnderPRC: 0.5955057764111575
Mean absolute error: 0.2992546583850936
Coverage of cases: 70.2
Instances selection time: 17.0
Test time: 47.0
Accumulative iteration time: 240.0
Weighted Recall: 0.702
Weighted FalsePositiveRate: 0.642
Kappa statistic: 0.07797029702970296
Training time: 1.0
		
Iteration: 13
Labeled set size: 340
Unlabelled set size: 160
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.6
Correctly Classified Instances: 70.4
Weighted Precision: 0.6606945796819214
Weighted AreaUnderROC: 0.5295238095238095
Root mean squared error: 0.542473552799727
Relative absolute error: 59.43859649122818
Root relative squared error: 108.49471055994539
Weighted TruePositiveRate: 0.704
Weighted MatthewsCorrelation: 0.12187263394040442
Weighted FMeasure: 0.6219991173874668
Iteration time: 17.0
Weighted AreaUnderPRC: 0.59551911716975
Mean absolute error: 0.2971929824561409
Coverage of cases: 70.4
Instances selection time: 16.0
Test time: 50.0
Accumulative iteration time: 257.0
Weighted Recall: 0.704
Weighted FalsePositiveRate: 0.6449523809523809
Kappa statistic: 0.07730673316708209
Training time: 1.0
		
Iteration: 14
Labeled set size: 360
Unlabelled set size: 140
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.4
Correctly Classified Instances: 70.6
Weighted Precision: 0.667578947368421
Weighted AreaUnderROC: 0.530952380952381
Root mean squared error: 0.5407248113712082
Relative absolute error: 59.02762430939229
Root relative squared error: 108.14496227424164
Weighted TruePositiveRate: 0.706
Weighted MatthewsCorrelation: 0.13016280532365732
Weighted FMeasure: 0.6232727272727273
Iteration time: 16.0
Weighted AreaUnderPRC: 0.5965578947368422
Mean absolute error: 0.29513812154696145
Coverage of cases: 70.6
Instances selection time: 15.0
Test time: 51.0
Accumulative iteration time: 273.0
Weighted Recall: 0.706
Weighted FalsePositiveRate: 0.644095238095238
Kappa statistic: 0.08124999999999974
Training time: 1.0
		
Iteration: 15
Labeled set size: 380
Unlabelled set size: 120
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.2
Correctly Classified Instances: 70.8
Weighted Precision: 0.6726997578692494
Weighted AreaUnderROC: 0.5361904761904762
Root mean squared error: 0.5389601627864566
Relative absolute error: 58.61780104712042
Root relative squared error: 107.79203255729132
Weighted TruePositiveRate: 0.708
Weighted MatthewsCorrelation: 0.14426265808002778
Weighted FMeasure: 0.6296016840263539
Iteration time: 14.0
Weighted AreaUnderPRC: 0.5995704600484262
Mean absolute error: 0.2930890052356021
Coverage of cases: 70.8
Instances selection time: 13.0
Test time: 54.0
Accumulative iteration time: 287.0
Weighted Recall: 0.708
Weighted FalsePositiveRate: 0.6356190476190478
Kappa statistic: 0.09429280397022327
Training time: 1.0
		
Iteration: 16
Labeled set size: 400
Unlabelled set size: 100
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.8
Correctly Classified Instances: 70.2
Weighted Precision: 0.6620661824051655
Weighted AreaUnderROC: 0.5585714285714286
Root mean squared error: 0.5445398065348093
Relative absolute error: 59.80099502487541
Root relative squared error: 108.90796130696185
Weighted TruePositiveRate: 0.702
Weighted MatthewsCorrelation: 0.1663987898486613
Weighted FMeasure: 0.6542659948342296
Iteration time: 16.0
Weighted AreaUnderPRC: 0.6104145970252508
Mean absolute error: 0.29900497512437707
Coverage of cases: 70.2
Instances selection time: 14.0
Test time: 56.0
Accumulative iteration time: 303.0
Weighted Recall: 0.702
Weighted FalsePositiveRate: 0.5848571428571429
Kappa statistic: 0.14170506912442374
Training time: 2.0
		
Iteration: 17
Labeled set size: 420
Unlabelled set size: 80
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.2
Correctly Classified Instances: 70.8
Weighted Precision: 0.6762909632571996
Weighted AreaUnderROC: 0.5819047619047619
Root mean squared error: 0.5390934325992797
Relative absolute error: 58.59715639810421
Root relative squared error: 107.81868651985593
Weighted TruePositiveRate: 0.708
Weighted MatthewsCorrelation: 0.20908798394123287
Weighted FMeasure: 0.6741533465962362
Iteration time: 12.0
Weighted AreaUnderPRC: 0.6235807348560078
Mean absolute error: 0.29298578199052105
Coverage of cases: 70.8
Instances selection time: 11.0
Test time: 59.0
Accumulative iteration time: 315.0
Weighted Recall: 0.708
Weighted FalsePositiveRate: 0.5441904761904761
Kappa statistic: 0.19068736141906864
Training time: 1.0
		
Iteration: 18
Labeled set size: 440
Unlabelled set size: 60
	
Mean region size: 50.0
Incorrectly Classified Instances: 28.8
Correctly Classified Instances: 71.2
Weighted Precision: 0.6834636480336095
Weighted AreaUnderROC: 0.5923809523809523
Root mean squared error: 0.5354455632737097
Relative absolute error: 57.79185520362008
Root relative squared error: 107.08911265474195
Weighted TruePositiveRate: 0.712
Weighted MatthewsCorrelation: 0.22866358888697674
Weighted FMeasure: 0.6825431034482761
Iteration time: 9.0
Weighted AreaUnderPRC: 0.6297802777453613
Mean absolute error: 0.2889592760181004
Coverage of cases: 71.2
Instances selection time: 8.0
Test time: 61.0
Accumulative iteration time: 324.0
Weighted Recall: 0.712
Weighted FalsePositiveRate: 0.5272380952380953
Kappa statistic: 0.212253829321663
Training time: 1.0
		
Iteration: 19
Labeled set size: 460
Unlabelled set size: 40
	
Mean region size: 50.0
Incorrectly Classified Instances: 28.6
Correctly Classified Instances: 71.4
Weighted Precision: 0.69127937731429
Weighted AreaUnderROC: 0.6109523809523809
Root mean squared error: 0.5336352591719604
Relative absolute error: 57.385281385281374
Root relative squared error: 106.72705183439209
Weighted TruePositiveRate: 0.714
Weighted MatthewsCorrelation: 0.2551857917514379
Weighted FMeasure: 0.6944218953042529
Iteration time: 7.0
Weighted AreaUnderPRC: 0.6402751555454798
Mean absolute error: 0.28692640692640686
Coverage of cases: 71.4
Instances selection time: 6.0
Test time: 62.0
Accumulative iteration time: 331.0
Weighted Recall: 0.714
Weighted FalsePositiveRate: 0.4920952380952381
Kappa statistic: 0.24578059071729946
Training time: 1.0
		
Iteration: 20
Labeled set size: 480
Unlabelled set size: 20
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.8
Correctly Classified Instances: 70.2
Weighted Precision: 0.6821996804001945
Weighted AreaUnderROC: 0.608095238095238
Root mean squared error: 0.5447639762418032
Relative absolute error: 59.76763485477179
Root relative squared error: 108.95279524836063
Weighted TruePositiveRate: 0.702
Weighted MatthewsCorrelation: 0.23838554623311678
Weighted FMeasure: 0.6875989610066415
Iteration time: 4.0
Weighted AreaUnderPRC: 0.6373338104171009
Mean absolute error: 0.29883817427385895
Coverage of cases: 70.2
Instances selection time: 3.0
Test time: 65.0
Accumulative iteration time: 335.0
Weighted Recall: 0.702
Weighted FalsePositiveRate: 0.48580952380952386
Kappa statistic: 0.23353909465020573
Training time: 1.0
		
Time end:Tue Oct 31 11.35.21 EET 2017