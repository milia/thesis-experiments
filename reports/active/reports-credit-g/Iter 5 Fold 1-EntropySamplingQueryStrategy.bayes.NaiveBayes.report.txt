Tue Oct 31 11.28.45 EET 2017
Dataset: german_credit
Test set size: 500
Initial Labelled set size: 100
Initial Unlabelled set size: 400
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Tue Oct 31 11.28.45 EET 2017
		
Iteration: 1
Labeled set size: 100
Unlabelled set size: 400
	
Mean region size: 75.8
Incorrectly Classified Instances: 29.8
Correctly Classified Instances: 70.2
Weighted Precision: 0.6804095027466648
Weighted AreaUnderROC: 0.7226285714285714
Root mean squared error: 0.47320816560142936
Relative absolute error: 62.85149003523755
Root relative squared error: 94.64163312028587
Weighted TruePositiveRate: 0.702
Weighted MatthewsCorrelation: 0.23304849542300854
Weighted FMeasure: 0.685693188304853
Iteration time: 16.0
Weighted AreaUnderPRC: 0.7545400057651107
Mean absolute error: 0.3142574501761878
Coverage of cases: 92.2
Instances selection time: 16.0
Test time: 16.0
Accumulative iteration time: 16.0
Weighted Recall: 0.702
Weighted FalsePositiveRate: 0.49342857142857144
Kappa statistic: 0.22717842323651452
Training time: 0.0
		
Iteration: 2
Labeled set size: 120
Unlabelled set size: 380
	
Mean region size: 77.3
Incorrectly Classified Instances: 28.4
Correctly Classified Instances: 71.6
Weighted Precision: 0.6933394253223678
Weighted AreaUnderROC: 0.7226666666666668
Root mean squared error: 0.4635499067166815
Relative absolute error: 61.77374475684684
Root relative squared error: 92.7099813433363
Weighted TruePositiveRate: 0.716
Weighted MatthewsCorrelation: 0.2594635113889238
Weighted FMeasure: 0.6960449553877831
Iteration time: 10.0
Weighted AreaUnderPRC: 0.752205022759732
Mean absolute error: 0.3088687237842342
Coverage of cases: 92.2
Instances selection time: 9.0
Test time: 11.0
Accumulative iteration time: 26.0
Weighted Recall: 0.716
Weighted FalsePositiveRate: 0.4912380952380952
Kappa statistic: 0.24947145877378418
Training time: 1.0
		
Iteration: 3
Labeled set size: 140
Unlabelled set size: 360
	
Mean region size: 81.9
Incorrectly Classified Instances: 28.4
Correctly Classified Instances: 71.6
Weighted Precision: 0.6946792787466746
Weighted AreaUnderROC: 0.7371809523809524
Root mean squared error: 0.44688231809870166
Relative absolute error: 61.533311093935026
Root relative squared error: 89.37646361974033
Weighted TruePositiveRate: 0.716
Weighted MatthewsCorrelation: 0.26426433631576013
Weighted FMeasure: 0.6980646804176215
Iteration time: 12.0
Weighted AreaUnderPRC: 0.7647127417326267
Mean absolute error: 0.30766655546967514
Coverage of cases: 96.2
Instances selection time: 8.0
Test time: 11.0
Accumulative iteration time: 38.0
Weighted Recall: 0.716
Weighted FalsePositiveRate: 0.4836190476190476
Kappa statistic: 0.2557651991614256
Training time: 4.0
		
Iteration: 4
Labeled set size: 160
Unlabelled set size: 340
	
Mean region size: 82.1
Incorrectly Classified Instances: 27.8
Correctly Classified Instances: 72.2
Weighted Precision: 0.6997141000649773
Weighted AreaUnderROC: 0.7486857142857143
Root mean squared error: 0.44103286754413157
Relative absolute error: 61.09403467162148
Root relative squared error: 88.20657350882631
Weighted TruePositiveRate: 0.722
Weighted MatthewsCorrelation: 0.2725631393529576
Weighted FMeasure: 0.7009217461819165
Iteration time: 9.0
Weighted AreaUnderPRC: 0.7703696278535718
Mean absolute error: 0.3054701733581074
Coverage of cases: 96.0
Instances selection time: 8.0
Test time: 16.0
Accumulative iteration time: 47.0
Weighted Recall: 0.722
Weighted FalsePositiveRate: 0.48866666666666664
Kappa statistic: 0.2606382978723404
Training time: 1.0
		
Iteration: 5
Labeled set size: 180
Unlabelled set size: 320
	
Mean region size: 83.3
Incorrectly Classified Instances: 27.8
Correctly Classified Instances: 72.2
Weighted Precision: 0.701424352961612
Weighted AreaUnderROC: 0.7469904761904762
Root mean squared error: 0.43797287408223
Relative absolute error: 60.950327936660756
Root relative squared error: 87.594574816446
Weighted TruePositiveRate: 0.722
Weighted MatthewsCorrelation: 0.27936779578892995
Weighted FMeasure: 0.7039579997765947
Iteration time: 7.0
Weighted AreaUnderPRC: 0.7682132596992889
Mean absolute error: 0.30475163968330377
Coverage of cases: 96.6
Instances selection time: 6.0
Test time: 15.0
Accumulative iteration time: 54.0
Weighted Recall: 0.722
Weighted FalsePositiveRate: 0.47723809523809524
Kappa statistic: 0.2699579831932773
Training time: 1.0
		
Iteration: 6
Labeled set size: 200
Unlabelled set size: 300
	
Mean region size: 85.5
Incorrectly Classified Instances: 27.6
Correctly Classified Instances: 72.4
Weighted Precision: 0.7024133663366336
Weighted AreaUnderROC: 0.7536761904761905
Root mean squared error: 0.4298818328335119
Relative absolute error: 61.275051322770935
Root relative squared error: 85.97636656670238
Weighted TruePositiveRate: 0.724
Weighted MatthewsCorrelation: 0.27923161896479953
Weighted FMeasure: 0.7035906061978392
Iteration time: 5.0
Weighted AreaUnderPRC: 0.7755371175832645
Mean absolute error: 0.3063752566138547
Coverage of cases: 97.4
Instances selection time: 4.0
Test time: 13.0
Accumulative iteration time: 59.0
Weighted Recall: 0.724
Weighted FalsePositiveRate: 0.484
Kappa statistic: 0.267515923566879
Training time: 1.0
		
Iteration: 7
Labeled set size: 220
Unlabelled set size: 280
	
Mean region size: 87.7
Incorrectly Classified Instances: 28.0
Correctly Classified Instances: 72.0
Weighted Precision: 0.6981419433445019
Weighted AreaUnderROC: 0.7563047619047619
Root mean squared error: 0.4279109226298541
Relative absolute error: 62.30689843476418
Root relative squared error: 85.58218452597082
Weighted TruePositiveRate: 0.72
Weighted MatthewsCorrelation: 0.2704577279732002
Weighted FMeasure: 0.7003260123541524
Iteration time: 4.0
Weighted AreaUnderPRC: 0.7804526619426767
Mean absolute error: 0.3115344921738209
Coverage of cases: 98.2
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 63.0
Weighted Recall: 0.72
Weighted FalsePositiveRate: 0.4857142857142857
Kappa statistic: 0.2600422832980971
Training time: 1.0
		
Iteration: 8
Labeled set size: 240
Unlabelled set size: 260
	
Mean region size: 89.6
Incorrectly Classified Instances: 27.4
Correctly Classified Instances: 72.6
Weighted Precision: 0.710592885375494
Weighted AreaUnderROC: 0.7548761904761905
Root mean squared error: 0.424625568587027
Relative absolute error: 63.84880291898762
Root relative squared error: 84.9251137174054
Weighted TruePositiveRate: 0.726
Weighted MatthewsCorrelation: 0.30593776078426455
Weighted FMeasure: 0.714429469901168
Iteration time: 4.0
Weighted AreaUnderPRC: 0.7820520663527418
Mean absolute error: 0.3192440145949381
Coverage of cases: 98.4
Instances selection time: 4.0
Test time: 7.0
Accumulative iteration time: 67.0
Weighted Recall: 0.726
Weighted FalsePositiveRate: 0.4450476190476191
Kappa statistic: 0.30102040816326525
Training time: 0.0
		
Iteration: 9
Labeled set size: 260
Unlabelled set size: 240
	
Mean region size: 90.5
Incorrectly Classified Instances: 27.6
Correctly Classified Instances: 72.4
Weighted Precision: 0.7080174529588219
Weighted AreaUnderROC: 0.7585142857142857
Root mean squared error: 0.42241010633221227
Relative absolute error: 64.36465926531017
Root relative squared error: 84.48202126644246
Weighted TruePositiveRate: 0.724
Weighted MatthewsCorrelation: 0.29959651857344743
Weighted FMeasure: 0.7119318181818183
Iteration time: 4.0
Weighted AreaUnderPRC: 0.7862499350401538
Mean absolute error: 0.3218232963265508
Coverage of cases: 99.0
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 71.0
Weighted Recall: 0.724
Weighted FalsePositiveRate: 0.44971428571428573
Kappa statistic: 0.2944785276073619
Training time: 1.0
		
Iteration: 10
Labeled set size: 280
Unlabelled set size: 220
	
Mean region size: 90.8
Incorrectly Classified Instances: 27.2
Correctly Classified Instances: 72.8
Weighted Precision: 0.7138699085988108
Weighted AreaUnderROC: 0.7579238095238096
Root mean squared error: 0.4235457524762878
Relative absolute error: 64.31193428062562
Root relative squared error: 84.70915049525756
Weighted TruePositiveRate: 0.728
Weighted MatthewsCorrelation: 0.31451335690873
Weighted FMeasure: 0.7177065492211077
Iteration time: 4.0
Weighted AreaUnderPRC: 0.7837713838541845
Mean absolute error: 0.32155967140312813
Coverage of cases: 98.6
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 75.0
Weighted Recall: 0.728
Weighted FalsePositiveRate: 0.4365714285714286
Kappa statistic: 0.3103448275862068
Training time: 1.0
		
Iteration: 11
Labeled set size: 300
Unlabelled set size: 200
	
Mean region size: 90.6
Incorrectly Classified Instances: 26.4
Correctly Classified Instances: 73.6
Weighted Precision: 0.7213526043086993
Weighted AreaUnderROC: 0.7546857142857143
Root mean squared error: 0.4256895319196627
Relative absolute error: 64.48056584844826
Root relative squared error: 85.13790638393253
Weighted TruePositiveRate: 0.736
Weighted MatthewsCorrelation: 0.33080448925818157
Weighted FMeasure: 0.7244565217391304
Iteration time: 9.0
Weighted AreaUnderPRC: 0.780803463884233
Mean absolute error: 0.32240282924224134
Coverage of cases: 98.6
Instances selection time: 8.0
Test time: 15.0
Accumulative iteration time: 84.0
Weighted Recall: 0.736
Weighted FalsePositiveRate: 0.4331428571428572
Kappa statistic: 0.3251533742331288
Training time: 1.0
		
Iteration: 12
Labeled set size: 320
Unlabelled set size: 180
	
Mean region size: 91.9
Incorrectly Classified Instances: 27.0
Correctly Classified Instances: 73.0
Weighted Precision: 0.7143559488692232
Weighted AreaUnderROC: 0.7617714285714287
Root mean squared error: 0.42442172682186036
Relative absolute error: 64.52813866699823
Root relative squared error: 84.88434536437207
Weighted TruePositiveRate: 0.73
Weighted MatthewsCorrelation: 0.3140958107046256
Weighted FMeasure: 0.7177850808178257
Iteration time: 4.0
Weighted AreaUnderPRC: 0.7874203395511882
Mean absolute error: 0.3226406933349911
Coverage of cases: 99.4
Instances selection time: 3.0
Test time: 8.0
Accumulative iteration time: 88.0
Weighted Recall: 0.73
Weighted FalsePositiveRate: 0.44333333333333336
Kappa statistic: 0.3084016393442622
Training time: 1.0
		
Iteration: 13
Labeled set size: 340
Unlabelled set size: 160
	
Mean region size: 89.9
Incorrectly Classified Instances: 28.4
Correctly Classified Instances: 71.6
Weighted Precision: 0.6975757575757576
Weighted AreaUnderROC: 0.7555428571428572
Root mean squared error: 0.4285307259621217
Relative absolute error: 64.44328561284324
Root relative squared error: 85.70614519242434
Weighted TruePositiveRate: 0.716
Weighted MatthewsCorrelation: 0.2739271345377727
Weighted FMeasure: 0.701829521829522
Iteration time: 7.0
Weighted AreaUnderPRC: 0.7792640908735612
Mean absolute error: 0.3222164280642162
Coverage of cases: 98.0
Instances selection time: 6.0
Test time: 7.0
Accumulative iteration time: 95.0
Weighted Recall: 0.716
Weighted FalsePositiveRate: 0.4683809523809524
Kappa statistic: 0.268041237113402
Training time: 1.0
		
Iteration: 14
Labeled set size: 360
Unlabelled set size: 140
	
Mean region size: 89.6
Incorrectly Classified Instances: 27.2
Correctly Classified Instances: 72.8
Weighted Precision: 0.7117912371134021
Weighted AreaUnderROC: 0.7565333333333333
Root mean squared error: 0.42697328447544475
Relative absolute error: 63.8067195771261
Root relative squared error: 85.39465689508894
Weighted TruePositiveRate: 0.728
Weighted MatthewsCorrelation: 0.30776045042428835
Weighted FMeasure: 0.7152775191874057
Iteration time: 9.0
Weighted AreaUnderPRC: 0.7815433264034147
Mean absolute error: 0.3190335978856305
Coverage of cases: 98.0
Instances selection time: 8.0
Test time: 9.0
Accumulative iteration time: 104.0
Weighted Recall: 0.728
Weighted FalsePositiveRate: 0.448
Kappa statistic: 0.30184804928131403
Training time: 1.0
		
Iteration: 15
Labeled set size: 380
Unlabelled set size: 120
	
Mean region size: 88.5
Incorrectly Classified Instances: 26.8
Correctly Classified Instances: 73.2
Weighted Precision: 0.7156643356643355
Weighted AreaUnderROC: 0.7595238095238094
Root mean squared error: 0.4303805656468795
Relative absolute error: 62.99947327939622
Root relative squared error: 86.0761131293759
Weighted TruePositiveRate: 0.732
Weighted MatthewsCorrelation: 0.316069770620507
Weighted FMeasure: 0.7186278586278586
Iteration time: 3.0
Weighted AreaUnderPRC: 0.781990853112626
Mean absolute error: 0.3149973663969811
Coverage of cases: 98.2
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 107.0
Weighted Recall: 0.732
Weighted FalsePositiveRate: 0.4462857142857143
Kappa statistic: 0.3092783505154639
Training time: 1.0
		
Iteration: 16
Labeled set size: 400
Unlabelled set size: 100
	
Mean region size: 87.5
Incorrectly Classified Instances: 28.0
Correctly Classified Instances: 72.0
Weighted Precision: 0.7020979020979021
Weighted AreaUnderROC: 0.7572
Root mean squared error: 0.43479095439550064
Relative absolute error: 63.12221771794121
Root relative squared error: 86.95819087910013
Weighted TruePositiveRate: 0.72
Weighted MatthewsCorrelation: 0.28446279355845633
Weighted FMeasure: 0.706029106029106
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7795694966450487
Mean absolute error: 0.31561108858970605
Coverage of cases: 97.4
Instances selection time: 1.0
Test time: 7.0
Accumulative iteration time: 109.0
Weighted Recall: 0.72
Weighted FalsePositiveRate: 0.46285714285714286
Kappa statistic: 0.2783505154639175
Training time: 1.0
		
Iteration: 17
Labeled set size: 420
Unlabelled set size: 80
	
Mean region size: 86.5
Incorrectly Classified Instances: 28.8
Correctly Classified Instances: 71.2
Weighted Precision: 0.6930536130536129
Weighted AreaUnderROC: 0.7575047619047618
Root mean squared error: 0.436060625060477
Relative absolute error: 62.976356537926115
Root relative squared error: 87.2121250120954
Weighted TruePositiveRate: 0.712
Weighted MatthewsCorrelation: 0.2633914755170892
Weighted FMeasure: 0.6976299376299375
Iteration time: 3.0
Weighted AreaUnderPRC: 0.7792180623390618
Mean absolute error: 0.3148817826896306
Coverage of cases: 96.8
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 112.0
Weighted Recall: 0.712
Weighted FalsePositiveRate: 0.4739047619047619
Kappa statistic: 0.2577319587628865
Training time: 1.0
		
Iteration: 18
Labeled set size: 440
Unlabelled set size: 60
	
Mean region size: 85.7
Incorrectly Classified Instances: 29.0
Correctly Classified Instances: 71.0
Weighted Precision: 0.6903845702620897
Weighted AreaUnderROC: 0.7542285714285715
Root mean squared error: 0.43953873461573545
Relative absolute error: 62.751564757917535
Root relative squared error: 87.90774692314709
Weighted TruePositiveRate: 0.71
Weighted MatthewsCorrelation: 0.25685912487145923
Weighted FMeasure: 0.6950692740166424
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7769762112043047
Mean absolute error: 0.31375782378958766
Coverage of cases: 96.2
Instances selection time: 1.0
Test time: 7.0
Accumulative iteration time: 114.0
Weighted Recall: 0.71
Weighted FalsePositiveRate: 0.47857142857142854
Kappa statistic: 0.2510330578512396
Training time: 1.0
		
Iteration: 19
Labeled set size: 460
Unlabelled set size: 40
	
Mean region size: 84.4
Incorrectly Classified Instances: 28.4
Correctly Classified Instances: 71.6
Weighted Precision: 0.6999281609195402
Weighted AreaUnderROC: 0.757752380952381
Root mean squared error: 0.44005454202581956
Relative absolute error: 62.2537303897264
Root relative squared error: 88.01090840516392
Weighted TruePositiveRate: 0.716
Weighted MatthewsCorrelation: 0.28123194462509693
Weighted FMeasure: 0.7044272807358998
Iteration time: 9.0
Weighted AreaUnderPRC: 0.7797125264797136
Mean absolute error: 0.311268651948632
Coverage of cases: 96.4
Instances selection time: 4.0
Test time: 12.0
Accumulative iteration time: 123.0
Weighted Recall: 0.716
Weighted FalsePositiveRate: 0.45695238095238094
Kappa statistic: 0.2769857433808554
Training time: 5.0
		
Iteration: 20
Labeled set size: 480
Unlabelled set size: 20
	
Mean region size: 82.0
Incorrectly Classified Instances: 27.6
Correctly Classified Instances: 72.4
Weighted Precision: 0.711050394656952
Weighted AreaUnderROC: 0.7602095238095237
Root mean squared error: 0.44267203613113215
Relative absolute error: 61.409005081040505
Root relative squared error: 88.53440722622643
Weighted TruePositiveRate: 0.724
Weighted MatthewsCorrelation: 0.30891437698095536
Weighted FMeasure: 0.7151018099547511
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7802859599451243
Mean absolute error: 0.3070450254052025
Coverage of cases: 95.4
Instances selection time: 1.0
Test time: 7.0
Accumulative iteration time: 125.0
Weighted Recall: 0.724
Weighted FalsePositiveRate: 0.43447619047619046
Kappa statistic: 0.305835010060362
Training time: 1.0
		
Time end:Tue Oct 31 11.28.46 EET 2017