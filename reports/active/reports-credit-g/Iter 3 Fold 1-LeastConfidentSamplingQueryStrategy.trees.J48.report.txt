Tue Oct 31 11.32.09 EET 2017
Dataset: german_credit
Test set size: 500
Initial Labelled set size: 100
Initial Unlabelled set size: 400
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Tue Oct 31 11.32.09 EET 2017
		
Iteration: 1
Labeled set size: 100
Unlabelled set size: 400
	
Mean region size: 80.6
Incorrectly Classified Instances: 32.0
Correctly Classified Instances: 68.0
Weighted Precision: 0.6431861588585552
Weighted AreaUnderROC: 0.6156666666666667
Root mean squared error: 0.4973584991536635
Relative absolute error: 70.47185185185178
Root relative squared error: 99.47169983073269
Weighted TruePositiveRate: 0.68
Weighted MatthewsCorrelation: 0.14109147936310057
Weighted FMeasure: 0.6500133108527819
Iteration time: 3.0
Weighted AreaUnderPRC: 0.6499220983759176
Mean absolute error: 0.35235925925925893
Coverage of cases: 90.8
Instances selection time: 2.0
Test time: 1.0
Accumulative iteration time: 3.0
Weighted Recall: 0.68
Weighted FalsePositiveRate: 0.5638095238095239
Kappa statistic: 0.13232104121475072
Training time: 1.0
		
Iteration: 2
Labeled set size: 120
Unlabelled set size: 380
	
Mean region size: 80.6
Incorrectly Classified Instances: 29.4
Correctly Classified Instances: 70.6
Weighted Precision: 0.6664177351932742
Weighted AreaUnderROC: 0.627095238095238
Root mean squared error: 0.4793685442018283
Relative absolute error: 69.99470899470917
Root relative squared error: 95.87370884036565
Weighted TruePositiveRate: 0.706
Weighted MatthewsCorrelation: 0.16301171652631538
Weighted FMeasure: 0.6479976736982508
Iteration time: 3.0
Weighted AreaUnderPRC: 0.6521425412837701
Mean absolute error: 0.3499735449735458
Coverage of cases: 90.8
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 6.0
Weighted Recall: 0.706
Weighted FalsePositiveRate: 0.6021904761904763
Kappa statistic: 0.12914691943127954
Training time: 2.0
		
Iteration: 3
Labeled set size: 140
Unlabelled set size: 360
	
Mean region size: 80.6
Incorrectly Classified Instances: 29.4
Correctly Classified Instances: 70.6
Weighted Precision: 0.6664177351932742
Weighted AreaUnderROC: 0.6144285714285714
Root mean squared error: 0.4864140784277626
Relative absolute error: 68.20296296296306
Root relative squared error: 97.28281568555252
Weighted TruePositiveRate: 0.706
Weighted MatthewsCorrelation: 0.16301171652631538
Weighted FMeasure: 0.6479976736982508
Iteration time: 3.0
Weighted AreaUnderPRC: 0.6461844908190156
Mean absolute error: 0.34101481481481527
Coverage of cases: 90.8
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 9.0
Weighted Recall: 0.706
Weighted FalsePositiveRate: 0.6021904761904763
Kappa statistic: 0.12914691943127954
Training time: 2.0
		
Iteration: 4
Labeled set size: 160
Unlabelled set size: 340
	
Mean region size: 80.6
Incorrectly Classified Instances: 29.8
Correctly Classified Instances: 70.2
Weighted Precision: 0.6614351906855175
Weighted AreaUnderROC: 0.6306380952380952
Root mean squared error: 0.4904215238724183
Relative absolute error: 67.70757834757842
Root relative squared error: 98.08430477448366
Weighted TruePositiveRate: 0.702
Weighted MatthewsCorrelation: 0.1634172971103882
Weighted FMeasure: 0.6525321198165104
Iteration time: 2.0
Weighted AreaUnderPRC: 0.6541423714095754
Mean absolute error: 0.3385378917378921
Coverage of cases: 90.8
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 11.0
Weighted Recall: 0.702
Weighted FalsePositiveRate: 0.5886666666666667
Kappa statistic: 0.1377314814814814
Training time: 2.0
		
Iteration: 5
Labeled set size: 180
Unlabelled set size: 320
	
Mean region size: 80.6
Incorrectly Classified Instances: 29.6
Correctly Classified Instances: 70.4
Weighted Precision: 0.6645913770913772
Weighted AreaUnderROC: 0.6324285714285715
Root mean squared error: 0.4958472484273741
Relative absolute error: 66.48485996523401
Root relative squared error: 99.16944968547482
Weighted TruePositiveRate: 0.704
Weighted MatthewsCorrelation: 0.16883585627078515
Weighted FMeasure: 0.6539874299968208
Iteration time: 3.0
Weighted AreaUnderPRC: 0.654874516846294
Mean absolute error: 0.33242429982617006
Coverage of cases: 90.8
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 14.0
Weighted Recall: 0.704
Weighted FalsePositiveRate: 0.5878095238095238
Kappa statistic: 0.14153132250580033
Training time: 2.0
		
Iteration: 6
Labeled set size: 200
Unlabelled set size: 300
	
Mean region size: 80.6
Incorrectly Classified Instances: 29.6
Correctly Classified Instances: 70.4
Weighted Precision: 0.6645913770913772
Weighted AreaUnderROC: 0.6324285714285715
Root mean squared error: 0.5008061738043414
Relative absolute error: 65.65418809828263
Root relative squared error: 100.1612347608683
Weighted TruePositiveRate: 0.704
Weighted MatthewsCorrelation: 0.16883585627078515
Weighted FMeasure: 0.6539874299968208
Iteration time: 3.0
Weighted AreaUnderPRC: 0.654874516846294
Mean absolute error: 0.32827094049141314
Coverage of cases: 90.8
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 17.0
Weighted Recall: 0.704
Weighted FalsePositiveRate: 0.5878095238095238
Kappa statistic: 0.14153132250580033
Training time: 3.0
		
Iteration: 7
Labeled set size: 220
Unlabelled set size: 280
	
Mean region size: 80.6
Incorrectly Classified Instances: 29.6
Correctly Classified Instances: 70.4
Weighted Precision: 0.6645913770913772
Weighted AreaUnderROC: 0.6324285714285715
Root mean squared error: 0.5047860911904902
Relative absolute error: 65.0495493924065
Root relative squared error: 100.95721823809805
Weighted TruePositiveRate: 0.704
Weighted MatthewsCorrelation: 0.16883585627078515
Weighted FMeasure: 0.6539874299968208
Iteration time: 3.0
Weighted AreaUnderPRC: 0.654874516846294
Mean absolute error: 0.3252477469620325
Coverage of cases: 90.8
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 20.0
Weighted Recall: 0.704
Weighted FalsePositiveRate: 0.5878095238095238
Kappa statistic: 0.14153132250580033
Training time: 3.0
		
Iteration: 8
Labeled set size: 240
Unlabelled set size: 260
	
Mean region size: 80.6
Incorrectly Classified Instances: 29.6
Correctly Classified Instances: 70.4
Weighted Precision: 0.6645913770913772
Weighted AreaUnderROC: 0.6324285714285715
Root mean squared error: 0.5080165451466456
Relative absolute error: 64.5897343286566
Root relative squared error: 101.60330902932913
Weighted TruePositiveRate: 0.704
Weighted MatthewsCorrelation: 0.16883585627078515
Weighted FMeasure: 0.6539874299968208
Iteration time: 3.0
Weighted AreaUnderPRC: 0.654874516846294
Mean absolute error: 0.322948671643283
Coverage of cases: 90.8
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 23.0
Weighted Recall: 0.704
Weighted FalsePositiveRate: 0.5878095238095238
Kappa statistic: 0.14153132250580033
Training time: 3.0
		
Iteration: 9
Labeled set size: 260
Unlabelled set size: 240
	
Mean region size: 80.6
Incorrectly Classified Instances: 29.6
Correctly Classified Instances: 70.4
Weighted Precision: 0.6645913770913772
Weighted AreaUnderROC: 0.6324285714285715
Root mean squared error: 0.5106771981536792
Relative absolute error: 64.22827542827538
Root relative squared error: 102.13543963073583
Weighted TruePositiveRate: 0.704
Weighted MatthewsCorrelation: 0.16883585627078515
Weighted FMeasure: 0.6539874299968208
Iteration time: 4.0
Weighted AreaUnderPRC: 0.654874516846294
Mean absolute error: 0.3211413771413769
Coverage of cases: 90.8
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 27.0
Weighted Recall: 0.704
Weighted FalsePositiveRate: 0.5878095238095238
Kappa statistic: 0.14153132250580033
Training time: 3.0
		
Iteration: 10
Labeled set size: 280
Unlabelled set size: 220
	
Mean region size: 80.6
Incorrectly Classified Instances: 29.6
Correctly Classified Instances: 70.4
Weighted Precision: 0.6645913770913772
Weighted AreaUnderROC: 0.6324285714285715
Root mean squared error: 0.5129002362386346
Relative absolute error: 63.93666365840264
Root relative squared error: 102.58004724772691
Weighted TruePositiveRate: 0.704
Weighted MatthewsCorrelation: 0.16883585627078515
Weighted FMeasure: 0.6539874299968208
Iteration time: 4.0
Weighted AreaUnderPRC: 0.654874516846294
Mean absolute error: 0.3196833182920132
Coverage of cases: 90.8
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 31.0
Weighted Recall: 0.704
Weighted FalsePositiveRate: 0.5878095238095238
Kappa statistic: 0.14153132250580033
Training time: 3.0
		
Iteration: 11
Labeled set size: 300
Unlabelled set size: 200
	
Mean region size: 80.6
Incorrectly Classified Instances: 29.6
Correctly Classified Instances: 70.4
Weighted Precision: 0.6645913770913772
Weighted AreaUnderROC: 0.6324285714285715
Root mean squared error: 0.4959009563517297
Relative absolute error: 66.47529184797934
Root relative squared error: 99.18019127034594
Weighted TruePositiveRate: 0.704
Weighted MatthewsCorrelation: 0.16883585627078515
Weighted FMeasure: 0.6539874299968208
Iteration time: 5.0
Weighted AreaUnderPRC: 0.654874516846294
Mean absolute error: 0.33237645923989667
Coverage of cases: 90.8
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 36.0
Weighted Recall: 0.704
Weighted FalsePositiveRate: 0.5878095238095238
Kappa statistic: 0.14153132250580033
Training time: 4.0
		
Iteration: 12
Labeled set size: 320
Unlabelled set size: 180
	
Mean region size: 87.3
Incorrectly Classified Instances: 28.6
Correctly Classified Instances: 71.4
Weighted Precision: 0.6840556927913766
Weighted AreaUnderROC: 0.6405142857142857
Root mean squared error: 0.47767089815769126
Relative absolute error: 65.71309523809525
Root relative squared error: 95.53417963153825
Weighted TruePositiveRate: 0.714
Weighted MatthewsCorrelation: 0.2237142044394169
Weighted FMeasure: 0.6787944895568214
Iteration time: 7.0
Weighted AreaUnderPRC: 0.6768794565832515
Mean absolute error: 0.3285654761904762
Coverage of cases: 90.6
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 43.0
Weighted Recall: 0.714
Weighted FalsePositiveRate: 0.5416190476190476
Kappa statistic: 0.2020089285714286
Training time: 7.0
		
Iteration: 13
Labeled set size: 340
Unlabelled set size: 160
	
Mean region size: 87.0
Incorrectly Classified Instances: 29.4
Correctly Classified Instances: 70.6
Weighted Precision: 0.6725578261845947
Weighted AreaUnderROC: 0.6793238095238096
Root mean squared error: 0.48454226012385193
Relative absolute error: 65.53437662337649
Root relative squared error: 96.90845202477038
Weighted TruePositiveRate: 0.706
Weighted MatthewsCorrelation: 0.1989944028439012
Weighted FMeasure: 0.6698097200339354
Iteration time: 7.0
Weighted AreaUnderPRC: 0.6907243445736584
Mean absolute error: 0.32767188311688245
Coverage of cases: 90.2
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 50.0
Weighted Recall: 0.706
Weighted FalsePositiveRate: 0.5526666666666668
Kappa statistic: 0.1796875
Training time: 7.0
		
Iteration: 14
Labeled set size: 360
Unlabelled set size: 140
	
Mean region size: 92.7
Incorrectly Classified Instances: 27.8
Correctly Classified Instances: 72.2
Weighted Precision: 0.7011371486263882
Weighted AreaUnderROC: 0.7392857142857143
Root mean squared error: 0.43819593158761827
Relative absolute error: 64.61513222549809
Root relative squared error: 87.63918631752365
Weighted TruePositiveRate: 0.722
Weighted MatthewsCorrelation: 0.21792780703775766
Weighted FMeasure: 0.6614034520026663
Iteration time: 8.0
Weighted AreaUnderPRC: 0.7379295810686546
Mean absolute error: 0.3230756611274904
Coverage of cases: 95.2
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 58.0
Weighted Recall: 0.722
Weighted FalsePositiveRate: 0.5915238095238096
Kappa statistic: 0.16466346153846143
Training time: 7.0
		
Iteration: 15
Labeled set size: 380
Unlabelled set size: 120
	
Mean region size: 89.3
Incorrectly Classified Instances: 26.2
Correctly Classified Instances: 73.8
Weighted Precision: 0.7183529411764706
Weighted AreaUnderROC: 0.6792095238095238
Root mean squared error: 0.4648784971118228
Relative absolute error: 64.17549031701913
Root relative squared error: 92.97569942236456
Weighted TruePositiveRate: 0.738
Weighted MatthewsCorrelation: 0.299454842362549
Weighted FMeasure: 0.707010752688172
Iteration time: 8.0
Weighted AreaUnderPRC: 0.6936844103224555
Mean absolute error: 0.3208774515850956
Coverage of cases: 91.0
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 66.0
Weighted Recall: 0.738
Weighted FalsePositiveRate: 0.5046666666666666
Kappa statistic: 0.27222222222222214
Training time: 7.0
		
Iteration: 16
Labeled set size: 400
Unlabelled set size: 100
	
Mean region size: 88.2
Incorrectly Classified Instances: 27.0
Correctly Classified Instances: 73.0
Weighted Precision: 0.7090829832765317
Weighted AreaUnderROC: 0.6777333333333333
Root mean squared error: 0.47095177347952377
Relative absolute error: 64.02925468672895
Root relative squared error: 94.19035469590476
Weighted TruePositiveRate: 0.73
Weighted MatthewsCorrelation: 0.2927468165564306
Weighted FMeasure: 0.7084984588287099
Iteration time: 8.0
Weighted AreaUnderPRC: 0.6943531165971444
Mean absolute error: 0.32014627343364477
Coverage of cases: 91.0
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 74.0
Weighted Recall: 0.73
Weighted FalsePositiveRate: 0.48142857142857143
Kappa statistic: 0.2788461538461537
Training time: 8.0
		
Iteration: 17
Labeled set size: 420
Unlabelled set size: 80
	
Mean region size: 88.2
Incorrectly Classified Instances: 27.0
Correctly Classified Instances: 73.0
Weighted Precision: 0.7090829832765317
Weighted AreaUnderROC: 0.6661333333333334
Root mean squared error: 0.46994753499511055
Relative absolute error: 64.44721972169407
Root relative squared error: 93.98950699902211
Weighted TruePositiveRate: 0.73
Weighted MatthewsCorrelation: 0.2927468165564306
Weighted FMeasure: 0.7084984588287099
Iteration time: 8.0
Weighted AreaUnderPRC: 0.6883721462343645
Mean absolute error: 0.32223609860847036
Coverage of cases: 91.0
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 82.0
Weighted Recall: 0.73
Weighted FalsePositiveRate: 0.48142857142857143
Kappa statistic: 0.2788461538461537
Training time: 8.0
		
Iteration: 18
Labeled set size: 440
Unlabelled set size: 60
	
Mean region size: 91.9
Incorrectly Classified Instances: 26.8
Correctly Classified Instances: 73.2
Weighted Precision: 0.7107678729037953
Weighted AreaUnderROC: 0.6796857142857143
Root mean squared error: 0.46349945017190514
Relative absolute error: 64.47982365765559
Root relative squared error: 92.69989003438103
Weighted TruePositiveRate: 0.732
Weighted MatthewsCorrelation: 0.2933866673200632
Weighted FMeasure: 0.7079953241138977
Iteration time: 8.0
Weighted AreaUnderPRC: 0.6987270333222397
Mean absolute error: 0.32239911828827794
Coverage of cases: 93.4
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 90.0
Weighted Recall: 0.732
Weighted FalsePositiveRate: 0.4881904761904762
Kappa statistic: 0.2764578833693303
Training time: 8.0
		
Iteration: 19
Labeled set size: 460
Unlabelled set size: 40
	
Mean region size: 90.5
Incorrectly Classified Instances: 26.8
Correctly Classified Instances: 73.2
Weighted Precision: 0.7117388114453412
Weighted AreaUnderROC: 0.6727904761904762
Root mean squared error: 0.46672649752836887
Relative absolute error: 64.02699931505812
Root relative squared error: 93.34529950567378
Weighted TruePositiveRate: 0.732
Weighted MatthewsCorrelation: 0.2993630556338965
Weighted FMeasure: 0.7111718275652702
Iteration time: 9.0
Weighted AreaUnderPRC: 0.6934257528036296
Mean absolute error: 0.3201349965752906
Coverage of cases: 92.2
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 99.0
Weighted Recall: 0.732
Weighted FalsePositiveRate: 0.4767619047619048
Kappa statistic: 0.28571428571428564
Training time: 8.0
		
Iteration: 20
Labeled set size: 480
Unlabelled set size: 20
	
Mean region size: 88.9
Incorrectly Classified Instances: 27.4
Correctly Classified Instances: 72.6
Weighted Precision: 0.704137275105017
Weighted AreaUnderROC: 0.6546285714285713
Root mean squared error: 0.47436128276804124
Relative absolute error: 68.45475468672892
Root relative squared error: 94.87225655360825
Weighted TruePositiveRate: 0.726
Weighted MatthewsCorrelation: 0.2815304634316631
Weighted FMeasure: 0.7041799174780241
Iteration time: 10.0
Weighted AreaUnderPRC: 0.6820213065471649
Mean absolute error: 0.34227377343364457
Coverage of cases: 91.0
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 109.0
Weighted Recall: 0.726
Weighted FalsePositiveRate: 0.4869523809523809
Kappa statistic: 0.26816239316239304
Training time: 10.0
		
Time end:Tue Oct 31 11.32.10 EET 2017