Tue Oct 31 11.35.08 EET 2017
Dataset: german_credit
Test set size: 500
Initial Labelled set size: 100
Initial Unlabelled set size: 400
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Tue Oct 31 11.35.08 EET 2017
		
Iteration: 1
Labeled set size: 100
Unlabelled set size: 400
	
Mean region size: 50.0
Incorrectly Classified Instances: 27.4
Correctly Classified Instances: 72.6
Weighted Precision: 0.7022709543208806
Weighted AreaUnderROC: 0.61
Root mean squared error: 0.523450093132096
Relative absolute error: 54.800000000000004
Root relative squared error: 104.6900186264192
Weighted TruePositiveRate: 0.726
Weighted MatthewsCorrelation: 0.27095380791264556
Weighted FMeasure: 0.6985725556904723
Iteration time: 26.0
Weighted AreaUnderPRC: 0.641617352864696
Mean absolute error: 0.274
Coverage of cases: 72.6
Instances selection time: 4.0
Test time: 3.0
Accumulative iteration time: 26.0
Weighted Recall: 0.726
Weighted FalsePositiveRate: 0.506
Kappa statistic: 0.25218340611353696
Training time: 22.0
		
Iteration: 2
Labeled set size: 120
Unlabelled set size: 380
	
Mean region size: 50.0
Incorrectly Classified Instances: 27.0
Correctly Classified Instances: 73.0
Weighted Precision: 0.7077958894401134
Weighted AreaUnderROC: 0.6166666666666667
Root mean squared error: 0.5196152422706632
Relative absolute error: 54.0
Root relative squared error: 103.92304845413265
Weighted TruePositiveRate: 0.73
Weighted MatthewsCorrelation: 0.2846576933131981
Weighted FMeasure: 0.7041301627033792
Iteration time: 24.0
Weighted AreaUnderPRC: 0.646016300496102
Mean absolute error: 0.27
Coverage of cases: 73.0
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 50.0
Weighted Recall: 0.73
Weighted FalsePositiveRate: 0.49666666666666665
Kappa statistic: 0.2663043478260869
Training time: 23.0
		
Iteration: 3
Labeled set size: 140
Unlabelled set size: 360
	
Mean region size: 50.0
Incorrectly Classified Instances: 26.6
Correctly Classified Instances: 73.4
Weighted Precision: 0.7134448727411903
Weighted AreaUnderROC: 0.6252380952380953
Root mean squared error: 0.5157518783291051
Relative absolute error: 53.2
Root relative squared error: 103.15037566582102
Weighted TruePositiveRate: 0.734
Weighted MatthewsCorrelation: 0.3000752570961464
Weighted FMeasure: 0.71071536571017
Iteration time: 19.0
Weighted AreaUnderPRC: 0.6515094835834768
Mean absolute error: 0.266
Coverage of cases: 73.4
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 69.0
Weighted Recall: 0.734
Weighted FalsePositiveRate: 0.48352380952380947
Kappa statistic: 0.28340517241379304
Training time: 17.0
		
Iteration: 4
Labeled set size: 160
Unlabelled set size: 340
	
Mean region size: 50.0
Incorrectly Classified Instances: 26.4
Correctly Classified Instances: 73.6
Weighted Precision: 0.7154791459781529
Weighted AreaUnderROC: 0.6152380952380953
Root mean squared error: 0.5138093031466052
Relative absolute error: 52.800000000000004
Root relative squared error: 102.76186062932105
Weighted TruePositiveRate: 0.736
Weighted MatthewsCorrelation: 0.2941819308940602
Weighted FMeasure: 0.7053989161007066
Iteration time: 19.0
Weighted AreaUnderPRC: 0.646574031777557
Mean absolute error: 0.264
Coverage of cases: 73.6
Instances selection time: 3.0
Test time: 3.0
Accumulative iteration time: 88.0
Weighted Recall: 0.736
Weighted FalsePositiveRate: 0.5055238095238095
Kappa statistic: 0.26829268292682923
Training time: 16.0
		
Iteration: 5
Labeled set size: 180
Unlabelled set size: 320
	
Mean region size: 50.0
Incorrectly Classified Instances: 27.2
Correctly Classified Instances: 72.8
Weighted Precision: 0.7075721153846154
Weighted AreaUnderROC: 0.5828571428571429
Root mean squared error: 0.521536192416212
Relative absolute error: 54.400000000000006
Root relative squared error: 104.30723848324239
Weighted TruePositiveRate: 0.728
Weighted MatthewsCorrelation: 0.24877052466147467
Weighted FMeasure: 0.678721556366163
Iteration time: 17.0
Weighted AreaUnderPRC: 0.6277962225274726
Mean absolute error: 0.272
Coverage of cases: 72.8
Instances selection time: 3.0
Test time: 3.0
Accumulative iteration time: 105.0
Weighted Recall: 0.728
Weighted FalsePositiveRate: 0.5622857142857144
Kappa statistic: 0.2037470725995316
Training time: 14.0
		
Iteration: 6
Labeled set size: 200
Unlabelled set size: 300
	
Mean region size: 50.0
Incorrectly Classified Instances: 26.6
Correctly Classified Instances: 73.4
Weighted Precision: 0.720444363998371
Weighted AreaUnderROC: 0.5871428571428572
Root mean squared error: 0.5157518783291051
Relative absolute error: 53.2
Root relative squared error: 103.15037566582102
Weighted TruePositiveRate: 0.734
Weighted MatthewsCorrelation: 0.2686303830820267
Weighted FMeasure: 0.6832677745782595
Iteration time: 26.0
Weighted AreaUnderPRC: 0.6317540793701073
Mean absolute error: 0.266
Coverage of cases: 73.4
Instances selection time: 3.0
Test time: 3.0
Accumulative iteration time: 131.0
Weighted Recall: 0.734
Weighted FalsePositiveRate: 0.5597142857142857
Kappa statistic: 0.21580188679245266
Training time: 23.0
		
Iteration: 7
Labeled set size: 220
Unlabelled set size: 280
	
Mean region size: 50.0
Incorrectly Classified Instances: 27.6
Correctly Classified Instances: 72.4
Weighted Precision: 0.706304347826087
Weighted AreaUnderROC: 0.5666666666666667
Root mean squared error: 0.5253570214625479
Relative absolute error: 55.2
Root relative squared error: 105.07140429250958
Weighted TruePositiveRate: 0.724
Weighted MatthewsCorrelation: 0.2252213082307254
Weighted FMeasure: 0.6628460038986355
Iteration time: 30.0
Weighted AreaUnderPRC: 0.6186521739130435
Mean absolute error: 0.276
Coverage of cases: 72.4
Instances selection time: 3.0
Test time: 3.0
Accumulative iteration time: 161.0
Weighted Recall: 0.724
Weighted FalsePositiveRate: 0.5906666666666667
Kappa statistic: 0.16867469879518054
Training time: 27.0
		
Iteration: 8
Labeled set size: 240
Unlabelled set size: 260
	
Mean region size: 50.0
Incorrectly Classified Instances: 27.2
Correctly Classified Instances: 72.8
Weighted Precision: 0.7256677350427351
Weighted AreaUnderROC: 0.5638095238095238
Root mean squared error: 0.521536192416212
Relative absolute error: 54.400000000000006
Root relative squared error: 104.30723848324239
Weighted TruePositiveRate: 0.728
Weighted MatthewsCorrelation: 0.23894460831859304
Weighted FMeasure: 0.6594427577312663
Iteration time: 21.0
Weighted AreaUnderPRC: 0.6187898504273505
Mean absolute error: 0.272
Coverage of cases: 72.8
Instances selection time: 3.0
Test time: 3.0
Accumulative iteration time: 182.0
Weighted Recall: 0.728
Weighted FalsePositiveRate: 0.6003809523809523
Kappa statistic: 0.16461916461916457
Training time: 18.0
		
Iteration: 9
Labeled set size: 260
Unlabelled set size: 240
	
Mean region size: 50.0
Incorrectly Classified Instances: 28.8
Correctly Classified Instances: 71.2
Weighted Precision: 0.6785685805422648
Weighted AreaUnderROC: 0.5561904761904762
Root mean squared error: 0.5366563145999496
Relative absolute error: 57.599999999999994
Root relative squared error: 107.33126291998991
Weighted TruePositiveRate: 0.712
Weighted MatthewsCorrelation: 0.1817873255506067
Weighted FMeasure: 0.6522575528894119
Iteration time: 36.0
Weighted AreaUnderPRC: 0.6105397926634769
Mean absolute error: 0.288
Coverage of cases: 71.2
Instances selection time: 3.0
Test time: 3.0
Accumulative iteration time: 218.0
Weighted Recall: 0.712
Weighted FalsePositiveRate: 0.5996190476190477
Kappa statistic: 0.1408114558472554
Training time: 33.0
		
Iteration: 10
Labeled set size: 280
Unlabelled set size: 220
	
Mean region size: 50.0
Incorrectly Classified Instances: 28.2
Correctly Classified Instances: 71.8
Weighted Precision: 0.7408247422680413
Weighted AreaUnderROC: 0.5357142857142858
Root mean squared error: 0.5310367218940701
Relative absolute error: 56.39999999999999
Root relative squared error: 106.20734437881403
Weighted TruePositiveRate: 0.718
Weighted MatthewsCorrelation: 0.19188238911333405
Weighted FMeasure: 0.6254327708219923
Iteration time: 24.0
Weighted AreaUnderPRC: 0.6027319587628867
Mean absolute error: 0.282
Coverage of cases: 71.8
Instances selection time: 3.0
Test time: 3.0
Accumulative iteration time: 242.0
Weighted Recall: 0.718
Weighted FalsePositiveRate: 0.6465714285714286
Kappa statistic: 0.09615384615384623
Training time: 21.0
		
Iteration: 11
Labeled set size: 300
Unlabelled set size: 200
	
Mean region size: 50.0
Incorrectly Classified Instances: 28.4
Correctly Classified Instances: 71.6
Weighted Precision: 0.6930555555555555
Weighted AreaUnderROC: 0.5495238095238095
Root mean squared error: 0.532916503778969
Relative absolute error: 56.8
Root relative squared error: 106.58330075579381
Weighted TruePositiveRate: 0.716
Weighted MatthewsCorrelation: 0.18544954675472894
Weighted FMeasure: 0.644418173513528
Iteration time: 42.0
Weighted AreaUnderPRC: 0.6080222222222222
Mean absolute error: 0.284
Coverage of cases: 71.6
Instances selection time: 3.0
Test time: 8.0
Accumulative iteration time: 284.0
Weighted Recall: 0.716
Weighted FalsePositiveRate: 0.616952380952381
Kappa statistic: 0.12776412776412768
Training time: 39.0
		
Iteration: 12
Labeled set size: 320
Unlabelled set size: 180
	
Mean region size: 50.0
Incorrectly Classified Instances: 27.2
Correctly Classified Instances: 72.8
Weighted Precision: 0.7610416666666666
Weighted AreaUnderROC: 0.5523809523809524
Root mean squared error: 0.521536192416212
Relative absolute error: 54.400000000000006
Root relative squared error: 104.30723848324239
Weighted TruePositiveRate: 0.728
Weighted MatthewsCorrelation: 0.2449894717530557
Weighted FMeasure: 0.6453012048192771
Iteration time: 27.0
Weighted AreaUnderPRC: 0.6146041666666667
Mean absolute error: 0.272
Coverage of cases: 72.8
Instances selection time: 3.0
Test time: 3.0
Accumulative iteration time: 311.0
Weighted Recall: 0.728
Weighted FalsePositiveRate: 0.6232380952380953
Kappa statistic: 0.13924050632911372
Training time: 24.0
		
Iteration: 13
Labeled set size: 340
Unlabelled set size: 160
	
Mean region size: 50.0
Incorrectly Classified Instances: 28.0
Correctly Classified Instances: 72.0
Weighted Precision: 0.7018429689472355
Weighted AreaUnderROC: 0.5561904761904762
Root mean squared error: 0.5291502622129182
Relative absolute error: 56.00000000000001
Root relative squared error: 105.83005244258364
Weighted TruePositiveRate: 0.72
Weighted MatthewsCorrelation: 0.2045690569374432
Weighted FMeasure: 0.6516410912190964
Iteration time: 45.0
Weighted AreaUnderPRC: 0.6123881848018177
Mean absolute error: 0.28
Coverage of cases: 72.0
Instances selection time: 3.0
Test time: 3.0
Accumulative iteration time: 356.0
Weighted Recall: 0.72
Weighted FalsePositiveRate: 0.6076190476190477
Kappa statistic: 0.14425427872860638
Training time: 42.0
		
Iteration: 14
Labeled set size: 360
Unlabelled set size: 140
	
Mean region size: 50.0
Incorrectly Classified Instances: 27.2
Correctly Classified Instances: 72.8
Weighted Precision: 0.7448529411764705
Weighted AreaUnderROC: 0.5561904761904762
Root mean squared error: 0.521536192416212
Relative absolute error: 54.400000000000006
Root relative squared error: 104.30723848324239
Weighted TruePositiveRate: 0.728
Weighted MatthewsCorrelation: 0.24091467912876244
Weighted FMeasure: 0.6502630040911747
Iteration time: 32.0
Weighted AreaUnderPRC: 0.6157883753501401
Mean absolute error: 0.272
Coverage of cases: 72.8
Instances selection time: 3.0
Test time: 3.0
Accumulative iteration time: 388.0
Weighted Recall: 0.728
Weighted FalsePositiveRate: 0.6156190476190476
Kappa statistic: 0.1478696741854637
Training time: 29.0
		
Iteration: 15
Labeled set size: 380
Unlabelled set size: 120
	
Mean region size: 50.0
Incorrectly Classified Instances: 28.0
Correctly Classified Instances: 72.0
Weighted Precision: 0.7456095041322315
Weighted AreaUnderROC: 0.539047619047619
Root mean squared error: 0.5291502622129182
Relative absolute error: 56.00000000000001
Root relative squared error: 105.83005244258364
Weighted TruePositiveRate: 0.72
Weighted MatthewsCorrelation: 0.20333939771990203
Weighted FMeasure: 0.629481956603392
Iteration time: 34.0
Weighted AreaUnderPRC: 0.6050828512396694
Mean absolute error: 0.28
Coverage of cases: 72.0
Instances selection time: 3.0
Test time: 3.0
Accumulative iteration time: 422.0
Weighted Recall: 0.72
Weighted FalsePositiveRate: 0.641904761904762
Kappa statistic: 0.10485933503836299
Training time: 31.0
		
Iteration: 16
Labeled set size: 400
Unlabelled set size: 100
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.4
Correctly Classified Instances: 70.6
Weighted Precision: 0.7929577464788732
Weighted AreaUnderROC: 0.51
Root mean squared error: 0.5422176684690383
Relative absolute error: 58.8
Root relative squared error: 108.44353369380767
Weighted TruePositiveRate: 0.706
Weighted MatthewsCorrelation: 0.11867816581938533
Weighted FMeasure: 0.5902771025765677
Iteration time: 48.0
Weighted AreaUnderPRC: 0.5871577464788733
Mean absolute error: 0.294
Coverage of cases: 70.6
Instances selection time: 2.0
Test time: 3.0
Accumulative iteration time: 470.0
Weighted Recall: 0.706
Weighted FalsePositiveRate: 0.686
Kappa statistic: 0.027777777777777655
Training time: 46.0
		
Iteration: 17
Labeled set size: 420
Unlabelled set size: 80
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.2
Correctly Classified Instances: 70.8
Weighted Precision: 0.7201219512195121
Weighted AreaUnderROC: 0.5171428571428571
Root mean squared error: 0.5403702434442518
Relative absolute error: 58.4
Root relative squared error: 108.07404868885035
Weighted TruePositiveRate: 0.708
Weighted MatthewsCorrelation: 0.12521758066945232
Weighted FMeasure: 0.6014071379175562
Iteration time: 79.0
Weighted AreaUnderPRC: 0.5904926829268293
Mean absolute error: 0.292
Coverage of cases: 70.8
Instances selection time: 3.0
Test time: 3.0
Accumulative iteration time: 549.0
Weighted Recall: 0.708
Weighted FalsePositiveRate: 0.6737142857142857
Kappa statistic: 0.046997389033942454
Training time: 76.0
		
Iteration: 18
Labeled set size: 440
Unlabelled set size: 60
	
Mean region size: 50.0
Incorrectly Classified Instances: 26.8
Correctly Classified Instances: 73.2
Weighted Precision: 0.7295977011494252
Weighted AreaUnderROC: 0.5723809523809524
Root mean squared error: 0.5176871642217914
Relative absolute error: 53.6
Root relative squared error: 103.53743284435828
Weighted TruePositiveRate: 0.732
Weighted MatthewsCorrelation: 0.2566396178498175
Weighted FMeasure: 0.6686375525085202
Iteration time: 81.0
Weighted AreaUnderPRC: 0.624231417624521
Mean absolute error: 0.268
Coverage of cases: 73.2
Instances selection time: 2.0
Test time: 3.0
Accumulative iteration time: 630.0
Weighted Recall: 0.732
Weighted FalsePositiveRate: 0.5872380952380952
Kappa statistic: 0.18491484184914833
Training time: 79.0
		
Iteration: 19
Labeled set size: 460
Unlabelled set size: 40
	
Mean region size: 50.0
Incorrectly Classified Instances: 27.2
Correctly Classified Instances: 72.8
Weighted Precision: 0.7048755339519812
Weighted AreaUnderROC: 0.5923809523809523
Root mean squared error: 0.521536192416212
Relative absolute error: 54.400000000000006
Root relative squared error: 104.30723848324239
Weighted TruePositiveRate: 0.728
Weighted MatthewsCorrelation: 0.2568970886605476
Weighted FMeasure: 0.686734987070204
Iteration time: 126.0
Weighted AreaUnderPRC: 0.6326591839740757
Mean absolute error: 0.272
Coverage of cases: 72.8
Instances selection time: 3.0
Test time: 3.0
Accumulative iteration time: 756.0
Weighted Recall: 0.728
Weighted FalsePositiveRate: 0.5432380952380953
Kappa statistic: 0.22196796338672767
Training time: 123.0
		
Iteration: 20
Labeled set size: 480
Unlabelled set size: 20
	
Mean region size: 50.0
Incorrectly Classified Instances: 24.4
Correctly Classified Instances: 75.6
Weighted Precision: 0.7421286031042128
Weighted AreaUnderROC: 0.6447619047619046
Root mean squared error: 0.4939635614091388
Relative absolute error: 48.8
Root relative squared error: 98.79271228182776
Weighted TruePositiveRate: 0.756
Weighted MatthewsCorrelation: 0.35831820114247903
Weighted FMeasure: 0.7310434626436781
Iteration time: 133.0
Weighted AreaUnderPRC: 0.6677623059866962
Mean absolute error: 0.244
Coverage of cases: 75.6
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 889.0
Weighted Recall: 0.756
Weighted FalsePositiveRate: 0.4664761904761905
Kappa statistic: 0.3326039387308535
Training time: 133.0
		
Time end:Tue Oct 31 11.35.10 EET 2017