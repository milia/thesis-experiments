Tue Oct 31 11.34.13 EET 2017
Dataset: german_credit
Test set size: 500
Initial Labelled set size: 100
Initial Unlabelled set size: 400
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Tue Oct 31 11.34.13 EET 2017
		
Iteration: 1
Labeled set size: 100
Unlabelled set size: 400
	
Mean region size: 50.0
Incorrectly Classified Instances: 36.0
Correctly Classified Instances: 64.0
Weighted Precision: 0.6667689579923621
Weighted AreaUnderROC: 0.6076190476190476
Root mean squared error: 0.5941694146863441
Relative absolute error: 72.54901960784326
Root relative squared error: 118.83388293726883
Weighted TruePositiveRate: 0.64
Weighted MatthewsCorrelation: 0.20363037926338098
Weighted FMeasure: 0.6499043601065447
Iteration time: 1.0
Weighted AreaUnderPRC: 0.6339077195853792
Mean absolute error: 0.36274509803921634
Coverage of cases: 64.0
Instances selection time: 1.0
Test time: 18.0
Accumulative iteration time: 1.0
Weighted Recall: 0.64
Weighted FalsePositiveRate: 0.4247619047619048
Kappa statistic: 0.20071047957371232
Training time: 0.0
		
Iteration: 2
Labeled set size: 120
Unlabelled set size: 380
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.4
Correctly Classified Instances: 65.6
Weighted Precision: 0.6735339506172839
Weighted AreaUnderROC: 0.6152380952380953
Root mean squared error: 0.5817455130707602
Relative absolute error: 69.31147540983613
Root relative squared error: 116.34910261415203
Weighted TruePositiveRate: 0.656
Weighted MatthewsCorrelation: 0.2211448752503668
Weighted FMeasure: 0.6630827765742477
Iteration time: 1.0
Weighted AreaUnderPRC: 0.638670061728395
Mean absolute error: 0.34655737704918066
Coverage of cases: 65.6
Instances selection time: 1.0
Test time: 21.0
Accumulative iteration time: 2.0
Weighted Recall: 0.656
Weighted FalsePositiveRate: 0.4255238095238095
Kappa statistic: 0.21960072595281308
Training time: 0.0
		
Iteration: 3
Labeled set size: 140
Unlabelled set size: 360
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.8
Correctly Classified Instances: 65.2
Weighted Precision: 0.6654901960784314
Weighted AreaUnderROC: 0.6047619047619047
Root mean squared error: 0.5857885154933905
Relative absolute error: 70.0281690140844
Root relative squared error: 117.1577030986781
Weighted TruePositiveRate: 0.652
Weighted MatthewsCorrelation: 0.20268966403502012
Weighted FMeasure: 0.6577573529411765
Iteration time: 1.0
Weighted AreaUnderPRC: 0.6326274509803922
Mean absolute error: 0.350140845070422
Coverage of cases: 65.2
Instances selection time: 1.0
Test time: 23.0
Accumulative iteration time: 3.0
Weighted Recall: 0.652
Weighted FalsePositiveRate: 0.44247619047619047
Kappa statistic: 0.20183486238532125
Training time: 0.0
		
Iteration: 4
Labeled set size: 160
Unlabelled set size: 340
	
Mean region size: 50.0
Incorrectly Classified Instances: 36.4
Correctly Classified Instances: 63.6
Weighted Precision: 0.6485370051635112
Weighted AreaUnderROC: 0.5838095238095238
Root mean squared error: 0.5996201103924684
Relative absolute error: 73.13580246913591
Root relative squared error: 119.92402207849369
Weighted TruePositiveRate: 0.636
Weighted MatthewsCorrelation: 0.1626220149098983
Weighted FMeasure: 0.6414983677308693
Iteration time: 0.0
Weighted AreaUnderPRC: 0.6207536431440046
Mean absolute error: 0.36567901234567957
Coverage of cases: 63.6
Instances selection time: 0.0
Test time: 27.0
Accumulative iteration time: 3.0
Weighted Recall: 0.636
Weighted FalsePositiveRate: 0.46838095238095234
Kappa statistic: 0.16206261510128916
Training time: 0.0
		
Iteration: 5
Labeled set size: 180
Unlabelled set size: 320
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.4
Correctly Classified Instances: 65.6
Weighted Precision: 0.6639491562568485
Weighted AreaUnderROC: 0.6019047619047619
Root mean squared error: 0.5833094974457474
Relative absolute error: 69.14285714285727
Root relative squared error: 116.66189948914949
Weighted TruePositiveRate: 0.656
Weighted MatthewsCorrelation: 0.19956678850642037
Weighted FMeasure: 0.6596153846153846
Iteration time: 1.0
Weighted AreaUnderPRC: 0.631163649645701
Mean absolute error: 0.34571428571428636
Coverage of cases: 65.6
Instances selection time: 1.0
Test time: 29.0
Accumulative iteration time: 4.0
Weighted Recall: 0.656
Weighted FalsePositiveRate: 0.45219047619047614
Kappa statistic: 0.19925512104283058
Training time: 0.0
		
Iteration: 6
Labeled set size: 200
Unlabelled set size: 300
	
Mean region size: 50.0
Incorrectly Classified Instances: 33.8
Correctly Classified Instances: 66.2
Weighted Precision: 0.6691712929881456
Weighted AreaUnderROC: 0.608095238095238
Root mean squared error: 0.5785135890778803
Relative absolute error: 67.92079207920783
Root relative squared error: 115.70271781557607
Weighted TruePositiveRate: 0.662
Weighted MatthewsCorrelation: 0.2120329827158805
Weighted FMeasure: 0.6652793787538676
Iteration time: 1.0
Weighted AreaUnderPRC: 0.6348411770094725
Mean absolute error: 0.33960396039603913
Coverage of cases: 66.2
Instances selection time: 1.0
Test time: 32.0
Accumulative iteration time: 5.0
Weighted Recall: 0.662
Weighted FalsePositiveRate: 0.44580952380952377
Kappa statistic: 0.2117537313432836
Training time: 0.0
		
Iteration: 7
Labeled set size: 220
Unlabelled set size: 280
	
Mean region size: 50.0
Incorrectly Classified Instances: 33.0
Correctly Classified Instances: 67.0
Weighted Precision: 0.6877525318780501
Weighted AreaUnderROC: 0.6328571428571429
Root mean squared error: 0.5718805098863372
Relative absolute error: 66.30630630630637
Root relative squared error: 114.37610197726744
Weighted TruePositiveRate: 0.67
Weighted MatthewsCorrelation: 0.25462829474063237
Weighted FMeasure: 0.6770042395408754
Iteration time: 1.0
Weighted AreaUnderPRC: 0.6494154693813297
Mean absolute error: 0.33153153153153186
Coverage of cases: 67.0
Instances selection time: 1.0
Test time: 34.0
Accumulative iteration time: 6.0
Weighted Recall: 0.67
Weighted FalsePositiveRate: 0.4042857142857143
Kappa statistic: 0.2527173913043479
Training time: 0.0
		
Iteration: 8
Labeled set size: 240
Unlabelled set size: 260
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.0
Correctly Classified Instances: 66.0
Weighted Precision: 0.6746313102665911
Weighted AreaUnderROC: 0.6161904761904762
Root mean squared error: 0.5806954089546543
Relative absolute error: 68.26446280991718
Root relative squared error: 116.13908179093086
Weighted TruePositiveRate: 0.66
Weighted MatthewsCorrelation: 0.2241707958562531
Weighted FMeasure: 0.6660986826435076
Iteration time: 1.0
Weighted AreaUnderPRC: 0.6393530346001135
Mean absolute error: 0.3413223140495859
Coverage of cases: 66.0
Instances selection time: 1.0
Test time: 36.0
Accumulative iteration time: 7.0
Weighted Recall: 0.66
Weighted FalsePositiveRate: 0.42761904761904757
Kappa statistic: 0.22303473491773315
Training time: 0.0
		
Iteration: 9
Labeled set size: 260
Unlabelled set size: 240
	
Mean region size: 50.0
Incorrectly Classified Instances: 33.8
Correctly Classified Instances: 66.2
Weighted Precision: 0.6704975332690102
Weighted AreaUnderROC: 0.61
Root mean squared error: 0.5791670011696697
Relative absolute error: 67.84732824427498
Root relative squared error: 115.83340023393393
Weighted TruePositiveRate: 0.662
Weighted MatthewsCorrelation: 0.21507680675700822
Weighted FMeasure: 0.6658212071747793
Iteration time: 0.0
Weighted AreaUnderPRC: 0.6359141868890062
Mean absolute error: 0.33923664122137487
Coverage of cases: 66.2
Instances selection time: 0.0
Test time: 39.0
Accumulative iteration time: 7.0
Weighted Recall: 0.662
Weighted FalsePositiveRate: 0.442
Kappa statistic: 0.21468401486988856
Training time: 0.0
		
Iteration: 10
Labeled set size: 280
Unlabelled set size: 220
	
Mean region size: 50.0
Incorrectly Classified Instances: 33.4
Correctly Classified Instances: 66.6
Weighted Precision: 0.6784157743256046
Weighted AreaUnderROC: 0.6204761904761906
Root mean squared error: 0.5758852146859981
Relative absolute error: 67.03546099290796
Root relative squared error: 115.17704293719962
Weighted TruePositiveRate: 0.666
Weighted MatthewsCorrelation: 0.2334283093662212
Weighted FMeasure: 0.6712873839411894
Iteration time: 1.0
Weighted AreaUnderPRC: 0.64204264287885
Mean absolute error: 0.3351773049645398
Coverage of cases: 66.6
Instances selection time: 1.0
Test time: 42.0
Accumulative iteration time: 8.0
Weighted Recall: 0.666
Weighted FalsePositiveRate: 0.42504761904761906
Kappa statistic: 0.2325367647058825
Training time: 0.0
		
Iteration: 11
Labeled set size: 300
Unlabelled set size: 200
	
Mean region size: 50.0
Incorrectly Classified Instances: 35.0
Correctly Classified Instances: 65.0
Weighted Precision: 0.667032967032967
Weighted AreaUnderROC: 0.6071428571428571
Root mean squared error: 0.5896550547876986
Relative absolute error: 70.19867549668909
Root relative squared error: 117.93101095753973
Weighted TruePositiveRate: 0.65
Weighted MatthewsCorrelation: 0.2058790548922549
Weighted FMeasure: 0.656980056980057
Iteration time: 1.0
Weighted AreaUnderPRC: 0.633901098901099
Mean absolute error: 0.35099337748344545
Coverage of cases: 65.0
Instances selection time: 1.0
Test time: 44.0
Accumulative iteration time: 9.0
Weighted Recall: 0.65
Weighted FalsePositiveRate: 0.4357142857142857
Kappa statistic: 0.2045454545454545
Training time: 0.0
		
Iteration: 12
Labeled set size: 320
Unlabelled set size: 180
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.4
Correctly Classified Instances: 65.6
Weighted Precision: 0.6693761140819965
Weighted AreaUnderROC: 0.6095238095238095
Root mean squared error: 0.5846990668086472
Relative absolute error: 68.99378881987563
Root relative squared error: 116.93981336172945
Weighted TruePositiveRate: 0.656
Weighted MatthewsCorrelation: 0.2119028305820665
Weighted FMeasure: 0.6616911764705883
Iteration time: 1.0
Weighted AreaUnderPRC: 0.6354295900178253
Mean absolute error: 0.34496894409937817
Coverage of cases: 65.6
Instances selection time: 1.0
Test time: 47.0
Accumulative iteration time: 10.0
Weighted Recall: 0.656
Weighted FalsePositiveRate: 0.436952380952381
Kappa statistic: 0.21100917431192676
Training time: 0.0
		
Iteration: 13
Labeled set size: 340
Unlabelled set size: 160
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.6
Correctly Classified Instances: 65.4
Weighted Precision: 0.6681224337439344
Weighted AreaUnderROC: 0.608095238095238
Root mean squared error: 0.5865024789600971
Relative absolute error: 69.38011695906432
Root relative squared error: 117.30049579201942
Weighted TruePositiveRate: 0.654
Weighted MatthewsCorrelation: 0.2088431358449962
Weighted FMeasure: 0.6599672415454282
Iteration time: 0.0
Weighted AreaUnderPRC: 0.63455924563181
Mean absolute error: 0.3469005847953216
Coverage of cases: 65.4
Instances selection time: 0.0
Test time: 50.0
Accumulative iteration time: 10.0
Weighted Recall: 0.654
Weighted FalsePositiveRate: 0.43780952380952387
Kappa statistic: 0.2078754578754579
Training time: 0.0
		
Iteration: 14
Labeled set size: 360
Unlabelled set size: 140
	
Mean region size: 50.0
Incorrectly Classified Instances: 33.2
Correctly Classified Instances: 66.8
Weighted Precision: 0.6823879750425411
Weighted AreaUnderROC: 0.6257142857142857
Root mean squared error: 0.5746071500434434
Relative absolute error: 66.5856353591161
Root relative squared error: 114.92143000868869
Weighted TruePositiveRate: 0.668
Weighted MatthewsCorrelation: 0.24254545125430668
Weighted FMeasure: 0.6739551842283662
Iteration time: 0.0
Weighted AreaUnderPRC: 0.6451539421440725
Mean absolute error: 0.33292817679558045
Coverage of cases: 66.8
Instances selection time: 0.0
Test time: 53.0
Accumulative iteration time: 10.0
Weighted Recall: 0.668
Weighted FalsePositiveRate: 0.41657142857142854
Kappa statistic: 0.24131627056672766
Training time: 0.0
		
Iteration: 15
Labeled set size: 380
Unlabelled set size: 120
	
Mean region size: 50.0
Incorrectly Classified Instances: 32.2
Correctly Classified Instances: 67.8
Weighted Precision: 0.6861681018004405
Weighted AreaUnderROC: 0.6290476190476191
Root mean squared error: 0.5659690707167464
Relative absolute error: 64.58638743455509
Root relative squared error: 113.19381414334929
Weighted TruePositiveRate: 0.678
Weighted MatthewsCorrelation: 0.2523195438577889
Weighted FMeasure: 0.6816403216280442
Iteration time: 0.0
Weighted AreaUnderPRC: 0.6475696819646466
Mean absolute error: 0.32293193717277546
Coverage of cases: 67.8
Instances selection time: 0.0
Test time: 54.0
Accumulative iteration time: 10.0
Weighted Recall: 0.678
Weighted FalsePositiveRate: 0.4199047619047619
Kappa statistic: 0.25185873605947967
Training time: 0.0
		
Iteration: 16
Labeled set size: 400
Unlabelled set size: 100
	
Mean region size: 50.0
Incorrectly Classified Instances: 32.4
Correctly Classified Instances: 67.6
Weighted Precision: 0.6848577235772357
Weighted AreaUnderROC: 0.6276190476190476
Root mean squared error: 0.5677977172084576
Relative absolute error: 64.97512437810954
Root relative squared error: 113.55954344169152
Weighted TruePositiveRate: 0.676
Weighted MatthewsCorrelation: 0.24913433444134775
Weighted FMeasure: 0.6799168074873262
Iteration time: 1.0
Weighted AreaUnderPRC: 0.6466342624854821
Mean absolute error: 0.3248756218905477
Coverage of cases: 67.6
Instances selection time: 1.0
Test time: 57.0
Accumulative iteration time: 11.0
Weighted Recall: 0.676
Weighted FalsePositiveRate: 0.4207619047619048
Kappa statistic: 0.24860853432282018
Training time: 0.0
		
Iteration: 17
Labeled set size: 420
Unlabelled set size: 80
	
Mean region size: 50.0
Incorrectly Classified Instances: 32.8
Correctly Classified Instances: 67.2
Weighted Precision: 0.6809451219512196
Weighted AreaUnderROC: 0.6228571428571429
Root mean squared error: 0.571359005317961
Relative absolute error: 65.76303317535539
Root relative squared error: 114.2718010635922
Weighted TruePositiveRate: 0.672
Weighted MatthewsCorrelation: 0.2398382771860736
Weighted FMeasure: 0.6759651631353177
Iteration time: 1.0
Weighted AreaUnderPRC: 0.6436797038327525
Mean absolute error: 0.32881516587677695
Coverage of cases: 67.2
Instances selection time: 1.0
Test time: 57.0
Accumulative iteration time: 12.0
Weighted Recall: 0.672
Weighted FalsePositiveRate: 0.42628571428571427
Kappa statistic: 0.23933209647495376
Training time: 0.0
		
Iteration: 18
Labeled set size: 440
Unlabelled set size: 60
	
Mean region size: 50.0
Incorrectly Classified Instances: 32.8
Correctly Classified Instances: 67.2
Weighted Precision: 0.679640587332895
Weighted AreaUnderROC: 0.6209523809523809
Root mean squared error: 0.5714201219369062
Relative absolute error: 65.75565610859717
Root relative squared error: 114.28402438738124
Weighted TruePositiveRate: 0.672
Weighted MatthewsCorrelation: 0.23686899196556435
Weighted FMeasure: 0.6754472271914131
Iteration time: 0.0
Weighted AreaUnderPRC: 0.6425824530645043
Mean absolute error: 0.32877828054298586
Coverage of cases: 67.2
Instances selection time: 0.0
Test time: 60.0
Accumulative iteration time: 12.0
Weighted Recall: 0.672
Weighted FalsePositiveRate: 0.43009523809523814
Kappa statistic: 0.2364990689013036
Training time: 0.0
		
Iteration: 19
Labeled set size: 460
Unlabelled set size: 40
	
Mean region size: 50.0
Incorrectly Classified Instances: 32.0
Correctly Classified Instances: 68.0
Weighted Precision: 0.6849507735583685
Weighted AreaUnderROC: 0.6266666666666667
Root mean squared error: 0.5644638196415611
Relative absolute error: 64.15584415584425
Root relative squared error: 112.89276392831222
Weighted TruePositiveRate: 0.68
Weighted MatthewsCorrelation: 0.24970681354989005
Weighted FMeasure: 0.6823061331731852
Iteration time: 1.0
Weighted AreaUnderPRC: 0.6462942334739803
Mean absolute error: 0.3207792207792213
Coverage of cases: 68.0
Instances selection time: 1.0
Test time: 62.0
Accumulative iteration time: 13.0
Weighted Recall: 0.68
Weighted FalsePositiveRate: 0.4266666666666667
Kappa statistic: 0.24953095684803014
Training time: 0.0
		
Iteration: 20
Labeled set size: 480
Unlabelled set size: 20
	
Mean region size: 50.0
Incorrectly Classified Instances: 31.8
Correctly Classified Instances: 68.2
Weighted Precision: 0.6862992330690237
Weighted AreaUnderROC: 0.628095238095238
Root mean squared error: 0.5627475475370036
Relative absolute error: 63.751037344398384
Root relative squared error: 112.54950950740073
Weighted TruePositiveRate: 0.682
Weighted MatthewsCorrelation: 0.25295644340840934
Weighted FMeasure: 0.684019346560063
Iteration time: 0.0
Weighted AreaUnderPRC: 0.6472372620749846
Mean absolute error: 0.31875518672199193
Coverage of cases: 68.2
Instances selection time: 0.0
Test time: 65.0
Accumulative iteration time: 13.0
Weighted Recall: 0.682
Weighted FalsePositiveRate: 0.42580952380952386
Kappa statistic: 0.25281954887218056
Training time: 0.0
		
Time end:Tue Oct 31 11.34.14 EET 2017