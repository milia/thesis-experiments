Tue Oct 31 11.35.21 EET 2017
Dataset: german_credit
Test set size: 500
Initial Labelled set size: 100
Initial Unlabelled set size: 400
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Tue Oct 31 11.35.21 EET 2017
		
Iteration: 1
Labeled set size: 100
Unlabelled set size: 400
	
Mean region size: 50.0
Incorrectly Classified Instances: 38.6
Correctly Classified Instances: 61.4
Weighted Precision: 0.6234858276747192
Weighted AreaUnderROC: 0.5528571428571429
Root mean squared error: 0.6152458772126342
Relative absolute error: 77.64705882352965
Root relative squared error: 123.04917544252685
Weighted TruePositiveRate: 0.614
Weighted MatthewsCorrelation: 0.1033485954546663
Weighted FMeasure: 0.6183638638149848
Iteration time: 14.0
Weighted AreaUnderPRC: 0.604442995758315
Mean absolute error: 0.38823529411764823
Coverage of cases: 61.4
Instances selection time: 14.0
Test time: 18.0
Accumulative iteration time: 14.0
Weighted Recall: 0.614
Weighted FalsePositiveRate: 0.5082857142857142
Kappa statistic: 0.10315985130111523
Training time: 0.0
		
Iteration: 2
Labeled set size: 120
Unlabelled set size: 380
	
Mean region size: 50.0
Incorrectly Classified Instances: 37.4
Correctly Classified Instances: 62.6
Weighted Precision: 0.6209927327574386
Weighted AreaUnderROC: 0.5480952380952381
Root mean squared error: 0.6065773146922611
Relative absolute error: 75.21311475409856
Root relative squared error: 121.31546293845221
Weighted TruePositiveRate: 0.626
Weighted MatthewsCorrelation: 0.09754603017356965
Weighted FMeasure: 0.623383908356706
Iteration time: 17.0
Weighted AreaUnderPRC: 0.6021981978805507
Mean absolute error: 0.37606557377049277
Coverage of cases: 62.6
Instances selection time: 16.0
Test time: 20.0
Accumulative iteration time: 31.0
Weighted Recall: 0.626
Weighted FalsePositiveRate: 0.5298095238095237
Kappa statistic: 0.09749034749034748
Training time: 1.0
		
Iteration: 3
Labeled set size: 140
Unlabelled set size: 360
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.4
Correctly Classified Instances: 65.6
Weighted Precision: 0.6351229035406869
Weighted AreaUnderROC: 0.5600000000000002
Root mean squared error: 0.5824126740654944
Relative absolute error: 69.2394366197184
Root relative squared error: 116.48253481309887
Weighted TruePositiveRate: 0.656
Weighted MatthewsCorrelation: 0.1295054999035947
Weighted FMeasure: 0.6429818122502242
Iteration time: 17.0
Weighted AreaUnderPRC: 0.6087220516461088
Mean absolute error: 0.34619718309859204
Coverage of cases: 65.6
Instances selection time: 17.0
Test time: 23.0
Accumulative iteration time: 48.0
Weighted Recall: 0.656
Weighted FalsePositiveRate: 0.536
Kappa statistic: 0.12778904665314403
Training time: 0.0
		
Iteration: 4
Labeled set size: 160
Unlabelled set size: 340
	
Mean region size: 50.0
Incorrectly Classified Instances: 32.4
Correctly Classified Instances: 67.6
Weighted Precision: 0.6488539238539238
Weighted AreaUnderROC: 0.5704761904761905
Root mean squared error: 0.5657191033963488
Relative absolute error: 65.23456790123439
Root relative squared error: 113.14382067926975
Weighted TruePositiveRate: 0.676
Weighted MatthewsCorrelation: 0.159143097011857
Weighted FMeasure: 0.6566506934622448
Iteration time: 17.0
Weighted AreaUnderPRC: 0.6149185703185703
Mean absolute error: 0.32617283950617193
Coverage of cases: 67.6
Instances selection time: 17.0
Test time: 26.0
Accumulative iteration time: 65.0
Weighted Recall: 0.676
Weighted FalsePositiveRate: 0.535047619047619
Kappa statistic: 0.1544885177453028
Training time: 0.0
		
Iteration: 5
Labeled set size: 180
Unlabelled set size: 320
	
Mean region size: 50.0
Incorrectly Classified Instances: 31.8
Correctly Classified Instances: 68.2
Weighted Precision: 0.6530718657900704
Weighted AreaUnderROC: 0.5728571428571428
Root mean squared error: 0.560834809989648
Relative absolute error: 63.99999999999989
Root relative squared error: 112.16696199792959
Weighted TruePositiveRate: 0.682
Weighted MatthewsCorrelation: 0.16756835252347646
Weighted FMeasure: 0.6602313381354981
Iteration time: 18.0
Weighted AreaUnderPRC: 0.6164966220811607
Mean absolute error: 0.31999999999999945
Coverage of cases: 68.2
Instances selection time: 18.0
Test time: 28.0
Accumulative iteration time: 83.0
Weighted Recall: 0.682
Weighted FalsePositiveRate: 0.5362857142857143
Kappa statistic: 0.16139240506329122
Training time: 0.0
		
Iteration: 6
Labeled set size: 200
Unlabelled set size: 300
	
Mean region size: 50.0
Incorrectly Classified Instances: 31.4
Correctly Classified Instances: 68.6
Weighted Precision: 0.6482542208079201
Weighted AreaUnderROC: 0.5604761904761905
Root mean squared error: 0.5575980599949631
Relative absolute error: 63.16831683168329
Root relative squared error: 111.51961199899263
Weighted TruePositiveRate: 0.686
Weighted MatthewsCorrelation: 0.1504334974737726
Weighted FMeasure: 0.6531910222417374
Iteration time: 19.0
Weighted AreaUnderPRC: 0.6101523498040602
Mean absolute error: 0.31584158415841646
Coverage of cases: 68.6
Instances selection time: 19.0
Test time: 31.0
Accumulative iteration time: 102.0
Weighted Recall: 0.686
Weighted FalsePositiveRate: 0.5650476190476189
Kappa statistic: 0.13925438596491246
Training time: 0.0
		
Iteration: 7
Labeled set size: 220
Unlabelled set size: 280
	
Mean region size: 50.0
Incorrectly Classified Instances: 31.8
Correctly Classified Instances: 68.2
Weighted Precision: 0.6416999909798851
Weighted AreaUnderROC: 0.5538095238095239
Root mean squared error: 0.5613870551553238
Relative absolute error: 63.9279279279278
Root relative squared error: 112.27741103106477
Weighted TruePositiveRate: 0.682
Weighted MatthewsCorrelation: 0.1352116780188925
Weighted FMeasure: 0.6473450801148624
Iteration time: 19.0
Weighted AreaUnderPRC: 0.6064392615532638
Mean absolute error: 0.319639639639639
Coverage of cases: 68.2
Instances selection time: 19.0
Test time: 35.0
Accumulative iteration time: 121.0
Weighted Recall: 0.682
Weighted FalsePositiveRate: 0.5743809523809524
Kappa statistic: 0.12444933920704852
Training time: 0.0
		
Iteration: 8
Labeled set size: 240
Unlabelled set size: 260
	
Mean region size: 50.0
Incorrectly Classified Instances: 31.2
Correctly Classified Instances: 68.8
Weighted Precision: 0.6472275091993401
Weighted AreaUnderROC: 0.5561904761904762
Root mean squared error: 0.5562720224311933
Relative absolute error: 62.71074380165311
Root relative squared error: 111.25440448623867
Weighted TruePositiveRate: 0.688
Weighted MatthewsCorrelation: 0.1450279474451314
Weighted FMeasure: 0.6503497790868924
Iteration time: 19.0
Weighted AreaUnderPRC: 0.6080169521634309
Mean absolute error: 0.31355371900826556
Coverage of cases: 68.8
Instances selection time: 18.0
Test time: 36.0
Accumulative iteration time: 140.0
Weighted Recall: 0.688
Weighted FalsePositiveRate: 0.5756190476190477
Kappa statistic: 0.13140311804008883
Training time: 1.0
		
Iteration: 9
Labeled set size: 260
Unlabelled set size: 240
	
Mean region size: 50.0
Incorrectly Classified Instances: 31.2
Correctly Classified Instances: 68.8
Weighted Precision: 0.6428152492668621
Weighted AreaUnderROC: 0.5485714285714286
Root mean squared error: 0.5564466627865694
Relative absolute error: 62.68702290076325
Root relative squared error: 111.28933255731388
Weighted TruePositiveRate: 0.688
Weighted MatthewsCorrelation: 0.13151451626080002
Weighted FMeasure: 0.6440476190476192
Iteration time: 19.0
Weighted AreaUnderPRC: 0.6040321742773357
Mean absolute error: 0.3134351145038163
Coverage of cases: 68.8
Instances selection time: 18.0
Test time: 39.0
Accumulative iteration time: 159.0
Weighted Recall: 0.688
Weighted FalsePositiveRate: 0.5908571428571429
Kappa statistic: 0.11564625850340121
Training time: 1.0
		
Iteration: 10
Labeled set size: 280
Unlabelled set size: 220
	
Mean region size: 50.0
Incorrectly Classified Instances: 31.0
Correctly Classified Instances: 69.0
Weighted Precision: 0.6442737277977553
Weighted AreaUnderROC: 0.5480952380952381
Root mean squared error: 0.5548098712711375
Relative absolute error: 62.269503546099095
Root relative squared error: 110.96197425422748
Weighted TruePositiveRate: 0.69
Weighted MatthewsCorrelation: 0.13283139486494489
Weighted FMeasure: 0.643824829536303
Iteration time: 18.0
Weighted AreaUnderPRC: 0.6039052776869711
Mean absolute error: 0.31134751773049546
Coverage of cases: 69.0
Instances selection time: 17.0
Test time: 42.0
Accumulative iteration time: 177.0
Weighted Recall: 0.69
Weighted FalsePositiveRate: 0.5938095238095238
Kappa statistic: 0.115296803652968
Training time: 1.0
		
Iteration: 11
Labeled set size: 300
Unlabelled set size: 200
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.6
Correctly Classified Instances: 69.4
Weighted Precision: 0.6487220877051385
Weighted AreaUnderROC: 0.549047619047619
Root mean squared error: 0.5513478705546617
Relative absolute error: 61.45695364238421
Root relative squared error: 110.26957411093234
Weighted TruePositiveRate: 0.694
Weighted MatthewsCorrelation: 0.139342076052131
Weighted FMeasure: 0.6449845450311217
Iteration time: 17.0
Weighted AreaUnderPRC: 0.6046774049732887
Mean absolute error: 0.307284768211921
Coverage of cases: 69.4
Instances selection time: 17.0
Test time: 44.0
Accumulative iteration time: 194.0
Weighted Recall: 0.694
Weighted FalsePositiveRate: 0.5959047619047619
Kappa statistic: 0.11866359447004582
Training time: 0.0
		
Iteration: 12
Labeled set size: 320
Unlabelled set size: 180
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.8
Correctly Classified Instances: 70.2
Weighted Precision: 0.6602127390148157
Weighted AreaUnderROC: 0.5528571428571429
Root mean squared error: 0.5442046609618624
Relative absolute error: 59.85093167701877
Root relative squared error: 108.84093219237248
Weighted TruePositiveRate: 0.702
Weighted MatthewsCorrelation: 0.15736988775386396
Weighted FMeasure: 0.6489372091154637
Iteration time: 17.0
Weighted AreaUnderPRC: 0.6074007091300493
Mean absolute error: 0.29925465838509385
Coverage of cases: 70.2
Instances selection time: 17.0
Test time: 46.0
Accumulative iteration time: 211.0
Weighted Recall: 0.702
Weighted FalsePositiveRate: 0.5962857142857143
Kappa statistic: 0.12967289719626166
Training time: 0.0
		
Iteration: 13
Labeled set size: 340
Unlabelled set size: 160
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.2
Correctly Classified Instances: 69.8
Weighted Precision: 0.652181317961483
Weighted AreaUnderROC: 0.5461904761904762
Root mean squared error: 0.5479438545762327
Relative absolute error: 60.631578947368524
Root relative squared error: 109.58877091524654
Weighted TruePositiveRate: 0.698
Weighted MatthewsCorrelation: 0.13987942176459978
Weighted FMeasure: 0.6423365027179496
Iteration time: 17.0
Weighted AreaUnderPRC: 0.6035089130529718
Mean absolute error: 0.3031578947368426
Coverage of cases: 69.8
Instances selection time: 16.0
Test time: 49.0
Accumulative iteration time: 228.0
Weighted Recall: 0.698
Weighted FalsePositiveRate: 0.6056190476190477
Kappa statistic: 0.1138497652582158
Training time: 1.0
		
Iteration: 14
Labeled set size: 360
Unlabelled set size: 140
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.6
Correctly Classified Instances: 70.4
Weighted Precision: 0.6626659292035398
Weighted AreaUnderROC: 0.5504761904761905
Root mean squared error: 0.5425608462731825
Relative absolute error: 59.42541436464096
Root relative squared error: 108.51216925463649
Weighted TruePositiveRate: 0.704
Weighted MatthewsCorrelation: 0.15703858804309678
Weighted FMeasure: 0.6465805184009672
Iteration time: 15.0
Weighted AreaUnderPRC: 0.6063788348082595
Mean absolute error: 0.2971270718232048
Coverage of cases: 70.4
Instances selection time: 14.0
Test time: 51.0
Accumulative iteration time: 243.0
Weighted Recall: 0.704
Weighted FalsePositiveRate: 0.603047619047619
Kappa statistic: 0.12529550827423164
Training time: 1.0
		
Iteration: 15
Labeled set size: 380
Unlabelled set size: 120
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.6
Correctly Classified Instances: 70.4
Weighted Precision: 0.6626659292035398
Weighted AreaUnderROC: 0.5504761904761905
Root mean squared error: 0.5426390279572847
Relative absolute error: 59.41361256544499
Root relative squared error: 108.52780559145694
Weighted TruePositiveRate: 0.704
Weighted MatthewsCorrelation: 0.15703858804309678
Weighted FMeasure: 0.6465805184009672
Iteration time: 14.0
Weighted AreaUnderPRC: 0.6063788348082595
Mean absolute error: 0.297068062827225
Coverage of cases: 70.4
Instances selection time: 13.0
Test time: 53.0
Accumulative iteration time: 257.0
Weighted Recall: 0.704
Weighted FalsePositiveRate: 0.603047619047619
Kappa statistic: 0.12529550827423164
Training time: 1.0
		
Iteration: 16
Labeled set size: 400
Unlabelled set size: 100
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.8
Correctly Classified Instances: 70.2
Weighted Precision: 0.6620661824051655
Weighted AreaUnderROC: 0.5585714285714286
Root mean squared error: 0.5445398065348095
Relative absolute error: 59.800995024875384
Root relative squared error: 108.9079613069619
Weighted TruePositiveRate: 0.702
Weighted MatthewsCorrelation: 0.1663987898486613
Weighted FMeasure: 0.6542659948342296
Iteration time: 13.0
Weighted AreaUnderPRC: 0.6104145970252508
Mean absolute error: 0.2990049751243769
Coverage of cases: 70.2
Instances selection time: 12.0
Test time: 56.0
Accumulative iteration time: 270.0
Weighted Recall: 0.702
Weighted FalsePositiveRate: 0.5848571428571429
Kappa statistic: 0.14170506912442374
Training time: 1.0
		
Iteration: 17
Labeled set size: 420
Unlabelled set size: 80
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.8
Correctly Classified Instances: 70.2
Weighted Precision: 0.6668088928812037
Weighted AreaUnderROC: 0.5719047619047619
Root mean squared error: 0.544603794563274
Relative absolute error: 59.791469194312754
Root relative squared error: 108.9207589126548
Weighted TruePositiveRate: 0.702
Weighted MatthewsCorrelation: 0.18663450204614337
Weighted FMeasure: 0.6653173352724921
Iteration time: 12.0
Weighted AreaUnderPRC: 0.6175148118443425
Mean absolute error: 0.29895734597156376
Coverage of cases: 70.2
Instances selection time: 11.0
Test time: 58.0
Accumulative iteration time: 282.0
Weighted Recall: 0.702
Weighted FalsePositiveRate: 0.5581904761904761
Kappa statistic: 0.16852678571428573
Training time: 1.0
		
Iteration: 18
Labeled set size: 440
Unlabelled set size: 60
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.8
Correctly Classified Instances: 70.2
Weighted Precision: 0.6728532764701058
Weighted AreaUnderROC: 0.5871428571428572
Root mean squared error: 0.544662007451155
Relative absolute error: 59.7828054298644
Root relative squared error: 108.932401490231
Weighted TruePositiveRate: 0.702
Weighted MatthewsCorrelation: 0.20879761235207148
Weighted FMeasure: 0.6759142066978595
Iteration time: 8.0
Weighted AreaUnderPRC: 0.6257552530140245
Mean absolute error: 0.298914027149322
Coverage of cases: 70.2
Instances selection time: 8.0
Test time: 61.0
Accumulative iteration time: 290.0
Weighted Recall: 0.702
Weighted FalsePositiveRate: 0.5277142857142857
Kappa statistic: 0.19719827586206878
Training time: 0.0
		
Iteration: 19
Labeled set size: 460
Unlabelled set size: 40
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.2
Correctly Classified Instances: 69.8
Weighted Precision: 0.6749005424954791
Weighted AreaUnderROC: 0.5976190476190476
Root mean squared error: 0.5483587564380289
Relative absolute error: 60.57142857142858
Root relative squared error: 109.67175128760577
Weighted TruePositiveRate: 0.698
Weighted MatthewsCorrelation: 0.2196600187522237
Weighted FMeasure: 0.6804737465455981
Iteration time: 6.0
Weighted AreaUnderPRC: 0.6311326100060277
Mean absolute error: 0.3028571428571429
Coverage of cases: 69.8
Instances selection time: 6.0
Test time: 62.0
Accumulative iteration time: 296.0
Weighted Recall: 0.698
Weighted FalsePositiveRate: 0.5027619047619047
Kappa statistic: 0.21354166666666657
Training time: 0.0
		
Iteration: 20
Labeled set size: 480
Unlabelled set size: 20
	
Mean region size: 50.0
Incorrectly Classified Instances: 31.2
Correctly Classified Instances: 68.8
Weighted Precision: 0.6701215724554087
Weighted AreaUnderROC: 0.5980952380952381
Root mean squared error: 0.5574134000223466
Relative absolute error: 62.556016597510386
Root relative squared error: 111.48268000446933
Weighted TruePositiveRate: 0.688
Weighted MatthewsCorrelation: 0.21173121412809928
Weighted FMeasure: 0.6761928064595059
Iteration time: 3.0
Weighted AreaUnderPRC: 0.6306143224775934
Mean absolute error: 0.31278008298755194
Coverage of cases: 68.8
Instances selection time: 3.0
Test time: 65.0
Accumulative iteration time: 299.0
Weighted Recall: 0.688
Weighted FalsePositiveRate: 0.49180952380952386
Kappa statistic: 0.20892494929006067
Training time: 0.0
		
Time end:Tue Oct 31 11.35.23 EET 2017