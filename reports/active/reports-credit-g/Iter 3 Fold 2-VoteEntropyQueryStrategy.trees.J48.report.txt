Tue Oct 31 11.35.43 EET 2017
Dataset: german_credit
Test set size: 500
Initial Labelled set size: 100
Initial Unlabelled set size: 400
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Tue Oct 31 11.35.43 EET 2017
		
Iteration: 1
Labeled set size: 100
Unlabelled set size: 400
	
Mean region size: 100.0
Incorrectly Classified Instances: 30.8
Correctly Classified Instances: 69.2
Weighted Precision: 0.632695374800638
Weighted AreaUnderROC: 0.5266666666666666
Root mean squared error: 0.47361116731680036
Relative absolute error: 80.14782608695708
Root relative squared error: 94.72223346336007
Weighted TruePositiveRate: 0.692
Weighted MatthewsCorrelation: 0.0922285951821433
Weighted FMeasure: 0.6214966984592811
Iteration time: 3.0
Weighted AreaUnderPRC: 0.5929862838915472
Mean absolute error: 0.40073913043478543
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 3.0
Weighted Recall: 0.692
Weighted FalsePositiveRate: 0.6386666666666667
Kappa statistic: 0.06779661016949144
Training time: 2.0
		
Iteration: 2
Labeled set size: 120
Unlabelled set size: 380
	
Mean region size: 100.0
Incorrectly Classified Instances: 30.8
Correctly Classified Instances: 69.2
Weighted Precision: 0.632695374800638
Weighted AreaUnderROC: 0.5266666666666666
Root mean squared error: 0.47941556682515535
Relative absolute error: 76.79999999999954
Root relative squared error: 95.88311336503106
Weighted TruePositiveRate: 0.692
Weighted MatthewsCorrelation: 0.0922285951821433
Weighted FMeasure: 0.6214966984592811
Iteration time: 3.0
Weighted AreaUnderPRC: 0.5929862838915472
Mean absolute error: 0.38399999999999773
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 6.0
Weighted Recall: 0.692
Weighted FalsePositiveRate: 0.6386666666666667
Kappa statistic: 0.06779661016949144
Training time: 2.0
		
Iteration: 3
Labeled set size: 140
Unlabelled set size: 360
	
Mean region size: 100.0
Incorrectly Classified Instances: 30.8
Correctly Classified Instances: 69.2
Weighted Precision: 0.632695374800638
Weighted AreaUnderROC: 0.5266666666666666
Root mean squared error: 0.4854765699804676
Relative absolute error: 74.46666666666678
Root relative squared error: 97.09531399609352
Weighted TruePositiveRate: 0.692
Weighted MatthewsCorrelation: 0.0922285951821433
Weighted FMeasure: 0.6214966984592811
Iteration time: 2.0
Weighted AreaUnderPRC: 0.5929862838915472
Mean absolute error: 0.3723333333333339
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 8.0
Weighted Recall: 0.692
Weighted FalsePositiveRate: 0.6386666666666667
Kappa statistic: 0.06779661016949144
Training time: 2.0
		
Iteration: 4
Labeled set size: 160
Unlabelled set size: 340
	
Mean region size: 96.2
Incorrectly Classified Instances: 29.4
Correctly Classified Instances: 70.6
Weighted Precision: 0.6660086663456909
Weighted AreaUnderROC: 0.5455333333333333
Root mean squared error: 0.4859052817263118
Relative absolute error: 70.22251655629177
Root relative squared error: 97.18105634526236
Weighted TruePositiveRate: 0.706
Weighted MatthewsCorrelation: 0.1393521936064996
Weighted FMeasure: 0.6307125655191953
Iteration time: 3.0
Weighted AreaUnderPRC: 0.6045812092452739
Mean absolute error: 0.3511125827814588
Coverage of cases: 97.2
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 11.0
Weighted Recall: 0.706
Weighted FalsePositiveRate: 0.6326666666666667
Kappa statistic: 0.09482758620689637
Training time: 2.0
		
Iteration: 5
Labeled set size: 180
Unlabelled set size: 320
	
Mean region size: 96.2
Incorrectly Classified Instances: 29.4
Correctly Classified Instances: 70.6
Weighted Precision: 0.6660086663456909
Weighted AreaUnderROC: 0.5455333333333333
Root mean squared error: 0.490764401054897
Relative absolute error: 68.88654970760197
Root relative squared error: 98.1528802109794
Weighted TruePositiveRate: 0.706
Weighted MatthewsCorrelation: 0.1393521936064996
Weighted FMeasure: 0.6307125655191953
Iteration time: 3.0
Weighted AreaUnderPRC: 0.6045812092452739
Mean absolute error: 0.34443274853800987
Coverage of cases: 97.2
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 14.0
Weighted Recall: 0.706
Weighted FalsePositiveRate: 0.6326666666666667
Kappa statistic: 0.09482758620689637
Training time: 3.0
		
Iteration: 6
Labeled set size: 200
Unlabelled set size: 300
	
Mean region size: 96.5
Incorrectly Classified Instances: 28.6
Correctly Classified Instances: 71.4
Weighted Precision: 0.6945263157894738
Weighted AreaUnderROC: 0.5527619047619048
Root mean squared error: 0.48422935257190663
Relative absolute error: 66.18047138047123
Root relative squared error: 96.84587051438133
Weighted TruePositiveRate: 0.714
Weighted MatthewsCorrelation: 0.17021289926939803
Weighted FMeasure: 0.6335238095238095
Iteration time: 3.0
Weighted AreaUnderPRC: 0.6109174049104215
Mean absolute error: 0.33090235690235614
Coverage of cases: 98.4
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 17.0
Weighted Recall: 0.714
Weighted FalsePositiveRate: 0.6330476190476191
Kappa statistic: 0.10624999999999976
Training time: 3.0
		
Iteration: 7
Labeled set size: 220
Unlabelled set size: 280
	
Mean region size: 96.9
Incorrectly Classified Instances: 28.6
Correctly Classified Instances: 71.4
Weighted Precision: 0.6945263157894738
Weighted AreaUnderROC: 0.5527047619047619
Root mean squared error: 0.4871668773671722
Relative absolute error: 65.5923076923079
Root relative squared error: 97.43337547343444
Weighted TruePositiveRate: 0.714
Weighted MatthewsCorrelation: 0.17021289926939803
Weighted FMeasure: 0.6335238095238095
Iteration time: 3.0
Weighted AreaUnderPRC: 0.6105811544389013
Mean absolute error: 0.32796153846153947
Coverage of cases: 98.6
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 20.0
Weighted Recall: 0.714
Weighted FalsePositiveRate: 0.6330476190476191
Kappa statistic: 0.10624999999999976
Training time: 3.0
		
Iteration: 8
Labeled set size: 240
Unlabelled set size: 260
	
Mean region size: 96.9
Incorrectly Classified Instances: 28.6
Correctly Classified Instances: 71.4
Weighted Precision: 0.6945263157894738
Weighted AreaUnderROC: 0.5527047619047619
Root mean squared error: 0.4904421579665278
Relative absolute error: 64.86491228070183
Root relative squared error: 98.08843159330556
Weighted TruePositiveRate: 0.714
Weighted MatthewsCorrelation: 0.17021289926939803
Weighted FMeasure: 0.6335238095238095
Iteration time: 4.0
Weighted AreaUnderPRC: 0.6105811544389013
Mean absolute error: 0.3243245614035092
Coverage of cases: 98.6
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 24.0
Weighted Recall: 0.714
Weighted FalsePositiveRate: 0.6330476190476191
Kappa statistic: 0.10624999999999976
Training time: 3.0
		
Iteration: 9
Labeled set size: 260
Unlabelled set size: 240
	
Mean region size: 98.5
Incorrectly Classified Instances: 30.6
Correctly Classified Instances: 69.4
Weighted Precision: 0.6162830349531118
Weighted AreaUnderROC: 0.5321714285714286
Root mean squared error: 0.4978046739593242
Relative absolute error: 66.93278688524568
Root relative squared error: 99.56093479186484
Weighted TruePositiveRate: 0.694
Weighted MatthewsCorrelation: 0.04575577624313952
Weighted FMeasure: 0.5965782720273738
Iteration time: 3.0
Weighted AreaUnderPRC: 0.5954801225169128
Mean absolute error: 0.3346639344262284
Coverage of cases: 99.2
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 27.0
Weighted Recall: 0.694
Weighted FalsePositiveRate: 0.6759047619047619
Kappa statistic: 0.02423469387755083
Training time: 3.0
		
Iteration: 10
Labeled set size: 280
Unlabelled set size: 220
	
Mean region size: 77.0
Incorrectly Classified Instances: 29.4
Correctly Classified Instances: 70.6
Weighted Precision: 0.667578947368421
Weighted AreaUnderROC: 0.687495238095238
Root mean squared error: 0.4886331522819455
Relative absolute error: 62.031512285355305
Root relative squared error: 97.7266304563891
Weighted TruePositiveRate: 0.706
Weighted MatthewsCorrelation: 0.13016280532365732
Weighted FMeasure: 0.6232727272727273
Iteration time: 4.0
Weighted AreaUnderPRC: 0.6931527173994136
Mean absolute error: 0.31015756142677653
Coverage of cases: 91.6
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 31.0
Weighted Recall: 0.706
Weighted FalsePositiveRate: 0.644095238095238
Kappa statistic: 0.08124999999999974
Training time: 3.0
		
Iteration: 11
Labeled set size: 300
Unlabelled set size: 200
	
Mean region size: 74.1
Incorrectly Classified Instances: 29.4
Correctly Classified Instances: 70.6
Weighted Precision: 0.667578947368421
Weighted AreaUnderROC: 0.687495238095238
Root mean squared error: 0.49276922089021585
Relative absolute error: 61.80492151970412
Root relative squared error: 98.55384417804316
Weighted TruePositiveRate: 0.706
Weighted MatthewsCorrelation: 0.13016280532365732
Weighted FMeasure: 0.6232727272727273
Iteration time: 4.0
Weighted AreaUnderPRC: 0.6931527173994136
Mean absolute error: 0.30902460759852063
Coverage of cases: 90.2
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 35.0
Weighted Recall: 0.706
Weighted FalsePositiveRate: 0.644095238095238
Kappa statistic: 0.08124999999999974
Training time: 4.0
		
Iteration: 12
Labeled set size: 320
Unlabelled set size: 180
	
Mean region size: 100.0
Incorrectly Classified Instances: 30.0
Correctly Classified Instances: 70.0
Weighted Precision: 0.48999999999999994
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.5038232732069843
Relative absolute error: 67.25000000000001
Root relative squared error: 100.76465464139686
Weighted TruePositiveRate: 0.7
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.5764705882352942
Iteration time: 4.0
Weighted AreaUnderPRC: 0.58
Mean absolute error: 0.33625000000000005
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 39.0
Weighted Recall: 0.7
Weighted FalsePositiveRate: 0.7
Kappa statistic: 0.0
Training time: 4.0
		
Iteration: 13
Labeled set size: 340
Unlabelled set size: 160
	
Mean region size: 100.0
Incorrectly Classified Instances: 30.0
Correctly Classified Instances: 70.0
Weighted Precision: 0.48999999999999994
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.5060618696532613
Relative absolute error: 66.82352941176481
Root relative squared error: 101.21237393065226
Weighted TruePositiveRate: 0.7
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.5764705882352942
Iteration time: 5.0
Weighted AreaUnderPRC: 0.58
Mean absolute error: 0.3341176470588241
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 44.0
Weighted Recall: 0.7
Weighted FalsePositiveRate: 0.7
Kappa statistic: 0.0
Training time: 4.0
		
Iteration: 14
Labeled set size: 360
Unlabelled set size: 140
	
Mean region size: 100.0
Incorrectly Classified Instances: 30.0
Correctly Classified Instances: 70.0
Weighted Precision: 0.48999999999999994
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.5080904094721048
Relative absolute error: 66.44444444444476
Root relative squared error: 101.61808189442097
Weighted TruePositiveRate: 0.7
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.5764705882352942
Iteration time: 4.0
Weighted AreaUnderPRC: 0.58
Mean absolute error: 0.3322222222222238
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 48.0
Weighted Recall: 0.7
Weighted FalsePositiveRate: 0.7
Kappa statistic: 0.0
Training time: 4.0
		
Iteration: 15
Labeled set size: 380
Unlabelled set size: 120
	
Mean region size: 100.0
Incorrectly Classified Instances: 30.0
Correctly Classified Instances: 70.0
Weighted Precision: 0.48999999999999994
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.5087870536167145
Relative absolute error: 66.31578947368425
Root relative squared error: 101.7574107233429
Weighted TruePositiveRate: 0.7
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.5764705882352942
Iteration time: 5.0
Weighted AreaUnderPRC: 0.58
Mean absolute error: 0.33157894736842125
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 53.0
Weighted Recall: 0.7
Weighted FalsePositiveRate: 0.7
Kappa statistic: 0.0
Training time: 4.0
		
Iteration: 16
Labeled set size: 400
Unlabelled set size: 100
	
Mean region size: 100.0
Incorrectly Classified Instances: 30.0
Correctly Classified Instances: 70.0
Weighted Precision: 0.48999999999999994
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.4905354217587146
Relative absolute error: 70.0
Root relative squared error: 98.10708435174293
Weighted TruePositiveRate: 0.7
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.5764705882352942
Iteration time: 5.0
Weighted AreaUnderPRC: 0.58
Mean absolute error: 0.35
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 58.0
Weighted Recall: 0.7
Weighted FalsePositiveRate: 0.7
Kappa statistic: 0.0
Training time: 5.0
		
Iteration: 17
Labeled set size: 420
Unlabelled set size: 80
	
Mean region size: 97.3
Incorrectly Classified Instances: 29.0
Correctly Classified Instances: 71.0
Weighted Precision: 0.6752378728620629
Weighted AreaUnderROC: 0.5281238095238096
Root mean squared error: 0.48277403736987334
Relative absolute error: 67.89109826589572
Root relative squared error: 96.55480747397466
Weighted TruePositiveRate: 0.71
Weighted MatthewsCorrelation: 0.165057092118244
Weighted FMeasure: 0.6425334306819004
Iteration time: 7.0
Weighted AreaUnderPRC: 0.5992243042561681
Mean absolute error: 0.3394554913294786
Coverage of cases: 97.8
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 65.0
Weighted Recall: 0.71
Weighted FalsePositiveRate: 0.6157142857142857
Kappa statistic: 0.12014563106796107
Training time: 7.0
		
Iteration: 18
Labeled set size: 440
Unlabelled set size: 60
	
Mean region size: 98.5
Incorrectly Classified Instances: 28.8
Correctly Classified Instances: 71.2
Weighted Precision: 0.6917459109927729
Weighted AreaUnderROC: 0.5553333333333333
Root mean squared error: 0.4661391389562687
Relative absolute error: 70.96112994350278
Root relative squared error: 93.22782779125373
Weighted TruePositiveRate: 0.712
Weighted MatthewsCorrelation: 0.157469621516175
Weighted FMeasure: 0.627098078867543
Iteration time: 8.0
Weighted AreaUnderPRC: 0.6126680049461962
Mean absolute error: 0.3548056497175139
Coverage of cases: 99.8
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 73.0
Weighted Recall: 0.712
Weighted FalsePositiveRate: 0.6415238095238095
Kappa statistic: 0.09319899244332482
Training time: 8.0
		
Iteration: 19
Labeled set size: 460
Unlabelled set size: 40
	
Mean region size: 89.3
Incorrectly Classified Instances: 28.6
Correctly Classified Instances: 71.4
Weighted Precision: 0.6853814005231667
Weighted AreaUnderROC: 0.6375809523809524
Root mean squared error: 0.48044151486312664
Relative absolute error: 64.1643971734255
Root relative squared error: 96.08830297262533
Weighted TruePositiveRate: 0.714
Weighted MatthewsCorrelation: 0.2309367598021792
Weighted FMeasure: 0.6828323676504738
Iteration time: 10.0
Weighted AreaUnderPRC: 0.6626341005397995
Mean absolute error: 0.3208219858671275
Coverage of cases: 91.6
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 83.0
Weighted Recall: 0.714
Weighted FalsePositiveRate: 0.5301904761904762
Kappa statistic: 0.212555066079295
Training time: 10.0
		
Iteration: 20
Labeled set size: 480
Unlabelled set size: 20
	
Mean region size: 91.9
Incorrectly Classified Instances: 25.8
Correctly Classified Instances: 74.2
Weighted Precision: 0.7272215660390469
Weighted AreaUnderROC: 0.7116285714285714
Root mean squared error: 0.46262729472309616
Relative absolute error: 63.14316230833028
Root relative squared error: 92.52545894461923
Weighted TruePositiveRate: 0.742
Weighted MatthewsCorrelation: 0.34340120536664837
Weighted FMeasure: 0.7295319863748775
Iteration time: 10.0
Weighted AreaUnderPRC: 0.7212229346464504
Mean absolute error: 0.3157158115416514
Coverage of cases: 94.0
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 93.0
Weighted Recall: 0.742
Weighted FalsePositiveRate: 0.4305714285714286
Kappa statistic: 0.3364197530864198
Training time: 10.0
		
Time end:Tue Oct 31 11.35.44 EET 2017