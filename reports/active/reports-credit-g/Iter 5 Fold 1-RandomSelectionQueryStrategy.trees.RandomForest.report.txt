Tue Oct 31 11.34.46 EET 2017
Dataset: german_credit
Test set size: 500
Initial Labelled set size: 100
Initial Unlabelled set size: 400
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Tue Oct 31 11.34.46 EET 2017
		
Iteration: 1
Labeled set size: 100
Unlabelled set size: 400
	
Mean region size: 96.5
Incorrectly Classified Instances: 32.8
Correctly Classified Instances: 67.2
Weighted Precision: 0.6181818181818182
Weighted AreaUnderROC: 0.5921866666666666
Root mean squared error: 0.4707059822837723
Relative absolute error: 80.3407296351066
Root relative squared error: 94.14119645675446
Weighted TruePositiveRate: 0.672
Weighted MatthewsCorrelation: 0.07994019615852548
Weighted FMeasure: 0.6257936507936507
Iteration time: 5.0
Weighted AreaUnderPRC: 0.652605125779692
Mean absolute error: 0.401703648175533
Coverage of cases: 99.2
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 5.0
Weighted Recall: 0.672
Weighted FalsePositiveRate: 0.6129523809523809
Kappa statistic: 0.070294784580499
Training time: 4.0
		
Iteration: 2
Labeled set size: 120
Unlabelled set size: 380
	
Mean region size: 94.9
Incorrectly Classified Instances: 32.8
Correctly Classified Instances: 67.2
Weighted Precision: 0.6259061569016882
Weighted AreaUnderROC: 0.6079171428571428
Root mean squared error: 0.4660908140395601
Relative absolute error: 78.04908788872756
Root relative squared error: 93.21816280791202
Weighted TruePositiveRate: 0.672
Weighted MatthewsCorrelation: 0.09968148071616914
Weighted FMeasure: 0.6339804715190597
Iteration time: 5.0
Weighted AreaUnderPRC: 0.6637046869998264
Mean absolute error: 0.39024543944363776
Coverage of cases: 98.6
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 10.0
Weighted Recall: 0.672
Weighted FalsePositiveRate: 0.5939047619047619
Kappa statistic: 0.09090909090909105
Training time: 4.0
		
Iteration: 3
Labeled set size: 140
Unlabelled set size: 360
	
Mean region size: 93.9
Incorrectly Classified Instances: 30.2
Correctly Classified Instances: 69.8
Weighted Precision: 0.652181317961483
Weighted AreaUnderROC: 0.6909114285714286
Root mean squared error: 0.4429890004949345
Relative absolute error: 71.64648541242515
Root relative squared error: 88.5978000989869
Weighted TruePositiveRate: 0.698
Weighted MatthewsCorrelation: 0.13987942176459978
Weighted FMeasure: 0.6423365027179496
Iteration time: 6.0
Weighted AreaUnderPRC: 0.7124598870654565
Mean absolute error: 0.35823242706212577
Coverage of cases: 99.0
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 16.0
Weighted Recall: 0.698
Weighted FalsePositiveRate: 0.6056190476190477
Kappa statistic: 0.1138497652582158
Training time: 5.0
		
Iteration: 4
Labeled set size: 160
Unlabelled set size: 340
	
Mean region size: 95.7
Incorrectly Classified Instances: 33.2
Correctly Classified Instances: 66.8
Weighted Precision: 0.6233333333333333
Weighted AreaUnderROC: 0.62276
Root mean squared error: 0.4618476827830381
Relative absolute error: 78.46085369072881
Root relative squared error: 92.36953655660763
Weighted TruePositiveRate: 0.668
Weighted MatthewsCorrelation: 0.09523809523809523
Weighted FMeasure: 0.6325691699604744
Iteration time: 6.0
Weighted AreaUnderPRC: 0.6720011925090441
Mean absolute error: 0.392304268453644
Coverage of cases: 99.2
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 22.0
Weighted Recall: 0.668
Weighted FalsePositiveRate: 0.5918095238095238
Kappa statistic: 0.08791208791208799
Training time: 5.0
		
Iteration: 5
Labeled set size: 180
Unlabelled set size: 320
	
Mean region size: 93.4
Incorrectly Classified Instances: 30.2
Correctly Classified Instances: 69.8
Weighted Precision: 0.6703963612735543
Weighted AreaUnderROC: 0.689432380952381
Root mean squared error: 0.4446445929930408
Relative absolute error: 73.36765863234291
Root relative squared error: 88.92891859860816
Weighted TruePositiveRate: 0.698
Weighted MatthewsCorrelation: 0.20581298277672308
Weighted FMeasure: 0.6751020408163265
Iteration time: 7.0
Weighted AreaUnderPRC: 0.7166770550241349
Mean absolute error: 0.3668382931617145
Coverage of cases: 98.8
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 29.0
Weighted Recall: 0.698
Weighted FalsePositiveRate: 0.5218095238095237
Kappa statistic: 0.19680851063829774
Training time: 6.0
		
Iteration: 6
Labeled set size: 200
Unlabelled set size: 300
	
Mean region size: 96.3
Incorrectly Classified Instances: 29.4
Correctly Classified Instances: 70.6
Weighted Precision: 0.682525620983151
Weighted AreaUnderROC: 0.684687619047619
Root mean squared error: 0.44097506054629076
Relative absolute error: 74.75294958396256
Root relative squared error: 88.19501210925816
Weighted TruePositiveRate: 0.706
Weighted MatthewsCorrelation: 0.23588642679454397
Weighted FMeasure: 0.6869196112745282
Iteration time: 8.0
Weighted AreaUnderPRC: 0.7235843299768114
Mean absolute error: 0.3737647479198128
Coverage of cases: 99.4
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 37.0
Weighted Recall: 0.706
Weighted FalsePositiveRate: 0.4993333333333333
Kappa statistic: 0.22794117647058817
Training time: 7.0
		
Iteration: 7
Labeled set size: 220
Unlabelled set size: 280
	
Mean region size: 96.2
Incorrectly Classified Instances: 29.6
Correctly Classified Instances: 70.4
Weighted Precision: 0.6773294203961849
Weighted AreaUnderROC: 0.6883209523809524
Root mean squared error: 0.44075315208788063
Relative absolute error: 75.34080203739312
Root relative squared error: 88.15063041757612
Weighted TruePositiveRate: 0.704
Weighted MatthewsCorrelation: 0.22117121274444587
Weighted FMeasure: 0.6809957498482088
Iteration time: 9.0
Weighted AreaUnderPRC: 0.7263625476340022
Mean absolute error: 0.3767040101869656
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 46.0
Weighted Recall: 0.704
Weighted FalsePositiveRate: 0.5154285714285715
Kappa statistic: 0.21108742004264378
Training time: 8.0
		
Iteration: 8
Labeled set size: 240
Unlabelled set size: 260
	
Mean region size: 95.3
Incorrectly Classified Instances: 28.4
Correctly Classified Instances: 71.6
Weighted Precision: 0.6926980198019802
Weighted AreaUnderROC: 0.7097819047619048
Root mean squared error: 0.43460353388369083
Relative absolute error: 73.71679457165722
Root relative squared error: 86.92070677673817
Weighted TruePositiveRate: 0.716
Weighted MatthewsCorrelation: 0.2570703793644186
Weighted FMeasure: 0.6949990295658924
Iteration time: 10.0
Weighted AreaUnderPRC: 0.7323449383691861
Mean absolute error: 0.3685839728582861
Coverage of cases: 99.0
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 56.0
Weighted Recall: 0.716
Weighted FalsePositiveRate: 0.495047619047619
Kappa statistic: 0.2462845010615711
Training time: 10.0
		
Iteration: 9
Labeled set size: 260
Unlabelled set size: 240
	
Mean region size: 95.9
Incorrectly Classified Instances: 30.8
Correctly Classified Instances: 69.2
Weighted Precision: 0.6655
Weighted AreaUnderROC: 0.6782247619047619
Root mean squared error: 0.444612695802322
Relative absolute error: 74.32128246553677
Root relative squared error: 88.9225391604644
Weighted TruePositiveRate: 0.692
Weighted MatthewsCorrelation: 0.19639610121239312
Weighted FMeasure: 0.6714666666666667
Iteration time: 11.0
Weighted AreaUnderPRC: 0.7066173939766544
Mean absolute error: 0.3716064123276838
Coverage of cases: 98.8
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 67.0
Weighted Recall: 0.692
Weighted FalsePositiveRate: 0.5205714285714287
Kappa statistic: 0.18947368421052618
Training time: 10.0
		
Iteration: 10
Labeled set size: 280
Unlabelled set size: 220
	
Mean region size: 94.9
Incorrectly Classified Instances: 26.8
Correctly Classified Instances: 73.2
Weighted Precision: 0.7169075538587402
Weighted AreaUnderROC: 0.7298638095238096
Root mean squared error: 0.4325710000341301
Relative absolute error: 72.27814733995706
Root relative squared error: 86.51420000682603
Weighted TruePositiveRate: 0.732
Weighted MatthewsCorrelation: 0.32040183236327013
Weighted FMeasure: 0.7202816205533598
Iteration time: 12.0
Weighted AreaUnderPRC: 0.7497911107784425
Mean absolute error: 0.3613907366997853
Coverage of cases: 99.6
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 79.0
Weighted Recall: 0.732
Weighted FalsePositiveRate: 0.43866666666666665
Kappa statistic: 0.31492842535787313
Training time: 11.0
		
Iteration: 11
Labeled set size: 300
Unlabelled set size: 200
	
Mean region size: 94.4
Incorrectly Classified Instances: 30.8
Correctly Classified Instances: 69.2
Weighted Precision: 0.6744964060697489
Weighted AreaUnderROC: 0.6781028571428571
Root mean squared error: 0.45056272767008965
Relative absolute error: 75.24395322027716
Root relative squared error: 90.11254553401793
Weighted TruePositiveRate: 0.692
Weighted MatthewsCorrelation: 0.2220094284061624
Weighted FMeasure: 0.6803441807356659
Iteration time: 12.0
Weighted AreaUnderPRC: 0.7092368967654112
Mean absolute error: 0.3762197661013858
Coverage of cases: 99.0
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 91.0
Weighted Recall: 0.692
Weighted FalsePositiveRate: 0.48628571428571427
Kappa statistic: 0.21906693711967526
Training time: 12.0
		
Iteration: 12
Labeled set size: 320
Unlabelled set size: 180
	
Mean region size: 95.7
Incorrectly Classified Instances: 28.2
Correctly Classified Instances: 71.8
Weighted Precision: 0.7050074399948244
Weighted AreaUnderROC: 0.7122247619047619
Root mean squared error: 0.4376633900577141
Relative absolute error: 73.4452049805848
Root relative squared error: 87.53267801154281
Weighted TruePositiveRate: 0.718
Weighted MatthewsCorrelation: 0.2948900443618854
Weighted FMeasure: 0.7092915337757154
Iteration time: 13.0
Weighted AreaUnderPRC: 0.7300473650671491
Mean absolute error: 0.36722602490292405
Coverage of cases: 99.2
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 104.0
Weighted Recall: 0.718
Weighted FalsePositiveRate: 0.4408571428571429
Kappa statistic: 0.2921686746987951
Training time: 13.0
		
Iteration: 13
Labeled set size: 340
Unlabelled set size: 160
	
Mean region size: 94.4
Incorrectly Classified Instances: 31.4
Correctly Classified Instances: 68.6
Weighted Precision: 0.661012658227848
Weighted AreaUnderROC: 0.6741019047619047
Root mean squared error: 0.4490321543692672
Relative absolute error: 74.60528434240182
Root relative squared error: 89.80643087385344
Weighted TruePositiveRate: 0.686
Weighted MatthewsCorrelation: 0.18751465015433733
Weighted FMeasure: 0.6677773391235688
Iteration time: 14.0
Weighted AreaUnderPRC: 0.7143175890304715
Mean absolute error: 0.37302642171200906
Coverage of cases: 99.0
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 118.0
Weighted Recall: 0.686
Weighted FalsePositiveRate: 0.5193333333333332
Kappa statistic: 0.18229166666666682
Training time: 13.0
		
Iteration: 14
Labeled set size: 360
Unlabelled set size: 140
	
Mean region size: 94.0
Incorrectly Classified Instances: 29.6
Correctly Classified Instances: 70.4
Weighted Precision: 0.6867007902298851
Weighted AreaUnderROC: 0.7055838095238096
Root mean squared error: 0.4413301313201263
Relative absolute error: 73.58417653005264
Root relative squared error: 88.26602626402526
Weighted TruePositiveRate: 0.704
Weighted MatthewsCorrelation: 0.25021371543850535
Weighted FMeasure: 0.6919382925979801
Iteration time: 15.0
Weighted AreaUnderPRC: 0.7319355584622809
Mean absolute error: 0.36792088265026324
Coverage of cases: 99.2
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 133.0
Weighted Recall: 0.704
Weighted FalsePositiveRate: 0.4735238095238095
Kappa statistic: 0.24643584521384926
Training time: 14.0
		
Iteration: 15
Labeled set size: 380
Unlabelled set size: 120
	
Mean region size: 92.9
Incorrectly Classified Instances: 27.4
Correctly Classified Instances: 72.6
Weighted Precision: 0.710592885375494
Weighted AreaUnderROC: 0.7322733333333333
Root mean squared error: 0.42964922287810386
Relative absolute error: 69.40808194342351
Root relative squared error: 85.92984457562078
Weighted TruePositiveRate: 0.726
Weighted MatthewsCorrelation: 0.30593776078426455
Weighted FMeasure: 0.714429469901168
Iteration time: 15.0
Weighted AreaUnderPRC: 0.752927775208957
Mean absolute error: 0.3470404097171175
Coverage of cases: 98.4
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 148.0
Weighted Recall: 0.726
Weighted FalsePositiveRate: 0.4450476190476191
Kappa statistic: 0.30102040816326525
Training time: 14.0
		
Iteration: 16
Labeled set size: 400
Unlabelled set size: 100
	
Mean region size: 92.6
Incorrectly Classified Instances: 28.2
Correctly Classified Instances: 71.8
Weighted Precision: 0.6994697200778995
Weighted AreaUnderROC: 0.7194628571428573
Root mean squared error: 0.43469255466104095
Relative absolute error: 71.00224431324233
Root relative squared error: 86.93851093220819
Weighted TruePositiveRate: 0.718
Weighted MatthewsCorrelation: 0.2779997935851596
Weighted FMeasure: 0.703481156112735
Iteration time: 15.0
Weighted AreaUnderPRC: 0.7467065809508724
Mean absolute error: 0.3550112215662116
Coverage of cases: 98.8
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 163.0
Weighted Recall: 0.718
Weighted FalsePositiveRate: 0.4675238095238095
Kappa statistic: 0.27169421487603296
Training time: 15.0
		
Iteration: 17
Labeled set size: 420
Unlabelled set size: 80
	
Mean region size: 93.1
Incorrectly Classified Instances: 28.2
Correctly Classified Instances: 71.8
Weighted Precision: 0.6987491379515349
Weighted AreaUnderROC: 0.7433942857142857
Root mean squared error: 0.42222244212797483
Relative absolute error: 69.08363443549649
Root relative squared error: 84.44448842559497
Weighted TruePositiveRate: 0.718
Weighted MatthewsCorrelation: 0.2756144306600877
Weighted FMeasure: 0.7025687218186866
Iteration time: 16.0
Weighted AreaUnderPRC: 0.7677266133883099
Mean absolute error: 0.34541817217748244
Coverage of cases: 99.2
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 179.0
Weighted Recall: 0.718
Weighted FalsePositiveRate: 0.4713333333333333
Kappa statistic: 0.2686721991701245
Training time: 16.0
		
Iteration: 18
Labeled set size: 440
Unlabelled set size: 60
	
Mean region size: 94.3
Incorrectly Classified Instances: 26.0
Correctly Classified Instances: 74.0
Weighted Precision: 0.7215700660308143
Weighted AreaUnderROC: 0.7217152380952382
Root mean squared error: 0.4298243007801192
Relative absolute error: 70.453961350202
Root relative squared error: 85.96486015602383
Weighted TruePositiveRate: 0.74
Weighted MatthewsCorrelation: 0.32170358217373946
Weighted FMeasure: 0.7197935640558591
Iteration time: 17.0
Weighted AreaUnderPRC: 0.7452694116510227
Mean absolute error: 0.35226980675100994
Coverage of cases: 99.0
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 196.0
Weighted Recall: 0.74
Weighted FalsePositiveRate: 0.46571428571428575
Kappa statistic: 0.3070362473347547
Training time: 17.0
		
Iteration: 19
Labeled set size: 460
Unlabelled set size: 40
	
Mean region size: 92.9
Incorrectly Classified Instances: 30.2
Correctly Classified Instances: 69.8
Weighted Precision: 0.6721756215521802
Weighted AreaUnderROC: 0.6972285714285715
Root mean squared error: 0.4404270425198434
Relative absolute error: 71.8045611348763
Root relative squared error: 88.08540850396868
Weighted TruePositiveRate: 0.698
Weighted MatthewsCorrelation: 0.21137707213745718
Weighted FMeasure: 0.6773266167198755
Iteration time: 19.0
Weighted AreaUnderPRC: 0.7344237409402169
Mean absolute error: 0.3590228056743815
Coverage of cases: 98.8
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 215.0
Weighted Recall: 0.698
Weighted FalsePositiveRate: 0.5141904761904762
Kappa statistic: 0.2035864978902952
Training time: 18.0
		
Iteration: 20
Labeled set size: 480
Unlabelled set size: 20
	
Mean region size: 94.0
Incorrectly Classified Instances: 27.4
Correctly Classified Instances: 72.6
Weighted Precision: 0.7079189555539701
Weighted AreaUnderROC: 0.7092542857142856
Root mean squared error: 0.43518641338860214
Relative absolute error: 70.6657771667599
Root relative squared error: 87.03728267772043
Weighted TruePositiveRate: 0.726
Weighted MatthewsCorrelation: 0.29689739827862727
Weighted FMeasure: 0.7110064885756032
Iteration time: 20.0
Weighted AreaUnderPRC: 0.7309539193641023
Mean absolute error: 0.35332888583379946
Coverage of cases: 98.6
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 235.0
Weighted Recall: 0.726
Weighted FalsePositiveRate: 0.4602857142857143
Kappa statistic: 0.2894190871369295
Training time: 19.0
		
Time end:Tue Oct 31 11.34.47 EET 2017