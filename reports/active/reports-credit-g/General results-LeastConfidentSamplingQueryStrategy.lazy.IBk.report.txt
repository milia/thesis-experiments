Tue Oct 31 11.31.44 EET 2017
Dataset: german_credit
Test set size: 500
Initial Labelled set size: 100
Initial Unlabelled set size: 400
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Tue Oct 31 11.31.44 EET 2017
		
Iteration: 1
Labeled set size: 100
Unlabelled set size: 400
	
Mean region size: 50.0
Incorrectly Classified Instances: 35.160000000000004
Correctly Classified Instances: 64.84
Weighted Precision: 0.6368071918337918
Weighted AreaUnderROC: 0.5627619047619048
Root mean squared error: 0.586851867625495
Relative absolute error: 70.90196078431381
Root relative squared error: 117.37037352509901
Weighted TruePositiveRate: 0.6484000000000002
Weighted MatthewsCorrelation: 0.13281494406565467
Weighted FMeasure: 0.6391455978721192
Iteration time: 17.8
Weighted AreaUnderPRC: 0.6101939190623628
Mean absolute error: 0.3545098039215691
Coverage of cases: 64.84
Instances selection time: 17.8
Test time: 20.9
Accumulative iteration time: 17.8
Weighted Recall: 0.6484000000000002
Weighted FalsePositiveRate: 0.5228761904761905
Kappa statistic: 0.13049785602538322
Training time: 0.0
		
Iteration: 2
Labeled set size: 120
Unlabelled set size: 380
	
Mean region size: 50.0
Incorrectly Classified Instances: 33.940000000000005
Correctly Classified Instances: 66.05999999999997
Weighted Precision: 0.6370114677508194
Weighted AreaUnderROC: 0.5587142857142857
Root mean squared error: 0.5775660497842428
Relative absolute error: 68.40655737704934
Root relative squared error: 115.5132099568485
Weighted TruePositiveRate: 0.6606
Weighted MatthewsCorrelation: 0.13114077490751738
Weighted FMeasure: 0.6426775929596203
Iteration time: 17.6
Weighted AreaUnderPRC: 0.6084314509539439
Mean absolute error: 0.34203278688524674
Coverage of cases: 66.05999999999997
Instances selection time: 17.6
Test time: 22.9
Accumulative iteration time: 35.4
Weighted Recall: 0.6606
Weighted FalsePositiveRate: 0.5431714285714285
Kappa statistic: 0.12685979685138407
Training time: 0.0
		
Iteration: 3
Labeled set size: 140
Unlabelled set size: 360
	
Mean region size: 50.0
Incorrectly Classified Instances: 32.56
Correctly Classified Instances: 67.44
Weighted Precision: 0.644007734201247
Weighted AreaUnderROC: 0.5619047619047619
Root mean squared error: 0.5664217185607942
Relative absolute error: 65.61126760563397
Root relative squared error: 113.28434371215883
Weighted TruePositiveRate: 0.6744000000000001
Weighted MatthewsCorrelation: 0.1444154599555601
Weighted FMeasure: 0.6494624982937636
Iteration time: 18.4
Weighted AreaUnderPRC: 0.610642721816285
Mean absolute error: 0.32805633802816986
Coverage of cases: 67.44
Instances selection time: 18.3
Test time: 26.6
Accumulative iteration time: 53.8
Weighted Recall: 0.6744000000000001
Weighted FalsePositiveRate: 0.5505904761904762
Kappa statistic: 0.137169868776199
Training time: 0.1
		
Iteration: 4
Labeled set size: 160
Unlabelled set size: 340
	
Mean region size: 50.0
Incorrectly Classified Instances: 31.459999999999997
Correctly Classified Instances: 68.53999999999999
Weighted Precision: 0.651108973563279
Weighted AreaUnderROC: 0.5629047619047619
Root mean squared error: 0.5573170917615163
Relative absolute error: 63.377777777777645
Root relative squared error: 111.46341835230328
Weighted TruePositiveRate: 0.6854
Weighted MatthewsCorrelation: 0.15536288172750812
Weighted FMeasure: 0.6534139868908577
Iteration time: 22.1
Weighted AreaUnderPRC: 0.6116344549985433
Mean absolute error: 0.31688888888888816
Coverage of cases: 68.53999999999999
Instances selection time: 22.1
Test time: 36.9
Accumulative iteration time: 75.9
Weighted Recall: 0.6854
Weighted FalsePositiveRate: 0.5595904761904762
Kappa statistic: 0.1432611853725894
Training time: 0.0
		
Iteration: 5
Labeled set size: 180
Unlabelled set size: 320
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.940000000000005
Correctly Classified Instances: 69.05999999999999
Weighted Precision: 0.6541343505431236
Weighted AreaUnderROC: 0.5626190476190476
Root mean squared error: 0.5530612130254151
Relative absolute error: 62.298901098901
Root relative squared error: 110.61224260508303
Weighted TruePositiveRate: 0.6906
Weighted MatthewsCorrelation: 0.15942987474439868
Weighted FMeasure: 0.6544834157687653
Iteration time: 25.0
Weighted AreaUnderPRC: 0.6118793626845105
Mean absolute error: 0.311494505494505
Coverage of cases: 69.05999999999999
Instances selection time: 25.0
Test time: 37.5
Accumulative iteration time: 100.9
Weighted Recall: 0.6906
Weighted FalsePositiveRate: 0.5653619047619047
Kappa statistic: 0.14465156970529947
Training time: 0.0
		
Iteration: 6
Labeled set size: 200
Unlabelled set size: 300
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.7
Correctly Classified Instances: 69.3
Weighted Precision: 0.6552386081738464
Weighted AreaUnderROC: 0.5593809523809524
Root mean squared error: 0.5511794639587883
Relative absolute error: 61.7821782178219
Root relative squared error: 110.23589279175765
Weighted TruePositiveRate: 0.6929999999999998
Weighted MatthewsCorrelation: 0.15765870104610222
Weighted FMeasure: 0.652722651417995
Iteration time: 30.5
Weighted AreaUnderPRC: 0.6104028456829061
Mean absolute error: 0.30891089108910946
Coverage of cases: 69.3
Instances selection time: 30.5
Test time: 45.7
Accumulative iteration time: 131.4
Weighted Recall: 0.6929999999999998
Weighted FalsePositiveRate: 0.5742380952380952
Kappa statistic: 0.13951101243379957
Training time: 0.0
		
Iteration: 7
Labeled set size: 220
Unlabelled set size: 280
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.560000000000002
Correctly Classified Instances: 69.44
Weighted Precision: 0.6549934358281431
Weighted AreaUnderROC: 0.5559999999999998
Root mean squared error: 0.5501947914479406
Relative absolute error: 61.47027027027015
Root relative squared error: 110.0389582895881
Weighted TruePositiveRate: 0.6944
Weighted MatthewsCorrelation: 0.1537225673917968
Weighted FMeasure: 0.6501713810756576
Iteration time: 25.0
Weighted AreaUnderPRC: 0.6086889529103041
Mean absolute error: 0.3073513513513507
Coverage of cases: 69.44
Instances selection time: 25.0
Test time: 38.5
Accumulative iteration time: 156.4
Weighted Recall: 0.6944
Weighted FalsePositiveRate: 0.5824
Kappa statistic: 0.13311835103442396
Training time: 0.0
		
Iteration: 8
Labeled set size: 240
Unlabelled set size: 260
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.420000000000005
Correctly Classified Instances: 69.58000000000001
Weighted Precision: 0.6556853682557672
Weighted AreaUnderROC: 0.5543333333333333
Root mean squared error: 0.5491572330518608
Relative absolute error: 61.16363636363659
Root relative squared error: 109.83144661037215
Weighted TruePositiveRate: 0.6958
Weighted MatthewsCorrelation: 0.1526629918571462
Weighted FMeasure: 0.6488779383020943
Iteration time: 21.0
Weighted AreaUnderPRC: 0.6078804811988118
Mean absolute error: 0.30581818181818293
Coverage of cases: 69.58000000000001
Instances selection time: 21.0
Test time: 37.4
Accumulative iteration time: 177.4
Weighted Recall: 0.6958
Weighted FalsePositiveRate: 0.5871333333333333
Kappa statistic: 0.13016470270562616
Training time: 0.0
		
Iteration: 9
Labeled set size: 260
Unlabelled set size: 240
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.4
Correctly Classified Instances: 69.6
Weighted Precision: 0.6543597658900673
Weighted AreaUnderROC: 0.5512380952380952
Root mean squared error: 0.5491752077028397
Relative absolute error: 61.09923664122126
Root relative squared error: 109.83504154056793
Weighted TruePositiveRate: 0.6960000000000001
Weighted MatthewsCorrelation: 0.14748364536365943
Weighted FMeasure: 0.6461832927052352
Iteration time: 20.4
Weighted AreaUnderPRC: 0.6062336948239263
Mean absolute error: 0.3054961832061064
Coverage of cases: 69.6
Instances selection time: 20.3
Test time: 39.6
Accumulative iteration time: 197.8
Weighted Recall: 0.6960000000000001
Weighted FalsePositiveRate: 0.5935238095238096
Kappa statistic: 0.12368417643724787
Training time: 0.1
		
Iteration: 10
Labeled set size: 280
Unlabelled set size: 220
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.2
Correctly Classified Instances: 69.79999999999998
Weighted Precision: 0.656378373695681
Weighted AreaUnderROC: 0.5507619047619048
Root mean squared error: 0.547519243735541
Relative absolute error: 60.680851063829586
Root relative squared error: 109.50384874710817
Weighted TruePositiveRate: 0.698
Weighted MatthewsCorrelation: 0.14925262787801066
Weighted FMeasure: 0.6458391190742161
Iteration time: 22.0
Weighted AreaUnderPRC: 0.6061691061946122
Mean absolute error: 0.30340425531914794
Coverage of cases: 69.79999999999998
Instances selection time: 22.0
Test time: 50.8
Accumulative iteration time: 219.8
Weighted Recall: 0.698
Weighted FalsePositiveRate: 0.5964761904761904
Kappa statistic: 0.1233232971186852
Training time: 0.0
		
Iteration: 11
Labeled set size: 300
Unlabelled set size: 200
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.020000000000003
Correctly Classified Instances: 69.98
Weighted Precision: 0.6585137482253234
Weighted AreaUnderROC: 0.5497619047619048
Root mean squared error: 0.5460069548183186
Relative absolute error: 60.30463576158951
Root relative squared error: 109.20139096366374
Weighted TruePositiveRate: 0.6998
Weighted MatthewsCorrelation: 0.15057553967106804
Weighted FMeasure: 0.6451241174466823
Iteration time: 21.4
Weighted AreaUnderPRC: 0.6058142829821895
Mean absolute error: 0.3015231788079475
Coverage of cases: 69.98
Instances selection time: 21.3
Test time: 44.2
Accumulative iteration time: 241.2
Weighted Recall: 0.6998
Weighted FalsePositiveRate: 0.6002761904761905
Kappa statistic: 0.12203117504907188
Training time: 0.1
		
Iteration: 12
Labeled set size: 320
Unlabelled set size: 180
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.8
Correctly Classified Instances: 70.2
Weighted Precision: 0.6616919319489979
Weighted AreaUnderROC: 0.5486666666666666
Root mean squared error: 0.5441123341858158
Relative absolute error: 59.85093167701875
Root relative squared error: 108.82246683716319
Weighted TruePositiveRate: 0.702
Weighted MatthewsCorrelation: 0.15261321314889326
Weighted FMeasure: 0.6440821734778985
Iteration time: 17.0
Weighted AreaUnderPRC: 0.6055143169912686
Mean absolute error: 0.2992546583850938
Coverage of cases: 70.2
Instances selection time: 17.0
Test time: 46.7
Accumulative iteration time: 258.2
Weighted Recall: 0.702
Weighted FalsePositiveRate: 0.6046666666666666
Kappa statistic: 0.12047716633570198
Training time: 0.0
		
Iteration: 13
Labeled set size: 340
Unlabelled set size: 160
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.779999999999994
Correctly Classified Instances: 70.22
Weighted Precision: 0.6613774115471156
Weighted AreaUnderROC: 0.5455714285714286
Root mean squared error: 0.5440481749628455
Relative absolute error: 59.79649122807028
Root relative squared error: 108.80963499256913
Weighted TruePositiveRate: 0.7022
Weighted MatthewsCorrelation: 0.14782209820268705
Weighted FMeasure: 0.6407835109243025
Iteration time: 17.1
Weighted AreaUnderPRC: 0.6038838301695912
Mean absolute error: 0.2989824561403514
Coverage of cases: 70.22
Instances selection time: 17.1
Test time: 49.6
Accumulative iteration time: 275.3
Weighted Recall: 0.7022
Weighted FalsePositiveRate: 0.611057142857143
Kappa statistic: 0.11365806610753204
Training time: 0.0
		
Iteration: 14
Labeled set size: 360
Unlabelled set size: 140
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.68
Correctly Classified Instances: 70.32000000000001
Weighted Precision: 0.6631654851879347
Weighted AreaUnderROC: 0.544952380952381
Root mean squared error: 0.5432084693538004
Relative absolute error: 59.58453038674039
Root relative squared error: 108.64169387076008
Weighted TruePositiveRate: 0.7031999999999999
Weighted MatthewsCorrelation: 0.1486890768084755
Weighted FMeasure: 0.640002700149669
Iteration time: 15.3
Weighted AreaUnderPRC: 0.6037350468889453
Mean absolute error: 0.29792265193370193
Coverage of cases: 70.32000000000001
Instances selection time: 15.2
Test time: 52.0
Accumulative iteration time: 290.6
Weighted Recall: 0.7031999999999999
Weighted FalsePositiveRate: 0.613295238095238
Kappa statistic: 0.11259709294933737
Training time: 0.1
		
Iteration: 15
Labeled set size: 380
Unlabelled set size: 120
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.74
Correctly Classified Instances: 70.26000000000002
Weighted Precision: 0.6608954147171404
Weighted AreaUnderROC: 0.5447142857142858
Root mean squared error: 0.5438397777537145
Relative absolute error: 59.69214659685864
Root relative squared error: 108.76795555074291
Weighted TruePositiveRate: 0.7026
Weighted MatthewsCorrelation: 0.14636521647603834
Weighted FMeasure: 0.640015180417645
Iteration time: 14.1
Weighted AreaUnderPRC: 0.603549749372211
Mean absolute error: 0.2984607329842932
Coverage of cases: 70.26000000000002
Instances selection time: 14.0
Test time: 53.8
Accumulative iteration time: 304.7
Weighted Recall: 0.7026
Weighted FalsePositiveRate: 0.6131714285714286
Kappa statistic: 0.1118957634487944
Training time: 0.1
		
Iteration: 16
Labeled set size: 400
Unlabelled set size: 100
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.1
Correctly Classified Instances: 69.9
Weighted Precision: 0.6590402252939603
Weighted AreaUnderROC: 0.5589047619047619
Root mean squared error: 0.5471941964330447
Relative absolute error: 60.39800995024852
Root relative squared error: 109.43883928660894
Weighted TruePositiveRate: 0.699
Weighted MatthewsCorrelation: 0.16292385793107678
Weighted FMeasure: 0.6540828956953977
Iteration time: 11.9
Weighted AreaUnderPRC: 0.6105194491337086
Mean absolute error: 0.30199004975124266
Coverage of cases: 69.9
Instances selection time: 11.9
Test time: 57.2
Accumulative iteration time: 316.6
Weighted Recall: 0.699
Weighted FalsePositiveRate: 0.5811904761904761
Kappa statistic: 0.14114692802648626
Training time: 0.0
		
Iteration: 17
Labeled set size: 420
Unlabelled set size: 80
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.5
Correctly Classified Instances: 69.5
Weighted Precision: 0.6606451533230706
Weighted AreaUnderROC: 0.5712857142857144
Root mean squared error: 0.5508737774615529
Relative absolute error: 61.1848341232227
Root relative squared error: 110.17475549231062
Weighted TruePositiveRate: 0.6950000000000001
Weighted MatthewsCorrelation: 0.17749524039807268
Weighted FMeasure: 0.6631158315909723
Iteration time: 10.4
Weighted AreaUnderPRC: 0.6167458863540778
Mean absolute error: 0.30592417061611343
Coverage of cases: 69.5
Instances selection time: 10.3
Test time: 67.7
Accumulative iteration time: 327.0
Weighted Recall: 0.6950000000000001
Weighted FalsePositiveRate: 0.5524285714285714
Kappa statistic: 0.16411781911630302
Training time: 0.1
		
Iteration: 18
Labeled set size: 440
Unlabelled set size: 60
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.5
Correctly Classified Instances: 69.5
Weighted Precision: 0.6696146573687213
Weighted AreaUnderROC: 0.5884285714285714
Root mean squared error: 0.5506879280523196
Relative absolute error: 61.17647058823543
Root relative squared error: 110.1375856104639
Weighted TruePositiveRate: 0.695
Weighted MatthewsCorrelation: 0.20459841343223747
Weighted FMeasure: 0.674082508478093
Iteration time: 9.4
Weighted AreaUnderPRC: 0.6264466070056327
Mean absolute error: 0.30588235294117705
Coverage of cases: 69.5
Instances selection time: 9.4
Test time: 71.3
Accumulative iteration time: 336.4
Weighted Recall: 0.695
Weighted FalsePositiveRate: 0.5181428571428571
Kappa statistic: 0.19625101591742222
Training time: 0.0
		
Iteration: 19
Labeled set size: 460
Unlabelled set size: 40
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.52
Correctly Classified Instances: 69.48
Weighted Precision: 0.6754396202834916
Weighted AreaUnderROC: 0.6008571428571429
Root mean squared error: 0.5509382841531615
Relative absolute error: 61.20865800865803
Root relative squared error: 110.18765683063228
Weighted TruePositiveRate: 0.6948000000000001
Weighted MatthewsCorrelation: 0.22243031637263275
Weighted FMeasure: 0.6807158717902222
Iteration time: 5.9
Weighted AreaUnderPRC: 0.6331890666852615
Mean absolute error: 0.3060432900432902
Coverage of cases: 69.48
Instances selection time: 5.8
Test time: 63.2
Accumulative iteration time: 342.3
Weighted Recall: 0.6948000000000001
Weighted FalsePositiveRate: 0.49308571428571435
Kappa statistic: 0.21774248198463836
Training time: 0.1
		
Iteration: 20
Labeled set size: 480
Unlabelled set size: 20
	
Mean region size: 50.0
Incorrectly Classified Instances: 31.139999999999997
Correctly Classified Instances: 68.86
Weighted Precision: 0.6751740436209643
Weighted AreaUnderROC: 0.605952380952381
Root mean squared error: 0.5566037450586352
Relative absolute error: 62.4365145228216
Root relative squared error: 111.32074901172703
Weighted TruePositiveRate: 0.6886
Weighted MatthewsCorrelation: 0.22449497543660848
Weighted FMeasure: 0.6798596020949421
Iteration time: 3.9
Weighted AreaUnderPRC: 0.6354852294810261
Mean absolute error: 0.312182572614108
Coverage of cases: 68.86
Instances selection time: 3.8
Test time: 65.5
Accumulative iteration time: 346.2
Weighted Recall: 0.6886
Weighted FalsePositiveRate: 0.47669523809523817
Kappa statistic: 0.22241351966404924
Training time: 0.1
		
Time end:Tue Oct 31 11.32.05 EET 2017