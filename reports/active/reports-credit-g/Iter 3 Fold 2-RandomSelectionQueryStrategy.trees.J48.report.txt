Tue Oct 31 11.34.32 EET 2017
Dataset: german_credit
Test set size: 500
Initial Labelled set size: 100
Initial Unlabelled set size: 400
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Tue Oct 31 11.34.32 EET 2017
		
Iteration: 1
Labeled set size: 100
Unlabelled set size: 400
	
Mean region size: 81.9
Incorrectly Classified Instances: 31.6
Correctly Classified Instances: 68.4
Weighted Precision: 0.6757918552036198
Weighted AreaUnderROC: 0.5977523809523809
Root mean squared error: 0.5365771360735015
Relative absolute error: 68.94424242424255
Root relative squared error: 107.3154272147003
Weighted TruePositiveRate: 0.684
Weighted MatthewsCorrelation: 0.22754022935788004
Weighted FMeasure: 0.6793637734814205
Iteration time: 3.0
Weighted AreaUnderPRC: 0.6394604283248908
Mean absolute error: 0.3447212121212127
Coverage of cases: 82.4
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 3.0
Weighted Recall: 0.684
Weighted FalsePositiveRate: 0.4630476190476191
Kappa statistic: 0.22700587084148752
Training time: 2.0
		
Iteration: 2
Labeled set size: 120
Unlabelled set size: 380
	
Mean region size: 89.9
Incorrectly Classified Instances: 29.6
Correctly Classified Instances: 70.4
Weighted Precision: 0.7160071301247771
Weighted AreaUnderROC: 0.7110952380952381
Root mean squared error: 0.4820890592021157
Relative absolute error: 67.13907023656695
Root relative squared error: 96.41781184042314
Weighted TruePositiveRate: 0.704
Weighted MatthewsCorrelation: 0.32246082914662294
Weighted FMeasure: 0.7088970588235295
Iteration time: 3.0
Weighted AreaUnderPRC: 0.7134298042435191
Mean absolute error: 0.3356953511828348
Coverage of cases: 91.0
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 6.0
Weighted Recall: 0.704
Weighted FalsePositiveRate: 0.3706666666666667
Kappa statistic: 0.3211009174311927
Training time: 2.0
		
Iteration: 3
Labeled set size: 140
Unlabelled set size: 360
	
Mean region size: 76.4
Incorrectly Classified Instances: 31.2
Correctly Classified Instances: 68.8
Weighted Precision: 0.6659207459207459
Weighted AreaUnderROC: 0.6296095238095238
Root mean squared error: 0.5180997627033798
Relative absolute error: 71.54652014652021
Root relative squared error: 103.61995254067595
Weighted TruePositiveRate: 0.688
Weighted MatthewsCorrelation: 0.20017752139298778
Weighted FMeasure: 0.6724324324324326
Iteration time: 3.0
Weighted AreaUnderPRC: 0.6560466307764713
Mean absolute error: 0.35773260073260105
Coverage of cases: 83.4
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 9.0
Weighted Recall: 0.688
Weighted FalsePositiveRate: 0.5070476190476191
Kappa statistic: 0.1958762886597937
Training time: 3.0
		
Iteration: 4
Labeled set size: 160
Unlabelled set size: 340
	
Mean region size: 83.6
Incorrectly Classified Instances: 30.2
Correctly Classified Instances: 69.8
Weighted Precision: 0.6865730583589295
Weighted AreaUnderROC: 0.6689619047619048
Root mean squared error: 0.4954380728235702
Relative absolute error: 70.00334440753042
Root relative squared error: 99.08761456471404
Weighted TruePositiveRate: 0.698
Weighted MatthewsCorrelation: 0.2523649654490769
Weighted FMeasure: 0.6910324668545778
Iteration time: 4.0
Weighted AreaUnderPRC: 0.6834408363258436
Mean absolute error: 0.3500167220376521
Coverage of cases: 89.8
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 13.0
Weighted Recall: 0.698
Weighted FalsePositiveRate: 0.4570476190476191
Kappa statistic: 0.2509920634920634
Training time: 3.0
		
Iteration: 5
Labeled set size: 180
Unlabelled set size: 320
	
Mean region size: 82.6
Incorrectly Classified Instances: 29.8
Correctly Classified Instances: 70.2
Weighted Precision: 0.6831103793647527
Weighted AreaUnderROC: 0.6177142857142857
Root mean squared error: 0.5043867401203893
Relative absolute error: 69.14938238573018
Root relative squared error: 100.87734802407786
Weighted TruePositiveRate: 0.702
Weighted MatthewsCorrelation: 0.24105027333145682
Weighted FMeasure: 0.688518348458193
Iteration time: 4.0
Weighted AreaUnderPRC: 0.6504376905840434
Mean absolute error: 0.3457469119286509
Coverage of cases: 86.6
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 17.0
Weighted Recall: 0.702
Weighted FalsePositiveRate: 0.482
Kappa statistic: 0.2366803278688523
Training time: 3.0
		
Iteration: 6
Labeled set size: 200
Unlabelled set size: 300
	
Mean region size: 86.7
Incorrectly Classified Instances: 27.0
Correctly Classified Instances: 73.0
Weighted Precision: 0.7211051118144769
Weighted AreaUnderROC: 0.7203904761904762
Root mean squared error: 0.4744291830271377
Relative absolute error: 64.94588763088758
Root relative squared error: 94.88583660542754
Weighted TruePositiveRate: 0.73
Weighted MatthewsCorrelation: 0.33448097661734677
Weighted FMeasure: 0.7244393409193275
Iteration time: 4.0
Weighted AreaUnderPRC: 0.7209232634581058
Mean absolute error: 0.3247294381544379
Coverage of cases: 91.2
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 21.0
Weighted Recall: 0.73
Weighted FalsePositiveRate: 0.40904761904761905
Kappa statistic: 0.33300395256917
Training time: 3.0
		
Iteration: 7
Labeled set size: 220
Unlabelled set size: 280
	
Mean region size: 80.8
Incorrectly Classified Instances: 33.8
Correctly Classified Instances: 66.2
Weighted Precision: 0.6499244916113283
Weighted AreaUnderROC: 0.5245428571428571
Root mean squared error: 0.5211652189334751
Relative absolute error: 73.94885897872012
Root relative squared error: 104.23304378669502
Weighted TruePositiveRate: 0.662
Weighted MatthewsCorrelation: 0.16575170057892258
Weighted FMeasure: 0.6550388786323431
Iteration time: 5.0
Weighted AreaUnderPRC: 0.6060197922085735
Mean absolute error: 0.3697442948936006
Coverage of cases: 85.0
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 26.0
Weighted Recall: 0.662
Weighted FalsePositiveRate: 0.5029523809523809
Kappa statistic: 0.16501976284585
Training time: 4.0
		
Iteration: 8
Labeled set size: 240
Unlabelled set size: 260
	
Mean region size: 86.1
Incorrectly Classified Instances: 30.4
Correctly Classified Instances: 69.6
Weighted Precision: 0.6971642468239563
Weighted AreaUnderROC: 0.6300285714285715
Root mean squared error: 0.4853342564301933
Relative absolute error: 66.64105294705307
Root relative squared error: 97.06685128603866
Weighted TruePositiveRate: 0.696
Weighted MatthewsCorrelation: 0.2789499366385215
Weighted FMeasure: 0.6965710924305965
Iteration time: 5.0
Weighted AreaUnderPRC: 0.6716566921972342
Mean absolute error: 0.33320526473526535
Coverage of cases: 93.0
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 31.0
Weighted Recall: 0.696
Weighted FalsePositiveRate: 0.416
Kappa statistic: 0.2789373814041744
Training time: 4.0
		
Iteration: 9
Labeled set size: 260
Unlabelled set size: 240
	
Mean region size: 90.2
Incorrectly Classified Instances: 34.6
Correctly Classified Instances: 65.4
Weighted Precision: 0.6349440676857323
Weighted AreaUnderROC: 0.5946857142857143
Root mean squared error: 0.5315433995527805
Relative absolute error: 75.59611354491992
Root relative squared error: 106.30867991055611
Weighted TruePositiveRate: 0.654
Weighted MatthewsCorrelation: 0.12941415573316656
Weighted FMeasure: 0.6423691150491749
Iteration time: 6.0
Weighted AreaUnderPRC: 0.642277693024123
Mean absolute error: 0.3779805677245996
Coverage of cases: 88.8
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 37.0
Weighted Recall: 0.654
Weighted FalsePositiveRate: 0.533047619047619
Kappa statistic: 0.12802419354838726
Training time: 5.0
		
Iteration: 10
Labeled set size: 280
Unlabelled set size: 220
	
Mean region size: 90.1
Incorrectly Classified Instances: 30.8
Correctly Classified Instances: 69.2
Weighted Precision: 0.6862698412698413
Weighted AreaUnderROC: 0.6919809523809525
Root mean squared error: 0.5007527012760771
Relative absolute error: 67.16403280929585
Root relative squared error: 100.15054025521542
Weighted TruePositiveRate: 0.692
Weighted MatthewsCorrelation: 0.25272513219191517
Weighted FMeasure: 0.6888586692569209
Iteration time: 6.0
Weighted AreaUnderPRC: 0.7071883447586494
Mean absolute error: 0.33582016404647924
Coverage of cases: 91.8
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 43.0
Weighted Recall: 0.692
Weighted FalsePositiveRate: 0.4443809523809524
Kappa statistic: 0.25242718446601936
Training time: 5.0
		
Iteration: 11
Labeled set size: 300
Unlabelled set size: 200
	
Mean region size: 87.1
Incorrectly Classified Instances: 33.2
Correctly Classified Instances: 66.8
Weighted Precision: 0.6823879750425411
Weighted AreaUnderROC: 0.649647619047619
Root mean squared error: 0.5203160296318989
Relative absolute error: 69.95439552808406
Root relative squared error: 104.0632059263798
Weighted TruePositiveRate: 0.668
Weighted MatthewsCorrelation: 0.24254545125430668
Weighted FMeasure: 0.6739551842283662
Iteration time: 6.0
Weighted AreaUnderPRC: 0.6741806539859425
Mean absolute error: 0.34977197764042034
Coverage of cases: 87.4
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 49.0
Weighted Recall: 0.668
Weighted FalsePositiveRate: 0.41657142857142854
Kappa statistic: 0.24131627056672766
Training time: 6.0
		
Iteration: 12
Labeled set size: 320
Unlabelled set size: 180
	
Mean region size: 87.9
Incorrectly Classified Instances: 31.8
Correctly Classified Instances: 68.2
Weighted Precision: 0.6813954377712766
Weighted AreaUnderROC: 0.6226285714285714
Root mean squared error: 0.5082816281551703
Relative absolute error: 67.60875248281141
Root relative squared error: 101.65632563103406
Weighted TruePositiveRate: 0.682
Weighted MatthewsCorrelation: 0.2414149613442084
Weighted FMeasure: 0.6816950462549917
Iteration time: 6.0
Weighted AreaUnderPRC: 0.662401502557775
Mean absolute error: 0.33804376241405704
Coverage of cases: 88.4
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 55.0
Weighted Recall: 0.682
Weighted FalsePositiveRate: 0.441047619047619
Kappa statistic: 0.24141221374045815
Training time: 5.0
		
Iteration: 13
Labeled set size: 340
Unlabelled set size: 160
	
Mean region size: 87.1
Incorrectly Classified Instances: 31.0
Correctly Classified Instances: 69.0
Weighted Precision: 0.6954296464339069
Weighted AreaUnderROC: 0.635847619047619
Root mean squared error: 0.5053199276418465
Relative absolute error: 70.25630237708481
Root relative squared error: 101.0639855283693
Weighted TruePositiveRate: 0.69
Weighted MatthewsCorrelation: 0.2745884220238983
Weighted FMeasure: 0.6924957497927584
Iteration time: 6.0
Weighted AreaUnderPRC: 0.6671754013554598
Mean absolute error: 0.35128151188542406
Coverage of cases: 87.4
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 61.0
Weighted Recall: 0.69
Weighted FalsePositiveRate: 0.41095238095238096
Kappa statistic: 0.2743445692883894
Training time: 5.0
		
Iteration: 14
Labeled set size: 360
Unlabelled set size: 140
	
Mean region size: 87.0
Incorrectly Classified Instances: 31.4
Correctly Classified Instances: 68.6
Weighted Precision: 0.6878058427982143
Weighted AreaUnderROC: 0.6288952380952381
Root mean squared error: 0.5076473631408048
Relative absolute error: 70.81990610768993
Root relative squared error: 101.52947262816096
Weighted TruePositiveRate: 0.686
Weighted MatthewsCorrelation: 0.2566546816785473
Weighted FMeasure: 0.6868787022174241
Iteration time: 6.0
Weighted AreaUnderPRC: 0.6631549917483047
Mean absolute error: 0.35409953053844967
Coverage of cases: 87.2
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 67.0
Weighted Recall: 0.686
Weighted FalsePositiveRate: 0.42790476190476184
Kappa statistic: 0.256628787878788
Training time: 6.0
		
Iteration: 15
Labeled set size: 380
Unlabelled set size: 120
	
Mean region size: 86.7
Incorrectly Classified Instances: 30.2
Correctly Classified Instances: 69.8
Weighted Precision: 0.6886541967999017
Weighted AreaUnderROC: 0.6138285714285714
Root mean squared error: 0.5094471109904983
Relative absolute error: 69.50320559023983
Root relative squared error: 101.88942219809967
Weighted TruePositiveRate: 0.698
Weighted MatthewsCorrelation: 0.2577935264138866
Weighted FMeasure: 0.6925095238799277
Iteration time: 7.0
Weighted AreaUnderPRC: 0.6572032557997315
Mean absolute error: 0.3475160279511992
Coverage of cases: 86.8
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 74.0
Weighted Recall: 0.698
Weighted FalsePositiveRate: 0.4494285714285715
Kappa statistic: 0.25688976377952744
Training time: 6.0
		
Iteration: 16
Labeled set size: 400
Unlabelled set size: 100
	
Mean region size: 88.8
Incorrectly Classified Instances: 29.6
Correctly Classified Instances: 70.4
Weighted Precision: 0.704
Weighted AreaUnderROC: 0.6783809523809524
Root mean squared error: 0.5004100290848733
Relative absolute error: 68.98867732462475
Root relative squared error: 100.08200581697466
Weighted TruePositiveRate: 0.704
Weighted MatthewsCorrelation: 0.29523809523809524
Weighted FMeasure: 0.704
Iteration time: 8.0
Weighted AreaUnderPRC: 0.6965827181128063
Mean absolute error: 0.34494338662312374
Coverage of cases: 88.0
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 82.0
Weighted Recall: 0.704
Weighted FalsePositiveRate: 0.40876190476190477
Kappa statistic: 0.2952380952380952
Training time: 7.0
		
Iteration: 17
Labeled set size: 420
Unlabelled set size: 80
	
Mean region size: 89.0
Incorrectly Classified Instances: 28.4
Correctly Classified Instances: 71.6
Weighted Precision: 0.7128277153558054
Weighted AreaUnderROC: 0.6638190476190476
Root mean squared error: 0.49567458818516996
Relative absolute error: 69.46013960349158
Root relative squared error: 99.134917637034
Weighted TruePositiveRate: 0.716
Weighted MatthewsCorrelation: 0.31612435140713707
Weighted FMeasure: 0.714308839683182
Iteration time: 8.0
Weighted AreaUnderPRC: 0.6819321444111006
Mean absolute error: 0.3473006980174579
Coverage of cases: 88.4
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 90.0
Weighted Recall: 0.716
Weighted FalsePositiveRate: 0.4036190476190476
Kappa statistic: 0.3159922928709055
Training time: 7.0
		
Iteration: 18
Labeled set size: 440
Unlabelled set size: 60
	
Mean region size: 89.1
Incorrectly Classified Instances: 29.4
Correctly Classified Instances: 70.6
Weighted Precision: 0.7043360120252068
Weighted AreaUnderROC: 0.6643047619047621
Root mean squared error: 0.49418983238811887
Relative absolute error: 69.49283764006618
Root relative squared error: 98.83796647762378
Weighted TruePositiveRate: 0.706
Weighted MatthewsCorrelation: 0.29600756610477347
Weighted FMeasure: 0.7051424630371997
Iteration time: 9.0
Weighted AreaUnderPRC: 0.6837841616837199
Mean absolute error: 0.3474641882003309
Coverage of cases: 88.4
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 99.0
Weighted Recall: 0.706
Weighted FalsePositiveRate: 0.4117142857142857
Kappa statistic: 0.2959770114942527
Training time: 8.0
		
Iteration: 19
Labeled set size: 460
Unlabelled set size: 40
	
Mean region size: 86.7
Incorrectly Classified Instances: 29.0
Correctly Classified Instances: 71.0
Weighted Precision: 0.7116742950782619
Weighted AreaUnderROC: 0.6690571428571429
Root mean squared error: 0.4958696517405395
Relative absolute error: 68.6741441768147
Root relative squared error: 99.1739303481079
Weighted TruePositiveRate: 0.71
Weighted MatthewsCorrelation: 0.31347859644132536
Weighted FMeasure: 0.7108115402644999
Iteration time: 9.0
Weighted AreaUnderPRC: 0.6875320927802231
Mean absolute error: 0.3433707208840735
Coverage of cases: 87.8
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 108.0
Weighted Recall: 0.71
Weighted FalsePositiveRate: 0.39476190476190476
Kappa statistic: 0.3134469696969696
Training time: 8.0
		
Iteration: 20
Labeled set size: 480
Unlabelled set size: 20
	
Mean region size: 88.9
Incorrectly Classified Instances: 29.0
Correctly Classified Instances: 71.0
Weighted Precision: 0.7105542799673619
Weighted AreaUnderROC: 0.6811714285714285
Root mean squared error: 0.4895629259735159
Relative absolute error: 69.72030652273219
Root relative squared error: 97.91258519470318
Weighted TruePositiveRate: 0.71
Weighted MatthewsCorrelation: 0.31084001279664947
Weighted FMeasure: 0.7102742883758953
Iteration time: 9.0
Weighted AreaUnderPRC: 0.6923544767393026
Mean absolute error: 0.34860153261366095
Coverage of cases: 89.2
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 117.0
Weighted Recall: 0.71
Weighted FalsePositiveRate: 0.3985714285714286
Kappa statistic: 0.31083650190114054
Training time: 8.0
		
Time end:Tue Oct 31 11.34.33 EET 2017