Tue Oct 31 11.29.33 EET 2017
Dataset: german_credit
Test set size: 500
Initial Labelled set size: 100
Initial Unlabelled set size: 400
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Tue Oct 31 11.29.33 EET 2017
		
Iteration: 1
Labeled set size: 100
Unlabelled set size: 400
	
Mean region size: 100.0
Incorrectly Classified Instances: 30.0
Correctly Classified Instances: 70.0
Weighted Precision: 0.48999999999999994
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.46904157598234286
Relative absolute error: 76.00000000000045
Root relative squared error: 93.80831519646857
Weighted TruePositiveRate: 0.7
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.5764705882352942
Iteration time: 65.0
Weighted AreaUnderPRC: 0.58
Mean absolute error: 0.3800000000000023
Coverage of cases: 100.0
Instances selection time: 10.0
Test time: 3.0
Accumulative iteration time: 65.0
Weighted Recall: 0.7
Weighted FalsePositiveRate: 0.7
Kappa statistic: 0.0
Training time: 55.0
		
Iteration: 2
Labeled set size: 120
Unlabelled set size: 380
	
Mean region size: 100.0
Incorrectly Classified Instances: 30.0
Correctly Classified Instances: 70.0
Weighted Precision: 0.48999999999999994
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.4772607021092114
Relative absolute error: 73.33333333333333
Root relative squared error: 95.45214042184227
Weighted TruePositiveRate: 0.7
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.5764705882352942
Iteration time: 28.0
Weighted AreaUnderPRC: 0.58
Mean absolute error: 0.36666666666666664
Coverage of cases: 100.0
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 93.0
Weighted Recall: 0.7
Weighted FalsePositiveRate: 0.7
Kappa statistic: 0.0
Training time: 26.0
		
Iteration: 3
Labeled set size: 140
Unlabelled set size: 360
	
Mean region size: 100.0
Incorrectly Classified Instances: 30.0
Correctly Classified Instances: 70.0
Weighted Precision: 0.48999999999999994
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.4844521416518056
Relative absolute error: 71.42857142857174
Root relative squared error: 96.89042833036112
Weighted TruePositiveRate: 0.7
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.5764705882352942
Iteration time: 23.0
Weighted AreaUnderPRC: 0.58
Mean absolute error: 0.3571428571428587
Coverage of cases: 100.0
Instances selection time: 3.0
Test time: 6.0
Accumulative iteration time: 116.0
Weighted Recall: 0.7
Weighted FalsePositiveRate: 0.7
Kappa statistic: 0.0
Training time: 20.0
		
Iteration: 4
Labeled set size: 160
Unlabelled set size: 340
	
Mean region size: 100.0
Incorrectly Classified Instances: 30.0
Correctly Classified Instances: 70.0
Weighted Precision: 0.48999999999999994
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.4905354217587146
Relative absolute error: 70.0
Root relative squared error: 98.10708435174293
Weighted TruePositiveRate: 0.7
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.5764705882352942
Iteration time: 24.0
Weighted AreaUnderPRC: 0.58
Mean absolute error: 0.35
Coverage of cases: 100.0
Instances selection time: 18.0
Test time: 7.0
Accumulative iteration time: 140.0
Weighted Recall: 0.7
Weighted FalsePositiveRate: 0.7
Kappa statistic: 0.0
Training time: 6.0
		
Iteration: 5
Labeled set size: 180
Unlabelled set size: 320
	
Mean region size: 100.0
Incorrectly Classified Instances: 30.0
Correctly Classified Instances: 70.0
Weighted Precision: 0.48999999999999994
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.49566017829323333
Relative absolute error: 68.88888888888874
Root relative squared error: 99.13203565864667
Weighted TruePositiveRate: 0.7
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.5764705882352942
Iteration time: 14.0
Weighted AreaUnderPRC: 0.58
Mean absolute error: 0.3444444444444437
Coverage of cases: 100.0
Instances selection time: 3.0
Test time: 5.0
Accumulative iteration time: 154.0
Weighted Recall: 0.7
Weighted FalsePositiveRate: 0.7
Kappa statistic: 0.0
Training time: 11.0
		
Iteration: 6
Labeled set size: 200
Unlabelled set size: 300
	
Mean region size: 100.0
Incorrectly Classified Instances: 30.0
Correctly Classified Instances: 70.0
Weighted Precision: 0.48999999999999994
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.5000000000000004
Relative absolute error: 68.00000000000034
Root relative squared error: 100.00000000000009
Weighted TruePositiveRate: 0.7
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.5764705882352942
Iteration time: 20.0
Weighted AreaUnderPRC: 0.58
Mean absolute error: 0.3400000000000017
Coverage of cases: 100.0
Instances selection time: 10.0
Test time: 2.0
Accumulative iteration time: 174.0
Weighted Recall: 0.7
Weighted FalsePositiveRate: 0.7
Kappa statistic: 0.0
Training time: 10.0
		
Iteration: 7
Labeled set size: 220
Unlabelled set size: 280
	
Mean region size: 100.0
Incorrectly Classified Instances: 30.0
Correctly Classified Instances: 70.0
Weighted Precision: 0.48999999999999994
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.503705279170729
Relative absolute error: 67.27272727272708
Root relative squared error: 100.74105583414581
Weighted TruePositiveRate: 0.7
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.5764705882352942
Iteration time: 28.0
Weighted AreaUnderPRC: 0.58
Mean absolute error: 0.3363636363636354
Coverage of cases: 100.0
Instances selection time: 6.0
Test time: 2.0
Accumulative iteration time: 202.0
Weighted Recall: 0.7
Weighted FalsePositiveRate: 0.7
Kappa statistic: 0.0
Training time: 22.0
		
Iteration: 8
Labeled set size: 240
Unlabelled set size: 260
	
Mean region size: 100.0
Incorrectly Classified Instances: 30.0
Correctly Classified Instances: 70.0
Weighted Precision: 0.48999999999999994
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.5068968775248508
Relative absolute error: 66.66666666666656
Root relative squared error: 101.37937550497016
Weighted TruePositiveRate: 0.7
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.5764705882352942
Iteration time: 18.0
Weighted AreaUnderPRC: 0.58
Mean absolute error: 0.33333333333333276
Coverage of cases: 100.0
Instances selection time: 6.0
Test time: 1.0
Accumulative iteration time: 220.0
Weighted Recall: 0.7
Weighted FalsePositiveRate: 0.7
Kappa statistic: 0.0
Training time: 12.0
		
Iteration: 9
Labeled set size: 260
Unlabelled set size: 240
	
Mean region size: 100.0
Incorrectly Classified Instances: 30.0
Correctly Classified Instances: 70.0
Weighted Precision: 0.48999999999999994
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.5096698084146898
Relative absolute error: 66.15384615384616
Root relative squared error: 101.93396168293796
Weighted TruePositiveRate: 0.7
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.5764705882352942
Iteration time: 16.0
Weighted AreaUnderPRC: 0.58
Mean absolute error: 0.3307692307692308
Coverage of cases: 100.0
Instances selection time: 4.0
Test time: 2.0
Accumulative iteration time: 236.0
Weighted Recall: 0.7
Weighted FalsePositiveRate: 0.7
Kappa statistic: 0.0
Training time: 12.0
		
Iteration: 10
Labeled set size: 280
Unlabelled set size: 220
	
Mean region size: 100.0
Incorrectly Classified Instances: 30.0
Correctly Classified Instances: 70.0
Weighted Precision: 0.48999999999999994
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.5120985236838554
Relative absolute error: 65.71428571428567
Root relative squared error: 102.4197047367711
Weighted TruePositiveRate: 0.7
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.5764705882352942
Iteration time: 12.0
Weighted AreaUnderPRC: 0.58
Mean absolute error: 0.32857142857142835
Coverage of cases: 100.0
Instances selection time: 5.0
Test time: 1.0
Accumulative iteration time: 248.0
Weighted Recall: 0.7
Weighted FalsePositiveRate: 0.7
Kappa statistic: 0.0
Training time: 7.0
		
Iteration: 11
Labeled set size: 300
Unlabelled set size: 200
	
Mean region size: 100.0
Incorrectly Classified Instances: 30.0
Correctly Classified Instances: 70.0
Weighted Precision: 0.48999999999999994
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.5142416206847163
Relative absolute error: 65.33333333333343
Root relative squared error: 102.84832413694328
Weighted TruePositiveRate: 0.7
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.5764705882352942
Iteration time: 12.0
Weighted AreaUnderPRC: 0.58
Mean absolute error: 0.3266666666666671
Coverage of cases: 100.0
Instances selection time: 4.0
Test time: 1.0
Accumulative iteration time: 260.0
Weighted Recall: 0.7
Weighted FalsePositiveRate: 0.7
Kappa statistic: 0.0
Training time: 8.0
		
Iteration: 12
Labeled set size: 320
Unlabelled set size: 180
	
Mean region size: 100.0
Incorrectly Classified Instances: 30.0
Correctly Classified Instances: 70.0
Weighted Precision: 0.48999999999999994
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.5161455705515645
Relative absolute error: 65.0
Root relative squared error: 103.2291141103129
Weighted TruePositiveRate: 0.7
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.5764705882352942
Iteration time: 16.0
Weighted AreaUnderPRC: 0.58
Mean absolute error: 0.325
Coverage of cases: 100.0
Instances selection time: 4.0
Test time: 1.0
Accumulative iteration time: 276.0
Weighted Recall: 0.7
Weighted FalsePositiveRate: 0.7
Kappa statistic: 0.0
Training time: 12.0
		
Iteration: 13
Labeled set size: 340
Unlabelled set size: 160
	
Mean region size: 100.0
Incorrectly Classified Instances: 30.0
Correctly Classified Instances: 70.0
Weighted Precision: 0.48999999999999994
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.5178475547546768
Relative absolute error: 64.70588235294098
Root relative squared error: 103.56951095093537
Weighted TruePositiveRate: 0.7
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.5764705882352942
Iteration time: 20.0
Weighted AreaUnderPRC: 0.58
Mean absolute error: 0.32352941176470484
Coverage of cases: 100.0
Instances selection time: 4.0
Test time: 1.0
Accumulative iteration time: 296.0
Weighted Recall: 0.7
Weighted FalsePositiveRate: 0.7
Kappa statistic: 0.0
Training time: 16.0
		
Iteration: 14
Labeled set size: 360
Unlabelled set size: 140
	
Mean region size: 100.0
Incorrectly Classified Instances: 30.0
Correctly Classified Instances: 70.0
Weighted Precision: 0.48999999999999994
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.5193775952231218
Relative absolute error: 64.44444444444457
Root relative squared error: 103.87551904462435
Weighted TruePositiveRate: 0.7
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.5764705882352942
Iteration time: 13.0
Weighted AreaUnderPRC: 0.58
Mean absolute error: 0.32222222222222285
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 309.0
Weighted Recall: 0.7
Weighted FalsePositiveRate: 0.7
Kappa statistic: 0.0
Training time: 12.0
		
Iteration: 15
Labeled set size: 380
Unlabelled set size: 120
	
Mean region size: 100.0
Incorrectly Classified Instances: 30.0
Correctly Classified Instances: 70.0
Weighted Precision: 0.48999999999999994
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.5087870536167145
Relative absolute error: 66.31578947368425
Root relative squared error: 101.7574107233429
Weighted TruePositiveRate: 0.7
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.5764705882352942
Iteration time: 12.0
Weighted AreaUnderPRC: 0.58
Mean absolute error: 0.33157894736842125
Coverage of cases: 100.0
Instances selection time: 5.0
Test time: 1.0
Accumulative iteration time: 321.0
Weighted Recall: 0.7
Weighted FalsePositiveRate: 0.7
Kappa statistic: 0.0
Training time: 7.0
		
Iteration: 16
Labeled set size: 400
Unlabelled set size: 100
	
Mean region size: 91.8
Incorrectly Classified Instances: 29.0
Correctly Classified Instances: 71.0
Weighted Precision: 0.6781975254411011
Weighted AreaUnderROC: 0.5571142857142857
Root mean squared error: 0.5046807745443043
Relative absolute error: 62.974294272705535
Root relative squared error: 100.93615490886086
Weighted TruePositiveRate: 0.71
Weighted MatthewsCorrelation: 0.15497405346635953
Weighted FMeasure: 0.6333535203696269
Iteration time: 27.0
Weighted AreaUnderPRC: 0.6143689075602192
Mean absolute error: 0.31487147136352767
Coverage of cases: 95.2
Instances selection time: 8.0
Test time: 1.0
Accumulative iteration time: 348.0
Weighted Recall: 0.71
Weighted FalsePositiveRate: 0.630952380952381
Kappa statistic: 0.10272277227722773
Training time: 19.0
		
Iteration: 17
Labeled set size: 420
Unlabelled set size: 80
	
Mean region size: 94.7
Incorrectly Classified Instances: 29.2
Correctly Classified Instances: 70.8
Weighted Precision: 0.670334928229665
Weighted AreaUnderROC: 0.557152380952381
Root mean squared error: 0.4968968832042415
Relative absolute error: 66.6788591800357
Root relative squared error: 99.3793766408483
Weighted TruePositiveRate: 0.708
Weighted MatthewsCorrelation: 0.1581061631693885
Weighted FMeasure: 0.6411592076302275
Iteration time: 24.0
Weighted AreaUnderPRC: 0.6110814979923397
Mean absolute error: 0.3333942959001785
Coverage of cases: 96.4
Instances selection time: 5.0
Test time: 2.0
Accumulative iteration time: 372.0
Weighted Recall: 0.708
Weighted FalsePositiveRate: 0.6165714285714285
Kappa statistic: 0.11622276029055686
Training time: 19.0
		
Iteration: 18
Labeled set size: 440
Unlabelled set size: 60
	
Mean region size: 93.6
Incorrectly Classified Instances: 29.6
Correctly Classified Instances: 70.4
Weighted Precision: 0.6618620414673047
Weighted AreaUnderROC: 0.5728666666666666
Root mean squared error: 0.496589992104711
Relative absolute error: 67.30179487179485
Root relative squared error: 99.3179984209422
Weighted TruePositiveRate: 0.704
Weighted MatthewsCorrelation: 0.15097591444033434
Weighted FMeasure: 0.6425980404696734
Iteration time: 28.0
Weighted AreaUnderPRC: 0.6176556240370671
Mean absolute error: 0.33650897435897426
Coverage of cases: 96.0
Instances selection time: 3.0
Test time: 2.0
Accumulative iteration time: 400.0
Weighted Recall: 0.704
Weighted FalsePositiveRate: 0.6106666666666666
Kappa statistic: 0.11694510739856803
Training time: 25.0
		
Iteration: 19
Labeled set size: 460
Unlabelled set size: 40
	
Mean region size: 93.6
Incorrectly Classified Instances: 29.6
Correctly Classified Instances: 70.4
Weighted Precision: 0.6618620414673047
Weighted AreaUnderROC: 0.5695333333333333
Root mean squared error: 0.48562480984586387
Relative absolute error: 70.78962258715067
Root relative squared error: 97.12496196917277
Weighted TruePositiveRate: 0.704
Weighted MatthewsCorrelation: 0.15097591444033434
Weighted FMeasure: 0.6425980404696734
Iteration time: 33.0
Weighted AreaUnderPRC: 0.6165303652095209
Mean absolute error: 0.35394811293575335
Coverage of cases: 96.0
Instances selection time: 5.0
Test time: 2.0
Accumulative iteration time: 433.0
Weighted Recall: 0.704
Weighted FalsePositiveRate: 0.6106666666666666
Kappa statistic: 0.11694510739856803
Training time: 28.0
		
Iteration: 20
Labeled set size: 480
Unlabelled set size: 20
	
Mean region size: 90.1
Incorrectly Classified Instances: 31.2
Correctly Classified Instances: 68.8
Weighted Precision: 0.6680119989091899
Weighted AreaUnderROC: 0.5910190476190477
Root mean squared error: 0.4999615463829242
Relative absolute error: 69.72112947240316
Root relative squared error: 99.99230927658485
Weighted TruePositiveRate: 0.688
Weighted MatthewsCorrelation: 0.2059726065192451
Weighted FMeasure: 0.6743577075098814
Iteration time: 28.0
Weighted AreaUnderPRC: 0.6354935166670325
Mean absolute error: 0.3486056473620158
Coverage of cases: 90.2
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 461.0
Weighted Recall: 0.688
Weighted FalsePositiveRate: 0.49942857142857144
Kappa statistic: 0.20245398773006118
Training time: 25.0
		
Time end:Tue Oct 31 11.29.35 EET 2017