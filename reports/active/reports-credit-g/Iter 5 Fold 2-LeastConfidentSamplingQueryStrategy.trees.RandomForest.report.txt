Tue Oct 31 11.32.25 EET 2017
Dataset: german_credit
Test set size: 500
Initial Labelled set size: 100
Initial Unlabelled set size: 400
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Tue Oct 31 11.32.25 EET 2017
		
Iteration: 1
Labeled set size: 100
Unlabelled set size: 400
	
Mean region size: 96.3
Incorrectly Classified Instances: 30.0
Correctly Classified Instances: 70.0
Weighted Precision: 0.6715686274509804
Weighted AreaUnderROC: 0.7063580952380952
Root mean squared error: 0.4372081666464105
Relative absolute error: 74.26511747792456
Root relative squared error: 87.4416333292821
Weighted TruePositiveRate: 0.7
Weighted MatthewsCorrelation: 0.20724505672934357
Weighted FMeasure: 0.6755271593362263
Iteration time: 6.0
Weighted AreaUnderPRC: 0.7361216337492532
Mean absolute error: 0.3713255873896228
Coverage of cases: 99.6
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 6.0
Weighted Recall: 0.7
Weighted FalsePositiveRate: 0.5247619047619048
Kappa statistic: 0.1970021413276231
Training time: 5.0
		
Iteration: 2
Labeled set size: 120
Unlabelled set size: 380
	
Mean region size: 97.6
Incorrectly Classified Instances: 29.8
Correctly Classified Instances: 70.2
Weighted Precision: 0.6761121485763986
Weighted AreaUnderROC: 0.6588295238095239
Root mean squared error: 0.44946307400072627
Relative absolute error: 77.62509282874527
Root relative squared error: 89.89261480014525
Weighted TruePositiveRate: 0.702
Weighted MatthewsCorrelation: 0.2196367652118523
Weighted FMeasure: 0.6805157238791124
Iteration time: 6.0
Weighted AreaUnderPRC: 0.7022687182085261
Mean absolute error: 0.38812546414372634
Coverage of cases: 99.4
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 12.0
Weighted Recall: 0.702
Weighted FalsePositiveRate: 0.5124761904761905
Kappa statistic: 0.2108050847457627
Training time: 5.0
		
Iteration: 3
Labeled set size: 140
Unlabelled set size: 360
	
Mean region size: 97.2
Incorrectly Classified Instances: 29.0
Correctly Classified Instances: 71.0
Weighted Precision: 0.6764809902740938
Weighted AreaUnderROC: 0.6399142857142857
Root mean squared error: 0.45320670324168516
Relative absolute error: 76.84498104348366
Root relative squared error: 90.64134064833704
Weighted TruePositiveRate: 0.71
Weighted MatthewsCorrelation: 0.20115032750298395
Weighted FMeasure: 0.6683750555473263
Iteration time: 6.0
Weighted AreaUnderPRC: 0.6772249612389708
Mean absolute error: 0.3842249052174183
Coverage of cases: 98.8
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 18.0
Weighted Recall: 0.71
Weighted FalsePositiveRate: 0.5623809523809524
Kappa statistic: 0.17613636363636348
Training time: 5.0
		
Iteration: 4
Labeled set size: 160
Unlabelled set size: 340
	
Mean region size: 94.0
Incorrectly Classified Instances: 27.8
Correctly Classified Instances: 72.2
Weighted Precision: 0.6955535593981585
Weighted AreaUnderROC: 0.7266857142857143
Root mean squared error: 0.42660422938649173
Relative absolute error: 70.40617856326735
Root relative squared error: 85.32084587729834
Weighted TruePositiveRate: 0.722
Weighted MatthewsCorrelation: 0.24843400603493257
Weighted FMeasure: 0.6877792590797075
Iteration time: 8.0
Weighted AreaUnderPRC: 0.7555759322649865
Mean absolute error: 0.35203089281633676
Coverage of cases: 99.4
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 26.0
Weighted Recall: 0.722
Weighted FalsePositiveRate: 0.5305714285714286
Kappa statistic: 0.22433035714285718
Training time: 7.0
		
Iteration: 5
Labeled set size: 180
Unlabelled set size: 320
	
Mean region size: 94.7
Incorrectly Classified Instances: 27.2
Correctly Classified Instances: 72.8
Weighted Precision: 0.7077469793887704
Weighted AreaUnderROC: 0.7098171428571429
Root mean squared error: 0.4331282468423728
Relative absolute error: 70.70105751243632
Root relative squared error: 86.62564936847455
Weighted TruePositiveRate: 0.728
Weighted MatthewsCorrelation: 0.29244616114175315
Weighted FMeasure: 0.7088881262868909
Iteration time: 8.0
Weighted AreaUnderPRC: 0.7406153191463066
Mean absolute error: 0.3535052875621816
Coverage of cases: 99.0
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 34.0
Weighted Recall: 0.728
Weighted FalsePositiveRate: 0.4746666666666667
Kappa statistic: 0.2811839323467229
Training time: 7.0
		
Iteration: 6
Labeled set size: 200
Unlabelled set size: 300
	
Mean region size: 98.0
Incorrectly Classified Instances: 27.2
Correctly Classified Instances: 72.8
Weighted Precision: 0.7053364790472981
Weighted AreaUnderROC: 0.6715104761904762
Root mean squared error: 0.4417335725308767
Relative absolute error: 76.6904010834066
Root relative squared error: 88.34671450617535
Weighted TruePositiveRate: 0.728
Weighted MatthewsCorrelation: 0.2798699836546749
Weighted FMeasure: 0.7025113142248648
Iteration time: 9.0
Weighted AreaUnderPRC: 0.7140247860276209
Mean absolute error: 0.383452005417033
Coverage of cases: 99.4
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 43.0
Weighted Recall: 0.728
Weighted FalsePositiveRate: 0.4975238095238095
Kappa statistic: 0.26247288503253796
Training time: 8.0
		
Iteration: 7
Labeled set size: 220
Unlabelled set size: 280
	
Mean region size: 96.7
Incorrectly Classified Instances: 31.0
Correctly Classified Instances: 69.0
Weighted Precision: 0.6516072579902368
Weighted AreaUnderROC: 0.6838885714285715
Root mean squared error: 0.44004217353373704
Relative absolute error: 75.3568987418547
Root relative squared error: 88.00843470674741
Weighted TruePositiveRate: 0.69
Weighted MatthewsCorrelation: 0.15597841995084358
Weighted FMeasure: 0.654791959925002
Iteration time: 10.0
Weighted AreaUnderPRC: 0.7201075882789855
Mean absolute error: 0.3767844937092735
Coverage of cases: 99.0
Instances selection time: 2.0
Test time: 2.0
Accumulative iteration time: 53.0
Weighted Recall: 0.69
Weighted FalsePositiveRate: 0.5671428571428571
Kappa statistic: 0.1426991150442477
Training time: 8.0
		
Iteration: 8
Labeled set size: 240
Unlabelled set size: 260
	
Mean region size: 96.9
Incorrectly Classified Instances: 28.2
Correctly Classified Instances: 71.8
Weighted Precision: 0.7002084346557355
Weighted AreaUnderROC: 0.7110790476190477
Root mean squared error: 0.4347294023777209
Relative absolute error: 74.7505136757347
Root relative squared error: 86.94588047554419
Weighted TruePositiveRate: 0.718
Weighted MatthewsCorrelation: 0.28039180988652934
Weighted FMeasure: 0.7043721711539359
Iteration time: 12.0
Weighted AreaUnderPRC: 0.7401571109692029
Mean absolute error: 0.3737525683786735
Coverage of cases: 99.6
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 65.0
Weighted Recall: 0.718
Weighted FalsePositiveRate: 0.46371428571428575
Kappa statistic: 0.27469135802469136
Training time: 11.0
		
Iteration: 9
Labeled set size: 260
Unlabelled set size: 240
	
Mean region size: 94.5
Incorrectly Classified Instances: 27.4
Correctly Classified Instances: 72.6
Weighted Precision: 0.7050906858356144
Weighted AreaUnderROC: 0.723015238095238
Root mean squared error: 0.42864348786710194
Relative absolute error: 70.68639294521694
Root relative squared error: 85.72869757342039
Weighted TruePositiveRate: 0.726
Weighted MatthewsCorrelation: 0.2858589054767324
Weighted FMeasure: 0.7062460011505932
Iteration time: 12.0
Weighted AreaUnderPRC: 0.7569922933677699
Mean absolute error: 0.3534319647260847
Coverage of cases: 99.2
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 77.0
Weighted Recall: 0.726
Weighted FalsePositiveRate: 0.47933333333333333
Kappa statistic: 0.27436440677966106
Training time: 11.0
		
Iteration: 10
Labeled set size: 280
Unlabelled set size: 220
	
Mean region size: 96.7
Incorrectly Classified Instances: 31.6
Correctly Classified Instances: 68.4
Weighted Precision: 0.6570795152231748
Weighted AreaUnderROC: 0.68572
Root mean squared error: 0.44731646267803393
Relative absolute error: 76.39511197574286
Root relative squared error: 89.46329253560678
Weighted TruePositiveRate: 0.684
Weighted MatthewsCorrelation: 0.1776202916220683
Weighted FMeasure: 0.664043799337917
Iteration time: 13.0
Weighted AreaUnderPRC: 0.7157561041129172
Mean absolute error: 0.38197555987871434
Coverage of cases: 99.0
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 90.0
Weighted Recall: 0.684
Weighted FalsePositiveRate: 0.5278095238095237
Kappa statistic: 0.17190775681341744
Training time: 12.0
		
Iteration: 11
Labeled set size: 300
Unlabelled set size: 200
	
Mean region size: 96.3
Incorrectly Classified Instances: 28.0
Correctly Classified Instances: 72.0
Weighted Precision: 0.699379249187112
Weighted AreaUnderROC: 0.7226123809523809
Root mean squared error: 0.4300055974884433
Relative absolute error: 72.97417056811155
Root relative squared error: 86.00111949768866
Weighted TruePositiveRate: 0.72
Weighted MatthewsCorrelation: 0.27509484190247163
Weighted FMeasure: 0.7023172905525846
Iteration time: 13.0
Weighted AreaUnderPRC: 0.7517028030646544
Mean absolute error: 0.36487085284055776
Coverage of cases: 99.2
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 103.0
Weighted Recall: 0.72
Weighted FalsePositiveRate: 0.47809523809523813
Kappa statistic: 0.26624737945492666
Training time: 12.0
		
Iteration: 12
Labeled set size: 320
Unlabelled set size: 180
	
Mean region size: 94.7
Incorrectly Classified Instances: 27.2
Correctly Classified Instances: 72.8
Weighted Precision: 0.7117912371134021
Weighted AreaUnderROC: 0.7399609523809525
Root mean squared error: 0.4256638869004789
Relative absolute error: 69.44128181258627
Root relative squared error: 85.13277738009577
Weighted TruePositiveRate: 0.728
Weighted MatthewsCorrelation: 0.30776045042428835
Weighted FMeasure: 0.7152775191874057
Iteration time: 14.0
Weighted AreaUnderPRC: 0.7602419412081441
Mean absolute error: 0.3472064090629313
Coverage of cases: 98.6
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 117.0
Weighted Recall: 0.728
Weighted FalsePositiveRate: 0.448
Kappa statistic: 0.30184804928131403
Training time: 13.0
		
Iteration: 13
Labeled set size: 340
Unlabelled set size: 160
	
Mean region size: 95.4
Incorrectly Classified Instances: 28.2
Correctly Classified Instances: 71.8
Weighted Precision: 0.6973637230686459
Weighted AreaUnderROC: 0.7037009523809523
Root mean squared error: 0.4378128375979502
Relative absolute error: 73.59309249666323
Root relative squared error: 87.56256751959003
Weighted TruePositiveRate: 0.718
Weighted MatthewsCorrelation: 0.2708633268764127
Weighted FMeasure: 0.7006778100544471
Iteration time: 15.0
Weighted AreaUnderPRC: 0.7303453258100628
Mean absolute error: 0.3679654624833162
Coverage of cases: 99.0
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 132.0
Weighted Recall: 0.718
Weighted FalsePositiveRate: 0.47895238095238096
Kappa statistic: 0.26255230125523
Training time: 14.0
		
Iteration: 14
Labeled set size: 360
Unlabelled set size: 140
	
Mean region size: 93.3
Incorrectly Classified Instances: 25.2
Correctly Classified Instances: 74.8
Weighted Precision: 0.7329470357245473
Weighted AreaUnderROC: 0.737804761904762
Root mean squared error: 0.42848427979611303
Relative absolute error: 69.82797878863124
Root relative squared error: 85.6968559592226
Weighted TruePositiveRate: 0.748
Weighted MatthewsCorrelation: 0.3545091041589279
Weighted FMeasure: 0.7337953629032258
Iteration time: 16.0
Weighted AreaUnderPRC: 0.75872769497322
Mean absolute error: 0.3491398939431562
Coverage of cases: 99.2
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 148.0
Weighted Recall: 0.748
Weighted FalsePositiveRate: 0.4318095238095238
Kappa statistic: 0.3451143451143452
Training time: 15.0
		
Iteration: 15
Labeled set size: 380
Unlabelled set size: 120
	
Mean region size: 93.9
Incorrectly Classified Instances: 25.8
Correctly Classified Instances: 74.2
Weighted Precision: 0.727746907228282
Weighted AreaUnderROC: 0.7465276190476191
Root mean squared error: 0.4225566536004676
Relative absolute error: 68.42596107788698
Root relative squared error: 84.51133072009353
Weighted TruePositiveRate: 0.742
Weighted MatthewsCorrelation: 0.34540104100741226
Weighted FMeasure: 0.7303279661148113
Iteration time: 16.0
Weighted AreaUnderPRC: 0.7661401561961511
Mean absolute error: 0.34212980538943494
Coverage of cases: 98.8
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 164.0
Weighted Recall: 0.742
Weighted FalsePositiveRate: 0.4267619047619048
Kappa statistic: 0.33913934426229503
Training time: 15.0
		
Iteration: 16
Labeled set size: 400
Unlabelled set size: 100
	
Mean region size: 94.7
Incorrectly Classified Instances: 25.8
Correctly Classified Instances: 74.2
Weighted Precision: 0.7250477679346882
Weighted AreaUnderROC: 0.7255838095238096
Root mean squared error: 0.42533363043251915
Relative absolute error: 69.74448359690368
Root relative squared error: 85.06672608650383
Weighted TruePositiveRate: 0.742
Weighted MatthewsCorrelation: 0.33371950703191244
Weighted FMeasure: 0.7252559854041777
Iteration time: 18.0
Weighted AreaUnderPRC: 0.7484916586385137
Mean absolute error: 0.34872241798451836
Coverage of cases: 98.8
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 182.0
Weighted Recall: 0.742
Weighted FalsePositiveRate: 0.44961904761904764
Kappa statistic: 0.3224789915966387
Training time: 17.0
		
Iteration: 17
Labeled set size: 420
Unlabelled set size: 80
	
Mean region size: 94.5
Incorrectly Classified Instances: 28.2
Correctly Classified Instances: 71.8
Weighted Precision: 0.691684724509549
Weighted AreaUnderROC: 0.7305942857142856
Root mean squared error: 0.42681583040708804
Relative absolute error: 71.35029320859266
Root relative squared error: 85.3631660814176
Weighted TruePositiveRate: 0.718
Weighted MatthewsCorrelation: 0.24749460376436455
Weighted FMeasure: 0.6897717543967635
Iteration time: 18.0
Weighted AreaUnderPRC: 0.7576018379466559
Mean absolute error: 0.3567514660429633
Coverage of cases: 99.6
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 200.0
Weighted Recall: 0.718
Weighted FalsePositiveRate: 0.5170476190476191
Kappa statistic: 0.23034934497816575
Training time: 17.0
		
Iteration: 18
Labeled set size: 440
Unlabelled set size: 60
	
Mean region size: 95.1
Incorrectly Classified Instances: 24.4
Correctly Classified Instances: 75.6
Weighted Precision: 0.74130081300813
Weighted AreaUnderROC: 0.7551419047619047
Root mean squared error: 0.4130121204963446
Relative absolute error: 67.40956905675509
Root relative squared error: 82.60242409926892
Weighted TruePositiveRate: 0.756
Weighted MatthewsCorrelation: 0.36351902751519843
Weighted FMeasure: 0.7351315789473685
Iteration time: 20.0
Weighted AreaUnderPRC: 0.7714714114943809
Mean absolute error: 0.33704784528377546
Coverage of cases: 98.8
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 220.0
Weighted Recall: 0.756
Weighted FalsePositiveRate: 0.4512380952380952
Kappa statistic: 0.34408602150537637
Training time: 19.0
		
Iteration: 19
Labeled set size: 460
Unlabelled set size: 40
	
Mean region size: 95.0
Incorrectly Classified Instances: 26.6
Correctly Classified Instances: 73.4
Weighted Precision: 0.7200977438575351
Weighted AreaUnderROC: 0.7541571428571429
Root mean squared error: 0.41946844498220603
Relative absolute error: 68.34849126947155
Root relative squared error: 83.89368899644121
Weighted TruePositiveRate: 0.734
Weighted MatthewsCorrelation: 0.328843088029971
Weighted FMeasure: 0.723549519444487
Iteration time: 20.0
Weighted AreaUnderPRC: 0.7792952320909049
Mean absolute error: 0.3417424563473578
Coverage of cases: 99.8
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 240.0
Weighted Recall: 0.734
Weighted FalsePositiveRate: 0.4301904761904761
Kappa statistic: 0.32418699186991856
Training time: 20.0
		
Iteration: 20
Labeled set size: 480
Unlabelled set size: 20
	
Mean region size: 94.6
Incorrectly Classified Instances: 28.0
Correctly Classified Instances: 72.0
Weighted Precision: 0.699379249187112
Weighted AreaUnderROC: 0.723172380952381
Root mean squared error: 0.42991932637581876
Relative absolute error: 71.24609589755514
Root relative squared error: 85.98386527516375
Weighted TruePositiveRate: 0.72
Weighted MatthewsCorrelation: 0.27509484190247163
Weighted FMeasure: 0.7023172905525846
Iteration time: 20.0
Weighted AreaUnderPRC: 0.748458797469885
Mean absolute error: 0.3562304794877757
Coverage of cases: 99.4
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 260.0
Weighted Recall: 0.72
Weighted FalsePositiveRate: 0.47809523809523813
Kappa statistic: 0.26624737945492666
Training time: 20.0
		
Time end:Tue Oct 31 11.32.26 EET 2017