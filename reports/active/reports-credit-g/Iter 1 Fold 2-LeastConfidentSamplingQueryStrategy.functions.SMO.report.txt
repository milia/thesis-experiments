Tue Oct 31 11.31.27 EET 2017
Dataset: german_credit
Test set size: 500
Initial Labelled set size: 100
Initial Unlabelled set size: 400
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Tue Oct 31 11.31.27 EET 2017
		
Iteration: 1
Labeled set size: 100
Unlabelled set size: 400
	
Mean region size: 50.0
Incorrectly Classified Instances: 36.2
Correctly Classified Instances: 63.8
Weighted Precision: 0.6842665750601168
Weighted AreaUnderROC: 0.6290476190476191
Root mean squared error: 0.6016643582596529
Relative absolute error: 72.39999999999999
Root relative squared error: 120.33287165193059
Weighted TruePositiveRate: 0.638
Weighted MatthewsCorrelation: 0.23918222049163881
Weighted FMeasure: 0.6515121242393971
Iteration time: 18.0
Weighted AreaUnderPRC: 0.6462137082658553
Mean absolute error: 0.362
Coverage of cases: 63.8
Instances selection time: 3.0
Test time: 3.0
Accumulative iteration time: 18.0
Weighted Recall: 0.638
Weighted FalsePositiveRate: 0.37990476190476186
Kappa statistic: 0.2304421768707484
Training time: 15.0
		
Iteration: 2
Labeled set size: 120
Unlabelled set size: 380
	
Mean region size: 50.0
Incorrectly Classified Instances: 31.4
Correctly Classified Instances: 68.6
Weighted Precision: 0.6965654277031522
Weighted AreaUnderROC: 0.6423809523809524
Root mean squared error: 0.5603570290448759
Relative absolute error: 62.8
Root relative squared error: 112.07140580897519
Weighted TruePositiveRate: 0.686
Weighted MatthewsCorrelation: 0.27668217985413707
Weighted FMeasure: 0.6905117984767517
Iteration time: 21.0
Weighted AreaUnderPRC: 0.6558761360162558
Mean absolute error: 0.314
Coverage of cases: 68.6
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 39.0
Weighted Recall: 0.686
Weighted FalsePositiveRate: 0.4012380952380952
Kappa statistic: 0.2758302583025831
Training time: 19.0
		
Iteration: 3
Labeled set size: 140
Unlabelled set size: 360
	
Mean region size: 50.0
Incorrectly Classified Instances: 26.8
Correctly Classified Instances: 73.2
Weighted Precision: 0.7145196820227948
Weighted AreaUnderROC: 0.6390476190476191
Root mean squared error: 0.5176871642217914
Relative absolute error: 53.6
Root relative squared error: 103.53743284435828
Weighted TruePositiveRate: 0.732
Weighted MatthewsCorrelation: 0.31179716389881607
Weighted FMeasure: 0.7168934811827956
Iteration time: 15.0
Weighted AreaUnderPRC: 0.6588156689972224
Mean absolute error: 0.268
Coverage of cases: 73.2
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 54.0
Weighted Recall: 0.732
Weighted FalsePositiveRate: 0.4539047619047619
Kappa statistic: 0.30353430353430355
Training time: 14.0
		
Iteration: 4
Labeled set size: 160
Unlabelled set size: 340
	
Mean region size: 50.0
Incorrectly Classified Instances: 24.0
Correctly Classified Instances: 76.0
Weighted Precision: 0.747652798232695
Weighted AreaUnderROC: 0.6780952380952381
Root mean squared error: 0.4898979485566356
Relative absolute error: 48.0
Root relative squared error: 97.97958971132712
Weighted TruePositiveRate: 0.76
Weighted MatthewsCorrelation: 0.39150479067579536
Weighted FMeasure: 0.7487742816359461
Iteration time: 26.0
Weighted AreaUnderPRC: 0.6869879602356406
Mean absolute error: 0.24
Coverage of cases: 76.0
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 80.0
Weighted Recall: 0.76
Weighted FalsePositiveRate: 0.4038095238095238
Kappa statistic: 0.3839835728952771
Training time: 25.0
		
Iteration: 5
Labeled set size: 180
Unlabelled set size: 320
	
Mean region size: 50.0
Incorrectly Classified Instances: 24.2
Correctly Classified Instances: 75.8
Weighted Precision: 0.7438147056600922
Weighted AreaUnderROC: 0.6633333333333332
Root mean squared error: 0.4919349550499537
Relative absolute error: 48.4
Root relative squared error: 98.38699100999074
Weighted TruePositiveRate: 0.758
Weighted MatthewsCorrelation: 0.37565977068988515
Weighted FMeasure: 0.741433911411291
Iteration time: 41.0
Weighted AreaUnderPRC: 0.6782352552961032
Mean absolute error: 0.242
Coverage of cases: 75.8
Instances selection time: 3.0
Test time: 3.0
Accumulative iteration time: 121.0
Weighted Recall: 0.758
Weighted FalsePositiveRate: 0.43133333333333335
Kappa statistic: 0.36181434599156115
Training time: 38.0
		
Iteration: 6
Labeled set size: 200
Unlabelled set size: 300
	
Mean region size: 50.0
Incorrectly Classified Instances: 25.4
Correctly Classified Instances: 74.6
Weighted Precision: 0.7286667213428469
Weighted AreaUnderROC: 0.6395238095238095
Root mean squared error: 0.5039841267341661
Relative absolute error: 50.8
Root relative squared error: 100.79682534683323
Weighted TruePositiveRate: 0.746
Weighted MatthewsCorrelation: 0.33430437387517453
Weighted FMeasure: 0.7237658003397862
Iteration time: 30.0
Weighted AreaUnderPRC: 0.662069477022335
Mean absolute error: 0.254
Coverage of cases: 74.6
Instances selection time: 3.0
Test time: 3.0
Accumulative iteration time: 151.0
Weighted Recall: 0.746
Weighted FalsePositiveRate: 0.46695238095238095
Kappa statistic: 0.3157327586206896
Training time: 27.0
		
Iteration: 7
Labeled set size: 220
Unlabelled set size: 280
	
Mean region size: 50.0
Incorrectly Classified Instances: 26.8
Correctly Classified Instances: 73.2
Weighted Precision: 0.720334928229665
Weighted AreaUnderROC: 0.58
Root mean squared error: 0.5176871642217914
Relative absolute error: 53.6
Root relative squared error: 103.53743284435828
Weighted TruePositiveRate: 0.732
Weighted MatthewsCorrelation: 0.2588158533262875
Weighted FMeasure: 0.6764063339387583
Iteration time: 30.0
Weighted AreaUnderPRC: 0.6276669856459329
Mean absolute error: 0.268
Coverage of cases: 73.2
Instances selection time: 3.0
Test time: 3.0
Accumulative iteration time: 181.0
Weighted Recall: 0.732
Weighted FalsePositiveRate: 0.572
Kappa statistic: 0.20047732696897383
Training time: 27.0
		
Iteration: 8
Labeled set size: 240
Unlabelled set size: 260
	
Mean region size: 50.0
Incorrectly Classified Instances: 27.4
Correctly Classified Instances: 72.6
Weighted Precision: 0.7137178214931994
Weighted AreaUnderROC: 0.5661904761904762
Root mean squared error: 0.523450093132096
Relative absolute error: 54.800000000000004
Root relative squared error: 104.6900186264192
Weighted TruePositiveRate: 0.726
Weighted MatthewsCorrelation: 0.23174682630743348
Weighted FMeasure: 0.6622557241615197
Iteration time: 44.0
Weighted AreaUnderPRC: 0.6190783842157492
Mean absolute error: 0.274
Coverage of cases: 72.6
Instances selection time: 3.0
Test time: 2.0
Accumulative iteration time: 225.0
Weighted Recall: 0.726
Weighted FalsePositiveRate: 0.5936190476190477
Kappa statistic: 0.16868932038834947
Training time: 41.0
		
Iteration: 9
Labeled set size: 260
Unlabelled set size: 240
	
Mean region size: 50.0
Incorrectly Classified Instances: 28.6
Correctly Classified Instances: 71.4
Weighted Precision: 0.7012526096033402
Weighted AreaUnderROC: 0.5366666666666666
Root mean squared error: 0.5347896782848375
Relative absolute error: 57.199999999999996
Root relative squared error: 106.95793565696749
Weighted TruePositiveRate: 0.714
Weighted MatthewsCorrelation: 0.1675342762078164
Weighted FMeasure: 0.6283749179946247
Iteration time: 30.0
Weighted AreaUnderPRC: 0.6012942240779402
Mean absolute error: 0.286
Coverage of cases: 71.4
Instances selection time: 4.0
Test time: 2.0
Accumulative iteration time: 255.0
Weighted Recall: 0.714
Weighted FalsePositiveRate: 0.6406666666666666
Kappa statistic: 0.09722222222222204
Training time: 26.0
		
Iteration: 10
Labeled set size: 280
Unlabelled set size: 220
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.6
Correctly Classified Instances: 70.4
Weighted Precision: 0.68119918699187
Weighted AreaUnderROC: 0.5123809523809524
Root mean squared error: 0.5440588203494178
Relative absolute error: 59.199999999999996
Root relative squared error: 108.81176406988355
Weighted TruePositiveRate: 0.704
Weighted MatthewsCorrelation: 0.09043491937238224
Weighted FMeasure: 0.595946961724646
Iteration time: 44.0
Weighted AreaUnderPRC: 0.5869174796747967
Mean absolute error: 0.296
Coverage of cases: 70.4
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 299.0
Weighted Recall: 0.704
Weighted FalsePositiveRate: 0.6792380952380952
Kappa statistic: 0.03394255874673618
Training time: 43.0
		
Iteration: 11
Labeled set size: 300
Unlabelled set size: 200
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.2
Correctly Classified Instances: 70.8
Weighted Precision: 0.7445344129554655
Weighted AreaUnderROC: 0.5152380952380953
Root mean squared error: 0.5403702434442518
Relative absolute error: 58.4
Root relative squared error: 108.07404868885035
Weighted TruePositiveRate: 0.708
Weighted MatthewsCorrelation: 0.12826295206503516
Weighted FMeasure: 0.5981407218374044
Iteration time: 31.0
Weighted AreaUnderPRC: 0.5898547908232118
Mean absolute error: 0.292
Coverage of cases: 70.8
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 330.0
Weighted Recall: 0.708
Weighted FalsePositiveRate: 0.6775238095238095
Kappa statistic: 0.04199475065616773
Training time: 30.0
		
Iteration: 12
Labeled set size: 320
Unlabelled set size: 180
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.0
Correctly Classified Instances: 70.0
Weighted Precision: 0.48999999999999994
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.5477225575051661
Relative absolute error: 60.0
Root relative squared error: 109.54451150103321
Weighted TruePositiveRate: 0.7
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.5764705882352942
Iteration time: 39.0
Weighted AreaUnderPRC: 0.58
Mean absolute error: 0.3
Coverage of cases: 70.0
Instances selection time: 3.0
Test time: 3.0
Accumulative iteration time: 369.0
Weighted Recall: 0.7
Weighted FalsePositiveRate: 0.7
Kappa statistic: 0.0
Training time: 36.0
		
Iteration: 13
Labeled set size: 340
Unlabelled set size: 160
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.0
Correctly Classified Instances: 70.0
Weighted Precision: 0.48999999999999994
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.5477225575051661
Relative absolute error: 60.0
Root relative squared error: 109.54451150103321
Weighted TruePositiveRate: 0.7
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.5764705882352942
Iteration time: 119.0
Weighted AreaUnderPRC: 0.58
Mean absolute error: 0.3
Coverage of cases: 70.0
Instances selection time: 3.0
Test time: 2.0
Accumulative iteration time: 488.0
Weighted Recall: 0.7
Weighted FalsePositiveRate: 0.7
Kappa statistic: 0.0
Training time: 116.0
		
Iteration: 14
Labeled set size: 360
Unlabelled set size: 140
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.0
Correctly Classified Instances: 70.0
Weighted Precision: 0.48999999999999994
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.5477225575051661
Relative absolute error: 60.0
Root relative squared error: 109.54451150103321
Weighted TruePositiveRate: 0.7
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.5764705882352942
Iteration time: 115.0
Weighted AreaUnderPRC: 0.58
Mean absolute error: 0.3
Coverage of cases: 70.0
Instances selection time: 3.0
Test time: 3.0
Accumulative iteration time: 603.0
Weighted Recall: 0.7
Weighted FalsePositiveRate: 0.7
Kappa statistic: 0.0
Training time: 112.0
		
Iteration: 15
Labeled set size: 380
Unlabelled set size: 120
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.0
Correctly Classified Instances: 70.0
Weighted Precision: 0.48999999999999994
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.5477225575051661
Relative absolute error: 60.0
Root relative squared error: 109.54451150103321
Weighted TruePositiveRate: 0.7
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.5764705882352942
Iteration time: 128.0
Weighted AreaUnderPRC: 0.58
Mean absolute error: 0.3
Coverage of cases: 70.0
Instances selection time: 3.0
Test time: 3.0
Accumulative iteration time: 731.0
Weighted Recall: 0.7
Weighted FalsePositiveRate: 0.7
Kappa statistic: 0.0
Training time: 125.0
		
Iteration: 16
Labeled set size: 400
Unlabelled set size: 100
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.6
Correctly Classified Instances: 70.4
Weighted Precision: 0.6931174089068826
Weighted AreaUnderROC: 0.5104761904761905
Root mean squared error: 0.5440588203494178
Relative absolute error: 59.199999999999996
Root relative squared error: 108.81176406988355
Weighted TruePositiveRate: 0.704
Weighted MatthewsCorrelation: 0.08818077954471167
Weighted FMeasure: 0.5926358002187386
Iteration time: 105.0
Weighted AreaUnderPRC: 0.5860329284750337
Mean absolute error: 0.296
Coverage of cases: 70.4
Instances selection time: 2.0
Test time: 3.0
Accumulative iteration time: 836.0
Weighted Recall: 0.704
Weighted FalsePositiveRate: 0.683047619047619
Kappa statistic: 0.028871391076115225
Training time: 103.0
		
Iteration: 17
Labeled set size: 420
Unlabelled set size: 80
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.0
Correctly Classified Instances: 71.0
Weighted Precision: 0.6886420833789255
Weighted AreaUnderROC: 0.53
Root mean squared error: 0.5385164807134504
Relative absolute error: 57.99999999999999
Root relative squared error: 107.70329614269008
Weighted TruePositiveRate: 0.71
Weighted MatthewsCorrelation: 0.14380750894152938
Weighted FMeasure: 0.6204615527025968
Iteration time: 75.0
Weighted AreaUnderPRC: 0.5969429259218734
Mean absolute error: 0.29
Coverage of cases: 71.0
Instances selection time: 2.0
Test time: 3.0
Accumulative iteration time: 911.0
Weighted Recall: 0.71
Weighted FalsePositiveRate: 0.65
Kappa statistic: 0.07994923857868019
Training time: 73.0
		
Iteration: 18
Labeled set size: 440
Unlabelled set size: 60
	
Mean region size: 50.0
Incorrectly Classified Instances: 27.2
Correctly Classified Instances: 72.8
Weighted Precision: 0.7084444444444444
Weighted AreaUnderROC: 0.580952380952381
Root mean squared error: 0.521536192416212
Relative absolute error: 54.400000000000006
Root relative squared error: 104.30723848324239
Weighted TruePositiveRate: 0.728
Weighted MatthewsCorrelation: 0.2473136089341247
Weighted FMeasure: 0.677
Iteration time: 121.0
Weighted AreaUnderPRC: 0.6268444444444444
Mean absolute error: 0.272
Coverage of cases: 72.8
Instances selection time: 2.0
Test time: 3.0
Accumulative iteration time: 1032.0
Weighted Recall: 0.728
Weighted FalsePositiveRate: 0.5660952380952381
Kappa statistic: 0.19999999999999987
Training time: 119.0
		
Iteration: 19
Labeled set size: 460
Unlabelled set size: 40
	
Mean region size: 50.0
Incorrectly Classified Instances: 24.2
Correctly Classified Instances: 75.8
Weighted Precision: 0.7465882352941177
Weighted AreaUnderROC: 0.6404761904761905
Root mean squared error: 0.4919349550499537
Relative absolute error: 48.4
Root relative squared error: 98.38699100999074
Weighted TruePositiveRate: 0.758
Weighted MatthewsCorrelation: 0.3605680754977631
Weighted FMeasure: 0.7293763440860216
Iteration time: 98.0
Weighted AreaUnderPRC: 0.6663019607843137
Mean absolute error: 0.242
Coverage of cases: 75.8
Instances selection time: 2.0
Test time: 3.0
Accumulative iteration time: 1130.0
Weighted Recall: 0.758
Weighted FalsePositiveRate: 0.47704761904761905
Kappa statistic: 0.3277777777777778
Training time: 96.0
		
Iteration: 20
Labeled set size: 480
Unlabelled set size: 20
	
Mean region size: 50.0
Incorrectly Classified Instances: 22.4
Correctly Classified Instances: 77.6
Weighted Precision: 0.7653771956543811
Weighted AreaUnderROC: 0.6838095238095238
Root mean squared error: 0.4732863826479693
Relative absolute error: 44.800000000000004
Root relative squared error: 94.65727652959386
Weighted TruePositiveRate: 0.776
Weighted MatthewsCorrelation: 0.42437676015307035
Weighted FMeasure: 0.7602608098833219
Iteration time: 149.0
Weighted AreaUnderPRC: 0.6950200832571835
Mean absolute error: 0.224
Coverage of cases: 77.6
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 1279.0
Weighted Recall: 0.776
Weighted FalsePositiveRate: 0.40838095238095234
Kappa statistic: 0.4080338266384778
Training time: 148.0
		
Time end:Tue Oct 31 11.31.29 EET 2017