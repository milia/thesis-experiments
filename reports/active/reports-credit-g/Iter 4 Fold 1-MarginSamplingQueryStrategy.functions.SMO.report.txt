Tue Oct 31 11.32.48 EET 2017
Dataset: german_credit
Test set size: 500
Initial Labelled set size: 100
Initial Unlabelled set size: 400
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Tue Oct 31 11.32.48 EET 2017
		
Iteration: 1
Labeled set size: 100
Unlabelled set size: 400
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.6
Correctly Classified Instances: 69.4
Weighted Precision: 0.6782071131075689
Weighted AreaUnderROC: 0.608095238095238
Root mean squared error: 0.5531726674375732
Relative absolute error: 61.199999999999996
Root relative squared error: 110.63453348751464
Weighted TruePositiveRate: 0.694
Weighted MatthewsCorrelation: 0.23131506575928193
Weighted FMeasure: 0.6837137260261491
Iteration time: 14.0
Weighted AreaUnderPRC: 0.6366363985259164
Mean absolute error: 0.306
Coverage of cases: 69.4
Instances selection time: 4.0
Test time: 3.0
Accumulative iteration time: 14.0
Weighted Recall: 0.694
Weighted FalsePositiveRate: 0.47780952380952385
Kappa statistic: 0.22883064516129029
Training time: 10.0
		
Iteration: 2
Labeled set size: 120
Unlabelled set size: 380
	
Mean region size: 50.0
Incorrectly Classified Instances: 28.8
Correctly Classified Instances: 71.2
Weighted Precision: 0.6858536585365854
Weighted AreaUnderROC: 0.6
Root mean squared error: 0.5366563145999496
Relative absolute error: 57.599999999999994
Root relative squared error: 107.33126291998991
Weighted TruePositiveRate: 0.712
Weighted MatthewsCorrelation: 0.23855936180684892
Weighted FMeasure: 0.6873684210526315
Iteration time: 16.0
Weighted AreaUnderPRC: 0.633951219512195
Mean absolute error: 0.288
Coverage of cases: 71.2
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 30.0
Weighted Recall: 0.712
Weighted FalsePositiveRate: 0.512
Kappa statistic: 0.22580645161290314
Training time: 14.0
		
Iteration: 3
Labeled set size: 140
Unlabelled set size: 360
	
Mean region size: 50.0
Incorrectly Classified Instances: 27.6
Correctly Classified Instances: 72.4
Weighted Precision: 0.6989077859260416
Weighted AreaUnderROC: 0.5838095238095238
Root mean squared error: 0.5253570214625479
Relative absolute error: 55.2
Root relative squared error: 105.07140429250958
Weighted TruePositiveRate: 0.724
Weighted MatthewsCorrelation: 0.23987107915970635
Weighted FMeasure: 0.6789918414918416
Iteration time: 16.0
Weighted AreaUnderPRC: 0.6272830082696208
Mean absolute error: 0.276
Coverage of cases: 72.4
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 46.0
Weighted Recall: 0.724
Weighted FalsePositiveRate: 0.5563809523809524
Kappa statistic: 0.20323325635103928
Training time: 14.0
		
Iteration: 4
Labeled set size: 160
Unlabelled set size: 340
	
Mean region size: 50.0
Incorrectly Classified Instances: 27.8
Correctly Classified Instances: 72.2
Weighted Precision: 0.6959550561797753
Weighted AreaUnderROC: 0.5785714285714286
Root mean squared error: 0.5272570530585627
Relative absolute error: 55.60000000000001
Root relative squared error: 105.45141061171255
Weighted TruePositiveRate: 0.722
Weighted MatthewsCorrelation: 0.23015104259530228
Weighted FMeasure: 0.6741954287467404
Iteration time: 18.0
Weighted AreaUnderPRC: 0.6241235955056179
Mean absolute error: 0.278
Coverage of cases: 72.2
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 64.0
Weighted Recall: 0.722
Weighted FalsePositiveRate: 0.5648571428571428
Kappa statistic: 0.19186046511627894
Training time: 16.0
		
Iteration: 5
Labeled set size: 180
Unlabelled set size: 320
	
Mean region size: 50.0
Incorrectly Classified Instances: 27.6
Correctly Classified Instances: 72.4
Weighted Precision: 0.7099137931034483
Weighted AreaUnderROC: 0.562857142857143
Root mean squared error: 0.5253570214625479
Relative absolute error: 55.2
Root relative squared error: 105.07140429250958
Weighted TruePositiveRate: 0.724
Weighted MatthewsCorrelation: 0.22287124708010464
Weighted FMeasure: 0.6587461361654909
Iteration time: 16.0
Weighted AreaUnderPRC: 0.6168310344827586
Mean absolute error: 0.276
Coverage of cases: 72.4
Instances selection time: 4.0
Test time: 3.0
Accumulative iteration time: 80.0
Weighted Recall: 0.724
Weighted FalsePositiveRate: 0.5982857142857143
Kappa statistic: 0.1605839416058393
Training time: 12.0
		
Iteration: 6
Labeled set size: 200
Unlabelled set size: 300
	
Mean region size: 50.0
Incorrectly Classified Instances: 28.0
Correctly Classified Instances: 72.0
Weighted Precision: 0.6961946350592638
Weighted AreaUnderROC: 0.5638095238095238
Root mean squared error: 0.5291502622129182
Relative absolute error: 56.00000000000001
Root relative squared error: 105.83005244258364
Weighted TruePositiveRate: 0.72
Weighted MatthewsCorrelation: 0.21083260109757626
Weighted FMeasure: 0.6599628712871288
Iteration time: 19.0
Weighted AreaUnderPRC: 0.6161345809939698
Mean absolute error: 0.28
Coverage of cases: 72.0
Instances selection time: 3.0
Test time: 3.0
Accumulative iteration time: 99.0
Weighted Recall: 0.72
Weighted FalsePositiveRate: 0.5923809523809523
Kappa statistic: 0.16067146282973616
Training time: 16.0
		
Iteration: 7
Labeled set size: 220
Unlabelled set size: 280
	
Mean region size: 50.0
Incorrectly Classified Instances: 28.2
Correctly Classified Instances: 71.8
Weighted Precision: 0.6944778471776313
Weighted AreaUnderROC: 0.5566666666666666
Root mean squared error: 0.5310367218940701
Relative absolute error: 56.39999999999999
Root relative squared error: 106.20734437881403
Weighted TruePositiveRate: 0.718
Weighted MatthewsCorrelation: 0.19840195921283874
Weighted FMeasure: 0.65239457742171
Iteration time: 29.0
Weighted AreaUnderPRC: 0.6120663008580935
Mean absolute error: 0.282
Coverage of cases: 71.8
Instances selection time: 4.0
Test time: 7.0
Accumulative iteration time: 128.0
Weighted Recall: 0.718
Weighted FalsePositiveRate: 0.6046666666666666
Kappa statistic: 0.14441747572815528
Training time: 25.0
		
Iteration: 8
Labeled set size: 240
Unlabelled set size: 260
	
Mean region size: 50.0
Incorrectly Classified Instances: 27.2
Correctly Classified Instances: 72.8
Weighted Precision: 0.7256677350427351
Weighted AreaUnderROC: 0.5638095238095238
Root mean squared error: 0.521536192416212
Relative absolute error: 54.400000000000006
Root relative squared error: 104.30723848324239
Weighted TruePositiveRate: 0.728
Weighted MatthewsCorrelation: 0.23894460831859304
Weighted FMeasure: 0.6594427577312663
Iteration time: 87.0
Weighted AreaUnderPRC: 0.6187898504273505
Mean absolute error: 0.272
Coverage of cases: 72.8
Instances selection time: 9.0
Test time: 3.0
Accumulative iteration time: 215.0
Weighted Recall: 0.728
Weighted FalsePositiveRate: 0.6003809523809523
Kappa statistic: 0.16461916461916457
Training time: 78.0
		
Iteration: 9
Labeled set size: 260
Unlabelled set size: 240
	
Mean region size: 50.0
Incorrectly Classified Instances: 28.2
Correctly Classified Instances: 71.8
Weighted Precision: 0.7169997017596184
Weighted AreaUnderROC: 0.5414285714285716
Root mean squared error: 0.5310367218940701
Relative absolute error: 56.39999999999999
Root relative squared error: 106.20734437881403
Weighted TruePositiveRate: 0.718
Weighted MatthewsCorrelation: 0.18929197441662374
Weighted FMeasure: 0.6335724715891055
Iteration time: 39.0
Weighted AreaUnderPRC: 0.6049246048314942
Mean absolute error: 0.282
Coverage of cases: 71.8
Instances selection time: 8.0
Test time: 10.0
Accumulative iteration time: 254.0
Weighted Recall: 0.718
Weighted FalsePositiveRate: 0.6351428571428571
Kappa statistic: 0.10984848484848468
Training time: 31.0
		
Iteration: 10
Labeled set size: 280
Unlabelled set size: 220
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.0
Correctly Classified Instances: 71.0
Weighted Precision: 0.7050229031748538
Weighted AreaUnderROC: 0.5242857142857142
Root mean squared error: 0.5385164807134504
Relative absolute error: 57.99999999999999
Root relative squared error: 107.70329614269008
Weighted TruePositiveRate: 0.71
Weighted MatthewsCorrelation: 0.13986993465016023
Weighted FMeasure: 0.6118624066377876
Iteration time: 97.0
Weighted AreaUnderPRC: 0.5943083557099984
Mean absolute error: 0.29
Coverage of cases: 71.0
Instances selection time: 6.0
Test time: 9.0
Accumulative iteration time: 351.0
Weighted Recall: 0.71
Weighted FalsePositiveRate: 0.6614285714285715
Kappa statistic: 0.065721649484536
Training time: 91.0
		
Iteration: 11
Labeled set size: 300
Unlabelled set size: 200
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.4
Correctly Classified Instances: 70.6
Weighted Precision: 0.6947046843177189
Weighted AreaUnderROC: 0.5157142857142857
Root mean squared error: 0.5422176684690383
Relative absolute error: 58.8
Root relative squared error: 108.44353369380767
Weighted TruePositiveRate: 0.706
Weighted MatthewsCorrelation: 0.10832839077674206
Weighted FMeasure: 0.6002871693626187
Iteration time: 42.0
Weighted AreaUnderPRC: 0.5890643584521386
Mean absolute error: 0.294
Coverage of cases: 70.6
Instances selection time: 5.0
Test time: 9.0
Accumulative iteration time: 393.0
Weighted Recall: 0.706
Weighted FalsePositiveRate: 0.6745714285714286
Kappa statistic: 0.042968749999999965
Training time: 37.0
		
Iteration: 12
Labeled set size: 320
Unlabelled set size: 180
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.4
Correctly Classified Instances: 70.6
Weighted Precision: 0.7084033613445379
Weighted AreaUnderROC: 0.5138095238095238
Root mean squared error: 0.5422176684690383
Relative absolute error: 58.8
Root relative squared error: 108.44353369380767
Weighted TruePositiveRate: 0.706
Weighted MatthewsCorrelation: 0.10772484326627123
Weighted FMeasure: 0.5970442233152753
Iteration time: 44.0
Weighted AreaUnderPRC: 0.5882369747899161
Mean absolute error: 0.294
Coverage of cases: 70.6
Instances selection time: 3.0
Test time: 3.0
Accumulative iteration time: 437.0
Weighted Recall: 0.706
Weighted FalsePositiveRate: 0.6783809523809523
Kappa statistic: 0.03795811518324589
Training time: 41.0
		
Iteration: 13
Labeled set size: 340
Unlabelled set size: 160
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.8
Correctly Classified Instances: 70.2
Weighted Precision: 0.6721212121212121
Weighted AreaUnderROC: 0.5071428571428571
Root mean squared error: 0.5458937625582472
Relative absolute error: 59.599999999999994
Root relative squared error: 109.17875251164944
Weighted TruePositiveRate: 0.702
Weighted MatthewsCorrelation: 0.06579516949597691
Weighted FMeasure: 0.5881809505630845
Iteration time: 29.0
Weighted AreaUnderPRC: 0.5839090909090909
Mean absolute error: 0.298
Coverage of cases: 70.2
Instances selection time: 3.0
Test time: 3.0
Accumulative iteration time: 466.0
Weighted Recall: 0.702
Weighted FalsePositiveRate: 0.6877142857142856
Kappa statistic: 0.01973684210526317
Training time: 26.0
		
Iteration: 14
Labeled set size: 360
Unlabelled set size: 140
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.8
Correctly Classified Instances: 70.2
Weighted Precision: 0.7909819639278558
Weighted AreaUnderROC: 0.5033333333333333
Root mean squared error: 0.5458937625582472
Relative absolute error: 59.599999999999994
Root relative squared error: 109.17875251164944
Weighted TruePositiveRate: 0.702
Weighted MatthewsCorrelation: 0.0683814207520933
Weighted FMeasure: 0.5811230976840693
Iteration time: 51.0
Weighted AreaUnderPRC: 0.5823819639278557
Mean absolute error: 0.298
Coverage of cases: 70.2
Instances selection time: 2.0
Test time: 3.0
Accumulative iteration time: 517.0
Weighted Recall: 0.702
Weighted FalsePositiveRate: 0.6953333333333332
Kappa statistic: 0.009308510638297587
Training time: 49.0
		
Iteration: 15
Labeled set size: 380
Unlabelled set size: 120
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.0
Correctly Classified Instances: 70.0
Weighted Precision: 0.48999999999999994
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.5477225575051661
Relative absolute error: 60.0
Root relative squared error: 109.54451150103321
Weighted TruePositiveRate: 0.7
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.5764705882352942
Iteration time: 58.0
Weighted AreaUnderPRC: 0.58
Mean absolute error: 0.3
Coverage of cases: 70.0
Instances selection time: 3.0
Test time: 3.0
Accumulative iteration time: 575.0
Weighted Recall: 0.7
Weighted FalsePositiveRate: 0.7
Kappa statistic: 0.0
Training time: 55.0
		
Iteration: 16
Labeled set size: 400
Unlabelled set size: 100
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.8
Correctly Classified Instances: 70.2
Weighted Precision: 0.6721212121212121
Weighted AreaUnderROC: 0.5071428571428571
Root mean squared error: 0.5458937625582472
Relative absolute error: 59.599999999999994
Root relative squared error: 109.17875251164944
Weighted TruePositiveRate: 0.702
Weighted MatthewsCorrelation: 0.06579516949597691
Weighted FMeasure: 0.5881809505630845
Iteration time: 43.0
Weighted AreaUnderPRC: 0.5839090909090909
Mean absolute error: 0.298
Coverage of cases: 70.2
Instances selection time: 3.0
Test time: 3.0
Accumulative iteration time: 618.0
Weighted Recall: 0.702
Weighted FalsePositiveRate: 0.6877142857142856
Kappa statistic: 0.01973684210526317
Training time: 40.0
		
Iteration: 17
Labeled set size: 420
Unlabelled set size: 80
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.0
Correctly Classified Instances: 71.0
Weighted Precision: 0.7294636795655126
Weighted AreaUnderROC: 0.5204761904761905
Root mean squared error: 0.5385164807134504
Relative absolute error: 57.99999999999999
Root relative squared error: 107.70329614269008
Weighted TruePositiveRate: 0.71
Weighted MatthewsCorrelation: 0.14115517586060333
Weighted FMeasure: 0.6057254391672088
Iteration time: 70.0
Weighted AreaUnderPRC: 0.59278420457117
Mean absolute error: 0.29
Coverage of cases: 71.0
Instances selection time: 3.0
Test time: 2.0
Accumulative iteration time: 688.0
Weighted Recall: 0.71
Weighted FalsePositiveRate: 0.669047619047619
Kappa statistic: 0.05598958333333331
Training time: 67.0
		
Iteration: 18
Labeled set size: 440
Unlabelled set size: 60
	
Mean region size: 50.0
Incorrectly Classified Instances: 26.2
Correctly Classified Instances: 73.8
Weighted Precision: 0.728118919408118
Weighted AreaUnderROC: 0.5919047619047619
Root mean squared error: 0.5118593556827891
Relative absolute error: 52.400000000000006
Root relative squared error: 102.37187113655781
Weighted TruePositiveRate: 0.738
Weighted MatthewsCorrelation: 0.28330963898814837
Weighted FMeasure: 0.6880306651861053
Iteration time: 84.0
Weighted AreaUnderPRC: 0.6354555138241549
Mean absolute error: 0.262
Coverage of cases: 73.8
Instances selection time: 3.0
Test time: 2.0
Accumulative iteration time: 772.0
Weighted Recall: 0.738
Weighted FalsePositiveRate: 0.5541904761904762
Kappa statistic: 0.22759433962264136
Training time: 81.0
		
Iteration: 19
Labeled set size: 460
Unlabelled set size: 40
	
Mean region size: 50.0
Incorrectly Classified Instances: 25.6
Correctly Classified Instances: 74.4
Weighted Precision: 0.7278405315614617
Weighted AreaUnderROC: 0.6190476190476191
Root mean squared error: 0.5059644256269407
Relative absolute error: 51.2
Root relative squared error: 101.19288512538813
Weighted TruePositiveRate: 0.744
Weighted MatthewsCorrelation: 0.31444675225915136
Weighted FMeasure: 0.7105827505827506
Iteration time: 125.0
Weighted AreaUnderPRC: 0.6507641196013288
Mean absolute error: 0.256
Coverage of cases: 74.4
Instances selection time: 2.0
Test time: 3.0
Accumulative iteration time: 897.0
Weighted Recall: 0.744
Weighted FalsePositiveRate: 0.505904761904762
Kappa statistic: 0.28089887640449435
Training time: 123.0
		
Iteration: 20
Labeled set size: 480
Unlabelled set size: 20
	
Mean region size: 50.0
Incorrectly Classified Instances: 24.4
Correctly Classified Instances: 75.6
Weighted Precision: 0.7412404092071612
Weighted AreaUnderROC: 0.6542857142857142
Root mean squared error: 0.4939635614091388
Relative absolute error: 48.8
Root relative squared error: 98.79271228182776
Weighted TruePositiveRate: 0.756
Weighted MatthewsCorrelation: 0.3649315129364528
Weighted FMeasure: 0.7360954229267973
Iteration time: 162.0
Weighted AreaUnderPRC: 0.6727667519181586
Mean absolute error: 0.244
Coverage of cases: 75.6
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 1059.0
Weighted Recall: 0.756
Weighted FalsePositiveRate: 0.44742857142857145
Kappa statistic: 0.3468950749464669
Training time: 161.0
		
Time end:Tue Oct 31 11.32.50 EET 2017