Tue Oct 31 11.33.10 EET 2017
Dataset: german_credit
Test set size: 500
Initial Labelled set size: 100
Initial Unlabelled set size: 400
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Tue Oct 31 11.33.10 EET 2017
		
Iteration: 1
Labeled set size: 100
Unlabelled set size: 400
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.2
Correctly Classified Instances: 65.8
Weighted Precision: 0.6470242363401691
Weighted AreaUnderROC: 0.5766666666666668
Root mean squared error: 0.5791288583080483
Relative absolute error: 69.01960784313728
Root relative squared error: 115.82577166160965
Weighted TruePositiveRate: 0.658
Weighted MatthewsCorrelation: 0.1590220603549262
Weighted FMeasure: 0.6517823084997857
Iteration time: 14.0
Weighted AreaUnderPRC: 0.6175104832927004
Mean absolute error: 0.3450980392156864
Coverage of cases: 65.8
Instances selection time: 14.0
Test time: 18.0
Accumulative iteration time: 14.0
Weighted Recall: 0.658
Weighted FalsePositiveRate: 0.5046666666666667
Kappa statistic: 0.1584645669291339
Training time: 0.0
		
Iteration: 2
Labeled set size: 120
Unlabelled set size: 380
	
Mean region size: 50.0
Incorrectly Classified Instances: 32.2
Correctly Classified Instances: 67.8
Weighted Precision: 0.6551865490168832
Weighted AreaUnderROC: 0.5795238095238096
Root mean squared error: 0.5628396731891487
Relative absolute error: 64.98360655737716
Root relative squared error: 112.56793463782975
Weighted TruePositiveRate: 0.678
Weighted MatthewsCorrelation: 0.1753761507529978
Weighted FMeasure: 0.6624391457857
Iteration time: 16.0
Weighted AreaUnderPRC: 0.6198589267931172
Mean absolute error: 0.3249180327868858
Coverage of cases: 67.8
Instances selection time: 16.0
Test time: 22.0
Accumulative iteration time: 30.0
Weighted Recall: 0.678
Weighted FalsePositiveRate: 0.5189523809523809
Kappa statistic: 0.17181069958847756
Training time: 0.0
		
Iteration: 3
Labeled set size: 140
Unlabelled set size: 360
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.6
Correctly Classified Instances: 70.4
Weighted Precision: 0.6757723577235772
Weighted AreaUnderROC: 0.5904761904761905
Root mean squared error: 0.5402597331378194
Relative absolute error: 59.77464788732413
Root relative squared error: 108.05194662756388
Weighted TruePositiveRate: 0.704
Weighted MatthewsCorrelation: 0.21583942258714905
Weighted FMeasure: 0.6786842105263159
Iteration time: 17.0
Weighted AreaUnderPRC: 0.6277831978319783
Mean absolute error: 0.29887323943662064
Coverage of cases: 70.4
Instances selection time: 17.0
Test time: 24.0
Accumulative iteration time: 47.0
Weighted Recall: 0.704
Weighted FalsePositiveRate: 0.523047619047619
Kappa statistic: 0.20430107526881708
Training time: 0.0
		
Iteration: 4
Labeled set size: 160
Unlabelled set size: 340
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.0
Correctly Classified Instances: 71.0
Weighted Precision: 0.6805003093785911
Weighted AreaUnderROC: 0.589047619047619
Root mean squared error: 0.5352175791526179
Relative absolute error: 58.518518518518434
Root relative squared error: 107.04351583052359
Weighted TruePositiveRate: 0.71
Weighted MatthewsCorrelation: 0.2215044411621691
Weighted FMeasure: 0.679698714809248
Iteration time: 18.0
Weighted AreaUnderPRC: 0.6277034856654586
Mean absolute error: 0.2925925925925922
Coverage of cases: 71.0
Instances selection time: 18.0
Test time: 27.0
Accumulative iteration time: 65.0
Weighted Recall: 0.71
Weighted FalsePositiveRate: 0.5319047619047619
Kappa statistic: 0.20504385964912275
Training time: 0.0
		
Iteration: 5
Labeled set size: 180
Unlabelled set size: 320
	
Mean region size: 50.0
Incorrectly Classified Instances: 28.4
Correctly Classified Instances: 71.6
Weighted Precision: 0.6867601246105919
Weighted AreaUnderROC: 0.5876190476190476
Root mean squared error: 0.5300087833892475
Relative absolute error: 57.2747252747253
Root relative squared error: 106.0017566778495
Weighted TruePositiveRate: 0.716
Weighted MatthewsCorrelation: 0.22872837414518352
Weighted FMeasure: 0.6803446119641493
Iteration time: 19.0
Weighted AreaUnderPRC: 0.6277865005192108
Mean absolute error: 0.2863736263736265
Coverage of cases: 71.6
Instances selection time: 19.0
Test time: 29.0
Accumulative iteration time: 84.0
Weighted Recall: 0.716
Weighted FalsePositiveRate: 0.5407619047619047
Kappa statistic: 0.20581655480984337
Training time: 0.0
		
Iteration: 6
Labeled set size: 200
Unlabelled set size: 300
	
Mean region size: 50.0
Incorrectly Classified Instances: 28.2
Correctly Classified Instances: 71.8
Weighted Precision: 0.6892867951175226
Weighted AreaUnderROC: 0.5871428571428572
Root mean squared error: 0.5284244772844219
Relative absolute error: 56.831683168316985
Root relative squared error: 105.68489545688438
Weighted TruePositiveRate: 0.718
Weighted MatthewsCorrelation: 0.2315678491835376
Weighted FMeasure: 0.6804728746075457
Iteration time: 19.0
Weighted AreaUnderPRC: 0.6278609704428528
Mean absolute error: 0.2841584158415849
Coverage of cases: 71.8
Instances selection time: 19.0
Test time: 32.0
Accumulative iteration time: 103.0
Weighted Recall: 0.718
Weighted FalsePositiveRate: 0.5437142857142857
Kappa statistic: 0.20608108108108092
Training time: 0.0
		
Iteration: 7
Labeled set size: 220
Unlabelled set size: 280
	
Mean region size: 50.0
Incorrectly Classified Instances: 28.2
Correctly Classified Instances: 71.8
Weighted Precision: 0.6887598491355166
Weighted AreaUnderROC: 0.5795238095238096
Root mean squared error: 0.5286584436290506
Relative absolute error: 56.79279279279265
Root relative squared error: 105.73168872581012
Weighted TruePositiveRate: 0.718
Weighted MatthewsCorrelation: 0.22269457946993887
Weighted FMeasure: 0.6744310093164902
Iteration time: 20.0
Weighted AreaUnderPRC: 0.6238145039023115
Mean absolute error: 0.2839639639639633
Coverage of cases: 71.8
Instances selection time: 19.0
Test time: 34.0
Accumulative iteration time: 123.0
Weighted Recall: 0.718
Weighted FalsePositiveRate: 0.5589523809523811
Kappa statistic: 0.19151376146788981
Training time: 1.0
		
Iteration: 8
Labeled set size: 240
Unlabelled set size: 260
	
Mean region size: 50.0
Incorrectly Classified Instances: 28.8
Correctly Classified Instances: 71.2
Weighted Precision: 0.6786394133250117
Weighted AreaUnderROC: 0.5695238095238095
Root mean squared error: 0.534450100614761
Relative absolute error: 57.95041322314069
Root relative squared error: 106.8900201229522
Weighted TruePositiveRate: 0.712
Weighted MatthewsCorrelation: 0.19898396339384733
Weighted FMeasure: 0.665034965034965
Iteration time: 19.0
Weighted AreaUnderPRC: 0.617514869714464
Mean absolute error: 0.2897520661157035
Coverage of cases: 71.2
Instances selection time: 19.0
Test time: 37.0
Accumulative iteration time: 142.0
Weighted Recall: 0.712
Weighted FalsePositiveRate: 0.572952380952381
Kappa statistic: 0.16859122401847576
Training time: 0.0
		
Iteration: 9
Labeled set size: 260
Unlabelled set size: 240
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.2
Correctly Classified Instances: 70.8
Weighted Precision: 0.6709134615384615
Weighted AreaUnderROC: 0.559047619047619
Root mean squared error: 0.5383173416136948
Relative absolute error: 58.71755725190828
Root relative squared error: 107.66346832273896
Weighted TruePositiveRate: 0.708
Weighted MatthewsCorrelation: 0.17728474171277506
Weighted FMeasure: 0.6550981413930868
Iteration time: 20.0
Weighted AreaUnderPRC: 0.6114002747252747
Mean absolute error: 0.2935877862595414
Coverage of cases: 70.8
Instances selection time: 20.0
Test time: 39.0
Accumulative iteration time: 162.0
Weighted Recall: 0.708
Weighted FalsePositiveRate: 0.5899047619047619
Kappa statistic: 0.14519906323185006
Training time: 0.0
		
Iteration: 10
Labeled set size: 280
Unlabelled set size: 220
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.2
Correctly Classified Instances: 70.8
Weighted Precision: 0.6706666666666667
Weighted AreaUnderROC: 0.5571428571428572
Root mean squared error: 0.5384623040049917
Relative absolute error: 58.69503546099273
Root relative squared error: 107.69246080099835
Weighted TruePositiveRate: 0.708
Weighted MatthewsCorrelation: 0.1745743121887939
Weighted FMeasure: 0.65325
Iteration time: 19.0
Weighted AreaUnderPRC: 0.6103999999999999
Mean absolute error: 0.29347517730496364
Coverage of cases: 70.8
Instances selection time: 19.0
Test time: 42.0
Accumulative iteration time: 181.0
Weighted Recall: 0.708
Weighted FalsePositiveRate: 0.5937142857142856
Kappa statistic: 0.1411764705882351
Training time: 0.0
		
Iteration: 11
Labeled set size: 300
Unlabelled set size: 200
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.2
Correctly Classified Instances: 70.8
Weighted Precision: 0.6704646017699115
Weighted AreaUnderROC: 0.5552380952380952
Root mean squared error: 0.5385881446865942
Relative absolute error: 58.6754966887418
Root relative squared error: 107.71762893731885
Weighted TruePositiveRate: 0.708
Weighted MatthewsCorrelation: 0.171853549179238
Weighted FMeasure: 0.6513564573414947
Iteration time: 17.0
Weighted AreaUnderPRC: 0.6094020648967552
Mean absolute error: 0.293377483443709
Coverage of cases: 70.8
Instances selection time: 17.0
Test time: 45.0
Accumulative iteration time: 198.0
Weighted Recall: 0.708
Weighted FalsePositiveRate: 0.5975238095238096
Kappa statistic: 0.1371158392434988
Training time: 0.0
		
Iteration: 12
Labeled set size: 320
Unlabelled set size: 180
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.0
Correctly Classified Instances: 71.0
Weighted Precision: 0.6743589743589744
Weighted AreaUnderROC: 0.5547619047619048
Root mean squared error: 0.5368504470088569
Relative absolute error: 58.26086956521753
Root relative squared error: 107.37008940177137
Weighted TruePositiveRate: 0.71
Weighted MatthewsCorrelation: 0.17537845416747636
Weighted FMeasure: 0.6508361204013378
Iteration time: 17.0
Weighted AreaUnderPRC: 0.6094590964590963
Mean absolute error: 0.29130434782608766
Coverage of cases: 71.0
Instances selection time: 17.0
Test time: 47.0
Accumulative iteration time: 215.0
Weighted Recall: 0.71
Weighted FalsePositiveRate: 0.6004761904761905
Kappa statistic: 0.13690476190476172
Training time: 0.0
		
Iteration: 13
Labeled set size: 340
Unlabelled set size: 160
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.2
Correctly Classified Instances: 70.8
Weighted Precision: 0.6701809107922645
Weighted AreaUnderROC: 0.5495238095238095
Root mean squared error: 0.5387958308116567
Relative absolute error: 58.643274853801266
Root relative squared error: 107.75916616233134
Weighted TruePositiveRate: 0.708
Weighted MatthewsCorrelation: 0.16363127249364126
Weighted FMeasure: 0.6453898514851485
Iteration time: 17.0
Weighted AreaUnderPRC: 0.6064227906009565
Mean absolute error: 0.2932163742690063
Coverage of cases: 70.8
Instances selection time: 17.0
Test time: 50.0
Accumulative iteration time: 232.0
Weighted Recall: 0.708
Weighted FalsePositiveRate: 0.6089523809523809
Kappa statistic: 0.12470023980815338
Training time: 0.0
		
Iteration: 14
Labeled set size: 360
Unlabelled set size: 140
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.4
Correctly Classified Instances: 70.6
Weighted Precision: 0.6657686380785376
Weighted AreaUnderROC: 0.5461904761904762
Root mean squared error: 0.5407248113712083
Relative absolute error: 59.027624309392316
Root relative squared error: 108.14496227424166
Weighted TruePositiveRate: 0.706
Weighted MatthewsCorrelation: 0.15429925023841234
Weighted FMeasure: 0.641915880894906
Iteration time: 15.0
Weighted AreaUnderPRC: 0.6043997343110686
Mean absolute error: 0.29513812154696156
Coverage of cases: 70.6
Instances selection time: 15.0
Test time: 51.0
Accumulative iteration time: 247.0
Weighted Recall: 0.706
Weighted FalsePositiveRate: 0.6136190476190477
Kappa statistic: 0.1165865384615383
Training time: 0.0
		
Iteration: 15
Labeled set size: 380
Unlabelled set size: 120
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.4
Correctly Classified Instances: 70.6
Weighted Precision: 0.6656599365926914
Weighted AreaUnderROC: 0.5442857142857143
Root mean squared error: 0.5408027236124234
Relative absolute error: 59.01570680628272
Root relative squared error: 108.16054472248469
Weighted TruePositiveRate: 0.706
Weighted MatthewsCorrelation: 0.15135282622168603
Weighted FMeasure: 0.6397862720920674
Iteration time: 14.0
Weighted AreaUnderPRC: 0.6034106123811113
Mean absolute error: 0.2950785340314136
Coverage of cases: 70.6
Instances selection time: 14.0
Test time: 55.0
Accumulative iteration time: 261.0
Weighted Recall: 0.706
Weighted FalsePositiveRate: 0.6174285714285714
Kappa statistic: 0.11231884057971016
Training time: 0.0
		
Iteration: 16
Labeled set size: 400
Unlabelled set size: 100
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.4
Correctly Classified Instances: 69.6
Weighted Precision: 0.6533804684047724
Weighted AreaUnderROC: 0.5542857142857143
Root mean squared error: 0.5499943182931065
Relative absolute error: 60.99502487562164
Root relative squared error: 109.9988636586213
Weighted TruePositiveRate: 0.696
Weighted MatthewsCorrelation: 0.15096014488300225
Weighted FMeasure: 0.6498802796666986
Iteration time: 19.0
Weighted AreaUnderPRC: 0.6075856827220504
Mean absolute error: 0.3049751243781082
Coverage of cases: 69.6
Instances selection time: 19.0
Test time: 87.0
Accumulative iteration time: 280.0
Weighted Recall: 0.696
Weighted FalsePositiveRate: 0.5874285714285714
Kappa statistic: 0.13043478260869557
Training time: 0.0
		
Iteration: 17
Labeled set size: 420
Unlabelled set size: 80
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.8
Correctly Classified Instances: 69.2
Weighted Precision: 0.6529247557416571
Weighted AreaUnderROC: 0.560952380952381
Root mean squared error: 0.5536658736889857
Relative absolute error: 61.78199052132697
Root relative squared error: 110.73317473779713
Weighted TruePositiveRate: 0.692
Weighted MatthewsCorrelation: 0.15731845146590523
Weighted FMeasure: 0.654832474226804
Iteration time: 15.0
Weighted AreaUnderPRC: 0.6107973099860423
Mean absolute error: 0.30890995260663484
Coverage of cases: 69.2
Instances selection time: 15.0
Test time: 67.0
Accumulative iteration time: 295.0
Weighted Recall: 0.692
Weighted FalsePositiveRate: 0.5700952380952382
Kappa statistic: 0.14253897550111336
Training time: 0.0
		
Iteration: 18
Labeled set size: 440
Unlabelled set size: 60
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.8
Correctly Classified Instances: 69.2
Weighted Precision: 0.661615515771526
Weighted AreaUnderROC: 0.5780952380952381
Root mean squared error: 0.5537250703119229
Relative absolute error: 61.77375565610872
Root relative squared error: 110.74501406238457
Weighted TruePositiveRate: 0.692
Weighted MatthewsCorrelation: 0.18471842012832798
Weighted FMeasure: 0.6668745502518589
Iteration time: 9.0
Weighted AreaUnderPRC: 0.6199653878942882
Mean absolute error: 0.3088687782805436
Coverage of cases: 69.2
Instances selection time: 9.0
Test time: 63.0
Accumulative iteration time: 304.0
Weighted Recall: 0.692
Weighted FalsePositiveRate: 0.5358095238095238
Kappa statistic: 0.1755888650963597
Training time: 0.0
		
Iteration: 19
Labeled set size: 460
Unlabelled set size: 40
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.8
Correctly Classified Instances: 69.2
Weighted Precision: 0.6724570493591491
Weighted AreaUnderROC: 0.599047619047619
Root mean squared error: 0.5537791542991553
Relative absolute error: 61.7662337662338
Root relative squared error: 110.75583085983105
Weighted TruePositiveRate: 0.692
Weighted MatthewsCorrelation: 0.21637526341415644
Weighted FMeasure: 0.6785326086956521
Iteration time: 6.0
Weighted AreaUnderPRC: 0.6314318334696847
Mean absolute error: 0.308831168831169
Coverage of cases: 69.2
Instances selection time: 6.0
Test time: 65.0
Accumulative iteration time: 310.0
Weighted Recall: 0.692
Weighted FalsePositiveRate: 0.49390476190476185
Kappa statistic: 0.21267893660531684
Training time: 0.0
		
Iteration: 20
Labeled set size: 480
Unlabelled set size: 20
	
Mean region size: 50.0
Incorrectly Classified Instances: 31.8
Correctly Classified Instances: 68.2
Weighted Precision: 0.6652281994810177
Weighted AreaUnderROC: 0.5938095238095238
Root mean squared error: 0.5627475475370038
Relative absolute error: 63.75103734439836
Root relative squared error: 112.54950950740077
Weighted TruePositiveRate: 0.682
Weighted MatthewsCorrelation: 0.2007447927514473
Weighted FMeasure: 0.6713103427330569
Iteration time: 3.0
Weighted AreaUnderPRC: 0.6278626790815325
Mean absolute error: 0.3187551867219918
Coverage of cases: 68.2
Instances selection time: 3.0
Test time: 69.0
Accumulative iteration time: 313.0
Weighted Recall: 0.682
Weighted FalsePositiveRate: 0.4943809523809524
Kappa statistic: 0.19858870967741957
Training time: 0.0
		
Time end:Tue Oct 31 11.33.12 EET 2017