Tue Oct 31 11.31.38 EET 2017
Dataset: german_credit
Test set size: 500
Initial Labelled set size: 100
Initial Unlabelled set size: 400
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Tue Oct 31 11.31.38 EET 2017
		
Iteration: 1
Labeled set size: 100
Unlabelled set size: 400
	
Mean region size: 50.0
Incorrectly Classified Instances: 32.6
Correctly Classified Instances: 67.4
Weighted Precision: 0.6721454587500723
Weighted AreaUnderROC: 0.6090476190476191
Root mean squared error: 0.5709640969448079
Relative absolute error: 65.2
Root relative squared error: 114.19281938896158
Weighted TruePositiveRate: 0.674
Weighted MatthewsCorrelation: 0.21937130303557645
Weighted FMeasure: 0.6730491256807047
Iteration time: 24.0
Weighted AreaUnderPRC: 0.6359059914050607
Mean absolute error: 0.326
Coverage of cases: 67.4
Instances selection time: 4.0
Test time: 3.0
Accumulative iteration time: 24.0
Weighted Recall: 0.674
Weighted FalsePositiveRate: 0.45590476190476187
Kappa statistic: 0.21934865900383146
Training time: 20.0
		
Iteration: 2
Labeled set size: 120
Unlabelled set size: 380
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.2
Correctly Classified Instances: 69.8
Weighted Precision: 0.6721756215521802
Weighted AreaUnderROC: 0.5919047619047619
Root mean squared error: 0.5495452665613635
Relative absolute error: 60.4
Root relative squared error: 109.9090533122727
Weighted TruePositiveRate: 0.698
Weighted MatthewsCorrelation: 0.21137707213745718
Weighted FMeasure: 0.6773266167198755
Iteration time: 20.0
Weighted AreaUnderPRC: 0.6279828559913347
Mean absolute error: 0.302
Coverage of cases: 69.8
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 44.0
Weighted Recall: 0.698
Weighted FalsePositiveRate: 0.5141904761904762
Kappa statistic: 0.2035864978902952
Training time: 18.0
		
Iteration: 3
Labeled set size: 140
Unlabelled set size: 360
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.8
Correctly Classified Instances: 69.2
Weighted Precision: 0.6635519801980198
Weighted AreaUnderROC: 0.5819047619047619
Root mean squared error: 0.5549774770204643
Relative absolute error: 61.6
Root relative squared error: 110.99549540409286
Weighted TruePositiveRate: 0.692
Weighted MatthewsCorrelation: 0.19058666056327586
Weighted FMeasure: 0.6692242996700524
Iteration time: 23.0
Weighted AreaUnderPRC: 0.6220278877887789
Mean absolute error: 0.308
Coverage of cases: 69.2
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 67.0
Weighted Recall: 0.692
Weighted FalsePositiveRate: 0.5281904761904762
Kappa statistic: 0.18259023354564746
Training time: 21.0
		
Iteration: 4
Labeled set size: 160
Unlabelled set size: 340
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.4
Correctly Classified Instances: 70.6
Weighted Precision: 0.6786614363631479
Weighted AreaUnderROC: 0.5938095238095238
Root mean squared error: 0.5422176684690383
Relative absolute error: 58.8
Root relative squared error: 108.44353369380767
Weighted TruePositiveRate: 0.706
Weighted MatthewsCorrelation: 0.2228302916132909
Weighted FMeasure: 0.6814393256031358
Iteration time: 24.0
Weighted AreaUnderPRC: 0.6298272011606976
Mean absolute error: 0.294
Coverage of cases: 70.6
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 91.0
Weighted Recall: 0.706
Weighted FalsePositiveRate: 0.5183809523809523
Kappa statistic: 0.21137339055793986
Training time: 22.0
		
Iteration: 5
Labeled set size: 180
Unlabelled set size: 320
	
Mean region size: 50.0
Incorrectly Classified Instances: 26.4
Correctly Classified Instances: 73.6
Weighted Precision: 0.7191761585270714
Weighted AreaUnderROC: 0.5980952380952381
Root mean squared error: 0.5138093031466052
Relative absolute error: 52.800000000000004
Root relative squared error: 102.76186062932105
Weighted TruePositiveRate: 0.736
Weighted MatthewsCorrelation: 0.2807581949255654
Weighted FMeasure: 0.692948717948718
Iteration time: 32.0
Weighted AreaUnderPRC: 0.637753284443751
Mean absolute error: 0.264
Coverage of cases: 73.6
Instances selection time: 4.0
Test time: 2.0
Accumulative iteration time: 123.0
Weighted Recall: 0.736
Weighted FalsePositiveRate: 0.5398095238095239
Kappa statistic: 0.23787528868360283
Training time: 28.0
		
Iteration: 6
Labeled set size: 200
Unlabelled set size: 300
	
Mean region size: 50.0
Incorrectly Classified Instances: 26.6
Correctly Classified Instances: 73.4
Weighted Precision: 0.7191536748329621
Weighted AreaUnderROC: 0.589047619047619
Root mean squared error: 0.5157518783291051
Relative absolute error: 53.2
Root relative squared error: 103.15037566582102
Weighted TruePositiveRate: 0.734
Weighted MatthewsCorrelation: 0.26966445226783675
Weighted FMeasure: 0.6849718864999158
Iteration time: 45.0
Weighted AreaUnderPRC: 0.6326709725315516
Mean absolute error: 0.266
Coverage of cases: 73.4
Instances selection time: 3.0
Test time: 3.0
Accumulative iteration time: 168.0
Weighted Recall: 0.734
Weighted FalsePositiveRate: 0.5559047619047619
Kappa statistic: 0.21948356807511732
Training time: 42.0
		
Iteration: 7
Labeled set size: 220
Unlabelled set size: 280
	
Mean region size: 50.0
Incorrectly Classified Instances: 28.0
Correctly Classified Instances: 72.0
Weighted Precision: 0.6952751196172249
Weighted AreaUnderROC: 0.5657142857142857
Root mean squared error: 0.5291502622129182
Relative absolute error: 56.00000000000001
Root relative squared error: 105.83005244258364
Weighted TruePositiveRate: 0.72
Weighted MatthewsCorrelation: 0.212598736660879
Weighted FMeasure: 0.6619170653091505
Iteration time: 41.0
Weighted AreaUnderPRC: 0.6170916267942584
Mean absolute error: 0.28
Coverage of cases: 72.0
Instances selection time: 4.0
Test time: 3.0
Accumulative iteration time: 209.0
Weighted Recall: 0.72
Weighted FalsePositiveRate: 0.5885714285714285
Kappa statistic: 0.16467780429594278
Training time: 37.0
		
Iteration: 8
Labeled set size: 240
Unlabelled set size: 260
	
Mean region size: 50.0
Incorrectly Classified Instances: 27.8
Correctly Classified Instances: 72.2
Weighted Precision: 0.6989743589743589
Weighted AreaUnderROC: 0.569047619047619
Root mean squared error: 0.5272570530585627
Relative absolute error: 55.60000000000001
Root relative squared error: 105.45141061171255
Weighted TruePositiveRate: 0.722
Weighted MatthewsCorrelation: 0.2211293552546441
Weighted FMeasure: 0.6652842809364549
Iteration time: 41.0
Weighted AreaUnderPRC: 0.6192686202686202
Mean absolute error: 0.278
Coverage of cases: 72.2
Instances selection time: 4.0
Test time: 2.0
Accumulative iteration time: 250.0
Weighted Recall: 0.722
Weighted FalsePositiveRate: 0.5839047619047619
Kappa statistic: 0.17261904761904745
Training time: 37.0
		
Iteration: 9
Labeled set size: 260
Unlabelled set size: 240
	
Mean region size: 50.0
Incorrectly Classified Instances: 28.2
Correctly Classified Instances: 71.8
Weighted Precision: 0.6995185363505055
Weighted AreaUnderROC: 0.550952380952381
Root mean squared error: 0.5310367218940701
Relative absolute error: 56.39999999999999
Root relative squared error: 106.20734437881403
Weighted TruePositiveRate: 0.718
Weighted MatthewsCorrelation: 0.19364525605059035
Weighted FMeasure: 0.645785522028616
Iteration time: 24.0
Weighted AreaUnderPRC: 0.6092746818900887
Mean absolute error: 0.282
Coverage of cases: 71.8
Instances selection time: 3.0
Test time: 3.0
Accumulative iteration time: 274.0
Weighted Recall: 0.718
Weighted FalsePositiveRate: 0.6160952380952381
Kappa statistic: 0.13177339901477816
Training time: 21.0
		
Iteration: 10
Labeled set size: 280
Unlabelled set size: 220
	
Mean region size: 50.0
Incorrectly Classified Instances: 28.0
Correctly Classified Instances: 72.0
Weighted Precision: 0.6972826086956523
Weighted AreaUnderROC: 0.5619047619047619
Root mean squared error: 0.5291502622129182
Relative absolute error: 56.00000000000001
Root relative squared error: 105.83005244258364
Weighted TruePositiveRate: 0.72
Weighted MatthewsCorrelation: 0.20913407192853073
Weighted FMeasure: 0.657959714100065
Iteration time: 27.0
Weighted AreaUnderPRC: 0.6151847826086956
Mean absolute error: 0.28
Coverage of cases: 72.0
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 301.0
Weighted Recall: 0.72
Weighted FalsePositiveRate: 0.5961904761904763
Kappa statistic: 0.1566265060240962
Training time: 25.0
		
Iteration: 11
Labeled set size: 300
Unlabelled set size: 200
	
Mean region size: 50.0
Incorrectly Classified Instances: 28.6
Correctly Classified Instances: 71.4
Weighted Precision: 0.6900285526026795
Weighted AreaUnderROC: 0.5442857142857143
Root mean squared error: 0.5347896782848375
Relative absolute error: 57.199999999999996
Root relative squared error: 106.95793565696749
Weighted TruePositiveRate: 0.714
Weighted MatthewsCorrelation: 0.17364562617314983
Weighted FMeasure: 0.6384107131921148
Iteration time: 33.0
Weighted AreaUnderPRC: 0.6049320887327037
Mean absolute error: 0.286
Coverage of cases: 71.4
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 334.0
Weighted Recall: 0.714
Weighted FalsePositiveRate: 0.6254285714285714
Kappa statistic: 0.11509900990099012
Training time: 32.0
		
Iteration: 12
Labeled set size: 320
Unlabelled set size: 180
	
Mean region size: 50.0
Incorrectly Classified Instances: 28.6
Correctly Classified Instances: 71.4
Weighted Precision: 0.6900285526026795
Weighted AreaUnderROC: 0.5442857142857143
Root mean squared error: 0.5347896782848375
Relative absolute error: 57.199999999999996
Root relative squared error: 106.95793565696749
Weighted TruePositiveRate: 0.714
Weighted MatthewsCorrelation: 0.17364562617314983
Weighted FMeasure: 0.6384107131921148
Iteration time: 29.0
Weighted AreaUnderPRC: 0.6049320887327037
Mean absolute error: 0.286
Coverage of cases: 71.4
Instances selection time: 2.0
Test time: 3.0
Accumulative iteration time: 363.0
Weighted Recall: 0.714
Weighted FalsePositiveRate: 0.6254285714285714
Kappa statistic: 0.11509900990099012
Training time: 27.0
		
Iteration: 13
Labeled set size: 340
Unlabelled set size: 160
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.2
Correctly Classified Instances: 70.8
Weighted Precision: 0.6850206611570248
Weighted AreaUnderROC: 0.5247619047619047
Root mean squared error: 0.5403702434442518
Relative absolute error: 58.4
Root relative squared error: 108.07404868885035
Weighted TruePositiveRate: 0.708
Weighted MatthewsCorrelation: 0.12894693513945005
Weighted FMeasure: 0.6136026118863943
Iteration time: 45.0
Weighted AreaUnderPRC: 0.59389173553719
Mean absolute error: 0.292
Coverage of cases: 70.8
Instances selection time: 3.0
Test time: 3.0
Accumulative iteration time: 408.0
Weighted Recall: 0.708
Weighted FalsePositiveRate: 0.6584761904761904
Kappa statistic: 0.06649616368286423
Training time: 42.0
		
Iteration: 14
Labeled set size: 360
Unlabelled set size: 140
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.2
Correctly Classified Instances: 70.8
Weighted Precision: 0.6897707231040565
Weighted AreaUnderROC: 0.5228571428571429
Root mean squared error: 0.5403702434442518
Relative absolute error: 58.4
Root relative squared error: 108.07404868885035
Weighted TruePositiveRate: 0.708
Weighted MatthewsCorrelation: 0.12698412698412698
Weighted FMeasure: 0.6106780254405414
Iteration time: 49.0
Weighted AreaUnderPRC: 0.5929862433862434
Mean absolute error: 0.292
Coverage of cases: 70.8
Instances selection time: 3.0
Test time: 3.0
Accumulative iteration time: 457.0
Weighted Recall: 0.708
Weighted FalsePositiveRate: 0.6622857142857144
Kappa statistic: 0.061696658097686354
Training time: 46.0
		
Iteration: 15
Labeled set size: 380
Unlabelled set size: 120
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.2
Correctly Classified Instances: 70.8
Weighted Precision: 0.7201219512195121
Weighted AreaUnderROC: 0.5171428571428571
Root mean squared error: 0.5403702434442518
Relative absolute error: 58.4
Root relative squared error: 108.07404868885035
Weighted TruePositiveRate: 0.708
Weighted MatthewsCorrelation: 0.12521758066945232
Weighted FMeasure: 0.6014071379175562
Iteration time: 34.0
Weighted AreaUnderPRC: 0.5904926829268293
Mean absolute error: 0.292
Coverage of cases: 70.8
Instances selection time: 3.0
Test time: 3.0
Accumulative iteration time: 491.0
Weighted Recall: 0.708
Weighted FalsePositiveRate: 0.6737142857142857
Kappa statistic: 0.046997389033942454
Training time: 31.0
		
Iteration: 16
Labeled set size: 400
Unlabelled set size: 100
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.0
Correctly Classified Instances: 71.0
Weighted Precision: 0.7050229031748538
Weighted AreaUnderROC: 0.5242857142857142
Root mean squared error: 0.5385164807134504
Relative absolute error: 57.99999999999999
Root relative squared error: 107.70329614269008
Weighted TruePositiveRate: 0.71
Weighted MatthewsCorrelation: 0.13986993465016023
Weighted FMeasure: 0.6118624066377876
Iteration time: 104.0
Weighted AreaUnderPRC: 0.5943083557099984
Mean absolute error: 0.29
Coverage of cases: 71.0
Instances selection time: 3.0
Test time: 3.0
Accumulative iteration time: 595.0
Weighted Recall: 0.71
Weighted FalsePositiveRate: 0.6614285714285715
Kappa statistic: 0.065721649484536
Training time: 101.0
		
Iteration: 17
Labeled set size: 420
Unlabelled set size: 80
	
Mean region size: 50.0
Incorrectly Classified Instances: 27.8
Correctly Classified Instances: 72.2
Weighted Precision: 0.7081370449678801
Weighted AreaUnderROC: 0.5576190476190476
Root mean squared error: 0.5272570530585627
Relative absolute error: 55.60000000000001
Root relative squared error: 105.45141061171255
Weighted TruePositiveRate: 0.722
Weighted MatthewsCorrelation: 0.21269648493678212
Weighted FMeasure: 0.6530369002949615
Iteration time: 75.0
Weighted AreaUnderPRC: 0.6137003568879372
Mean absolute error: 0.278
Coverage of cases: 72.2
Instances selection time: 3.0
Test time: 2.0
Accumulative iteration time: 670.0
Weighted Recall: 0.722
Weighted FalsePositiveRate: 0.6067619047619048
Kappa statistic: 0.14828431372549017
Training time: 72.0
		
Iteration: 18
Labeled set size: 440
Unlabelled set size: 60
	
Mean region size: 50.0
Incorrectly Classified Instances: 27.4
Correctly Classified Instances: 72.6
Weighted Precision: 0.7012753783047809
Weighted AreaUnderROC: 0.5947619047619047
Root mean squared error: 0.523450093132096
Relative absolute error: 54.800000000000004
Root relative squared error: 104.6900186264192
Weighted TruePositiveRate: 0.726
Weighted MatthewsCorrelation: 0.2549541464505272
Weighted FMeasure: 0.6881214282771569
Iteration time: 122.0
Weighted AreaUnderPRC: 0.6334503395263866
Mean absolute error: 0.274
Coverage of cases: 72.6
Instances selection time: 3.0
Test time: 2.0
Accumulative iteration time: 792.0
Weighted Recall: 0.726
Weighted FalsePositiveRate: 0.5364761904761903
Kappa statistic: 0.2251131221719457
Training time: 119.0
		
Iteration: 19
Labeled set size: 460
Unlabelled set size: 40
	
Mean region size: 50.0
Incorrectly Classified Instances: 24.0
Correctly Classified Instances: 76.0
Weighted Precision: 0.7461324257425742
Weighted AreaUnderROC: 0.6628571428571428
Root mean squared error: 0.4898979485566356
Relative absolute error: 48.0
Root relative squared error: 97.97958971132712
Weighted TruePositiveRate: 0.76
Weighted MatthewsCorrelation: 0.3789571971665136
Weighted FMeasure: 0.7422527010415992
Iteration time: 170.0
Weighted AreaUnderPRC: 0.678557797029703
Mean absolute error: 0.24
Coverage of cases: 76.0
Instances selection time: 2.0
Test time: 3.0
Accumulative iteration time: 962.0
Weighted Recall: 0.76
Weighted FalsePositiveRate: 0.4342857142857143
Kappa statistic: 0.3630573248407644
Training time: 168.0
		
Iteration: 20
Labeled set size: 480
Unlabelled set size: 20
	
Mean region size: 50.0
Incorrectly Classified Instances: 25.6
Correctly Classified Instances: 74.4
Weighted Precision: 0.7279428904428905
Weighted AreaUnderROC: 0.6514285714285715
Root mean squared error: 0.5059644256269407
Relative absolute error: 51.2
Root relative squared error: 101.19288512538813
Weighted TruePositiveRate: 0.744
Weighted MatthewsCorrelation: 0.3419426003362873
Weighted FMeasure: 0.7287116590318972
Iteration time: 154.0
Weighted AreaUnderPRC: 0.6681541958041958
Mean absolute error: 0.256
Coverage of cases: 74.4
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 1116.0
Weighted Recall: 0.744
Weighted FalsePositiveRate: 0.44114285714285717
Kappa statistic: 0.3319415448851774
Training time: 154.0
		
Time end:Tue Oct 31 11.31.40 EET 2017