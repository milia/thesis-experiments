Tue Oct 31 11.33.47 EET 2017
Dataset: german_credit
Test set size: 500
Initial Labelled set size: 100
Initial Unlabelled set size: 400
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Tue Oct 31 11.33.47 EET 2017
		
Iteration: 1
Labeled set size: 100
Unlabelled set size: 400
	
Mean region size: 79.0
Incorrectly Classified Instances: 33.2
Correctly Classified Instances: 66.8
Weighted Precision: 0.6692679975801573
Weighted AreaUnderROC: 0.6832
Root mean squared error: 0.49912154736469116
Relative absolute error: 69.47291925197074
Root relative squared error: 99.82430947293824
Weighted TruePositiveRate: 0.668
Weighted MatthewsCorrelation: 0.2125332850579212
Weighted FMeasure: 0.668623693049204
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7183119190982593
Mean absolute error: 0.34736459625985366
Coverage of cases: 89.6
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 2.0
Weighted Recall: 0.668
Weighted FalsePositiveRate: 0.4546666666666666
Kappa statistic: 0.21252371916508545
Training time: 1.0
		
Iteration: 2
Labeled set size: 120
Unlabelled set size: 380
	
Mean region size: 84.2
Incorrectly Classified Instances: 30.4
Correctly Classified Instances: 69.6
Weighted Precision: 0.684989604989605
Weighted AreaUnderROC: 0.6796190476190476
Root mean squared error: 0.4777417082615889
Relative absolute error: 68.45830059838667
Root relative squared error: 95.54834165231779
Weighted TruePositiveRate: 0.696
Weighted MatthewsCorrelation: 0.24874698365017792
Weighted FMeasure: 0.6893650793650793
Iteration time: 1.0
Weighted AreaUnderPRC: 0.7175125283316408
Mean absolute error: 0.34229150299193334
Coverage of cases: 92.6
Instances selection time: 1.0
Test time: 7.0
Accumulative iteration time: 3.0
Weighted Recall: 0.696
Weighted FalsePositiveRate: 0.4579047619047619
Kappa statistic: 0.24752475247524747
Training time: 0.0
		
Iteration: 3
Labeled set size: 140
Unlabelled set size: 360
	
Mean region size: 83.1
Incorrectly Classified Instances: 31.2
Correctly Classified Instances: 68.8
Weighted Precision: 0.6765904365904366
Weighted AreaUnderROC: 0.6996380952380953
Root mean squared error: 0.47721053519908563
Relative absolute error: 66.92766528286577
Root relative squared error: 95.44210703981713
Weighted TruePositiveRate: 0.688
Weighted MatthewsCorrelation: 0.2288472249581637
Weighted FMeasure: 0.6811904761904761
Iteration time: 1.0
Weighted AreaUnderPRC: 0.7294065314026439
Mean absolute error: 0.3346383264143289
Coverage of cases: 92.8
Instances selection time: 1.0
Test time: 7.0
Accumulative iteration time: 4.0
Weighted Recall: 0.688
Weighted FalsePositiveRate: 0.46895238095238095
Kappa statistic: 0.22772277227722765
Training time: 0.0
		
Iteration: 4
Labeled set size: 160
Unlabelled set size: 340
	
Mean region size: 81.4
Incorrectly Classified Instances: 30.6
Correctly Classified Instances: 69.4
Weighted Precision: 0.6866602320484205
Weighted AreaUnderROC: 0.7112952380952381
Root mean squared error: 0.4743764885793574
Relative absolute error: 65.97406157227978
Root relative squared error: 94.87529771587148
Weighted TruePositiveRate: 0.694
Weighted MatthewsCorrelation: 0.2534407940661247
Weighted FMeasure: 0.689859307729523
Iteration time: 1.0
Weighted AreaUnderPRC: 0.7386530103743796
Mean absolute error: 0.3298703078613989
Coverage of cases: 92.4
Instances selection time: 1.0
Test time: 7.0
Accumulative iteration time: 5.0
Weighted Recall: 0.694
Weighted FalsePositiveRate: 0.4473333333333333
Kappa statistic: 0.25292968749999983
Training time: 0.0
		
Iteration: 5
Labeled set size: 180
Unlabelled set size: 320
	
Mean region size: 81.8
Incorrectly Classified Instances: 28.8
Correctly Classified Instances: 71.2
Weighted Precision: 0.7067063492063491
Weighted AreaUnderROC: 0.7275619047619047
Root mean squared error: 0.4646437893390808
Relative absolute error: 64.43910346299424
Root relative squared error: 92.92875786781616
Weighted TruePositiveRate: 0.712
Weighted MatthewsCorrelation: 0.3013261191518989
Weighted FMeasure: 0.7090626517727053
Iteration time: 1.0
Weighted AreaUnderPRC: 0.7486119465932575
Mean absolute error: 0.3221955173149712
Coverage of cases: 93.2
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 6.0
Weighted Recall: 0.712
Weighted FalsePositiveRate: 0.4167619047619048
Kappa statistic: 0.30097087378640774
Training time: 0.0
		
Iteration: 6
Labeled set size: 200
Unlabelled set size: 300
	
Mean region size: 79.6
Incorrectly Classified Instances: 29.8
Correctly Classified Instances: 70.2
Weighted Precision: 0.689787000485529
Weighted AreaUnderROC: 0.7239619047619048
Root mean squared error: 0.4666053629889329
Relative absolute error: 63.857882942161716
Root relative squared error: 93.32107259778658
Weighted TruePositiveRate: 0.702
Weighted MatthewsCorrelation: 0.25967721235080643
Weighted FMeasure: 0.6943681311822479
Iteration time: 1.0
Weighted AreaUnderPRC: 0.7459199622436226
Mean absolute error: 0.3192894147108086
Coverage of cases: 92.2
Instances selection time: 1.0
Test time: 7.0
Accumulative iteration time: 7.0
Weighted Recall: 0.702
Weighted FalsePositiveRate: 0.45533333333333337
Kappa statistic: 0.25796812749003967
Training time: 0.0
		
Iteration: 7
Labeled set size: 220
Unlabelled set size: 280
	
Mean region size: 83.4
Incorrectly Classified Instances: 29.4
Correctly Classified Instances: 70.6
Weighted Precision: 0.6979908675799086
Weighted AreaUnderROC: 0.7335238095238096
Root mean squared error: 0.45825738273424116
Relative absolute error: 64.220251133985
Root relative squared error: 91.65147654684823
Weighted TruePositiveRate: 0.706
Weighted MatthewsCorrelation: 0.2801701716290789
Weighted FMeasure: 0.701347073978653
Iteration time: 1.0
Weighted AreaUnderPRC: 0.7548379341949308
Mean absolute error: 0.32110125566992503
Coverage of cases: 94.2
Instances selection time: 1.0
Test time: 7.0
Accumulative iteration time: 8.0
Weighted Recall: 0.706
Weighted FalsePositiveRate: 0.43457142857142855
Kappa statistic: 0.2794117647058823
Training time: 0.0
		
Iteration: 8
Labeled set size: 240
Unlabelled set size: 260
	
Mean region size: 81.6
Incorrectly Classified Instances: 28.6
Correctly Classified Instances: 71.4
Weighted Precision: 0.7043567305902066
Weighted AreaUnderROC: 0.7383619047619048
Root mean squared error: 0.4568273424085546
Relative absolute error: 63.45667208339849
Root relative squared error: 91.36546848171092
Weighted TruePositiveRate: 0.714
Weighted MatthewsCorrelation: 0.2947799704906587
Weighted FMeasure: 0.7081098203812136
Iteration time: 2.0
Weighted AreaUnderPRC: 0.758462745582712
Mean absolute error: 0.31728336041699245
Coverage of cases: 93.6
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 10.0
Weighted Recall: 0.714
Weighted FalsePositiveRate: 0.4311428571428571
Kappa statistic: 0.29347826086956524
Training time: 1.0
		
Iteration: 9
Labeled set size: 260
Unlabelled set size: 240
	
Mean region size: 81.6
Incorrectly Classified Instances: 28.2
Correctly Classified Instances: 71.8
Weighted Precision: 0.709469177029768
Weighted AreaUnderROC: 0.7325142857142858
Root mean squared error: 0.4600398428938208
Relative absolute error: 64.20281801424814
Root relative squared error: 92.00796857876416
Weighted TruePositiveRate: 0.718
Weighted MatthewsCorrelation: 0.3071792594433667
Weighted FMeasure: 0.7128731315699987
Iteration time: 1.0
Weighted AreaUnderPRC: 0.754108512463674
Mean absolute error: 0.3210140900712407
Coverage of cases: 93.4
Instances selection time: 1.0
Test time: 7.0
Accumulative iteration time: 11.0
Weighted Recall: 0.718
Weighted FalsePositiveRate: 0.42180952380952375
Kappa statistic: 0.3061023622047243
Training time: 0.0
		
Iteration: 10
Labeled set size: 280
Unlabelled set size: 220
	
Mean region size: 82.3
Incorrectly Classified Instances: 28.2
Correctly Classified Instances: 71.8
Weighted Precision: 0.7076349275998244
Weighted AreaUnderROC: 0.7334666666666667
Root mean squared error: 0.4597895782784411
Relative absolute error: 64.4778174516613
Root relative squared error: 91.95791565568821
Weighted TruePositiveRate: 0.718
Weighted MatthewsCorrelation: 0.302239464549685
Weighted FMeasure: 0.7114938928906983
Iteration time: 1.0
Weighted AreaUnderPRC: 0.7562401087312828
Mean absolute error: 0.3223890872583065
Coverage of cases: 93.8
Instances selection time: 0.0
Test time: 7.0
Accumulative iteration time: 12.0
Weighted Recall: 0.718
Weighted FalsePositiveRate: 0.42942857142857144
Kappa statistic: 0.300595238095238
Training time: 1.0
		
Iteration: 11
Labeled set size: 300
Unlabelled set size: 200
	
Mean region size: 84.2
Incorrectly Classified Instances: 27.2
Correctly Classified Instances: 72.8
Weighted Precision: 0.7169340463458111
Weighted AreaUnderROC: 0.7428761904761905
Root mean squared error: 0.45128542879656386
Relative absolute error: 63.51089683134914
Root relative squared error: 90.25708575931277
Weighted TruePositiveRate: 0.728
Weighted MatthewsCorrelation: 0.32368692037257424
Weighted FMeasure: 0.720682200336296
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7629623547684975
Mean absolute error: 0.3175544841567457
Coverage of cases: 94.8
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 14.0
Weighted Recall: 0.728
Weighted FalsePositiveRate: 0.42133333333333334
Kappa statistic: 0.32135728542914177
Training time: 1.0
		
Iteration: 12
Labeled set size: 320
Unlabelled set size: 180
	
Mean region size: 83.3
Incorrectly Classified Instances: 27.2
Correctly Classified Instances: 72.8
Weighted Precision: 0.719441699604743
Weighted AreaUnderROC: 0.7468952380952381
Root mean squared error: 0.4487694677183759
Relative absolute error: 62.879174604021934
Root relative squared error: 89.75389354367518
Weighted TruePositiveRate: 0.728
Weighted MatthewsCorrelation: 0.3306937176630563
Weighted FMeasure: 0.7227286196882594
Iteration time: 1.0
Weighted AreaUnderPRC: 0.7669634056510629
Mean absolute error: 0.3143958730201097
Coverage of cases: 94.8
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 15.0
Weighted Recall: 0.728
Weighted FalsePositiveRate: 0.4099047619047619
Kappa statistic: 0.32938856015779083
Training time: 0.0
		
Iteration: 13
Labeled set size: 340
Unlabelled set size: 160
	
Mean region size: 82.3
Incorrectly Classified Instances: 26.2
Correctly Classified Instances: 73.8
Weighted Precision: 0.7302841572596341
Weighted AreaUnderROC: 0.7495428571428572
Root mean squared error: 0.4477450631184963
Relative absolute error: 62.35733191168021
Root relative squared error: 89.54901262369927
Weighted TruePositiveRate: 0.738
Weighted MatthewsCorrelation: 0.35656499247284695
Weighted FMeasure: 0.7332367392600698
Iteration time: 1.0
Weighted AreaUnderPRC: 0.7681244143649112
Mean absolute error: 0.31178665955840107
Coverage of cases: 94.6
Instances selection time: 0.0
Test time: 7.0
Accumulative iteration time: 16.0
Weighted Recall: 0.738
Weighted FalsePositiveRate: 0.39419047619047615
Kappa statistic: 0.3553149606299212
Training time: 1.0
		
Iteration: 14
Labeled set size: 360
Unlabelled set size: 140
	
Mean region size: 82.4
Incorrectly Classified Instances: 26.0
Correctly Classified Instances: 74.0
Weighted Precision: 0.7353174603174604
Weighted AreaUnderROC: 0.7452761904761904
Root mean squared error: 0.4501700980796546
Relative absolute error: 62.67698426099627
Root relative squared error: 90.03401961593092
Weighted TruePositiveRate: 0.74
Weighted MatthewsCorrelation: 0.3693675008958761
Weighted FMeasure: 0.7373482272948034
Iteration time: 1.0
Weighted AreaUnderPRC: 0.763981403365531
Mean absolute error: 0.31338492130498136
Coverage of cases: 94.2
Instances selection time: 0.0
Test time: 7.0
Accumulative iteration time: 17.0
Weighted Recall: 0.74
Weighted FalsePositiveRate: 0.37809523809523815
Kappa statistic: 0.36893203883495146
Training time: 1.0
		
Iteration: 15
Labeled set size: 380
Unlabelled set size: 120
	
Mean region size: 81.7
Incorrectly Classified Instances: 27.2
Correctly Classified Instances: 72.8
Weighted Precision: 0.721210407239819
Weighted AreaUnderROC: 0.7443428571428573
Root mean squared error: 0.4510943315837108
Relative absolute error: 62.353073969255114
Root relative squared error: 90.21886631674217
Weighted TruePositiveRate: 0.728
Weighted MatthewsCorrelation: 0.33542568293273695
Weighted FMeasure: 0.724009324009324
Iteration time: 1.0
Weighted AreaUnderPRC: 0.7634243256840532
Mean absolute error: 0.31176536984627556
Coverage of cases: 94.2
Instances selection time: 0.0
Test time: 7.0
Accumulative iteration time: 18.0
Weighted Recall: 0.728
Weighted FalsePositiveRate: 0.4022857142857143
Kappa statistic: 0.3346379647749511
Training time: 1.0
		
Iteration: 16
Labeled set size: 400
Unlabelled set size: 100
	
Mean region size: 81.9
Incorrectly Classified Instances: 26.2
Correctly Classified Instances: 73.8
Weighted Precision: 0.7302841572596341
Weighted AreaUnderROC: 0.7487047619047619
Root mean squared error: 0.44917556014656856
Relative absolute error: 62.310612448433055
Root relative squared error: 89.8351120293137
Weighted TruePositiveRate: 0.738
Weighted MatthewsCorrelation: 0.35656499247284695
Weighted FMeasure: 0.7332367392600698
Iteration time: 1.0
Weighted AreaUnderPRC: 0.7664650020563147
Mean absolute error: 0.3115530622421653
Coverage of cases: 94.4
Instances selection time: 0.0
Test time: 7.0
Accumulative iteration time: 19.0
Weighted Recall: 0.738
Weighted FalsePositiveRate: 0.39419047619047615
Kappa statistic: 0.3553149606299212
Training time: 1.0
		
Iteration: 17
Labeled set size: 420
Unlabelled set size: 80
	
Mean region size: 83.3
Incorrectly Classified Instances: 26.2
Correctly Classified Instances: 73.8
Weighted Precision: 0.7319599445014177
Weighted AreaUnderROC: 0.7539809523809524
Root mean squared error: 0.4475147133556831
Relative absolute error: 62.13458496736727
Root relative squared error: 89.50294267113662
Weighted TruePositiveRate: 0.738
Weighted MatthewsCorrelation: 0.3610797413528958
Weighted FMeasure: 0.7344547013893301
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7693438462719904
Mean absolute error: 0.31067292483683634
Coverage of cases: 94.6
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 21.0
Weighted Recall: 0.738
Weighted FalsePositiveRate: 0.38657142857142857
Kappa statistic: 0.36035156249999994
Training time: 1.0
		
Iteration: 18
Labeled set size: 440
Unlabelled set size: 60
	
Mean region size: 82.8
Incorrectly Classified Instances: 26.6
Correctly Classified Instances: 73.4
Weighted Precision: 0.7269710806697107
Weighted AreaUnderROC: 0.7549142857142859
Root mean squared error: 0.44597010192219316
Relative absolute error: 61.83163014740205
Root relative squared error: 89.19402038443863
Weighted TruePositiveRate: 0.734
Weighted MatthewsCorrelation: 0.3489838979941159
Weighted FMeasure: 0.7297902097902098
Iteration time: 0.0
Weighted AreaUnderPRC: 0.7703244012408719
Mean absolute error: 0.30915815073701025
Coverage of cases: 94.6
Instances selection time: 0.0
Test time: 7.0
Accumulative iteration time: 21.0
Weighted Recall: 0.734
Weighted FalsePositiveRate: 0.39590476190476187
Kappa statistic: 0.3480392156862745
Training time: 0.0
		
Iteration: 19
Labeled set size: 460
Unlabelled set size: 40
	
Mean region size: 83.6
Incorrectly Classified Instances: 26.8
Correctly Classified Instances: 73.2
Weighted Precision: 0.7236166007905137
Weighted AreaUnderROC: 0.7556190476190477
Root mean squared error: 0.44338008136070467
Relative absolute error: 61.82334542074355
Root relative squared error: 88.67601627214093
Weighted TruePositiveRate: 0.732
Weighted MatthewsCorrelation: 0.3405947271739262
Weighted FMeasure: 0.7268061399869615
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7717134809282652
Mean absolute error: 0.30911672710371774
Coverage of cases: 94.8
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 23.0
Weighted Recall: 0.732
Weighted FalsePositiveRate: 0.4043809523809524
Kappa statistic: 0.33925049309664684
Training time: 1.0
		
Iteration: 20
Labeled set size: 480
Unlabelled set size: 20
	
Mean region size: 82.6
Incorrectly Classified Instances: 26.6
Correctly Classified Instances: 73.4
Weighted Precision: 0.723698043106542
Weighted AreaUnderROC: 0.7512190476190475
Root mean squared error: 0.4476329661573597
Relative absolute error: 62.06308966007741
Root relative squared error: 89.52659323147194
Weighted TruePositiveRate: 0.734
Weighted MatthewsCorrelation: 0.3398863899109011
Weighted FMeasure: 0.727187660719725
Iteration time: 1.0
Weighted AreaUnderPRC: 0.7701805826991149
Mean absolute error: 0.31031544830038704
Coverage of cases: 94.6
Instances selection time: 0.0
Test time: 7.0
Accumulative iteration time: 24.0
Weighted Recall: 0.734
Weighted FalsePositiveRate: 0.4111428571428571
Kappa statistic: 0.3376494023904381
Training time: 1.0
		
Time end:Tue Oct 31 11.33.48 EET 2017