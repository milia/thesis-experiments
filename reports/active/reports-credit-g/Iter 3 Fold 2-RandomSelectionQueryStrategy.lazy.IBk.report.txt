Tue Oct 31 11.34.19 EET 2017
Dataset: german_credit
Test set size: 500
Initial Labelled set size: 100
Initial Unlabelled set size: 400
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Tue Oct 31 11.34.19 EET 2017
		
Iteration: 1
Labeled set size: 100
Unlabelled set size: 400
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.2
Correctly Classified Instances: 65.8
Weighted Precision: 0.648310502283105
Weighted AreaUnderROC: 0.5785714285714286
Root mean squared error: 0.5791288583080482
Relative absolute error: 69.01960784313732
Root relative squared error: 115.82577166160964
Weighted TruePositiveRate: 0.658
Weighted MatthewsCorrelation: 0.1622037835747299
Weighted FMeasure: 0.6525874125874126
Iteration time: 7.0
Weighted AreaUnderPRC: 0.6185251141552512
Mean absolute error: 0.3450980392156866
Coverage of cases: 65.8
Instances selection time: 7.0
Test time: 28.0
Accumulative iteration time: 7.0
Weighted Recall: 0.658
Weighted FalsePositiveRate: 0.5008571428571429
Kappa statistic: 0.16176470588235306
Training time: 0.0
		
Iteration: 2
Labeled set size: 120
Unlabelled set size: 380
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.8
Correctly Classified Instances: 65.2
Weighted Precision: 0.6387941787941788
Weighted AreaUnderROC: 0.5666666666666667
Root mean squared error: 0.5851173114918674
Relative absolute error: 70.09836065573789
Root relative squared error: 117.02346229837349
Weighted TruePositiveRate: 0.652
Weighted MatthewsCorrelation: 0.13929831084409963
Weighted FMeasure: 0.6444047619047618
Iteration time: 1.0
Weighted AreaUnderPRC: 0.6120748440748441
Mean absolute error: 0.35049180327868945
Coverage of cases: 65.2
Instances selection time: 1.0
Test time: 22.0
Accumulative iteration time: 8.0
Weighted Recall: 0.652
Weighted FalsePositiveRate: 0.5186666666666666
Kappa statistic: 0.13861386138613874
Training time: 0.0
		
Iteration: 3
Labeled set size: 140
Unlabelled set size: 360
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.0
Correctly Classified Instances: 66.0
Weighted Precision: 0.6484683794466403
Weighted AreaUnderROC: 0.5780952380952381
Root mean squared error: 0.579017150817045
Relative absolute error: 68.45070422535217
Root relative squared error: 115.80343016340899
Weighted TruePositiveRate: 0.66
Weighted MatthewsCorrelation: 0.16237655597826714
Weighted FMeasure: 0.6534107746103242
Iteration time: 1.0
Weighted AreaUnderPRC: 0.6183368906455862
Mean absolute error: 0.34225352112676083
Coverage of cases: 66.0
Instances selection time: 1.0
Test time: 24.0
Accumulative iteration time: 9.0
Weighted Recall: 0.66
Weighted FalsePositiveRate: 0.5038095238095238
Kappa statistic: 0.16173570019723865
Training time: 0.0
		
Iteration: 4
Labeled set size: 160
Unlabelled set size: 340
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.4
Correctly Classified Instances: 65.6
Weighted Precision: 0.6520833333333333
Weighted AreaUnderROC: 0.5847619047619048
Root mean squared error: 0.5829161092021069
Relative absolute error: 69.18518518518519
Root relative squared error: 116.58322184042137
Weighted TruePositiveRate: 0.656
Weighted MatthewsCorrelation: 0.17155528826362926
Weighted FMeasure: 0.653951552292305
Iteration time: 0.0
Weighted AreaUnderPRC: 0.6217805555555554
Mean absolute error: 0.34592592592592597
Coverage of cases: 65.6
Instances selection time: 0.0
Test time: 27.0
Accumulative iteration time: 9.0
Weighted Recall: 0.656
Weighted FalsePositiveRate: 0.48647619047619056
Kappa statistic: 0.17148362235067446
Training time: 0.0
		
Iteration: 5
Labeled set size: 180
Unlabelled set size: 320
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.0
Correctly Classified Instances: 66.0
Weighted Precision: 0.6548508930679048
Weighted AreaUnderROC: 0.5876190476190476
Root mean squared error: 0.5799085495613653
Relative absolute error: 68.3516483516484
Root relative squared error: 115.98170991227306
Weighted TruePositiveRate: 0.66
Weighted MatthewsCorrelation: 0.17808301103792454
Weighted FMeasure: 0.657263369708227
Iteration time: 1.0
Weighted AreaUnderPRC: 0.6234598473522701
Mean absolute error: 0.34175824175824193
Coverage of cases: 66.0
Instances selection time: 1.0
Test time: 29.0
Accumulative iteration time: 10.0
Weighted Recall: 0.66
Weighted FalsePositiveRate: 0.48476190476190484
Kappa statistic: 0.17794970986460348
Training time: 0.0
		
Iteration: 6
Labeled set size: 200
Unlabelled set size: 300
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.8
Correctly Classified Instances: 65.2
Weighted Precision: 0.6427601809954752
Weighted AreaUnderROC: 0.5723809523809524
Root mean squared error: 0.5870084861795275
Relative absolute error: 69.90099009900985
Root relative squared error: 117.4016972359055
Weighted TruePositiveRate: 0.652
Weighted MatthewsCorrelation: 0.14907808130343864
Weighted FMeasure: 0.6468942821883998
Iteration time: 0.0
Weighted AreaUnderPRC: 0.615067097608274
Mean absolute error: 0.3495049504950492
Coverage of cases: 65.2
Instances selection time: 0.0
Test time: 33.0
Accumulative iteration time: 10.0
Weighted Recall: 0.652
Weighted FalsePositiveRate: 0.5072380952380953
Kappa statistic: 0.14872798434442286
Training time: 0.0
		
Iteration: 7
Labeled set size: 220
Unlabelled set size: 280
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.6
Correctly Classified Instances: 65.4
Weighted Precision: 0.6467865043145539
Weighted AreaUnderROC: 0.5776190476190476
Root mean squared error: 0.5855793485461334
Relative absolute error: 69.4774774774775
Root relative squared error: 117.11586970922667
Weighted TruePositiveRate: 0.654
Weighted MatthewsCorrelation: 0.15878773577513108
Weighted FMeasure: 0.6500917368684879
Iteration time: 1.0
Weighted AreaUnderPRC: 0.6178948444568445
Mean absolute error: 0.34738738738738756
Coverage of cases: 65.4
Instances selection time: 1.0
Test time: 34.0
Accumulative iteration time: 11.0
Weighted Recall: 0.654
Weighted FalsePositiveRate: 0.49876190476190474
Kappa statistic: 0.15856031128404677
Training time: 0.0
		
Iteration: 8
Labeled set size: 240
Unlabelled set size: 260
	
Mean region size: 50.0
Incorrectly Classified Instances: 35.0
Correctly Classified Instances: 65.0
Weighted Precision: 0.6400304414003043
Weighted AreaUnderROC: 0.569047619047619
Root mean squared error: 0.5891727364304238
Relative absolute error: 70.24793388429755
Root relative squared error: 117.83454728608476
Weighted TruePositiveRate: 0.65
Weighted MatthewsCorrelation: 0.14254271889900505
Weighted FMeasure: 0.6444608023555393
Iteration time: 1.0
Weighted AreaUnderPRC: 0.6132668696093353
Mean absolute error: 0.35123966942148777
Coverage of cases: 65.0
Instances selection time: 1.0
Test time: 37.0
Accumulative iteration time: 12.0
Weighted Recall: 0.65
Weighted FalsePositiveRate: 0.5119047619047619
Kappa statistic: 0.14215686274509814
Training time: 0.0
		
Iteration: 9
Labeled set size: 260
Unlabelled set size: 240
	
Mean region size: 50.0
Incorrectly Classified Instances: 33.0
Correctly Classified Instances: 67.0
Weighted Precision: 0.6644046701831328
Weighted AreaUnderROC: 0.5985714285714285
Root mean squared error: 0.5722722114048155
Relative absolute error: 66.25954198473289
Root relative squared error: 114.45444228096311
Weighted TruePositiveRate: 0.67
Weighted MatthewsCorrelation: 0.20077235699081097
Weighted FMeasure: 0.6669914065112762
Iteration time: 0.0
Weighted AreaUnderPRC: 0.6298650032596457
Mean absolute error: 0.3312977099236645
Coverage of cases: 67.0
Instances selection time: 0.0
Test time: 40.0
Accumulative iteration time: 12.0
Weighted Recall: 0.67
Weighted FalsePositiveRate: 0.47285714285714286
Kappa statistic: 0.2005813953488372
Training time: 0.0
		
Iteration: 10
Labeled set size: 280
Unlabelled set size: 220
	
Mean region size: 50.0
Incorrectly Classified Instances: 32.6
Correctly Classified Instances: 67.4
Weighted Precision: 0.6783959443650072
Weighted AreaUnderROC: 0.6185714285714285
Root mean squared error: 0.5689468499627982
Relative absolute error: 65.4468085106384
Root relative squared error: 113.78936999255964
Weighted TruePositiveRate: 0.674
Weighted MatthewsCorrelation: 0.23414927289477297
Weighted FMeasure: 0.6760701477313855
Iteration time: 1.0
Weighted AreaUnderPRC: 0.6413134352194017
Mean absolute error: 0.327234042553192
Coverage of cases: 67.4
Instances selection time: 1.0
Test time: 42.0
Accumulative iteration time: 13.0
Weighted Recall: 0.674
Weighted FalsePositiveRate: 0.4368571428571429
Kappa statistic: 0.2340225563909775
Training time: 0.0
		
Iteration: 11
Labeled set size: 300
Unlabelled set size: 200
	
Mean region size: 50.0
Incorrectly Classified Instances: 32.0
Correctly Classified Instances: 68.0
Weighted Precision: 0.681223532970357
Weighted AreaUnderROC: 0.6209523809523809
Root mean squared error: 0.5638189063284166
Relative absolute error: 64.23841059602677
Root relative squared error: 112.76378126568332
Weighted TruePositiveRate: 0.68
Weighted MatthewsCorrelation: 0.24099756430674996
Weighted FMeasure: 0.6806011499269435
Iteration time: 1.0
Weighted AreaUnderPRC: 0.6429967634603749
Mean absolute error: 0.3211920529801338
Coverage of cases: 68.0
Instances selection time: 1.0
Test time: 44.0
Accumulative iteration time: 14.0
Weighted Recall: 0.68
Weighted FalsePositiveRate: 0.4380952380952381
Kappa statistic: 0.24098671726755225
Training time: 0.0
		
Iteration: 12
Labeled set size: 320
Unlabelled set size: 180
	
Mean region size: 50.0
Incorrectly Classified Instances: 32.6
Correctly Classified Instances: 67.4
Weighted Precision: 0.6796713329275715
Weighted AreaUnderROC: 0.6204761904761906
Root mean squared error: 0.56919662680759
Relative absolute error: 65.41614906832288
Root relative squared error: 113.839325361518
Weighted TruePositiveRate: 0.674
Weighted MatthewsCorrelation: 0.23710194802746168
Weighted FMeasure: 0.6766245626852879
Iteration time: 0.0
Weighted AreaUnderPRC: 0.6424056400892675
Mean absolute error: 0.3270807453416144
Coverage of cases: 67.4
Instances selection time: 0.0
Test time: 47.0
Accumulative iteration time: 14.0
Weighted Recall: 0.674
Weighted FalsePositiveRate: 0.433047619047619
Kappa statistic: 0.2368913857677904
Training time: 0.0
		
Iteration: 13
Labeled set size: 340
Unlabelled set size: 160
	
Mean region size: 50.0
Incorrectly Classified Instances: 32.8
Correctly Classified Instances: 67.2
Weighted Precision: 0.6809451219512196
Weighted AreaUnderROC: 0.6228571428571429
Root mean squared error: 0.5710432741782657
Relative absolute error: 65.80116959064325
Root relative squared error: 114.20865483565315
Weighted TruePositiveRate: 0.672
Weighted MatthewsCorrelation: 0.2398382771860736
Weighted FMeasure: 0.6759651631353177
Iteration time: 0.0
Weighted AreaUnderPRC: 0.6436797038327525
Mean absolute error: 0.3290058479532163
Coverage of cases: 67.2
Instances selection time: 0.0
Test time: 50.0
Accumulative iteration time: 14.0
Weighted Recall: 0.672
Weighted FalsePositiveRate: 0.42628571428571427
Kappa statistic: 0.23933209647495376
Training time: 0.0
		
Iteration: 14
Labeled set size: 360
Unlabelled set size: 140
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.6
Correctly Classified Instances: 69.4
Weighted Precision: 0.6969518466573166
Weighted AreaUnderROC: 0.6404761904761905
Root mean squared error: 0.5516493662553126
Relative absolute error: 61.41436464088401
Root relative squared error: 110.32987325106251
Weighted TruePositiveRate: 0.694
Weighted MatthewsCorrelation: 0.2783793215012686
Weighted FMeasure: 0.695407477296851
Iteration time: 0.0
Weighted AreaUnderPRC: 0.6552739597942964
Mean absolute error: 0.30707182320442006
Coverage of cases: 69.4
Instances selection time: 0.0
Test time: 53.0
Accumulative iteration time: 14.0
Weighted Recall: 0.694
Weighted FalsePositiveRate: 0.41304761904761905
Kappa statistic: 0.27830188679245277
Training time: 0.0
		
Iteration: 15
Labeled set size: 380
Unlabelled set size: 120
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.0
Correctly Classified Instances: 70.0
Weighted Precision: 0.7
Weighted AreaUnderROC: 0.6428571428571429
Root mean squared error: 0.5462931193544822
Relative absolute error: 60.209424083769704
Root relative squared error: 109.25862387089644
Weighted TruePositiveRate: 0.7
Weighted MatthewsCorrelation: 0.2857142857142857
Weighted FMeasure: 0.7
Iteration time: 1.0
Weighted AreaUnderPRC: 0.6571428571428571
Mean absolute error: 0.3010471204188485
Coverage of cases: 70.0
Instances selection time: 0.0
Test time: 55.0
Accumulative iteration time: 15.0
Weighted Recall: 0.7
Weighted FalsePositiveRate: 0.41428571428571426
Kappa statistic: 0.2857142857142857
Training time: 1.0
		
Iteration: 16
Labeled set size: 400
Unlabelled set size: 100
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.2
Correctly Classified Instances: 69.8
Weighted Precision: 0.6985768230896222
Weighted AreaUnderROC: 0.6414285714285715
Root mean squared error: 0.548182178115501
Relative absolute error: 60.59701492537308
Root relative squared error: 109.6364356231002
Weighted TruePositiveRate: 0.698
Weighted MatthewsCorrelation: 0.2823225804299844
Weighted FMeasure: 0.6982856382397254
Iteration time: 0.0
Weighted AreaUnderPRC: 0.6561382682783354
Mean absolute error: 0.3029850746268654
Coverage of cases: 69.8
Instances selection time: 0.0
Test time: 57.0
Accumulative iteration time: 15.0
Weighted Recall: 0.698
Weighted FalsePositiveRate: 0.4151428571428571
Kappa statistic: 0.2823193916349808
Training time: 0.0
		
Iteration: 17
Labeled set size: 420
Unlabelled set size: 80
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.0
Correctly Classified Instances: 70.0
Weighted Precision: 0.6977401129943502
Weighted AreaUnderROC: 0.6390476190476191
Root mean squared error: 0.5464282335126273
Relative absolute error: 60.189573459715575
Root relative squared error: 109.28564670252545
Weighted TruePositiveRate: 0.7
Weighted MatthewsCorrelation: 0.2802819595747497
Weighted FMeasure: 0.6988252457002456
Iteration time: 1.0
Weighted AreaUnderPRC: 0.654897175141243
Mean absolute error: 0.30094786729857786
Coverage of cases: 70.0
Instances selection time: 1.0
Test time: 59.0
Accumulative iteration time: 16.0
Weighted Recall: 0.7
Weighted FalsePositiveRate: 0.4219047619047619
Kappa statistic: 0.2802303262955852
Training time: 0.0
		
Iteration: 18
Labeled set size: 440
Unlabelled set size: 60
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.0
Correctly Classified Instances: 70.0
Weighted Precision: 0.6977401129943502
Weighted AreaUnderROC: 0.6390476190476191
Root mean squared error: 0.5464866444726411
Relative absolute error: 60.18099547511322
Root relative squared error: 109.2973288945282
Weighted TruePositiveRate: 0.7
Weighted MatthewsCorrelation: 0.2802819595747497
Weighted FMeasure: 0.6988252457002456
Iteration time: 1.0
Weighted AreaUnderPRC: 0.654897175141243
Mean absolute error: 0.3009049773755661
Coverage of cases: 70.0
Instances selection time: 1.0
Test time: 61.0
Accumulative iteration time: 17.0
Weighted Recall: 0.7
Weighted FalsePositiveRate: 0.4219047619047619
Kappa statistic: 0.2802303262955852
Training time: 0.0
		
Iteration: 19
Labeled set size: 460
Unlabelled set size: 40
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.2
Correctly Classified Instances: 69.8
Weighted Precision: 0.6940490881667353
Weighted AreaUnderROC: 0.6338095238095238
Root mean squared error: 0.5483587564380294
Relative absolute error: 60.57142857142866
Root relative squared error: 109.67175128760589
Weighted TruePositiveRate: 0.698
Weighted MatthewsCorrelation: 0.2713904403838918
Weighted FMeasure: 0.6958875409725271
Iteration time: 1.0
Weighted AreaUnderPRC: 0.6516670819376702
Mean absolute error: 0.3028571428571433
Coverage of cases: 69.8
Instances selection time: 1.0
Test time: 63.0
Accumulative iteration time: 18.0
Weighted Recall: 0.698
Weighted FalsePositiveRate: 0.43038095238095236
Kappa statistic: 0.2712355212355211
Training time: 0.0
		
Iteration: 20
Labeled set size: 480
Unlabelled set size: 20
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.2
Correctly Classified Instances: 69.8
Weighted Precision: 0.6962883737064232
Weighted AreaUnderROC: 0.6376190476190478
Root mean squared error: 0.5484078703851778
Relative absolute error: 60.56431535269715
Root relative squared error: 109.68157407703556
Weighted TruePositiveRate: 0.698
Weighted MatthewsCorrelation: 0.2768485003374742
Weighted FMeasure: 0.697119128698076
Iteration time: 1.0
Weighted AreaUnderPRC: 0.6538954693492127
Mean absolute error: 0.30282157676348576
Coverage of cases: 69.8
Instances selection time: 1.0
Test time: 65.0
Accumulative iteration time: 19.0
Weighted Recall: 0.698
Weighted FalsePositiveRate: 0.4227619047619048
Kappa statistic: 0.27681992337164735
Training time: 0.0
		
Time end:Tue Oct 31 11.34.21 EET 2017