Tue Oct 31 11.35.40 EET 2017
Dataset: german_credit
Test set size: 500
Initial Labelled set size: 100
Initial Unlabelled set size: 400
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Tue Oct 31 11.35.40 EET 2017
		
Iteration: 1
Labeled set size: 100
Unlabelled set size: 400
	
Mean region size: 94.3
Incorrectly Classified Instances: 30.8
Correctly Classified Instances: 69.2
Weighted Precision: 0.6558333333333333
Weighted AreaUnderROC: 0.6062190476190477
Root mean squared error: 0.4916913900870568
Relative absolute error: 81.81020703933739
Root relative squared error: 98.33827801741137
Weighted TruePositiveRate: 0.692
Weighted MatthewsCorrelation: 0.16666666666666666
Weighted FMeasure: 0.6591304347826088
Iteration time: 2.0
Weighted AreaUnderPRC: 0.6374207404248268
Mean absolute error: 0.4090510351966869
Coverage of cases: 93.8
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 2.0
Weighted Recall: 0.692
Weighted FalsePositiveRate: 0.5586666666666668
Kappa statistic: 0.1538461538461537
Training time: 2.0
		
Iteration: 2
Labeled set size: 120
Unlabelled set size: 380
	
Mean region size: 91.7
Incorrectly Classified Instances: 28.0
Correctly Classified Instances: 72.0
Weighted Precision: 0.7067395264116576
Weighted AreaUnderROC: 0.6499714285714285
Root mean squared error: 0.47838785718167115
Relative absolute error: 68.57983488132072
Root relative squared error: 95.67757143633423
Weighted TruePositiveRate: 0.72
Weighted MatthewsCorrelation: 0.29875271984342394
Weighted FMeasure: 0.7109728506787331
Iteration time: 3.0
Weighted AreaUnderPRC: 0.6700861287952621
Mean absolute error: 0.3428991744066036
Coverage of cases: 91.4
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 5.0
Weighted Recall: 0.72
Weighted FalsePositiveRate: 0.44000000000000006
Kappa statistic: 0.29577464788732377
Training time: 2.0
		
Iteration: 3
Labeled set size: 140
Unlabelled set size: 360
	
Mean region size: 100.0
Incorrectly Classified Instances: 30.0
Correctly Classified Instances: 70.0
Weighted Precision: 0.48999999999999994
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.4591473908568059
Relative absolute error: 81.71428571428578
Root relative squared error: 91.82947817136117
Weighted TruePositiveRate: 0.7
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.5764705882352942
Iteration time: 3.0
Weighted AreaUnderPRC: 0.58
Mean absolute error: 0.40857142857142886
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 8.0
Weighted Recall: 0.7
Weighted FalsePositiveRate: 0.7
Kappa statistic: 0.0
Training time: 3.0
		
Iteration: 4
Labeled set size: 160
Unlabelled set size: 340
	
Mean region size: 100.0
Incorrectly Classified Instances: 30.0
Correctly Classified Instances: 70.0
Weighted Precision: 0.48999999999999994
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.4625000000000001
Relative absolute error: 78.99999999999943
Root relative squared error: 92.50000000000001
Weighted TruePositiveRate: 0.7
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.5764705882352942
Iteration time: 4.0
Weighted AreaUnderPRC: 0.58
Mean absolute error: 0.39499999999999713
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 12.0
Weighted Recall: 0.7
Weighted FalsePositiveRate: 0.7
Kappa statistic: 0.0
Training time: 3.0
		
Iteration: 5
Labeled set size: 180
Unlabelled set size: 320
	
Mean region size: 100.0
Incorrectly Classified Instances: 30.0
Correctly Classified Instances: 70.0
Weighted Precision: 0.48999999999999994
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.4667989230577775
Relative absolute error: 76.88888888888886
Root relative squared error: 93.3597846115555
Weighted TruePositiveRate: 0.7
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.5764705882352942
Iteration time: 5.0
Weighted AreaUnderPRC: 0.58
Mean absolute error: 0.3844444444444443
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 17.0
Weighted Recall: 0.7
Weighted FalsePositiveRate: 0.7
Kappa statistic: 0.0
Training time: 4.0
		
Iteration: 6
Labeled set size: 200
Unlabelled set size: 300
	
Mean region size: 100.0
Incorrectly Classified Instances: 30.0
Correctly Classified Instances: 70.0
Weighted Precision: 0.48999999999999994
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.47127486671792646
Relative absolute error: 75.20000000000002
Root relative squared error: 94.25497334358529
Weighted TruePositiveRate: 0.7
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.5764705882352942
Iteration time: 4.0
Weighted AreaUnderPRC: 0.58
Mean absolute error: 0.37600000000000006
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 21.0
Weighted Recall: 0.7
Weighted FalsePositiveRate: 0.7
Kappa statistic: 0.0
Training time: 3.0
		
Iteration: 7
Labeled set size: 220
Unlabelled set size: 280
	
Mean region size: 100.0
Incorrectly Classified Instances: 30.0
Correctly Classified Instances: 70.0
Weighted Precision: 0.48999999999999994
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.4756031403464843
Relative absolute error: 73.81818181818139
Root relative squared error: 95.12062806929687
Weighted TruePositiveRate: 0.7
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.5764705882352942
Iteration time: 4.0
Weighted AreaUnderPRC: 0.58
Mean absolute error: 0.3690909090909069
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 25.0
Weighted Recall: 0.7
Weighted FalsePositiveRate: 0.7
Kappa statistic: 0.0
Training time: 4.0
		
Iteration: 8
Labeled set size: 240
Unlabelled set size: 260
	
Mean region size: 100.0
Incorrectly Classified Instances: 30.0
Correctly Classified Instances: 70.0
Weighted Precision: 0.48999999999999994
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.4796555477052719
Relative absolute error: 72.66666666666673
Root relative squared error: 95.93110954105438
Weighted TruePositiveRate: 0.7
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.5764705882352942
Iteration time: 4.0
Weighted AreaUnderPRC: 0.58
Mean absolute error: 0.3633333333333337
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 29.0
Weighted Recall: 0.7
Weighted FalsePositiveRate: 0.7
Kappa statistic: 0.0
Training time: 4.0
		
Iteration: 9
Labeled set size: 260
Unlabelled set size: 240
	
Mean region size: 100.0
Incorrectly Classified Instances: 30.0
Correctly Classified Instances: 70.0
Weighted Precision: 0.48999999999999994
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.4833928413343075
Relative absolute error: 71.69230769230738
Root relative squared error: 96.6785682668615
Weighted TruePositiveRate: 0.7
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.5764705882352942
Iteration time: 4.0
Weighted AreaUnderPRC: 0.58
Mean absolute error: 0.3584615384615369
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 33.0
Weighted Recall: 0.7
Weighted FalsePositiveRate: 0.7
Kappa statistic: 0.0
Training time: 4.0
		
Iteration: 10
Labeled set size: 280
Unlabelled set size: 220
	
Mean region size: 100.0
Incorrectly Classified Instances: 30.0
Correctly Classified Instances: 70.0
Weighted Precision: 0.48999999999999994
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.4868159774682496
Relative absolute error: 70.8571428571432
Root relative squared error: 97.36319549364993
Weighted TruePositiveRate: 0.7
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.5764705882352942
Iteration time: 4.0
Weighted AreaUnderPRC: 0.58
Mean absolute error: 0.35428571428571604
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 37.0
Weighted Recall: 0.7
Weighted FalsePositiveRate: 0.7
Kappa statistic: 0.0
Training time: 4.0
		
Iteration: 11
Labeled set size: 300
Unlabelled set size: 200
	
Mean region size: 100.0
Incorrectly Classified Instances: 30.0
Correctly Classified Instances: 70.0
Weighted Precision: 0.48999999999999994
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.4899433073779508
Relative absolute error: 70.1333333333333
Root relative squared error: 97.98866147559015
Weighted TruePositiveRate: 0.7
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.5764705882352942
Iteration time: 5.0
Weighted AreaUnderPRC: 0.58
Mean absolute error: 0.35066666666666646
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 42.0
Weighted Recall: 0.7
Weighted FalsePositiveRate: 0.7
Kappa statistic: 0.0
Training time: 4.0
		
Iteration: 12
Labeled set size: 320
Unlabelled set size: 180
	
Mean region size: 100.0
Incorrectly Classified Instances: 30.0
Correctly Classified Instances: 70.0
Weighted Precision: 0.48999999999999994
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.4927997184455371
Relative absolute error: 69.49999999999963
Root relative squared error: 98.55994368910743
Weighted TruePositiveRate: 0.7
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.5764705882352942
Iteration time: 4.0
Weighted AreaUnderPRC: 0.58
Mean absolute error: 0.34749999999999814
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 46.0
Weighted Recall: 0.7
Weighted FalsePositiveRate: 0.7
Kappa statistic: 0.0
Training time: 4.0
		
Iteration: 13
Labeled set size: 340
Unlabelled set size: 160
	
Mean region size: 100.0
Incorrectly Classified Instances: 30.0
Correctly Classified Instances: 70.0
Weighted Precision: 0.48999999999999994
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.4954114713564451
Relative absolute error: 68.94117647058809
Root relative squared error: 99.08229427128902
Weighted TruePositiveRate: 0.7
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.5764705882352942
Iteration time: 5.0
Weighted AreaUnderPRC: 0.58
Mean absolute error: 0.3447058823529405
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 51.0
Weighted Recall: 0.7
Weighted FalsePositiveRate: 0.7
Kappa statistic: 0.0
Training time: 5.0
		
Iteration: 14
Labeled set size: 360
Unlabelled set size: 140
	
Mean region size: 100.0
Incorrectly Classified Instances: 30.0
Correctly Classified Instances: 70.0
Weighted Precision: 0.48999999999999994
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.4978038187632852
Relative absolute error: 68.44444444444441
Root relative squared error: 99.56076375265704
Weighted TruePositiveRate: 0.7
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.5764705882352942
Iteration time: 5.0
Weighted AreaUnderPRC: 0.58
Mean absolute error: 0.3422222222222221
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 56.0
Weighted Recall: 0.7
Weighted FalsePositiveRate: 0.7
Kappa statistic: 0.0
Training time: 4.0
		
Iteration: 15
Labeled set size: 380
Unlabelled set size: 120
	
Mean region size: 100.0
Incorrectly Classified Instances: 30.0
Correctly Classified Instances: 70.0
Weighted Precision: 0.48999999999999994
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.5000000000000004
Relative absolute error: 68.00000000000034
Root relative squared error: 100.00000000000009
Weighted TruePositiveRate: 0.7
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.5764705882352942
Iteration time: 5.0
Weighted AreaUnderPRC: 0.58
Mean absolute error: 0.3400000000000017
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 61.0
Weighted Recall: 0.7
Weighted FalsePositiveRate: 0.7
Kappa statistic: 0.0
Training time: 5.0
		
Iteration: 16
Labeled set size: 400
Unlabelled set size: 100
	
Mean region size: 100.0
Incorrectly Classified Instances: 30.0
Correctly Classified Instances: 70.0
Weighted Precision: 0.48999999999999994
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.4905354217587146
Relative absolute error: 70.0
Root relative squared error: 98.10708435174293
Weighted TruePositiveRate: 0.7
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.5764705882352942
Iteration time: 6.0
Weighted AreaUnderPRC: 0.58
Mean absolute error: 0.35
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 67.0
Weighted Recall: 0.7
Weighted FalsePositiveRate: 0.7
Kappa statistic: 0.0
Training time: 5.0
		
Iteration: 17
Labeled set size: 420
Unlabelled set size: 80
	
Mean region size: 100.0
Incorrectly Classified Instances: 30.0
Correctly Classified Instances: 70.0
Weighted Precision: 0.48999999999999994
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.4772607021092114
Relative absolute error: 73.33333333333333
Root relative squared error: 95.45214042184227
Weighted TruePositiveRate: 0.7
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.5764705882352942
Iteration time: 6.0
Weighted AreaUnderPRC: 0.58
Mean absolute error: 0.36666666666666664
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 73.0
Weighted Recall: 0.7
Weighted FalsePositiveRate: 0.7
Kappa statistic: 0.0
Training time: 5.0
		
Iteration: 18
Labeled set size: 440
Unlabelled set size: 60
	
Mean region size: 100.0
Incorrectly Classified Instances: 30.0
Correctly Classified Instances: 70.0
Weighted Precision: 0.48999999999999994
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.4680935486074706
Relative absolute error: 76.36363636363586
Root relative squared error: 93.61870972149411
Weighted TruePositiveRate: 0.7
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.5764705882352942
Iteration time: 6.0
Weighted AreaUnderPRC: 0.58
Mean absolute error: 0.38181818181817934
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 79.0
Weighted Recall: 0.7
Weighted FalsePositiveRate: 0.7
Kappa statistic: 0.0
Training time: 6.0
		
Iteration: 19
Labeled set size: 460
Unlabelled set size: 40
	
Mean region size: 100.0
Incorrectly Classified Instances: 30.0
Correctly Classified Instances: 70.0
Weighted Precision: 0.48999999999999994
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.4622824936873054
Relative absolute error: 79.13043478260926
Root relative squared error: 92.45649873746108
Weighted TruePositiveRate: 0.7
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.5764705882352942
Iteration time: 7.0
Weighted AreaUnderPRC: 0.58
Mean absolute error: 0.3956521739130463
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 86.0
Weighted Recall: 0.7
Weighted FalsePositiveRate: 0.7
Kappa statistic: 0.0
Training time: 7.0
		
Iteration: 20
Labeled set size: 480
Unlabelled set size: 20
	
Mean region size: 98.2
Incorrectly Classified Instances: 29.2
Correctly Classified Instances: 70.8
Weighted Precision: 0.6703122007278299
Weighted AreaUnderROC: 0.7413809523809524
Root mean squared error: 0.4372398044452769
Relative absolute error: 70.68467377165966
Root relative squared error: 87.44796088905538
Weighted TruePositiveRate: 0.708
Weighted MatthewsCorrelation: 0.16912255576554946
Weighted FMeasure: 0.6494161843842013
Iteration time: 9.0
Weighted AreaUnderPRC: 0.7351987736577378
Mean absolute error: 0.3534233688582983
Coverage of cases: 98.4
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 95.0
Weighted Recall: 0.708
Weighted FalsePositiveRate: 0.6013333333333334
Kappa statistic: 0.13301662707838466
Training time: 9.0
		
Time end:Tue Oct 31 11.35.41 EET 2017