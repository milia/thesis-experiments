Tue Oct 31 11.29.45 EET 2017
Dataset: german_credit
Test set size: 500
Initial Labelled set size: 100
Initial Unlabelled set size: 400
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Tue Oct 31 11.29.45 EET 2017
		
Iteration: 1
Labeled set size: 100
Unlabelled set size: 400
	
Mean region size: 88.9
Incorrectly Classified Instances: 27.4
Correctly Classified Instances: 72.6
Weighted Precision: 0.7218584496870486
Weighted AreaUnderROC: 0.7014828571428572
Root mean squared error: 0.4416433469652845
Relative absolute error: 66.44276031175568
Root relative squared error: 88.3286693930569
Weighted TruePositiveRate: 0.726
Weighted MatthewsCorrelation: 0.22984063101331753
Weighted FMeasure: 0.6558341597015631
Iteration time: 114.0
Weighted AreaUnderPRC: 0.7243548158613279
Mean absolute error: 0.3322138015587784
Coverage of cases: 97.4
Instances selection time: 42.0
Test time: 19.0
Accumulative iteration time: 114.0
Weighted Recall: 0.726
Weighted FalsePositiveRate: 0.6050476190476191
Kappa statistic: 0.15640394088669937
Training time: 72.0
		
Iteration: 2
Labeled set size: 120
Unlabelled set size: 380
	
Mean region size: 92.0
Incorrectly Classified Instances: 26.0
Correctly Classified Instances: 74.0
Weighted Precision: 0.732853982300885
Weighted AreaUnderROC: 0.7451285714285715
Root mean squared error: 0.42332698793913875
Relative absolute error: 66.26457094797573
Root relative squared error: 84.66539758782775
Weighted TruePositiveRate: 0.74
Weighted MatthewsCorrelation: 0.29037323826836764
Weighted FMeasure: 0.6895639688657145
Iteration time: 30.0
Weighted AreaUnderPRC: 0.7642493493651072
Mean absolute error: 0.3313228547398786
Coverage of cases: 98.8
Instances selection time: 5.0
Test time: 5.0
Accumulative iteration time: 144.0
Weighted Recall: 0.74
Weighted FalsePositiveRate: 0.5533333333333333
Kappa statistic: 0.231678486997636
Training time: 25.0
		
Iteration: 3
Labeled set size: 140
Unlabelled set size: 360
	
Mean region size: 93.1
Incorrectly Classified Instances: 29.2
Correctly Classified Instances: 70.8
Weighted Precision: 0.6706666666666667
Weighted AreaUnderROC: 0.6870352380952381
Root mean squared error: 0.44165299041730177
Relative absolute error: 69.72505314973458
Root relative squared error: 88.33059808346036
Weighted TruePositiveRate: 0.708
Weighted MatthewsCorrelation: 0.1745743121887939
Weighted FMeasure: 0.65325
Iteration time: 34.0
Weighted AreaUnderPRC: 0.7098334107827992
Mean absolute error: 0.3486252657486729
Coverage of cases: 98.2
Instances selection time: 4.0
Test time: 10.0
Accumulative iteration time: 178.0
Weighted Recall: 0.708
Weighted FalsePositiveRate: 0.5937142857142856
Kappa statistic: 0.1411764705882351
Training time: 30.0
		
Iteration: 4
Labeled set size: 160
Unlabelled set size: 340
	
Mean region size: 92.0
Incorrectly Classified Instances: 29.2
Correctly Classified Instances: 70.8
Weighted Precision: 0.6718832891246684
Weighted AreaUnderROC: 0.7073333333333334
Root mean squared error: 0.4417600501685491
Relative absolute error: 68.49056439049097
Root relative squared error: 88.35201003370982
Weighted TruePositiveRate: 0.708
Weighted MatthewsCorrelation: 0.18535492480522767
Weighted FMeasure: 0.6603826728826728
Iteration time: 36.0
Weighted AreaUnderPRC: 0.7291757689044405
Mean absolute error: 0.34245282195245486
Coverage of cases: 98.4
Instances selection time: 3.0
Test time: 14.0
Accumulative iteration time: 214.0
Weighted Recall: 0.708
Weighted FalsePositiveRate: 0.5784761904761905
Kappa statistic: 0.15704387990762125
Training time: 33.0
		
Iteration: 5
Labeled set size: 180
Unlabelled set size: 320
	
Mean region size: 95.0
Incorrectly Classified Instances: 28.0
Correctly Classified Instances: 72.0
Weighted Precision: 0.6925759840558046
Weighted AreaUnderROC: 0.6657361904761905
Root mean squared error: 0.44470865774757146
Relative absolute error: 72.73619763939828
Root relative squared error: 88.94173154951429
Weighted TruePositiveRate: 0.72
Weighted MatthewsCorrelation: 0.22216883122584308
Weighted FMeasure: 0.6710020691693763
Iteration time: 39.0
Weighted AreaUnderPRC: 0.6999436141354188
Mean absolute error: 0.3636809881969914
Coverage of cases: 98.6
Instances selection time: 2.0
Test time: 9.0
Accumulative iteration time: 253.0
Weighted Recall: 0.72
Weighted FalsePositiveRate: 0.5695238095238095
Kappa statistic: 0.18414918414918396
Training time: 37.0
		
Iteration: 6
Labeled set size: 200
Unlabelled set size: 300
	
Mean region size: 95.6
Incorrectly Classified Instances: 31.6
Correctly Classified Instances: 68.4
Weighted Precision: 0.6668421052631579
Weighted AreaUnderROC: 0.701995238095238
Root mean squared error: 0.44157012643615434
Relative absolute error: 72.99351975457098
Root relative squared error: 88.31402528723086
Weighted TruePositiveRate: 0.684
Weighted MatthewsCorrelation: 0.20437977982832192
Weighted FMeasure: 0.672937595129376
Iteration time: 48.0
Weighted AreaUnderPRC: 0.7272409226971073
Mean absolute error: 0.3649675987728549
Coverage of cases: 99.4
Instances selection time: 8.0
Test time: 5.0
Accumulative iteration time: 301.0
Weighted Recall: 0.684
Weighted FalsePositiveRate: 0.49352380952380953
Kappa statistic: 0.20202020202020218
Training time: 40.0
		
Iteration: 7
Labeled set size: 220
Unlabelled set size: 280
	
Mean region size: 94.2
Incorrectly Classified Instances: 26.8
Correctly Classified Instances: 73.2
Weighted Precision: 0.7125494974109047
Weighted AreaUnderROC: 0.657552380952381
Root mean squared error: 0.45327433772391656
Relative absolute error: 75.54640476598101
Root relative squared error: 90.65486754478331
Weighted TruePositiveRate: 0.732
Weighted MatthewsCorrelation: 0.3034403777260295
Weighted FMeasure: 0.7131691832532601
Iteration time: 50.0
Weighted AreaUnderPRC: 0.6940627806791437
Mean absolute error: 0.37773202382990506
Coverage of cases: 98.0
Instances selection time: 7.0
Test time: 9.0
Accumulative iteration time: 351.0
Weighted Recall: 0.732
Weighted FalsePositiveRate: 0.4691428571428572
Kappa statistic: 0.2917547568710358
Training time: 43.0
		
Iteration: 8
Labeled set size: 240
Unlabelled set size: 260
	
Mean region size: 95.2
Incorrectly Classified Instances: 28.6
Correctly Classified Instances: 71.4
Weighted Precision: 0.6886778258416401
Weighted AreaUnderROC: 0.6865028571428571
Root mean squared error: 0.4403575596290004
Relative absolute error: 74.23928210916324
Root relative squared error: 88.07151192580008
Weighted TruePositiveRate: 0.714
Weighted MatthewsCorrelation: 0.24545265624408186
Weighted FMeasure: 0.6901076432737988
Iteration time: 55.0
Weighted AreaUnderPRC: 0.7304861805955256
Mean absolute error: 0.3711964105458162
Coverage of cases: 99.0
Instances selection time: 6.0
Test time: 8.0
Accumulative iteration time: 406.0
Weighted Recall: 0.714
Weighted FalsePositiveRate: 0.5073333333333333
Kappa statistic: 0.23283261802575103
Training time: 49.0
		
Iteration: 9
Labeled set size: 260
Unlabelled set size: 240
	
Mean region size: 95.0
Incorrectly Classified Instances: 29.0
Correctly Classified Instances: 71.0
Weighted Precision: 0.6817150956768249
Weighted AreaUnderROC: 0.6784847619047618
Root mean squared error: 0.44519650762514035
Relative absolute error: 73.80256323536219
Root relative squared error: 89.03930152502807
Weighted TruePositiveRate: 0.71
Weighted MatthewsCorrelation: 0.22656428651458624
Weighted FMeasure: 0.682213878459185
Iteration time: 50.0
Weighted AreaUnderPRC: 0.7107620983286099
Mean absolute error: 0.3690128161768109
Coverage of cases: 98.4
Instances selection time: 8.0
Test time: 8.0
Accumulative iteration time: 456.0
Weighted Recall: 0.71
Weighted FalsePositiveRate: 0.5242857142857142
Kappa statistic: 0.21195652173913032
Training time: 42.0
		
Iteration: 10
Labeled set size: 280
Unlabelled set size: 220
	
Mean region size: 95.4
Incorrectly Classified Instances: 27.4
Correctly Classified Instances: 72.6
Weighted Precision: 0.7056071941358723
Weighted AreaUnderROC: 0.7041895238095237
Root mean squared error: 0.43892456596631485
Relative absolute error: 71.74612581832014
Root relative squared error: 87.78491319326297
Weighted TruePositiveRate: 0.726
Weighted MatthewsCorrelation: 0.28804233146192354
Weighted FMeasure: 0.707243354242536
Iteration time: 42.0
Weighted AreaUnderPRC: 0.723344029125693
Mean absolute error: 0.3587306290916007
Coverage of cases: 99.2
Instances selection time: 7.0
Test time: 8.0
Accumulative iteration time: 498.0
Weighted Recall: 0.726
Weighted FalsePositiveRate: 0.4755238095238095
Kappa statistic: 0.27742616033755263
Training time: 35.0
		
Iteration: 11
Labeled set size: 300
Unlabelled set size: 200
	
Mean region size: 92.3
Incorrectly Classified Instances: 27.2
Correctly Classified Instances: 72.8
Weighted Precision: 0.7050480769230769
Weighted AreaUnderROC: 0.7385733333333333
Root mean squared error: 0.4257215520746146
Relative absolute error: 67.79012145165244
Root relative squared error: 85.14431041492291
Weighted TruePositiveRate: 0.728
Weighted MatthewsCorrelation: 0.27783119144576074
Weighted FMeasure: 0.7013590413068219
Iteration time: 47.0
Weighted AreaUnderPRC: 0.7597972128126381
Mean absolute error: 0.3389506072582622
Coverage of cases: 99.4
Instances selection time: 9.0
Test time: 9.0
Accumulative iteration time: 545.0
Weighted Recall: 0.728
Weighted FalsePositiveRate: 0.5013333333333333
Kappa statistic: 0.25925925925925913
Training time: 38.0
		
Iteration: 12
Labeled set size: 320
Unlabelled set size: 180
	
Mean region size: 92.7
Incorrectly Classified Instances: 27.6
Correctly Classified Instances: 72.4
Weighted Precision: 0.6991666666666666
Weighted AreaUnderROC: 0.7116476190476191
Root mean squared error: 0.435297614658266
Relative absolute error: 69.82695990464484
Root relative squared error: 87.0595229316532
Weighted TruePositiveRate: 0.724
Weighted MatthewsCorrelation: 0.2619047619047619
Weighted FMeasure: 0.6945454545454546
Iteration time: 41.0
Weighted AreaUnderPRC: 0.7365179836231011
Mean absolute error: 0.3491347995232242
Coverage of cases: 98.2
Instances selection time: 10.0
Test time: 7.0
Accumulative iteration time: 586.0
Weighted Recall: 0.724
Weighted FalsePositiveRate: 0.5144761904761905
Kappa statistic: 0.24175824175824168
Training time: 31.0
		
Iteration: 13
Labeled set size: 340
Unlabelled set size: 160
	
Mean region size: 93.2
Incorrectly Classified Instances: 27.0
Correctly Classified Instances: 73.0
Weighted Precision: 0.7072221052948073
Weighted AreaUnderROC: 0.7058780952380952
Root mean squared error: 0.4352815689991647
Relative absolute error: 69.38759714850036
Root relative squared error: 87.05631379983294
Weighted TruePositiveRate: 0.73
Weighted MatthewsCorrelation: 0.2787993006938226
Weighted FMeasure: 0.7005760114182793
Iteration time: 49.0
Weighted AreaUnderPRC: 0.7294221726782589
Mean absolute error: 0.34693798574250184
Coverage of cases: 98.2
Instances selection time: 7.0
Test time: 7.0
Accumulative iteration time: 635.0
Weighted Recall: 0.73
Weighted FalsePositiveRate: 0.508095238095238
Kappa statistic: 0.2566079295154184
Training time: 42.0
		
Iteration: 14
Labeled set size: 360
Unlabelled set size: 140
	
Mean region size: 91.8
Incorrectly Classified Instances: 26.2
Correctly Classified Instances: 73.8
Weighted Precision: 0.7192592592592593
Weighted AreaUnderROC: 0.71054
Root mean squared error: 0.4325114657652107
Relative absolute error: 67.75698115159346
Root relative squared error: 86.50229315304215
Weighted TruePositiveRate: 0.738
Weighted MatthewsCorrelation: 0.3170632437371139
Weighted FMeasure: 0.7181348830923097
Iteration time: 46.0
Weighted AreaUnderPRC: 0.7351785524896466
Mean absolute error: 0.3387849057579673
Coverage of cases: 97.4
Instances selection time: 6.0
Test time: 7.0
Accumulative iteration time: 681.0
Weighted Recall: 0.738
Weighted FalsePositiveRate: 0.4665714285714286
Kappa statistic: 0.3031914893617021
Training time: 40.0
		
Iteration: 15
Labeled set size: 380
Unlabelled set size: 120
	
Mean region size: 92.3
Incorrectly Classified Instances: 28.6
Correctly Classified Instances: 71.4
Weighted Precision: 0.6858746574737028
Weighted AreaUnderROC: 0.7063400000000001
Root mean squared error: 0.4342842410967165
Relative absolute error: 70.04341320232902
Root relative squared error: 86.85684821934329
Weighted TruePositiveRate: 0.714
Weighted MatthewsCorrelation: 0.23334959844356853
Weighted FMeasure: 0.6841166635704997
Iteration time: 53.0
Weighted AreaUnderPRC: 0.7454282116575465
Mean absolute error: 0.3502170660116451
Coverage of cases: 98.4
Instances selection time: 10.0
Test time: 7.0
Accumulative iteration time: 734.0
Weighted Recall: 0.714
Weighted FalsePositiveRate: 0.5263809523809524
Kappa statistic: 0.2160087719298245
Training time: 43.0
		
Iteration: 16
Labeled set size: 400
Unlabelled set size: 100
	
Mean region size: 93.6
Incorrectly Classified Instances: 25.8
Correctly Classified Instances: 74.2
Weighted Precision: 0.7262776304155615
Weighted AreaUnderROC: 0.7513485714285714
Root mean squared error: 0.42017385849892613
Relative absolute error: 67.52239755602238
Root relative squared error: 84.03477169978522
Weighted TruePositiveRate: 0.742
Weighted MatthewsCorrelation: 0.3049698513754918
Weighted FMeasure: 0.704968152866242
Iteration time: 46.0
Weighted AreaUnderPRC: 0.765746083971926
Mean absolute error: 0.3376119877801119
Coverage of cases: 99.0
Instances selection time: 8.0
Test time: 17.0
Accumulative iteration time: 780.0
Weighted Recall: 0.742
Weighted FalsePositiveRate: 0.5181904761904763
Kappa statistic: 0.2670454545454545
Training time: 38.0
		
Iteration: 17
Labeled set size: 420
Unlabelled set size: 80
	
Mean region size: 96.4
Incorrectly Classified Instances: 28.4
Correctly Classified Instances: 71.6
Weighted Precision: 0.6874875868917577
Weighted AreaUnderROC: 0.7050819047619048
Root mean squared error: 0.4367770609955111
Relative absolute error: 73.47522139536747
Root relative squared error: 87.35541219910222
Weighted TruePositiveRate: 0.716
Weighted MatthewsCorrelation: 0.23340054021346923
Weighted FMeasure: 0.6830806521689419
Iteration time: 45.0
Weighted AreaUnderPRC: 0.7188885417119626
Mean absolute error: 0.3673761069768373
Coverage of cases: 98.8
Instances selection time: 7.0
Test time: 17.0
Accumulative iteration time: 825.0
Weighted Recall: 0.716
Weighted FalsePositiveRate: 0.5331428571428571
Kappa statistic: 0.21286031042128598
Training time: 38.0
		
Iteration: 18
Labeled set size: 440
Unlabelled set size: 60
	
Mean region size: 93.9
Incorrectly Classified Instances: 26.2
Correctly Classified Instances: 73.8
Weighted Precision: 0.7185492926117224
Weighted AreaUnderROC: 0.7059285714285715
Root mean squared error: 0.4309146758527655
Relative absolute error: 68.68643131058067
Root relative squared error: 86.1829351705531
Weighted TruePositiveRate: 0.738
Weighted MatthewsCorrelation: 0.29787360922596395
Weighted FMeasure: 0.7057487981254796
Iteration time: 47.0
Weighted AreaUnderPRC: 0.735706162569068
Mean absolute error: 0.3434321565529034
Coverage of cases: 98.2
Instances selection time: 6.0
Test time: 6.0
Accumulative iteration time: 872.0
Weighted Recall: 0.738
Weighted FalsePositiveRate: 0.5084761904761905
Kappa statistic: 0.26897321428571436
Training time: 41.0
		
Iteration: 19
Labeled set size: 460
Unlabelled set size: 40
	
Mean region size: 93.6
Incorrectly Classified Instances: 26.4
Correctly Classified Instances: 73.6
Weighted Precision: 0.7156948657454218
Weighted AreaUnderROC: 0.7112228571428572
Root mean squared error: 0.432833584346425
Relative absolute error: 70.05388863651753
Root relative squared error: 86.566716869285
Weighted TruePositiveRate: 0.736
Weighted MatthewsCorrelation: 0.3029997343699373
Weighted FMeasure: 0.7112609814535451
Iteration time: 44.0
Weighted AreaUnderPRC: 0.7266869262009023
Mean absolute error: 0.3502694431825876
Coverage of cases: 98.2
Instances selection time: 7.0
Test time: 3.0
Accumulative iteration time: 916.0
Weighted Recall: 0.736
Weighted FalsePositiveRate: 0.4864761904761905
Kappa statistic: 0.2841648590021692
Training time: 37.0
		
Iteration: 20
Labeled set size: 480
Unlabelled set size: 20
	
Mean region size: 92.9
Incorrectly Classified Instances: 26.0
Correctly Classified Instances: 74.0
Weighted Precision: 0.7211382113821139
Weighted AreaUnderROC: 0.7520219047619047
Root mean squared error: 0.4194631007074144
Relative absolute error: 67.01570462082242
Root relative squared error: 83.89262014148288
Weighted TruePositiveRate: 0.74
Weighted MatthewsCorrelation: 0.31807914907579865
Weighted FMeasure: 0.7177631578947369
Iteration time: 42.0
Weighted AreaUnderPRC: 0.77351822017636
Mean absolute error: 0.33507852310411207
Coverage of cases: 98.6
Instances selection time: 4.0
Test time: 7.0
Accumulative iteration time: 958.0
Weighted Recall: 0.74
Weighted FalsePositiveRate: 0.47333333333333333
Kappa statistic: 0.30107526881720426
Training time: 38.0
		
Time end:Tue Oct 31 11.29.47 EET 2017