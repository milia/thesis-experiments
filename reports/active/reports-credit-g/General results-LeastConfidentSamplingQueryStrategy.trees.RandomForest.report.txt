Tue Oct 31 11.32.14 EET 2017
Dataset: german_credit
Test set size: 500
Initial Labelled set size: 100
Initial Unlabelled set size: 400
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Tue Oct 31 11.32.14 EET 2017
		
Iteration: 1
Labeled set size: 100
Unlabelled set size: 400
	
Mean region size: 94.49999999999999
Incorrectly Classified Instances: 30.139999999999997
Correctly Classified Instances: 69.85999999999999
Weighted Precision: 0.6706602245405048
Weighted AreaUnderROC: 0.6658564761904763
Root mean squared error: 0.45084023124331096
Relative absolute error: 74.28436135089188
Root relative squared error: 90.1680462486622
Weighted TruePositiveRate: 0.6986000000000001
Weighted MatthewsCorrelation: 0.18932537934046806
Weighted FMeasure: 0.6627331559123113
Iteration time: 5.0
Weighted AreaUnderPRC: 0.7019830862760867
Mean absolute error: 0.37142180675445935
Coverage of cases: 98.72
Instances selection time: 1.3
Test time: 2.2
Accumulative iteration time: 5.0
Weighted Recall: 0.6986000000000001
Weighted FalsePositiveRate: 0.5508857142857144
Kappa statistic: 0.16857903311705175
Training time: 3.7
		
Iteration: 2
Labeled set size: 120
Unlabelled set size: 380
	
Mean region size: 93.54999999999998
Incorrectly Classified Instances: 30.0
Correctly Classified Instances: 70.0
Weighted Precision: 0.6719564573245597
Weighted AreaUnderROC: 0.6840510476190476
Root mean squared error: 0.44494097215108963
Relative absolute error: 72.63294891150142
Root relative squared error: 88.98819443021793
Weighted TruePositiveRate: 0.6999999999999998
Weighted MatthewsCorrelation: 0.19825067479038566
Weighted FMeasure: 0.6685633795762402
Iteration time: 6.1
Weighted AreaUnderPRC: 0.7184192500081151
Mean absolute error: 0.36316474455750725
Coverage of cases: 98.46000000000001
Instances selection time: 1.2
Test time: 2.6
Accumulative iteration time: 11.1
Weighted Recall: 0.6999999999999998
Weighted FalsePositiveRate: 0.5430476190476191
Kappa statistic: 0.1803500560049975
Training time: 4.9
		
Iteration: 3
Labeled set size: 140
Unlabelled set size: 360
	
Mean region size: 94.52
Incorrectly Classified Instances: 30.0
Correctly Classified Instances: 69.99999999999999
Weighted Precision: 0.6699207596330824
Weighted AreaUnderROC: 0.6692270476190474
Root mean squared error: 0.4487639833044071
Relative absolute error: 74.27243776873617
Root relative squared error: 89.75279666088142
Weighted TruePositiveRate: 0.7
Weighted MatthewsCorrelation: 0.19648509251119725
Weighted FMeasure: 0.6683605487207143
Iteration time: 6.8
Weighted AreaUnderPRC: 0.7028954330897521
Mean absolute error: 0.3713621888436808
Coverage of cases: 98.66
Instances selection time: 1.5
Test time: 2.5
Accumulative iteration time: 17.9
Weighted Recall: 0.7
Weighted FalsePositiveRate: 0.54
Kappa statistic: 0.1809561160692941
Training time: 5.3
		
Iteration: 4
Labeled set size: 160
Unlabelled set size: 340
	
Mean region size: 94.22999999999999
Incorrectly Classified Instances: 29.740000000000002
Correctly Classified Instances: 70.26
Weighted Precision: 0.674723005780106
Weighted AreaUnderROC: 0.6924279999999999
Root mean squared error: 0.44167284902887366
Relative absolute error: 72.32030742593088
Root relative squared error: 88.33456980577475
Weighted TruePositiveRate: 0.7026
Weighted MatthewsCorrelation: 0.20098030120468705
Weighted FMeasure: 0.6682859863400376
Iteration time: 7.9
Weighted AreaUnderPRC: 0.7240950405083495
Mean absolute error: 0.3616015371296544
Coverage of cases: 98.94
Instances selection time: 1.5
Test time: 2.2
Accumulative iteration time: 25.8
Weighted Recall: 0.7026
Weighted FalsePositiveRate: 0.5465047619047618
Kappa statistic: 0.18038856341172257
Training time: 6.4
		
Iteration: 5
Labeled set size: 180
Unlabelled set size: 320
	
Mean region size: 95.09
Incorrectly Classified Instances: 29.1
Correctly Classified Instances: 70.9
Weighted Precision: 0.6829906492515646
Weighted AreaUnderROC: 0.6772512380952381
Root mean squared error: 0.4459325639830321
Relative absolute error: 73.70473631305791
Root relative squared error: 89.1865127966064
Weighted TruePositiveRate: 0.709
Weighted MatthewsCorrelation: 0.2283527737500742
Weighted FMeasure: 0.6812857960320067
Iteration time: 8.9
Weighted AreaUnderPRC: 0.7100435930671087
Mean absolute error: 0.36852368156528953
Coverage of cases: 98.82000000000001
Instances selection time: 1.4
Test time: 2.7
Accumulative iteration time: 34.7
Weighted Recall: 0.709
Weighted FalsePositiveRate: 0.519
Kappa statistic: 0.2133962972927843
Training time: 7.5
		
Iteration: 6
Labeled set size: 200
Unlabelled set size: 300
	
Mean region size: 95.42
Incorrectly Classified Instances: 29.28
Correctly Classified Instances: 70.72
Weighted Precision: 0.6811851786439006
Weighted AreaUnderROC: 0.6990441904761905
Root mean squared error: 0.43936626306219695
Relative absolute error: 73.35456001089406
Root relative squared error: 87.87325261243939
Weighted TruePositiveRate: 0.7071999999999999
Weighted MatthewsCorrelation: 0.22727690011190665
Weighted FMeasure: 0.6816667275686487
Iteration time: 10.8
Weighted AreaUnderPRC: 0.728134737888024
Mean absolute error: 0.3667728000544703
Coverage of cases: 99.16
Instances selection time: 1.5
Test time: 2.4
Accumulative iteration time: 45.5
Weighted Recall: 0.7071999999999999
Weighted FalsePositiveRate: 0.5140571428571429
Kappa statistic: 0.2148632527330431
Training time: 9.3
		
Iteration: 7
Labeled set size: 220
Unlabelled set size: 280
	
Mean region size: 95.07000000000002
Incorrectly Classified Instances: 28.919999999999998
Correctly Classified Instances: 71.08000000000001
Weighted Precision: 0.6868792363022422
Weighted AreaUnderROC: 0.6909742857142855
Root mean squared error: 0.4416112631944098
Relative absolute error: 73.62236440742592
Root relative squared error: 88.32225263888196
Weighted TruePositiveRate: 0.7107999999999999
Weighted MatthewsCorrelation: 0.24011535444984325
Weighted FMeasure: 0.686403083822802
Iteration time: 11.6
Weighted AreaUnderPRC: 0.7233485412601665
Mean absolute error: 0.3681118220371295
Coverage of cases: 98.96000000000001
Instances selection time: 1.6
Test time: 2.9
Accumulative iteration time: 57.1
Weighted Recall: 0.7107999999999999
Weighted FalsePositiveRate: 0.5060380952380952
Kappa statistic: 0.22697827351633415
Training time: 10.0
		
Iteration: 8
Labeled set size: 240
Unlabelled set size: 260
	
Mean region size: 95.28
Incorrectly Classified Instances: 29.359999999999996
Correctly Classified Instances: 70.63999999999999
Weighted Precision: 0.6822915399909968
Weighted AreaUnderROC: 0.7013687619047619
Root mean squared error: 0.43768117908279713
Relative absolute error: 73.40343465048713
Root relative squared error: 87.5362358165594
Weighted TruePositiveRate: 0.7063999999999999
Weighted MatthewsCorrelation: 0.23337241588707552
Weighted FMeasure: 0.6854131304290301
Iteration time: 11.3
Weighted AreaUnderPRC: 0.7359400020777132
Mean absolute error: 0.36701717325243566
Coverage of cases: 99.32
Instances selection time: 1.2
Test time: 2.5
Accumulative iteration time: 68.4
Weighted Recall: 0.7063999999999999
Weighted FalsePositiveRate: 0.5048761904761905
Kappa statistic: 0.22369229692396414
Training time: 10.1
		
Iteration: 9
Labeled set size: 260
Unlabelled set size: 240
	
Mean region size: 94.82000000000001
Incorrectly Classified Instances: 28.02
Correctly Classified Instances: 71.98
Weighted Precision: 0.6968301667629869
Weighted AreaUnderROC: 0.7089991428571427
Root mean squared error: 0.434924862865128
Relative absolute error: 72.01794031314921
Root relative squared error: 86.9849725730256
Weighted TruePositiveRate: 0.7198
Weighted MatthewsCorrelation: 0.2633694161102823
Weighted FMeasure: 0.6964119816807731
Iteration time: 12.2
Weighted AreaUnderPRC: 0.7389720721292858
Mean absolute error: 0.36008970156574616
Coverage of cases: 99.16
Instances selection time: 1.2
Test time: 2.7
Accumulative iteration time: 80.6
Weighted Recall: 0.7198
Weighted FalsePositiveRate: 0.49722857142857146
Kappa statistic: 0.24953803965832533
Training time: 11.0
		
Iteration: 10
Labeled set size: 280
Unlabelled set size: 220
	
Mean region size: 95.65
Incorrectly Classified Instances: 29.339999999999996
Correctly Classified Instances: 70.66
Weighted Precision: 0.6842434140974664
Weighted AreaUnderROC: 0.7030841904761905
Root mean squared error: 0.4390108909626534
Relative absolute error: 73.64095037273094
Root relative squared error: 87.80217819253066
Weighted TruePositiveRate: 0.7066
Weighted MatthewsCorrelation: 0.23925912533895163
Weighted FMeasure: 0.6876511339673212
Iteration time: 12.9
Weighted AreaUnderPRC: 0.7298445403099427
Mean absolute error: 0.36820475186365476
Coverage of cases: 99.08
Instances selection time: 1.2
Test time: 2.3
Accumulative iteration time: 93.5
Weighted Recall: 0.7066
Weighted FalsePositiveRate: 0.4967904761904762
Kappa statistic: 0.2307539121475172
Training time: 11.7
		
Iteration: 11
Labeled set size: 300
Unlabelled set size: 200
	
Mean region size: 95.14999999999999
Incorrectly Classified Instances: 27.5
Correctly Classified Instances: 72.5
Weighted Precision: 0.7048499802729082
Weighted AreaUnderROC: 0.7247839047619047
Root mean squared error: 0.43132153044583876
Relative absolute error: 71.40952158050469
Root relative squared error: 86.26430608916773
Weighted TruePositiveRate: 0.7249999999999999
Weighted MatthewsCorrelation: 0.2855273351191486
Weighted FMeasure: 0.705790709145115
Iteration time: 13.4
Weighted AreaUnderPRC: 0.7483724793316249
Mean absolute error: 0.35704760790252354
Coverage of cases: 99.16
Instances selection time: 1.2
Test time: 2.7
Accumulative iteration time: 106.9
Weighted Recall: 0.7249999999999999
Weighted FalsePositiveRate: 0.47671428571428576
Kappa statistic: 0.27437595904796996
Training time: 12.2
		
Iteration: 12
Labeled set size: 320
Unlabelled set size: 180
	
Mean region size: 94.75
Incorrectly Classified Instances: 28.240000000000002
Correctly Classified Instances: 71.76
Weighted Precision: 0.6948019966223417
Weighted AreaUnderROC: 0.7148863809523809
Root mean squared error: 0.4343333244014743
Relative absolute error: 71.66011182805983
Root relative squared error: 86.86666488029486
Weighted TruePositiveRate: 0.7176
Weighted MatthewsCorrelation: 0.26009949535699645
Weighted FMeasure: 0.6953049273495805
Iteration time: 14.7
Weighted AreaUnderPRC: 0.7406861938738858
Mean absolute error: 0.3583005591402992
Coverage of cases: 99.16
Instances selection time: 1.0
Test time: 2.6
Accumulative iteration time: 121.6
Weighted Recall: 0.7176
Weighted FalsePositiveRate: 0.4951238095238096
Kappa statistic: 0.24775959240367867
Training time: 13.7
		
Iteration: 13
Labeled set size: 340
Unlabelled set size: 160
	
Mean region size: 94.33
Incorrectly Classified Instances: 28.46
Correctly Classified Instances: 71.53999999999999
Weighted Precision: 0.691443313939389
Weighted AreaUnderROC: 0.7074306666666665
Root mean squared error: 0.4377534421920924
Relative absolute error: 72.06883727606584
Root relative squared error: 87.55068843841848
Weighted TruePositiveRate: 0.7154
Weighted MatthewsCorrelation: 0.2525260308333161
Weighted FMeasure: 0.6926763234923123
Iteration time: 15.6
Weighted AreaUnderPRC: 0.7319080841870662
Mean absolute error: 0.3603441863803292
Coverage of cases: 98.78
Instances selection time: 1.0
Test time: 2.7
Accumulative iteration time: 137.2
Weighted Recall: 0.7154
Weighted FalsePositiveRate: 0.5002571428571428
Kappa statistic: 0.24044567492701746
Training time: 14.6
		
Iteration: 14
Labeled set size: 360
Unlabelled set size: 140
	
Mean region size: 94.39
Incorrectly Classified Instances: 28.22
Correctly Classified Instances: 71.78
Weighted Precision: 0.6940498483777281
Weighted AreaUnderROC: 0.7098585714285714
Root mean squared error: 0.43538980785854003
Relative absolute error: 71.7434218308817
Root relative squared error: 87.07796157170802
Weighted TruePositiveRate: 0.7178
Weighted MatthewsCorrelation: 0.25620504168935776
Weighted FMeasure: 0.693209350770158
Iteration time: 15.8
Weighted AreaUnderPRC: 0.736094433576885
Mean absolute error: 0.35871710915440846
Coverage of cases: 98.96000000000002
Instances selection time: 0.8
Test time: 2.7
Accumulative iteration time: 153.0
Weighted Recall: 0.7178
Weighted FalsePositiveRate: 0.5018952380952381
Kappa statistic: 0.2420018468224649
Training time: 15.0
		
Iteration: 15
Labeled set size: 380
Unlabelled set size: 120
	
Mean region size: 94.53
Incorrectly Classified Instances: 27.82
Correctly Classified Instances: 72.18000000000002
Weighted Precision: 0.7004375933287961
Weighted AreaUnderROC: 0.7253499999999999
Root mean squared error: 0.4308541887020623
Relative absolute error: 70.49976521199048
Root relative squared error: 86.17083774041247
Weighted TruePositiveRate: 0.7218000000000001
Weighted MatthewsCorrelation: 0.27381454022616286
Weighted FMeasure: 0.7007994469271155
Iteration time: 16.4
Weighted AreaUnderPRC: 0.7482640986084836
Mean absolute error: 0.3524988260599524
Coverage of cases: 99.14
Instances selection time: 0.8
Test time: 2.9
Accumulative iteration time: 169.4
Weighted Recall: 0.7218000000000001
Weighted FalsePositiveRate: 0.4857047619047618
Kappa statistic: 0.2617384572611686
Training time: 15.6
		
Iteration: 16
Labeled set size: 400
Unlabelled set size: 100
	
Mean region size: 94.30000000000001
Incorrectly Classified Instances: 27.04
Correctly Classified Instances: 72.96000000000001
Weighted Precision: 0.7091893300762873
Weighted AreaUnderROC: 0.7186989523809524
Root mean squared error: 0.43098437193663236
Relative absolute error: 70.65857151499233
Root relative squared error: 86.19687438732649
Weighted TruePositiveRate: 0.7296
Weighted MatthewsCorrelation: 0.29190342703130057
Weighted FMeasure: 0.7076097750185133
Iteration time: 17.4
Weighted AreaUnderPRC: 0.7425453699872808
Mean absolute error: 0.35329285757496165
Coverage of cases: 98.82
Instances selection time: 0.8
Test time: 2.9
Accumulative iteration time: 186.8
Weighted Recall: 0.7296
Weighted FalsePositiveRate: 0.4823619047619047
Kappa statistic: 0.2771652843796205
Training time: 16.6
		
Iteration: 17
Labeled set size: 420
Unlabelled set size: 80
	
Mean region size: 93.98999999999998
Incorrectly Classified Instances: 26.82
Correctly Classified Instances: 73.17999999999999
Weighted Precision: 0.711106124208212
Weighted AreaUnderROC: 0.7259286666666667
Root mean squared error: 0.42904098244038247
Relative absolute error: 70.56618339120826
Root relative squared error: 85.80819648807648
Weighted TruePositiveRate: 0.7318
Weighted MatthewsCorrelation: 0.2956720301154164
Weighted FMeasure: 0.7090222835554647
Iteration time: 19.1
Weighted AreaUnderPRC: 0.7513842510728204
Mean absolute error: 0.35283091695604135
Coverage of cases: 99.2
Instances selection time: 0.9
Test time: 2.8
Accumulative iteration time: 205.9
Weighted Recall: 0.7318
Weighted FalsePositiveRate: 0.4821809523809525
Kappa statistic: 0.2802467335162793
Training time: 18.2
		
Iteration: 18
Labeled set size: 440
Unlabelled set size: 60
	
Mean region size: 94.16
Incorrectly Classified Instances: 28.099999999999994
Correctly Classified Instances: 71.9
Weighted Precision: 0.6946853133603124
Weighted AreaUnderROC: 0.7120680952380952
Root mean squared error: 0.433968386610165
Relative absolute error: 71.02692277085278
Root relative squared error: 86.793677322033
Weighted TruePositiveRate: 0.7190000000000001
Weighted MatthewsCorrelation: 0.2568170602403056
Weighted FMeasure: 0.6937514541285421
Iteration time: 19.8
Weighted AreaUnderPRC: 0.7356621249720279
Mean absolute error: 0.35513461385426387
Coverage of cases: 98.68
Instances selection time: 1.2
Test time: 2.7
Accumulative iteration time: 225.7
Weighted Recall: 0.7190000000000001
Weighted FalsePositiveRate: 0.5055714285714284
Kappa statistic: 0.24166264570577436
Training time: 18.6
		
Iteration: 19
Labeled set size: 460
Unlabelled set size: 40
	
Mean region size: 93.76
Incorrectly Classified Instances: 27.5
Correctly Classified Instances: 72.49999999999999
Weighted Precision: 0.7020556349334403
Weighted AreaUnderROC: 0.7203159047619049
Root mean squared error: 0.4313036825478454
Relative absolute error: 70.12223876994082
Root relative squared error: 86.26073650956909
Weighted TruePositiveRate: 0.7249999999999999
Weighted MatthewsCorrelation: 0.27359352736527975
Weighted FMeasure: 0.7001144513216839
Iteration time: 19.4
Weighted AreaUnderPRC: 0.7450213866311548
Mean absolute error: 0.35061119384970413
Coverage of cases: 98.89999999999999
Instances selection time: 0.3
Test time: 3.4
Accumulative iteration time: 245.1
Weighted Recall: 0.7249999999999999
Weighted FalsePositiveRate: 0.4965238095238095
Kappa statistic: 0.25777482430856413
Training time: 19.1
		
Iteration: 20
Labeled set size: 480
Unlabelled set size: 20
	
Mean region size: 92.94000000000001
Incorrectly Classified Instances: 28.18
Correctly Classified Instances: 71.82000000000001
Weighted Precision: 0.6961471125143327
Weighted AreaUnderROC: 0.720875238095238
Root mean squared error: 0.4320873383634578
Relative absolute error: 69.90952028304626
Root relative squared error: 86.41746767269157
Weighted TruePositiveRate: 0.7182000000000001
Weighted MatthewsCorrelation: 0.2630598990438794
Weighted FMeasure: 0.6964021278926003
Iteration time: 20.0
Weighted AreaUnderPRC: 0.745409314503837
Mean absolute error: 0.34954760141523133
Coverage of cases: 98.66
Instances selection time: 0.3
Test time: 2.9
Accumulative iteration time: 265.1
Weighted Recall: 0.7182000000000001
Weighted FalsePositiveRate: 0.49372380952380956
Kappa statistic: 0.2504385714499685
Training time: 19.7
		
Time end:Tue Oct 31 11.32.26 EET 2017