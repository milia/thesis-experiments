Tue Oct 31 11.33.40 EET 2017
Dataset: german_credit
Test set size: 500
Initial Labelled set size: 100
Initial Unlabelled set size: 400
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Tue Oct 31 11.33.40 EET 2017
		
Iteration: 1
Labeled set size: 100
Unlabelled set size: 400
	
Mean region size: 76.9
Incorrectly Classified Instances: 27.4
Correctly Classified Instances: 72.6
Weighted Precision: 0.7286676016830295
Weighted AreaUnderROC: 0.7397333333333334
Root mean squared error: 0.46648939114075855
Relative absolute error: 60.84406760315871
Root relative squared error: 93.2978782281517
Weighted TruePositiveRate: 0.726
Weighted MatthewsCorrelation: 0.3538720188575448
Weighted FMeasure: 0.7272602901285531
Iteration time: 1.0
Weighted AreaUnderPRC: 0.7604586556553055
Mean absolute error: 0.30422033801579357
Coverage of cases: 90.0
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 1.0
Weighted Recall: 0.726
Weighted FalsePositiveRate: 0.36885714285714283
Kappa statistic: 0.3537735849056604
Training time: 0.0
		
Iteration: 2
Labeled set size: 120
Unlabelled set size: 380
	
Mean region size: 77.4
Incorrectly Classified Instances: 27.0
Correctly Classified Instances: 73.0
Weighted Precision: 0.7315646719783014
Weighted AreaUnderROC: 0.743904761904762
Root mean squared error: 0.4612214553357424
Relative absolute error: 60.72402833140025
Root relative squared error: 92.24429106714848
Weighted TruePositiveRate: 0.73
Weighted MatthewsCorrelation: 0.3608318587436403
Weighted FMeasure: 0.7307555719703964
Iteration time: 1.0
Weighted AreaUnderPRC: 0.7655390502518631
Mean absolute error: 0.30362014165700124
Coverage of cases: 90.6
Instances selection time: 1.0
Test time: 7.0
Accumulative iteration time: 2.0
Weighted Recall: 0.73
Weighted FalsePositiveRate: 0.36714285714285716
Kappa statistic: 0.3607954545454545
Training time: 0.0
		
Iteration: 3
Labeled set size: 140
Unlabelled set size: 360
	
Mean region size: 81.2
Incorrectly Classified Instances: 27.0
Correctly Classified Instances: 73.0
Weighted Precision: 0.729488135528404
Weighted AreaUnderROC: 0.7446095238095238
Root mean squared error: 0.45347278098494603
Relative absolute error: 61.53760710220604
Root relative squared error: 90.69455619698921
Weighted TruePositiveRate: 0.73
Weighted MatthewsCorrelation: 0.3559200813493665
Weighted FMeasure: 0.7297410770089553
Iteration time: 1.0
Weighted AreaUnderPRC: 0.7687836854121413
Mean absolute error: 0.3076880355110302
Coverage of cases: 92.4
Instances selection time: 1.0
Test time: 7.0
Accumulative iteration time: 3.0
Weighted Recall: 0.73
Weighted FalsePositiveRate: 0.37476190476190474
Kappa statistic: 0.3559160305343511
Training time: 0.0
		
Iteration: 4
Labeled set size: 160
Unlabelled set size: 340
	
Mean region size: 82.9
Incorrectly Classified Instances: 26.6
Correctly Classified Instances: 73.4
Weighted Precision: 0.7315298688683828
Weighted AreaUnderROC: 0.7532952380952381
Root mean squared error: 0.44410383781688895
Relative absolute error: 60.60432877224977
Root relative squared error: 88.82076756337779
Weighted TruePositiveRate: 0.734
Weighted MatthewsCorrelation: 0.36068114750926167
Weighted FMeasure: 0.7326890251232119
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7760140466270108
Mean absolute error: 0.30302164386124886
Coverage of cases: 93.8
Instances selection time: 1.0
Test time: 15.0
Accumulative iteration time: 5.0
Weighted Recall: 0.734
Weighted FalsePositiveRate: 0.37685714285714284
Kappa statistic: 0.3605769230769231
Training time: 1.0
		
Iteration: 5
Labeled set size: 180
Unlabelled set size: 320
	
Mean region size: 83.0
Incorrectly Classified Instances: 24.8
Correctly Classified Instances: 75.2
Weighted Precision: 0.752
Weighted AreaUnderROC: 0.7667999999999999
Root mean squared error: 0.43540729109504483
Relative absolute error: 59.197545615456484
Root relative squared error: 87.08145821900897
Weighted TruePositiveRate: 0.752
Weighted MatthewsCorrelation: 0.4095238095238095
Weighted FMeasure: 0.752
Iteration time: 1.0
Weighted AreaUnderPRC: 0.7841382523192284
Mean absolute error: 0.2959877280772824
Coverage of cases: 94.6
Instances selection time: 1.0
Test time: 7.0
Accumulative iteration time: 6.0
Weighted Recall: 0.752
Weighted FalsePositiveRate: 0.3424761904761905
Kappa statistic: 0.40952380952380957
Training time: 0.0
		
Iteration: 6
Labeled set size: 200
Unlabelled set size: 300
	
Mean region size: 82.8
Incorrectly Classified Instances: 24.4
Correctly Classified Instances: 75.6
Weighted Precision: 0.7508767715589719
Weighted AreaUnderROC: 0.7696571428571428
Root mean squared error: 0.4315864117091337
Relative absolute error: 58.82132947573376
Root relative squared error: 86.31728234182674
Weighted TruePositiveRate: 0.756
Weighted MatthewsCorrelation: 0.4061531770774398
Weighted FMeasure: 0.7529728464419475
Iteration time: 1.0
Weighted AreaUnderPRC: 0.7852400155240925
Mean absolute error: 0.2941066473786688
Coverage of cases: 94.8
Instances selection time: 1.0
Test time: 7.0
Accumulative iteration time: 7.0
Weighted Recall: 0.756
Weighted FalsePositiveRate: 0.35980952380952386
Kappa statistic: 0.40545808966861596
Training time: 0.0
		
Iteration: 7
Labeled set size: 220
Unlabelled set size: 280
	
Mean region size: 84.5
Incorrectly Classified Instances: 24.6
Correctly Classified Instances: 75.4
Weighted Precision: 0.7500325964558762
Weighted AreaUnderROC: 0.7744380952380953
Root mean squared error: 0.43011759521527615
Relative absolute error: 59.69654446439001
Root relative squared error: 86.02351904305523
Weighted TruePositiveRate: 0.754
Weighted MatthewsCorrelation: 0.4044544582858366
Weighted FMeasure: 0.7517572303084059
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7909437524667812
Mean absolute error: 0.29848272232195006
Coverage of cases: 94.6
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 9.0
Weighted Recall: 0.754
Weighted FalsePositiveRate: 0.3568571428571429
Kappa statistic: 0.4040697674418604
Training time: 1.0
		
Iteration: 8
Labeled set size: 240
Unlabelled set size: 260
	
Mean region size: 83.9
Incorrectly Classified Instances: 23.8
Correctly Classified Instances: 76.2
Weighted Precision: 0.7589880707527767
Weighted AreaUnderROC: 0.7699428571428572
Root mean squared error: 0.43343351144211695
Relative absolute error: 60.426675467586776
Root relative squared error: 86.68670228842339
Weighted TruePositiveRate: 0.762
Weighted MatthewsCorrelation: 0.4259188050152893
Weighted FMeasure: 0.7603352144088128
Iteration time: 1.0
Weighted AreaUnderPRC: 0.7875889363193871
Mean absolute error: 0.3021333773379339
Coverage of cases: 94.2
Instances selection time: 1.0
Test time: 7.0
Accumulative iteration time: 10.0
Weighted Recall: 0.762
Weighted FalsePositiveRate: 0.342
Kappa statistic: 0.4256756756756757
Training time: 0.0
		
Iteration: 9
Labeled set size: 260
Unlabelled set size: 240
	
Mean region size: 81.2
Incorrectly Classified Instances: 26.0
Correctly Classified Instances: 74.0
Weighted Precision: 0.7282938676381299
Weighted AreaUnderROC: 0.7597904761904762
Root mean squared error: 0.44302477533642143
Relative absolute error: 59.60156173122654
Root relative squared error: 88.60495506728428
Weighted TruePositiveRate: 0.74
Weighted MatthewsCorrelation: 0.34956100553108105
Weighted FMeasure: 0.7316176470588235
Iteration time: 1.0
Weighted AreaUnderPRC: 0.7792511818611676
Mean absolute error: 0.2980078086561327
Coverage of cases: 93.4
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 11.0
Weighted Recall: 0.74
Weighted FalsePositiveRate: 0.41238095238095235
Kappa statistic: 0.34607645875251497
Training time: 0.0
		
Iteration: 10
Labeled set size: 280
Unlabelled set size: 220
	
Mean region size: 83.5
Incorrectly Classified Instances: 26.2
Correctly Classified Instances: 73.8
Weighted Precision: 0.7279369234341685
Weighted AreaUnderROC: 0.7686285714285713
Root mean squared error: 0.4353733390165621
Relative absolute error: 60.05815818035158
Root relative squared error: 87.07466780331242
Weighted TruePositiveRate: 0.738
Weighted MatthewsCorrelation: 0.3499125371059129
Weighted FMeasure: 0.7312901019119094
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7847408636265328
Mean absolute error: 0.3002907909017579
Coverage of cases: 94.6
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 13.0
Weighted Recall: 0.738
Weighted FalsePositiveRate: 0.4056190476190476
Kappa statistic: 0.34760956175298796
Training time: 1.0
		
Iteration: 11
Labeled set size: 300
Unlabelled set size: 200
	
Mean region size: 84.4
Incorrectly Classified Instances: 26.0
Correctly Classified Instances: 74.0
Weighted Precision: 0.7289807824296499
Weighted AreaUnderROC: 0.766552380952381
Root mean squared error: 0.43636937208105064
Relative absolute error: 60.68068176412196
Root relative squared error: 87.27387441621013
Weighted TruePositiveRate: 0.74
Weighted MatthewsCorrelation: 0.35169389662214323
Weighted FMeasure: 0.7323198809595625
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7836831523291592
Mean absolute error: 0.3034034088206098
Coverage of cases: 95.8
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 15.0
Weighted Recall: 0.74
Weighted FalsePositiveRate: 0.40857142857142853
Kappa statistic: 0.3486973947895791
Training time: 1.0
		
Iteration: 12
Labeled set size: 320
Unlabelled set size: 180
	
Mean region size: 83.8
Incorrectly Classified Instances: 26.8
Correctly Classified Instances: 73.2
Weighted Precision: 0.7211866564807741
Weighted AreaUnderROC: 0.7655238095238095
Root mean squared error: 0.4398963170742598
Relative absolute error: 60.75877522560265
Root relative squared error: 87.97926341485196
Weighted TruePositiveRate: 0.732
Weighted MatthewsCorrelation: 0.3337393092040207
Weighted FMeasure: 0.7247898150372327
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7825004734211952
Mean absolute error: 0.30379387612801323
Coverage of cases: 95.2
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 17.0
Weighted Recall: 0.732
Weighted FalsePositiveRate: 0.41580952380952374
Kappa statistic: 0.33133732534930144
Training time: 1.0
		
Iteration: 13
Labeled set size: 340
Unlabelled set size: 160
	
Mean region size: 85.1
Incorrectly Classified Instances: 25.4
Correctly Classified Instances: 74.6
Weighted Precision: 0.7350779582066377
Weighted AreaUnderROC: 0.7694476190476192
Root mean squared error: 0.43396561035862047
Relative absolute error: 60.95032799803647
Root relative squared error: 86.7931220717241
Weighted TruePositiveRate: 0.746
Weighted MatthewsCorrelation: 0.36582579386474445
Weighted FMeasure: 0.7381562041809634
Iteration time: 1.0
Weighted AreaUnderPRC: 0.7873723513832754
Mean absolute error: 0.30475163999018234
Coverage of cases: 96.6
Instances selection time: 0.0
Test time: 7.0
Accumulative iteration time: 18.0
Weighted Recall: 0.746
Weighted FalsePositiveRate: 0.40219047619047615
Kappa statistic: 0.3624497991967871
Training time: 1.0
		
Iteration: 14
Labeled set size: 360
Unlabelled set size: 140
	
Mean region size: 85.2
Incorrectly Classified Instances: 25.2
Correctly Classified Instances: 74.8
Weighted Precision: 0.7375428963623885
Weighted AreaUnderROC: 0.7708190476190476
Root mean squared error: 0.4321964644670859
Relative absolute error: 60.97754856614412
Root relative squared error: 86.43929289341717
Weighted TruePositiveRate: 0.748
Weighted MatthewsCorrelation: 0.3719061895314618
Weighted FMeasure: 0.7405561923146529
Iteration time: 1.0
Weighted AreaUnderPRC: 0.7879887265824114
Mean absolute error: 0.3048877428307206
Coverage of cases: 96.2
Instances selection time: 0.0
Test time: 7.0
Accumulative iteration time: 19.0
Weighted Recall: 0.748
Weighted FalsePositiveRate: 0.3975238095238095
Kappa statistic: 0.3687374749498998
Training time: 1.0
		
Iteration: 15
Labeled set size: 380
Unlabelled set size: 120
	
Mean region size: 84.8
Incorrectly Classified Instances: 25.8
Correctly Classified Instances: 74.2
Weighted Precision: 0.7294889609387062
Weighted AreaUnderROC: 0.7699428571428572
Root mean squared error: 0.43374391477586444
Relative absolute error: 60.534998956402454
Root relative squared error: 86.74878295517289
Weighted TruePositiveRate: 0.742
Weighted MatthewsCorrelation: 0.35151875721411646
Weighted FMeasure: 0.7326044172315767
Iteration time: 5.0
Weighted AreaUnderPRC: 0.7872512558670831
Mean absolute error: 0.30267499478201226
Coverage of cases: 96.2
Instances selection time: 4.0
Test time: 11.0
Accumulative iteration time: 24.0
Weighted Recall: 0.742
Weighted FalsePositiveRate: 0.41533333333333333
Kappa statistic: 0.347165991902834
Training time: 1.0
		
Iteration: 16
Labeled set size: 400
Unlabelled set size: 100
	
Mean region size: 85.0
Incorrectly Classified Instances: 25.2
Correctly Classified Instances: 74.8
Weighted Precision: 0.735744076670512
Weighted AreaUnderROC: 0.7695238095238095
Root mean squared error: 0.4331130243846002
Relative absolute error: 60.758355613356855
Root relative squared error: 86.62260487692004
Weighted TruePositiveRate: 0.748
Weighted MatthewsCorrelation: 0.36590442829904535
Weighted FMeasure: 0.7384634206019085
Iteration time: 1.0
Weighted AreaUnderPRC: 0.7854897320800239
Mean absolute error: 0.30379177806678426
Coverage of cases: 96.2
Instances selection time: 0.0
Test time: 7.0
Accumulative iteration time: 25.0
Weighted Recall: 0.748
Weighted FalsePositiveRate: 0.4089523809523809
Kappa statistic: 0.3610547667342799
Training time: 1.0
		
Iteration: 17
Labeled set size: 420
Unlabelled set size: 80
	
Mean region size: 84.8
Incorrectly Classified Instances: 26.0
Correctly Classified Instances: 74.0
Weighted Precision: 0.7263829022988506
Weighted AreaUnderROC: 0.7692380952380953
Root mean squared error: 0.43446393457552934
Relative absolute error: 60.77578468338394
Root relative squared error: 86.89278691510587
Weighted TruePositiveRate: 0.74
Weighted MatthewsCorrelation: 0.34326840299828
Weighted FMeasure: 0.7294052570117392
Iteration time: 1.0
Weighted AreaUnderPRC: 0.7851178757175945
Mean absolute error: 0.3038789234169197
Coverage of cases: 96.4
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 26.0
Weighted Recall: 0.74
Weighted FalsePositiveRate: 0.4238095238095238
Kappa statistic: 0.33808553971486766
Training time: 0.0
		
Iteration: 18
Labeled set size: 440
Unlabelled set size: 60
	
Mean region size: 84.3
Incorrectly Classified Instances: 25.8
Correctly Classified Instances: 74.2
Weighted Precision: 0.7288813014661579
Weighted AreaUnderROC: 0.7723619047619047
Root mean squared error: 0.43343837891092385
Relative absolute error: 59.98721193317992
Root relative squared error: 86.68767578218477
Weighted TruePositiveRate: 0.742
Weighted MatthewsCorrelation: 0.3494602095365522
Weighted FMeasure: 0.7318638196115701
Iteration time: 1.0
Weighted AreaUnderPRC: 0.7878694096066452
Mean absolute error: 0.2999360596658996
Coverage of cases: 96.4
Instances selection time: 0.0
Test time: 7.0
Accumulative iteration time: 27.0
Weighted Recall: 0.742
Weighted FalsePositiveRate: 0.41914285714285715
Kappa statistic: 0.34451219512195114
Training time: 1.0
		
Iteration: 19
Labeled set size: 460
Unlabelled set size: 40
	
Mean region size: 84.7
Incorrectly Classified Instances: 25.6
Correctly Classified Instances: 74.4
Weighted Precision: 0.7307920258620689
Weighted AreaUnderROC: 0.7733333333333333
Root mean squared error: 0.43174495554359776
Relative absolute error: 60.204497213284554
Root relative squared error: 86.34899110871956
Weighted TruePositiveRate: 0.744
Weighted MatthewsCorrelation: 0.3536078127271439
Weighted FMeasure: 0.7335682530577124
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7897064334029494
Mean absolute error: 0.3010224860664228
Coverage of cases: 96.4
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 29.0
Weighted Recall: 0.744
Weighted FalsePositiveRate: 0.41828571428571426
Kappa statistic: 0.3482688391038697
Training time: 1.0
		
Iteration: 20
Labeled set size: 480
Unlabelled set size: 20
	
Mean region size: 85.6
Incorrectly Classified Instances: 25.6
Correctly Classified Instances: 74.4
Weighted Precision: 0.7319736842105263
Weighted AreaUnderROC: 0.7793714285714286
Root mean squared error: 0.42628098965378564
Relative absolute error: 59.88217391349136
Root relative squared error: 85.25619793075712
Weighted TruePositiveRate: 0.744
Weighted MatthewsCorrelation: 0.3576646146995634
Weighted FMeasure: 0.7350380517503806
Iteration time: 1.0
Weighted AreaUnderPRC: 0.7950212961314703
Mean absolute error: 0.29941086956745677
Coverage of cases: 97.0
Instances selection time: 0.0
Test time: 7.0
Accumulative iteration time: 30.0
Weighted Recall: 0.744
Weighted FalsePositiveRate: 0.4106666666666666
Kappa statistic: 0.35353535353535354
Training time: 1.0
		
Time end:Tue Oct 31 11.33.41 EET 2017