Tue Oct 31 11.34.49 EET 2017
Dataset: german_credit
Test set size: 500
Initial Labelled set size: 100
Initial Unlabelled set size: 400
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Tue Oct 31 11.34.49 EET 2017
		
Iteration: 1
Labeled set size: 100
Unlabelled set size: 400
	
Mean region size: 85.7
Incorrectly Classified Instances: 30.4
Correctly Classified Instances: 69.6
Weighted Precision: 0.7234375
Weighted AreaUnderROC: 0.7414285714285714
Root mean squared error: 0.46040666009800707
Relative absolute error: 67.07491765410924
Root relative squared error: 92.08133201960142
Weighted TruePositiveRate: 0.696
Weighted MatthewsCorrelation: 0.33561084188120116
Weighted FMeasure: 0.7049645390070923
Iteration time: 5.0
Weighted AreaUnderPRC: 0.7564598005565945
Mean absolute error: 0.3353745882705462
Coverage of cases: 94.2
Instances selection time: 4.0
Test time: 7.0
Accumulative iteration time: 5.0
Weighted Recall: 0.696
Weighted FalsePositiveRate: 0.33980952380952373
Kappa statistic: 0.32980599647266307
Training time: 1.0
		
Iteration: 2
Labeled set size: 120
Unlabelled set size: 380
	
Mean region size: 83.4
Incorrectly Classified Instances: 26.4
Correctly Classified Instances: 73.6
Weighted Precision: 0.7380526987463404
Weighted AreaUnderROC: 0.7424380952380952
Root mean squared error: 0.4487612515657766
Relative absolute error: 63.376932302179846
Root relative squared error: 89.75225031315533
Weighted TruePositiveRate: 0.736
Weighted MatthewsCorrelation: 0.37624870593027093
Weighted FMeasure: 0.7369782214156079
Iteration time: 5.0
Weighted AreaUnderPRC: 0.7569668607947699
Mean absolute error: 0.3168846615108992
Coverage of cases: 93.6
Instances selection time: 4.0
Test time: 7.0
Accumulative iteration time: 10.0
Weighted Recall: 0.736
Weighted FalsePositiveRate: 0.35695238095238097
Kappa statistic: 0.3761814744801512
Training time: 1.0
		
Iteration: 3
Labeled set size: 140
Unlabelled set size: 360
	
Mean region size: 85.8
Incorrectly Classified Instances: 25.8
Correctly Classified Instances: 74.2
Weighted Precision: 0.7283003952569171
Weighted AreaUnderROC: 0.7554285714285716
Root mean squared error: 0.4298161562918613
Relative absolute error: 61.06767975705022
Root relative squared error: 85.96323125837226
Weighted TruePositiveRate: 0.742
Weighted MatthewsCorrelation: 0.3474208469923004
Weighted FMeasure: 0.7311051212938006
Iteration time: 5.0
Weighted AreaUnderPRC: 0.7675017153600074
Mean absolute error: 0.3053383987852511
Coverage of cases: 96.4
Instances selection time: 4.0
Test time: 7.0
Accumulative iteration time: 15.0
Weighted Recall: 0.742
Weighted FalsePositiveRate: 0.4229523809523809
Kappa statistic: 0.34183673469387754
Training time: 1.0
		
Iteration: 4
Labeled set size: 160
Unlabelled set size: 340
	
Mean region size: 84.6
Incorrectly Classified Instances: 26.8
Correctly Classified Instances: 73.2
Weighted Precision: 0.7150793650793651
Weighted AreaUnderROC: 0.7614285714285715
Root mean squared error: 0.4275520959196531
Relative absolute error: 59.92697702724578
Root relative squared error: 85.51041918393062
Weighted TruePositiveRate: 0.732
Weighted MatthewsCorrelation: 0.3139259031987207
Weighted FMeasure: 0.7177709521720051
Iteration time: 9.0
Weighted AreaUnderPRC: 0.771551987623517
Mean absolute error: 0.29963488513622893
Coverage of cases: 96.0
Instances selection time: 8.0
Test time: 11.0
Accumulative iteration time: 24.0
Weighted Recall: 0.732
Weighted FalsePositiveRate: 0.4500952380952381
Kappa statistic: 0.3064182194616976
Training time: 1.0
		
Iteration: 5
Labeled set size: 180
Unlabelled set size: 320
	
Mean region size: 81.5
Incorrectly Classified Instances: 26.6
Correctly Classified Instances: 73.4
Weighted Precision: 0.7151590720169273
Weighted AreaUnderROC: 0.7716761904761905
Root mean squared error: 0.4276451181327664
Relative absolute error: 57.48538350758964
Root relative squared error: 85.52902362655328
Weighted TruePositiveRate: 0.734
Weighted MatthewsCorrelation: 0.30994669126891394
Weighted FMeasure: 0.7157909935347249
Iteration time: 7.0
Weighted AreaUnderPRC: 0.7791313064437454
Mean absolute error: 0.2874269175379482
Coverage of cases: 95.2
Instances selection time: 6.0
Test time: 13.0
Accumulative iteration time: 31.0
Weighted Recall: 0.734
Weighted FalsePositiveRate: 0.4644761904761905
Kappa statistic: 0.29852320675105476
Training time: 1.0
		
Iteration: 6
Labeled set size: 200
Unlabelled set size: 300
	
Mean region size: 80.0
Incorrectly Classified Instances: 26.2
Correctly Classified Instances: 73.8
Weighted Precision: 0.7181207460443738
Weighted AreaUnderROC: 0.771447619047619
Root mean squared error: 0.4339711889709549
Relative absolute error: 57.12799425068984
Root relative squared error: 86.79423779419098
Weighted TruePositiveRate: 0.738
Weighted MatthewsCorrelation: 0.30442054213196507
Weighted FMeasure: 0.7106243561380103
Iteration time: 4.0
Weighted AreaUnderPRC: 0.7786054901247405
Mean absolute error: 0.2856399712534492
Coverage of cases: 94.6
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 35.0
Weighted Recall: 0.738
Weighted FalsePositiveRate: 0.4932380952380952
Kappa statistic: 0.2817982456140351
Training time: 1.0
		
Iteration: 7
Labeled set size: 220
Unlabelled set size: 280
	
Mean region size: 79.2
Incorrectly Classified Instances: 26.8
Correctly Classified Instances: 73.2
Weighted Precision: 0.7105571847507332
Weighted AreaUnderROC: 0.7749904761904762
Root mean squared error: 0.43728674503653486
Relative absolute error: 57.19157308243534
Root relative squared error: 87.45734900730697
Weighted TruePositiveRate: 0.732
Weighted MatthewsCorrelation: 0.27334389654205493
Weighted FMeasure: 0.6942460317460318
Iteration time: 4.0
Weighted AreaUnderPRC: 0.7817527277553027
Mean absolute error: 0.2859578654121767
Coverage of cases: 95.2
Instances selection time: 3.0
Test time: 6.0
Accumulative iteration time: 39.0
Weighted Recall: 0.732
Weighted FalsePositiveRate: 0.5300952380952381
Kappa statistic: 0.2403628117913832
Training time: 1.0
		
Iteration: 8
Labeled set size: 240
Unlabelled set size: 260
	
Mean region size: 78.2
Incorrectly Classified Instances: 27.4
Correctly Classified Instances: 72.6
Weighted Precision: 0.7015473466274382
Weighted AreaUnderROC: 0.7772380952380953
Root mean squared error: 0.4406760529962489
Relative absolute error: 57.07907760185663
Root relative squared error: 88.13521059924977
Weighted TruePositiveRate: 0.726
Weighted MatthewsCorrelation: 0.25119600415053933
Weighted FMeasure: 0.6851871073966032
Iteration time: 3.0
Weighted AreaUnderPRC: 0.7839882313848091
Mean absolute error: 0.28539538800928316
Coverage of cases: 95.2
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 42.0
Weighted Recall: 0.726
Weighted FalsePositiveRate: 0.5440952380952382
Kappa statistic: 0.21803652968036533
Training time: 1.0
		
Iteration: 9
Labeled set size: 260
Unlabelled set size: 240
	
Mean region size: 77.1
Incorrectly Classified Instances: 27.0
Correctly Classified Instances: 73.0
Weighted Precision: 0.7079110820529586
Weighted AreaUnderROC: 0.7769714285714285
Root mean squared error: 0.44353097591610263
Relative absolute error: 56.94737147767005
Root relative squared error: 88.70619518322053
Weighted TruePositiveRate: 0.73
Weighted MatthewsCorrelation: 0.2643476274044943
Weighted FMeasure: 0.6897829160477477
Iteration time: 4.0
Weighted AreaUnderPRC: 0.7828393107942588
Mean absolute error: 0.28473685738835025
Coverage of cases: 94.8
Instances selection time: 3.0
Test time: 6.0
Accumulative iteration time: 46.0
Weighted Recall: 0.73
Weighted FalsePositiveRate: 0.5385714285714286
Kappa statistic: 0.2294520547945206
Training time: 1.0
		
Iteration: 10
Labeled set size: 280
Unlabelled set size: 220
	
Mean region size: 76.2
Incorrectly Classified Instances: 27.0
Correctly Classified Instances: 73.0
Weighted Precision: 0.7082975465850107
Weighted AreaUnderROC: 0.7741142857142858
Root mean squared error: 0.44690544456947956
Relative absolute error: 56.902385468771854
Root relative squared error: 89.38108891389591
Weighted TruePositiveRate: 0.73
Weighted MatthewsCorrelation: 0.2626995937459758
Weighted FMeasure: 0.688285008920044
Iteration time: 4.0
Weighted AreaUnderPRC: 0.7809953556502323
Mean absolute error: 0.28451192734385927
Coverage of cases: 94.2
Instances selection time: 3.0
Test time: 6.0
Accumulative iteration time: 50.0
Weighted Recall: 0.73
Weighted FalsePositiveRate: 0.5423809523809524
Kappa statistic: 0.2259174311926605
Training time: 1.0
		
Iteration: 11
Labeled set size: 300
Unlabelled set size: 200
	
Mean region size: 75.5
Incorrectly Classified Instances: 27.0
Correctly Classified Instances: 73.0
Weighted Precision: 0.7079110820529586
Weighted AreaUnderROC: 0.7745714285714287
Root mean squared error: 0.4480909752038031
Relative absolute error: 56.762515907818845
Root relative squared error: 89.61819504076061
Weighted TruePositiveRate: 0.73
Weighted MatthewsCorrelation: 0.2643476274044943
Weighted FMeasure: 0.6897829160477477
Iteration time: 3.0
Weighted AreaUnderPRC: 0.7820403089527551
Mean absolute error: 0.2838125795390942
Coverage of cases: 94.4
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 53.0
Weighted Recall: 0.73
Weighted FalsePositiveRate: 0.5385714285714286
Kappa statistic: 0.2294520547945206
Training time: 1.0
		
Iteration: 12
Labeled set size: 320
Unlabelled set size: 180
	
Mean region size: 74.9
Incorrectly Classified Instances: 27.2
Correctly Classified Instances: 72.8
Weighted Precision: 0.7056639101263849
Weighted AreaUnderROC: 0.7753142857142857
Root mean squared error: 0.44918200701531535
Relative absolute error: 56.62475612595542
Root relative squared error: 89.83640140306306
Weighted TruePositiveRate: 0.728
Weighted MatthewsCorrelation: 0.25350011774832604
Weighted FMeasure: 0.6836441336441336
Iteration time: 3.0
Weighted AreaUnderPRC: 0.78295987654285
Mean absolute error: 0.2831237806297771
Coverage of cases: 94.4
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 56.0
Weighted Recall: 0.728
Weighted FalsePositiveRate: 0.5508571428571429
Kappa statistic: 0.2147806004618938
Training time: 1.0
		
Iteration: 13
Labeled set size: 340
Unlabelled set size: 160
	
Mean region size: 74.8
Incorrectly Classified Instances: 27.0
Correctly Classified Instances: 73.0
Weighted Precision: 0.7100102145045966
Weighted AreaUnderROC: 0.7744380952380953
Root mean squared error: 0.453738548467806
Relative absolute error: 56.758915540178
Root relative squared error: 90.74770969356119
Weighted TruePositiveRate: 0.73
Weighted MatthewsCorrelation: 0.2580481386674601
Weighted FMeasure: 0.6835710998619421
Iteration time: 3.0
Weighted AreaUnderPRC: 0.7826728072053861
Mean absolute error: 0.28379457770089
Coverage of cases: 94.0
Instances selection time: 1.0
Test time: 7.0
Accumulative iteration time: 59.0
Weighted Recall: 0.73
Weighted FalsePositiveRate: 0.5538095238095239
Kappa statistic: 0.21511627906976732
Training time: 2.0
		
Iteration: 14
Labeled set size: 360
Unlabelled set size: 140
	
Mean region size: 74.3
Incorrectly Classified Instances: 27.4
Correctly Classified Instances: 72.6
Weighted Precision: 0.7035709763201217
Weighted AreaUnderROC: 0.7774285714285715
Root mean squared error: 0.4552082769555707
Relative absolute error: 56.61444360781797
Root relative squared error: 91.04165539111413
Weighted TruePositiveRate: 0.726
Weighted MatthewsCorrelation: 0.24243469194514178
Weighted FMeasure: 0.6772107224752923
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7859078581569722
Mean absolute error: 0.28307221803908983
Coverage of cases: 94.0
Instances selection time: 1.0
Test time: 7.0
Accumulative iteration time: 61.0
Weighted Recall: 0.726
Weighted FalsePositiveRate: 0.5631428571428571
Kappa statistic: 0.19976635514018695
Training time: 1.0
		
Iteration: 15
Labeled set size: 380
Unlabelled set size: 120
	
Mean region size: 73.8
Incorrectly Classified Instances: 27.6
Correctly Classified Instances: 72.4
Weighted Precision: 0.7008888888888889
Weighted AreaUnderROC: 0.7761333333333333
Root mean squared error: 0.4571462093480474
Relative absolute error: 56.65637330130397
Root relative squared error: 91.42924186960948
Weighted TruePositiveRate: 0.724
Weighted MatthewsCorrelation: 0.23276574958505855
Weighted FMeasure: 0.67225
Iteration time: 3.0
Weighted AreaUnderPRC: 0.7851142025578605
Mean absolute error: 0.2832818665065199
Coverage of cases: 93.6
Instances selection time: 2.0
Test time: 6.0
Accumulative iteration time: 64.0
Weighted Recall: 0.724
Weighted FalsePositiveRate: 0.5716190476190477
Kappa statistic: 0.18823529411764692
Training time: 1.0
		
Iteration: 16
Labeled set size: 400
Unlabelled set size: 100
	
Mean region size: 76.2
Incorrectly Classified Instances: 27.4
Correctly Classified Instances: 72.6
Weighted Precision: 0.702494950695022
Weighted AreaUnderROC: 0.7773904761904762
Root mean squared error: 0.45291904513373277
Relative absolute error: 57.177334699789974
Root relative squared error: 90.58380902674655
Weighted TruePositiveRate: 0.726
Weighted MatthewsCorrelation: 0.2458125729643654
Weighted FMeasure: 0.6805161101668586
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7870751735185021
Mean absolute error: 0.28588667349894986
Coverage of cases: 94.8
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 66.0
Weighted Recall: 0.726
Weighted FalsePositiveRate: 0.5555238095238096
Kappa statistic: 0.2071759259259259
Training time: 1.0
		
Iteration: 17
Labeled set size: 420
Unlabelled set size: 80
	
Mean region size: 79.6
Incorrectly Classified Instances: 25.6
Correctly Classified Instances: 74.4
Weighted Precision: 0.7278405315614617
Weighted AreaUnderROC: 0.7892190476190475
Root mean squared error: 0.4332380112161951
Relative absolute error: 56.97784559699811
Root relative squared error: 86.64760224323902
Weighted TruePositiveRate: 0.744
Weighted MatthewsCorrelation: 0.31444675225915136
Weighted FMeasure: 0.7105827505827506
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7981838699322047
Mean absolute error: 0.28488922798499056
Coverage of cases: 95.6
Instances selection time: 1.0
Test time: 7.0
Accumulative iteration time: 68.0
Weighted Recall: 0.744
Weighted FalsePositiveRate: 0.505904761904762
Kappa statistic: 0.28089887640449435
Training time: 1.0
		
Iteration: 18
Labeled set size: 440
Unlabelled set size: 60
	
Mean region size: 83.0
Incorrectly Classified Instances: 24.6
Correctly Classified Instances: 75.4
Weighted Precision: 0.7387572323056194
Weighted AreaUnderROC: 0.790552380952381
Root mean squared error: 0.42044262929622034
Relative absolute error: 57.372669707265146
Root relative squared error: 84.08852585924407
Weighted TruePositiveRate: 0.754
Weighted MatthewsCorrelation: 0.36004493530503534
Weighted FMeasure: 0.7344097069328245
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7984597639631287
Mean absolute error: 0.2868633485363257
Coverage of cases: 96.0
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 70.0
Weighted Recall: 0.754
Weighted FalsePositiveRate: 0.4482857142857143
Kappa statistic: 0.3429487179487179
Training time: 1.0
		
Iteration: 19
Labeled set size: 460
Unlabelled set size: 40
	
Mean region size: 84.8
Incorrectly Classified Instances: 23.8
Correctly Classified Instances: 76.2
Weighted Precision: 0.7489692585895118
Weighted AreaUnderROC: 0.7965714285714287
Root mean squared error: 0.4140152918409915
Relative absolute error: 58.1651215594176
Root relative squared error: 82.80305836819831
Weighted TruePositiveRate: 0.762
Weighted MatthewsCorrelation: 0.39110198460761786
Weighted FMeasure: 0.7481879194630873
Iteration time: 2.0
Weighted AreaUnderPRC: 0.8043081657398797
Mean absolute error: 0.290825607797088
Coverage of cases: 97.0
Instances selection time: 0.0
Test time: 7.0
Accumulative iteration time: 72.0
Weighted Recall: 0.762
Weighted FalsePositiveRate: 0.41438095238095235
Kappa statistic: 0.38020833333333337
Training time: 2.0
		
Iteration: 20
Labeled set size: 480
Unlabelled set size: 20
	
Mean region size: 85.4
Incorrectly Classified Instances: 23.6
Correctly Classified Instances: 76.4
Weighted Precision: 0.7515873015873016
Weighted AreaUnderROC: 0.8010857142857143
Root mean squared error: 0.4124527954406373
Relative absolute error: 58.08130368850563
Root relative squared error: 82.49055908812745
Weighted TruePositiveRate: 0.764
Weighted MatthewsCorrelation: 0.3987707419010777
Weighted FMeasure: 0.7514699429574375
Iteration time: 2.0
Weighted AreaUnderPRC: 0.8073883085410709
Mean absolute error: 0.2904065184425281
Coverage of cases: 96.4
Instances selection time: 0.0
Test time: 7.0
Accumulative iteration time: 74.0
Weighted Recall: 0.764
Weighted FalsePositiveRate: 0.4059047619047619
Kappa statistic: 0.38923395445134573
Training time: 2.0
		
Time end:Tue Oct 31 11.34.50 EET 2017