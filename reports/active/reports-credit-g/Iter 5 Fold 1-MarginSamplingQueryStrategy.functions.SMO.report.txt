Tue Oct 31 11.32.53 EET 2017
Dataset: german_credit
Test set size: 500
Initial Labelled set size: 100
Initial Unlabelled set size: 400
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Tue Oct 31 11.32.53 EET 2017
		
Iteration: 1
Labeled set size: 100
Unlabelled set size: 400
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.2
Correctly Classified Instances: 69.8
Weighted Precision: 0.6865730583589295
Weighted AreaUnderROC: 0.6204761904761906
Root mean squared error: 0.5495452665613635
Relative absolute error: 60.4
Root relative squared error: 109.9090533122727
Weighted TruePositiveRate: 0.698
Weighted MatthewsCorrelation: 0.2523649654490769
Weighted FMeasure: 0.6910324668545778
Iteration time: 17.0
Weighted AreaUnderPRC: 0.6439744959150839
Mean absolute error: 0.302
Coverage of cases: 69.8
Instances selection time: 4.0
Test time: 3.0
Accumulative iteration time: 17.0
Weighted Recall: 0.698
Weighted FalsePositiveRate: 0.4570476190476191
Kappa statistic: 0.2509920634920634
Training time: 13.0
		
Iteration: 2
Labeled set size: 120
Unlabelled set size: 380
	
Mean region size: 50.0
Incorrectly Classified Instances: 28.8
Correctly Classified Instances: 71.2
Weighted Precision: 0.6907245532245533
Weighted AreaUnderROC: 0.6133333333333333
Root mean squared error: 0.5366563145999496
Relative absolute error: 57.599999999999994
Root relative squared error: 107.33126291998991
Weighted TruePositiveRate: 0.712
Weighted MatthewsCorrelation: 0.25591930465420243
Weighted FMeasure: 0.6948006164108843
Iteration time: 16.0
Weighted AreaUnderPRC: 0.6413538850038849
Mean absolute error: 0.288
Coverage of cases: 71.2
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 33.0
Weighted Recall: 0.712
Weighted FalsePositiveRate: 0.48533333333333334
Kappa statistic: 0.2484342379958245
Training time: 14.0
		
Iteration: 3
Labeled set size: 140
Unlabelled set size: 360
	
Mean region size: 50.0
Incorrectly Classified Instances: 28.0
Correctly Classified Instances: 72.0
Weighted Precision: 0.6949780923491743
Weighted AreaUnderROC: 0.6057142857142858
Root mean squared error: 0.5291502622129182
Relative absolute error: 56.00000000000001
Root relative squared error: 105.83005244258364
Weighted TruePositiveRate: 0.72
Weighted MatthewsCorrelation: 0.2567402329394125
Weighted FMeasure: 0.6937616469961843
Iteration time: 29.0
Weighted AreaUnderPRC: 0.6382422649140547
Mean absolute error: 0.28
Coverage of cases: 72.0
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 62.0
Weighted Recall: 0.72
Weighted FalsePositiveRate: 0.5085714285714286
Kappa statistic: 0.2407809110629067
Training time: 27.0
		
Iteration: 4
Labeled set size: 160
Unlabelled set size: 340
	
Mean region size: 50.0
Incorrectly Classified Instances: 28.4
Correctly Classified Instances: 71.6
Weighted Precision: 0.6853925353925355
Weighted AreaUnderROC: 0.5723809523809524
Root mean squared error: 0.532916503778969
Relative absolute error: 56.8
Root relative squared error: 106.58330075579381
Weighted TruePositiveRate: 0.716
Weighted MatthewsCorrelation: 0.21035287010786344
Weighted FMeasure: 0.6680149666185714
Iteration time: 15.0
Weighted AreaUnderPRC: 0.6196921492921492
Mean absolute error: 0.284
Coverage of cases: 71.6
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 77.0
Weighted Recall: 0.716
Weighted FalsePositiveRate: 0.5712380952380952
Kappa statistic: 0.1763341067285382
Training time: 13.0
		
Iteration: 5
Labeled set size: 180
Unlabelled set size: 320
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.4
Correctly Classified Instances: 70.6
Weighted Precision: 0.6692221516860227
Weighted AreaUnderROC: 0.5652380952380952
Root mean squared error: 0.5422176684690383
Relative absolute error: 58.8
Root relative squared error: 108.44353369380767
Weighted TruePositiveRate: 0.706
Weighted MatthewsCorrelation: 0.18268956519390195
Weighted FMeasure: 0.6605770097129369
Iteration time: 25.0
Weighted AreaUnderPRC: 0.6144088502184547
Mean absolute error: 0.294
Coverage of cases: 70.6
Instances selection time: 3.0
Test time: 3.0
Accumulative iteration time: 102.0
Weighted Recall: 0.706
Weighted FalsePositiveRate: 0.5755238095238095
Kappa statistic: 0.15711009174311916
Training time: 22.0
		
Iteration: 6
Labeled set size: 200
Unlabelled set size: 300
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.2
Correctly Classified Instances: 70.8
Weighted Precision: 0.6709134615384615
Weighted AreaUnderROC: 0.559047619047619
Root mean squared error: 0.5403702434442518
Relative absolute error: 58.4
Root relative squared error: 108.07404868885035
Weighted TruePositiveRate: 0.708
Weighted MatthewsCorrelation: 0.17728474171277506
Weighted FMeasure: 0.6550981413930868
Iteration time: 22.0
Weighted AreaUnderPRC: 0.6114002747252747
Mean absolute error: 0.292
Coverage of cases: 70.8
Instances selection time: 3.0
Test time: 3.0
Accumulative iteration time: 124.0
Weighted Recall: 0.708
Weighted FalsePositiveRate: 0.5899047619047619
Kappa statistic: 0.14519906323185006
Training time: 19.0
		
Iteration: 7
Labeled set size: 220
Unlabelled set size: 280
	
Mean region size: 50.0
Incorrectly Classified Instances: 27.2
Correctly Classified Instances: 72.8
Weighted Precision: 0.7106301474813255
Weighted AreaUnderROC: 0.5771428571428571
Root mean squared error: 0.521536192416212
Relative absolute error: 54.400000000000006
Root relative squared error: 104.30723848324239
Weighted TruePositiveRate: 0.728
Weighted MatthewsCorrelation: 0.2446236967323126
Weighted FMeasure: 0.6734287744948726
Iteration time: 35.0
Weighted AreaUnderPRC: 0.6249665581306265
Mean absolute error: 0.272
Coverage of cases: 72.8
Instances selection time: 4.0
Test time: 2.0
Accumulative iteration time: 159.0
Weighted Recall: 0.728
Weighted FalsePositiveRate: 0.5737142857142856
Kappa statistic: 0.19239904988123507
Training time: 31.0
		
Iteration: 8
Labeled set size: 240
Unlabelled set size: 260
	
Mean region size: 50.0
Incorrectly Classified Instances: 27.0
Correctly Classified Instances: 73.0
Weighted Precision: 0.7127698085886239
Weighted AreaUnderROC: 0.5823809523809523
Root mean squared error: 0.5196152422706632
Relative absolute error: 54.0
Root relative squared error: 103.92304845413265
Weighted TruePositiveRate: 0.73
Weighted MatthewsCorrelation: 0.253951127175905
Weighted FMeasure: 0.6785048839704139
Iteration time: 29.0
Weighted AreaUnderPRC: 0.6281431467487217
Mean absolute error: 0.27
Coverage of cases: 73.0
Instances selection time: 3.0
Test time: 3.0
Accumulative iteration time: 188.0
Weighted Recall: 0.73
Weighted FalsePositiveRate: 0.5652380952380952
Kappa statistic: 0.20400943396226398
Training time: 26.0
		
Iteration: 9
Labeled set size: 260
Unlabelled set size: 240
	
Mean region size: 50.0
Incorrectly Classified Instances: 26.8
Correctly Classified Instances: 73.2
Weighted Precision: 0.716
Weighted AreaUnderROC: 0.5857142857142857
Root mean squared error: 0.5176871642217914
Relative absolute error: 53.6
Root relative squared error: 103.53743284435828
Weighted TruePositiveRate: 0.732
Weighted MatthewsCorrelation: 0.26186146828319085
Weighted FMeasure: 0.68175
Iteration time: 35.0
Weighted AreaUnderPRC: 0.6304
Mean absolute error: 0.268
Coverage of cases: 73.2
Instances selection time: 3.0
Test time: 3.0
Accumulative iteration time: 223.0
Weighted Recall: 0.732
Weighted FalsePositiveRate: 0.5605714285714286
Kappa statistic: 0.21176470588235283
Training time: 32.0
		
Iteration: 10
Labeled set size: 280
Unlabelled set size: 220
	
Mean region size: 50.0
Incorrectly Classified Instances: 27.2
Correctly Classified Instances: 72.8
Weighted Precision: 0.7106301474813255
Weighted AreaUnderROC: 0.5771428571428571
Root mean squared error: 0.521536192416212
Relative absolute error: 54.400000000000006
Root relative squared error: 104.30723848324239
Weighted TruePositiveRate: 0.728
Weighted MatthewsCorrelation: 0.2446236967323126
Weighted FMeasure: 0.6734287744948726
Iteration time: 34.0
Weighted AreaUnderPRC: 0.6249665581306265
Mean absolute error: 0.272
Coverage of cases: 72.8
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 257.0
Weighted Recall: 0.728
Weighted FalsePositiveRate: 0.5737142857142856
Kappa statistic: 0.19239904988123507
Training time: 32.0
		
Iteration: 11
Labeled set size: 300
Unlabelled set size: 200
	
Mean region size: 50.0
Incorrectly Classified Instances: 28.0
Correctly Classified Instances: 72.0
Weighted Precision: 0.7000718390804598
Weighted AreaUnderROC: 0.5580952380952381
Root mean squared error: 0.5291502622129182
Relative absolute error: 56.00000000000001
Root relative squared error: 105.83005244258364
Weighted TruePositiveRate: 0.72
Weighted MatthewsCorrelation: 0.20598706169524822
Weighted FMeasure: 0.6538004279939762
Iteration time: 25.0
Weighted AreaUnderPRC: 0.6133104406130268
Mean absolute error: 0.28
Coverage of cases: 72.0
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 282.0
Weighted Recall: 0.72
Weighted FalsePositiveRate: 0.6038095238095237
Kappa statistic: 0.1484184914841848
Training time: 24.0
		
Iteration: 12
Labeled set size: 320
Unlabelled set size: 180
	
Mean region size: 50.0
Incorrectly Classified Instances: 28.6
Correctly Classified Instances: 71.4
Weighted Precision: 0.684857860019847
Weighted AreaUnderROC: 0.5519047619047619
Root mean squared error: 0.5347896782848375
Relative absolute error: 57.199999999999996
Root relative squared error: 106.95793565696749
Weighted TruePositiveRate: 0.714
Weighted MatthewsCorrelation: 0.18172952566554137
Weighted FMeasure: 0.6474640040518053
Iteration time: 46.0
Weighted AreaUnderPRC: 0.6087353803047109
Mean absolute error: 0.286
Coverage of cases: 71.4
Instances selection time: 3.0
Test time: 3.0
Accumulative iteration time: 328.0
Weighted Recall: 0.714
Weighted FalsePositiveRate: 0.6101904761904763
Kappa statistic: 0.13228155339805817
Training time: 43.0
		
Iteration: 13
Labeled set size: 340
Unlabelled set size: 160
	
Mean region size: 50.0
Incorrectly Classified Instances: 28.2
Correctly Classified Instances: 71.8
Weighted Precision: 0.6932921742032371
Weighted AreaUnderROC: 0.5585714285714286
Root mean squared error: 0.5310367218940701
Relative absolute error: 56.39999999999999
Root relative squared error: 106.20734437881403
Weighted TruePositiveRate: 0.718
Weighted MatthewsCorrelation: 0.20017631855126217
Weighted FMeasure: 0.6544888732311668
Iteration time: 40.0
Weighted AreaUnderPRC: 0.6130148172868346
Mean absolute error: 0.282
Coverage of cases: 71.8
Instances selection time: 3.0
Test time: 3.0
Accumulative iteration time: 368.0
Weighted Recall: 0.718
Weighted FalsePositiveRate: 0.6008571428571429
Kappa statistic: 0.1485507246376812
Training time: 37.0
		
Iteration: 14
Labeled set size: 360
Unlabelled set size: 140
	
Mean region size: 50.0
Incorrectly Classified Instances: 28.8
Correctly Classified Instances: 71.2
Weighted Precision: 0.6834042553191488
Weighted AreaUnderROC: 0.5428571428571428
Root mean squared error: 0.5366563145999496
Relative absolute error: 57.599999999999994
Root relative squared error: 107.33126291998991
Weighted TruePositiveRate: 0.712
Weighted MatthewsCorrelation: 0.1653953539259914
Weighted FMeasure: 0.6370731707317073
Iteration time: 32.0
Weighted AreaUnderPRC: 0.603744680851064
Mean absolute error: 0.288
Coverage of cases: 71.2
Instances selection time: 3.0
Test time: 3.0
Accumulative iteration time: 400.0
Weighted Recall: 0.712
Weighted FalsePositiveRate: 0.6262857142857143
Kappa statistic: 0.11111111111111088
Training time: 29.0
		
Iteration: 15
Labeled set size: 380
Unlabelled set size: 120
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.6
Correctly Classified Instances: 70.4
Weighted Precision: 0.6622916666666667
Weighted AreaUnderROC: 0.5238095238095238
Root mean squared error: 0.5440588203494178
Relative absolute error: 59.199999999999996
Root relative squared error: 108.81176406988355
Weighted TruePositiveRate: 0.704
Weighted MatthewsCorrelation: 0.11135885079684349
Weighted FMeasure: 0.6140042523033311
Iteration time: 43.0
Weighted AreaUnderPRC: 0.5926041666666666
Mean absolute error: 0.296
Coverage of cases: 70.4
Instances selection time: 3.0
Test time: 3.0
Accumulative iteration time: 443.0
Weighted Recall: 0.704
Weighted FalsePositiveRate: 0.6563809523809523
Kappa statistic: 0.06329113924050604
Training time: 40.0
		
Iteration: 16
Labeled set size: 400
Unlabelled set size: 100
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.4
Correctly Classified Instances: 70.6
Weighted Precision: 0.6656178857042788
Weighted AreaUnderROC: 0.5423809523809524
Root mean squared error: 0.5422176684690383
Relative absolute error: 58.8
Root relative squared error: 108.44353369380767
Weighted TruePositiveRate: 0.706
Weighted MatthewsCorrelation: 0.14838465857094665
Weighted FMeasure: 0.6376028573119955
Iteration time: 74.0
Weighted AreaUnderPRC: 0.6024237814488353
Mean absolute error: 0.294
Coverage of cases: 70.6
Instances selection time: 2.0
Test time: 3.0
Accumulative iteration time: 517.0
Weighted Recall: 0.706
Weighted FalsePositiveRate: 0.6212380952380953
Kappa statistic: 0.10800970873786397
Training time: 72.0
		
Iteration: 17
Labeled set size: 420
Unlabelled set size: 80
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.4
Correctly Classified Instances: 70.6
Weighted Precision: 0.6667224761301417
Weighted AreaUnderROC: 0.5538095238095239
Root mean squared error: 0.5422176684690383
Relative absolute error: 58.8
Root relative squared error: 108.44353369380767
Weighted TruePositiveRate: 0.706
Weighted MatthewsCorrelation: 0.16587559173917496
Weighted FMeasure: 0.6499275403233395
Iteration time: 59.0
Weighted AreaUnderPRC: 0.6083780895063126
Mean absolute error: 0.294
Coverage of cases: 70.6
Instances selection time: 2.0
Test time: 3.0
Accumulative iteration time: 576.0
Weighted Recall: 0.706
Weighted FalsePositiveRate: 0.5983809523809523
Kappa statistic: 0.1332547169811318
Training time: 57.0
		
Iteration: 18
Labeled set size: 440
Unlabelled set size: 60
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.4
Correctly Classified Instances: 70.6
Weighted Precision: 0.6702564102564101
Weighted AreaUnderROC: 0.569047619047619
Root mean squared error: 0.5422176684690383
Relative absolute error: 58.8
Root relative squared error: 108.44353369380767
Weighted TruePositiveRate: 0.706
Weighted MatthewsCorrelation: 0.1881728870189205
Weighted FMeasure: 0.6638009183824619
Iteration time: 111.0
Weighted AreaUnderPRC: 0.6164358974358974
Mean absolute error: 0.294
Coverage of cases: 70.6
Instances selection time: 2.0
Test time: 3.0
Accumulative iteration time: 687.0
Weighted Recall: 0.706
Weighted FalsePositiveRate: 0.5679047619047619
Kappa statistic: 0.16477272727272713
Training time: 109.0
		
Iteration: 19
Labeled set size: 460
Unlabelled set size: 40
	
Mean region size: 50.0
Incorrectly Classified Instances: 28.8
Correctly Classified Instances: 71.2
Weighted Precision: 0.6829166666666666
Weighted AreaUnderROC: 0.5904761904761905
Root mean squared error: 0.5366563145999496
Relative absolute error: 57.599999999999994
Root relative squared error: 107.33126291998991
Weighted TruePositiveRate: 0.712
Weighted MatthewsCorrelation: 0.2261904761904762
Weighted FMeasure: 0.6812648221343873
Iteration time: 103.0
Weighted AreaUnderPRC: 0.6287440476190476
Mean absolute error: 0.288
Coverage of cases: 71.2
Instances selection time: 2.0
Test time: 3.0
Accumulative iteration time: 790.0
Weighted Recall: 0.712
Weighted FalsePositiveRate: 0.531047619047619
Kappa statistic: 0.2087912087912087
Training time: 101.0
		
Iteration: 20
Labeled set size: 480
Unlabelled set size: 20
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.0
Correctly Classified Instances: 70.0
Weighted Precision: 0.6741293532338308
Weighted AreaUnderROC: 0.5933333333333334
Root mean squared error: 0.5477225575051661
Relative absolute error: 60.0
Root relative squared error: 109.54451150103321
Weighted TruePositiveRate: 0.7
Weighted MatthewsCorrelation: 0.21548664505181808
Weighted FMeasure: 0.6789207275223061
Iteration time: 141.0
Weighted AreaUnderPRC: 0.6289512437810945
Mean absolute error: 0.3
Coverage of cases: 70.0
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 931.0
Weighted Recall: 0.7
Weighted FalsePositiveRate: 0.5133333333333334
Kappa statistic: 0.20718816067653256
Training time: 141.0
		
Time end:Tue Oct 31 11.32.55 EET 2017