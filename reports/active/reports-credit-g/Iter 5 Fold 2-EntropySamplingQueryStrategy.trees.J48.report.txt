Tue Oct 31 11.29.44 EET 2017
Dataset: german_credit
Test set size: 500
Initial Labelled set size: 100
Initial Unlabelled set size: 400
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Tue Oct 31 11.29.44 EET 2017
		
Iteration: 1
Labeled set size: 100
Unlabelled set size: 400
	
Mean region size: 77.4
Incorrectly Classified Instances: 29.4
Correctly Classified Instances: 70.6
Weighted Precision: 0.6990146990810561
Weighted AreaUnderROC: 0.6258190476190476
Root mean squared error: 0.5130627605961953
Relative absolute error: 70.21001317523076
Root relative squared error: 102.61255211923906
Weighted TruePositiveRate: 0.706
Weighted MatthewsCorrelation: 0.2827968705988804
Weighted FMeasure: 0.7020216878185613
Iteration time: 4.0
Weighted AreaUnderPRC: 0.6522804622034738
Mean absolute error: 0.3510500658761538
Coverage of cases: 83.6
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 4.0
Weighted Recall: 0.706
Weighted FalsePositiveRate: 0.43076190476190473
Kappa statistic: 0.28222656249999983
Training time: 3.0
		
Iteration: 2
Labeled set size: 120
Unlabelled set size: 380
	
Mean region size: 87.2
Incorrectly Classified Instances: 27.6
Correctly Classified Instances: 72.4
Weighted Precision: 0.711050394656952
Weighted AreaUnderROC: 0.5577333333333333
Root mean squared error: 0.5020392901163868
Relative absolute error: 69.48131752305693
Root relative squared error: 100.40785802327736
Weighted TruePositiveRate: 0.724
Weighted MatthewsCorrelation: 0.30891437698095536
Weighted FMeasure: 0.7151018099547511
Iteration time: 3.0
Weighted AreaUnderPRC: 0.6203505316797551
Mean absolute error: 0.34740658761528465
Coverage of cases: 88.0
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 7.0
Weighted Recall: 0.724
Weighted FalsePositiveRate: 0.43447619047619046
Kappa statistic: 0.305835010060362
Training time: 2.0
		
Iteration: 3
Labeled set size: 140
Unlabelled set size: 360
	
Mean region size: 77.1
Incorrectly Classified Instances: 30.0
Correctly Classified Instances: 70.0
Weighted Precision: 0.6944444444444444
Weighted AreaUnderROC: 0.5894952380952381
Root mean squared error: 0.5268196318722816
Relative absolute error: 67.32433811802244
Root relative squared error: 105.36392637445633
Weighted TruePositiveRate: 0.7
Weighted MatthewsCorrelation: 0.2721655269759087
Weighted FMeasure: 0.6969402622632347
Iteration time: 3.0
Weighted AreaUnderPRC: 0.6348539822583473
Mean absolute error: 0.3366216905901122
Coverage of cases: 83.0
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 10.0
Weighted Recall: 0.7
Weighted FalsePositiveRate: 0.4333333333333333
Kappa statistic: 0.2718446601941747
Training time: 2.0
		
Iteration: 4
Labeled set size: 160
Unlabelled set size: 340
	
Mean region size: 100.0
Incorrectly Classified Instances: 30.0
Correctly Classified Instances: 70.0
Weighted Precision: 0.48999999999999994
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.46774893105169224
Relative absolute error: 76.49999999999953
Root relative squared error: 93.54978621033845
Weighted TruePositiveRate: 0.7
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.5764705882352942
Iteration time: 4.0
Weighted AreaUnderPRC: 0.58
Mean absolute error: 0.3824999999999977
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 14.0
Weighted Recall: 0.7
Weighted FalsePositiveRate: 0.7
Kappa statistic: 0.0
Training time: 3.0
		
Iteration: 5
Labeled set size: 180
Unlabelled set size: 320
	
Mean region size: 100.0
Incorrectly Classified Instances: 30.0
Correctly Classified Instances: 70.0
Weighted Precision: 0.48999999999999994
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.4728753653037031
Relative absolute error: 74.66666666666623
Root relative squared error: 94.57507306074062
Weighted TruePositiveRate: 0.7
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.5764705882352942
Iteration time: 4.0
Weighted AreaUnderPRC: 0.58
Mean absolute error: 0.37333333333333113
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 18.0
Weighted Recall: 0.7
Weighted FalsePositiveRate: 0.7
Kappa statistic: 0.0
Training time: 3.0
		
Iteration: 6
Labeled set size: 200
Unlabelled set size: 300
	
Mean region size: 100.0
Incorrectly Classified Instances: 30.0
Correctly Classified Instances: 70.0
Weighted Precision: 0.48999999999999994
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.47772900267829727
Relative absolute error: 73.19999999999992
Root relative squared error: 95.54580053565945
Weighted TruePositiveRate: 0.7
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.5764705882352942
Iteration time: 3.0
Weighted AreaUnderPRC: 0.58
Mean absolute error: 0.3659999999999996
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 21.0
Weighted Recall: 0.7
Weighted FalsePositiveRate: 0.7
Kappa statistic: 0.0
Training time: 3.0
		
Iteration: 7
Labeled set size: 220
Unlabelled set size: 280
	
Mean region size: 100.0
Incorrectly Classified Instances: 30.0
Correctly Classified Instances: 70.0
Weighted Precision: 0.48999999999999994
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.48218253804964695
Relative absolute error: 71.99999999999956
Root relative squared error: 96.4365076099294
Weighted TruePositiveRate: 0.7
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.5764705882352942
Iteration time: 4.0
Weighted AreaUnderPRC: 0.58
Mean absolute error: 0.3599999999999978
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 25.0
Weighted Recall: 0.7
Weighted FalsePositiveRate: 0.7
Kappa statistic: 0.0
Training time: 3.0
		
Iteration: 8
Labeled set size: 240
Unlabelled set size: 260
	
Mean region size: 100.0
Incorrectly Classified Instances: 30.0
Correctly Classified Instances: 70.0
Weighted Precision: 0.48999999999999994
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.4862162584694174
Relative absolute error: 71.00000000000026
Root relative squared error: 97.24325169388348
Weighted TruePositiveRate: 0.7
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.5764705882352942
Iteration time: 4.0
Weighted AreaUnderPRC: 0.58
Mean absolute error: 0.35500000000000126
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 29.0
Weighted Recall: 0.7
Weighted FalsePositiveRate: 0.7
Kappa statistic: 0.0
Training time: 3.0
		
Iteration: 9
Labeled set size: 260
Unlabelled set size: 240
	
Mean region size: 100.0
Incorrectly Classified Instances: 30.0
Correctly Classified Instances: 70.0
Weighted Precision: 0.48999999999999994
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.48985265264339933
Relative absolute error: 70.15384615384619
Root relative squared error: 97.97053052867987
Weighted TruePositiveRate: 0.7
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.5764705882352942
Iteration time: 4.0
Weighted AreaUnderPRC: 0.58
Mean absolute error: 0.35076923076923094
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 33.0
Weighted Recall: 0.7
Weighted FalsePositiveRate: 0.7
Kappa statistic: 0.0
Training time: 3.0
		
Iteration: 10
Labeled set size: 280
Unlabelled set size: 220
	
Mean region size: 100.0
Incorrectly Classified Instances: 30.0
Correctly Classified Instances: 70.0
Weighted Precision: 0.48999999999999994
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.49312880711652185
Relative absolute error: 69.42857142857147
Root relative squared error: 98.62576142330437
Weighted TruePositiveRate: 0.7
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.5764705882352942
Iteration time: 4.0
Weighted AreaUnderPRC: 0.58
Mean absolute error: 0.34714285714285736
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 37.0
Weighted Recall: 0.7
Weighted FalsePositiveRate: 0.7
Kappa statistic: 0.0
Training time: 3.0
		
Iteration: 11
Labeled set size: 300
Unlabelled set size: 200
	
Mean region size: 100.0
Incorrectly Classified Instances: 30.0
Correctly Classified Instances: 70.0
Weighted Precision: 0.48999999999999994
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.4960846701924992
Relative absolute error: 68.7999999999997
Root relative squared error: 99.21693403849984
Weighted TruePositiveRate: 0.7
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.5764705882352942
Iteration time: 5.0
Weighted AreaUnderPRC: 0.58
Mean absolute error: 0.3439999999999985
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 42.0
Weighted Recall: 0.7
Weighted FalsePositiveRate: 0.7
Kappa statistic: 0.0
Training time: 4.0
		
Iteration: 12
Labeled set size: 320
Unlabelled set size: 180
	
Mean region size: 100.0
Incorrectly Classified Instances: 30.0
Correctly Classified Instances: 70.0
Weighted Precision: 0.48999999999999994
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.49875822361641364
Relative absolute error: 68.24999999999966
Root relative squared error: 99.75164472328272
Weighted TruePositiveRate: 0.7
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.5764705882352942
Iteration time: 4.0
Weighted AreaUnderPRC: 0.58
Mean absolute error: 0.3412499999999983
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 46.0
Weighted Recall: 0.7
Weighted FalsePositiveRate: 0.7
Kappa statistic: 0.0
Training time: 4.0
		
Iteration: 13
Labeled set size: 340
Unlabelled set size: 160
	
Mean region size: 100.0
Incorrectly Classified Instances: 30.0
Correctly Classified Instances: 70.0
Weighted Precision: 0.48999999999999994
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.501183719914431
Relative absolute error: 67.76470588235316
Root relative squared error: 100.2367439828862
Weighted TruePositiveRate: 0.7
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.5764705882352942
Iteration time: 6.0
Weighted AreaUnderPRC: 0.58
Mean absolute error: 0.33882352941176574
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 52.0
Weighted Recall: 0.7
Weighted FalsePositiveRate: 0.7
Kappa statistic: 0.0
Training time: 5.0
		
Iteration: 14
Labeled set size: 360
Unlabelled set size: 140
	
Mean region size: 100.0
Incorrectly Classified Instances: 30.0
Correctly Classified Instances: 70.0
Weighted Precision: 0.48999999999999994
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.5033912770179643
Relative absolute error: 67.33333333333313
Root relative squared error: 100.67825540359286
Weighted TruePositiveRate: 0.7
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.5764705882352942
Iteration time: 7.0
Weighted AreaUnderPRC: 0.58
Mean absolute error: 0.3366666666666656
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 59.0
Weighted Recall: 0.7
Weighted FalsePositiveRate: 0.7
Kappa statistic: 0.0
Training time: 6.0
		
Iteration: 15
Labeled set size: 380
Unlabelled set size: 120
	
Mean region size: 100.0
Incorrectly Classified Instances: 30.0
Correctly Classified Instances: 70.0
Weighted Precision: 0.48999999999999994
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.5054070518786236
Relative absolute error: 66.94736842105287
Root relative squared error: 101.08141037572472
Weighted TruePositiveRate: 0.7
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.5764705882352942
Iteration time: 7.0
Weighted AreaUnderPRC: 0.58
Mean absolute error: 0.33473684210526433
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 66.0
Weighted Recall: 0.7
Weighted FalsePositiveRate: 0.7
Kappa statistic: 0.0
Training time: 6.0
		
Iteration: 16
Labeled set size: 400
Unlabelled set size: 100
	
Mean region size: 100.0
Incorrectly Classified Instances: 30.0
Correctly Classified Instances: 70.0
Weighted Precision: 0.48999999999999994
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.4905354217587146
Relative absolute error: 70.0
Root relative squared error: 98.10708435174293
Weighted TruePositiveRate: 0.7
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.5764705882352942
Iteration time: 6.0
Weighted AreaUnderPRC: 0.58
Mean absolute error: 0.35
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 72.0
Weighted Recall: 0.7
Weighted FalsePositiveRate: 0.7
Kappa statistic: 0.0
Training time: 6.0
		
Iteration: 17
Labeled set size: 420
Unlabelled set size: 80
	
Mean region size: 100.0
Incorrectly Classified Instances: 30.0
Correctly Classified Instances: 70.0
Weighted Precision: 0.48999999999999994
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.4772607021092114
Relative absolute error: 73.33333333333333
Root relative squared error: 95.45214042184227
Weighted TruePositiveRate: 0.7
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.5764705882352942
Iteration time: 8.0
Weighted AreaUnderPRC: 0.58
Mean absolute error: 0.36666666666666664
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 80.0
Weighted Recall: 0.7
Weighted FalsePositiveRate: 0.7
Kappa statistic: 0.0
Training time: 7.0
		
Iteration: 18
Labeled set size: 440
Unlabelled set size: 60
	
Mean region size: 90.1
Incorrectly Classified Instances: 25.4
Correctly Classified Instances: 74.6
Weighted Precision: 0.7354587032553135
Weighted AreaUnderROC: 0.6898095238095238
Root mean squared error: 0.44583861524943563
Relative absolute error: 59.80153667032814
Root relative squared error: 89.16772304988713
Weighted TruePositiveRate: 0.746
Weighted MatthewsCorrelation: 0.3152107157295779
Weighted FMeasure: 0.7053139687513232
Iteration time: 9.0
Weighted AreaUnderPRC: 0.7122875715770001
Mean absolute error: 0.2990076833516407
Coverage of cases: 94.0
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 89.0
Weighted Recall: 0.746
Weighted FalsePositiveRate: 0.5240952380952381
Kappa statistic: 0.26843317972350217
Training time: 8.0
		
Iteration: 19
Labeled set size: 460
Unlabelled set size: 40
	
Mean region size: 92.5
Incorrectly Classified Instances: 28.2
Correctly Classified Instances: 71.8
Weighted Precision: 0.6954311734158757
Weighted AreaUnderROC: 0.7071428571428572
Root mean squared error: 0.45722054027411885
Relative absolute error: 63.00289607078432
Root relative squared error: 91.44410805482377
Weighted TruePositiveRate: 0.718
Weighted MatthewsCorrelation: 0.2637848587217724
Weighted FMeasure: 0.6976692420600996
Iteration time: 9.0
Weighted AreaUnderPRC: 0.7168502323208807
Mean absolute error: 0.3150144803539216
Coverage of cases: 95.2
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 98.0
Weighted Recall: 0.718
Weighted FalsePositiveRate: 0.49038095238095236
Kappa statistic: 0.2531779661016949
Training time: 8.0
		
Iteration: 20
Labeled set size: 480
Unlabelled set size: 20
	
Mean region size: 89.2
Incorrectly Classified Instances: 28.4
Correctly Classified Instances: 71.6
Weighted Precision: 0.7069169960474309
Weighted AreaUnderROC: 0.6472095238095238
Root mean squared error: 0.4798430666650998
Relative absolute error: 64.94898761774654
Root relative squared error: 95.96861333301996
Weighted TruePositiveRate: 0.716
Weighted MatthewsCorrelation: 0.3009906891304464
Weighted FMeasure: 0.7104960587921533
Iteration time: 9.0
Weighted AreaUnderPRC: 0.6748993559434278
Mean absolute error: 0.32474493808873267
Coverage of cases: 91.0
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 107.0
Weighted Recall: 0.716
Weighted FalsePositiveRate: 0.4264761904761905
Kappa statistic: 0.29980276134122275
Training time: 9.0
		
Time end:Tue Oct 31 11.29.45 EET 2017