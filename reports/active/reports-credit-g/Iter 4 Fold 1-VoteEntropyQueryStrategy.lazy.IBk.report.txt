Tue Oct 31 11.35.31 EET 2017
Dataset: german_credit
Test set size: 500
Initial Labelled set size: 100
Initial Unlabelled set size: 400
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Tue Oct 31 11.35.31 EET 2017
		
Iteration: 1
Labeled set size: 100
Unlabelled set size: 400
	
Mean region size: 50.0
Incorrectly Classified Instances: 32.6
Correctly Classified Instances: 67.4
Weighted Precision: 0.6398430688753269
Weighted AreaUnderROC: 0.5576190476190476
Root mean squared error: 0.5654236995522732
Relative absolute error: 65.8823529411764
Root relative squared error: 113.08473991045464
Weighted TruePositiveRate: 0.674
Weighted MatthewsCorrelation: 0.1357178728096862
Weighted FMeasure: 0.6480388799191089
Iteration time: 39.0
Weighted AreaUnderPRC: 0.608068061609997
Mean absolute error: 0.32941176470588207
Coverage of cases: 67.4
Instances selection time: 38.0
Test time: 48.0
Accumulative iteration time: 39.0
Weighted Recall: 0.674
Weighted FalsePositiveRate: 0.5587619047619047
Kappa statistic: 0.12927350427350429
Training time: 1.0
		
Iteration: 2
Labeled set size: 120
Unlabelled set size: 380
	
Mean region size: 50.0
Incorrectly Classified Instances: 32.4
Correctly Classified Instances: 67.6
Weighted Precision: 0.6341666666666667
Weighted AreaUnderROC: 0.5476190476190477
Root mean squared error: 0.564584547105588
Relative absolute error: 65.37704918032802
Root relative squared error: 112.91690942111761
Weighted TruePositiveRate: 0.676
Weighted MatthewsCorrelation: 0.11904761904761904
Weighted FMeasure: 0.6414229249011858
Iteration time: 44.0
Weighted AreaUnderPRC: 0.6029761904761903
Mean absolute error: 0.3268852459016401
Coverage of cases: 67.6
Instances selection time: 43.0
Test time: 43.0
Accumulative iteration time: 83.0
Weighted Recall: 0.676
Weighted FalsePositiveRate: 0.5807619047619048
Kappa statistic: 0.10989010989010999
Training time: 1.0
		
Iteration: 3
Labeled set size: 140
Unlabelled set size: 360
	
Mean region size: 50.0
Incorrectly Classified Instances: 31.8
Correctly Classified Instances: 68.2
Weighted Precision: 0.6380642263642489
Weighted AreaUnderROC: 0.5480952380952381
Root mean squared error: 0.5599738566176267
Relative absolute error: 64.11267605633824
Root relative squared error: 111.99477132352533
Weighted TruePositiveRate: 0.682
Weighted MatthewsCorrelation: 0.12483499805735417
Weighted FMeasure: 0.6428554114652769
Iteration time: 18.0
Weighted AreaUnderPRC: 0.6034725931153957
Mean absolute error: 0.32056338028169123
Coverage of cases: 68.2
Instances selection time: 17.0
Test time: 24.0
Accumulative iteration time: 101.0
Weighted Recall: 0.682
Weighted FalsePositiveRate: 0.5858095238095238
Kappa statistic: 0.11272321428571455
Training time: 1.0
		
Iteration: 4
Labeled set size: 160
Unlabelled set size: 340
	
Mean region size: 50.0
Incorrectly Classified Instances: 31.0
Correctly Classified Instances: 69.0
Weighted Precision: 0.6453580901856764
Weighted AreaUnderROC: 0.55
Root mean squared error: 0.55336330150611
Relative absolute error: 62.469135802469026
Root relative squared error: 110.67266030122201
Weighted TruePositiveRate: 0.69
Weighted MatthewsCorrelation: 0.13626312508266653
Weighted FMeasure: 0.645504369723004
Iteration time: 18.0
Weighted AreaUnderPRC: 0.6048992042440318
Mean absolute error: 0.3123456790123451
Coverage of cases: 69.0
Instances selection time: 18.0
Test time: 27.0
Accumulative iteration time: 119.0
Weighted Recall: 0.69
Weighted FalsePositiveRate: 0.59
Kappa statistic: 0.11931818181818162
Training time: 0.0
		
Iteration: 5
Labeled set size: 180
Unlabelled set size: 320
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.8
Correctly Classified Instances: 69.2
Weighted Precision: 0.645909090909091
Weighted AreaUnderROC: 0.5476190476190477
Root mean squared error: 0.5519470755480219
Relative absolute error: 62.021978021977965
Root relative squared error: 110.38941510960439
Weighted TruePositiveRate: 0.692
Weighted MatthewsCorrelation: 0.13430382733756338
Weighted FMeasure: 0.6435443037974683
Iteration time: 19.0
Weighted AreaUnderPRC: 0.6037878787878788
Mean absolute error: 0.3101098901098898
Coverage of cases: 69.2
Instances selection time: 18.0
Test time: 30.0
Accumulative iteration time: 138.0
Weighted Recall: 0.692
Weighted FalsePositiveRate: 0.5967619047619048
Kappa statistic: 0.11494252873563197
Training time: 1.0
		
Iteration: 6
Labeled set size: 200
Unlabelled set size: 300
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.4
Correctly Classified Instances: 69.6
Weighted Precision: 0.6507239382239383
Weighted AreaUnderROC: 0.5485714285714286
Root mean squared error: 0.5486479804128841
Relative absolute error: 61.18811881188131
Root relative squared error: 109.72959608257682
Weighted TruePositiveRate: 0.696
Weighted MatthewsCorrelation: 0.1411578470460663
Weighted FMeasure: 0.644635738915654
Iteration time: 20.0
Weighted AreaUnderPRC: 0.6045843629343628
Mean absolute error: 0.30594059405940655
Coverage of cases: 69.6
Instances selection time: 19.0
Test time: 31.0
Accumulative iteration time: 158.0
Weighted Recall: 0.696
Weighted FalsePositiveRate: 0.5988571428571429
Kappa statistic: 0.11832946635730843
Training time: 1.0
		
Iteration: 7
Labeled set size: 220
Unlabelled set size: 280
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.8
Correctly Classified Instances: 69.2
Weighted Precision: 0.6415865384615385
Weighted AreaUnderROC: 0.54
Root mean squared error: 0.5524902856938357
Relative absolute error: 61.94594594594582
Root relative squared error: 110.49805713876715
Weighted TruePositiveRate: 0.692
Weighted MatthewsCorrelation: 0.12009611535381536
Weighted FMeasure: 0.6361994094146257
Iteration time: 20.0
Weighted AreaUnderPRC: 0.5998288461538461
Mean absolute error: 0.3097297297297291
Coverage of cases: 69.2
Instances selection time: 20.0
Test time: 34.0
Accumulative iteration time: 178.0
Weighted Recall: 0.692
Weighted FalsePositiveRate: 0.612
Kappa statistic: 0.0983606557377048
Training time: 0.0
		
Iteration: 8
Labeled set size: 240
Unlabelled set size: 260
	
Mean region size: 50.0
Incorrectly Classified Instances: 31.0
Correctly Classified Instances: 69.0
Weighted Precision: 0.6360242544911534
Weighted AreaUnderROC: 0.5347619047619048
Root mean squared error: 0.5544863315405615
Relative absolute error: 62.31404958677711
Root relative squared error: 110.8972663081123
Weighted TruePositiveRate: 0.69
Weighted MatthewsCorrelation: 0.10715856811468825
Weighted FMeasure: 0.6308759778919567
Iteration time: 20.0
Weighted AreaUnderPRC: 0.597011421331282
Mean absolute error: 0.31157024793388555
Coverage of cases: 69.0
Instances selection time: 20.0
Test time: 37.0
Accumulative iteration time: 198.0
Weighted Recall: 0.69
Weighted FalsePositiveRate: 0.6204761904761905
Kappa statistic: 0.08608490566037706
Training time: 0.0
		
Iteration: 9
Labeled set size: 260
Unlabelled set size: 240
	
Mean region size: 50.0
Incorrectly Classified Instances: 31.0
Correctly Classified Instances: 69.0
Weighted Precision: 0.6360242544911534
Weighted AreaUnderROC: 0.5347619047619048
Root mean squared error: 0.5546603967310834
Relative absolute error: 62.29007633587776
Root relative squared error: 110.93207934621667
Weighted TruePositiveRate: 0.69
Weighted MatthewsCorrelation: 0.10715856811468825
Weighted FMeasure: 0.6308759778919567
Iteration time: 20.0
Weighted AreaUnderPRC: 0.597011421331282
Mean absolute error: 0.31145038167938877
Coverage of cases: 69.0
Instances selection time: 19.0
Test time: 39.0
Accumulative iteration time: 218.0
Weighted Recall: 0.69
Weighted FalsePositiveRate: 0.6204761904761905
Kappa statistic: 0.08608490566037706
Training time: 1.0
		
Iteration: 10
Labeled set size: 280
Unlabelled set size: 220
	
Mean region size: 50.0
Incorrectly Classified Instances: 31.4
Correctly Classified Instances: 68.6
Weighted Precision: 0.6267765722605796
Weighted AreaUnderROC: 0.5280952380952381
Root mean squared error: 0.5583776718902448
Relative absolute error: 63.063829787233814
Root relative squared error: 111.67553437804895
Weighted TruePositiveRate: 0.686
Weighted MatthewsCorrelation: 0.08823569977112483
Weighted FMeasure: 0.6240519372151386
Iteration time: 19.0
Weighted AreaUnderPRC: 0.593434963129961
Mean absolute error: 0.3153191489361691
Coverage of cases: 68.6
Instances selection time: 18.0
Test time: 42.0
Accumulative iteration time: 237.0
Weighted Recall: 0.686
Weighted FalsePositiveRate: 0.6298095238095238
Kappa statistic: 0.06990521327014237
Training time: 1.0
		
Iteration: 11
Labeled set size: 300
Unlabelled set size: 200
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.8
Correctly Classified Instances: 69.2
Weighted Precision: 0.6368022328548645
Weighted AreaUnderROC: 0.5323809523809524
Root mean squared error: 0.553146661683904
Relative absolute error: 61.854304635761714
Root relative squared error: 110.6293323367808
Weighted TruePositiveRate: 0.692
Weighted MatthewsCorrelation: 0.1047587977749259
Weighted FMeasure: 0.6281087718400654
Iteration time: 18.0
Weighted AreaUnderPRC: 0.595904625199362
Mean absolute error: 0.30927152317880857
Coverage of cases: 69.2
Instances selection time: 18.0
Test time: 44.0
Accumulative iteration time: 255.0
Weighted Recall: 0.692
Weighted FalsePositiveRate: 0.6272380952380953
Kappa statistic: 0.08114558472553698
Training time: 0.0
		
Iteration: 12
Labeled set size: 320
Unlabelled set size: 180
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.2
Correctly Classified Instances: 69.8
Weighted Precision: 0.6472384448523278
Weighted AreaUnderROC: 0.5347619047619048
Root mean squared error: 0.5478447483476669
Relative absolute error: 60.64596273291938
Root relative squared error: 109.5689496695334
Weighted TruePositiveRate: 0.698
Weighted MatthewsCorrelation: 0.11880383133530194
Weighted FMeasure: 0.6299845379993346
Iteration time: 18.0
Weighted AreaUnderPRC: 0.597564013571389
Mean absolute error: 0.3032298136645969
Coverage of cases: 69.8
Instances selection time: 18.0
Test time: 46.0
Accumulative iteration time: 273.0
Weighted Recall: 0.698
Weighted FalsePositiveRate: 0.6284761904761904
Kappa statistic: 0.08816425120772947
Training time: 0.0
		
Iteration: 13
Labeled set size: 340
Unlabelled set size: 160
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.8
Correctly Classified Instances: 70.2
Weighted Precision: 0.6555760368663595
Weighted AreaUnderROC: 0.5357142857142858
Root mean squared error: 0.5443030953253208
Relative absolute error: 59.836257309941644
Root relative squared error: 108.86061906506417
Weighted TruePositiveRate: 0.702
Weighted MatthewsCorrelation: 0.12828950144769635
Weighted FMeasure: 0.630402918255679
Iteration time: 18.0
Weighted AreaUnderPRC: 0.5984562211981568
Mean absolute error: 0.2991812865497082
Coverage of cases: 70.2
Instances selection time: 17.0
Test time: 49.0
Accumulative iteration time: 291.0
Weighted Recall: 0.702
Weighted FalsePositiveRate: 0.6305714285714286
Kappa statistic: 0.09146341463414609
Training time: 1.0
		
Iteration: 14
Labeled set size: 360
Unlabelled set size: 140
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.0
Correctly Classified Instances: 70.0
Weighted Precision: 0.6495726495726497
Weighted AreaUnderROC: 0.5304761904761904
Root mean squared error: 0.5462144015504883
Relative absolute error: 60.220994475138156
Root relative squared error: 109.24288031009766
Weighted TruePositiveRate: 0.7
Weighted MatthewsCorrelation: 0.11412279800291011
Weighted FMeasure: 0.6243853945565437
Iteration time: 16.0
Weighted AreaUnderPRC: 0.5955350427350428
Mean absolute error: 0.3011049723756908
Coverage of cases: 70.0
Instances selection time: 15.0
Test time: 52.0
Accumulative iteration time: 307.0
Weighted Recall: 0.7
Weighted FalsePositiveRate: 0.6390476190476191
Kappa statistic: 0.07862407862407848
Training time: 1.0
		
Iteration: 15
Labeled set size: 380
Unlabelled set size: 120
	
Mean region size: 50.0
Incorrectly Classified Instances: 31.6
Correctly Classified Instances: 68.4
Weighted Precision: 0.6160869565217392
Weighted AreaUnderROC: 0.5190476190476191
Root mean squared error: 0.5606713855910094
Relative absolute error: 63.392670157068075
Root relative squared error: 112.13427711820188
Weighted TruePositiveRate: 0.684
Weighted MatthewsCorrelation: 0.06434894520877868
Weighted FMeasure: 0.6139831059129305
Iteration time: 14.0
Weighted AreaUnderPRC: 0.5888695652173912
Mean absolute error: 0.3169633507853404
Coverage of cases: 68.4
Instances selection time: 14.0
Test time: 56.0
Accumulative iteration time: 321.0
Weighted Recall: 0.684
Weighted FalsePositiveRate: 0.645904761904762
Kappa statistic: 0.0481927710843374
Training time: 0.0
		
Iteration: 16
Labeled set size: 400
Unlabelled set size: 100
	
Mean region size: 50.0
Incorrectly Classified Instances: 31.6
Correctly Classified Instances: 68.4
Weighted Precision: 0.6284504235176881
Weighted AreaUnderROC: 0.5323809523809524
Root mean squared error: 0.5607441918220478
Relative absolute error: 63.38308457711419
Root relative squared error: 112.14883836440957
Weighted TruePositiveRate: 0.684
Weighted MatthewsCorrelation: 0.0956169653377046
Weighted FMeasure: 0.6287023352054388
Iteration time: 13.0
Weighted AreaUnderPRC: 0.5955199468526822
Mean absolute error: 0.31691542288557095
Coverage of cases: 68.4
Instances selection time: 12.0
Test time: 56.0
Accumulative iteration time: 334.0
Weighted Recall: 0.684
Weighted FalsePositiveRate: 0.6192380952380951
Kappa statistic: 0.07925407925407928
Training time: 1.0
		
Iteration: 17
Labeled set size: 420
Unlabelled set size: 80
	
Mean region size: 50.0
Incorrectly Classified Instances: 32.2
Correctly Classified Instances: 67.8
Weighted Precision: 0.6387535754528908
Weighted AreaUnderROC: 0.5528571428571429
Root mean squared error: 0.5661091316847728
Relative absolute error: 64.56872037914687
Root relative squared error: 113.22182633695456
Weighted TruePositiveRate: 0.678
Weighted MatthewsCorrelation: 0.13019858302295956
Weighted FMeasure: 0.6457677479282193
Iteration time: 11.0
Weighted AreaUnderPRC: 0.6057598509144492
Mean absolute error: 0.32284360189573436
Coverage of cases: 67.8
Instances selection time: 10.0
Test time: 59.0
Accumulative iteration time: 345.0
Weighted Recall: 0.678
Weighted FalsePositiveRate: 0.5722857142857143
Kappa statistic: 0.12117903930131005
Training time: 1.0
		
Iteration: 18
Labeled set size: 440
Unlabelled set size: 60
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.0
Correctly Classified Instances: 66.0
Weighted Precision: 0.6302447552447553
Weighted AreaUnderROC: 0.5514285714285714
Root mean squared error: 0.5817788730367006
Relative absolute error: 68.1447963800906
Root relative squared error: 116.35577460734012
Weighted TruePositiveRate: 0.66
Weighted MatthewsCorrelation: 0.11613144917081457
Weighted FMeasure: 0.6396951721517384
Iteration time: 9.0
Weighted AreaUnderPRC: 0.6044321678321678
Mean absolute error: 0.340723981900453
Coverage of cases: 66.0
Instances selection time: 8.0
Test time: 61.0
Accumulative iteration time: 354.0
Weighted Recall: 0.66
Weighted FalsePositiveRate: 0.5571428571428572
Kappa statistic: 0.11273486430062635
Training time: 1.0
		
Iteration: 19
Labeled set size: 460
Unlabelled set size: 40
	
Mean region size: 50.0
Incorrectly Classified Instances: 33.2
Correctly Classified Instances: 66.8
Weighted Precision: 0.6470186781609195
Weighted AreaUnderROC: 0.5723809523809524
Root mean squared error: 0.5749499592420123
Relative absolute error: 66.54545454545456
Root relative squared error: 114.98999184840247
Weighted TruePositiveRate: 0.668
Weighted MatthewsCorrelation: 0.15715902787873062
Weighted FMeasure: 0.6544713281842207
Iteration time: 6.0
Weighted AreaUnderPRC: 0.6155867816091954
Mean absolute error: 0.3327272727272728
Coverage of cases: 66.8
Instances selection time: 5.0
Test time: 63.0
Accumulative iteration time: 360.0
Weighted Recall: 0.668
Weighted FalsePositiveRate: 0.5232380952380952
Kappa statistic: 0.15478615071283114
Training time: 1.0
		
Iteration: 20
Labeled set size: 480
Unlabelled set size: 20
	
Mean region size: 50.0
Incorrectly Classified Instances: 33.6
Correctly Classified Instances: 66.4
Weighted Precision: 0.6476407000686343
Weighted AreaUnderROC: 0.5752380952380952
Root mean squared error: 0.5784549364144382
Relative absolute error: 67.33609958506224
Root relative squared error: 115.69098728288765
Weighted TruePositiveRate: 0.664
Weighted MatthewsCorrelation: 0.15967711398361675
Weighted FMeasure: 0.6540749230862036
Iteration time: 4.0
Weighted AreaUnderPRC: 0.6169543239533287
Mean absolute error: 0.3366804979253112
Coverage of cases: 66.4
Instances selection time: 3.0
Test time: 65.0
Accumulative iteration time: 364.0
Weighted Recall: 0.664
Weighted FalsePositiveRate: 0.5135238095238095
Kappa statistic: 0.15831663326653314
Training time: 1.0
		
Time end:Tue Oct 31 11.35.33 EET 2017