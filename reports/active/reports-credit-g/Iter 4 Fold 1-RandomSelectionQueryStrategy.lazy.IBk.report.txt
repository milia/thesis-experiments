Tue Oct 31 11.34.21 EET 2017
Dataset: german_credit
Test set size: 500
Initial Labelled set size: 100
Initial Unlabelled set size: 400
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Tue Oct 31 11.34.21 EET 2017
		
Iteration: 1
Labeled set size: 100
Unlabelled set size: 400
	
Mean region size: 50.0
Incorrectly Classified Instances: 32.4
Correctly Classified Instances: 67.6
Weighted Precision: 0.6558369252873563
Weighted AreaUnderROC: 0.5819047619047619
Root mean squared error: 0.5636871257192716
Relative absolute error: 65.49019607843137
Root relative squared error: 112.73742514385432
Weighted TruePositiveRate: 0.676
Weighted MatthewsCorrelation: 0.17783784733645835
Weighted FMeasure: 0.6627973202761673
Iteration time: 1.0
Weighted AreaUnderPRC: 0.6210415229885057
Mean absolute error: 0.3274509803921568
Coverage of cases: 67.6
Instances selection time: 1.0
Test time: 18.0
Accumulative iteration time: 1.0
Weighted Recall: 0.676
Weighted FalsePositiveRate: 0.5121904761904762
Kappa statistic: 0.17515274949083523
Training time: 0.0
		
Iteration: 2
Labeled set size: 120
Unlabelled set size: 380
	
Mean region size: 50.0
Incorrectly Classified Instances: 32.8
Correctly Classified Instances: 67.2
Weighted Precision: 0.64175
Weighted AreaUnderROC: 0.5619047619047619
Root mean squared error: 0.5680582162595047
Relative absolute error: 66.16393442622967
Root relative squared error: 113.61164325190094
Weighted TruePositiveRate: 0.672
Weighted MatthewsCorrelation: 0.14184162865339503
Weighted FMeasure: 0.6501333333333335
Iteration time: 1.0
Weighted AreaUnderPRC: 0.610225
Mean absolute error: 0.3308196721311484
Coverage of cases: 67.2
Instances selection time: 1.0
Test time: 21.0
Accumulative iteration time: 2.0
Weighted Recall: 0.672
Weighted FalsePositiveRate: 0.5481904761904762
Kappa statistic: 0.13684210526315801
Training time: 0.0
		
Iteration: 3
Labeled set size: 140
Unlabelled set size: 360
	
Mean region size: 50.0
Incorrectly Classified Instances: 35.8
Correctly Classified Instances: 64.2
Weighted Precision: 0.6247999999999999
Weighted AreaUnderROC: 0.55
Root mean squared error: 0.5941442079272738
Relative absolute error: 72.00000000000003
Root relative squared error: 118.82884158545477
Weighted TruePositiveRate: 0.642
Weighted MatthewsCorrelation: 0.10583005244258362
Weighted FMeasure: 0.6318996865203762
Iteration time: 1.0
Weighted AreaUnderPRC: 0.603352
Mean absolute error: 0.36000000000000015
Coverage of cases: 64.2
Instances selection time: 1.0
Test time: 23.0
Accumulative iteration time: 3.0
Weighted Recall: 0.642
Weighted FalsePositiveRate: 0.542
Kappa statistic: 0.1050000000000001
Training time: 0.0
		
Iteration: 4
Labeled set size: 160
Unlabelled set size: 340
	
Mean region size: 50.0
Incorrectly Classified Instances: 35.8
Correctly Classified Instances: 64.2
Weighted Precision: 0.6247999999999999
Weighted AreaUnderROC: 0.55
Root mean squared error: 0.5946581798488523
Relative absolute error: 71.95061728395056
Root relative squared error: 118.93163596977045
Weighted TruePositiveRate: 0.642
Weighted MatthewsCorrelation: 0.10583005244258362
Weighted FMeasure: 0.6318996865203762
Iteration time: 1.0
Weighted AreaUnderPRC: 0.603352
Mean absolute error: 0.35975308641975284
Coverage of cases: 64.2
Instances selection time: 1.0
Test time: 26.0
Accumulative iteration time: 4.0
Weighted Recall: 0.642
Weighted FalsePositiveRate: 0.542
Kappa statistic: 0.1050000000000001
Training time: 0.0
		
Iteration: 5
Labeled set size: 180
Unlabelled set size: 320
	
Mean region size: 50.0
Incorrectly Classified Instances: 35.0
Correctly Classified Instances: 65.0
Weighted Precision: 0.636024572180781
Weighted AreaUnderROC: 0.5633333333333334
Root mean squared error: 0.5883740610738005
Relative absolute error: 70.32967032967031
Root relative squared error: 117.67481221476011
Weighted TruePositiveRate: 0.65
Weighted MatthewsCorrelation: 0.13266616760761749
Weighted FMeasure: 0.6419250443678882
Iteration time: 0.0
Weighted AreaUnderPRC: 0.6102960655258154
Mean absolute error: 0.3516483516483515
Coverage of cases: 65.0
Instances selection time: 0.0
Test time: 29.0
Accumulative iteration time: 4.0
Weighted Recall: 0.65
Weighted FalsePositiveRate: 0.5233333333333332
Kappa statistic: 0.1319444444444445
Training time: 0.0
		
Iteration: 6
Labeled set size: 200
Unlabelled set size: 300
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.4
Correctly Classified Instances: 65.6
Weighted Precision: 0.6403870639164757
Weighted AreaUnderROC: 0.5676190476190476
Root mean squared error: 0.583625365116336
Relative absolute error: 69.10891089108911
Root relative squared error: 116.7250730232672
Weighted TruePositiveRate: 0.656
Weighted MatthewsCorrelation: 0.14274392140653896
Weighted FMeasure: 0.6467451357194332
Iteration time: 1.0
Weighted AreaUnderPRC: 0.6126789236906883
Mean absolute error: 0.3455445544554456
Coverage of cases: 65.6
Instances selection time: 1.0
Test time: 31.0
Accumulative iteration time: 5.0
Weighted Recall: 0.656
Weighted FalsePositiveRate: 0.5207619047619046
Kappa statistic: 0.14171656686626763
Training time: 0.0
		
Iteration: 7
Labeled set size: 220
Unlabelled set size: 280
	
Mean region size: 50.0
Incorrectly Classified Instances: 36.2
Correctly Classified Instances: 63.8
Weighted Precision: 0.6331687919923215
Weighted AreaUnderROC: 0.5623809523809524
Root mean squared error: 0.5989649650017687
Relative absolute error: 72.64864864864872
Root relative squared error: 119.79299300035373
Weighted TruePositiveRate: 0.638
Weighted MatthewsCorrelation: 0.12652009854195664
Weighted FMeasure: 0.6354678471260095
Iteration time: 1.0
Weighted AreaUnderPRC: 0.6095615404203638
Mean absolute error: 0.3632432432432436
Coverage of cases: 63.8
Instances selection time: 1.0
Test time: 33.0
Accumulative iteration time: 6.0
Weighted Recall: 0.638
Weighted FalsePositiveRate: 0.5132380952380953
Kappa statistic: 0.12644787644787644
Training time: 0.0
		
Iteration: 8
Labeled set size: 240
Unlabelled set size: 260
	
Mean region size: 50.0
Incorrectly Classified Instances: 36.8
Correctly Classified Instances: 63.2
Weighted Precision: 0.6277855805243446
Weighted AreaUnderROC: 0.5561904761904762
Root mean squared error: 0.6041322314049591
Relative absolute error: 73.81818181818184
Root relative squared error: 120.82644628099182
Weighted TruePositiveRate: 0.632
Weighted MatthewsCorrelation: 0.11372766300622612
Weighted FMeasure: 0.629808637335954
Iteration time: 1.0
Weighted AreaUnderPRC: 0.6063161360799001
Mean absolute error: 0.3690909090909092
Coverage of cases: 63.2
Instances selection time: 1.0
Test time: 36.0
Accumulative iteration time: 7.0
Weighted Recall: 0.632
Weighted FalsePositiveRate: 0.5196190476190476
Kappa statistic: 0.11368015414258194
Training time: 0.0
		
Iteration: 9
Labeled set size: 260
Unlabelled set size: 240
	
Mean region size: 50.0
Incorrectly Classified Instances: 35.8
Correctly Classified Instances: 64.2
Weighted Precision: 0.6385915492957747
Weighted AreaUnderROC: 0.569047619047619
Root mean squared error: 0.5960551513812646
Relative absolute error: 71.81679389312986
Root relative squared error: 119.21103027625293
Weighted TruePositiveRate: 0.642
Weighted MatthewsCorrelation: 0.13946337703691453
Weighted FMeasure: 0.6402356052410145
Iteration time: 0.0
Weighted AreaUnderPRC: 0.6130845070422535
Mean absolute error: 0.3590839694656493
Coverage of cases: 64.2
Instances selection time: 0.0
Test time: 39.0
Accumulative iteration time: 7.0
Weighted Recall: 0.642
Weighted FalsePositiveRate: 0.5039047619047619
Kappa statistic: 0.13942307692307704
Training time: 0.0
		
Iteration: 10
Labeled set size: 280
Unlabelled set size: 220
	
Mean region size: 50.0
Incorrectly Classified Instances: 35.8
Correctly Classified Instances: 64.2
Weighted Precision: 0.6399549054749378
Weighted AreaUnderROC: 0.570952380952381
Root mean squared error: 0.5962160411528536
Relative absolute error: 71.80141843971633
Root relative squared error: 119.24320823057073
Weighted TruePositiveRate: 0.642
Weighted MatthewsCorrelation: 0.14273503996637943
Weighted FMeasure: 0.6409557883242094
Iteration time: 0.0
Weighted AreaUnderPRC: 0.6140783912431828
Mean absolute error: 0.3590070921985817
Coverage of cases: 64.2
Instances selection time: 0.0
Test time: 42.0
Accumulative iteration time: 7.0
Weighted Recall: 0.642
Weighted FalsePositiveRate: 0.5000952380952381
Kappa statistic: 0.14272030651340994
Training time: 0.0
		
Iteration: 11
Labeled set size: 300
Unlabelled set size: 200
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.4
Correctly Classified Instances: 65.6
Weighted Precision: 0.654691339066339
Weighted AreaUnderROC: 0.5885714285714284
Root mean squared error: 0.5845791808951301
Relative absolute error: 69.00662251655653
Root relative squared error: 116.91583617902602
Weighted TruePositiveRate: 0.656
Weighted MatthewsCorrelation: 0.17782839445076787
Weighted FMeasure: 0.655335666073921
Iteration time: 0.0
Weighted AreaUnderPRC: 0.623840816953317
Mean absolute error: 0.3450331125827827
Coverage of cases: 65.6
Instances selection time: 0.0
Test time: 44.0
Accumulative iteration time: 7.0
Weighted Recall: 0.656
Weighted FalsePositiveRate: 0.47885714285714287
Kappa statistic: 0.17782026768642453
Training time: 0.0
		
Iteration: 12
Labeled set size: 320
Unlabelled set size: 180
	
Mean region size: 50.0
Incorrectly Classified Instances: 35.2
Correctly Classified Instances: 64.8
Weighted Precision: 0.6506868853689662
Weighted AreaUnderROC: 0.5847619047619048
Root mean squared error: 0.591458628549714
Relative absolute error: 70.58385093167698
Root relative squared error: 118.2917257099428
Weighted TruePositiveRate: 0.648
Weighted MatthewsCorrelation: 0.16827203431052318
Weighted FMeasure: 0.6493042952208108
Iteration time: 1.0
Weighted AreaUnderPRC: 0.6215462502815104
Mean absolute error: 0.35291925465838486
Coverage of cases: 64.8
Instances selection time: 1.0
Test time: 46.0
Accumulative iteration time: 8.0
Weighted Recall: 0.648
Weighted FalsePositiveRate: 0.4784761904761905
Kappa statistic: 0.16824196597353505
Training time: 0.0
		
Iteration: 13
Labeled set size: 340
Unlabelled set size: 160
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.0
Correctly Classified Instances: 66.0
Weighted Precision: 0.6587070024570025
Weighted AreaUnderROC: 0.5933333333333334
Root mean squared error: 0.5813950855877932
Relative absolute error: 68.18713450292407
Root relative squared error: 116.27901711755864
Weighted TruePositiveRate: 0.66
Weighted MatthewsCorrelation: 0.18738906081908868
Weighted FMeasure: 0.6593433908870151
Iteration time: 1.0
Weighted AreaUnderPRC: 0.6265740786240787
Mean absolute error: 0.3409356725146203
Coverage of cases: 66.0
Instances selection time: 1.0
Test time: 53.0
Accumulative iteration time: 9.0
Weighted Recall: 0.66
Weighted FalsePositiveRate: 0.4733333333333334
Kappa statistic: 0.18738049713193122
Training time: 0.0
		
Iteration: 14
Labeled set size: 360
Unlabelled set size: 140
	
Mean region size: 50.0
Incorrectly Classified Instances: 33.4
Correctly Classified Instances: 66.6
Weighted Precision: 0.6615795968737145
Weighted AreaUnderROC: 0.5957142857142858
Root mean squared error: 0.5763352558687446
Relative absolute error: 66.9834254143648
Root relative squared error: 115.26705117374891
Weighted TruePositiveRate: 0.666
Weighted MatthewsCorrelation: 0.19412625806819306
Weighted FMeasure: 0.6636637042543846
Iteration time: 0.0
Weighted AreaUnderPRC: 0.6281138508550272
Mean absolute error: 0.33491712707182403
Coverage of cases: 66.6
Instances selection time: 0.0
Test time: 52.0
Accumulative iteration time: 9.0
Weighted Recall: 0.666
Weighted FalsePositiveRate: 0.47457142857142853
Kappa statistic: 0.19401544401544407
Training time: 0.0
		
Iteration: 15
Labeled set size: 380
Unlabelled set size: 120
	
Mean region size: 50.0
Incorrectly Classified Instances: 32.8
Correctly Classified Instances: 67.2
Weighted Precision: 0.6682818352059925
Weighted AreaUnderROC: 0.6038095238095237
Root mean squared error: 0.5712176252492669
Relative absolute error: 65.78010471204205
Root relative squared error: 114.24352504985337
Weighted TruePositiveRate: 0.672
Weighted MatthewsCorrelation: 0.21010703843523135
Weighted FMeasure: 0.6700468289298722
Iteration time: 1.0
Weighted AreaUnderPRC: 0.632870443196005
Mean absolute error: 0.3289005235602102
Coverage of cases: 67.2
Instances selection time: 1.0
Test time: 54.0
Accumulative iteration time: 10.0
Weighted Recall: 0.672
Weighted FalsePositiveRate: 0.46438095238095234
Kappa statistic: 0.21001926782273617
Training time: 0.0
		
Iteration: 16
Labeled set size: 400
Unlabelled set size: 100
	
Mean region size: 50.0
Incorrectly Classified Instances: 33.0
Correctly Classified Instances: 67.0
Weighted Precision: 0.6656382832853421
Weighted AreaUnderROC: 0.6004761904761905
Root mean squared error: 0.5730308865330626
Relative absolute error: 66.16915422885566
Root relative squared error: 114.60617730661251
Weighted TruePositiveRate: 0.67
Weighted MatthewsCorrelation: 0.20378428085765543
Weighted FMeasure: 0.6676916838441523
Iteration time: 0.0
Weighted AreaUnderPRC: 0.630920886956181
Mean absolute error: 0.3308457711442783
Coverage of cases: 67.0
Instances selection time: 0.0
Test time: 57.0
Accumulative iteration time: 10.0
Weighted Recall: 0.67
Weighted FalsePositiveRate: 0.4690476190476191
Kappa statistic: 0.20366795366795373
Training time: 0.0
		
Iteration: 17
Labeled set size: 420
Unlabelled set size: 80
	
Mean region size: 50.0
Incorrectly Classified Instances: 32.4
Correctly Classified Instances: 67.6
Weighted Precision: 0.6723314606741573
Weighted AreaUnderROC: 0.6085714285714285
Root mean squared error: 0.5678644823405503
Relative absolute error: 64.9668246445497
Root relative squared error: 113.57289646811006
Weighted TruePositiveRate: 0.676
Weighted MatthewsCorrelation: 0.2197449759781319
Weighted FMeasure: 0.6740706480892641
Iteration time: 0.0
Weighted AreaUnderPRC: 0.6357404494382023
Mean absolute error: 0.3248341232227485
Coverage of cases: 67.6
Instances selection time: 0.0
Test time: 58.0
Accumulative iteration time: 10.0
Weighted Recall: 0.676
Weighted FalsePositiveRate: 0.4588571428571429
Kappa statistic: 0.21965317919075159
Training time: 0.0
		
Iteration: 18
Labeled set size: 440
Unlabelled set size: 60
	
Mean region size: 50.0
Incorrectly Classified Instances: 32.4
Correctly Classified Instances: 67.6
Weighted Precision: 0.6687244775402356
Weighted AreaUnderROC: 0.6028571428571429
Root mean squared error: 0.5679252198149852
Relative absolute error: 64.95927601809956
Root relative squared error: 113.58504396299705
Weighted TruePositiveRate: 0.676
Weighted MatthewsCorrelation: 0.2108872265594399
Weighted FMeasure: 0.6719803370786517
Iteration time: 0.0
Weighted AreaUnderPRC: 0.6325394186884459
Mean absolute error: 0.32479638009049777
Coverage of cases: 67.6
Instances selection time: 0.0
Test time: 61.0
Accumulative iteration time: 10.0
Weighted Recall: 0.676
Weighted FalsePositiveRate: 0.47028571428571425
Kappa statistic: 0.21052631578947376
Training time: 0.0
		
Iteration: 19
Labeled set size: 460
Unlabelled set size: 40
	
Mean region size: 50.0
Incorrectly Classified Instances: 31.6
Correctly Classified Instances: 68.4
Weighted Precision: 0.6780952380952382
Weighted AreaUnderROC: 0.6142857142857143
Root mean squared error: 0.5609248788399888
Relative absolute error: 63.359307359307415
Root relative squared error: 112.18497576799776
Weighted TruePositiveRate: 0.684
Weighted MatthewsCorrelation: 0.23328473740792172
Weighted FMeasure: 0.6807770762506071
Iteration time: 1.0
Weighted AreaUnderPRC: 0.6394285714285716
Mean absolute error: 0.3167965367965371
Coverage of cases: 68.4
Instances selection time: 1.0
Test time: 63.0
Accumulative iteration time: 11.0
Weighted Recall: 0.684
Weighted FalsePositiveRate: 0.4554285714285715
Kappa statistic: 0.23300970873786428
Training time: 0.0
		
Iteration: 20
Labeled set size: 480
Unlabelled set size: 20
	
Mean region size: 50.0
Incorrectly Classified Instances: 31.6
Correctly Classified Instances: 68.4
Weighted Precision: 0.6804307116104868
Weighted AreaUnderROC: 0.618095238095238
Root mean squared error: 0.5609751340425876
Relative absolute error: 63.35269709543574
Root relative squared error: 112.19502680851751
Weighted TruePositiveRate: 0.684
Weighted MatthewsCorrelation: 0.23902085106393292
Weighted FMeasure: 0.6821182864080476
Iteration time: 0.0
Weighted AreaUnderPRC: 0.6415975031210986
Mean absolute error: 0.3167634854771787
Coverage of cases: 68.4
Instances selection time: 0.0
Test time: 65.0
Accumulative iteration time: 11.0
Weighted Recall: 0.684
Weighted FalsePositiveRate: 0.4478095238095237
Kappa statistic: 0.23892100192678242
Training time: 0.0
		
Time end:Tue Oct 31 11.34.23 EET 2017