Tue Oct 31 11.30.49 EET 2017
Dataset: german_credit
Test set size: 500
Initial Labelled set size: 100
Initial Unlabelled set size: 400
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Tue Oct 31 11.30.49 EET 2017
		
Iteration: 1
Labeled set size: 100
Unlabelled set size: 400
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.2
Correctly Classified Instances: 65.8
Weighted Precision: 0.6470242363401691
Weighted AreaUnderROC: 0.5766666666666668
Root mean squared error: 0.5791288583080483
Relative absolute error: 69.01960784313728
Root relative squared error: 115.82577166160965
Weighted TruePositiveRate: 0.658
Weighted MatthewsCorrelation: 0.1590220603549262
Weighted FMeasure: 0.6517823084997857
Iteration time: 55.0
Weighted AreaUnderPRC: 0.6175104832927004
Mean absolute error: 0.3450980392156864
Coverage of cases: 65.8
Instances selection time: 49.0
Test time: 19.0
Accumulative iteration time: 55.0
Weighted Recall: 0.658
Weighted FalsePositiveRate: 0.5046666666666667
Kappa statistic: 0.1584645669291339
Training time: 6.0
		
Iteration: 2
Labeled set size: 120
Unlabelled set size: 380
	
Mean region size: 50.0
Incorrectly Classified Instances: 32.0
Correctly Classified Instances: 68.0
Weighted Precision: 0.6535062160062161
Weighted AreaUnderROC: 0.5752380952380952
Root mean squared error: 0.5610893730956893
Relative absolute error: 64.59016393442633
Root relative squared error: 112.21787461913786
Weighted TruePositiveRate: 0.68
Weighted MatthewsCorrelation: 0.16989600897211762
Weighted FMeasure: 0.6608895737898716
Iteration time: 33.0
Weighted AreaUnderPRC: 0.6176615773115773
Mean absolute error: 0.32295081967213163
Coverage of cases: 68.0
Instances selection time: 32.0
Test time: 21.0
Accumulative iteration time: 88.0
Weighted Recall: 0.68
Weighted FalsePositiveRate: 0.5295238095238095
Kappa statistic: 0.1649269311064719
Training time: 1.0
		
Iteration: 3
Labeled set size: 140
Unlabelled set size: 360
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.8
Correctly Classified Instances: 70.2
Weighted Precision: 0.6705122648868856
Weighted AreaUnderROC: 0.5814285714285714
Root mean squared error: 0.5420815531228335
Relative absolute error: 60.169014084507246
Root relative squared error: 108.41631062456669
Weighted TruePositiveRate: 0.702
Weighted MatthewsCorrelation: 0.20057619546780256
Weighted FMeasure: 0.6721701518093457
Iteration time: 34.0
Weighted AreaUnderPRC: 0.6226484701395509
Mean absolute error: 0.3008450704225362
Coverage of cases: 70.2
Instances selection time: 34.0
Test time: 23.0
Accumulative iteration time: 122.0
Weighted Recall: 0.702
Weighted FalsePositiveRate: 0.5391428571428571
Kappa statistic: 0.18668122270742335
Training time: 0.0
		
Iteration: 4
Labeled set size: 160
Unlabelled set size: 340
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.2
Correctly Classified Instances: 70.8
Weighted Precision: 0.675713741910925
Weighted AreaUnderROC: 0.58
Root mean squared error: 0.5370597412541405
Relative absolute error: 58.91358024691349
Root relative squared error: 107.41194825082809
Weighted TruePositiveRate: 0.708
Weighted MatthewsCorrelation: 0.20648046754900062
Weighted FMeasure: 0.6727632547864506
Iteration time: 36.0
Weighted AreaUnderPRC: 0.6225531785306433
Mean absolute error: 0.29456790123456744
Coverage of cases: 70.8
Instances selection time: 36.0
Test time: 27.0
Accumulative iteration time: 158.0
Weighted Recall: 0.708
Weighted FalsePositiveRate: 0.548
Kappa statistic: 0.1870824053452114
Training time: 0.0
		
Iteration: 5
Labeled set size: 180
Unlabelled set size: 320
	
Mean region size: 50.0
Incorrectly Classified Instances: 28.6
Correctly Classified Instances: 71.4
Weighted Precision: 0.6829926579573266
Weighted AreaUnderROC: 0.5804761904761905
Root mean squared error: 0.5318715375424523
Relative absolute error: 57.670329670329714
Root relative squared error: 106.37430750849046
Weighted TruePositiveRate: 0.714
Weighted MatthewsCorrelation: 0.21651884799064872
Weighted FMeasure: 0.6744625127272513
Iteration time: 41.0
Weighted AreaUnderPRC: 0.6236448864223916
Mean absolute error: 0.28835164835164856
Coverage of cases: 71.4
Instances selection time: 40.0
Test time: 30.0
Accumulative iteration time: 199.0
Weighted Recall: 0.714
Weighted FalsePositiveRate: 0.5530476190476189
Kappa statistic: 0.19117647058823525
Training time: 1.0
		
Iteration: 6
Labeled set size: 200
Unlabelled set size: 300
	
Mean region size: 50.0
Incorrectly Classified Instances: 28.2
Correctly Classified Instances: 71.8
Weighted Precision: 0.6888198757763975
Weighted AreaUnderROC: 0.5814285714285714
Root mean squared error: 0.5284244772844219
Relative absolute error: 56.83168316831699
Root relative squared error: 105.68489545688438
Weighted TruePositiveRate: 0.718
Weighted MatthewsCorrelation: 0.22489275764262948
Weighted FMeasure: 0.6759954900943141
Iteration time: 38.0
Weighted AreaUnderPRC: 0.6248211180124222
Mean absolute error: 0.28415841584158497
Coverage of cases: 71.8
Instances selection time: 37.0
Test time: 31.0
Accumulative iteration time: 237.0
Weighted Recall: 0.718
Weighted FalsePositiveRate: 0.5551428571428572
Kappa statistic: 0.19520547945205483
Training time: 1.0
		
Iteration: 7
Labeled set size: 220
Unlabelled set size: 280
	
Mean region size: 50.0
Incorrectly Classified Instances: 28.2
Correctly Classified Instances: 71.8
Weighted Precision: 0.6889274770173646
Weighted AreaUnderROC: 0.5738095238095238
Root mean squared error: 0.5286584436290508
Relative absolute error: 56.79279279279269
Root relative squared error: 105.73168872581016
Weighted TruePositiveRate: 0.718
Weighted MatthewsCorrelation: 0.21620249455922333
Weighted FMeasure: 0.6695075931891394
Iteration time: 38.0
Weighted AreaUnderPRC: 0.6208161389172625
Mean absolute error: 0.28396396396396345
Coverage of cases: 71.8
Instances selection time: 38.0
Test time: 34.0
Accumulative iteration time: 275.0
Weighted Recall: 0.718
Weighted FalsePositiveRate: 0.5703809523809523
Kappa statistic: 0.18023255813953473
Training time: 0.0
		
Iteration: 8
Labeled set size: 240
Unlabelled set size: 260
	
Mean region size: 50.0
Incorrectly Classified Instances: 28.8
Correctly Classified Instances: 71.2
Weighted Precision: 0.6782451923076924
Weighted AreaUnderROC: 0.5638095238095238
Root mean squared error: 0.534450100614761
Relative absolute error: 57.95041322314071
Root relative squared error: 106.8900201229522
Weighted TruePositiveRate: 0.712
Weighted MatthewsCorrelation: 0.19158189830251499
Weighted FMeasure: 0.659822824387702
Iteration time: 38.0
Weighted AreaUnderPRC: 0.6145077609890109
Mean absolute error: 0.28975206611570353
Coverage of cases: 71.2
Instances selection time: 37.0
Test time: 37.0
Accumulative iteration time: 313.0
Weighted Recall: 0.712
Weighted FalsePositiveRate: 0.5843809523809523
Kappa statistic: 0.15690866510538634
Training time: 1.0
		
Iteration: 9
Labeled set size: 260
Unlabelled set size: 240
	
Mean region size: 50.0
Incorrectly Classified Instances: 28.8
Correctly Classified Instances: 71.2
Weighted Precision: 0.6782632743362832
Weighted AreaUnderROC: 0.5600000000000002
Root mean squared error: 0.5346177088660462
Relative absolute error: 57.923664122137275
Root relative squared error: 106.92354177320924
Weighted TruePositiveRate: 0.712
Weighted MatthewsCorrelation: 0.1866685103153792
Weighted FMeasure: 0.6561323962820222
Iteration time: 38.0
Weighted AreaUnderPRC: 0.6125174778761061
Mean absolute error: 0.2896183206106864
Coverage of cases: 71.2
Instances selection time: 38.0
Test time: 39.0
Accumulative iteration time: 351.0
Weighted Recall: 0.712
Weighted FalsePositiveRate: 0.592
Kappa statistic: 0.14893617021276595
Training time: 0.0
		
Iteration: 10
Labeled set size: 280
Unlabelled set size: 220
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.0
Correctly Classified Instances: 71.0
Weighted Precision: 0.6743459677798131
Weighted AreaUnderROC: 0.5566666666666666
Root mean squared error: 0.5366151667922994
Relative absolute error: 58.29787234042535
Root relative squared error: 107.32303335845987
Weighted TruePositiveRate: 0.71
Weighted MatthewsCorrelation: 0.17796691987735347
Weighted FMeasure: 0.6527868209948734
Iteration time: 36.0
Weighted AreaUnderPRC: 0.6104511671598328
Mean absolute error: 0.29148936170212675
Coverage of cases: 71.0
Instances selection time: 36.0
Test time: 42.0
Accumulative iteration time: 387.0
Weighted Recall: 0.71
Weighted FalsePositiveRate: 0.5966666666666667
Kappa statistic: 0.14099526066350704
Training time: 0.0
		
Iteration: 11
Labeled set size: 300
Unlabelled set size: 200
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.0
Correctly Classified Instances: 71.0
Weighted Precision: 0.6743589743589744
Weighted AreaUnderROC: 0.5547619047619048
Root mean squared error: 0.5367405654783884
Relative absolute error: 58.27814569536434
Root relative squared error: 107.34811309567769
Weighted TruePositiveRate: 0.71
Weighted MatthewsCorrelation: 0.17537845416747636
Weighted FMeasure: 0.6508361204013378
Iteration time: 35.0
Weighted AreaUnderPRC: 0.6094590964590963
Mean absolute error: 0.2913907284768217
Coverage of cases: 71.0
Instances selection time: 35.0
Test time: 44.0
Accumulative iteration time: 422.0
Weighted Recall: 0.71
Weighted FalsePositiveRate: 0.6004761904761905
Kappa statistic: 0.13690476190476172
Training time: 0.0
		
Iteration: 12
Labeled set size: 320
Unlabelled set size: 180
	
Mean region size: 50.0
Incorrectly Classified Instances: 28.8
Correctly Classified Instances: 71.2
Weighted Precision: 0.6788521522145976
Weighted AreaUnderROC: 0.5542857142857143
Root mean squared error: 0.5349960979426466
Relative absolute error: 57.863354037267236
Root relative squared error: 106.99921958852931
Weighted TruePositiveRate: 0.712
Weighted MatthewsCorrelation: 0.17936504869495293
Weighted FMeasure: 0.6502475247524754
Iteration time: 34.0
Weighted AreaUnderPRC: 0.609556082345602
Mean absolute error: 0.28931677018633617
Coverage of cases: 71.2
Instances selection time: 33.0
Test time: 46.0
Accumulative iteration time: 456.0
Weighted Recall: 0.712
Weighted FalsePositiveRate: 0.6034285714285714
Kappa statistic: 0.13669064748201432
Training time: 1.0
		
Iteration: 13
Labeled set size: 340
Unlabelled set size: 160
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.0
Correctly Classified Instances: 71.0
Weighted Precision: 0.6748706824628734
Weighted AreaUnderROC: 0.549047619047619
Root mean squared error: 0.5369475236989703
Relative absolute error: 58.2456140350878
Root relative squared error: 107.38950473979405
Weighted TruePositiveRate: 0.71
Weighted MatthewsCorrelation: 0.16762732366487806
Weighted FMeasure: 0.6446871391384338
Iteration time: 33.0
Weighted AreaUnderPRC: 0.6065007731242005
Mean absolute error: 0.291228070175439
Coverage of cases: 71.0
Instances selection time: 32.0
Test time: 49.0
Accumulative iteration time: 489.0
Weighted Recall: 0.71
Weighted FalsePositiveRate: 0.611904761904762
Kappa statistic: 0.12439613526570051
Training time: 1.0
		
Iteration: 14
Labeled set size: 360
Unlabelled set size: 140
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.2
Correctly Classified Instances: 70.8
Weighted Precision: 0.670334928229665
Weighted AreaUnderROC: 0.5457142857142858
Root mean squared error: 0.5388825209228685
Relative absolute error: 58.62983425414365
Root relative squared error: 107.7765041845737
Weighted TruePositiveRate: 0.708
Weighted MatthewsCorrelation: 0.1581061631693885
Weighted FMeasure: 0.6411592076302275
Iteration time: 29.0
Weighted AreaUnderPRC: 0.6044494873547506
Mean absolute error: 0.29314917127071827
Coverage of cases: 70.8
Instances selection time: 28.0
Test time: 51.0
Accumulative iteration time: 518.0
Weighted Recall: 0.708
Weighted FalsePositiveRate: 0.6165714285714285
Kappa statistic: 0.11622276029055686
Training time: 1.0
		
Iteration: 15
Labeled set size: 380
Unlabelled set size: 120
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.4
Correctly Classified Instances: 70.6
Weighted Precision: 0.6656178857042788
Weighted AreaUnderROC: 0.5423809523809524
Root mean squared error: 0.5408027236124234
Relative absolute error: 59.01570680628275
Root relative squared error: 108.16054472248469
Weighted TruePositiveRate: 0.706
Weighted MatthewsCorrelation: 0.14838465857094665
Weighted FMeasure: 0.6376028573119955
Iteration time: 27.0
Weighted AreaUnderPRC: 0.6024237814488353
Mean absolute error: 0.29507853403141376
Coverage of cases: 70.6
Instances selection time: 26.0
Test time: 53.0
Accumulative iteration time: 545.0
Weighted Recall: 0.706
Weighted FalsePositiveRate: 0.6212380952380953
Kappa statistic: 0.10800970873786397
Training time: 1.0
		
Iteration: 16
Labeled set size: 400
Unlabelled set size: 100
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.0
Correctly Classified Instances: 70.0
Weighted Precision: 0.6598173515981735
Weighted AreaUnderROC: 0.559047619047619
Root mean squared error: 0.5463640275971794
Relative absolute error: 60.19900497512414
Root relative squared error: 109.27280551943588
Weighted TruePositiveRate: 0.7
Weighted MatthewsCorrelation: 0.16420226285519543
Weighted FMeasure: 0.6544871180921369
Iteration time: 24.0
Weighted AreaUnderPRC: 0.610462100456621
Mean absolute error: 0.3009950248756207
Coverage of cases: 70.0
Instances selection time: 23.0
Test time: 57.0
Accumulative iteration time: 569.0
Weighted Recall: 0.7
Weighted FalsePositiveRate: 0.5819047619047619
Kappa statistic: 0.1418764302059496
Training time: 1.0
		
Iteration: 17
Labeled set size: 420
Unlabelled set size: 80
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.6
Correctly Classified Instances: 69.4
Weighted Precision: 0.6553110262744217
Weighted AreaUnderROC: 0.5623809523809524
Root mean squared error: 0.5518653625303672
Relative absolute error: 61.38388625592411
Root relative squared error: 110.37307250607344
Weighted TruePositiveRate: 0.694
Weighted MatthewsCorrelation: 0.1619147004506277
Weighted FMeasure: 0.6563325657496062
Iteration time: 20.0
Weighted AreaUnderPRC: 0.6117054377466234
Mean absolute error: 0.30691943127962057
Coverage of cases: 69.4
Instances selection time: 19.0
Test time: 58.0
Accumulative iteration time: 589.0
Weighted Recall: 0.694
Weighted FalsePositiveRate: 0.5692380952380953
Kappa statistic: 0.14620535714285712
Training time: 1.0
		
Iteration: 18
Labeled set size: 440
Unlabelled set size: 60
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.6
Correctly Classified Instances: 69.4
Weighted Precision: 0.6636368521454097
Weighted AreaUnderROC: 0.5795238095238096
Root mean squared error: 0.5519243637181634
Relative absolute error: 61.37556561085987
Root relative squared error: 110.38487274363268
Weighted TruePositiveRate: 0.694
Weighted MatthewsCorrelation: 0.1888967446671045
Weighted FMeasure: 0.6684368490971415
Iteration time: 16.0
Weighted AreaUnderPRC: 0.6208932158306242
Mean absolute error: 0.30687782805429936
Coverage of cases: 69.4
Instances selection time: 16.0
Test time: 60.0
Accumulative iteration time: 605.0
Weighted Recall: 0.694
Weighted FalsePositiveRate: 0.534952380952381
Kappa statistic: 0.17918454935622308
Training time: 0.0
		
Iteration: 19
Labeled set size: 460
Unlabelled set size: 40
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.6
Correctly Classified Instances: 69.4
Weighted Precision: 0.6741830737920468
Weighted AreaUnderROC: 0.6004761904761905
Root mean squared error: 0.5519782692687689
Relative absolute error: 61.3679653679654
Root relative squared error: 110.39565385375379
Weighted TruePositiveRate: 0.694
Weighted MatthewsCorrelation: 0.22018011979626578
Weighted FMeasure: 0.6801564249268692
Iteration time: 12.0
Weighted AreaUnderPRC: 0.6323806498822346
Mean absolute error: 0.30683982683982697
Coverage of cases: 69.4
Instances selection time: 12.0
Test time: 62.0
Accumulative iteration time: 617.0
Weighted Recall: 0.694
Weighted FalsePositiveRate: 0.493047619047619
Kappa statistic: 0.21618852459016374
Training time: 0.0
		
Iteration: 20
Labeled set size: 480
Unlabelled set size: 20
	
Mean region size: 50.0
Incorrectly Classified Instances: 31.6
Correctly Classified Instances: 68.4
Weighted Precision: 0.6668421052631579
Weighted AreaUnderROC: 0.5952380952380953
Root mean squared error: 0.5609751340425877
Relative absolute error: 63.35269709543569
Root relative squared error: 112.19502680851754
Weighted TruePositiveRate: 0.684
Weighted MatthewsCorrelation: 0.20437977982832192
Weighted FMeasure: 0.672937595129376
Iteration time: 7.0
Weighted AreaUnderPRC: 0.6287719298245614
Mean absolute error: 0.3167634854771784
Coverage of cases: 68.4
Instances selection time: 6.0
Test time: 64.0
Accumulative iteration time: 624.0
Weighted Recall: 0.684
Weighted FalsePositiveRate: 0.49352380952380953
Kappa statistic: 0.20202020202020218
Training time: 1.0
		
Time end:Tue Oct 31 11.30.51 EET 2017