Wed Nov 01 07.10.30 EET 2017
Dataset: mushroom
Test set size: 4062
Initial Labelled set size: 812
Initial Unlabelled set size: 3250
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Wed Nov 01 07.10.30 EET 2017
		
Iteration: 1
Labeled set size: 812
Unlabelled set size: 3250
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 8.558740007294249E-4
Relative absolute error: 0.14880386397377834
Root relative squared error: 0.171174800145885
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 763.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 7.440193198688917E-4
Coverage of cases: 100.0
Instances selection time: 762.0
Test time: 919.0
Accumulative iteration time: 763.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 2
Labeled set size: 832
Unlabelled set size: 3230
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 8.335835506649454E-4
Relative absolute error: 0.14487838086787475
Root relative squared error: 0.16671671013298908
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 747.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 7.243919043393738E-4
Coverage of cases: 100.0
Instances selection time: 746.0
Test time: 938.0
Accumulative iteration time: 1510.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 3
Labeled set size: 852
Unlabelled set size: 3210
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 8.129917630762503E-4
Relative absolute error: 0.14126851820306022
Root relative squared error: 0.16259835261525005
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 749.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 7.063425910153011E-4
Coverage of cases: 100.0
Instances selection time: 749.0
Test time: 963.0
Accumulative iteration time: 2259.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 4
Labeled set size: 872
Unlabelled set size: 3190
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 7.94769828785594E-4
Relative absolute error: 0.13819250202426148
Root relative squared error: 0.15895396575711881
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 757.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 6.909625101213074E-4
Coverage of cases: 100.0
Instances selection time: 757.0
Test time: 971.0
Accumulative iteration time: 3016.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 5
Labeled set size: 892
Unlabelled set size: 3170
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 7.778088481396614E-4
Relative absolute error: 0.13540746623901798
Root relative squared error: 0.15556176962793228
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 761.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 6.770373311950898E-4
Coverage of cases: 100.0
Instances selection time: 761.0
Test time: 967.0
Accumulative iteration time: 3777.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 6
Labeled set size: 912
Unlabelled set size: 3150
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 7.552046451135085E-4
Relative absolute error: 0.13130216245779727
Root relative squared error: 0.1510409290227017
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 769.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 6.565108122889864E-4
Coverage of cases: 100.0
Instances selection time: 768.0
Test time: 981.0
Accumulative iteration time: 4546.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 7
Labeled set size: 932
Unlabelled set size: 3130
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 7.364064861826674E-4
Relative absolute error: 0.12796080522272346
Root relative squared error: 0.14728129723653346
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 787.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 6.398040261136172E-4
Coverage of cases: 100.0
Instances selection time: 787.0
Test time: 997.0
Accumulative iteration time: 5333.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 8
Labeled set size: 952
Unlabelled set size: 3110
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 7.199149969491126E-4
Relative absolute error: 0.1252086873911284
Root relative squared error: 0.1439829993898225
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 828.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 6.26043436955642E-4
Coverage of cases: 100.0
Instances selection time: 828.0
Test time: 1012.0
Accumulative iteration time: 6161.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 9
Labeled set size: 972
Unlabelled set size: 3090
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 7.019066202818481E-4
Relative absolute error: 0.12191743356364841
Root relative squared error: 0.14038132405636963
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 793.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 6.09587167818242E-4
Coverage of cases: 100.0
Instances selection time: 792.0
Test time: 1023.0
Accumulative iteration time: 6954.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 10
Labeled set size: 992
Unlabelled set size: 3070
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 6.831378663604578E-4
Relative absolute error: 0.11853021461132743
Root relative squared error: 0.13662757327209155
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 792.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 5.926510730566372E-4
Coverage of cases: 100.0
Instances selection time: 792.0
Test time: 1030.0
Accumulative iteration time: 7746.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 11
Labeled set size: 1012
Unlabelled set size: 3050
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 6.666012082612959E-4
Relative absolute error: 0.11557956634642619
Root relative squared error: 0.13332024165225917
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 800.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 5.778978317321309E-4
Coverage of cases: 100.0
Instances selection time: 799.0
Test time: 1045.0
Accumulative iteration time: 8546.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 12
Labeled set size: 1032
Unlabelled set size: 3030
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 6.491193358340289E-4
Relative absolute error: 0.11239763032401265
Root relative squared error: 0.12982386716680577
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 805.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 5.619881516200632E-4
Coverage of cases: 100.0
Instances selection time: 804.0
Test time: 1056.0
Accumulative iteration time: 9351.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 13
Labeled set size: 1052
Unlabelled set size: 3010
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 6.3650820583258E-4
Relative absolute error: 0.11026467304940166
Root relative squared error: 0.127301641166516
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 813.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 5.513233652470082E-4
Coverage of cases: 100.0
Instances selection time: 812.0
Test time: 1071.0
Accumulative iteration time: 10164.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 14
Labeled set size: 1072
Unlabelled set size: 2990
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 6.215602242347683E-4
Relative absolute error: 0.10760770234142575
Root relative squared error: 0.12431204484695366
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 820.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 5.380385117071287E-4
Coverage of cases: 100.0
Instances selection time: 820.0
Test time: 1080.0
Accumulative iteration time: 10984.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 15
Labeled set size: 1092
Unlabelled set size: 2970
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 6.069646208783123E-4
Relative absolute error: 0.10504896891130498
Root relative squared error: 0.12139292417566246
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 831.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 5.252448445565249E-4
Coverage of cases: 100.0
Instances selection time: 831.0
Test time: 1100.0
Accumulative iteration time: 11815.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 16
Labeled set size: 1112
Unlabelled set size: 2950
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 5.937518694820987E-4
Relative absolute error: 0.10271444961870382
Root relative squared error: 0.11875037389641975
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 834.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 5.135722480935192E-4
Coverage of cases: 100.0
Instances selection time: 833.0
Test time: 1107.0
Accumulative iteration time: 12649.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 17
Labeled set size: 1132
Unlabelled set size: 2930
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 5.791944702293421E-4
Relative absolute error: 0.10012519806477875
Root relative squared error: 0.11583889404586842
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 833.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 5.006259903238937E-4
Coverage of cases: 100.0
Instances selection time: 832.0
Test time: 1121.0
Accumulative iteration time: 13482.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 18
Labeled set size: 1152
Unlabelled set size: 2910
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 5.648529836002599E-4
Relative absolute error: 0.09750235945438637
Root relative squared error: 0.11297059672005197
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 840.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.8751179727193184E-4
Coverage of cases: 100.0
Instances selection time: 840.0
Test time: 1133.0
Accumulative iteration time: 14322.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 19
Labeled set size: 1172
Unlabelled set size: 2890
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 5.522769115781299E-4
Relative absolute error: 0.09519992636636092
Root relative squared error: 0.11045538231562599
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 843.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.759996318318046E-4
Coverage of cases: 100.0
Instances selection time: 843.0
Test time: 1144.0
Accumulative iteration time: 15165.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 20
Labeled set size: 1192
Unlabelled set size: 2870
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 5.425090971876836E-4
Relative absolute error: 0.09355596726072675
Root relative squared error: 0.10850181943753673
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 847.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.6777983630363375E-4
Coverage of cases: 100.0
Instances selection time: 846.0
Test time: 1155.0
Accumulative iteration time: 16012.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 21
Labeled set size: 1212
Unlabelled set size: 2850
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 5.313374878845428E-4
Relative absolute error: 0.09153385390744859
Root relative squared error: 0.10626749757690855
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 848.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.576692695372429E-4
Coverage of cases: 100.0
Instances selection time: 847.0
Test time: 1171.0
Accumulative iteration time: 16860.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 22
Labeled set size: 1232
Unlabelled set size: 2830
	
Mean region size: 50.01230920728705
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.001820724676295863
Relative absolute error: 0.09525491465020962
Root relative squared error: 0.3641449352591726
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 853.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.762745732510481E-4
Coverage of cases: 100.0
Instances selection time: 852.0
Test time: 1178.0
Accumulative iteration time: 17713.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 23
Labeled set size: 1252
Unlabelled set size: 2810
	
Mean region size: 50.01230920728705
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0018173649842128357
Relative absolute error: 0.09312732305157866
Root relative squared error: 0.3634729968425671
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 857.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.656366152578933E-4
Coverage of cases: 100.0
Instances selection time: 856.0
Test time: 1195.0
Accumulative iteration time: 18570.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 24
Labeled set size: 1272
Unlabelled set size: 2790
	
Mean region size: 50.01230920728705
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.001814204769662617
Relative absolute error: 0.09108081548444699
Root relative squared error: 0.3628409539325234
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 861.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.55404077422235E-4
Coverage of cases: 100.0
Instances selection time: 860.0
Test time: 1209.0
Accumulative iteration time: 19431.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 25
Labeled set size: 1292
Unlabelled set size: 2770
	
Mean region size: 50.01230920728705
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0018116117790517832
Relative absolute error: 0.0893202840208086
Root relative squared error: 0.36232235581035666
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 866.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.4660142010404305E-4
Coverage of cases: 100.0
Instances selection time: 865.0
Test time: 1225.0
Accumulative iteration time: 20297.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 26
Labeled set size: 1312
Unlabelled set size: 2750
	
Mean region size: 50.01230920728705
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0018088436290838973
Relative absolute error: 0.08744679382459676
Root relative squared error: 0.36176872581677944
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 878.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.3723396912298383E-4
Coverage of cases: 100.0
Instances selection time: 877.0
Test time: 1236.0
Accumulative iteration time: 21175.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 27
Labeled set size: 1332
Unlabelled set size: 2730
	
Mean region size: 50.01230920728705
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.001806520637434257
Relative absolute error: 0.08589128252721681
Root relative squared error: 0.36130412748685137
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 869.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.2945641263608404E-4
Coverage of cases: 100.0
Instances selection time: 868.0
Test time: 1238.0
Accumulative iteration time: 22044.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 28
Labeled set size: 1352
Unlabelled set size: 2710
	
Mean region size: 50.01230920728705
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0018044012476174296
Relative absolute error: 0.08446012222517037
Root relative squared error: 0.3608802495234859
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 889.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.223006111258518E-4
Coverage of cases: 100.0
Instances selection time: 888.0
Test time: 1258.0
Accumulative iteration time: 22933.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 29
Labeled set size: 1372
Unlabelled set size: 2690
	
Mean region size: 50.01230920728705
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0031712990314433655
Relative absolute error: 0.08735031267034715
Root relative squared error: 0.6342598062886731
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 885.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.3675156335173574E-4
Coverage of cases: 100.0
Instances selection time: 884.0
Test time: 1266.0
Accumulative iteration time: 23818.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 30
Labeled set size: 1392
Unlabelled set size: 2670
	
Mean region size: 50.01230920728705
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.00317001902994056
Relative absolute error: 0.08570048261374803
Root relative squared error: 0.634003805988112
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 883.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.2850241306874017E-4
Coverage of cases: 100.0
Instances selection time: 882.0
Test time: 1278.0
Accumulative iteration time: 24701.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 31
Labeled set size: 1412
Unlabelled set size: 2650
	
Mean region size: 50.01230920728705
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.003168888011806499
Relative absolute error: 0.08428236893488084
Root relative squared error: 0.6337776023612998
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 887.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.2141184467440416E-4
Coverage of cases: 100.0
Instances selection time: 887.0
Test time: 1288.0
Accumulative iteration time: 25588.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 32
Labeled set size: 1432
Unlabelled set size: 2630
	
Mean region size: 50.01230920728705
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0031676432331170365
Relative absolute error: 0.08262634665871038
Root relative squared error: 0.6335286466234072
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 888.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.131317332935519E-4
Coverage of cases: 100.0
Instances selection time: 887.0
Test time: 1300.0
Accumulative iteration time: 26476.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 33
Labeled set size: 1452
Unlabelled set size: 2610
	
Mean region size: 50.01230920728705
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.003166607115913472
Relative absolute error: 0.08119694672755477
Root relative squared error: 0.6333214231826945
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 892.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.0598473363777387E-4
Coverage of cases: 100.0
Instances selection time: 891.0
Test time: 1315.0
Accumulative iteration time: 27368.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 34
Labeled set size: 1472
Unlabelled set size: 2590
	
Mean region size: 50.01230920728705
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0031656342594475854
Relative absolute error: 0.0798641088858492
Root relative squared error: 0.6331268518895171
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 892.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.9932054442924603E-4
Coverage of cases: 100.0
Instances selection time: 891.0
Test time: 1325.0
Accumulative iteration time: 28260.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 35
Labeled set size: 1492
Unlabelled set size: 2570
	
Mean region size: 50.01230920728705
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0031644958868939106
Relative absolute error: 0.07827139626599058
Root relative squared error: 0.6328991773787821
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 907.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.9135698132995294E-4
Coverage of cases: 100.0
Instances selection time: 907.0
Test time: 1342.0
Accumulative iteration time: 29167.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 36
Labeled set size: 1512
Unlabelled set size: 2550
	
Mean region size: 50.01230920728705
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.003163484124056995
Relative absolute error: 0.07680543549980699
Root relative squared error: 0.632696824811399
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 903.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.8402717749903495E-4
Coverage of cases: 100.0
Instances selection time: 902.0
Test time: 1343.0
Accumulative iteration time: 30070.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 37
Labeled set size: 1532
Unlabelled set size: 2530
	
Mean region size: 50.01230920728705
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0031625857958990722
Relative absolute error: 0.07548679857528887
Root relative squared error: 0.6325171591798144
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 904.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.7743399287644435E-4
Coverage of cases: 100.0
Instances selection time: 903.0
Test time: 1356.0
Accumulative iteration time: 30974.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 38
Labeled set size: 1552
Unlabelled set size: 2510
	
Mean region size: 50.01230920728705
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0031617518663776913
Relative absolute error: 0.07424106151157474
Root relative squared error: 0.6323503732755382
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 904.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.712053075578737E-4
Coverage of cases: 100.0
Instances selection time: 904.0
Test time: 1377.0
Accumulative iteration time: 31878.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 39
Labeled set size: 1572
Unlabelled set size: 2490
	
Mean region size: 50.01230920728705
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0031609875595267132
Relative absolute error: 0.07307800800751826
Root relative squared error: 0.6321975119053427
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 905.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.6539004003759135E-4
Coverage of cases: 100.0
Instances selection time: 904.0
Test time: 1383.0
Accumulative iteration time: 32783.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 40
Labeled set size: 1592
Unlabelled set size: 2470
	
Mean region size: 50.01230920728705
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0031602290286822116
Relative absolute error: 0.07191135043515186
Root relative squared error: 0.6320458057364423
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 906.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.595567521757593E-4
Coverage of cases: 100.0
Instances selection time: 905.0
Test time: 1394.0
Accumulative iteration time: 33689.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 41
Labeled set size: 1612
Unlabelled set size: 2450
	
Mean region size: 50.01230920728705
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.003159477973614911
Relative absolute error: 0.07077454385135463
Root relative squared error: 0.6318955947229822
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 908.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.5387271925677315E-4
Coverage of cases: 100.0
Instances selection time: 908.0
Test time: 1404.0
Accumulative iteration time: 34597.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 42
Labeled set size: 1632
Unlabelled set size: 2430
	
Mean region size: 50.01230920728705
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0031588029688218154
Relative absolute error: 0.06969242416520523
Root relative squared error: 0.6317605937643631
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 912.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.484621208260261E-4
Coverage of cases: 100.0
Instances selection time: 911.0
Test time: 1418.0
Accumulative iteration time: 35509.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 43
Labeled set size: 1652
Unlabelled set size: 2410
	
Mean region size: 50.01230920728705
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0031581320400887636
Relative absolute error: 0.06856936654603596
Root relative squared error: 0.6316264080177527
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 912.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.428468327301798E-4
Coverage of cases: 100.0
Instances selection time: 911.0
Test time: 1517.0
Accumulative iteration time: 36421.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 44
Labeled set size: 1672
Unlabelled set size: 2390
	
Mean region size: 50.01230920728705
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0031574321331278004
Relative absolute error: 0.06744723171003639
Root relative squared error: 0.6314864266255601
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1133.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.372361585501819E-4
Coverage of cases: 100.0
Instances selection time: 1133.0
Test time: 1455.0
Accumulative iteration time: 37554.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 45
Labeled set size: 1692
Unlabelled set size: 2370
	
Mean region size: 50.01230920728705
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0031568836291771952
Relative absolute error: 0.06652969396423673
Root relative squared error: 0.631376725835439
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 915.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.3264846982118364E-4
Coverage of cases: 100.0
Instances selection time: 914.0
Test time: 1461.0
Accumulative iteration time: 38469.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 46
Labeled set size: 1712
Unlabelled set size: 2350
	
Mean region size: 50.01230920728705
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0031563133740918022
Relative absolute error: 0.06557011033081164
Root relative squared error: 0.6312626748183604
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 923.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.2785055165405824E-4
Coverage of cases: 100.0
Instances selection time: 923.0
Test time: 1481.0
Accumulative iteration time: 39392.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Time end:Wed Nov 01 07.12.07 EET 2017