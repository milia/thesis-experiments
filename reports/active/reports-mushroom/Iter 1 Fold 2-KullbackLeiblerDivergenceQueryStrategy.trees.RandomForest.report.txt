Wed Nov 01 06.05.15 EET 2017
Dataset: mushroom
Test set size: 4062
Initial Labelled set size: 812
Initial Unlabelled set size: 3250
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Wed Nov 01 06.05.15 EET 2017
		
Iteration: 1
Labeled set size: 812
Unlabelled set size: 3250
	
Mean region size: 54.65288035450517
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.047281580536822944
Relative absolute error: 2.64780762660563
Root relative squared error: 9.45631610736459
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 23.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.013239038133028149
Coverage of cases: 100.0
Instances selection time: 13.0
Test time: 14.0
Accumulative iteration time: 23.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 10.0
		
Iteration: 2
Labeled set size: 832
Unlabelled set size: 3230
	
Mean region size: 53.274249138355486
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.03873418210597913
Relative absolute error: 1.8396481692903963
Root relative squared error: 7.746836421195826
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 20.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.009198240846451981
Coverage of cases: 100.0
Instances selection time: 11.0
Test time: 14.0
Accumulative iteration time: 43.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 9.0
		
Iteration: 3
Labeled set size: 852
Unlabelled set size: 3210
	
Mean region size: 54.2712949286066
Incorrectly Classified Instances: 0.049236829148202856
Correctly Classified Instances: 99.9507631708518
Weighted Precision: 0.999508134125142
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.04680246324415149
Relative absolute error: 2.4541892106685617
Root relative squared error: 9.360492648830299
Weighted TruePositiveRate: 0.999507631708518
Weighted MatthewsCorrelation: 0.9990145101369982
Weighted FMeasure: 0.9995076403134782
Iteration time: 21.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.012270946053342809
Coverage of cases: 100.0
Instances selection time: 11.0
Test time: 13.0
Accumulative iteration time: 64.0
Weighted Recall: 0.999507631708518
Weighted FalsePositiveRate: 4.5820205072329465E-4
Kappa statistic: 0.9990140245420991
Training time: 10.0
		
Iteration: 4
Labeled set size: 872
Unlabelled set size: 3190
	
Mean region size: 54.763663220088624
Incorrectly Classified Instances: 0.12309207287050714
Correctly Classified Instances: 99.8769079271295
Weighted Precision: 0.9987722145762585
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.04879760352842958
Relative absolute error: 2.6404591666146624
Root relative squared error: 9.759520705685915
Weighted TruePositiveRate: 0.9987690792712949
Weighted MatthewsCorrelation: 0.9975382225449091
Weighted FMeasure: 0.9987691319291265
Iteration time: 22.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.013202295833073311
Coverage of cases: 100.0
Instances selection time: 12.0
Test time: 14.0
Accumulative iteration time: 86.0
Weighted Recall: 0.9987690792712949
Weighted FalsePositiveRate: 0.0011455051268082367
Kappa statistic: 0.9975351923799943
Training time: 10.0
		
Iteration: 5
Labeled set size: 892
Unlabelled set size: 3170
	
Mean region size: 52.38798621368784
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.03755351219982837
Relative absolute error: 1.40830920141771
Root relative squared error: 7.510702439965675
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 19.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.00704154600708855
Coverage of cases: 100.0
Instances selection time: 10.0
Test time: 14.0
Accumulative iteration time: 105.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 9.0
		
Iteration: 6
Labeled set size: 912
Unlabelled set size: 3150
	
Mean region size: 53.655834564254064
Incorrectly Classified Instances: 0.049236829148202856
Correctly Classified Instances: 99.9507631708518
Weighted Precision: 0.9995080992947397
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.03955839306070143
Relative absolute error: 1.9652523515909033
Root relative squared error: 7.911678612140285
Weighted TruePositiveRate: 0.999507631708518
Weighted MatthewsCorrelation: 0.999014440317274
Weighted FMeasure: 0.9995076228639044
Iteration time: 21.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.009826261757954516
Coverage of cases: 100.0
Instances selection time: 11.0
Test time: 14.0
Accumulative iteration time: 126.0
Weighted Recall: 0.999507631708518
Weighted FalsePositiveRate: 5.290821681706783E-4
Kappa statistic: 0.999013954653566
Training time: 10.0
		
Iteration: 7
Labeled set size: 932
Unlabelled set size: 3130
	
Mean region size: 53.84047267355982
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.047134492490564256
Relative absolute error: 2.405217132320195
Root relative squared error: 9.426898498112852
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 22.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.012026085661600976
Coverage of cases: 100.0
Instances selection time: 12.0
Test time: 14.0
Accumulative iteration time: 148.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 10.0
		
Iteration: 8
Labeled set size: 952
Unlabelled set size: 3110
	
Mean region size: 54.99753815854259
Incorrectly Classified Instances: 0.19694731659281142
Correctly Classified Instances: 99.80305268340719
Weighted Precision: 0.9980323217709078
Weighted AreaUnderROC: 0.9999936887566657
Root mean squared error: 0.06315747162976829
Relative absolute error: 3.512749880122141
Root relative squared error: 12.631494325953657
Weighted TruePositiveRate: 0.9980305268340719
Weighted MatthewsCorrelation: 0.9960576158661245
Weighted FMeasure: 0.9980304551183937
Iteration time: 21.0
Weighted AreaUnderPRC: 0.999993702918774
Mean absolute error: 0.017563749400610704
Coverage of cases: 100.0
Instances selection time: 11.0
Test time: 13.0
Accumulative iteration time: 169.0
Weighted Recall: 0.9980305268340719
Weighted FalsePositiveRate: 0.0020454485552353296
Kappa statistic: 0.9960556788223356
Training time: 10.0
		
Iteration: 9
Labeled set size: 972
Unlabelled set size: 3090
	
Mean region size: 53.93894633185623
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.034076992704217615
Relative absolute error: 1.8970349726012798
Root relative squared error: 6.815398540843523
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 23.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0094851748630064
Coverage of cases: 100.0
Instances selection time: 12.0
Test time: 14.0
Accumulative iteration time: 192.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 11.0
		
Iteration: 10
Labeled set size: 992
Unlabelled set size: 3070
	
Mean region size: 52.75726243229936
Incorrectly Classified Instances: 0.09847365829640571
Correctly Classified Instances: 99.9015263417036
Weighted Precision: 0.9990172710349421
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.04509476123777207
Relative absolute error: 1.9191032348182004
Root relative squared error: 9.018952247554415
Weighted TruePositiveRate: 0.999015263417036
Weighted MatthewsCorrelation: 0.9980300592947201
Weighted FMeasure: 0.9990152973576452
Iteration time: 24.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.009595516174091002
Coverage of cases: 100.0
Instances selection time: 10.0
Test time: 15.0
Accumulative iteration time: 216.0
Weighted Recall: 0.999015263417036
Weighted FalsePositiveRate: 9.164041014465893E-4
Kappa statistic: 0.9980281189653013
Training time: 14.0
		
Iteration: 11
Labeled set size: 1012
Unlabelled set size: 3050
	
Mean region size: 54.037419990152635
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992625774237212
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.044272966202878586
Relative absolute error: 2.290349898273207
Root relative squared error: 8.854593240575717
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985221550307092
Weighted FMeasure: 0.9992614667891501
Iteration time: 23.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.011451749491366034
Coverage of cases: 100.0
Instances selection time: 11.0
Test time: 13.0
Accumulative iteration time: 239.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 6.873030760849421E-4
Kappa statistic: 0.9985210630190265
Training time: 12.0
		
Iteration: 12
Labeled set size: 1032
Unlabelled set size: 3030
	
Mean region size: 55.05908419497784
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.04820407027866628
Relative absolute error: 2.842367593950489
Root relative squared error: 9.640814055733255
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 23.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.014211837969752446
Coverage of cases: 100.0
Instances selection time: 11.0
Test time: 15.0
Accumulative iteration time: 262.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 12.0
		
Iteration: 13
Labeled set size: 1052
Unlabelled set size: 3010
	
Mean region size: 53.11422944362383
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.034211385471127495
Relative absolute error: 1.6659782010443307
Root relative squared error: 6.842277094225499
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 36.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.008329891005221654
Coverage of cases: 100.0
Instances selection time: 20.0
Test time: 14.0
Accumulative iteration time: 298.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 16.0
		
Iteration: 14
Labeled set size: 1072
Unlabelled set size: 2990
	
Mean region size: 53.23732151649434
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.03836730658954507
Relative absolute error: 1.8156736688630648
Root relative squared error: 7.673461317909014
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 22.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.009078368344315324
Coverage of cases: 100.0
Instances selection time: 10.0
Test time: 14.0
Accumulative iteration time: 320.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 12.0
		
Iteration: 15
Labeled set size: 1092
Unlabelled set size: 2970
	
Mean region size: 53.31117676021664
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992625774237212
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.03925247582839908
Relative absolute error: 1.7674277472140787
Root relative squared error: 7.850495165679816
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985221550307092
Weighted FMeasure: 0.9992614667891501
Iteration time: 23.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.008837138736070393
Coverage of cases: 100.0
Instances selection time: 11.0
Test time: 14.0
Accumulative iteration time: 343.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 6.873030760849421E-4
Kappa statistic: 0.9985210630190265
Training time: 12.0
		
Iteration: 16
Labeled set size: 1112
Unlabelled set size: 2950
	
Mean region size: 53.95125553914328
Incorrectly Classified Instances: 0.14771048744460857
Correctly Classified Instances: 99.85228951255539
Weighted Precision: 0.9985274076659035
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.04788992289649297
Relative absolute error: 2.4066016106973556
Root relative squared error: 9.577984579298594
Weighted TruePositiveRate: 0.9985228951255539
Weighted MatthewsCorrelation: 0.9970466443977584
Weighted FMeasure: 0.9985229704137654
Iteration time: 24.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.012033008053486777
Coverage of cases: 100.0
Instances selection time: 11.0
Test time: 16.0
Accumulative iteration time: 367.0
Weighted Recall: 0.9985228951255539
Weighted FalsePositiveRate: 0.0013746061521698841
Kappa statistic: 0.9970422832621776
Training time: 13.0
		
Iteration: 17
Labeled set size: 1132
Unlabelled set size: 2930
	
Mean region size: 52.27720334810438
Incorrectly Classified Instances: 0.09847365829640571
Correctly Classified Instances: 99.9015263417036
Weighted Precision: 0.9990172710349421
Weighted AreaUnderROC: 0.9999995145197434
Root mean squared error: 0.04498725570374326
Relative absolute error: 1.7206829859924173
Root relative squared error: 8.997451140748652
Weighted TruePositiveRate: 0.999015263417036
Weighted MatthewsCorrelation: 0.9980300592947201
Weighted FMeasure: 0.9990152973576452
Iteration time: 24.0
Weighted AreaUnderPRC: 0.9999995148875116
Mean absolute error: 0.008603414929962087
Coverage of cases: 100.0
Instances selection time: 11.0
Test time: 14.0
Accumulative iteration time: 391.0
Weighted Recall: 0.999015263417036
Weighted FalsePositiveRate: 9.164041014465893E-4
Kappa statistic: 0.9980281189653013
Training time: 13.0
		
Iteration: 18
Labeled set size: 1152
Unlabelled set size: 2910
	
Mean region size: 53.29886755292959
Incorrectly Classified Instances: 0.049236829148202856
Correctly Classified Instances: 99.9507631708518
Weighted Precision: 0.9995080992947397
Weighted AreaUnderROC: 0.9999975725987176
Root mean squared error: 0.04241115113156872
Relative absolute error: 1.8429334090964724
Root relative squared error: 8.482230226313744
Weighted TruePositiveRate: 0.999507631708518
Weighted MatthewsCorrelation: 0.999014440317274
Weighted FMeasure: 0.9995076228639044
Iteration time: 23.0
Weighted AreaUnderPRC: 0.9999975765928573
Mean absolute error: 0.009214667045482362
Coverage of cases: 100.0
Instances selection time: 11.0
Test time: 14.0
Accumulative iteration time: 414.0
Weighted Recall: 0.999507631708518
Weighted FalsePositiveRate: 5.290821681706783E-4
Kappa statistic: 0.999013954653566
Training time: 12.0
		
Iteration: 19
Labeled set size: 1172
Unlabelled set size: 2890
	
Mean region size: 53.01575578532743
Incorrectly Classified Instances: 0.17232890201870998
Correctly Classified Instances: 99.8276710979813
Weighted Precision: 0.9982828499228874
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.04356522956821304
Relative absolute error: 1.7618779727914264
Root relative squared error: 8.713045913642608
Weighted TruePositiveRate: 0.9982767109798129
Weighted MatthewsCorrelation: 0.9965553244703538
Weighted FMeasure: 0.9982768127217427
Iteration time: 23.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.008809389863957132
Coverage of cases: 100.0
Instances selection time: 11.0
Test time: 13.0
Accumulative iteration time: 437.0
Weighted Recall: 0.9982767109798129
Weighted FalsePositiveRate: 0.0016037071775315311
Kappa statistic: 0.9965493916109222
Training time: 12.0
		
Iteration: 20
Labeled set size: 1192
Unlabelled set size: 2870
	
Mean region size: 53.24963072378139
Incorrectly Classified Instances: 0.12309207287050714
Correctly Classified Instances: 99.8769079271295
Weighted Precision: 0.9987692221494877
Weighted AreaUnderROC: 0.9999947810872427
Root mean squared error: 0.048762415814741174
Relative absolute error: 2.1548597430232426
Root relative squared error: 9.752483162948234
Weighted TruePositiveRate: 0.9987690792712949
Weighted MatthewsCorrelation: 0.9975351389004643
Weighted FMeasure: 0.9987690901023809
Iteration time: 22.0
Weighted AreaUnderPRC: 0.9999946693394061
Mean absolute error: 0.010774298715116213
Coverage of cases: 100.0
Instances selection time: 10.0
Test time: 13.0
Accumulative iteration time: 459.0
Weighted Recall: 0.9987690792712949
Weighted FalsePositiveRate: 0.0012163852442556202
Kappa statistic: 0.9975350176772365
Training time: 12.0
		
Iteration: 21
Labeled set size: 1212
Unlabelled set size: 2850
	
Mean region size: 54.66518956179222
Incorrectly Classified Instances: 0.09847365829640571
Correctly Classified Instances: 99.9015263417036
Weighted Precision: 0.9990172710349421
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.044870942851031936
Relative absolute error: 2.437762825142834
Root relative squared error: 8.974188570206387
Weighted TruePositiveRate: 0.999015263417036
Weighted MatthewsCorrelation: 0.9980300592947201
Weighted FMeasure: 0.9990152973576452
Iteration time: 23.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.01218881412571417
Coverage of cases: 100.0
Instances selection time: 11.0
Test time: 14.0
Accumulative iteration time: 482.0
Weighted Recall: 0.999015263417036
Weighted FalsePositiveRate: 9.164041014465893E-4
Kappa statistic: 0.9980281189653013
Training time: 12.0
		
Iteration: 22
Labeled set size: 1232
Unlabelled set size: 2830
	
Mean region size: 52.80649926144756
Incorrectly Classified Instances: 0.024618414574101428
Correctly Classified Instances: 99.9753815854259
Weighted Precision: 0.9997539415225314
Weighted AreaUnderROC: 0.9999995145197434
Root mean squared error: 0.04139902375852218
Relative absolute error: 1.7179825956952106
Root relative squared error: 8.279804751704436
Weighted TruePositiveRate: 0.9997538158542589
Weighted MatthewsCorrelation: 0.9995071249983162
Weighted FMeasure: 0.9997538180204761
Iteration time: 22.0
Weighted AreaUnderPRC: 0.9999995148061623
Mean absolute error: 0.008589912978476054
Coverage of cases: 100.0
Instances selection time: 10.0
Test time: 13.0
Accumulative iteration time: 504.0
Weighted Recall: 0.9997538158542589
Weighted FalsePositiveRate: 2.2910102536164733E-4
Kappa statistic: 0.9995070035354472
Training time: 12.0
		
Iteration: 23
Labeled set size: 1252
Unlabelled set size: 2810
	
Mean region size: 53.37272279665189
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.03267969996529906
Relative absolute error: 1.6869691864356522
Root relative squared error: 6.535939993059812
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 23.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.008434845932178262
Coverage of cases: 100.0
Instances selection time: 10.0
Test time: 13.0
Accumulative iteration time: 527.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 13.0
		
Iteration: 24
Labeled set size: 1272
Unlabelled set size: 2790
	
Mean region size: 53.4219596258001
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.03396528421978806
Relative absolute error: 1.7134930580719663
Root relative squared error: 6.793056843957612
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 22.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.008567465290359831
Coverage of cases: 100.0
Instances selection time: 9.0
Test time: 13.0
Accumulative iteration time: 549.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 13.0
		
Iteration: 25
Labeled set size: 1292
Unlabelled set size: 2770
	
Mean region size: 54.849827671097984
Incorrectly Classified Instances: 0.049236829148202856
Correctly Classified Instances: 99.9507631708518
Weighted Precision: 0.9995080992947397
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.04602491511785587
Relative absolute error: 2.62708167545944
Root relative squared error: 9.204983023571174
Weighted TruePositiveRate: 0.999507631708518
Weighted MatthewsCorrelation: 0.999014440317274
Weighted FMeasure: 0.9995076228639044
Iteration time: 23.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0131354083772972
Coverage of cases: 100.0
Instances selection time: 10.0
Test time: 14.0
Accumulative iteration time: 572.0
Weighted Recall: 0.999507631708518
Weighted FalsePositiveRate: 5.290821681706783E-4
Kappa statistic: 0.999013954653566
Training time: 13.0
		
Iteration: 26
Labeled set size: 1312
Unlabelled set size: 2750
	
Mean region size: 53.274249138355486
Incorrectly Classified Instances: 0.17232890201870998
Correctly Classified Instances: 99.8276710979813
Weighted Precision: 0.9982778910328981
Weighted AreaUnderROC: 0.9999970871184611
Root mean squared error: 0.0441208303589361
Relative absolute error: 1.9710212909074636
Root relative squared error: 8.824166071787221
Weighted TruePositiveRate: 0.9982767109798129
Weighted MatthewsCorrelation: 0.9965502368995333
Weighted FMeasure: 0.99827675584135
Iteration time: 23.0
Weighted AreaUnderPRC: 0.9999970913621667
Mean absolute error: 0.009855106454537318
Coverage of cases: 100.0
Instances selection time: 10.0
Test time: 14.0
Accumulative iteration time: 595.0
Weighted Recall: 0.9982767109798129
Weighted FalsePositiveRate: 0.001674587294978915
Kappa statistic: 0.9965491470443955
Training time: 13.0
		
Iteration: 27
Labeled set size: 1332
Unlabelled set size: 2730
	
Mean region size: 52.58493353028065
Incorrectly Classified Instances: 0.09847365829640571
Correctly Classified Instances: 99.9015263417036
Weighted Precision: 0.999015263417036
Weighted AreaUnderROC: 0.9999941742369222
Root mean squared error: 0.04029623731946494
Relative absolute error: 1.6314276860688353
Root relative squared error: 8.059247463892987
Weighted TruePositiveRate: 0.999015263417036
Weighted MatthewsCorrelation: 0.998027979198142
Weighted FMeasure: 0.999015263417036
Iteration time: 24.0
Weighted AreaUnderPRC: 0.9999941875046137
Mean absolute error: 0.008157138430344177
Coverage of cases: 100.0
Instances selection time: 10.0
Test time: 15.0
Accumulative iteration time: 619.0
Weighted Recall: 0.999015263417036
Weighted FalsePositiveRate: 9.87284218893973E-4
Kappa statistic: 0.998027979198142
Training time: 14.0
		
Iteration: 28
Labeled set size: 1352
Unlabelled set size: 2710
	
Mean region size: 53.04037419990153
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.03300835321884907
Relative absolute error: 1.5717900990921778
Root relative squared error: 6.601670643769814
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 24.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.00785895049546089
Coverage of cases: 100.0
Instances selection time: 10.0
Test time: 13.0
Accumulative iteration time: 643.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 14.0
		
Iteration: 29
Labeled set size: 1372
Unlabelled set size: 2690
	
Mean region size: 53.79123584441162
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.04029146578122141
Relative absolute error: 2.004622136254744
Root relative squared error: 8.058293156244282
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 24.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.01002311068127372
Coverage of cases: 100.0
Instances selection time: 10.0
Test time: 14.0
Accumulative iteration time: 667.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 14.0
		
Iteration: 30
Labeled set size: 1392
Unlabelled set size: 2670
	
Mean region size: 53.655834564254064
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0396844133018512
Relative absolute error: 2.0358723251418036
Root relative squared error: 7.93688266037024
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 24.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.010179361625709019
Coverage of cases: 100.0
Instances selection time: 10.0
Test time: 15.0
Accumulative iteration time: 691.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 14.0
		
Iteration: 31
Labeled set size: 1412
Unlabelled set size: 2650
	
Mean region size: 55.637616937469225
Incorrectly Classified Instances: 0.12309207287050714
Correctly Classified Instances: 99.8769079271295
Weighted Precision: 0.9987722145762585
Weighted AreaUnderROC: 0.9999980580789739
Root mean squared error: 0.056578052082020575
Relative absolute error: 3.328941115853086
Root relative squared error: 11.315610416404114
Weighted TruePositiveRate: 0.9987690792712949
Weighted MatthewsCorrelation: 0.9975382225449091
Weighted FMeasure: 0.9987691319291265
Iteration time: 24.0
Weighted AreaUnderPRC: 0.9999980602070154
Mean absolute error: 0.01664470557926543
Coverage of cases: 100.0
Instances selection time: 9.0
Test time: 14.0
Accumulative iteration time: 715.0
Weighted Recall: 0.9987690792712949
Weighted FalsePositiveRate: 0.0011455051268082367
Kappa statistic: 0.9975351923799943
Training time: 15.0
		
Iteration: 32
Labeled set size: 1432
Unlabelled set size: 2630
	
Mean region size: 53.48350566223535
Incorrectly Classified Instances: 0.024618414574101428
Correctly Classified Instances: 99.9753815854259
Weighted Precision: 0.9997539328063473
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0407678082039821
Relative absolute error: 1.9709815040577074
Root relative squared error: 8.15356164079642
Weighted TruePositiveRate: 0.9997538158542589
Weighted MatthewsCorrelation: 0.9995071075347904
Weighted FMeasure: 0.9997538136580851
Iteration time: 41.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.009854907520288538
Coverage of cases: 100.0
Instances selection time: 18.0
Test time: 15.0
Accumulative iteration time: 756.0
Weighted Recall: 0.9997538158542589
Weighted FalsePositiveRate: 2.6454108408533914E-4
Kappa statistic: 0.9995069860633139
Training time: 23.0
		
Iteration: 33
Labeled set size: 1452
Unlabelled set size: 2610
	
Mean region size: 55.145248645987195
Incorrectly Classified Instances: 0.24618414574101427
Correctly Classified Instances: 99.75381585425899
Weighted Precision: 0.9975506678995888
Weighted AreaUnderROC: 0.9999985435592307
Root mean squared error: 0.05562717113185338
Relative absolute error: 3.08907161109758
Root relative squared error: 11.125434226370675
Weighted TruePositiveRate: 0.9975381585425899
Weighted MatthewsCorrelation: 0.9950829101864973
Weighted FMeasure: 0.9975383616876471
Iteration time: 26.0
Weighted AreaUnderPRC: 0.9999985449437823
Mean absolute error: 0.0154453580554879
Coverage of cases: 100.0
Instances selection time: 10.0
Test time: 19.0
Accumulative iteration time: 782.0
Weighted Recall: 0.9975381585425899
Weighted FalsePositiveRate: 0.0022910102536164733
Kappa statistic: 0.9950708214472418
Training time: 16.0
		
Iteration: 34
Labeled set size: 1472
Unlabelled set size: 2590
	
Mean region size: 53.58197932053176
Incorrectly Classified Instances: 0.17232890201870998
Correctly Classified Instances: 99.8276710979813
Weighted Precision: 0.9982828499228874
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.05074645131120508
Relative absolute error: 2.2485822657870855
Root relative squared error: 10.149290262241015
Weighted TruePositiveRate: 0.9982767109798129
Weighted MatthewsCorrelation: 0.9965553244703538
Weighted FMeasure: 0.9982768127217427
Iteration time: 25.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.011242911328935427
Coverage of cases: 100.0
Instances selection time: 9.0
Test time: 14.0
Accumulative iteration time: 807.0
Weighted Recall: 0.9982767109798129
Weighted FalsePositiveRate: 0.0016037071775315311
Kappa statistic: 0.9965493916109222
Training time: 16.0
		
Iteration: 35
Labeled set size: 1492
Unlabelled set size: 2570
	
Mean region size: 54.50516986706056
Incorrectly Classified Instances: 0.049236829148202856
Correctly Classified Instances: 99.9507631708518
Weighted Precision: 0.999508134125142
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.04513860982524119
Relative absolute error: 2.3035067618787752
Root relative squared error: 9.027721965048238
Weighted TruePositiveRate: 0.999507631708518
Weighted MatthewsCorrelation: 0.9990145101369982
Weighted FMeasure: 0.9995076403134782
Iteration time: 25.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.011517533809393877
Coverage of cases: 100.0
Instances selection time: 9.0
Test time: 13.0
Accumulative iteration time: 832.0
Weighted Recall: 0.999507631708518
Weighted FalsePositiveRate: 4.5820205072329465E-4
Kappa statistic: 0.9990140245420991
Training time: 16.0
		
Iteration: 36
Labeled set size: 1512
Unlabelled set size: 2550
	
Mean region size: 54.36976858690301
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.05166136563186902
Relative absolute error: 2.778445016122995
Root relative squared error: 10.332273126373805
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 26.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.013892225080614974
Coverage of cases: 100.0
Instances selection time: 10.0
Test time: 14.0
Accumulative iteration time: 858.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 16.0
		
Iteration: 37
Labeled set size: 1532
Unlabelled set size: 2530
	
Mean region size: 53.06499261447563
Incorrectly Classified Instances: 0.12309207287050714
Correctly Classified Instances: 99.8769079271295
Weighted Precision: 0.9987722145762585
Weighted AreaUnderROC: 0.9999970871184611
Root mean squared error: 0.04458468969282797
Relative absolute error: 1.7862089521396896
Root relative squared error: 8.916937938565594
Weighted TruePositiveRate: 0.9987690792712949
Weighted MatthewsCorrelation: 0.9975382225449091
Weighted FMeasure: 0.9987691319291265
Iteration time: 24.0
Weighted AreaUnderPRC: 0.9999970912341212
Mean absolute error: 0.008931044760698448
Coverage of cases: 100.0
Instances selection time: 9.0
Test time: 14.0
Accumulative iteration time: 882.0
Weighted Recall: 0.9987690792712949
Weighted FalsePositiveRate: 0.0011455051268082367
Kappa statistic: 0.9975351923799943
Training time: 15.0
		
Iteration: 38
Labeled set size: 1552
Unlabelled set size: 2510
	
Mean region size: 55.477597242737566
Incorrectly Classified Instances: 0.17232890201870998
Correctly Classified Instances: 99.8276710979813
Weighted Precision: 0.9982828499228874
Weighted AreaUnderROC: 0.9999987862993587
Root mean squared error: 0.05638285179952375
Relative absolute error: 3.2063795521552967
Root relative squared error: 11.27657035990475
Weighted TruePositiveRate: 0.9982767109798129
Weighted MatthewsCorrelation: 0.9965553244703538
Weighted FMeasure: 0.9982768127217427
Iteration time: 25.0
Weighted AreaUnderPRC: 0.9999987872310386
Mean absolute error: 0.016031897760776483
Coverage of cases: 100.0
Instances selection time: 9.0
Test time: 14.0
Accumulative iteration time: 907.0
Weighted Recall: 0.9982767109798129
Weighted FalsePositiveRate: 0.0016037071775315311
Kappa statistic: 0.9965493916109222
Training time: 16.0
		
Iteration: 39
Labeled set size: 1572
Unlabelled set size: 2490
	
Mean region size: 53.20039389463319
Incorrectly Classified Instances: 0.049236829148202856
Correctly Classified Instances: 99.9507631708518
Weighted Precision: 0.999508134125142
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.041936342170162615
Relative absolute error: 1.8574165389666581
Root relative squared error: 8.387268434032523
Weighted TruePositiveRate: 0.999507631708518
Weighted MatthewsCorrelation: 0.9990145101369982
Weighted FMeasure: 0.9995076403134782
Iteration time: 25.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.00928708269483329
Coverage of cases: 100.0
Instances selection time: 9.0
Test time: 14.0
Accumulative iteration time: 932.0
Weighted Recall: 0.999507631708518
Weighted FalsePositiveRate: 4.5820205072329465E-4
Kappa statistic: 0.9990140245420991
Training time: 16.0
		
Iteration: 40
Labeled set size: 1592
Unlabelled set size: 2470
	
Mean region size: 53.95125553914328
Incorrectly Classified Instances: 0.17232890201870998
Correctly Classified Instances: 99.8276710979813
Weighted Precision: 0.9982828499228874
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.048354514753866894
Relative absolute error: 2.3398284119345503
Root relative squared error: 9.670902950773378
Weighted TruePositiveRate: 0.9982767109798129
Weighted MatthewsCorrelation: 0.9965553244703538
Weighted FMeasure: 0.9982768127217427
Iteration time: 27.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.01169914205967275
Coverage of cases: 100.0
Instances selection time: 10.0
Test time: 13.0
Accumulative iteration time: 959.0
Weighted Recall: 0.9982767109798129
Weighted FalsePositiveRate: 0.0016037071775315311
Kappa statistic: 0.9965493916109222
Training time: 17.0
		
Iteration: 41
Labeled set size: 1612
Unlabelled set size: 2450
	
Mean region size: 53.11422944362383
Incorrectly Classified Instances: 0.049236829148202856
Correctly Classified Instances: 99.9507631708518
Weighted Precision: 0.999508134125142
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.043569258545406546
Relative absolute error: 1.9140443299902543
Root relative squared error: 8.71385170908131
Weighted TruePositiveRate: 0.999507631708518
Weighted MatthewsCorrelation: 0.9990145101369982
Weighted FMeasure: 0.9995076403134782
Iteration time: 25.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.009570221649951272
Coverage of cases: 100.0
Instances selection time: 9.0
Test time: 13.0
Accumulative iteration time: 984.0
Weighted Recall: 0.999507631708518
Weighted FalsePositiveRate: 4.5820205072329465E-4
Kappa statistic: 0.9990140245420991
Training time: 16.0
		
Iteration: 42
Labeled set size: 1632
Unlabelled set size: 2430
	
Mean region size: 52.461841457410145
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.036563756915604026
Relative absolute error: 1.4202923294497878
Root relative squared error: 7.312751383120805
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 22.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.007101461647248939
Coverage of cases: 100.0
Instances selection time: 8.0
Test time: 13.0
Accumulative iteration time: 1006.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 14.0
		
Iteration: 43
Labeled set size: 1652
Unlabelled set size: 2410
	
Mean region size: 52.41260462826194
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.033720617148361394
Relative absolute error: 1.349857548070585
Root relative squared error: 6.744123429672279
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 25.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0067492877403529244
Coverage of cases: 100.0
Instances selection time: 9.0
Test time: 13.0
Accumulative iteration time: 1031.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 16.0
		
Iteration: 44
Labeled set size: 1672
Unlabelled set size: 2390
	
Mean region size: 55.145248645987195
Incorrectly Classified Instances: 0.22156573116691286
Correctly Classified Instances: 99.77843426883308
Weighted Precision: 0.9977944804187858
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.05300271842520258
Relative absolute error: 2.910612591347593
Root relative squared error: 10.600543685040515
Weighted TruePositiveRate: 0.9977843426883308
Weighted MatthewsCorrelation: 0.9955734577461355
Weighted FMeasure: 0.9977845084484809
Iteration time: 27.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.014553062956737965
Coverage of cases: 100.0
Instances selection time: 8.0
Test time: 14.0
Accumulative iteration time: 1058.0
Weighted Recall: 0.9977843426883308
Weighted FalsePositiveRate: 0.002061909228254826
Kappa statistic: 0.9955636607043825
Training time: 19.0
		
Iteration: 45
Labeled set size: 1692
Unlabelled set size: 2370
	
Mean region size: 51.994091580502214
Incorrectly Classified Instances: 0.049236829148202856
Correctly Classified Instances: 99.9507631708518
Weighted Precision: 0.9995080992947397
Weighted AreaUnderROC: 0.9999997572598719
Root mean squared error: 0.03517146468509352
Relative absolute error: 1.2602954528491397
Root relative squared error: 7.034292937018705
Weighted TruePositiveRate: 0.999507631708518
Weighted MatthewsCorrelation: 0.999014440317274
Weighted FMeasure: 0.9995076228639044
Iteration time: 23.0
Weighted AreaUnderPRC: 0.9999995148875116
Mean absolute error: 0.006301477264245698
Coverage of cases: 100.0
Instances selection time: 8.0
Test time: 13.0
Accumulative iteration time: 1081.0
Weighted Recall: 0.999507631708518
Weighted FalsePositiveRate: 5.290821681706783E-4
Kappa statistic: 0.999013954653566
Training time: 15.0
		
Iteration: 46
Labeled set size: 1712
Unlabelled set size: 2350
	
Mean region size: 54.65288035450517
Incorrectly Classified Instances: 0.09847365829640571
Correctly Classified Instances: 99.9015263417036
Weighted Precision: 0.9990172710349421
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.044928691463166595
Relative absolute error: 2.4588603378891785
Root relative squared error: 8.98573829263332
Weighted TruePositiveRate: 0.999015263417036
Weighted MatthewsCorrelation: 0.9980300592947201
Weighted FMeasure: 0.9990152973576452
Iteration time: 26.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.012294301689445893
Coverage of cases: 100.0
Instances selection time: 8.0
Test time: 14.0
Accumulative iteration time: 1107.0
Weighted Recall: 0.999015263417036
Weighted FalsePositiveRate: 9.164041014465893E-4
Kappa statistic: 0.9980281189653013
Training time: 18.0
		
Time end:Wed Nov 01 06.05.21 EET 2017