Wed Nov 01 05.25.43 EET 2017
Dataset: mushroom
Test set size: 4062
Initial Labelled set size: 812
Initial Unlabelled set size: 3250
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Nov 01 05.25.43 EET 2017
		
Iteration: 1
Labeled set size: 812
Unlabelled set size: 3250
	
Mean region size: 50.01230920728705
Incorrectly Classified Instances: 0.049236829148202856
Correctly Classified Instances: 99.9507631708518
Weighted Precision: 0.9995080992947397
Weighted AreaUnderROC: 0.9993571027703445
Root mean squared error: 0.022803162500599997
Relative absolute error: 0.26543985613108256
Root relative squared error: 4.56063250012
Weighted TruePositiveRate: 0.999507631708518
Weighted MatthewsCorrelation: 0.999014440317274
Weighted FMeasure: 0.9995076228639044
Iteration time: 704.0
Weighted AreaUnderPRC: 0.999286610010517
Mean absolute error: 0.0013271992806554128
Coverage of cases: 99.9507631708518
Instances selection time: 704.0
Test time: 873.0
Accumulative iteration time: 704.0
Weighted Recall: 0.999507631708518
Weighted FalsePositiveRate: 5.290821681706783E-4
Kappa statistic: 0.999013954653566
Training time: 0.0
		
Iteration: 2
Labeled set size: 832
Unlabelled set size: 3230
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 8.400359732353856E-4
Relative absolute error: 0.14663075200891587
Root relative squared error: 0.1680071946470771
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 711.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 7.331537600445794E-4
Coverage of cases: 100.0
Instances selection time: 711.0
Test time: 894.0
Accumulative iteration time: 1415.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 3
Labeled set size: 852
Unlabelled set size: 3210
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 8.182367537615851E-4
Relative absolute error: 0.14273531319282468
Root relative squared error: 0.163647350752317
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 720.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 7.136765659641233E-4
Coverage of cases: 100.0
Instances selection time: 720.0
Test time: 907.0
Accumulative iteration time: 2135.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 4
Labeled set size: 872
Unlabelled set size: 3190
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 8.00074137414038E-4
Relative absolute error: 0.13966593701171062
Root relative squared error: 0.1600148274828076
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 724.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 6.983296850585531E-4
Coverage of cases: 100.0
Instances selection time: 724.0
Test time: 924.0
Accumulative iteration time: 2859.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 5
Labeled set size: 892
Unlabelled set size: 3170
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 7.806612478160787E-4
Relative absolute error: 0.13625214997455173
Root relative squared error: 0.15613224956321575
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 736.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 6.812607498727587E-4
Coverage of cases: 100.0
Instances selection time: 736.0
Test time: 960.0
Accumulative iteration time: 3595.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 6
Labeled set size: 912
Unlabelled set size: 3150
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 7.583108358136469E-4
Relative absolute error: 0.13220171753374363
Root relative squared error: 0.15166216716272937
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 746.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 6.610085876687181E-4
Coverage of cases: 100.0
Instances selection time: 746.0
Test time: 960.0
Accumulative iteration time: 4341.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 7
Labeled set size: 932
Unlabelled set size: 3130
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 7.395610148598872E-4
Relative absolute error: 0.1288599126964802
Root relative squared error: 0.14791220297197744
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 749.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 6.44299563482401E-4
Coverage of cases: 100.0
Instances selection time: 749.0
Test time: 969.0
Accumulative iteration time: 5090.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 8
Labeled set size: 952
Unlabelled set size: 3110
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 7.236824864088886E-4
Relative absolute error: 0.12615575107965177
Root relative squared error: 0.1447364972817777
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 757.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 6.307787553982588E-4
Coverage of cases: 100.0
Instances selection time: 757.0
Test time: 982.0
Accumulative iteration time: 5847.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 9
Labeled set size: 972
Unlabelled set size: 3090
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 7.058962910436749E-4
Relative absolute error: 0.12307699004685885
Root relative squared error: 0.14117925820873498
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 758.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 6.153849502342942E-4
Coverage of cases: 100.0
Instances selection time: 758.0
Test time: 984.0
Accumulative iteration time: 6605.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 10
Labeled set size: 992
Unlabelled set size: 3070
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 6.879352346464634E-4
Relative absolute error: 0.11983774461146264
Root relative squared error: 0.13758704692929266
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 761.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 5.991887230573132E-4
Coverage of cases: 100.0
Instances selection time: 761.0
Test time: 999.0
Accumulative iteration time: 7366.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 11
Labeled set size: 1012
Unlabelled set size: 3050
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 6.721541847310295E-4
Relative absolute error: 0.11707483315828678
Root relative squared error: 0.13443083694620592
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 767.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 5.853741657914339E-4
Coverage of cases: 100.0
Instances selection time: 767.0
Test time: 1009.0
Accumulative iteration time: 8133.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 12
Labeled set size: 1032
Unlabelled set size: 3030
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 6.564265124065783E-4
Relative absolute error: 0.11437455409283784
Root relative squared error: 0.13128530248131567
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 773.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 5.718727704641892E-4
Coverage of cases: 100.0
Instances selection time: 773.0
Test time: 1022.0
Accumulative iteration time: 8906.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 13
Labeled set size: 1052
Unlabelled set size: 3010
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 6.393044650901711E-4
Relative absolute error: 0.11131705211442249
Root relative squared error: 0.12786089301803422
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 777.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 5.565852605721125E-4
Coverage of cases: 100.0
Instances selection time: 777.0
Test time: 1035.0
Accumulative iteration time: 9683.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 14
Labeled set size: 1072
Unlabelled set size: 2990
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 6.243883676911342E-4
Relative absolute error: 0.10862302988943723
Root relative squared error: 0.12487767353822683
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 783.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 5.431151494471861E-4
Coverage of cases: 100.0
Instances selection time: 783.0
Test time: 1052.0
Accumulative iteration time: 10466.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 15
Labeled set size: 1092
Unlabelled set size: 2970
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 6.10126439576721E-4
Relative absolute error: 0.10612834146470294
Root relative squared error: 0.12202528791534419
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 786.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 5.306417073235147E-4
Coverage of cases: 100.0
Instances selection time: 785.0
Test time: 1060.0
Accumulative iteration time: 11252.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 16
Labeled set size: 1112
Unlabelled set size: 2950
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 5.956570673706211E-4
Relative absolute error: 0.10351066171475823
Root relative squared error: 0.11913141347412422
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 803.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 5.175533085737911E-4
Coverage of cases: 100.0
Instances selection time: 803.0
Test time: 1080.0
Accumulative iteration time: 12055.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 17
Labeled set size: 1132
Unlabelled set size: 2930
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 5.806968268581973E-4
Relative absolute error: 0.10074958714668321
Root relative squared error: 0.11613936537163945
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 796.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 5.03747935733416E-4
Coverage of cases: 100.0
Instances selection time: 796.0
Test time: 1090.0
Accumulative iteration time: 12851.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 18
Labeled set size: 1152
Unlabelled set size: 2910
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 5.651015823951598E-4
Relative absolute error: 0.09788580809178961
Root relative squared error: 0.11302031647903196
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 799.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.89429040458948E-4
Coverage of cases: 100.0
Instances selection time: 799.0
Test time: 1098.0
Accumulative iteration time: 13650.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 19
Labeled set size: 1172
Unlabelled set size: 2890
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 5.521731983719571E-4
Relative absolute error: 0.09552225389543288
Root relative squared error: 0.11043463967439142
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 803.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.776112694771644E-4
Coverage of cases: 100.0
Instances selection time: 803.0
Test time: 1108.0
Accumulative iteration time: 14453.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 20
Labeled set size: 1192
Unlabelled set size: 2870
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 5.411166872798682E-4
Relative absolute error: 0.09359395149363471
Root relative squared error: 0.10822333745597365
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 806.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.6796975746817357E-4
Coverage of cases: 100.0
Instances selection time: 806.0
Test time: 1123.0
Accumulative iteration time: 15259.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 21
Labeled set size: 1212
Unlabelled set size: 2850
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 5.287035088279802E-4
Relative absolute error: 0.09133632757047297
Root relative squared error: 0.10574070176559605
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 810.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.5668163785236485E-4
Coverage of cases: 100.0
Instances selection time: 810.0
Test time: 1135.0
Accumulative iteration time: 16069.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 22
Labeled set size: 1232
Unlabelled set size: 2830
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 5.170030809173429E-4
Relative absolute error: 0.08925194999836981
Root relative squared error: 0.10340061618346857
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 814.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.4625974999184907E-4
Coverage of cases: 100.0
Instances selection time: 814.0
Test time: 1147.0
Accumulative iteration time: 16883.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 23
Labeled set size: 1252
Unlabelled set size: 2810
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 5.059001461835803E-4
Relative absolute error: 0.08718319326225245
Root relative squared error: 0.10118002923671607
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 818.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.359159663112622E-4
Coverage of cases: 100.0
Instances selection time: 818.0
Test time: 1164.0
Accumulative iteration time: 17701.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 24
Labeled set size: 1272
Unlabelled set size: 2790
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 4.958928264134793E-4
Relative absolute error: 0.08539317876737867
Root relative squared error: 0.09917856528269585
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 823.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.2696589383689335E-4
Coverage of cases: 100.0
Instances selection time: 823.0
Test time: 1172.0
Accumulative iteration time: 18524.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 25
Labeled set size: 1292
Unlabelled set size: 2770
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 4.8543973762499987E-4
Relative absolute error: 0.08345399019498566
Root relative squared error: 0.09708794752499997
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 823.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.1726995097492826E-4
Coverage of cases: 100.0
Instances selection time: 823.0
Test time: 1184.0
Accumulative iteration time: 19347.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 26
Labeled set size: 1312
Unlabelled set size: 2750
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 4.7611131266582827E-4
Relative absolute error: 0.08173336616845953
Root relative squared error: 0.09522226253316565
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 825.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.086668308422976E-4
Coverage of cases: 100.0
Instances selection time: 825.0
Test time: 1200.0
Accumulative iteration time: 20172.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 27
Labeled set size: 1332
Unlabelled set size: 2730
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 4.666438646380169E-4
Relative absolute error: 0.08003533067700735
Root relative squared error: 0.09332877292760337
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 829.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.0017665338503675E-4
Coverage of cases: 100.0
Instances selection time: 829.0
Test time: 1225.0
Accumulative iteration time: 21001.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 28
Labeled set size: 1352
Unlabelled set size: 2710
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 4.5681539298138464E-4
Relative absolute error: 0.07823506360755136
Root relative squared error: 0.09136307859627693
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 831.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.9117531803775684E-4
Coverage of cases: 100.0
Instances selection time: 831.0
Test time: 1227.0
Accumulative iteration time: 21832.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 29
Labeled set size: 1372
Unlabelled set size: 2690
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 4.474242318621818E-4
Relative absolute error: 0.07649167160577718
Root relative squared error: 0.08948484637243635
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 832.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.8245835802888585E-4
Coverage of cases: 100.0
Instances selection time: 832.0
Test time: 1235.0
Accumulative iteration time: 22664.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 30
Labeled set size: 1392
Unlabelled set size: 2670
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 4.394650828140334E-4
Relative absolute error: 0.0750937371686615
Root relative squared error: 0.08789301656280668
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 836.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.754686858433075E-4
Coverage of cases: 100.0
Instances selection time: 835.0
Test time: 1247.0
Accumulative iteration time: 23500.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 31
Labeled set size: 1412
Unlabelled set size: 2650
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 4.3159065316983005E-4
Relative absolute error: 0.07368092061565883
Root relative squared error: 0.08631813063396601
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 836.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.6840460307829414E-4
Coverage of cases: 100.0
Instances selection time: 836.0
Test time: 1258.0
Accumulative iteration time: 24336.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 32
Labeled set size: 1432
Unlabelled set size: 2630
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 4.239391998425161E-4
Relative absolute error: 0.07240923643826805
Root relative squared error: 0.08478783996850321
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 838.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.6204618219134023E-4
Coverage of cases: 100.0
Instances selection time: 838.0
Test time: 1271.0
Accumulative iteration time: 25174.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 33
Labeled set size: 1452
Unlabelled set size: 2610
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 4.166114987189578E-4
Relative absolute error: 0.07109959624365848
Root relative squared error: 0.08332229974379156
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 840.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.554979812182924E-4
Coverage of cases: 100.0
Instances selection time: 840.0
Test time: 1283.0
Accumulative iteration time: 26014.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 34
Labeled set size: 1472
Unlabelled set size: 2590
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 4.0776349950803244E-4
Relative absolute error: 0.06958373933173118
Root relative squared error: 0.08155269990160649
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 841.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.479186966586559E-4
Coverage of cases: 100.0
Instances selection time: 841.0
Test time: 1296.0
Accumulative iteration time: 26855.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 35
Labeled set size: 1492
Unlabelled set size: 2570
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 4.013537565467227E-4
Relative absolute error: 0.06854775570115007
Root relative squared error: 0.08027075130934454
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 841.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.4273877850575035E-4
Coverage of cases: 100.0
Instances selection time: 841.0
Test time: 1312.0
Accumulative iteration time: 27696.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 36
Labeled set size: 1512
Unlabelled set size: 2550
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.9444887376619394E-4
Relative absolute error: 0.06739330993206466
Root relative squared error: 0.07888977475323879
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 843.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.369665496603233E-4
Coverage of cases: 100.0
Instances selection time: 843.0
Test time: 1334.0
Accumulative iteration time: 28539.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 37
Labeled set size: 1532
Unlabelled set size: 2530
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.884606666436597E-4
Relative absolute error: 0.06636193894377658
Root relative squared error: 0.07769213332873194
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1035.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.318096947188829E-4
Coverage of cases: 100.0
Instances selection time: 1035.0
Test time: 1352.0
Accumulative iteration time: 29574.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 38
Labeled set size: 1552
Unlabelled set size: 2510
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.818988362866832E-4
Relative absolute error: 0.0652796610505122
Root relative squared error: 0.07637976725733664
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 849.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.2639830525256095E-4
Coverage of cases: 100.0
Instances selection time: 849.0
Test time: 1360.0
Accumulative iteration time: 30423.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 39
Labeled set size: 1572
Unlabelled set size: 2490
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.7373613633639644E-4
Relative absolute error: 0.06387495050306295
Root relative squared error: 0.07474722726727928
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 851.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.1937475251531475E-4
Coverage of cases: 100.0
Instances selection time: 851.0
Test time: 1372.0
Accumulative iteration time: 31274.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 40
Labeled set size: 1592
Unlabelled set size: 2470
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.67141715569427E-4
Relative absolute error: 0.0627441016271437
Root relative squared error: 0.0734283431138854
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 850.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.137205081357185E-4
Coverage of cases: 100.0
Instances selection time: 850.0
Test time: 1384.0
Accumulative iteration time: 32124.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 41
Labeled set size: 1612
Unlabelled set size: 2450
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.5921643559923733E-4
Relative absolute error: 0.061388345954591375
Root relative squared error: 0.07184328711984747
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 842.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.069417297729569E-4
Coverage of cases: 100.0
Instances selection time: 842.0
Test time: 1380.0
Accumulative iteration time: 32966.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 42
Labeled set size: 1632
Unlabelled set size: 2430
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.526194442488078E-4
Relative absolute error: 0.06026773046258878
Root relative squared error: 0.07052388884976156
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 841.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.013386523129439E-4
Coverage of cases: 100.0
Instances selection time: 841.0
Test time: 1672.0
Accumulative iteration time: 33807.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 43
Labeled set size: 1652
Unlabelled set size: 2410
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.4583147015218195E-4
Relative absolute error: 0.059163251424565695
Root relative squared error: 0.06916629403043639
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 840.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 2.9581625712282847E-4
Coverage of cases: 100.0
Instances selection time: 840.0
Test time: 1405.0
Accumulative iteration time: 34647.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 44
Labeled set size: 1672
Unlabelled set size: 2390
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.404363999655519E-4
Relative absolute error: 0.05827808911455766
Root relative squared error: 0.06808727999311037
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 843.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 2.913904455727883E-4
Coverage of cases: 100.0
Instances selection time: 843.0
Test time: 1415.0
Accumulative iteration time: 35490.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 45
Labeled set size: 1692
Unlabelled set size: 2370
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.351128863347692E-4
Relative absolute error: 0.05741174887262121
Root relative squared error: 0.06702257726695383
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 852.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 2.8705874436310607E-4
Coverage of cases: 100.0
Instances selection time: 852.0
Test time: 1441.0
Accumulative iteration time: 36342.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 46
Labeled set size: 1712
Unlabelled set size: 2350
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.294697078697751E-4
Relative absolute error: 0.05645043541234218
Root relative squared error: 0.06589394157395502
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 840.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 2.822521770617109E-4
Coverage of cases: 100.0
Instances selection time: 840.0
Test time: 1445.0
Accumulative iteration time: 37182.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Time end:Wed Nov 01 05.27.18 EET 2017