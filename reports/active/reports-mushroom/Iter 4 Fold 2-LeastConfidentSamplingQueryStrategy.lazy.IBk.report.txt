Wed Nov 01 06.21.37 EET 2017
Dataset: mushroom
Test set size: 4062
Initial Labelled set size: 812
Initial Unlabelled set size: 3250
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Wed Nov 01 06.21.37 EET 2017
		
Iteration: 1
Labeled set size: 812
Unlabelled set size: 3250
	
Mean region size: 50.0492368291482
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 0.9999992717796152
Root mean squared error: 0.021791658906766747
Relative absolute error: 0.28379150331806346
Root relative squared error: 4.35833178135335
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 713.0
Weighted AreaUnderPRC: 0.9999992722793452
Mean absolute error: 0.0014189575165903174
Coverage of cases: 100.0
Instances selection time: 712.0
Test time: 890.0
Accumulative iteration time: 713.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 1.0
		
Iteration: 2
Labeled set size: 832
Unlabelled set size: 3230
	
Mean region size: 50.03692762186115
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.020329717413545556
Relative absolute error: 0.25556984734117455
Root relative squared error: 4.065943482709111
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 719.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0012778492367058727
Coverage of cases: 100.0
Instances selection time: 719.0
Test time: 894.0
Accumulative iteration time: 1432.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 0.0
		
Iteration: 3
Labeled set size: 852
Unlabelled set size: 3210
	
Mean region size: 50.03692762186115
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.020328807141586507
Relative absolute error: 0.25141852943694853
Root relative squared error: 4.065761428317302
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 723.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0012570926471847427
Coverage of cases: 100.0
Instances selection time: 723.0
Test time: 909.0
Accumulative iteration time: 2155.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 0.0
		
Iteration: 4
Labeled set size: 872
Unlabelled set size: 3190
	
Mean region size: 50.03692762186115
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.020328011477851234
Relative absolute error: 0.2476467491145462
Root relative squared error: 4.065602295570247
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 730.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.001238233745572731
Coverage of cases: 100.0
Instances selection time: 730.0
Test time: 941.0
Accumulative iteration time: 2885.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 0.0
		
Iteration: 5
Labeled set size: 892
Unlabelled set size: 3170
	
Mean region size: 50.03692762186115
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.020731739944156245
Relative absolute error: 0.2460227195084525
Root relative squared error: 4.146347988831249
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 742.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0012301135975422625
Coverage of cases: 100.0
Instances selection time: 742.0
Test time: 936.0
Accumulative iteration time: 3627.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 0.0
		
Iteration: 6
Labeled set size: 912
Unlabelled set size: 3150
	
Mean region size: 50.03692762186115
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.020731137561056375
Relative absolute error: 0.24291557737225142
Root relative squared error: 4.146227512211275
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 757.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.001214577886861257
Coverage of cases: 100.0
Instances selection time: 757.0
Test time: 948.0
Accumulative iteration time: 4384.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 0.0
		
Iteration: 7
Labeled set size: 932
Unlabelled set size: 3130
	
Mean region size: 50.03692762186115
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.020730494353256256
Relative absolute error: 0.2394943161763205
Root relative squared error: 4.146098870651251
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 768.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0011974715808816024
Coverage of cases: 100.0
Instances selection time: 768.0
Test time: 967.0
Accumulative iteration time: 5152.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 0.0
		
Iteration: 8
Labeled set size: 952
Unlabelled set size: 3110
	
Mean region size: 50.03692762186115
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.020729996738221464
Relative absolute error: 0.23689946514522542
Root relative squared error: 4.145999347644293
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 756.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0011844973257261271
Coverage of cases: 100.0
Instances selection time: 756.0
Test time: 980.0
Accumulative iteration time: 5908.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 0.0
		
Iteration: 9
Labeled set size: 972
Unlabelled set size: 3090
	
Mean region size: 50.03692762186115
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.020729416911028888
Relative absolute error: 0.2336200406042464
Root relative squared error: 4.145883382205778
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 764.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.001168100203021232
Coverage of cases: 100.0
Instances selection time: 764.0
Test time: 986.0
Accumulative iteration time: 6672.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 0.0
		
Iteration: 10
Labeled set size: 992
Unlabelled set size: 3070
	
Mean region size: 50.03692762186115
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.019393731674361828
Relative absolute error: 0.22347850903518263
Root relative squared error: 3.8787463348723654
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 770.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0011173925451759131
Coverage of cases: 100.0
Instances selection time: 770.0
Test time: 1017.0
Accumulative iteration time: 7442.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 0.0
		
Iteration: 11
Labeled set size: 1012
Unlabelled set size: 3050
	
Mean region size: 50.03692762186115
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.01939319275545118
Relative absolute error: 0.22056851130001628
Root relative squared error: 3.878638551090236
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 776.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0011028425565000814
Coverage of cases: 100.0
Instances selection time: 776.0
Test time: 1011.0
Accumulative iteration time: 8218.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 0.0
		
Iteration: 12
Labeled set size: 1032
Unlabelled set size: 3030
	
Mean region size: 50.03692762186115
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.01939271205909452
Relative absolute error: 0.2179832510580209
Root relative squared error: 3.8785424118189042
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 784.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0010899162552901046
Coverage of cases: 100.0
Instances selection time: 784.0
Test time: 1031.0
Accumulative iteration time: 9002.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 0.0
		
Iteration: 13
Labeled set size: 1052
Unlabelled set size: 3010
	
Mean region size: 50.03692762186115
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.01939223967482164
Relative absolute error: 0.21533370797304957
Root relative squared error: 3.878447934964328
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 792.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0010766685398652478
Coverage of cases: 100.0
Instances selection time: 792.0
Test time: 1039.0
Accumulative iteration time: 9794.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 0.0
		
Iteration: 14
Labeled set size: 1072
Unlabelled set size: 2990
	
Mean region size: 50.03692762186115
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.019391737940828768
Relative absolute error: 0.21238069428621292
Root relative squared error: 3.8783475881657536
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 797.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0010619034714310647
Coverage of cases: 100.0
Instances selection time: 797.0
Test time: 1049.0
Accumulative iteration time: 10591.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 0.0
		
Iteration: 15
Labeled set size: 1092
Unlabelled set size: 2970
	
Mean region size: 50.03692762186115
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0193912815082617
Relative absolute error: 0.20965830148342832
Root relative squared error: 3.87825630165234
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 795.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0010482915074171415
Coverage of cases: 100.0
Instances selection time: 795.0
Test time: 1062.0
Accumulative iteration time: 11386.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 0.0
		
Iteration: 16
Labeled set size: 1112
Unlabelled set size: 2950
	
Mean region size: 50.03692762186115
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.019390904782205596
Relative absolute error: 0.20731151526699496
Root relative squared error: 3.878180956441119
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 803.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0010365575763349748
Coverage of cases: 100.0
Instances selection time: 803.0
Test time: 1075.0
Accumulative iteration time: 12189.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 0.0
		
Iteration: 17
Labeled set size: 1132
Unlabelled set size: 2930
	
Mean region size: 50.03692762186115
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.019390526666830405
Relative absolute error: 0.20495697193997547
Root relative squared error: 3.878105333366081
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 817.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0010247848596998774
Coverage of cases: 100.0
Instances selection time: 817.0
Test time: 1092.0
Accumulative iteration time: 13006.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 0.0
		
Iteration: 18
Labeled set size: 1152
Unlabelled set size: 2910
	
Mean region size: 50.03692762186115
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.019390186723936775
Relative absolute error: 0.2026930490315943
Root relative squared error: 3.878037344787355
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 829.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0010134652451579715
Coverage of cases: 100.0
Instances selection time: 829.0
Test time: 1129.0
Accumulative iteration time: 13835.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 0.0
		
Iteration: 19
Labeled set size: 1172
Unlabelled set size: 2890
	
Mean region size: 50.03692762186115
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.019389888612027242
Relative absolute error: 0.2007192960619974
Root relative squared error: 3.8779777224054484
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 818.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.001003596480309987
Coverage of cases: 100.0
Instances selection time: 818.0
Test time: 1112.0
Accumulative iteration time: 14653.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 0.0
		
Iteration: 20
Labeled set size: 1192
Unlabelled set size: 2870
	
Mean region size: 50.03692762186115
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.01938959290583659
Relative absolute error: 0.19871347214785465
Root relative squared error: 3.877918581167318
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 818.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 9.935673607392732E-4
Coverage of cases: 100.0
Instances selection time: 818.0
Test time: 1124.0
Accumulative iteration time: 15471.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 0.0
		
Iteration: 21
Labeled set size: 1212
Unlabelled set size: 2850
	
Mean region size: 50.03692762186115
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.019389288919456536
Relative absolute error: 0.19660720107480736
Root relative squared error: 3.877857783891307
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 825.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 9.830360053740368E-4
Coverage of cases: 100.0
Instances selection time: 825.0
Test time: 1135.0
Accumulative iteration time: 16296.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 0.0
		
Iteration: 22
Labeled set size: 1232
Unlabelled set size: 2830
	
Mean region size: 50.03692762186115
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.01938898719122365
Relative absolute error: 0.19434339863470387
Root relative squared error: 3.8777974382447304
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 824.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 9.717169931735193E-4
Coverage of cases: 100.0
Instances selection time: 824.0
Test time: 1160.0
Accumulative iteration time: 17120.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 0.0
		
Iteration: 23
Labeled set size: 1252
Unlabelled set size: 2810
	
Mean region size: 50.03692762186115
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.01938869355643869
Relative absolute error: 0.19220740511506804
Root relative squared error: 3.877738711287738
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 843.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 9.610370255753402E-4
Coverage of cases: 100.0
Instances selection time: 843.0
Test time: 1189.0
Accumulative iteration time: 17963.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 0.0
		
Iteration: 24
Labeled set size: 1272
Unlabelled set size: 2790
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 4.963355553636104E-4
Relative absolute error: 0.08507360609454234
Root relative squared error: 0.09926711107272208
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 827.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.253680304727117E-4
Coverage of cases: 100.0
Instances selection time: 827.0
Test time: 1185.0
Accumulative iteration time: 18790.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 25
Labeled set size: 1292
Unlabelled set size: 2770
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 4.861692951546098E-4
Relative absolute error: 0.08331105746585578
Root relative squared error: 0.09723385903092197
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 829.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.165552873292789E-4
Coverage of cases: 100.0
Instances selection time: 829.0
Test time: 1192.0
Accumulative iteration time: 19619.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 26
Labeled set size: 1312
Unlabelled set size: 2750
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 4.764579826132109E-4
Relative absolute error: 0.08146841878943781
Root relative squared error: 0.09529159652264219
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 832.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.0734209394718905E-4
Coverage of cases: 100.0
Instances selection time: 832.0
Test time: 1203.0
Accumulative iteration time: 20451.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 27
Labeled set size: 1332
Unlabelled set size: 2730
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 4.668667041764449E-4
Relative absolute error: 0.07968872433044706
Root relative squared error: 0.09337334083528898
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 833.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.984436216522353E-4
Coverage of cases: 100.0
Instances selection time: 833.0
Test time: 1229.0
Accumulative iteration time: 21284.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 28
Labeled set size: 1352
Unlabelled set size: 2710
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 4.5744521148482136E-4
Relative absolute error: 0.07797696936374127
Root relative squared error: 0.09148904229696428
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 852.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.898848468187063E-4
Coverage of cases: 100.0
Instances selection time: 852.0
Test time: 1237.0
Accumulative iteration time: 22136.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 29
Labeled set size: 1372
Unlabelled set size: 2690
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 4.4734710965618123E-4
Relative absolute error: 0.07618554032811495
Root relative squared error: 0.08946942193123625
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 863.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.809277016405747E-4
Coverage of cases: 100.0
Instances selection time: 863.0
Test time: 1242.0
Accumulative iteration time: 22999.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 30
Labeled set size: 1392
Unlabelled set size: 2670
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 4.4132643206956873E-4
Relative absolute error: 0.07520186216431733
Root relative squared error: 0.08826528641391375
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 844.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.7600931082158663E-4
Coverage of cases: 100.0
Instances selection time: 844.0
Test time: 1550.0
Accumulative iteration time: 23843.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 31
Labeled set size: 1412
Unlabelled set size: 2650
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 4.340588231880893E-4
Relative absolute error: 0.07390917013487816
Root relative squared error: 0.08681176463761786
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 867.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.6954585067439083E-4
Coverage of cases: 100.0
Instances selection time: 867.0
Test time: 1302.0
Accumulative iteration time: 24710.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 32
Labeled set size: 1432
Unlabelled set size: 2630
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 4.266625797677712E-4
Relative absolute error: 0.07267779674064861
Root relative squared error: 0.08533251595355423
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1110.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.633889837032431E-4
Coverage of cases: 100.0
Instances selection time: 1110.0
Test time: 1293.0
Accumulative iteration time: 25820.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 33
Labeled set size: 1452
Unlabelled set size: 2610
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 4.208201610306681E-4
Relative absolute error: 0.0717252364600545
Root relative squared error: 0.08416403220613362
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 846.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.5862618230027255E-4
Coverage of cases: 100.0
Instances selection time: 846.0
Test time: 1285.0
Accumulative iteration time: 26666.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 34
Labeled set size: 1472
Unlabelled set size: 2590
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 4.1359972893490794E-4
Relative absolute error: 0.0704509971342412
Root relative squared error: 0.0827199457869816
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 862.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.5225498567120604E-4
Coverage of cases: 100.0
Instances selection time: 862.0
Test time: 1304.0
Accumulative iteration time: 27528.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 35
Labeled set size: 1492
Unlabelled set size: 2570
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 4.049650673250112E-4
Relative absolute error: 0.06897329288392649
Root relative squared error: 0.08099301346500223
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 848.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.4486646441963246E-4
Coverage of cases: 100.0
Instances selection time: 848.0
Test time: 1495.0
Accumulative iteration time: 28376.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 36
Labeled set size: 1512
Unlabelled set size: 2550
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.974494662217421E-4
Relative absolute error: 0.06774988942268387
Root relative squared error: 0.07948989324434842
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 847.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.387494471134194E-4
Coverage of cases: 100.0
Instances selection time: 847.0
Test time: 1342.0
Accumulative iteration time: 29223.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 37
Labeled set size: 1532
Unlabelled set size: 2530
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.9091419728805603E-4
Relative absolute error: 0.06664198438112309
Root relative squared error: 0.0781828394576112
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 857.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.3320992190561543E-4
Coverage of cases: 100.0
Instances selection time: 857.0
Test time: 1359.0
Accumulative iteration time: 30080.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 38
Labeled set size: 1552
Unlabelled set size: 2510
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.8583382145817327E-4
Relative absolute error: 0.06587350923406673
Root relative squared error: 0.07716676429163466
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 865.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.293675461703337E-4
Coverage of cases: 100.0
Instances selection time: 865.0
Test time: 1363.0
Accumulative iteration time: 30945.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 39
Labeled set size: 1572
Unlabelled set size: 2490
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.7807566051738315E-4
Relative absolute error: 0.06451083695486126
Root relative squared error: 0.07561513210347663
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 852.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.2255418477430633E-4
Coverage of cases: 100.0
Instances selection time: 852.0
Test time: 1370.0
Accumulative iteration time: 31797.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 40
Labeled set size: 1592
Unlabelled set size: 2470
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.7027265272508806E-4
Relative absolute error: 0.06319678631659388
Root relative squared error: 0.07405453054501761
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 846.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.159839315829694E-4
Coverage of cases: 100.0
Instances selection time: 846.0
Test time: 1382.0
Accumulative iteration time: 32643.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 41
Labeled set size: 1612
Unlabelled set size: 2450
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.63458333910487E-4
Relative absolute error: 0.06200751221675998
Root relative squared error: 0.07269166678209739
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 846.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.100375610837999E-4
Coverage of cases: 100.0
Instances selection time: 846.0
Test time: 1411.0
Accumulative iteration time: 33489.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 42
Labeled set size: 1632
Unlabelled set size: 2430
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.56677258242834E-4
Relative absolute error: 0.06084181198800365
Root relative squared error: 0.07133545164856679
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 871.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.0420905994001823E-4
Coverage of cases: 100.0
Instances selection time: 870.0
Test time: 1428.0
Accumulative iteration time: 34360.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 43
Labeled set size: 1652
Unlabelled set size: 2410
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.490566563182141E-4
Relative absolute error: 0.05952429850061254
Root relative squared error: 0.06981133126364282
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 849.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 2.976214925030627E-4
Coverage of cases: 100.0
Instances selection time: 849.0
Test time: 1405.0
Accumulative iteration time: 35209.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 44
Labeled set size: 1672
Unlabelled set size: 2390
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.437086659704173E-4
Relative absolute error: 0.05861360237463556
Root relative squared error: 0.06874173319408346
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 845.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 2.930680118731778E-4
Coverage of cases: 100.0
Instances selection time: 845.0
Test time: 1427.0
Accumulative iteration time: 36054.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 45
Labeled set size: 1692
Unlabelled set size: 2370
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.3715998781883873E-4
Relative absolute error: 0.05748797169301393
Root relative squared error: 0.06743199756376775
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 854.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 2.8743985846506966E-4
Coverage of cases: 100.0
Instances selection time: 854.0
Test time: 1436.0
Accumulative iteration time: 36908.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 46
Labeled set size: 1712
Unlabelled set size: 2350
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.303947829403142E-4
Relative absolute error: 0.05632482685950819
Root relative squared error: 0.06607895658806284
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 869.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 2.8162413429754093E-4
Coverage of cases: 100.0
Instances selection time: 869.0
Test time: 1456.0
Accumulative iteration time: 37777.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Time end:Wed Nov 01 06.23.14 EET 2017