Wed Nov 01 06.52.01 EET 2017
Dataset: mushroom
Test set size: 4062
Initial Labelled set size: 812
Initial Unlabelled set size: 3250
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Wed Nov 01 06.52.01 EET 2017
		
Iteration: 1
Labeled set size: 812
Unlabelled set size: 3250
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992625774237212
Weighted AreaUnderROC: 0.999287072243346
Root mean squared error: 0.0271763212599322
Relative absolute error: 0.14771048744460857
Root relative squared error: 5.43526425198644
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985221550307092
Weighted FMeasure: 0.9992614667891501
Iteration time: 163.0
Weighted AreaUnderPRC: 0.9989065740578712
Mean absolute error: 7.385524372230429E-4
Coverage of cases: 99.9261447562777
Instances selection time: 18.0
Test time: 37.0
Accumulative iteration time: 163.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 6.873030760849421E-4
Kappa statistic: 0.9985210630190265
Training time: 145.0
		
Iteration: 2
Labeled set size: 832
Unlabelled set size: 3230
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992625774237212
Weighted AreaUnderROC: 0.999287072243346
Root mean squared error: 0.0271763212599322
Relative absolute error: 0.14771048744460857
Root relative squared error: 5.43526425198644
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985221550307092
Weighted FMeasure: 0.9992614667891501
Iteration time: 199.0
Weighted AreaUnderPRC: 0.9989065740578712
Mean absolute error: 7.385524372230429E-4
Coverage of cases: 99.9261447562777
Instances selection time: 18.0
Test time: 34.0
Accumulative iteration time: 362.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 6.873030760849421E-4
Kappa statistic: 0.9985210630190265
Training time: 181.0
		
Iteration: 3
Labeled set size: 852
Unlabelled set size: 3210
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992625774237212
Weighted AreaUnderROC: 0.999287072243346
Root mean squared error: 0.0271763212599322
Relative absolute error: 0.14771048744460857
Root relative squared error: 5.43526425198644
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985221550307092
Weighted FMeasure: 0.9992614667891501
Iteration time: 173.0
Weighted AreaUnderPRC: 0.9989065740578712
Mean absolute error: 7.385524372230429E-4
Coverage of cases: 99.9261447562777
Instances selection time: 18.0
Test time: 34.0
Accumulative iteration time: 535.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 6.873030760849421E-4
Kappa statistic: 0.9985210630190265
Training time: 155.0
		
Iteration: 4
Labeled set size: 872
Unlabelled set size: 3190
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992625774237212
Weighted AreaUnderROC: 0.999287072243346
Root mean squared error: 0.0271763212599322
Relative absolute error: 0.14771048744460857
Root relative squared error: 5.43526425198644
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985221550307092
Weighted FMeasure: 0.9992614667891501
Iteration time: 172.0
Weighted AreaUnderPRC: 0.9989065740578712
Mean absolute error: 7.385524372230429E-4
Coverage of cases: 99.9261447562777
Instances selection time: 21.0
Test time: 34.0
Accumulative iteration time: 707.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 6.873030760849421E-4
Kappa statistic: 0.9985210630190265
Training time: 151.0
		
Iteration: 5
Labeled set size: 892
Unlabelled set size: 3170
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.049236829148202856
Correctly Classified Instances: 99.9507631708518
Weighted Precision: 0.999508134125142
Weighted AreaUnderROC: 0.9995247148288974
Root mean squared error: 0.022189373390928115
Relative absolute error: 0.09847365829640572
Root relative squared error: 4.437874678185623
Weighted TruePositiveRate: 0.999507631708518
Weighted MatthewsCorrelation: 0.9990145101369982
Weighted FMeasure: 0.9995076403134782
Iteration time: 236.0
Weighted AreaUnderPRC: 0.9992707985479086
Mean absolute error: 4.923682914820286E-4
Coverage of cases: 99.9507631708518
Instances selection time: 17.0
Test time: 34.0
Accumulative iteration time: 943.0
Weighted Recall: 0.999507631708518
Weighted FalsePositiveRate: 4.5820205072329465E-4
Kappa statistic: 0.9990140245420991
Training time: 219.0
		
Iteration: 6
Labeled set size: 912
Unlabelled set size: 3150
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.09847365829640571
Correctly Classified Instances: 99.9015263417036
Weighted Precision: 0.9990172710349421
Weighted AreaUnderROC: 0.9990494296577946
Root mean squared error: 0.03138051279001121
Relative absolute error: 0.19694731659281145
Root relative squared error: 6.276102558002242
Weighted TruePositiveRate: 0.999015263417036
Weighted MatthewsCorrelation: 0.9980300592947201
Weighted FMeasure: 0.9990152973576452
Iteration time: 179.0
Weighted AreaUnderPRC: 0.9985425998804756
Mean absolute error: 9.847365829640572E-4
Coverage of cases: 99.9015263417036
Instances selection time: 17.0
Test time: 34.0
Accumulative iteration time: 1122.0
Weighted Recall: 0.999015263417036
Weighted FalsePositiveRate: 9.164041014465893E-4
Kappa statistic: 0.9980281189653013
Training time: 162.0
		
Iteration: 7
Labeled set size: 932
Unlabelled set size: 3130
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.09847365829640571
Correctly Classified Instances: 99.9015263417036
Weighted Precision: 0.9990172710349421
Weighted AreaUnderROC: 0.9990494296577946
Root mean squared error: 0.03138051279001121
Relative absolute error: 0.19694731659281145
Root relative squared error: 6.276102558002242
Weighted TruePositiveRate: 0.999015263417036
Weighted MatthewsCorrelation: 0.9980300592947201
Weighted FMeasure: 0.9990152973576452
Iteration time: 213.0
Weighted AreaUnderPRC: 0.9985425998804756
Mean absolute error: 9.847365829640572E-4
Coverage of cases: 99.9015263417036
Instances selection time: 17.0
Test time: 34.0
Accumulative iteration time: 1335.0
Weighted Recall: 0.999015263417036
Weighted FalsePositiveRate: 9.164041014465893E-4
Kappa statistic: 0.9980281189653013
Training time: 196.0
		
Iteration: 8
Labeled set size: 952
Unlabelled set size: 3110
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.09847365829640571
Correctly Classified Instances: 99.9015263417036
Weighted Precision: 0.9990172710349421
Weighted AreaUnderROC: 0.9990494296577946
Root mean squared error: 0.03138051279001121
Relative absolute error: 0.19694731659281145
Root relative squared error: 6.276102558002242
Weighted TruePositiveRate: 0.999015263417036
Weighted MatthewsCorrelation: 0.9980300592947201
Weighted FMeasure: 0.9990152973576452
Iteration time: 218.0
Weighted AreaUnderPRC: 0.9985425998804756
Mean absolute error: 9.847365829640572E-4
Coverage of cases: 99.9015263417036
Instances selection time: 17.0
Test time: 35.0
Accumulative iteration time: 1553.0
Weighted Recall: 0.999015263417036
Weighted FalsePositiveRate: 9.164041014465893E-4
Kappa statistic: 0.9980281189653013
Training time: 201.0
		
Iteration: 9
Labeled set size: 972
Unlabelled set size: 3090
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.09847365829640571
Correctly Classified Instances: 99.9015263417036
Weighted Precision: 0.9990172710349421
Weighted AreaUnderROC: 0.9990494296577946
Root mean squared error: 0.03138051279001121
Relative absolute error: 0.19694731659281145
Root relative squared error: 6.276102558002242
Weighted TruePositiveRate: 0.999015263417036
Weighted MatthewsCorrelation: 0.9980300592947201
Weighted FMeasure: 0.9990152973576452
Iteration time: 230.0
Weighted AreaUnderPRC: 0.9985425998804756
Mean absolute error: 9.847365829640572E-4
Coverage of cases: 99.9015263417036
Instances selection time: 17.0
Test time: 34.0
Accumulative iteration time: 1783.0
Weighted Recall: 0.999015263417036
Weighted FalsePositiveRate: 9.164041014465893E-4
Kappa statistic: 0.9980281189653013
Training time: 213.0
		
Iteration: 10
Labeled set size: 992
Unlabelled set size: 3070
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.09847365829640571
Correctly Classified Instances: 99.9015263417036
Weighted Precision: 0.9990172710349421
Weighted AreaUnderROC: 0.9990494296577946
Root mean squared error: 0.03138051279001121
Relative absolute error: 0.19694731659281145
Root relative squared error: 6.276102558002242
Weighted TruePositiveRate: 0.999015263417036
Weighted MatthewsCorrelation: 0.9980300592947201
Weighted FMeasure: 0.9990152973576452
Iteration time: 248.0
Weighted AreaUnderPRC: 0.9985425998804756
Mean absolute error: 9.847365829640572E-4
Coverage of cases: 99.9015263417036
Instances selection time: 16.0
Test time: 35.0
Accumulative iteration time: 2031.0
Weighted Recall: 0.999015263417036
Weighted FalsePositiveRate: 9.164041014465893E-4
Kappa statistic: 0.9980281189653013
Training time: 232.0
		
Iteration: 11
Labeled set size: 1012
Unlabelled set size: 3050
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.09847365829640571
Correctly Classified Instances: 99.9015263417036
Weighted Precision: 0.9990172710349421
Weighted AreaUnderROC: 0.9990494296577946
Root mean squared error: 0.03138051279001121
Relative absolute error: 0.19694731659281145
Root relative squared error: 6.276102558002242
Weighted TruePositiveRate: 0.999015263417036
Weighted MatthewsCorrelation: 0.9980300592947201
Weighted FMeasure: 0.9990152973576452
Iteration time: 191.0
Weighted AreaUnderPRC: 0.9985425998804756
Mean absolute error: 9.847365829640572E-4
Coverage of cases: 99.9015263417036
Instances selection time: 16.0
Test time: 34.0
Accumulative iteration time: 2222.0
Weighted Recall: 0.999015263417036
Weighted FalsePositiveRate: 9.164041014465893E-4
Kappa statistic: 0.9980281189653013
Training time: 175.0
		
Iteration: 12
Labeled set size: 1032
Unlabelled set size: 3030
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.09847365829640571
Correctly Classified Instances: 99.9015263417036
Weighted Precision: 0.9990172710349421
Weighted AreaUnderROC: 0.9990494296577946
Root mean squared error: 0.03138051279001121
Relative absolute error: 0.19694731659281145
Root relative squared error: 6.276102558002242
Weighted TruePositiveRate: 0.999015263417036
Weighted MatthewsCorrelation: 0.9980300592947201
Weighted FMeasure: 0.9990152973576452
Iteration time: 261.0
Weighted AreaUnderPRC: 0.9985425998804756
Mean absolute error: 9.847365829640572E-4
Coverage of cases: 99.9015263417036
Instances selection time: 15.0
Test time: 35.0
Accumulative iteration time: 2483.0
Weighted Recall: 0.999015263417036
Weighted FalsePositiveRate: 9.164041014465893E-4
Kappa statistic: 0.9980281189653013
Training time: 246.0
		
Iteration: 13
Labeled set size: 1052
Unlabelled set size: 3010
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.09847365829640571
Correctly Classified Instances: 99.9015263417036
Weighted Precision: 0.9990172710349421
Weighted AreaUnderROC: 0.9990494296577946
Root mean squared error: 0.03138051279001121
Relative absolute error: 0.19694731659281145
Root relative squared error: 6.276102558002242
Weighted TruePositiveRate: 0.999015263417036
Weighted MatthewsCorrelation: 0.9980300592947201
Weighted FMeasure: 0.9990152973576452
Iteration time: 207.0
Weighted AreaUnderPRC: 0.9985425998804756
Mean absolute error: 9.847365829640572E-4
Coverage of cases: 99.9015263417036
Instances selection time: 16.0
Test time: 34.0
Accumulative iteration time: 2690.0
Weighted Recall: 0.999015263417036
Weighted FalsePositiveRate: 9.164041014465893E-4
Kappa statistic: 0.9980281189653013
Training time: 191.0
		
Iteration: 14
Labeled set size: 1072
Unlabelled set size: 2990
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.09847365829640571
Correctly Classified Instances: 99.9015263417036
Weighted Precision: 0.9990172710349421
Weighted AreaUnderROC: 0.9990494296577946
Root mean squared error: 0.03138051279001121
Relative absolute error: 0.19694731659281145
Root relative squared error: 6.276102558002242
Weighted TruePositiveRate: 0.999015263417036
Weighted MatthewsCorrelation: 0.9980300592947201
Weighted FMeasure: 0.9990152973576452
Iteration time: 191.0
Weighted AreaUnderPRC: 0.9985425998804756
Mean absolute error: 9.847365829640572E-4
Coverage of cases: 99.9015263417036
Instances selection time: 15.0
Test time: 35.0
Accumulative iteration time: 2881.0
Weighted Recall: 0.999015263417036
Weighted FalsePositiveRate: 9.164041014465893E-4
Kappa statistic: 0.9980281189653013
Training time: 176.0
		
Iteration: 15
Labeled set size: 1092
Unlabelled set size: 2970
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.09847365829640571
Correctly Classified Instances: 99.9015263417036
Weighted Precision: 0.9990172710349421
Weighted AreaUnderROC: 0.9990494296577946
Root mean squared error: 0.03138051279001121
Relative absolute error: 0.19694731659281145
Root relative squared error: 6.276102558002242
Weighted TruePositiveRate: 0.999015263417036
Weighted MatthewsCorrelation: 0.9980300592947201
Weighted FMeasure: 0.9990152973576452
Iteration time: 299.0
Weighted AreaUnderPRC: 0.9985425998804756
Mean absolute error: 9.847365829640572E-4
Coverage of cases: 99.9015263417036
Instances selection time: 15.0
Test time: 32.0
Accumulative iteration time: 3180.0
Weighted Recall: 0.999015263417036
Weighted FalsePositiveRate: 9.164041014465893E-4
Kappa statistic: 0.9980281189653013
Training time: 284.0
		
Iteration: 16
Labeled set size: 1112
Unlabelled set size: 2950
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.09847365829640571
Correctly Classified Instances: 99.9015263417036
Weighted Precision: 0.9990172710349421
Weighted AreaUnderROC: 0.9990494296577946
Root mean squared error: 0.03138051279001121
Relative absolute error: 0.19694731659281145
Root relative squared error: 6.276102558002242
Weighted TruePositiveRate: 0.999015263417036
Weighted MatthewsCorrelation: 0.9980300592947201
Weighted FMeasure: 0.9990152973576452
Iteration time: 217.0
Weighted AreaUnderPRC: 0.9985425998804756
Mean absolute error: 9.847365829640572E-4
Coverage of cases: 99.9015263417036
Instances selection time: 15.0
Test time: 32.0
Accumulative iteration time: 3397.0
Weighted Recall: 0.999015263417036
Weighted FalsePositiveRate: 9.164041014465893E-4
Kappa statistic: 0.9980281189653013
Training time: 202.0
		
Iteration: 17
Labeled set size: 1132
Unlabelled set size: 2930
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.09847365829640571
Correctly Classified Instances: 99.9015263417036
Weighted Precision: 0.9990172710349421
Weighted AreaUnderROC: 0.9990494296577946
Root mean squared error: 0.03138051279001121
Relative absolute error: 0.19694731659281145
Root relative squared error: 6.276102558002242
Weighted TruePositiveRate: 0.999015263417036
Weighted MatthewsCorrelation: 0.9980300592947201
Weighted FMeasure: 0.9990152973576452
Iteration time: 212.0
Weighted AreaUnderPRC: 0.9985425998804756
Mean absolute error: 9.847365829640572E-4
Coverage of cases: 99.9015263417036
Instances selection time: 14.0
Test time: 31.0
Accumulative iteration time: 3609.0
Weighted Recall: 0.999015263417036
Weighted FalsePositiveRate: 9.164041014465893E-4
Kappa statistic: 0.9980281189653013
Training time: 198.0
		
Iteration: 18
Labeled set size: 1152
Unlabelled set size: 2910
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992625774237212
Weighted AreaUnderROC: 0.999287072243346
Root mean squared error: 0.0271763212599322
Relative absolute error: 0.14771048744460857
Root relative squared error: 5.43526425198644
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985221550307092
Weighted FMeasure: 0.9992614667891501
Iteration time: 317.0
Weighted AreaUnderPRC: 0.9989065740578712
Mean absolute error: 7.385524372230429E-4
Coverage of cases: 99.9261447562777
Instances selection time: 14.0
Test time: 31.0
Accumulative iteration time: 3926.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 6.873030760849421E-4
Kappa statistic: 0.9985210630190265
Training time: 303.0
		
Iteration: 19
Labeled set size: 1172
Unlabelled set size: 2890
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992625774237212
Weighted AreaUnderROC: 0.999287072243346
Root mean squared error: 0.0271763212599322
Relative absolute error: 0.14771048744460857
Root relative squared error: 5.43526425198644
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985221550307092
Weighted FMeasure: 0.9992614667891501
Iteration time: 316.0
Weighted AreaUnderPRC: 0.9989065740578712
Mean absolute error: 7.385524372230429E-4
Coverage of cases: 99.9261447562777
Instances selection time: 14.0
Test time: 32.0
Accumulative iteration time: 4242.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 6.873030760849421E-4
Kappa statistic: 0.9985210630190265
Training time: 302.0
		
Iteration: 20
Labeled set size: 1192
Unlabelled set size: 2870
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992625774237212
Weighted AreaUnderROC: 0.999287072243346
Root mean squared error: 0.0271763212599322
Relative absolute error: 0.14771048744460857
Root relative squared error: 5.43526425198644
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985221550307092
Weighted FMeasure: 0.9992614667891501
Iteration time: 234.0
Weighted AreaUnderPRC: 0.9989065740578712
Mean absolute error: 7.385524372230429E-4
Coverage of cases: 99.9261447562777
Instances selection time: 14.0
Test time: 31.0
Accumulative iteration time: 4476.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 6.873030760849421E-4
Kappa statistic: 0.9985210630190265
Training time: 220.0
		
Iteration: 21
Labeled set size: 1212
Unlabelled set size: 2850
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992625774237212
Weighted AreaUnderROC: 0.999287072243346
Root mean squared error: 0.0271763212599322
Relative absolute error: 0.14771048744460857
Root relative squared error: 5.43526425198644
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985221550307092
Weighted FMeasure: 0.9992614667891501
Iteration time: 282.0
Weighted AreaUnderPRC: 0.9989065740578712
Mean absolute error: 7.385524372230429E-4
Coverage of cases: 99.9261447562777
Instances selection time: 14.0
Test time: 31.0
Accumulative iteration time: 4758.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 6.873030760849421E-4
Kappa statistic: 0.9985210630190265
Training time: 268.0
		
Iteration: 22
Labeled set size: 1232
Unlabelled set size: 2830
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992625774237212
Weighted AreaUnderROC: 0.999287072243346
Root mean squared error: 0.0271763212599322
Relative absolute error: 0.14771048744460857
Root relative squared error: 5.43526425198644
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985221550307092
Weighted FMeasure: 0.9992614667891501
Iteration time: 216.0
Weighted AreaUnderPRC: 0.9989065740578712
Mean absolute error: 7.385524372230429E-4
Coverage of cases: 99.9261447562777
Instances selection time: 14.0
Test time: 31.0
Accumulative iteration time: 4974.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 6.873030760849421E-4
Kappa statistic: 0.9985210630190265
Training time: 202.0
		
Iteration: 23
Labeled set size: 1252
Unlabelled set size: 2810
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992625774237212
Weighted AreaUnderROC: 0.999287072243346
Root mean squared error: 0.0271763212599322
Relative absolute error: 0.14771048744460857
Root relative squared error: 5.43526425198644
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985221550307092
Weighted FMeasure: 0.9992614667891501
Iteration time: 241.0
Weighted AreaUnderPRC: 0.9989065740578712
Mean absolute error: 7.385524372230429E-4
Coverage of cases: 99.9261447562777
Instances selection time: 13.0
Test time: 32.0
Accumulative iteration time: 5215.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 6.873030760849421E-4
Kappa statistic: 0.9985210630190265
Training time: 228.0
		
Iteration: 24
Labeled set size: 1272
Unlabelled set size: 2790
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992625774237212
Weighted AreaUnderROC: 0.999287072243346
Root mean squared error: 0.0271763212599322
Relative absolute error: 0.14771048744460857
Root relative squared error: 5.43526425198644
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985221550307092
Weighted FMeasure: 0.9992614667891501
Iteration time: 218.0
Weighted AreaUnderPRC: 0.9989065740578712
Mean absolute error: 7.385524372230429E-4
Coverage of cases: 99.9261447562777
Instances selection time: 13.0
Test time: 31.0
Accumulative iteration time: 5433.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 6.873030760849421E-4
Kappa statistic: 0.9985210630190265
Training time: 205.0
		
Iteration: 25
Labeled set size: 1292
Unlabelled set size: 2770
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992625774237212
Weighted AreaUnderROC: 0.999287072243346
Root mean squared error: 0.0271763212599322
Relative absolute error: 0.14771048744460857
Root relative squared error: 5.43526425198644
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985221550307092
Weighted FMeasure: 0.9992614667891501
Iteration time: 223.0
Weighted AreaUnderPRC: 0.9989065740578712
Mean absolute error: 7.385524372230429E-4
Coverage of cases: 99.9261447562777
Instances selection time: 13.0
Test time: 31.0
Accumulative iteration time: 5656.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 6.873030760849421E-4
Kappa statistic: 0.9985210630190265
Training time: 210.0
		
Iteration: 26
Labeled set size: 1312
Unlabelled set size: 2750
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992625774237212
Weighted AreaUnderROC: 0.999287072243346
Root mean squared error: 0.0271763212599322
Relative absolute error: 0.14771048744460857
Root relative squared error: 5.43526425198644
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985221550307092
Weighted FMeasure: 0.9992614667891501
Iteration time: 433.0
Weighted AreaUnderPRC: 0.9989065740578712
Mean absolute error: 7.385524372230429E-4
Coverage of cases: 99.9261447562777
Instances selection time: 13.0
Test time: 32.0
Accumulative iteration time: 6089.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 6.873030760849421E-4
Kappa statistic: 0.9985210630190265
Training time: 420.0
		
Iteration: 27
Labeled set size: 1332
Unlabelled set size: 2730
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992625774237212
Weighted AreaUnderROC: 0.999287072243346
Root mean squared error: 0.0271763212599322
Relative absolute error: 0.14771048744460857
Root relative squared error: 5.43526425198644
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985221550307092
Weighted FMeasure: 0.9992614667891501
Iteration time: 243.0
Weighted AreaUnderPRC: 0.9989065740578712
Mean absolute error: 7.385524372230429E-4
Coverage of cases: 99.9261447562777
Instances selection time: 13.0
Test time: 31.0
Accumulative iteration time: 6332.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 6.873030760849421E-4
Kappa statistic: 0.9985210630190265
Training time: 230.0
		
Iteration: 28
Labeled set size: 1352
Unlabelled set size: 2710
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992625774237212
Weighted AreaUnderROC: 0.999287072243346
Root mean squared error: 0.0271763212599322
Relative absolute error: 0.14771048744460857
Root relative squared error: 5.43526425198644
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985221550307092
Weighted FMeasure: 0.9992614667891501
Iteration time: 230.0
Weighted AreaUnderPRC: 0.9989065740578712
Mean absolute error: 7.385524372230429E-4
Coverage of cases: 99.9261447562777
Instances selection time: 13.0
Test time: 32.0
Accumulative iteration time: 6562.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 6.873030760849421E-4
Kappa statistic: 0.9985210630190265
Training time: 217.0
		
Iteration: 29
Labeled set size: 1372
Unlabelled set size: 2690
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 323.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 12.0
Test time: 31.0
Accumulative iteration time: 6885.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 311.0
		
Iteration: 30
Labeled set size: 1392
Unlabelled set size: 2670
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 243.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 12.0
Test time: 32.0
Accumulative iteration time: 7128.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 231.0
		
Iteration: 31
Labeled set size: 1412
Unlabelled set size: 2650
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 261.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 12.0
Test time: 31.0
Accumulative iteration time: 7389.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 249.0
		
Iteration: 32
Labeled set size: 1432
Unlabelled set size: 2630
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 272.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 12.0
Test time: 31.0
Accumulative iteration time: 7661.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 260.0
		
Iteration: 33
Labeled set size: 1452
Unlabelled set size: 2610
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 280.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 12.0
Test time: 31.0
Accumulative iteration time: 7941.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 268.0
		
Iteration: 34
Labeled set size: 1472
Unlabelled set size: 2590
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 273.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 11.0
Test time: 31.0
Accumulative iteration time: 8214.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 262.0
		
Iteration: 35
Labeled set size: 1492
Unlabelled set size: 2570
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 301.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 11.0
Test time: 30.0
Accumulative iteration time: 8515.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 290.0
		
Iteration: 36
Labeled set size: 1512
Unlabelled set size: 2550
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 280.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 11.0
Test time: 30.0
Accumulative iteration time: 8795.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 269.0
		
Iteration: 37
Labeled set size: 1532
Unlabelled set size: 2530
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 296.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 15.0
Test time: 30.0
Accumulative iteration time: 9091.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 281.0
		
Iteration: 38
Labeled set size: 1552
Unlabelled set size: 2510
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 287.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 13.0
Test time: 28.0
Accumulative iteration time: 9378.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 274.0
		
Iteration: 39
Labeled set size: 1572
Unlabelled set size: 2490
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 291.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 12.0
Test time: 27.0
Accumulative iteration time: 9669.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 279.0
		
Iteration: 40
Labeled set size: 1592
Unlabelled set size: 2470
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 309.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 14.0
Test time: 27.0
Accumulative iteration time: 9978.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 295.0
		
Iteration: 41
Labeled set size: 1612
Unlabelled set size: 2450
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 316.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 12.0
Test time: 28.0
Accumulative iteration time: 10294.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 304.0
		
Iteration: 42
Labeled set size: 1632
Unlabelled set size: 2430
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 321.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 12.0
Test time: 28.0
Accumulative iteration time: 10615.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 309.0
		
Iteration: 43
Labeled set size: 1652
Unlabelled set size: 2410
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 275.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 10.0
Test time: 28.0
Accumulative iteration time: 10890.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 265.0
		
Iteration: 44
Labeled set size: 1672
Unlabelled set size: 2390
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 306.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 10.0
Test time: 28.0
Accumulative iteration time: 11196.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 296.0
		
Iteration: 45
Labeled set size: 1692
Unlabelled set size: 2370
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 273.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 10.0
Test time: 28.0
Accumulative iteration time: 11469.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 263.0
		
Iteration: 46
Labeled set size: 1712
Unlabelled set size: 2350
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 299.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 9.0
Test time: 28.0
Accumulative iteration time: 11768.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 290.0
		
Time end:Wed Nov 01 06.52.19 EET 2017