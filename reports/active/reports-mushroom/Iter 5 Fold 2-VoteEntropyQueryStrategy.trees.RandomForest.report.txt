Wed Nov 01 07.28.15 EET 2017
Dataset: mushroom
Test set size: 4062
Initial Labelled set size: 812
Initial Unlabelled set size: 3250
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Wed Nov 01 07.28.15 EET 2017
		
Iteration: 1
Labeled set size: 812
Unlabelled set size: 3250
	
Mean region size: 54.197439684884294
Incorrectly Classified Instances: 0.29542097488921715
Correctly Classified Instances: 99.70457902511079
Weighted Precision: 0.9970637854373954
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0561365303412834
Relative absolute error: 2.795075746498749
Root relative squared error: 11.22730606825668
Weighted TruePositiveRate: 0.9970457902511078
Weighted MatthewsCorrelation: 0.9941025847695849
Weighted FMeasure: 0.9970460784686516
Iteration time: 18.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.013975378732493743
Coverage of cases: 100.0
Instances selection time: 7.0
Test time: 12.0
Accumulative iteration time: 18.0
Weighted Recall: 0.9970457902511078
Weighted FalsePositiveRate: 0.0027492123043397682
Kappa statistic: 0.994085195320577
Training time: 11.0
		
Iteration: 2
Labeled set size: 832
Unlabelled set size: 3230
	
Mean region size: 52.47415066469719
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.042931806332019616
Relative absolute error: 1.6056218928322061
Root relative squared error: 8.586361266403923
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 15.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.008028109464161031
Coverage of cases: 100.0
Instances selection time: 6.0
Test time: 12.0
Accumulative iteration time: 33.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 9.0
		
Iteration: 3
Labeled set size: 852
Unlabelled set size: 3210
	
Mean region size: 52.621861152141804
Incorrectly Classified Instances: 0.049236829148202856
Correctly Classified Instances: 99.9507631708518
Weighted Precision: 0.9995080992947397
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.04280617389038428
Relative absolute error: 1.7536706470244574
Root relative squared error: 8.561234778076857
Weighted TruePositiveRate: 0.999507631708518
Weighted MatthewsCorrelation: 0.999014440317274
Weighted FMeasure: 0.9995076228639044
Iteration time: 14.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.008768353235122288
Coverage of cases: 100.0
Instances selection time: 6.0
Test time: 12.0
Accumulative iteration time: 47.0
Weighted Recall: 0.999507631708518
Weighted FalsePositiveRate: 5.290821681706783E-4
Kappa statistic: 0.999013954653566
Training time: 8.0
		
Iteration: 4
Labeled set size: 872
Unlabelled set size: 3190
	
Mean region size: 53.01575578532743
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.03624777742997579
Relative absolute error: 1.5977348394095032
Root relative squared error: 7.249555485995159
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 14.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.007988674197047517
Coverage of cases: 100.0
Instances selection time: 6.0
Test time: 12.0
Accumulative iteration time: 61.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 8.0
		
Iteration: 5
Labeled set size: 892
Unlabelled set size: 3170
	
Mean region size: 53.80354505169867
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.038672216810658314
Relative absolute error: 2.006162442210915
Root relative squared error: 7.734443362131663
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 45.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.010030812211054577
Coverage of cases: 100.0
Instances selection time: 19.0
Test time: 34.0
Accumulative iteration time: 106.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 26.0
		
Iteration: 6
Labeled set size: 912
Unlabelled set size: 3150
	
Mean region size: 53.75430822255047
Incorrectly Classified Instances: 0.049236829148202856
Correctly Classified Instances: 99.9507631708518
Weighted Precision: 0.9995080992947397
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.04652652954674866
Relative absolute error: 2.226959561277897
Root relative squared error: 9.305305909349732
Weighted TruePositiveRate: 0.999507631708518
Weighted MatthewsCorrelation: 0.999014440317274
Weighted FMeasure: 0.9995076228639044
Iteration time: 45.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.011134797806389485
Coverage of cases: 100.0
Instances selection time: 18.0
Test time: 24.0
Accumulative iteration time: 151.0
Weighted Recall: 0.999507631708518
Weighted FalsePositiveRate: 5.290821681706783E-4
Kappa statistic: 0.999013954653566
Training time: 27.0
		
Iteration: 7
Labeled set size: 932
Unlabelled set size: 3130
	
Mean region size: 53.64352535696701
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 0.9999895621744855
Root mean squared error: 0.047221937760569835
Relative absolute error: 2.2661964913809496
Root relative squared error: 9.444387552113968
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 17.0
Weighted AreaUnderPRC: 0.9999896385218656
Mean absolute error: 0.011330982456904748
Coverage of cases: 100.0
Instances selection time: 7.0
Test time: 15.0
Accumulative iteration time: 168.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 10.0
		
Iteration: 8
Labeled set size: 952
Unlabelled set size: 3110
	
Mean region size: 53.31117676021664
Incorrectly Classified Instances: 0.049236829148202856
Correctly Classified Instances: 99.9507631708518
Weighted Precision: 0.999508134125142
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0463650204006549
Relative absolute error: 2.2496167790192274
Root relative squared error: 9.27300408013098
Weighted TruePositiveRate: 0.999507631708518
Weighted MatthewsCorrelation: 0.9990145101369982
Weighted FMeasure: 0.9995076403134782
Iteration time: 19.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.011248083895096137
Coverage of cases: 100.0
Instances selection time: 8.0
Test time: 23.0
Accumulative iteration time: 187.0
Weighted Recall: 0.999507631708518
Weighted FalsePositiveRate: 4.5820205072329465E-4
Kappa statistic: 0.9990140245420991
Training time: 11.0
		
Iteration: 9
Labeled set size: 972
Unlabelled set size: 3090
	
Mean region size: 53.926637124569176
Incorrectly Classified Instances: 0.049236829148202856
Correctly Classified Instances: 99.9507631708518
Weighted Precision: 0.9995080992947397
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.048447288459871035
Relative absolute error: 2.4932580323654054
Root relative squared error: 9.689457691974207
Weighted TruePositiveRate: 0.999507631708518
Weighted MatthewsCorrelation: 0.999014440317274
Weighted FMeasure: 0.9995076228639044
Iteration time: 19.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.012466290161827027
Coverage of cases: 100.0
Instances selection time: 8.0
Test time: 14.0
Accumulative iteration time: 206.0
Weighted Recall: 0.999507631708518
Weighted FalsePositiveRate: 5.290821681706783E-4
Kappa statistic: 0.999013954653566
Training time: 11.0
		
Iteration: 10
Labeled set size: 992
Unlabelled set size: 3070
	
Mean region size: 54.529788281634666
Incorrectly Classified Instances: 0.17232890201870998
Correctly Classified Instances: 99.8276710979813
Weighted Precision: 0.9982824253441622
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.04387386647772411
Relative absolute error: 2.3911392881279863
Root relative squared error: 8.774773295544822
Weighted TruePositiveRate: 0.9982767109798129
Weighted MatthewsCorrelation: 0.9965544712751208
Weighted FMeasure: 0.9982765989627084
Iteration time: 20.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.01195569644063993
Coverage of cases: 100.0
Instances selection time: 8.0
Test time: 14.0
Accumulative iteration time: 226.0
Weighted Recall: 0.9982767109798129
Weighted FalsePositiveRate: 0.001851787588597374
Kappa statistic: 0.9965485354763794
Training time: 12.0
		
Iteration: 11
Labeled set size: 1012
Unlabelled set size: 3050
	
Mean region size: 54.172821270310195
Incorrectly Classified Instances: 0.19694731659281142
Correctly Classified Instances: 99.80305268340719
Weighted Precision: 0.9980385409669953
Weighted AreaUnderROC: 0.9999978153388459
Root mean squared error: 0.04951667857971257
Relative absolute error: 2.4156206838399674
Root relative squared error: 9.903335715942514
Weighted TruePositiveRate: 0.9980305268340719
Weighted MatthewsCorrelation: 0.9960642623803833
Weighted FMeasure: 0.9980306587632487
Iteration time: 19.0
Weighted AreaUnderPRC: 0.9999978176855923
Mean absolute error: 0.012078103419199836
Coverage of cases: 100.0
Instances selection time: 7.0
Test time: 15.0
Accumulative iteration time: 245.0
Weighted Recall: 0.9980305268340719
Weighted FalsePositiveRate: 0.0018328082028931786
Kappa statistic: 0.9960565174253001
Training time: 12.0
		
Iteration: 12
Labeled set size: 1032
Unlabelled set size: 3030
	
Mean region size: 53.545051698670605
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.04248824523354421
Relative absolute error: 2.0373606367037
Root relative squared error: 8.497649046708842
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 19.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0101868031835185
Coverage of cases: 100.0
Instances selection time: 8.0
Test time: 15.0
Accumulative iteration time: 264.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 11.0
		
Iteration: 13
Labeled set size: 1052
Unlabelled set size: 3010
	
Mean region size: 54.222058099458394
Incorrectly Classified Instances: 0.049236829148202856
Correctly Classified Instances: 99.9507631708518
Weighted Precision: 0.999507631708518
Weighted AreaUnderROC: 0.9999997572598719
Root mean squared error: 0.05119493043002329
Relative absolute error: 2.7159287021039904
Root relative squared error: 10.238986086004658
Weighted TruePositiveRate: 0.999507631708518
Weighted MatthewsCorrelation: 0.999013989599071
Weighted FMeasure: 0.999507631708518
Iteration time: 26.0
Weighted AreaUnderPRC: 0.9999997573796393
Mean absolute error: 0.013579643510519952
Coverage of cases: 100.0
Instances selection time: 13.0
Test time: 15.0
Accumulative iteration time: 290.0
Weighted Recall: 0.999507631708518
Weighted FalsePositiveRate: 4.936421094469865E-4
Kappa statistic: 0.999013989599071
Training time: 13.0
		
Iteration: 14
Labeled set size: 1072
Unlabelled set size: 2990
	
Mean region size: 53.50812407680945
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.03981650542800593
Relative absolute error: 1.9122505640971275
Root relative squared error: 7.963301085601186
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 19.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.009561252820485637
Coverage of cases: 100.0
Instances selection time: 7.0
Test time: 15.0
Accumulative iteration time: 309.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 12.0
		
Iteration: 15
Labeled set size: 1092
Unlabelled set size: 2970
	
Mean region size: 53.495814869522405
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.04242795163043683
Relative absolute error: 1.9921846427629533
Root relative squared error: 8.485590326087367
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 20.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.009960923213814766
Coverage of cases: 100.0
Instances selection time: 7.0
Test time: 15.0
Accumulative iteration time: 329.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 13.0
		
Iteration: 16
Labeled set size: 1112
Unlabelled set size: 2950
	
Mean region size: 52.79419005416051
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.042850461415691446
Relative absolute error: 1.8185484156431408
Root relative squared error: 8.570092283138289
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 22.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.009092742078215704
Coverage of cases: 100.0
Instances selection time: 6.0
Test time: 15.0
Accumulative iteration time: 351.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 16.0
		
Iteration: 17
Labeled set size: 1132
Unlabelled set size: 2930
	
Mean region size: 53.360413589364846
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.04783103332001929
Relative absolute error: 2.2003099491844744
Root relative squared error: 9.566206664003857
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 21.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.011001549745922372
Coverage of cases: 100.0
Instances selection time: 8.0
Test time: 15.0
Accumulative iteration time: 372.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 13.0
		
Iteration: 18
Labeled set size: 1152
Unlabelled set size: 2910
	
Mean region size: 53.64352535696701
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.041150756522575604
Relative absolute error: 1.993728418279048
Root relative squared error: 8.230151304515122
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 41.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.00996864209139524
Coverage of cases: 100.0
Instances selection time: 14.0
Test time: 30.0
Accumulative iteration time: 413.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 27.0
		
Iteration: 19
Labeled set size: 1172
Unlabelled set size: 2890
	
Mean region size: 54.72673559822747
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.04471551877780718
Relative absolute error: 2.5385021678398236
Root relative squared error: 8.943103755561436
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 19.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.012692510839199119
Coverage of cases: 100.0
Instances selection time: 6.0
Test time: 13.0
Accumulative iteration time: 432.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 13.0
		
Iteration: 20
Labeled set size: 1192
Unlabelled set size: 2870
	
Mean region size: 52.25258493353028
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.03000165031261664
Relative absolute error: 1.2553668230348531
Root relative squared error: 6.000330062523328
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 18.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.006276834115174266
Coverage of cases: 100.0
Instances selection time: 6.0
Test time: 13.0
Accumulative iteration time: 450.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 12.0
		
Iteration: 21
Labeled set size: 1212
Unlabelled set size: 2850
	
Mean region size: 53.77892663712457
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.04568138492720716
Relative absolute error: 2.3363803814644006
Root relative squared error: 9.136276985441432
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 19.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.011681901907322004
Coverage of cases: 100.0
Instances selection time: 6.0
Test time: 13.0
Accumulative iteration time: 469.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 13.0
		
Iteration: 22
Labeled set size: 1232
Unlabelled set size: 2830
	
Mean region size: 52.41260462826194
Incorrectly Classified Instances: 0.024618414574101428
Correctly Classified Instances: 99.9753815854259
Weighted Precision: 0.9997539415225314
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.04129894059194556
Relative absolute error: 1.4979924449778033
Root relative squared error: 8.259788118389112
Weighted TruePositiveRate: 0.9997538158542589
Weighted MatthewsCorrelation: 0.9995071249983162
Weighted FMeasure: 0.9997538180204761
Iteration time: 17.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0074899622248890164
Coverage of cases: 100.0
Instances selection time: 6.0
Test time: 12.0
Accumulative iteration time: 486.0
Weighted Recall: 0.9997538158542589
Weighted FalsePositiveRate: 2.2910102536164733E-4
Kappa statistic: 0.9995070035354472
Training time: 11.0
		
Iteration: 23
Labeled set size: 1252
Unlabelled set size: 2810
	
Mean region size: 55.2191038897095
Incorrectly Classified Instances: 0.12309207287050714
Correctly Classified Instances: 99.8769079271295
Weighted Precision: 0.9987722145762585
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.05490851845371128
Relative absolute error: 3.011107058094736
Root relative squared error: 10.981703690742256
Weighted TruePositiveRate: 0.9987690792712949
Weighted MatthewsCorrelation: 0.9975382225449091
Weighted FMeasure: 0.9987691319291265
Iteration time: 20.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.01505553529047368
Coverage of cases: 100.0
Instances selection time: 6.0
Test time: 13.0
Accumulative iteration time: 506.0
Weighted Recall: 0.9987690792712949
Weighted FalsePositiveRate: 0.0011455051268082367
Kappa statistic: 0.9975351923799943
Training time: 14.0
		
Iteration: 24
Labeled set size: 1272
Unlabelled set size: 2790
	
Mean region size: 53.10192023633678
Incorrectly Classified Instances: 0.024618414574101428
Correctly Classified Instances: 99.9753815854259
Weighted Precision: 0.9997539415225314
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.04141963159065148
Relative absolute error: 1.854815199168645
Root relative squared error: 8.283926318130295
Weighted TruePositiveRate: 0.9997538158542589
Weighted MatthewsCorrelation: 0.9995071249983162
Weighted FMeasure: 0.9997538180204761
Iteration time: 17.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.009274075995843225
Coverage of cases: 100.0
Instances selection time: 5.0
Test time: 12.0
Accumulative iteration time: 523.0
Weighted Recall: 0.9997538158542589
Weighted FalsePositiveRate: 2.2910102536164733E-4
Kappa statistic: 0.9995070035354472
Training time: 12.0
		
Iteration: 25
Labeled set size: 1292
Unlabelled set size: 2770
	
Mean region size: 54.197439684884294
Incorrectly Classified Instances: 0.049236829148202856
Correctly Classified Instances: 99.9507631708518
Weighted Precision: 0.999508134125142
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.04301941306777177
Relative absolute error: 2.082333653793653
Root relative squared error: 8.603882613554353
Weighted TruePositiveRate: 0.999507631708518
Weighted MatthewsCorrelation: 0.9990145101369982
Weighted FMeasure: 0.9995076403134782
Iteration time: 19.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.010411668268968267
Coverage of cases: 100.0
Instances selection time: 5.0
Test time: 13.0
Accumulative iteration time: 542.0
Weighted Recall: 0.999507631708518
Weighted FalsePositiveRate: 4.5820205072329465E-4
Kappa statistic: 0.9990140245420991
Training time: 14.0
		
Iteration: 26
Labeled set size: 1312
Unlabelled set size: 2750
	
Mean region size: 54.02511078286558
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.045890793946962945
Relative absolute error: 2.336383504140772
Root relative squared error: 9.178158789392588
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 18.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.011681917520703858
Coverage of cases: 100.0
Instances selection time: 6.0
Test time: 12.0
Accumulative iteration time: 560.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 12.0
		
Iteration: 27
Labeled set size: 1332
Unlabelled set size: 2730
	
Mean region size: 54.222058099458394
Incorrectly Classified Instances: 0.024618414574101428
Correctly Classified Instances: 99.9753815854259
Weighted Precision: 0.9997539415225314
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.04180098494227039
Relative absolute error: 2.176134102961341
Root relative squared error: 8.360196988454078
Weighted TruePositiveRate: 0.9997538158542589
Weighted MatthewsCorrelation: 0.9995071249983162
Weighted FMeasure: 0.9997538180204761
Iteration time: 20.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.010880670514806704
Coverage of cases: 100.0
Instances selection time: 6.0
Test time: 13.0
Accumulative iteration time: 580.0
Weighted Recall: 0.9997538158542589
Weighted FalsePositiveRate: 2.2910102536164733E-4
Kappa statistic: 0.9995070035354472
Training time: 14.0
		
Iteration: 28
Labeled set size: 1352
Unlabelled set size: 2710
	
Mean region size: 53.23732151649434
Incorrectly Classified Instances: 0.22156573116691286
Correctly Classified Instances: 99.77843426883308
Weighted Precision: 0.9977944804187858
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.048239406033513364
Relative absolute error: 1.9491537138645223
Root relative squared error: 9.647881206702673
Weighted TruePositiveRate: 0.9977843426883308
Weighted MatthewsCorrelation: 0.9955734577461355
Weighted FMeasure: 0.9977845084484809
Iteration time: 18.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.009745768569322612
Coverage of cases: 100.0
Instances selection time: 5.0
Test time: 12.0
Accumulative iteration time: 598.0
Weighted Recall: 0.9977843426883308
Weighted FalsePositiveRate: 0.002061909228254826
Kappa statistic: 0.9955636607043825
Training time: 13.0
		
Iteration: 29
Labeled set size: 1372
Unlabelled set size: 2690
	
Mean region size: 53.29886755292959
Incorrectly Classified Instances: 0.049236829148202856
Correctly Classified Instances: 99.9507631708518
Weighted Precision: 0.999508134125142
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.04734806494604383
Relative absolute error: 2.0910929929547755
Root relative squared error: 9.469612989208766
Weighted TruePositiveRate: 0.999507631708518
Weighted MatthewsCorrelation: 0.9990145101369982
Weighted FMeasure: 0.9995076403134782
Iteration time: 18.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.010455464964773878
Coverage of cases: 100.0
Instances selection time: 5.0
Test time: 13.0
Accumulative iteration time: 616.0
Weighted Recall: 0.999507631708518
Weighted FalsePositiveRate: 4.5820205072329465E-4
Kappa statistic: 0.9990140245420991
Training time: 13.0
		
Iteration: 30
Labeled set size: 1392
Unlabelled set size: 2670
	
Mean region size: 54.07434761201379
Incorrectly Classified Instances: 0.12309207287050714
Correctly Classified Instances: 99.8769079271295
Weighted Precision: 0.9987722145762585
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.05331625242576296
Relative absolute error: 2.482138243251455
Root relative squared error: 10.663250485152592
Weighted TruePositiveRate: 0.9987690792712949
Weighted MatthewsCorrelation: 0.9975382225449091
Weighted FMeasure: 0.9987691319291265
Iteration time: 20.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.012410691216257276
Coverage of cases: 100.0
Instances selection time: 6.0
Test time: 12.0
Accumulative iteration time: 636.0
Weighted Recall: 0.9987690792712949
Weighted FalsePositiveRate: 0.0011455051268082367
Kappa statistic: 0.9975351923799943
Training time: 14.0
		
Iteration: 31
Labeled set size: 1412
Unlabelled set size: 2650
	
Mean region size: 54.14820285573609
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.048311789766250744
Relative absolute error: 2.5662477925074683
Root relative squared error: 9.66235795325015
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 21.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.012831238962537342
Coverage of cases: 100.0
Instances selection time: 6.0
Test time: 12.0
Accumulative iteration time: 657.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 15.0
		
Iteration: 32
Labeled set size: 1432
Unlabelled set size: 2630
	
Mean region size: 51.563269325455444
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.03340055436084001
Relative absolute error: 1.1052076931980865
Root relative squared error: 6.680110872168002
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 16.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0055260384659904325
Coverage of cases: 100.0
Instances selection time: 5.0
Test time: 12.0
Accumulative iteration time: 673.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 11.0
		
Iteration: 33
Labeled set size: 1452
Unlabelled set size: 2610
	
Mean region size: 52.646479566715904
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.028578849035665144
Relative absolute error: 1.3382155444169253
Root relative squared error: 5.715769807133029
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 18.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.006691077722084626
Coverage of cases: 100.0
Instances selection time: 5.0
Test time: 11.0
Accumulative iteration time: 691.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 13.0
		
Iteration: 34
Labeled set size: 1472
Unlabelled set size: 2590
	
Mean region size: 53.028064992614475
Incorrectly Classified Instances: 0.049236829148202856
Correctly Classified Instances: 99.9507631708518
Weighted Precision: 0.9995080992947397
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.04565095427908222
Relative absolute error: 2.063285603930062
Root relative squared error: 9.130190855816444
Weighted TruePositiveRate: 0.999507631708518
Weighted MatthewsCorrelation: 0.999014440317274
Weighted FMeasure: 0.9995076228639044
Iteration time: 20.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.01031642801965031
Coverage of cases: 100.0
Instances selection time: 6.0
Test time: 12.0
Accumulative iteration time: 711.0
Weighted Recall: 0.999507631708518
Weighted FalsePositiveRate: 5.290821681706783E-4
Kappa statistic: 0.999013954653566
Training time: 14.0
		
Iteration: 35
Labeled set size: 1492
Unlabelled set size: 2570
	
Mean region size: 52.92959133431807
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.03857101626145642
Relative absolute error: 1.5924438234306522
Root relative squared error: 7.714203252291283
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 20.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.00796221911715326
Coverage of cases: 100.0
Instances selection time: 6.0
Test time: 12.0
Accumulative iteration time: 731.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 14.0
		
Iteration: 36
Labeled set size: 1512
Unlabelled set size: 2550
	
Mean region size: 53.77892663712457
Incorrectly Classified Instances: 0.2708025603151157
Correctly Classified Instances: 99.72919743968488
Weighted Precision: 0.9973071030315033
Weighted AreaUnderROC: 0.9999973298585894
Root mean squared error: 0.05340825620977274
Relative absolute error: 2.3015798239920255
Root relative squared error: 10.681651241954548
Weighted TruePositiveRate: 0.9972919743968488
Weighted MatthewsCorrelation: 0.9945926193209534
Weighted FMeasure: 0.9972922183909625
Iteration time: 19.0
Weighted AreaUnderPRC: 0.9999973332823716
Mean absolute error: 0.011507899119960127
Coverage of cases: 100.0
Instances selection time: 5.0
Test time: 13.0
Accumulative iteration time: 750.0
Weighted Recall: 0.9972919743968488
Weighted FalsePositiveRate: 0.0025201112789781203
Kappa statistic: 0.9945779996529491
Training time: 14.0
		
Iteration: 37
Labeled set size: 1532
Unlabelled set size: 2530
	
Mean region size: 52.72033481043821
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.03275327890982239
Relative absolute error: 1.3119666451093799
Root relative squared error: 6.550655781964478
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 20.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0065598332255469
Coverage of cases: 100.0
Instances selection time: 5.0
Test time: 15.0
Accumulative iteration time: 770.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 15.0
		
Iteration: 38
Labeled set size: 1552
Unlabelled set size: 2510
	
Mean region size: 53.409650418513046
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.04464929074343161
Relative absolute error: 2.075784443559762
Root relative squared error: 8.929858148686321
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 19.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.010378922217798809
Coverage of cases: 100.0
Instances selection time: 5.0
Test time: 12.0
Accumulative iteration time: 789.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 14.0
		
Iteration: 39
Labeled set size: 1572
Unlabelled set size: 2490
	
Mean region size: 52.732644017725256
Incorrectly Classified Instances: 0.41851304775972425
Correctly Classified Instances: 99.58148695224027
Weighted Precision: 0.9958508934303113
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.05699707658537309
Relative absolute error: 1.9623297766935117
Root relative squared error: 11.399415317074618
Weighted TruePositiveRate: 0.9958148695224027
Weighted MatthewsCorrelation: 0.991656243469729
Weighted FMeasure: 0.9958154263311211
Iteration time: 22.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.009811648883467559
Coverage of cases: 100.0
Instances selection time: 5.0
Test time: 14.0
Accumulative iteration time: 811.0
Weighted Recall: 0.9958148695224027
Weighted FalsePositiveRate: 0.0038947174311480042
Kappa statistic: 0.9916214355550389
Training time: 17.0
		
Iteration: 40
Labeled set size: 1592
Unlabelled set size: 2470
	
Mean region size: 53.003446578040375
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.03281088263530633
Relative absolute error: 1.445764017049386
Root relative squared error: 6.562176527061266
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 22.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.00722882008524693
Coverage of cases: 100.0
Instances selection time: 6.0
Test time: 13.0
Accumulative iteration time: 833.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 16.0
		
Iteration: 41
Labeled set size: 1612
Unlabelled set size: 2450
	
Mean region size: 53.4465780403742
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0357500738476769
Relative absolute error: 1.6493322363827445
Root relative squared error: 7.15001476953538
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 22.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.008246661181913723
Coverage of cases: 100.0
Instances selection time: 5.0
Test time: 13.0
Accumulative iteration time: 855.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 17.0
		
Iteration: 42
Labeled set size: 1632
Unlabelled set size: 2430
	
Mean region size: 53.32348596750369
Incorrectly Classified Instances: 0.14771048744460857
Correctly Classified Instances: 99.85228951255539
Weighted Precision: 0.9985274076659035
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.044861314623655106
Relative absolute error: 1.9291851643730702
Root relative squared error: 8.972262924731021
Weighted TruePositiveRate: 0.9985228951255539
Weighted MatthewsCorrelation: 0.9970466443977584
Weighted FMeasure: 0.9985229704137654
Iteration time: 22.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.009645925821865351
Coverage of cases: 100.0
Instances selection time: 5.0
Test time: 12.0
Accumulative iteration time: 877.0
Weighted Recall: 0.9985228951255539
Weighted FalsePositiveRate: 0.0013746061521698841
Kappa statistic: 0.9970422832621776
Training time: 17.0
		
Iteration: 43
Labeled set size: 1652
Unlabelled set size: 2410
	
Mean region size: 54.554406696208765
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992625774237212
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.05105810910959649
Relative absolute error: 2.5695712789434877
Root relative squared error: 10.211621821919298
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985221550307092
Weighted FMeasure: 0.9992614667891501
Iteration time: 20.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.012847856394717439
Coverage of cases: 100.0
Instances selection time: 5.0
Test time: 12.0
Accumulative iteration time: 897.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 6.873030760849421E-4
Kappa statistic: 0.9985210630190265
Training time: 15.0
		
Iteration: 44
Labeled set size: 1672
Unlabelled set size: 2390
	
Mean region size: 52.90497291974397
Incorrectly Classified Instances: 0.12309207287050714
Correctly Classified Instances: 99.8769079271295
Weighted Precision: 0.9987722145762585
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.050493784759456525
Relative absolute error: 2.1022962133439935
Root relative squared error: 10.098756951891305
Weighted TruePositiveRate: 0.9987690792712949
Weighted MatthewsCorrelation: 0.9975382225449091
Weighted FMeasure: 0.9987691319291265
Iteration time: 21.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.010511481066719968
Coverage of cases: 100.0
Instances selection time: 5.0
Test time: 13.0
Accumulative iteration time: 918.0
Weighted Recall: 0.9987690792712949
Weighted FalsePositiveRate: 0.0011455051268082367
Kappa statistic: 0.9975351923799943
Training time: 16.0
		
Iteration: 45
Labeled set size: 1692
Unlabelled set size: 2370
	
Mean region size: 54.45593303791236
Incorrectly Classified Instances: 0.024618414574101428
Correctly Classified Instances: 99.9753815854259
Weighted Precision: 0.9997539415225314
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.048479632492388916
Relative absolute error: 2.481066841185974
Root relative squared error: 9.695926498477784
Weighted TruePositiveRate: 0.9997538158542589
Weighted MatthewsCorrelation: 0.9995071249983162
Weighted FMeasure: 0.9997538180204761
Iteration time: 23.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.012405334205929871
Coverage of cases: 100.0
Instances selection time: 5.0
Test time: 13.0
Accumulative iteration time: 941.0
Weighted Recall: 0.9997538158542589
Weighted FalsePositiveRate: 2.2910102536164733E-4
Kappa statistic: 0.9995070035354472
Training time: 18.0
		
Iteration: 46
Labeled set size: 1712
Unlabelled set size: 2350
	
Mean region size: 53.01575578532743
Incorrectly Classified Instances: 0.2708025603151157
Correctly Classified Instances: 99.72919743968488
Weighted Precision: 0.9973071030315033
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.047913775274791605
Relative absolute error: 1.939199178140149
Root relative squared error: 9.582755054958321
Weighted TruePositiveRate: 0.9972919743968488
Weighted MatthewsCorrelation: 0.9945926193209534
Weighted FMeasure: 0.9972922183909625
Iteration time: 22.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.009695995890700745
Coverage of cases: 100.0
Instances selection time: 5.0
Test time: 13.0
Accumulative iteration time: 963.0
Weighted Recall: 0.9972919743968488
Weighted FalsePositiveRate: 0.0025201112789781203
Kappa statistic: 0.9945779996529491
Training time: 17.0
		
Time end:Wed Nov 01 07.28.21 EET 2017