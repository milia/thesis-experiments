Wed Nov 01 06.10.15 EET 2017
Dataset: mushroom
Test set size: 4062
Initial Labelled set size: 812
Initial Unlabelled set size: 3250
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Wed Nov 01 06.10.15 EET 2017
		
Iteration: 1
Labeled set size: 812
Unlabelled set size: 3250
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 8.558740007294249E-4
Relative absolute error: 0.14880386397377834
Root relative squared error: 0.171174800145885
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 734.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 7.440193198688917E-4
Coverage of cases: 100.0
Instances selection time: 734.0
Test time: 919.0
Accumulative iteration time: 734.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 2
Labeled set size: 832
Unlabelled set size: 3230
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 8.333297477650232E-4
Relative absolute error: 0.14483066096609315
Root relative squared error: 0.16666594955300465
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 738.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 7.241533048304657E-4
Coverage of cases: 100.0
Instances selection time: 738.0
Test time: 932.0
Accumulative iteration time: 1472.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 3
Labeled set size: 852
Unlabelled set size: 3210
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 8.140779857211216E-4
Relative absolute error: 0.14163233468183203
Root relative squared error: 0.1628155971442243
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 748.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 7.081616734091602E-4
Coverage of cases: 100.0
Instances selection time: 748.0
Test time: 946.0
Accumulative iteration time: 2220.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 4
Labeled set size: 872
Unlabelled set size: 3190
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 7.945520854568596E-4
Relative absolute error: 0.13821778994844053
Root relative squared error: 0.1589104170913719
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 758.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 6.910889497422026E-4
Coverage of cases: 100.0
Instances selection time: 758.0
Test time: 957.0
Accumulative iteration time: 2978.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 5
Labeled set size: 892
Unlabelled set size: 3170
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 7.745129064246543E-4
Relative absolute error: 0.13460161504272122
Root relative squared error: 0.15490258128493087
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 759.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 6.730080752136061E-4
Coverage of cases: 100.0
Instances selection time: 759.0
Test time: 967.0
Accumulative iteration time: 3737.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 6
Labeled set size: 912
Unlabelled set size: 3150
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 7.520563096718797E-4
Relative absolute error: 0.13056526392987908
Root relative squared error: 0.15041126193437593
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 765.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 6.528263196493954E-4
Coverage of cases: 100.0
Instances selection time: 765.0
Test time: 980.0
Accumulative iteration time: 4502.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 7
Labeled set size: 932
Unlabelled set size: 3130
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 7.346540060597004E-4
Relative absolute error: 0.12749770714475786
Root relative squared error: 0.14693080121194008
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 773.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 6.374885357237893E-4
Coverage of cases: 100.0
Instances selection time: 773.0
Test time: 992.0
Accumulative iteration time: 5275.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 8
Labeled set size: 952
Unlabelled set size: 3110
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 7.162332190743478E-4
Relative absolute error: 0.12425052809344123
Root relative squared error: 0.14324664381486957
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 779.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 6.212526404672062E-4
Coverage of cases: 100.0
Instances selection time: 779.0
Test time: 1015.0
Accumulative iteration time: 6054.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 9
Labeled set size: 972
Unlabelled set size: 3090
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 6.983418121874697E-4
Relative absolute error: 0.12108365835978457
Root relative squared error: 0.13966836243749395
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 791.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 6.054182917989228E-4
Coverage of cases: 100.0
Instances selection time: 791.0
Test time: 1029.0
Accumulative iteration time: 6845.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 10
Labeled set size: 992
Unlabelled set size: 3070
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 6.81713713390978E-4
Relative absolute error: 0.11811556539657862
Root relative squared error: 0.1363427426781956
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 790.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 5.905778269828931E-4
Coverage of cases: 100.0
Instances selection time: 790.0
Test time: 1030.0
Accumulative iteration time: 7635.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 11
Labeled set size: 1012
Unlabelled set size: 3050
	
Mean region size: 50.01230920728705
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0018675143116154568
Relative absolute error: 0.12090303454788305
Root relative squared error: 0.3735028623230914
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 797.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 6.045151727394152E-4
Coverage of cases: 100.0
Instances selection time: 797.0
Test time: 1039.0
Accumulative iteration time: 8432.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 12
Labeled set size: 1032
Unlabelled set size: 3030
	
Mean region size: 50.01230920728705
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0018615073400608422
Relative absolute error: 0.11784820721093178
Root relative squared error: 0.37230146801216846
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 802.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 5.892410360546589E-4
Coverage of cases: 100.0
Instances selection time: 802.0
Test time: 1055.0
Accumulative iteration time: 9234.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 13
Labeled set size: 1052
Unlabelled set size: 3010
	
Mean region size: 50.01230920728705
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.00185637914077751
Relative absolute error: 0.11529620796563501
Root relative squared error: 0.37127582815550203
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 804.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 5.76481039828175E-4
Coverage of cases: 100.0
Instances selection time: 804.0
Test time: 1066.0
Accumulative iteration time: 10038.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 14
Labeled set size: 1072
Unlabelled set size: 2990
	
Mean region size: 50.01230920728705
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0018507732604910816
Relative absolute error: 0.11235782394630003
Root relative squared error: 0.3701546520982163
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 810.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 5.617891197315001E-4
Coverage of cases: 100.0
Instances selection time: 810.0
Test time: 1077.0
Accumulative iteration time: 10848.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 15
Labeled set size: 1092
Unlabelled set size: 2970
	
Mean region size: 50.01230920728705
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0018463985980469268
Relative absolute error: 0.11009935698304305
Root relative squared error: 0.3692797196093854
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 816.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 5.504967849152152E-4
Coverage of cases: 100.0
Instances selection time: 816.0
Test time: 1088.0
Accumulative iteration time: 11664.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 16
Labeled set size: 1112
Unlabelled set size: 2950
	
Mean region size: 50.01230920728705
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0018424393022485172
Relative absolute error: 0.10798248939259494
Root relative squared error: 0.36848786044970344
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 821.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 5.399124469629747E-4
Coverage of cases: 100.0
Instances selection time: 821.0
Test time: 1102.0
Accumulative iteration time: 12485.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 17
Labeled set size: 1132
Unlabelled set size: 2930
	
Mean region size: 50.01230920728705
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0018374768875296919
Relative absolute error: 0.10515617594220537
Root relative squared error: 0.36749537750593836
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 824.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 5.257808797110269E-4
Coverage of cases: 100.0
Instances selection time: 824.0
Test time: 1118.0
Accumulative iteration time: 13309.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 18
Labeled set size: 1152
Unlabelled set size: 2910
	
Mean region size: 50.01230920728705
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0018333855666802564
Relative absolute error: 0.10283072068615093
Root relative squared error: 0.3666771133360513
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 829.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 5.141536034307546E-4
Coverage of cases: 100.0
Instances selection time: 829.0
Test time: 1144.0
Accumulative iteration time: 14138.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 19
Labeled set size: 1172
Unlabelled set size: 2890
	
Mean region size: 50.01230920728705
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0018293703510040107
Relative absolute error: 0.10046439746525945
Root relative squared error: 0.36587407020080215
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 833.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 5.023219873262972E-4
Coverage of cases: 100.0
Instances selection time: 833.0
Test time: 1141.0
Accumulative iteration time: 14971.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 20
Labeled set size: 1192
Unlabelled set size: 2870
	
Mean region size: 50.01230920728705
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0018259937601579129
Relative absolute error: 0.09842155562311665
Root relative squared error: 0.3651987520315826
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 838.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.921077781155832E-4
Coverage of cases: 100.0
Instances selection time: 838.0
Test time: 1151.0
Accumulative iteration time: 15809.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 21
Labeled set size: 1212
Unlabelled set size: 2850
	
Mean region size: 50.01230920728705
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.001822800645817096
Relative absolute error: 0.09649881185369476
Root relative squared error: 0.36456012916341923
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 842.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.824940592684738E-4
Coverage of cases: 100.0
Instances selection time: 842.0
Test time: 1164.0
Accumulative iteration time: 16651.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 22
Labeled set size: 1232
Unlabelled set size: 2830
	
Mean region size: 50.01230920728705
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0018194095137852402
Relative absolute error: 0.0943260599117753
Root relative squared error: 0.363881902757048
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 846.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.716302995588765E-4
Coverage of cases: 100.0
Instances selection time: 846.0
Test time: 1174.0
Accumulative iteration time: 17497.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 23
Labeled set size: 1252
Unlabelled set size: 2810
	
Mean region size: 50.01230920728705
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0018162564982393715
Relative absolute error: 0.09234461073588343
Root relative squared error: 0.3632512996478743
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 848.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.617230536794172E-4
Coverage of cases: 100.0
Instances selection time: 848.0
Test time: 1186.0
Accumulative iteration time: 18345.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 24
Labeled set size: 1272
Unlabelled set size: 2790
	
Mean region size: 50.01230920728705
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0018134962645106121
Relative absolute error: 0.09054491434818132
Root relative squared error: 0.3626992529021224
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 851.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.527245717409066E-4
Coverage of cases: 100.0
Instances selection time: 851.0
Test time: 1196.0
Accumulative iteration time: 19196.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 25
Labeled set size: 1292
Unlabelled set size: 2770
	
Mean region size: 50.01230920728705
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.001810662772014504
Relative absolute error: 0.0886830493257364
Root relative squared error: 0.3621325544029008
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 856.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.43415246628682E-4
Coverage of cases: 100.0
Instances selection time: 856.0
Test time: 1214.0
Accumulative iteration time: 20052.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 26
Labeled set size: 1312
Unlabelled set size: 2750
	
Mean region size: 50.01230920728705
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.001807694921765653
Relative absolute error: 0.08661893584604692
Root relative squared error: 0.3615389843531306
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 857.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.330946792302346E-4
Coverage of cases: 100.0
Instances selection time: 857.0
Test time: 1227.0
Accumulative iteration time: 20909.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 27
Labeled set size: 1332
Unlabelled set size: 2730
	
Mean region size: 50.01230920728705
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0018051697547859372
Relative absolute error: 0.08484853027382756
Root relative squared error: 0.36103395095718743
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 860.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.2424265136913785E-4
Coverage of cases: 100.0
Instances selection time: 860.0
Test time: 1239.0
Accumulative iteration time: 21769.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 28
Labeled set size: 1352
Unlabelled set size: 2710
	
Mean region size: 50.01230920728705
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.001802985257250347
Relative absolute error: 0.08326800275613325
Root relative squared error: 0.3605970514500694
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 872.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.1634001378066624E-4
Coverage of cases: 100.0
Instances selection time: 872.0
Test time: 1270.0
Accumulative iteration time: 22641.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 29
Labeled set size: 1372
Unlabelled set size: 2690
	
Mean region size: 50.01230920728705
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0018008491276882033
Relative absolute error: 0.08171575420528485
Root relative squared error: 0.36016982553764065
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 872.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.0857877102642423E-4
Coverage of cases: 100.0
Instances selection time: 872.0
Test time: 1262.0
Accumulative iteration time: 23513.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 30
Labeled set size: 1392
Unlabelled set size: 2670
	
Mean region size: 50.01230920728705
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0017986086251948622
Relative absolute error: 0.08012323149126432
Root relative squared error: 0.35972172503897243
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 872.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.0061615745632165E-4
Coverage of cases: 100.0
Instances selection time: 872.0
Test time: 1274.0
Accumulative iteration time: 24385.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 31
Labeled set size: 1412
Unlabelled set size: 2650
	
Mean region size: 50.01230920728705
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.001796427238700369
Relative absolute error: 0.07847185022174301
Root relative squared error: 0.35928544774007376
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 873.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.9235925110871504E-4
Coverage of cases: 100.0
Instances selection time: 873.0
Test time: 1287.0
Accumulative iteration time: 25258.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 32
Labeled set size: 1432
Unlabelled set size: 2630
	
Mean region size: 50.01230920728705
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0017945128476476717
Relative absolute error: 0.07699803031645712
Root relative squared error: 0.35890256952953437
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 876.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.849901515822856E-4
Coverage of cases: 100.0
Instances selection time: 876.0
Test time: 1299.0
Accumulative iteration time: 26134.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 33
Labeled set size: 1452
Unlabelled set size: 2610
	
Mean region size: 50.01230920728705
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.001792909408348192
Relative absolute error: 0.0757928538221603
Root relative squared error: 0.3585818816696384
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 878.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.7896426911080154E-4
Coverage of cases: 100.0
Instances selection time: 878.0
Test time: 1311.0
Accumulative iteration time: 27012.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 34
Labeled set size: 1472
Unlabelled set size: 2590
	
Mean region size: 50.01230920728705
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0017913431269013277
Relative absolute error: 0.07462519224128701
Root relative squared error: 0.35826862538026555
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 878.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.7312596120643506E-4
Coverage of cases: 100.0
Instances selection time: 878.0
Test time: 1321.0
Accumulative iteration time: 27890.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 35
Labeled set size: 1492
Unlabelled set size: 2570
	
Mean region size: 50.01230920728705
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0017896257991893009
Relative absolute error: 0.07336651488303425
Root relative squared error: 0.35792515983786016
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 881.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.668325744151713E-4
Coverage of cases: 100.0
Instances selection time: 881.0
Test time: 1337.0
Accumulative iteration time: 28771.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 36
Labeled set size: 1512
Unlabelled set size: 2550
	
Mean region size: 50.01230920728705
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0017881598719299392
Relative absolute error: 0.07228320776445164
Root relative squared error: 0.3576319743859878
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 884.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.614160388222582E-4
Coverage of cases: 100.0
Instances selection time: 884.0
Test time: 1359.0
Accumulative iteration time: 29655.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 37
Labeled set size: 1532
Unlabelled set size: 2530
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.8687295496800443E-4
Relative absolute error: 0.06569358123727968
Root relative squared error: 0.07737459099360089
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 887.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.284679061863984E-4
Coverage of cases: 100.0
Instances selection time: 887.0
Test time: 1367.0
Accumulative iteration time: 30542.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 38
Labeled set size: 1552
Unlabelled set size: 2510
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.79047483118243E-4
Relative absolute error: 0.0643501948398631
Root relative squared error: 0.0758094966236486
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 878.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.217509741993155E-4
Coverage of cases: 100.0
Instances selection time: 878.0
Test time: 1376.0
Accumulative iteration time: 31420.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 39
Labeled set size: 1572
Unlabelled set size: 2490
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.731946099912345E-4
Relative absolute error: 0.0634011030368892
Root relative squared error: 0.0746389219982469
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 880.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.17005515184446E-4
Coverage of cases: 100.0
Instances selection time: 880.0
Test time: 1389.0
Accumulative iteration time: 32300.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 40
Labeled set size: 1592
Unlabelled set size: 2470
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.6662087888033674E-4
Relative absolute error: 0.06225033053564314
Root relative squared error: 0.07332417577606734
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 880.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.112516526782157E-4
Coverage of cases: 100.0
Instances selection time: 879.0
Test time: 1398.0
Accumulative iteration time: 33180.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 41
Labeled set size: 1612
Unlabelled set size: 2450
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.5986364436105635E-4
Relative absolute error: 0.06109213113097879
Root relative squared error: 0.07197272887221128
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 880.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.0546065565489395E-4
Coverage of cases: 100.0
Instances selection time: 880.0
Test time: 1411.0
Accumulative iteration time: 34060.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 42
Labeled set size: 1632
Unlabelled set size: 2430
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.5301070570333654E-4
Relative absolute error: 0.05996227998623732
Root relative squared error: 0.0706021411406673
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 889.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 2.998113999311866E-4
Coverage of cases: 100.0
Instances selection time: 889.0
Test time: 1424.0
Accumulative iteration time: 34949.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 43
Labeled set size: 1652
Unlabelled set size: 2410
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.4715641692762664E-4
Relative absolute error: 0.05900502282686788
Root relative squared error: 0.06943128338552533
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 876.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 2.950251141343394E-4
Coverage of cases: 100.0
Instances selection time: 876.0
Test time: 1436.0
Accumulative iteration time: 35825.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 44
Labeled set size: 1672
Unlabelled set size: 2390
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.4172367656803645E-4
Relative absolute error: 0.058125005523520734
Root relative squared error: 0.0683447353136073
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 879.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 2.9062502761760366E-4
Coverage of cases: 100.0
Instances selection time: 879.0
Test time: 1461.0
Accumulative iteration time: 36704.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 45
Labeled set size: 1692
Unlabelled set size: 2370
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.3484503969219946E-4
Relative absolute error: 0.056954243890588986
Root relative squared error: 0.06696900793843989
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 880.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 2.847712194529449E-4
Coverage of cases: 100.0
Instances selection time: 880.0
Test time: 1473.0
Accumulative iteration time: 37584.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 46
Labeled set size: 1712
Unlabelled set size: 2350
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.272274039070082E-4
Relative absolute error: 0.05564979685125934
Root relative squared error: 0.06544548078140164
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 872.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 2.7824898425629673E-4
Coverage of cases: 100.0
Instances selection time: 872.0
Test time: 1478.0
Accumulative iteration time: 38456.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Time end:Wed Nov 01 06.11.51 EET 2017