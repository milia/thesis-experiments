Wed Nov 01 05.24.06 EET 2017
Dataset: mushroom
Test set size: 4062
Initial Labelled set size: 812
Initial Unlabelled set size: 3250
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Nov 01 05.24.06 EET 2017
		
Iteration: 1
Labeled set size: 812
Unlabelled set size: 3250
	
Mean region size: 50.13540128015756
Incorrectly Classified Instances: 0.19694731659281142
Correctly Classified Instances: 99.80305268340719
Weighted Precision: 0.9980379869597004
Weighted AreaUnderROC: 0.9986280327951623
Root mean squared error: 0.046120431936861286
Relative absolute error: 0.6871523980165588
Root relative squared error: 9.224086387372257
Weighted TruePositiveRate: 0.9980305268340719
Weighted MatthewsCorrelation: 0.9960631485481362
Weighted FMeasure: 0.9980303795670112
Iteration time: 735.0
Weighted AreaUnderPRC: 0.9982157356147234
Mean absolute error: 0.003435761990082794
Coverage of cases: 99.8276710979813
Instances selection time: 734.0
Test time: 889.0
Accumulative iteration time: 735.0
Weighted Recall: 0.9980305268340719
Weighted FalsePositiveRate: 0.002116328672682713
Kappa statistic: 0.9960553992087492
Training time: 1.0
		
Iteration: 2
Labeled set size: 832
Unlabelled set size: 3230
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 8.338762051397591E-4
Relative absolute error: 0.14516451250080803
Root relative squared error: 0.16677524102795183
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 729.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 7.258225625040401E-4
Coverage of cases: 100.0
Instances selection time: 728.0
Test time: 906.0
Accumulative iteration time: 1464.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 3
Labeled set size: 852
Unlabelled set size: 3210
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 8.10847762563876E-4
Relative absolute error: 0.14111427450935163
Root relative squared error: 0.1621695525127752
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 732.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 7.055713725467582E-4
Coverage of cases: 100.0
Instances selection time: 732.0
Test time: 923.0
Accumulative iteration time: 2196.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 4
Labeled set size: 872
Unlabelled set size: 3190
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 7.889138611535618E-4
Relative absolute error: 0.13721302142591232
Root relative squared error: 0.15778277223071235
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 734.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 6.860651071295616E-4
Coverage of cases: 100.0
Instances selection time: 734.0
Test time: 937.0
Accumulative iteration time: 2930.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 5
Labeled set size: 892
Unlabelled set size: 3170
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 7.695513692034319E-4
Relative absolute error: 0.13393352144045453
Root relative squared error: 0.15391027384068637
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 743.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 6.696676072022726E-4
Coverage of cases: 100.0
Instances selection time: 743.0
Test time: 959.0
Accumulative iteration time: 3673.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 6
Labeled set size: 912
Unlabelled set size: 3150
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 7.513250559733163E-4
Relative absolute error: 0.13074835666668227
Root relative squared error: 0.15026501119466326
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 750.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 6.537417833334114E-4
Coverage of cases: 100.0
Instances selection time: 749.0
Test time: 964.0
Accumulative iteration time: 4423.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 7
Labeled set size: 932
Unlabelled set size: 3130
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 7.309368526656007E-4
Relative absolute error: 0.1270208114380546
Root relative squared error: 0.14618737053312014
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 764.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 6.35104057190273E-4
Coverage of cases: 100.0
Instances selection time: 764.0
Test time: 989.0
Accumulative iteration time: 5187.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 8
Labeled set size: 952
Unlabelled set size: 3110
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 7.130834354688234E-4
Relative absolute error: 0.12388568656414066
Root relative squared error: 0.14261668709376468
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 772.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 6.194284328207033E-4
Coverage of cases: 100.0
Instances selection time: 772.0
Test time: 1001.0
Accumulative iteration time: 5959.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 9
Labeled set size: 972
Unlabelled set size: 3090
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 6.968633190549626E-4
Relative absolute error: 0.12112008945990636
Root relative squared error: 0.13937266381099253
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 777.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 6.056004472995318E-4
Coverage of cases: 100.0
Instances selection time: 777.0
Test time: 1017.0
Accumulative iteration time: 6736.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 10
Labeled set size: 992
Unlabelled set size: 3070
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 6.823252324045993E-4
Relative absolute error: 0.11869315745794097
Root relative squared error: 0.13646504648091987
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 782.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 5.934657872897048E-4
Coverage of cases: 100.0
Instances selection time: 782.0
Test time: 1028.0
Accumulative iteration time: 7518.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 11
Labeled set size: 1012
Unlabelled set size: 3050
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 6.663778644520355E-4
Relative absolute error: 0.11595847254633818
Root relative squared error: 0.1332755728904071
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 782.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 5.797923627316909E-4
Coverage of cases: 100.0
Instances selection time: 782.0
Test time: 1028.0
Accumulative iteration time: 8300.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 12
Labeled set size: 1032
Unlabelled set size: 3030
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 6.511032277791772E-4
Relative absolute error: 0.11332531205816541
Root relative squared error: 0.13022064555583546
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 789.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 5.666265602908271E-4
Coverage of cases: 100.0
Instances selection time: 789.0
Test time: 1042.0
Accumulative iteration time: 9089.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 13
Labeled set size: 1052
Unlabelled set size: 3010
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 6.36143806939778E-4
Relative absolute error: 0.11064666338263525
Root relative squared error: 0.12722876138795558
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 802.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 5.532333169131762E-4
Coverage of cases: 100.0
Instances selection time: 802.0
Test time: 1054.0
Accumulative iteration time: 9891.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 14
Labeled set size: 1072
Unlabelled set size: 2990
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 6.2005639962015E-4
Relative absolute error: 0.10779875461498307
Root relative squared error: 0.12401127992403001
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 800.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 5.389937730749154E-4
Coverage of cases: 100.0
Instances selection time: 800.0
Test time: 1070.0
Accumulative iteration time: 10691.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 15
Labeled set size: 1092
Unlabelled set size: 2970
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 6.041950477824823E-4
Relative absolute error: 0.10485322108939989
Root relative squared error: 0.12083900955649646
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 801.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 5.242661054469995E-4
Coverage of cases: 100.0
Instances selection time: 801.0
Test time: 1080.0
Accumulative iteration time: 11492.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 16
Labeled set size: 1112
Unlabelled set size: 2950
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 5.900292799070315E-4
Relative absolute error: 0.10230319728904505
Root relative squared error: 0.11800585598140631
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 805.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 5.115159864452252E-4
Coverage of cases: 100.0
Instances selection time: 805.0
Test time: 1096.0
Accumulative iteration time: 12297.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 17
Labeled set size: 1132
Unlabelled set size: 2930
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 5.762595083406838E-4
Relative absolute error: 0.09978665865974985
Root relative squared error: 0.11525190166813676
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 809.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.989332932987492E-4
Coverage of cases: 100.0
Instances selection time: 809.0
Test time: 1107.0
Accumulative iteration time: 13106.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 18
Labeled set size: 1152
Unlabelled set size: 2910
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 5.637047252975745E-4
Relative absolute error: 0.09748537070115018
Root relative squared error: 0.1127409450595149
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 814.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.874268535057509E-4
Coverage of cases: 100.0
Instances selection time: 813.0
Test time: 1119.0
Accumulative iteration time: 13920.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 19
Labeled set size: 1172
Unlabelled set size: 2890
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 5.506978388804415E-4
Relative absolute error: 0.09510442367551196
Root relative squared error: 0.1101395677760883
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 819.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.755221183775598E-4
Coverage of cases: 100.0
Instances selection time: 819.0
Test time: 1129.0
Accumulative iteration time: 14739.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 20
Labeled set size: 1192
Unlabelled set size: 2870
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 5.367793763299833E-4
Relative absolute error: 0.09255732152837325
Root relative squared error: 0.10735587526599666
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 818.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.627866076418662E-4
Coverage of cases: 100.0
Instances selection time: 818.0
Test time: 1143.0
Accumulative iteration time: 15557.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 21
Labeled set size: 1212
Unlabelled set size: 2850
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 5.248171354863596E-4
Relative absolute error: 0.09043236021065332
Root relative squared error: 0.10496342709727191
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 823.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.521618010532666E-4
Coverage of cases: 100.0
Instances selection time: 823.0
Test time: 1154.0
Accumulative iteration time: 16380.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 22
Labeled set size: 1232
Unlabelled set size: 2830
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 5.131488914547591E-4
Relative absolute error: 0.08834406161187013
Root relative squared error: 0.10262977829095182
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 845.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.4172030805935064E-4
Coverage of cases: 100.0
Instances selection time: 845.0
Test time: 1166.0
Accumulative iteration time: 17225.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 23
Labeled set size: 1252
Unlabelled set size: 2810
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 5.012782053685859E-4
Relative absolute error: 0.08614772141197391
Root relative squared error: 0.10025564107371719
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 830.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.3073860705986956E-4
Coverage of cases: 100.0
Instances selection time: 830.0
Test time: 1183.0
Accumulative iteration time: 18055.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 24
Labeled set size: 1272
Unlabelled set size: 2790
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 4.900871675551921E-4
Relative absolute error: 0.08413817632981305
Root relative squared error: 0.09801743351103842
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 833.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.2069088164906526E-4
Coverage of cases: 100.0
Instances selection time: 833.0
Test time: 1210.0
Accumulative iteration time: 18888.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 25
Labeled set size: 1292
Unlabelled set size: 2770
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 4.7929910743011437E-4
Relative absolute error: 0.08215031239634393
Root relative squared error: 0.09585982148602287
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 843.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.107515619817197E-4
Coverage of cases: 100.0
Instances selection time: 842.0
Test time: 1205.0
Accumulative iteration time: 19731.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 26
Labeled set size: 1312
Unlabelled set size: 2750
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 4.698125878645532E-4
Relative absolute error: 0.08041974148335194
Root relative squared error: 0.09396251757291064
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 840.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.020987074167597E-4
Coverage of cases: 100.0
Instances selection time: 840.0
Test time: 1218.0
Accumulative iteration time: 20571.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 27
Labeled set size: 1332
Unlabelled set size: 2730
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 4.5969647725561984E-4
Relative absolute error: 0.07851140955404676
Root relative squared error: 0.09193929545112396
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 842.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.925570477702338E-4
Coverage of cases: 100.0
Instances selection time: 842.0
Test time: 1229.0
Accumulative iteration time: 21413.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 28
Labeled set size: 1352
Unlabelled set size: 2710
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 4.5002995328140397E-4
Relative absolute error: 0.07672833094521561
Root relative squared error: 0.09000599065628079
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 845.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.8364165472607804E-4
Coverage of cases: 100.0
Instances selection time: 845.0
Test time: 1242.0
Accumulative iteration time: 22258.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 29
Labeled set size: 1372
Unlabelled set size: 2690
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 4.417792968782362E-4
Relative absolute error: 0.07520110892332948
Root relative squared error: 0.08835585937564724
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 848.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.760055446166474E-4
Coverage of cases: 100.0
Instances selection time: 848.0
Test time: 1256.0
Accumulative iteration time: 23106.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 30
Labeled set size: 1392
Unlabelled set size: 2670
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 4.3400594088066884E-4
Relative absolute error: 0.07380349323914444
Root relative squared error: 0.08680118817613378
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 851.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.690174661957222E-4
Coverage of cases: 100.0
Instances selection time: 851.0
Test time: 1268.0
Accumulative iteration time: 23957.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 31
Labeled set size: 1412
Unlabelled set size: 2650
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 4.2604617893375786E-4
Relative absolute error: 0.07240544568042619
Root relative squared error: 0.08520923578675157
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 851.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.6202722840213093E-4
Coverage of cases: 100.0
Instances selection time: 851.0
Test time: 1281.0
Accumulative iteration time: 24808.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 32
Labeled set size: 1432
Unlabelled set size: 2630
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 4.199408152569811E-4
Relative absolute error: 0.0713520423257967
Root relative squared error: 0.08398816305139623
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 852.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.567602116289835E-4
Coverage of cases: 100.0
Instances selection time: 852.0
Test time: 1297.0
Accumulative iteration time: 25660.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 33
Labeled set size: 1452
Unlabelled set size: 2610
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 4.1139977390340296E-4
Relative absolute error: 0.06988915765808963
Root relative squared error: 0.08227995478068059
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 852.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.4944578829044815E-4
Coverage of cases: 100.0
Instances selection time: 852.0
Test time: 1320.0
Accumulative iteration time: 26512.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 34
Labeled set size: 1472
Unlabelled set size: 2590
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 4.046161720039522E-4
Relative absolute error: 0.06879380863832206
Root relative squared error: 0.08092323440079044
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 857.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.439690431916103E-4
Coverage of cases: 100.0
Instances selection time: 857.0
Test time: 1317.0
Accumulative iteration time: 27369.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 35
Labeled set size: 1492
Unlabelled set size: 2570
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.9741357328941117E-4
Relative absolute error: 0.06759948796177709
Root relative squared error: 0.07948271465788223
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 858.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.379974398088854E-4
Coverage of cases: 100.0
Instances selection time: 858.0
Test time: 1331.0
Accumulative iteration time: 28227.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 36
Labeled set size: 1512
Unlabelled set size: 2550
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.884482232022875E-4
Relative absolute error: 0.06605238392347719
Root relative squared error: 0.0776896446404575
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 853.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.3026191961738597E-4
Coverage of cases: 100.0
Instances selection time: 853.0
Test time: 1346.0
Accumulative iteration time: 29080.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 37
Labeled set size: 1532
Unlabelled set size: 2530
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.827889274787061E-4
Relative absolute error: 0.06508275592968563
Root relative squared error: 0.07655778549574122
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 857.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.2541377964842816E-4
Coverage of cases: 100.0
Instances selection time: 857.0
Test time: 1360.0
Accumulative iteration time: 29937.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 38
Labeled set size: 1552
Unlabelled set size: 2510
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.768638660238501E-4
Relative absolute error: 0.06408533571526659
Root relative squared error: 0.07537277320477001
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 856.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.2042667857633294E-4
Coverage of cases: 100.0
Instances selection time: 856.0
Test time: 1368.0
Accumulative iteration time: 30793.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 39
Labeled set size: 1572
Unlabelled set size: 2490
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.689377568143366E-4
Relative absolute error: 0.06276838992903741
Root relative squared error: 0.07378755136286731
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 877.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.1384194964518706E-4
Coverage of cases: 100.0
Instances selection time: 877.0
Test time: 1402.0
Accumulative iteration time: 31670.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 40
Labeled set size: 1592
Unlabelled set size: 2470
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.633088047559705E-4
Relative absolute error: 0.06180021077551638
Root relative squared error: 0.0726617609511941
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 871.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.090010538775819E-4
Coverage of cases: 100.0
Instances selection time: 871.0
Test time: 1413.0
Accumulative iteration time: 32541.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 41
Labeled set size: 1612
Unlabelled set size: 2450
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.5572285800741544E-4
Relative absolute error: 0.06051755935397407
Root relative squared error: 0.07114457160148309
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 873.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.0258779676987035E-4
Coverage of cases: 100.0
Instances selection time: 873.0
Test time: 1442.0
Accumulative iteration time: 33414.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 42
Labeled set size: 1632
Unlabelled set size: 2430
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.4887956386425164E-4
Relative absolute error: 0.05937152387414722
Root relative squared error: 0.06977591277285033
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 884.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 2.968576193707361E-4
Coverage of cases: 100.0
Instances selection time: 884.0
Test time: 1444.0
Accumulative iteration time: 34298.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 43
Labeled set size: 1652
Unlabelled set size: 2410
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.419155168905933E-4
Relative absolute error: 0.058206778149320675
Root relative squared error: 0.06838310337811866
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 853.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 2.910338907466034E-4
Coverage of cases: 100.0
Instances selection time: 853.0
Test time: 1434.0
Accumulative iteration time: 35151.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 44
Labeled set size: 1672
Unlabelled set size: 2390
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.352264345248116E-4
Relative absolute error: 0.05710956784887583
Root relative squared error: 0.06704528690496231
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 853.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 2.8554783924437916E-4
Coverage of cases: 100.0
Instances selection time: 853.0
Test time: 1444.0
Accumulative iteration time: 36004.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 45
Labeled set size: 1692
Unlabelled set size: 2370
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.291559633356528E-4
Relative absolute error: 0.05607107089739863
Root relative squared error: 0.06583119266713056
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 851.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 2.803553544869931E-4
Coverage of cases: 100.0
Instances selection time: 851.0
Test time: 1457.0
Accumulative iteration time: 36855.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 46
Labeled set size: 1712
Unlabelled set size: 2350
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.2313036623878743E-4
Relative absolute error: 0.05503273986440936
Root relative squared error: 0.06462607324775749
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 851.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 2.751636993220468E-4
Coverage of cases: 100.0
Instances selection time: 851.0
Test time: 1467.0
Accumulative iteration time: 37706.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Time end:Wed Nov 01 05.25.43 EET 2017