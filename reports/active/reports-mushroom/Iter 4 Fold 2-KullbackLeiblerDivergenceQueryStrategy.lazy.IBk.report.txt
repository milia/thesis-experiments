Wed Nov 01 05.57.31 EET 2017
Dataset: mushroom
Test set size: 4062
Initial Labelled set size: 812
Initial Unlabelled set size: 3250
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Wed Nov 01 05.57.31 EET 2017
		
Iteration: 1
Labeled set size: 812
Unlabelled set size: 3250
	
Mean region size: 50.0492368291482
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 0.9999992717796152
Root mean squared error: 0.021791658906766747
Relative absolute error: 0.28379150331806346
Root relative squared error: 4.35833178135335
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 1425.0
Weighted AreaUnderPRC: 0.9999992722793452
Mean absolute error: 0.0014189575165903174
Coverage of cases: 100.0
Instances selection time: 1422.0
Test time: 887.0
Accumulative iteration time: 1425.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 3.0
		
Iteration: 2
Labeled set size: 832
Unlabelled set size: 3230
	
Mean region size: 50.0492368291482
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 0.9999992717796152
Root mean squared error: 0.021790863875400506
Relative absolute error: 0.27993925781850487
Root relative squared error: 4.358172775080101
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 1421.0
Weighted AreaUnderPRC: 0.9999992722793452
Mean absolute error: 0.0013996962890925244
Coverage of cases: 100.0
Instances selection time: 1421.0
Test time: 898.0
Accumulative iteration time: 2846.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 0.0
		
Iteration: 3
Labeled set size: 852
Unlabelled set size: 3210
	
Mean region size: 50.0492368291482
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 0.9999992717796152
Root mean squared error: 0.020480297784309154
Relative absolute error: 0.2683833868315092
Root relative squared error: 4.096059556861831
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 1470.0
Weighted AreaUnderPRC: 0.9999992722793452
Mean absolute error: 0.001341916934157546
Coverage of cases: 100.0
Instances selection time: 1469.0
Test time: 954.0
Accumulative iteration time: 4316.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 1.0
		
Iteration: 4
Labeled set size: 872
Unlabelled set size: 3190
	
Mean region size: 50.0492368291482
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 0.9999992717796152
Root mean squared error: 0.020479461802118317
Relative absolute error: 0.2643418444876648
Root relative squared error: 4.0958923604236634
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 1447.0
Weighted AreaUnderPRC: 0.9999992722793452
Mean absolute error: 0.001321709222438324
Coverage of cases: 100.0
Instances selection time: 1447.0
Test time: 920.0
Accumulative iteration time: 5763.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 0.0
		
Iteration: 5
Labeled set size: 892
Unlabelled set size: 3170
	
Mean region size: 50.0492368291482
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 0.9999992717796152
Root mean squared error: 0.020478811646001068
Relative absolute error: 0.2612965784443566
Root relative squared error: 4.095762329200213
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 1459.0
Weighted AreaUnderPRC: 0.9999992722793452
Mean absolute error: 0.001306482892221783
Coverage of cases: 100.0
Instances selection time: 1459.0
Test time: 932.0
Accumulative iteration time: 7222.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 0.0
		
Iteration: 6
Labeled set size: 912
Unlabelled set size: 3150
	
Mean region size: 50.0492368291482
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 0.9999992717796152
Root mean squared error: 0.020478012385388565
Relative absolute error: 0.25727252296295644
Root relative squared error: 4.095602477077713
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 1476.0
Weighted AreaUnderPRC: 0.9999992722793452
Mean absolute error: 0.0012863626148147823
Coverage of cases: 100.0
Instances selection time: 1475.0
Test time: 947.0
Accumulative iteration time: 8698.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 1.0
		
Iteration: 7
Labeled set size: 932
Unlabelled set size: 3130
	
Mean region size: 50.0492368291482
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 0.9999992717796152
Root mean squared error: 0.020477367018856718
Relative absolute error: 0.2539562818454093
Root relative squared error: 4.095473403771344
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 1485.0
Weighted AreaUnderPRC: 0.9999992722793452
Mean absolute error: 0.0012697814092270466
Coverage of cases: 100.0
Instances selection time: 1485.0
Test time: 960.0
Accumulative iteration time: 10183.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 0.0
		
Iteration: 8
Labeled set size: 952
Unlabelled set size: 3110
	
Mean region size: 50.0492368291482
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 0.9999992717796152
Root mean squared error: 0.02047677380710749
Relative absolute error: 0.25085571887376085
Root relative squared error: 4.095354761421498
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 1502.0
Weighted AreaUnderPRC: 0.9999992722793452
Mean absolute error: 0.0012542785943688041
Coverage of cases: 100.0
Instances selection time: 1502.0
Test time: 990.0
Accumulative iteration time: 11685.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 0.0
		
Iteration: 9
Labeled set size: 972
Unlabelled set size: 3090
	
Mean region size: 50.0492368291482
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 0.9999992717796152
Root mean squared error: 0.020476224619709712
Relative absolute error: 0.2479639779005453
Root relative squared error: 4.095244923941943
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 1520.0
Weighted AreaUnderPRC: 0.9999992722793452
Mean absolute error: 0.0012398198895027265
Coverage of cases: 100.0
Instances selection time: 1519.0
Test time: 1037.0
Accumulative iteration time: 13205.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 1.0
		
Iteration: 10
Labeled set size: 992
Unlabelled set size: 3070
	
Mean region size: 50.0492368291482
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 0.9999992717796152
Root mean squared error: 0.020475736490237116
Relative absolute error: 0.24530097331003634
Root relative squared error: 4.095147298047423
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 1779.0
Weighted AreaUnderPRC: 0.9999992722793452
Mean absolute error: 0.0012265048665501817
Coverage of cases: 100.0
Instances selection time: 1779.0
Test time: 1029.0
Accumulative iteration time: 14984.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 0.0
		
Iteration: 11
Labeled set size: 1012
Unlabelled set size: 3050
	
Mean region size: 50.0492368291482
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 0.9999992717796152
Root mean squared error: 0.020475209690197442
Relative absolute error: 0.24233613914195845
Root relative squared error: 4.095041938039488
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 1581.0
Weighted AreaUnderPRC: 0.9999992722793452
Mean absolute error: 0.0012116806957097922
Coverage of cases: 100.0
Instances selection time: 1579.0
Test time: 1053.0
Accumulative iteration time: 16565.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 2.0
		
Iteration: 12
Labeled set size: 1032
Unlabelled set size: 3030
	
Mean region size: 50.0492368291482
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 0.9999992717796152
Root mean squared error: 0.0204747264406165
Relative absolute error: 0.2394892720272007
Root relative squared error: 4.0949452881233
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 1717.0
Weighted AreaUnderPRC: 0.9999992722793452
Mean absolute error: 0.0011974463601360035
Coverage of cases: 100.0
Instances selection time: 1716.0
Test time: 1034.0
Accumulative iteration time: 18282.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 1.0
		
Iteration: 13
Labeled set size: 1052
Unlabelled set size: 3010
	
Mean region size: 50.0492368291482
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 0.9999992717796152
Root mean squared error: 0.020474319259297265
Relative absolute error: 0.23705243105792423
Root relative squared error: 4.094863851859453
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 1562.0
Weighted AreaUnderPRC: 0.9999992722793452
Mean absolute error: 0.001185262155289621
Coverage of cases: 100.0
Instances selection time: 1561.0
Test time: 1044.0
Accumulative iteration time: 19844.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 1.0
		
Iteration: 14
Labeled set size: 1072
Unlabelled set size: 2990
	
Mean region size: 50.0492368291482
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 0.9999992717796152
Root mean squared error: 0.020473869175380157
Relative absolute error: 0.23429975885784224
Root relative squared error: 4.094773835076031
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 1780.0
Weighted AreaUnderPRC: 0.9999992722793452
Mean absolute error: 0.0011714987942892112
Coverage of cases: 100.0
Instances selection time: 1780.0
Test time: 1062.0
Accumulative iteration time: 21624.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 0.0
		
Iteration: 15
Labeled set size: 1092
Unlabelled set size: 2970
	
Mean region size: 50.0492368291482
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 0.9999992717796152
Root mean squared error: 0.020473495351487635
Relative absolute error: 0.2318929957904462
Root relative squared error: 4.094699070297527
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 2246.0
Weighted AreaUnderPRC: 0.9999992722793452
Mean absolute error: 0.001159464978952231
Coverage of cases: 100.0
Instances selection time: 2245.0
Test time: 1074.0
Accumulative iteration time: 23870.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 1.0
		
Iteration: 16
Labeled set size: 1112
Unlabelled set size: 2950
	
Mean region size: 50.0492368291482
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 0.9999992717796152
Root mean squared error: 0.020473110713991088
Relative absolute error: 0.2293589662322142
Root relative squared error: 4.094622142798218
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 1593.0
Weighted AreaUnderPRC: 0.9999992722793452
Mean absolute error: 0.001146794831161071
Coverage of cases: 100.0
Instances selection time: 1592.0
Test time: 1464.0
Accumulative iteration time: 25463.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 1.0
		
Iteration: 17
Labeled set size: 1132
Unlabelled set size: 2930
	
Mean region size: 50.0492368291482
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 0.9999992717796152
Root mean squared error: 0.02047284126495958
Relative absolute error: 0.22760204410110474
Root relative squared error: 4.094568252991916
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 2012.0
Weighted AreaUnderPRC: 0.9999992722793452
Mean absolute error: 0.0011380102205055236
Coverage of cases: 100.0
Instances selection time: 2011.0
Test time: 1100.0
Accumulative iteration time: 27475.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 1.0
		
Iteration: 18
Labeled set size: 1152
Unlabelled set size: 2910
	
Mean region size: 50.0492368291482
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 0.9999992717796152
Root mean squared error: 0.02047253576886429
Relative absolute error: 0.2255474211817567
Root relative squared error: 4.094507153772859
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 1615.0
Weighted AreaUnderPRC: 0.9999992722793452
Mean absolute error: 0.0011277371059087834
Coverage of cases: 100.0
Instances selection time: 1615.0
Test time: 1110.0
Accumulative iteration time: 29090.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 0.0
		
Iteration: 19
Labeled set size: 1172
Unlabelled set size: 2890
	
Mean region size: 50.0492368291482
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 0.9999992717796152
Root mean squared error: 0.020472249625032225
Relative absolute error: 0.22358164965398222
Root relative squared error: 4.094449925006445
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 1625.0
Weighted AreaUnderPRC: 0.9999992722793452
Mean absolute error: 0.001117908248269911
Coverage of cases: 100.0
Instances selection time: 1624.0
Test time: 1126.0
Accumulative iteration time: 30715.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 1.0
		
Iteration: 20
Labeled set size: 1192
Unlabelled set size: 2870
	
Mean region size: 50.0492368291482
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 0.9999992717796152
Root mean squared error: 0.02047192859789595
Relative absolute error: 0.2213356418166344
Root relative squared error: 4.09438571957919
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 1636.0
Weighted AreaUnderPRC: 0.9999992722793452
Mean absolute error: 0.001106678209083172
Coverage of cases: 100.0
Instances selection time: 1635.0
Test time: 1134.0
Accumulative iteration time: 32351.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 1.0
		
Iteration: 21
Labeled set size: 1212
Unlabelled set size: 2850
	
Mean region size: 50.0492368291482
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 0.9999992717796152
Root mean squared error: 0.02047161963051631
Relative absolute error: 0.21907732818303216
Root relative squared error: 4.094323926103262
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 1647.0
Weighted AreaUnderPRC: 0.9999992722793452
Mean absolute error: 0.0010953866409151608
Coverage of cases: 100.0
Instances selection time: 1646.0
Test time: 1384.0
Accumulative iteration time: 33998.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 1.0
		
Iteration: 22
Labeled set size: 1232
Unlabelled set size: 2830
	
Mean region size: 50.0492368291482
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 0.9999992717796152
Root mean squared error: 0.020471354527277295
Relative absolute error: 0.2170751039043654
Root relative squared error: 4.094270905455459
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 1655.0
Weighted AreaUnderPRC: 0.9999992722793452
Mean absolute error: 0.001085375519521827
Coverage of cases: 100.0
Instances selection time: 1655.0
Test time: 1161.0
Accumulative iteration time: 35653.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 0.0
		
Iteration: 23
Labeled set size: 1252
Unlabelled set size: 2810
	
Mean region size: 50.0492368291482
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 0.9999992717796152
Root mean squared error: 0.01943708688622884
Relative absolute error: 0.20876527713164628
Root relative squared error: 3.887417377245768
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 1657.0
Weighted AreaUnderPRC: 0.9999992722793452
Mean absolute error: 0.0010438263856582313
Coverage of cases: 100.0
Instances selection time: 1657.0
Test time: 1172.0
Accumulative iteration time: 37310.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 0.0
		
Iteration: 24
Labeled set size: 1272
Unlabelled set size: 2790
	
Mean region size: 50.0492368291482
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 0.9999992717796152
Root mean squared error: 0.01943682967190769
Relative absolute error: 0.20685627248909116
Root relative squared error: 3.887365934381538
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 1842.0
Weighted AreaUnderPRC: 0.9999992722793452
Mean absolute error: 0.0010342813624454558
Coverage of cases: 100.0
Instances selection time: 1841.0
Test time: 1184.0
Accumulative iteration time: 39152.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 1.0
		
Iteration: 25
Labeled set size: 1292
Unlabelled set size: 2770
	
Mean region size: 50.0492368291482
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 0.9999992717796152
Root mean squared error: 0.019436577829508098
Relative absolute error: 0.2049484361656165
Root relative squared error: 3.8873155659016194
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 1671.0
Weighted AreaUnderPRC: 0.9999992722793452
Mean absolute error: 0.0010247421808280825
Coverage of cases: 100.0
Instances selection time: 1670.0
Test time: 1200.0
Accumulative iteration time: 40823.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 1.0
		
Iteration: 26
Labeled set size: 1312
Unlabelled set size: 2750
	
Mean region size: 50.0492368291482
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 0.9999992717796152
Root mean squared error: 0.01943638277960542
Relative absolute error: 0.2034480133392855
Root relative squared error: 3.8872765559210842
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 1680.0
Weighted AreaUnderPRC: 0.9999992722793452
Mean absolute error: 0.0010172400666964274
Coverage of cases: 100.0
Instances selection time: 1679.0
Test time: 1214.0
Accumulative iteration time: 42503.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 1.0
		
Iteration: 27
Labeled set size: 1332
Unlabelled set size: 2730
	
Mean region size: 50.0492368291482
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 0.9999992717796152
Root mean squared error: 0.019436191253862655
Relative absolute error: 0.2019468776498543
Root relative squared error: 3.887238250772531
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 1692.0
Weighted AreaUnderPRC: 0.9999992722793452
Mean absolute error: 0.0010097343882492715
Coverage of cases: 100.0
Instances selection time: 1691.0
Test time: 1239.0
Accumulative iteration time: 44195.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 1.0
		
Iteration: 28
Labeled set size: 1352
Unlabelled set size: 2710
	
Mean region size: 50.01230920728705
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.007858916408382599
Relative absolute error: 0.10416495949600517
Root relative squared error: 1.5717832816765198
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1700.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 5.208247974800258E-4
Coverage of cases: 100.0
Instances selection time: 1699.0
Test time: 1234.0
Accumulative iteration time: 45895.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 29
Labeled set size: 1372
Unlabelled set size: 2690
	
Mean region size: 50.01230920728705
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.00785839383745632
Relative absolute error: 0.10261933301663208
Root relative squared error: 1.571678767491264
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1701.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 5.130966650831604E-4
Coverage of cases: 100.0
Instances selection time: 1700.0
Test time: 1247.0
Accumulative iteration time: 47596.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 30
Labeled set size: 1392
Unlabelled set size: 2670
	
Mean region size: 50.01230920728705
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.007857880448334097
Relative absolute error: 0.10101780331737259
Root relative squared error: 1.5715760896668194
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1707.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 5.05089016586863E-4
Coverage of cases: 100.0
Instances selection time: 1706.0
Test time: 1261.0
Accumulative iteration time: 49303.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 31
Labeled set size: 1412
Unlabelled set size: 2650
	
Mean region size: 50.01230920728705
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.007857407992468592
Relative absolute error: 0.09952707930707358
Root relative squared error: 1.5714815984937185
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1707.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.976353965353679E-4
Coverage of cases: 100.0
Instances selection time: 1706.0
Test time: 1274.0
Accumulative iteration time: 51010.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 32
Labeled set size: 1432
Unlabelled set size: 2630
	
Mean region size: 50.01230920728705
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.007856945174650734
Relative absolute error: 0.09797753164221584
Root relative squared error: 1.5713890349301467
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1731.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.898876582110792E-4
Coverage of cases: 100.0
Instances selection time: 1731.0
Test time: 1300.0
Accumulative iteration time: 52741.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 33
Labeled set size: 1452
Unlabelled set size: 2610
	
Mean region size: 50.01230920728705
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.007856532464707536
Relative absolute error: 0.09662405590736821
Root relative squared error: 1.571306492941507
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1715.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.8312027953684107E-4
Coverage of cases: 100.0
Instances selection time: 1714.0
Test time: 1307.0
Accumulative iteration time: 54456.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 34
Labeled set size: 1472
Unlabelled set size: 2590
	
Mean region size: 50.01230920728705
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.007856104222578746
Relative absolute error: 0.09512850375078621
Root relative squared error: 1.5712208445157492
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1754.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.7564251875393106E-4
Coverage of cases: 100.0
Instances selection time: 1753.0
Test time: 1321.0
Accumulative iteration time: 56210.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 35
Labeled set size: 1492
Unlabelled set size: 2570
	
Mean region size: 50.01230920728705
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0078556771545263
Relative absolute error: 0.09362781826033954
Root relative squared error: 1.5711354309052599
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1740.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.681390913016977E-4
Coverage of cases: 100.0
Instances selection time: 1739.0
Test time: 1327.0
Accumulative iteration time: 57950.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 36
Labeled set size: 1512
Unlabelled set size: 2550
	
Mean region size: 50.01230920728705
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.00785530562817342
Relative absolute error: 0.09235055979616057
Root relative squared error: 1.5710611256346838
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1740.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.617527989808028E-4
Coverage of cases: 100.0
Instances selection time: 1739.0
Test time: 1345.0
Accumulative iteration time: 59690.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 37
Labeled set size: 1532
Unlabelled set size: 2530
	
Mean region size: 50.01230920728705
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.00785498438541617
Relative absolute error: 0.09119278875356951
Root relative squared error: 1.570996877083234
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1734.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.5596394376784755E-4
Coverage of cases: 100.0
Instances selection time: 1733.0
Test time: 1349.0
Accumulative iteration time: 61424.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 38
Labeled set size: 1552
Unlabelled set size: 2510
	
Mean region size: 50.01230920728705
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.007854685746121508
Relative absolute error: 0.0900704293085056
Root relative squared error: 1.5709371492243016
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1736.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.50352146542528E-4
Coverage of cases: 100.0
Instances selection time: 1735.0
Test time: 1360.0
Accumulative iteration time: 63160.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 39
Labeled set size: 1572
Unlabelled set size: 2490
	
Mean region size: 50.01230920728705
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.007854314202106069
Relative absolute error: 0.08869258908845111
Root relative squared error: 1.570862840421214
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1741.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.4346294544225556E-4
Coverage of cases: 100.0
Instances selection time: 1740.0
Test time: 1374.0
Accumulative iteration time: 64901.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 40
Labeled set size: 1592
Unlabelled set size: 2470
	
Mean region size: 50.01230920728705
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.007854012931560588
Relative absolute error: 0.08755526842682614
Root relative squared error: 1.5708025863121176
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1761.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.3777634213413067E-4
Coverage of cases: 100.0
Instances selection time: 1760.0
Test time: 1400.0
Accumulative iteration time: 66662.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 41
Labeled set size: 1612
Unlabelled set size: 2450
	
Mean region size: 50.01230920728705
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.00785372195622462
Relative absolute error: 0.0864048866222745
Root relative squared error: 1.570744391244924
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1741.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.320244331113725E-4
Coverage of cases: 100.0
Instances selection time: 1740.0
Test time: 1400.0
Accumulative iteration time: 68403.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 42
Labeled set size: 1632
Unlabelled set size: 2430
	
Mean region size: 50.01230920728705
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.007853455692623506
Relative absolute error: 0.08535178588547307
Root relative squared error: 1.570691138524701
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1745.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.267589294273653E-4
Coverage of cases: 100.0
Instances selection time: 1744.0
Test time: 1409.0
Accumulative iteration time: 70148.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 43
Labeled set size: 1652
Unlabelled set size: 2410
	
Mean region size: 50.01230920728705
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.007853182532904486
Relative absolute error: 0.08425423617256443
Root relative squared error: 1.5706365065808974
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1743.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.2127118086282214E-4
Coverage of cases: 100.0
Instances selection time: 1742.0
Test time: 1426.0
Accumulative iteration time: 71891.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 44
Labeled set size: 1672
Unlabelled set size: 2390
	
Mean region size: 50.01230920728705
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.007852915657241541
Relative absolute error: 0.08313304413580254
Root relative squared error: 1.5705831314483083
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1747.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.1566522067901273E-4
Coverage of cases: 100.0
Instances selection time: 1746.0
Test time: 1441.0
Accumulative iteration time: 73638.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 45
Labeled set size: 1692
Unlabelled set size: 2370
	
Mean region size: 50.01230920728705
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.007852664735668662
Relative absolute error: 0.08206244217445696
Root relative squared error: 1.5705329471337324
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1748.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.1031221087228483E-4
Coverage of cases: 100.0
Instances selection time: 1747.0
Test time: 1453.0
Accumulative iteration time: 75386.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 46
Labeled set size: 1712
Unlabelled set size: 2350
	
Mean region size: 50.01230920728705
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.00785241388233293
Relative absolute error: 0.0810046696219902
Root relative squared error: 1.5704827764665859
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1766.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.05023348109951E-4
Coverage of cases: 100.0
Instances selection time: 1765.0
Test time: 1472.0
Accumulative iteration time: 77152.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Time end:Wed Nov 01 05.59.47 EET 2017