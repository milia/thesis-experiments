Wed Nov 01 07.13.48 EET 2017
Dataset: mushroom
Test set size: 4062
Initial Labelled set size: 812
Initial Unlabelled set size: 3250
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Wed Nov 01 07.13.48 EET 2017
		
Iteration: 1
Labeled set size: 812
Unlabelled set size: 3250
	
Mean region size: 50.03692762186115
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 0.9994266478170867
Root mean squared error: 0.028214611211273534
Relative absolute error: 0.33800768657703806
Root relative squared error: 5.6429222422547065
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 714.0
Weighted AreaUnderPRC: 0.9992487798312931
Mean absolute error: 0.0016900384328851902
Coverage of cases: 99.9261447562777
Instances selection time: 714.0
Test time: 890.0
Accumulative iteration time: 714.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 0.0
		
Iteration: 2
Labeled set size: 832
Unlabelled set size: 3230
	
Mean region size: 50.03692762186115
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 0.9994266478170867
Root mean squared error: 0.02821444002005199
Relative absolute error: 0.33354631977467425
Root relative squared error: 5.642888004010398
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 720.0
Weighted AreaUnderPRC: 0.9992487798312931
Mean absolute error: 0.0016677315988733712
Coverage of cases: 99.9261447562777
Instances selection time: 719.0
Test time: 903.0
Accumulative iteration time: 1434.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 1.0
		
Iteration: 3
Labeled set size: 852
Unlabelled set size: 3210
	
Mean region size: 50.03692762186115
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 0.9994266478170867
Root mean squared error: 0.028214343815687028
Relative absolute error: 0.3297244040758617
Root relative squared error: 5.642868763137406
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 731.0
Weighted AreaUnderPRC: 0.9992487798312931
Mean absolute error: 0.0016486220203793082
Coverage of cases: 99.9261447562777
Instances selection time: 731.0
Test time: 921.0
Accumulative iteration time: 2165.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 0.0
		
Iteration: 4
Labeled set size: 872
Unlabelled set size: 3190
	
Mean region size: 50.03692762186115
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 0.9994266478170867
Root mean squared error: 0.028214304019282186
Relative absolute error: 0.32626654179221887
Root relative squared error: 5.642860803856437
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 736.0
Weighted AreaUnderPRC: 0.9992487798312931
Mean absolute error: 0.0016313327089610944
Coverage of cases: 99.9261447562777
Instances selection time: 735.0
Test time: 931.0
Accumulative iteration time: 2901.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 1.0
		
Iteration: 5
Labeled set size: 892
Unlabelled set size: 3170
	
Mean region size: 50.03692762186115
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 0.9994266478170867
Root mean squared error: 0.02821428416324469
Relative absolute error: 0.32306091622927985
Root relative squared error: 5.642856832648938
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 743.0
Weighted AreaUnderPRC: 0.9992487798312931
Mean absolute error: 0.0016153045811463994
Coverage of cases: 99.9261447562777
Instances selection time: 742.0
Test time: 946.0
Accumulative iteration time: 3644.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 1.0
		
Iteration: 6
Labeled set size: 912
Unlabelled set size: 3150
	
Mean region size: 50.03692762186115
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 0.9994266478170867
Root mean squared error: 0.028214198275352483
Relative absolute error: 0.3192408880706772
Root relative squared error: 5.642839655070497
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 751.0
Weighted AreaUnderPRC: 0.9992487798312931
Mean absolute error: 0.0015962044403533859
Coverage of cases: 99.9261447562777
Instances selection time: 750.0
Test time: 960.0
Accumulative iteration time: 4395.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 1.0
		
Iteration: 7
Labeled set size: 932
Unlabelled set size: 3130
	
Mean region size: 50.03692762186115
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 0.9994266478170867
Root mean squared error: 0.02821420672109357
Relative absolute error: 0.3163751129746239
Root relative squared error: 5.642841344218714
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 757.0
Weighted AreaUnderPRC: 0.9992487798312931
Mean absolute error: 0.0015818755648731194
Coverage of cases: 99.9261447562777
Instances selection time: 756.0
Test time: 972.0
Accumulative iteration time: 5152.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 1.0
		
Iteration: 8
Labeled set size: 952
Unlabelled set size: 3110
	
Mean region size: 50.03692762186115
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.007661223628685825
Relative absolute error: 0.1661813259738238
Root relative squared error: 1.532244725737165
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 773.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 8.30906629869119E-4
Coverage of cases: 100.0
Instances selection time: 772.0
Test time: 990.0
Accumulative iteration time: 5925.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 9
Labeled set size: 972
Unlabelled set size: 3090
	
Mean region size: 50.03692762186115
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.007659454050002007
Relative absolute error: 0.1628156994072742
Root relative squared error: 1.5318908100004014
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 780.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 8.140784970363709E-4
Coverage of cases: 100.0
Instances selection time: 780.0
Test time: 1001.0
Accumulative iteration time: 6705.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 10
Labeled set size: 992
Unlabelled set size: 3070
	
Mean region size: 50.03692762186115
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.007657873507609002
Relative absolute error: 0.15987692261308492
Root relative squared error: 1.5315747015218004
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 774.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 7.993846130654246E-4
Coverage of cases: 100.0
Instances selection time: 773.0
Test time: 1008.0
Accumulative iteration time: 7479.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 11
Labeled set size: 1012
Unlabelled set size: 3050
	
Mean region size: 50.03692762186115
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.007656406819665607
Relative absolute error: 0.15709341374848135
Root relative squared error: 1.5312813639331215
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 781.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 7.854670687424067E-4
Coverage of cases: 100.0
Instances selection time: 780.0
Test time: 1025.0
Accumulative iteration time: 8260.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 12
Labeled set size: 1032
Unlabelled set size: 3030
	
Mean region size: 50.03692762186115
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.007654978028784461
Relative absolute error: 0.15435211050652403
Root relative squared error: 1.530995605756892
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 784.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 7.717605525326202E-4
Coverage of cases: 100.0
Instances selection time: 783.0
Test time: 1035.0
Accumulative iteration time: 9044.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 13
Labeled set size: 1052
Unlabelled set size: 3010
	
Mean region size: 50.03692762186115
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.007653683048665046
Relative absolute error: 0.15178049379452582
Root relative squared error: 1.530736609733009
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 795.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 7.589024689726291E-4
Coverage of cases: 100.0
Instances selection time: 794.0
Test time: 1048.0
Accumulative iteration time: 9839.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 14
Labeled set size: 1072
Unlabelled set size: 2990
	
Mean region size: 50.03692762186115
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.007652343710647493
Relative absolute error: 0.14894154321780678
Root relative squared error: 1.5304687421294987
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 795.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 7.447077160890339E-4
Coverage of cases: 100.0
Instances selection time: 794.0
Test time: 1060.0
Accumulative iteration time: 10634.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 15
Labeled set size: 1092
Unlabelled set size: 2970
	
Mean region size: 50.03692762186115
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.007650926529155187
Relative absolute error: 0.14581206850380612
Root relative squared error: 1.5301853058310375
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 801.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 7.290603425190306E-4
Coverage of cases: 100.0
Instances selection time: 800.0
Test time: 1075.0
Accumulative iteration time: 11435.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 16
Labeled set size: 1112
Unlabelled set size: 2950
	
Mean region size: 50.03692762186115
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.007649674985354738
Relative absolute error: 0.14304383105936636
Root relative squared error: 1.5299349970709477
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 805.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 7.152191552968318E-4
Coverage of cases: 100.0
Instances selection time: 804.0
Test time: 1086.0
Accumulative iteration time: 12240.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 17
Labeled set size: 1132
Unlabelled set size: 2930
	
Mean region size: 50.03692762186115
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.007648630152958634
Relative absolute error: 0.14068438071450912
Root relative squared error: 1.5297260305917266
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 811.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 7.034219035725455E-4
Coverage of cases: 100.0
Instances selection time: 810.0
Test time: 1103.0
Accumulative iteration time: 13051.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 18
Labeled set size: 1152
Unlabelled set size: 2910
	
Mean region size: 50.03692762186115
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0076475477002009
Relative absolute error: 0.13817482312515078
Root relative squared error: 1.52950954004018
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 815.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 6.908741156257539E-4
Coverage of cases: 100.0
Instances selection time: 814.0
Test time: 1122.0
Accumulative iteration time: 13866.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 19
Labeled set size: 1172
Unlabelled set size: 2890
	
Mean region size: 50.03692762186115
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.00764662921941237
Relative absolute error: 0.1359945857522734
Root relative squared error: 1.529325843882474
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 824.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 6.79972928761367E-4
Coverage of cases: 100.0
Instances selection time: 823.0
Test time: 1129.0
Accumulative iteration time: 14690.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 20
Labeled set size: 1192
Unlabelled set size: 2870
	
Mean region size: 50.03692762186115
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.007645734591176859
Relative absolute error: 0.13384125902623784
Root relative squared error: 1.5291469182353716
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 823.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 6.692062951311892E-4
Coverage of cases: 100.0
Instances selection time: 822.0
Test time: 1136.0
Accumulative iteration time: 15513.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 21
Labeled set size: 1212
Unlabelled set size: 2850
	
Mean region size: 50.03692762186115
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0076449150471119475
Relative absolute error: 0.1317998758951295
Root relative squared error: 1.5289830094223895
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 841.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 6.589993794756476E-4
Coverage of cases: 100.0
Instances selection time: 840.0
Test time: 1156.0
Accumulative iteration time: 16354.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 22
Labeled set size: 1232
Unlabelled set size: 2830
	
Mean region size: 50.03692762186115
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.007644158384765407
Relative absolute error: 0.129987004224904
Root relative squared error: 1.5288316769530814
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 832.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 6.4993502112452E-4
Coverage of cases: 100.0
Instances selection time: 831.0
Test time: 1163.0
Accumulative iteration time: 17186.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 23
Labeled set size: 1252
Unlabelled set size: 2810
	
Mean region size: 50.03692762186115
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0076434669631330965
Relative absolute error: 0.1282752422736374
Root relative squared error: 1.5286933926266193
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 836.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 6.41376211368187E-4
Coverage of cases: 100.0
Instances selection time: 836.0
Test time: 1177.0
Accumulative iteration time: 18022.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 24
Labeled set size: 1272
Unlabelled set size: 2790
	
Mean region size: 50.03692762186115
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.011856800109791675
Relative absolute error: 0.1494065485180183
Root relative squared error: 2.371360021958335
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 840.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 7.470327425900915E-4
Coverage of cases: 100.0
Instances selection time: 839.0
Test time: 1191.0
Accumulative iteration time: 18862.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 25
Labeled set size: 1292
Unlabelled set size: 2770
	
Mean region size: 50.03692762186115
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.011856341336610468
Relative absolute error: 0.14744202497026404
Root relative squared error: 2.3712682673220935
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 850.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 7.372101248513202E-4
Coverage of cases: 100.0
Instances selection time: 849.0
Test time: 1203.0
Accumulative iteration time: 19712.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 26
Labeled set size: 1312
Unlabelled set size: 2750
	
Mean region size: 50.03692762186115
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.01185598679492876
Relative absolute error: 0.14593760393125302
Root relative squared error: 2.3711973589857522
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 846.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 7.29688019656265E-4
Coverage of cases: 100.0
Instances selection time: 845.0
Test time: 1213.0
Accumulative iteration time: 20558.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 27
Labeled set size: 1332
Unlabelled set size: 2730
	
Mean region size: 50.03692762186115
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.011855621543779716
Relative absolute error: 0.14427514445500406
Root relative squared error: 2.371124308755943
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 851.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 7.213757222750203E-4
Coverage of cases: 100.0
Instances selection time: 850.0
Test time: 1244.0
Accumulative iteration time: 21409.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 28
Labeled set size: 1352
Unlabelled set size: 2710
	
Mean region size: 50.03692762186115
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.013595926646003944
Relative absolute error: 0.15258572965162698
Root relative squared error: 2.719185329200789
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 852.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 7.629286482581348E-4
Coverage of cases: 100.0
Instances selection time: 852.0
Test time: 1411.0
Accumulative iteration time: 22261.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 29
Labeled set size: 1372
Unlabelled set size: 2690
	
Mean region size: 50.03692762186115
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.013595644609952887
Relative absolute error: 0.15103448274029566
Root relative squared error: 2.7191289219905777
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 856.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 7.551724137014783E-4
Coverage of cases: 100.0
Instances selection time: 855.0
Test time: 1250.0
Accumulative iteration time: 23117.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 30
Labeled set size: 1392
Unlabelled set size: 2670
	
Mean region size: 50.03692762186115
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.013595369817295441
Relative absolute error: 0.14951289541897575
Root relative squared error: 2.7190739634590884
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 859.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 7.475644770948787E-4
Coverage of cases: 100.0
Instances selection time: 858.0
Test time: 1261.0
Accumulative iteration time: 23976.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 31
Labeled set size: 1412
Unlabelled set size: 2650
	
Mean region size: 50.03692762186115
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.013595100537842368
Relative absolute error: 0.14797845535896764
Root relative squared error: 2.7190201075684737
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 864.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 7.398922767948383E-4
Coverage of cases: 100.0
Instances selection time: 863.0
Test time: 1274.0
Accumulative iteration time: 24840.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 32
Labeled set size: 1432
Unlabelled set size: 2630
	
Mean region size: 50.03692762186115
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.013594855290560105
Relative absolute error: 0.14657081196183028
Root relative squared error: 2.7189710581120208
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 865.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 7.328540598091514E-4
Coverage of cases: 100.0
Instances selection time: 864.0
Test time: 1285.0
Accumulative iteration time: 25705.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 33
Labeled set size: 1452
Unlabelled set size: 2610
	
Mean region size: 50.03692762186115
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.013594604948997996
Relative absolute error: 0.1450690169363071
Root relative squared error: 2.7189209897995994
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 870.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 7.253450846815356E-4
Coverage of cases: 100.0
Instances selection time: 869.0
Test time: 1301.0
Accumulative iteration time: 26575.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 34
Labeled set size: 1472
Unlabelled set size: 2590
	
Mean region size: 50.03692762186115
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.013594367773084107
Relative absolute error: 0.1436354888140964
Root relative squared error: 2.7188735546168212
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 869.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 7.181774440704821E-4
Coverage of cases: 100.0
Instances selection time: 869.0
Test time: 1309.0
Accumulative iteration time: 27444.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 35
Labeled set size: 1492
Unlabelled set size: 2570
	
Mean region size: 50.03692762186115
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.013594143051247202
Relative absolute error: 0.14227963090286067
Root relative squared error: 2.7188286102494406
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 877.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 7.113981545143033E-4
Coverage of cases: 100.0
Instances selection time: 877.0
Test time: 1336.0
Accumulative iteration time: 28321.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 36
Labeled set size: 1512
Unlabelled set size: 2550
	
Mean region size: 50.03692762186115
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.013593943501664213
Relative absolute error: 0.1410195446986467
Root relative squared error: 2.7187887003328424
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 877.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 7.050977234932334E-4
Coverage of cases: 100.0
Instances selection time: 877.0
Test time: 1343.0
Accumulative iteration time: 29198.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 37
Labeled set size: 1532
Unlabelled set size: 2530
	
Mean region size: 50.03692762186115
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.013593721159768596
Relative absolute error: 0.1396145503272646
Root relative squared error: 2.718744231953719
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 877.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 6.98072751636323E-4
Coverage of cases: 100.0
Instances selection time: 876.0
Test time: 1352.0
Accumulative iteration time: 30075.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 38
Labeled set size: 1552
Unlabelled set size: 2510
	
Mean region size: 50.03692762186115
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.013593523155624966
Relative absolute error: 0.13833492450086493
Root relative squared error: 2.718704631124993
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 877.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 6.916746225043246E-4
Coverage of cases: 100.0
Instances selection time: 876.0
Test time: 1357.0
Accumulative iteration time: 30952.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 39
Labeled set size: 1572
Unlabelled set size: 2490
	
Mean region size: 50.03692762186115
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.013593358951504382
Relative absolute error: 0.13726014014030713
Root relative squared error: 2.7186717903008764
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 880.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 6.863007007015357E-4
Coverage of cases: 100.0
Instances selection time: 879.0
Test time: 1374.0
Accumulative iteration time: 31832.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 40
Labeled set size: 1592
Unlabelled set size: 2470
	
Mean region size: 50.03692762186115
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.013593195526424708
Relative absolute error: 0.13615527213160916
Root relative squared error: 2.7186391052849417
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 882.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 6.807763606580459E-4
Coverage of cases: 100.0
Instances selection time: 881.0
Test time: 1396.0
Accumulative iteration time: 32714.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 41
Labeled set size: 1612
Unlabelled set size: 2450
	
Mean region size: 50.03692762186115
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.013593017236836055
Relative absolute error: 0.134935036598904
Root relative squared error: 2.718603447367211
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 891.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 6.746751829945199E-4
Coverage of cases: 100.0
Instances selection time: 890.0
Test time: 1400.0
Accumulative iteration time: 33605.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 42
Labeled set size: 1632
Unlabelled set size: 2430
	
Mean region size: 50.03692762186115
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.01359286629922864
Relative absolute error: 0.13387419547864882
Root relative squared error: 2.718573259845728
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 887.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 6.693709773932441E-4
Coverage of cases: 100.0
Instances selection time: 886.0
Test time: 1413.0
Accumulative iteration time: 34492.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 43
Labeled set size: 1652
Unlabelled set size: 2410
	
Mean region size: 50.03692762186115
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.013592712107545757
Relative absolute error: 0.1327921543900285
Root relative squared error: 2.7185424215091514
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 888.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 6.639607719501425E-4
Coverage of cases: 100.0
Instances selection time: 887.0
Test time: 1425.0
Accumulative iteration time: 35380.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 44
Labeled set size: 1672
Unlabelled set size: 2390
	
Mean region size: 50.03692762186115
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.013592567764277844
Relative absolute error: 0.13172834252866392
Root relative squared error: 2.7185135528555686
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 897.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 6.586417126433195E-4
Coverage of cases: 100.0
Instances selection time: 896.0
Test time: 1528.0
Accumulative iteration time: 36277.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 45
Labeled set size: 1692
Unlabelled set size: 2370
	
Mean region size: 50.03692762186115
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.013592427807207867
Relative absolute error: 0.1307006803758938
Root relative squared error: 2.7184855614415735
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 885.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 6.53503401879469E-4
Coverage of cases: 100.0
Instances selection time: 884.0
Test time: 1449.0
Accumulative iteration time: 37162.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 46
Labeled set size: 1712
Unlabelled set size: 2350
	
Mean region size: 50.03692762186115
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.013592293561601429
Relative absolute error: 0.12967829544614531
Root relative squared error: 2.718458712320286
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 883.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 6.483914772307265E-4
Coverage of cases: 100.0
Instances selection time: 882.0
Test time: 1460.0
Accumulative iteration time: 38045.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Time end:Wed Nov 01 07.15.25 EET 2017