Wed Nov 01 07.05.27 EET 2017
Dataset: mushroom
Test set size: 4062
Initial Labelled set size: 812
Initial Unlabelled set size: 3250
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Wed Nov 01 07.05.27 EET 2017
		
Iteration: 1
Labeled set size: 812
Unlabelled set size: 3250
	
Mean region size: 50.33234859675037
Incorrectly Classified Instances: 0.32003938946331856
Correctly Classified Instances: 99.67996061053668
Weighted Precision: 0.996802903926376
Weighted AreaUnderROC: 0.9989545182676511
Root mean squared error: 0.052443669810572885
Relative absolute error: 0.6954702117183653
Root relative squared error: 10.488733962114576
Weighted TruePositiveRate: 0.9967996061053668
Weighted MatthewsCorrelation: 0.993594518373335
Weighted FMeasure: 0.9967997430157286
Iteration time: 22.0
Weighted AreaUnderPRC: 0.9984638300144435
Mean absolute error: 0.0034773510585918268
Coverage of cases: 99.9015263417036
Instances selection time: 20.0
Test time: 7.0
Accumulative iteration time: 22.0
Weighted Recall: 0.9967996061053668
Weighted FalsePositiveRate: 0.003120073564596183
Kappa statistic: 0.9935915001879855
Training time: 2.0
		
Iteration: 2
Labeled set size: 832
Unlabelled set size: 3230
	
Mean region size: 50.33234859675037
Incorrectly Classified Instances: 0.32003938946331856
Correctly Classified Instances: 99.67996061053668
Weighted Precision: 0.996802903926376
Weighted AreaUnderROC: 0.9989545182676511
Root mean squared error: 0.052443669810572885
Relative absolute error: 0.6954702117183653
Root relative squared error: 10.488733962114576
Weighted TruePositiveRate: 0.9967996061053668
Weighted MatthewsCorrelation: 0.993594518373335
Weighted FMeasure: 0.9967997430157286
Iteration time: 21.0
Weighted AreaUnderPRC: 0.9984638300144435
Mean absolute error: 0.0034773510585918268
Coverage of cases: 99.9015263417036
Instances selection time: 18.0
Test time: 7.0
Accumulative iteration time: 43.0
Weighted Recall: 0.9967996061053668
Weighted FalsePositiveRate: 0.003120073564596183
Kappa statistic: 0.9935915001879855
Training time: 3.0
		
Iteration: 3
Labeled set size: 852
Unlabelled set size: 3210
	
Mean region size: 50.33234859675037
Incorrectly Classified Instances: 0.32003938946331856
Correctly Classified Instances: 99.67996061053668
Weighted Precision: 0.996802903926376
Weighted AreaUnderROC: 0.9989545182676511
Root mean squared error: 0.052443669810572885
Relative absolute error: 0.6954702117183653
Root relative squared error: 10.488733962114576
Weighted TruePositiveRate: 0.9967996061053668
Weighted MatthewsCorrelation: 0.993594518373335
Weighted FMeasure: 0.9967997430157286
Iteration time: 20.0
Weighted AreaUnderPRC: 0.9984638300144435
Mean absolute error: 0.0034773510585918268
Coverage of cases: 99.9015263417036
Instances selection time: 18.0
Test time: 7.0
Accumulative iteration time: 63.0
Weighted Recall: 0.9967996061053668
Weighted FalsePositiveRate: 0.003120073564596183
Kappa statistic: 0.9935915001879855
Training time: 2.0
		
Iteration: 4
Labeled set size: 872
Unlabelled set size: 3190
	
Mean region size: 50.33234859675037
Incorrectly Classified Instances: 0.32003938946331856
Correctly Classified Instances: 99.67996061053668
Weighted Precision: 0.996802903926376
Weighted AreaUnderROC: 0.9989545182676511
Root mean squared error: 0.052443669810572885
Relative absolute error: 0.6954702117183653
Root relative squared error: 10.488733962114576
Weighted TruePositiveRate: 0.9967996061053668
Weighted MatthewsCorrelation: 0.993594518373335
Weighted FMeasure: 0.9967997430157286
Iteration time: 26.0
Weighted AreaUnderPRC: 0.9984638300144435
Mean absolute error: 0.0034773510585918268
Coverage of cases: 99.9015263417036
Instances selection time: 24.0
Test time: 8.0
Accumulative iteration time: 89.0
Weighted Recall: 0.9967996061053668
Weighted FalsePositiveRate: 0.003120073564596183
Kappa statistic: 0.9935915001879855
Training time: 2.0
		
Iteration: 5
Labeled set size: 892
Unlabelled set size: 3170
	
Mean region size: 50.33234859675037
Incorrectly Classified Instances: 0.32003938946331856
Correctly Classified Instances: 99.67996061053668
Weighted Precision: 0.996802903926376
Weighted AreaUnderROC: 0.9989545182676511
Root mean squared error: 0.052443669810572885
Relative absolute error: 0.6954702117183653
Root relative squared error: 10.488733962114576
Weighted TruePositiveRate: 0.9967996061053668
Weighted MatthewsCorrelation: 0.993594518373335
Weighted FMeasure: 0.9967997430157286
Iteration time: 20.0
Weighted AreaUnderPRC: 0.9984638300144435
Mean absolute error: 0.0034773510585918268
Coverage of cases: 99.9015263417036
Instances selection time: 18.0
Test time: 7.0
Accumulative iteration time: 109.0
Weighted Recall: 0.9967996061053668
Weighted FalsePositiveRate: 0.003120073564596183
Kappa statistic: 0.9935915001879855
Training time: 2.0
		
Iteration: 6
Labeled set size: 912
Unlabelled set size: 3150
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.09847365829640571
Correctly Classified Instances: 99.9015263417036
Weighted Precision: 0.999017131987402
Weighted AreaUnderROC: 0.9989785495403473
Root mean squared error: 0.03138051279001121
Relative absolute error: 0.19694731659281145
Root relative squared error: 6.276102558002242
Weighted TruePositiveRate: 0.999015263417036
Weighted MatthewsCorrelation: 0.9980297802902457
Weighted FMeasure: 0.9990152275591968
Iteration time: 20.0
Weighted AreaUnderPRC: 0.9985070665589048
Mean absolute error: 9.847365829640572E-4
Coverage of cases: 99.9015263417036
Instances selection time: 18.0
Test time: 7.0
Accumulative iteration time: 129.0
Weighted Recall: 0.999015263417036
Weighted FalsePositiveRate: 0.0010581643363413566
Kappa statistic: 0.9980278394111678
Training time: 2.0
		
Iteration: 7
Labeled set size: 932
Unlabelled set size: 3130
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.09847365829640571
Correctly Classified Instances: 99.9015263417036
Weighted Precision: 0.999017131987402
Weighted AreaUnderROC: 0.9989785495403473
Root mean squared error: 0.03138051279001121
Relative absolute error: 0.19694731659281145
Root relative squared error: 6.276102558002242
Weighted TruePositiveRate: 0.999015263417036
Weighted MatthewsCorrelation: 0.9980297802902457
Weighted FMeasure: 0.9990152275591968
Iteration time: 20.0
Weighted AreaUnderPRC: 0.9985070665589048
Mean absolute error: 9.847365829640572E-4
Coverage of cases: 99.9015263417036
Instances selection time: 17.0
Test time: 7.0
Accumulative iteration time: 149.0
Weighted Recall: 0.999015263417036
Weighted FalsePositiveRate: 0.0010581643363413566
Kappa statistic: 0.9980278394111678
Training time: 3.0
		
Iteration: 8
Labeled set size: 952
Unlabelled set size: 3110
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.09847365829640571
Correctly Classified Instances: 99.9015263417036
Weighted Precision: 0.999017131987402
Weighted AreaUnderROC: 0.9989785495403473
Root mean squared error: 0.03138051279001121
Relative absolute error: 0.19694731659281145
Root relative squared error: 6.276102558002242
Weighted TruePositiveRate: 0.999015263417036
Weighted MatthewsCorrelation: 0.9980297802902457
Weighted FMeasure: 0.9990152275591968
Iteration time: 19.0
Weighted AreaUnderPRC: 0.9985070665589048
Mean absolute error: 9.847365829640572E-4
Coverage of cases: 99.9015263417036
Instances selection time: 16.0
Test time: 7.0
Accumulative iteration time: 168.0
Weighted Recall: 0.999015263417036
Weighted FalsePositiveRate: 0.0010581643363413566
Kappa statistic: 0.9980278394111678
Training time: 3.0
		
Iteration: 9
Labeled set size: 972
Unlabelled set size: 3090
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.09847365829640571
Correctly Classified Instances: 99.9015263417036
Weighted Precision: 0.999017131987402
Weighted AreaUnderROC: 0.9989785495403473
Root mean squared error: 0.03138051279001121
Relative absolute error: 0.19694731659281145
Root relative squared error: 6.276102558002242
Weighted TruePositiveRate: 0.999015263417036
Weighted MatthewsCorrelation: 0.9980297802902457
Weighted FMeasure: 0.9990152275591968
Iteration time: 19.0
Weighted AreaUnderPRC: 0.9985070665589048
Mean absolute error: 9.847365829640572E-4
Coverage of cases: 99.9015263417036
Instances selection time: 16.0
Test time: 7.0
Accumulative iteration time: 187.0
Weighted Recall: 0.999015263417036
Weighted FalsePositiveRate: 0.0010581643363413566
Kappa statistic: 0.9980278394111678
Training time: 3.0
		
Iteration: 10
Labeled set size: 992
Unlabelled set size: 3070
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.09847365829640571
Correctly Classified Instances: 99.9015263417036
Weighted Precision: 0.999017131987402
Weighted AreaUnderROC: 0.9989785495403473
Root mean squared error: 0.03138051279001121
Relative absolute error: 0.19694731659281145
Root relative squared error: 6.276102558002242
Weighted TruePositiveRate: 0.999015263417036
Weighted MatthewsCorrelation: 0.9980297802902457
Weighted FMeasure: 0.9990152275591968
Iteration time: 19.0
Weighted AreaUnderPRC: 0.9985070665589048
Mean absolute error: 9.847365829640572E-4
Coverage of cases: 99.9015263417036
Instances selection time: 16.0
Test time: 8.0
Accumulative iteration time: 206.0
Weighted Recall: 0.999015263417036
Weighted FalsePositiveRate: 0.0010581643363413566
Kappa statistic: 0.9980278394111678
Training time: 3.0
		
Iteration: 11
Labeled set size: 1012
Unlabelled set size: 3050
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.09847365829640571
Correctly Classified Instances: 99.9015263417036
Weighted Precision: 0.999017131987402
Weighted AreaUnderROC: 0.9989785495403473
Root mean squared error: 0.03138051279001121
Relative absolute error: 0.19694731659281145
Root relative squared error: 6.276102558002242
Weighted TruePositiveRate: 0.999015263417036
Weighted MatthewsCorrelation: 0.9980297802902457
Weighted FMeasure: 0.9990152275591968
Iteration time: 19.0
Weighted AreaUnderPRC: 0.9985070665589048
Mean absolute error: 9.847365829640572E-4
Coverage of cases: 99.9015263417036
Instances selection time: 16.0
Test time: 7.0
Accumulative iteration time: 225.0
Weighted Recall: 0.999015263417036
Weighted FalsePositiveRate: 0.0010581643363413566
Kappa statistic: 0.9980278394111678
Training time: 3.0
		
Iteration: 12
Labeled set size: 1032
Unlabelled set size: 3030
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.09847365829640571
Correctly Classified Instances: 99.9015263417036
Weighted Precision: 0.999017131987402
Weighted AreaUnderROC: 0.9989785495403473
Root mean squared error: 0.03138051279001121
Relative absolute error: 0.19694731659281145
Root relative squared error: 6.276102558002242
Weighted TruePositiveRate: 0.999015263417036
Weighted MatthewsCorrelation: 0.9980297802902457
Weighted FMeasure: 0.9990152275591968
Iteration time: 19.0
Weighted AreaUnderPRC: 0.9985070665589048
Mean absolute error: 9.847365829640572E-4
Coverage of cases: 99.9015263417036
Instances selection time: 16.0
Test time: 7.0
Accumulative iteration time: 244.0
Weighted Recall: 0.999015263417036
Weighted FalsePositiveRate: 0.0010581643363413566
Kappa statistic: 0.9980278394111678
Training time: 3.0
		
Iteration: 13
Labeled set size: 1052
Unlabelled set size: 3010
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.09847365829640571
Correctly Classified Instances: 99.9015263417036
Weighted Precision: 0.999017131987402
Weighted AreaUnderROC: 0.9989785495403473
Root mean squared error: 0.03138051279001121
Relative absolute error: 0.19694731659281145
Root relative squared error: 6.276102558002242
Weighted TruePositiveRate: 0.999015263417036
Weighted MatthewsCorrelation: 0.9980297802902457
Weighted FMeasure: 0.9990152275591968
Iteration time: 18.0
Weighted AreaUnderPRC: 0.9985070665589048
Mean absolute error: 9.847365829640572E-4
Coverage of cases: 99.9015263417036
Instances selection time: 15.0
Test time: 8.0
Accumulative iteration time: 262.0
Weighted Recall: 0.999015263417036
Weighted FalsePositiveRate: 0.0010581643363413566
Kappa statistic: 0.9980278394111678
Training time: 3.0
		
Iteration: 14
Labeled set size: 1072
Unlabelled set size: 2990
	
Mean region size: 51.07090103397341
Incorrectly Classified Instances: 0.09847365829640571
Correctly Classified Instances: 99.9015263417036
Weighted Precision: 0.999017131987402
Weighted AreaUnderROC: 0.9999597051387115
Root mean squared error: 0.030799830761256492
Relative absolute error: 0.4562612834400126
Root relative squared error: 6.1599661522512985
Weighted TruePositiveRate: 0.999015263417036
Weighted MatthewsCorrelation: 0.9980297802902457
Weighted FMeasure: 0.9990152275591968
Iteration time: 18.0
Weighted AreaUnderPRC: 0.99992118153218
Mean absolute error: 0.002281306417200063
Coverage of cases: 100.0
Instances selection time: 15.0
Test time: 8.0
Accumulative iteration time: 280.0
Weighted Recall: 0.999015263417036
Weighted FalsePositiveRate: 0.0010581643363413566
Kappa statistic: 0.9980278394111678
Training time: 3.0
		
Iteration: 15
Labeled set size: 1092
Unlabelled set size: 2970
	
Mean region size: 51.07090103397341
Incorrectly Classified Instances: 0.09847365829640571
Correctly Classified Instances: 99.9015263417036
Weighted Precision: 0.999017131987402
Weighted AreaUnderROC: 0.9999597051387115
Root mean squared error: 0.03070824023712944
Relative absolute error: 0.42575375792857717
Root relative squared error: 6.141648047425888
Weighted TruePositiveRate: 0.999015263417036
Weighted MatthewsCorrelation: 0.9980297802902457
Weighted FMeasure: 0.9990152275591968
Iteration time: 19.0
Weighted AreaUnderPRC: 0.99992118153218
Mean absolute error: 0.002128768789642886
Coverage of cases: 100.0
Instances selection time: 16.0
Test time: 7.0
Accumulative iteration time: 299.0
Weighted Recall: 0.999015263417036
Weighted FalsePositiveRate: 0.0010581643363413566
Kappa statistic: 0.9980278394111678
Training time: 3.0
		
Iteration: 16
Labeled set size: 1112
Unlabelled set size: 2950
	
Mean region size: 51.07090103397341
Incorrectly Classified Instances: 0.09847365829640571
Correctly Classified Instances: 99.9015263417036
Weighted Precision: 0.999017131987402
Weighted AreaUnderROC: 0.9999597051387115
Root mean squared error: 0.03070824023712944
Relative absolute error: 0.42575375792857717
Root relative squared error: 6.141648047425888
Weighted TruePositiveRate: 0.999015263417036
Weighted MatthewsCorrelation: 0.9980297802902457
Weighted FMeasure: 0.9990152275591968
Iteration time: 18.0
Weighted AreaUnderPRC: 0.99992118153218
Mean absolute error: 0.002128768789642886
Coverage of cases: 100.0
Instances selection time: 15.0
Test time: 7.0
Accumulative iteration time: 317.0
Weighted Recall: 0.999015263417036
Weighted FalsePositiveRate: 0.0010581643363413566
Kappa statistic: 0.9980278394111678
Training time: 3.0
		
Iteration: 17
Labeled set size: 1132
Unlabelled set size: 2930
	
Mean region size: 51.07090103397341
Incorrectly Classified Instances: 0.09847365829640571
Correctly Classified Instances: 99.9015263417036
Weighted Precision: 0.999017131987402
Weighted AreaUnderROC: 0.9999597051387115
Root mean squared error: 0.03070824023712944
Relative absolute error: 0.42575375792857717
Root relative squared error: 6.141648047425888
Weighted TruePositiveRate: 0.999015263417036
Weighted MatthewsCorrelation: 0.9980297802902457
Weighted FMeasure: 0.9990152275591968
Iteration time: 19.0
Weighted AreaUnderPRC: 0.99992118153218
Mean absolute error: 0.002128768789642886
Coverage of cases: 100.0
Instances selection time: 15.0
Test time: 7.0
Accumulative iteration time: 336.0
Weighted Recall: 0.999015263417036
Weighted FalsePositiveRate: 0.0010581643363413566
Kappa statistic: 0.9980278394111678
Training time: 4.0
		
Iteration: 18
Labeled set size: 1152
Unlabelled set size: 2910
	
Mean region size: 51.07090103397341
Incorrectly Classified Instances: 0.09847365829640571
Correctly Classified Instances: 99.9015263417036
Weighted Precision: 0.999017131987402
Weighted AreaUnderROC: 0.9999597051387115
Root mean squared error: 0.03070824023712944
Relative absolute error: 0.42575375792857717
Root relative squared error: 6.141648047425888
Weighted TruePositiveRate: 0.999015263417036
Weighted MatthewsCorrelation: 0.9980297802902457
Weighted FMeasure: 0.9990152275591968
Iteration time: 18.0
Weighted AreaUnderPRC: 0.99992118153218
Mean absolute error: 0.002128768789642886
Coverage of cases: 100.0
Instances selection time: 15.0
Test time: 7.0
Accumulative iteration time: 354.0
Weighted Recall: 0.999015263417036
Weighted FalsePositiveRate: 0.0010581643363413566
Kappa statistic: 0.9980278394111678
Training time: 3.0
		
Iteration: 19
Labeled set size: 1172
Unlabelled set size: 2890
	
Mean region size: 50.418513047759724
Incorrectly Classified Instances: 0.2708025603151157
Correctly Classified Instances: 99.72919743968488
Weighted Precision: 0.9973060586907659
Weighted AreaUnderROC: 0.9999692933737769
Root mean squared error: 0.045881648340024284
Relative absolute error: 0.6260111134557222
Root relative squared error: 9.176329668004858
Weighted TruePositiveRate: 0.9972919743968488
Weighted MatthewsCorrelation: 0.9945905165650267
Weighted FMeasure: 0.9972916905300894
Iteration time: 23.0
Weighted AreaUnderPRC: 0.9999391100380275
Mean absolute error: 0.0031300555672786113
Coverage of cases: 100.0
Instances selection time: 20.0
Test time: 8.0
Accumulative iteration time: 377.0
Weighted Recall: 0.9972919743968488
Weighted FalsePositiveRate: 0.0029099519249387307
Kappa statistic: 0.994575885524745
Training time: 3.0
		
Iteration: 20
Labeled set size: 1192
Unlabelled set size: 2870
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 17.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 14.0
Test time: 8.0
Accumulative iteration time: 394.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 3.0
		
Iteration: 21
Labeled set size: 1212
Unlabelled set size: 2850
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 18.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 14.0
Test time: 7.0
Accumulative iteration time: 412.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 4.0
		
Iteration: 22
Labeled set size: 1232
Unlabelled set size: 2830
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 18.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 14.0
Test time: 7.0
Accumulative iteration time: 430.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 4.0
		
Iteration: 23
Labeled set size: 1252
Unlabelled set size: 2810
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 17.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 14.0
Test time: 9.0
Accumulative iteration time: 447.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 3.0
		
Iteration: 24
Labeled set size: 1272
Unlabelled set size: 2790
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 18.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 14.0
Test time: 7.0
Accumulative iteration time: 465.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 4.0
		
Iteration: 25
Labeled set size: 1292
Unlabelled set size: 2770
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 17.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 13.0
Test time: 7.0
Accumulative iteration time: 482.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 4.0
		
Iteration: 26
Labeled set size: 1312
Unlabelled set size: 2750
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 17.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 13.0
Test time: 7.0
Accumulative iteration time: 499.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 4.0
		
Iteration: 27
Labeled set size: 1332
Unlabelled set size: 2730
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 17.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 14.0
Test time: 7.0
Accumulative iteration time: 516.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 3.0
		
Iteration: 28
Labeled set size: 1352
Unlabelled set size: 2710
	
Mean region size: 50.418513047759724
Incorrectly Classified Instances: 0.2708025603151157
Correctly Classified Instances: 99.72919743968488
Weighted Precision: 0.9973060586907659
Weighted AreaUnderROC: 0.9999692933737769
Root mean squared error: 0.04649520975098705
Relative absolute error: 0.6154603643525357
Root relative squared error: 9.299041950197411
Weighted TruePositiveRate: 0.9972919743968488
Weighted MatthewsCorrelation: 0.9945905165650267
Weighted FMeasure: 0.9972916905300894
Iteration time: 16.0
Weighted AreaUnderPRC: 0.9999391100380275
Mean absolute error: 0.0030773018217626785
Coverage of cases: 100.0
Instances selection time: 13.0
Test time: 8.0
Accumulative iteration time: 532.0
Weighted Recall: 0.9972919743968488
Weighted FalsePositiveRate: 0.0029099519249387307
Kappa statistic: 0.994575885524745
Training time: 3.0
		
Iteration: 29
Labeled set size: 1372
Unlabelled set size: 2690
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 17.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 13.0
Test time: 7.0
Accumulative iteration time: 549.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 4.0
		
Iteration: 30
Labeled set size: 1392
Unlabelled set size: 2670
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 17.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 13.0
Test time: 7.0
Accumulative iteration time: 566.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 4.0
		
Iteration: 31
Labeled set size: 1412
Unlabelled set size: 2650
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 16.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 12.0
Test time: 9.0
Accumulative iteration time: 582.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 4.0
		
Iteration: 32
Labeled set size: 1432
Unlabelled set size: 2630
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 16.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 12.0
Test time: 7.0
Accumulative iteration time: 598.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 4.0
		
Iteration: 33
Labeled set size: 1452
Unlabelled set size: 2610
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.09847365829640571
Correctly Classified Instances: 99.9015263417036
Weighted Precision: 0.999017131987402
Weighted AreaUnderROC: 0.9994164527317003
Root mean squared error: 0.031336595345963746
Relative absolute error: 0.47002935973886656
Root relative squared error: 6.267319069192749
Weighted TruePositiveRate: 0.999015263417036
Weighted MatthewsCorrelation: 0.9980297802902457
Weighted FMeasure: 0.9990152275591968
Iteration time: 18.0
Weighted AreaUnderPRC: 0.9990639207528427
Mean absolute error: 0.002350146798694333
Coverage of cases: 99.9015263417036
Instances selection time: 14.0
Test time: 8.0
Accumulative iteration time: 616.0
Weighted Recall: 0.999015263417036
Weighted FalsePositiveRate: 0.0010581643363413566
Kappa statistic: 0.9980278394111678
Training time: 4.0
		
Iteration: 34
Labeled set size: 1472
Unlabelled set size: 2590
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.09847365829640571
Correctly Classified Instances: 99.9015263417036
Weighted Precision: 0.999017131987402
Weighted AreaUnderROC: 0.9994164527317003
Root mean squared error: 0.03133607556365049
Relative absolute error: 0.4675240198934812
Root relative squared error: 6.267215112730098
Weighted TruePositiveRate: 0.999015263417036
Weighted MatthewsCorrelation: 0.9980297802902457
Weighted FMeasure: 0.9990152275591968
Iteration time: 16.0
Weighted AreaUnderPRC: 0.9990639207528427
Mean absolute error: 0.002337620099467406
Coverage of cases: 99.9015263417036
Instances selection time: 12.0
Test time: 7.0
Accumulative iteration time: 632.0
Weighted Recall: 0.999015263417036
Weighted FalsePositiveRate: 0.0010581643363413566
Kappa statistic: 0.9980278394111678
Training time: 4.0
		
Iteration: 35
Labeled set size: 1492
Unlabelled set size: 2570
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.09847365829640571
Correctly Classified Instances: 99.9015263417036
Weighted Precision: 0.999017131987402
Weighted AreaUnderROC: 0.9994164527317003
Root mean squared error: 0.03133558185284254
Relative absolute error: 0.46506423168166405
Root relative squared error: 6.267116370568508
Weighted TruePositiveRate: 0.999015263417036
Weighted MatthewsCorrelation: 0.9980297802902457
Weighted FMeasure: 0.9990152275591968
Iteration time: 16.0
Weighted AreaUnderPRC: 0.9990639207528427
Mean absolute error: 0.0023253211584083203
Coverage of cases: 99.9015263417036
Instances selection time: 12.0
Test time: 7.0
Accumulative iteration time: 648.0
Weighted Recall: 0.999015263417036
Weighted FalsePositiveRate: 0.0010581643363413566
Kappa statistic: 0.9980278394111678
Training time: 4.0
		
Iteration: 36
Labeled set size: 1512
Unlabelled set size: 2550
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 16.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 12.0
Test time: 7.0
Accumulative iteration time: 664.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 4.0
		
Iteration: 37
Labeled set size: 1532
Unlabelled set size: 2530
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 16.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 12.0
Test time: 7.0
Accumulative iteration time: 680.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 4.0
		
Iteration: 38
Labeled set size: 1552
Unlabelled set size: 2510
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 15.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 11.0
Test time: 7.0
Accumulative iteration time: 695.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 4.0
		
Iteration: 39
Labeled set size: 1572
Unlabelled set size: 2490
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 15.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 11.0
Test time: 7.0
Accumulative iteration time: 710.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 4.0
		
Iteration: 40
Labeled set size: 1592
Unlabelled set size: 2470
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 16.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 11.0
Test time: 7.0
Accumulative iteration time: 726.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 5.0
		
Iteration: 41
Labeled set size: 1612
Unlabelled set size: 2450
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 15.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 11.0
Test time: 7.0
Accumulative iteration time: 741.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 4.0
		
Iteration: 42
Labeled set size: 1632
Unlabelled set size: 2430
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 15.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 10.0
Test time: 7.0
Accumulative iteration time: 756.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 5.0
		
Iteration: 43
Labeled set size: 1652
Unlabelled set size: 2410
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 15.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 11.0
Test time: 7.0
Accumulative iteration time: 771.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 4.0
		
Iteration: 44
Labeled set size: 1672
Unlabelled set size: 2390
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 15.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 11.0
Test time: 7.0
Accumulative iteration time: 786.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 4.0
		
Iteration: 45
Labeled set size: 1692
Unlabelled set size: 2370
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 14.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 10.0
Test time: 7.0
Accumulative iteration time: 800.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 4.0
		
Iteration: 46
Labeled set size: 1712
Unlabelled set size: 2350
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 15.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 10.0
Test time: 7.0
Accumulative iteration time: 815.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 5.0
		
Time end:Wed Nov 01 07.05.33 EET 2017