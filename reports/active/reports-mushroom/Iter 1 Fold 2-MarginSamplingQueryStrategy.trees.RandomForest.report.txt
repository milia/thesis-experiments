Wed Nov 01 06.49.33 EET 2017
Dataset: mushroom
Test set size: 4062
Initial Labelled set size: 812
Initial Unlabelled set size: 3250
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Wed Nov 01 06.49.33 EET 2017
		
Iteration: 1
Labeled set size: 812
Unlabelled set size: 3250
	
Mean region size: 54.65288035450517
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.047281580536822944
Relative absolute error: 2.64780762660563
Root relative squared error: 9.45631610736459
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 16.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.013239038133028149
Coverage of cases: 100.0
Instances selection time: 7.0
Test time: 13.0
Accumulative iteration time: 16.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 9.0
		
Iteration: 2
Labeled set size: 832
Unlabelled set size: 3230
	
Mean region size: 53.24963072378139
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.032952488981308675
Relative absolute error: 1.5702911824710861
Root relative squared error: 6.590497796261735
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 15.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.007851455912355431
Coverage of cases: 100.0
Instances selection time: 7.0
Test time: 11.0
Accumulative iteration time: 31.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 8.0
		
Iteration: 3
Labeled set size: 852
Unlabelled set size: 3210
	
Mean region size: 54.14820285573609
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.03660881729643799
Relative absolute error: 2.057121717727542
Root relative squared error: 7.321763459287597
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 16.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.01028560858863771
Coverage of cases: 100.0
Instances selection time: 7.0
Test time: 11.0
Accumulative iteration time: 47.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 9.0
		
Iteration: 4
Labeled set size: 872
Unlabelled set size: 3190
	
Mean region size: 54.87444608567208
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.041101878524228
Relative absolute error: 2.4624399705009594
Root relative squared error: 8.220375704845601
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 17.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.012312199852504797
Coverage of cases: 100.0
Instances selection time: 7.0
Test time: 13.0
Accumulative iteration time: 64.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 10.0
		
Iteration: 5
Labeled set size: 892
Unlabelled set size: 3170
	
Mean region size: 51.784835056622356
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.02360599911681692
Relative absolute error: 0.9690184927704306
Root relative squared error: 4.721199823363384
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 14.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.004845092463852153
Coverage of cases: 100.0
Instances selection time: 6.0
Test time: 12.0
Accumulative iteration time: 78.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 8.0
		
Iteration: 6
Labeled set size: 912
Unlabelled set size: 3150
	
Mean region size: 51.944854751354015
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.02194133742251726
Relative absolute error: 0.9583720481120517
Root relative squared error: 4.3882674845034515
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 16.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0047918602405602586
Coverage of cases: 100.0
Instances selection time: 6.0
Test time: 12.0
Accumulative iteration time: 94.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 10.0
		
Iteration: 7
Labeled set size: 932
Unlabelled set size: 3130
	
Mean region size: 52.15411127523387
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.025223778801697884
Relative absolute error: 1.0672461652484804
Root relative squared error: 5.0447557603395765
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 17.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.005336230826242402
Coverage of cases: 100.0
Instances selection time: 7.0
Test time: 12.0
Accumulative iteration time: 111.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 10.0
		
Iteration: 8
Labeled set size: 952
Unlabelled set size: 3110
	
Mean region size: 51.76021664204825
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.020768544657516787
Relative absolute error: 0.816508021778514
Root relative squared error: 4.153708931503357
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 16.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.00408254010889257
Coverage of cases: 100.0
Instances selection time: 7.0
Test time: 11.0
Accumulative iteration time: 127.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 9.0
		
Iteration: 9
Labeled set size: 972
Unlabelled set size: 3090
	
Mean region size: 51.89561792220581
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.022064549031909853
Relative absolute error: 0.9180586935035886
Root relative squared error: 4.41290980638197
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 18.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.004590293467517943
Coverage of cases: 100.0
Instances selection time: 7.0
Test time: 12.0
Accumulative iteration time: 145.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 11.0
		
Iteration: 10
Labeled set size: 992
Unlabelled set size: 3070
	
Mean region size: 53.06499261447563
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.02519130175087816
Relative absolute error: 1.1941036192136132
Root relative squared error: 5.038260350175632
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 16.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.005970518096068066
Coverage of cases: 100.0
Instances selection time: 6.0
Test time: 12.0
Accumulative iteration time: 161.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 10.0
		
Iteration: 11
Labeled set size: 1012
Unlabelled set size: 3050
	
Mean region size: 51.698670605612996
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.017841967517304932
Relative absolute error: 0.7704542575602343
Root relative squared error: 3.568393503460986
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 18.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0038522712878011718
Coverage of cases: 100.0
Instances selection time: 6.0
Test time: 12.0
Accumulative iteration time: 179.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 12.0
		
Iteration: 12
Labeled set size: 1032
Unlabelled set size: 3030
	
Mean region size: 52.44953225012309
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.025853014490264115
Relative absolute error: 1.1763059625052847
Root relative squared error: 5.170602898052823
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 18.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.005881529812526424
Coverage of cases: 100.0
Instances selection time: 6.0
Test time: 13.0
Accumulative iteration time: 197.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 12.0
		
Iteration: 13
Labeled set size: 1052
Unlabelled set size: 3010
	
Mean region size: 51.834071885770555
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.019024598213417343
Relative absolute error: 0.7817882997438439
Root relative squared error: 3.8049196426834686
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 19.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0039089414987192195
Coverage of cases: 100.0
Instances selection time: 6.0
Test time: 12.0
Accumulative iteration time: 216.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 13.0
		
Iteration: 14
Labeled set size: 1072
Unlabelled set size: 2990
	
Mean region size: 51.698670605612996
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0190239670481927
Relative absolute error: 0.7395701022781218
Root relative squared error: 3.8047934096385396
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 17.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.003697850511390609
Coverage of cases: 100.0
Instances selection time: 6.0
Test time: 16.0
Accumulative iteration time: 233.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 11.0
		
Iteration: 15
Labeled set size: 1092
Unlabelled set size: 2970
	
Mean region size: 51.698670605612996
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.01913191776458299
Relative absolute error: 0.8023189596158472
Root relative squared error: 3.8263835529165977
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 19.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.004011594798079236
Coverage of cases: 100.0
Instances selection time: 6.0
Test time: 13.0
Accumulative iteration time: 252.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 13.0
		
Iteration: 16
Labeled set size: 1112
Unlabelled set size: 2950
	
Mean region size: 51.41555883801083
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.016603826743782073
Relative absolute error: 0.5657067921832485
Root relative squared error: 3.3207653487564146
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 18.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0028285339609162422
Coverage of cases: 100.0
Instances selection time: 6.0
Test time: 12.0
Accumulative iteration time: 270.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 12.0
		
Iteration: 17
Labeled set size: 1132
Unlabelled set size: 2930
	
Mean region size: 51.46479566715904
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.017416251864457253
Relative absolute error: 0.600911294471346
Root relative squared error: 3.4832503728914506
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 17.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.00300455647235673
Coverage of cases: 100.0
Instances selection time: 5.0
Test time: 13.0
Accumulative iteration time: 287.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 12.0
		
Iteration: 18
Labeled set size: 1152
Unlabelled set size: 2910
	
Mean region size: 50.775480059084195
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.012427089948655193
Relative absolute error: 0.3620669275966706
Root relative squared error: 2.4854179897310384
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 18.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.001810334637983353
Coverage of cases: 100.0
Instances selection time: 6.0
Test time: 12.0
Accumulative iteration time: 305.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 12.0
		
Iteration: 19
Labeled set size: 1172
Unlabelled set size: 2890
	
Mean region size: 51.267848350566226
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.018480503111979872
Relative absolute error: 0.6009882901697559
Root relative squared error: 3.6961006223959743
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 20.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.003004941450848779
Coverage of cases: 100.0
Instances selection time: 6.0
Test time: 13.0
Accumulative iteration time: 325.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 14.0
		
Iteration: 20
Labeled set size: 1192
Unlabelled set size: 2870
	
Mean region size: 51.02166420482521
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.01613043716972661
Relative absolute error: 0.4892554853925353
Root relative squared error: 3.2260874339453216
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 18.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0024462774269626768
Coverage of cases: 100.0
Instances selection time: 5.0
Test time: 12.0
Accumulative iteration time: 343.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 13.0
		
Iteration: 21
Labeled set size: 1212
Unlabelled set size: 2850
	
Mean region size: 52.215657311669126
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.023820785915837816
Relative absolute error: 1.0318731877161642
Root relative squared error: 4.764157183167563
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 19.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0051593659385808215
Coverage of cases: 100.0
Instances selection time: 6.0
Test time: 12.0
Accumulative iteration time: 362.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 13.0
		
Iteration: 22
Labeled set size: 1232
Unlabelled set size: 2830
	
Mean region size: 51.07090103397341
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.013489558982066911
Relative absolute error: 0.40823257512941963
Root relative squared error: 2.6979117964133823
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 19.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0020411628756470982
Coverage of cases: 100.0
Instances selection time: 6.0
Test time: 12.0
Accumulative iteration time: 381.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 13.0
		
Iteration: 23
Labeled set size: 1252
Unlabelled set size: 2810
	
Mean region size: 50.72624322993599
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.01278808600269764
Relative absolute error: 0.3392423396412124
Root relative squared error: 2.557617200539528
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 19.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.001696211698206062
Coverage of cases: 100.0
Instances selection time: 5.0
Test time: 12.0
Accumulative iteration time: 400.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 14.0
		
Iteration: 24
Labeled set size: 1272
Unlabelled set size: 2790
	
Mean region size: 51.41555883801083
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.01563321062150849
Relative absolute error: 0.518389310111417
Root relative squared error: 3.1266421243016977
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 18.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0025919465505570847
Coverage of cases: 100.0
Instances selection time: 5.0
Test time: 12.0
Accumulative iteration time: 418.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 13.0
		
Iteration: 25
Labeled set size: 1292
Unlabelled set size: 2770
	
Mean region size: 50.640078778926636
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.011918507176604354
Relative absolute error: 0.3041728490208088
Root relative squared error: 2.3837014353208708
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 18.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.001520864245104044
Coverage of cases: 100.0
Instances selection time: 6.0
Test time: 12.0
Accumulative iteration time: 436.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 12.0
		
Iteration: 26
Labeled set size: 1312
Unlabelled set size: 2750
	
Mean region size: 50.76317085179714
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.01023063029796101
Relative absolute error: 0.27005823823387154
Root relative squared error: 2.046126059592202
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 19.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0013502911911693577
Coverage of cases: 100.0
Instances selection time: 5.0
Test time: 12.0
Accumulative iteration time: 455.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 14.0
		
Iteration: 27
Labeled set size: 1332
Unlabelled set size: 2730
	
Mean region size: 50.123092072870506
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.004989603122394941
Relative absolute error: 0.05615343133806947
Root relative squared error: 0.9979206244789882
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 19.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 2.8076715669034735E-4
Coverage of cases: 100.0
Instances selection time: 5.0
Test time: 12.0
Accumulative iteration time: 474.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 14.0
		
Iteration: 28
Labeled set size: 1352
Unlabelled set size: 2710
	
Mean region size: 50.29542097488922
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.007545728268305359
Relative absolute error: 0.13200665891524357
Root relative squared error: 1.5091456536610717
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 21.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 6.600332945762179E-4
Coverage of cases: 100.0
Instances selection time: 6.0
Test time: 13.0
Accumulative iteration time: 495.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 15.0
		
Iteration: 29
Labeled set size: 1372
Unlabelled set size: 2690
	
Mean region size: 50.83702609551945
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.013269044712441709
Relative absolute error: 0.37043774015926195
Root relative squared error: 2.6538089424883418
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 31.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0018521887007963097
Coverage of cases: 100.0
Instances selection time: 5.0
Test time: 12.0
Accumulative iteration time: 526.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 26.0
		
Iteration: 30
Labeled set size: 1392
Unlabelled set size: 2670
	
Mean region size: 51.28015755785327
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0193794366422661
Relative absolute error: 0.6741946139786262
Root relative squared error: 3.8758873284532203
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 21.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.003370973069893131
Coverage of cases: 100.0
Instances selection time: 6.0
Test time: 12.0
Accumulative iteration time: 547.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 15.0
		
Iteration: 31
Labeled set size: 1412
Unlabelled set size: 2650
	
Mean region size: 50.800098473658295
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.014528749949371655
Relative absolute error: 0.3804782407090592
Root relative squared error: 2.905749989874331
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 21.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.001902391203545296
Coverage of cases: 100.0
Instances selection time: 5.0
Test time: 12.0
Accumulative iteration time: 568.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 16.0
		
Iteration: 32
Labeled set size: 1432
Unlabelled set size: 2630
	
Mean region size: 50.51698670605613
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.011275059421815304
Relative absolute error: 0.2848752166366489
Root relative squared error: 2.2550118843630607
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 23.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0014243760831832446
Coverage of cases: 100.0
Instances selection time: 5.0
Test time: 13.0
Accumulative iteration time: 591.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 18.0
		
Iteration: 33
Labeled set size: 1452
Unlabelled set size: 2610
	
Mean region size: 50.68931560807484
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.010807088026448948
Relative absolute error: 0.2974915880503626
Root relative squared error: 2.1614176052897895
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 21.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.001487457940251813
Coverage of cases: 100.0
Instances selection time: 5.0
Test time: 13.0
Accumulative iteration time: 612.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 16.0
		
Iteration: 34
Labeled set size: 1472
Unlabelled set size: 2590
	
Mean region size: 51.21861152141802
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.01487703895768008
Relative absolute error: 0.49250922919738893
Root relative squared error: 2.975407791536016
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 20.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0024625461459869447
Coverage of cases: 100.0
Instances selection time: 5.0
Test time: 12.0
Accumulative iteration time: 632.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 15.0
		
Iteration: 35
Labeled set size: 1492
Unlabelled set size: 2570
	
Mean region size: 50.45544066962088
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.009376458024489182
Relative absolute error: 0.19769328819570506
Root relative squared error: 1.8752916048978365
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 21.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 9.884664409785253E-4
Coverage of cases: 100.0
Instances selection time: 5.0
Test time: 17.0
Accumulative iteration time: 653.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 16.0
		
Iteration: 36
Labeled set size: 1512
Unlabelled set size: 2550
	
Mean region size: 50.615460364352536
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.010908559623315679
Relative absolute error: 0.26419768344046873
Root relative squared error: 2.181711924663136
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 20.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0013209884172023438
Coverage of cases: 100.0
Instances selection time: 5.0
Test time: 12.0
Accumulative iteration time: 673.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 15.0
		
Iteration: 37
Labeled set size: 1532
Unlabelled set size: 2530
	
Mean region size: 50.984736582964054
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.012285664952684133
Relative absolute error: 0.3446863955596248
Root relative squared error: 2.4571329905368264
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 20.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.001723431977798124
Coverage of cases: 100.0
Instances selection time: 5.0
Test time: 12.0
Accumulative iteration time: 693.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 15.0
		
Iteration: 38
Labeled set size: 1552
Unlabelled set size: 2510
	
Mean region size: 50.8493353028065
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.013347149711900578
Relative absolute error: 0.37206798031866584
Root relative squared error: 2.6694299423801153
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 20.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0018603399015933292
Coverage of cases: 100.0
Instances selection time: 4.0
Test time: 12.0
Accumulative iteration time: 713.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 16.0
		
Iteration: 39
Labeled set size: 1572
Unlabelled set size: 2490
	
Mean region size: 50.16001969473166
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.00787718519859026
Relative absolute error: 0.1207750456164741
Root relative squared error: 1.5754370397180522
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 20.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 6.038752280823705E-4
Coverage of cases: 100.0
Instances selection time: 5.0
Test time: 12.0
Accumulative iteration time: 733.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 15.0
		
Iteration: 40
Labeled set size: 1592
Unlabelled set size: 2470
	
Mean region size: 51.25553914327917
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.014167762725704787
Relative absolute error: 0.4536390485264349
Root relative squared error: 2.8335525451409573
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 22.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0022681952426321745
Coverage of cases: 100.0
Instances selection time: 5.0
Test time: 12.0
Accumulative iteration time: 755.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 17.0
		
Iteration: 41
Labeled set size: 1612
Unlabelled set size: 2450
	
Mean region size: 50.50467749876908
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.010353805932137361
Relative absolute error: 0.2821435991593097
Root relative squared error: 2.070761186427472
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 23.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0014107179957965484
Coverage of cases: 100.0
Instances selection time: 5.0
Test time: 12.0
Accumulative iteration time: 778.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 18.0
		
Iteration: 42
Labeled set size: 1632
Unlabelled set size: 2430
	
Mean region size: 50.443131462333824
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.010235854195890749
Relative absolute error: 0.22020827369938276
Root relative squared error: 2.0471708391781496
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 21.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0011010413684969138
Coverage of cases: 100.0
Instances selection time: 4.0
Test time: 12.0
Accumulative iteration time: 799.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 17.0
		
Iteration: 43
Labeled set size: 1652
Unlabelled set size: 2410
	
Mean region size: 50.57853274249138
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.011100959206752012
Relative absolute error: 0.24542021265898983
Root relative squared error: 2.2201918413504025
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 21.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0012271010632949491
Coverage of cases: 100.0
Instances selection time: 4.0
Test time: 13.0
Accumulative iteration time: 820.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 17.0
		
Iteration: 44
Labeled set size: 1672
Unlabelled set size: 2390
	
Mean region size: 51.944854751354015
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.016922825831134844
Relative absolute error: 0.661815032390189
Root relative squared error: 3.384565166226969
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 22.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.003309075161950945
Coverage of cases: 100.0
Instances selection time: 5.0
Test time: 12.0
Accumulative iteration time: 842.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 17.0
		
Iteration: 45
Labeled set size: 1692
Unlabelled set size: 2370
	
Mean region size: 51.08321024126046
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.014335226954855786
Relative absolute error: 0.4169420957586738
Root relative squared error: 2.8670453909711573
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 22.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.002084710478793369
Coverage of cases: 100.0
Instances selection time: 5.0
Test time: 11.0
Accumulative iteration time: 864.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 17.0
		
Iteration: 46
Labeled set size: 1712
Unlabelled set size: 2350
	
Mean region size: 51.00935499753816
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.012764530871736542
Relative absolute error: 0.3773288089638803
Root relative squared error: 2.5529061743473083
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 23.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0018866440448194017
Coverage of cases: 100.0
Instances selection time: 5.0
Test time: 12.0
Accumulative iteration time: 887.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 18.0
		
Time end:Wed Nov 01 06.49.38 EET 2017