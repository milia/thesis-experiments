Wed Nov 01 07.08.21 EET 2017
Dataset: mushroom
Test set size: 4062
Initial Labelled set size: 812
Initial Unlabelled set size: 3250
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Wed Nov 01 07.08.21 EET 2017
		
Iteration: 1
Labeled set size: 812
Unlabelled set size: 3250
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 0.9992339121552605
Root mean squared error: 0.0271763212599322
Relative absolute error: 0.14771048744460857
Root relative squared error: 5.43526425198644
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 195.0
Weighted AreaUnderPRC: 0.9988799500610819
Mean absolute error: 7.385524372230429E-4
Coverage of cases: 99.9261447562777
Instances selection time: 24.0
Test time: 30.0
Accumulative iteration time: 195.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 171.0
		
Iteration: 2
Labeled set size: 832
Unlabelled set size: 3230
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 0.9992339121552605
Root mean squared error: 0.0271763212599322
Relative absolute error: 0.14771048744460857
Root relative squared error: 5.43526425198644
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 161.0
Weighted AreaUnderPRC: 0.9988799500610819
Mean absolute error: 7.385524372230429E-4
Coverage of cases: 99.9261447562777
Instances selection time: 21.0
Test time: 29.0
Accumulative iteration time: 356.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 140.0
		
Iteration: 3
Labeled set size: 852
Unlabelled set size: 3210
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 0.9992339121552605
Root mean squared error: 0.0271763212599322
Relative absolute error: 0.14771048744460857
Root relative squared error: 5.43526425198644
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 212.0
Weighted AreaUnderPRC: 0.9988799500610819
Mean absolute error: 7.385524372230429E-4
Coverage of cases: 99.9261447562777
Instances selection time: 21.0
Test time: 28.0
Accumulative iteration time: 568.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 191.0
		
Iteration: 4
Labeled set size: 872
Unlabelled set size: 3190
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 0.9992339121552605
Root mean squared error: 0.0271763212599322
Relative absolute error: 0.14771048744460857
Root relative squared error: 5.43526425198644
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 223.0
Weighted AreaUnderPRC: 0.9988799500610819
Mean absolute error: 7.385524372230429E-4
Coverage of cases: 99.9261447562777
Instances selection time: 21.0
Test time: 29.0
Accumulative iteration time: 791.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 202.0
		
Iteration: 5
Labeled set size: 892
Unlabelled set size: 3170
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 0.9992339121552605
Root mean squared error: 0.0271763212599322
Relative absolute error: 0.14771048744460857
Root relative squared error: 5.43526425198644
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 196.0
Weighted AreaUnderPRC: 0.9988799500610819
Mean absolute error: 7.385524372230429E-4
Coverage of cases: 99.9261447562777
Instances selection time: 21.0
Test time: 29.0
Accumulative iteration time: 987.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 175.0
		
Iteration: 6
Labeled set size: 912
Unlabelled set size: 3150
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 0.9992339121552605
Root mean squared error: 0.0271763212599322
Relative absolute error: 0.14771048744460857
Root relative squared error: 5.43526425198644
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 218.0
Weighted AreaUnderPRC: 0.9988799500610819
Mean absolute error: 7.385524372230429E-4
Coverage of cases: 99.9261447562777
Instances selection time: 21.0
Test time: 28.0
Accumulative iteration time: 1205.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 197.0
		
Iteration: 7
Labeled set size: 932
Unlabelled set size: 3130
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 0.9992339121552605
Root mean squared error: 0.0271763212599322
Relative absolute error: 0.14771048744460857
Root relative squared error: 5.43526425198644
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 204.0
Weighted AreaUnderPRC: 0.9988799500610819
Mean absolute error: 7.385524372230429E-4
Coverage of cases: 99.9261447562777
Instances selection time: 21.0
Test time: 28.0
Accumulative iteration time: 1409.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 183.0
		
Iteration: 8
Labeled set size: 952
Unlabelled set size: 3110
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 190.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 22.0
Test time: 28.0
Accumulative iteration time: 1599.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 168.0
		
Iteration: 9
Labeled set size: 972
Unlabelled set size: 3090
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 248.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 20.0
Test time: 28.0
Accumulative iteration time: 1847.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 228.0
		
Iteration: 10
Labeled set size: 992
Unlabelled set size: 3070
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 227.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 21.0
Test time: 28.0
Accumulative iteration time: 2074.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 206.0
		
Iteration: 11
Labeled set size: 1012
Unlabelled set size: 3050
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 235.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 21.0
Test time: 28.0
Accumulative iteration time: 2309.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 214.0
		
Iteration: 12
Labeled set size: 1032
Unlabelled set size: 3030
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 285.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 20.0
Test time: 28.0
Accumulative iteration time: 2594.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 265.0
		
Iteration: 13
Labeled set size: 1052
Unlabelled set size: 3010
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 228.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 21.0
Test time: 28.0
Accumulative iteration time: 2822.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 207.0
		
Iteration: 14
Labeled set size: 1072
Unlabelled set size: 2990
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 254.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 20.0
Test time: 28.0
Accumulative iteration time: 3076.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 234.0
		
Iteration: 15
Labeled set size: 1092
Unlabelled set size: 2970
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 264.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 20.0
Test time: 28.0
Accumulative iteration time: 3340.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 244.0
		
Iteration: 16
Labeled set size: 1112
Unlabelled set size: 2950
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 292.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 20.0
Test time: 28.0
Accumulative iteration time: 3632.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 272.0
		
Iteration: 17
Labeled set size: 1132
Unlabelled set size: 2930
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 253.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 20.0
Test time: 29.0
Accumulative iteration time: 3885.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 233.0
		
Iteration: 18
Labeled set size: 1152
Unlabelled set size: 2910
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 258.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 20.0
Test time: 28.0
Accumulative iteration time: 4143.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 238.0
		
Iteration: 19
Labeled set size: 1172
Unlabelled set size: 2890
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 298.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 20.0
Test time: 28.0
Accumulative iteration time: 4441.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 278.0
		
Iteration: 20
Labeled set size: 1192
Unlabelled set size: 2870
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 304.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 19.0
Test time: 28.0
Accumulative iteration time: 4745.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 285.0
		
Iteration: 21
Labeled set size: 1212
Unlabelled set size: 2850
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 283.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 20.0
Test time: 28.0
Accumulative iteration time: 5028.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 263.0
		
Iteration: 22
Labeled set size: 1232
Unlabelled set size: 2830
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 299.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 20.0
Test time: 28.0
Accumulative iteration time: 5327.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 279.0
		
Iteration: 23
Labeled set size: 1252
Unlabelled set size: 2810
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 273.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 19.0
Test time: 28.0
Accumulative iteration time: 5600.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 254.0
		
Iteration: 24
Labeled set size: 1272
Unlabelled set size: 2790
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 250.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 28.0
Test time: 31.0
Accumulative iteration time: 5850.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 222.0
		
Iteration: 25
Labeled set size: 1292
Unlabelled set size: 2770
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 230.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 21.0
Test time: 28.0
Accumulative iteration time: 6080.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 209.0
		
Iteration: 26
Labeled set size: 1312
Unlabelled set size: 2750
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 202.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 19.0
Test time: 28.0
Accumulative iteration time: 6282.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 183.0
		
Iteration: 27
Labeled set size: 1332
Unlabelled set size: 2730
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 227.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 19.0
Test time: 28.0
Accumulative iteration time: 6509.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 208.0
		
Iteration: 28
Labeled set size: 1352
Unlabelled set size: 2710
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 280.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 18.0
Test time: 29.0
Accumulative iteration time: 6789.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 262.0
		
Iteration: 29
Labeled set size: 1372
Unlabelled set size: 2690
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 282.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 19.0
Test time: 29.0
Accumulative iteration time: 7071.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 263.0
		
Iteration: 30
Labeled set size: 1392
Unlabelled set size: 2670
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 273.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 20.0
Test time: 30.0
Accumulative iteration time: 7344.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 253.0
		
Iteration: 31
Labeled set size: 1412
Unlabelled set size: 2650
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 331.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 19.0
Test time: 29.0
Accumulative iteration time: 7675.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 312.0
		
Iteration: 32
Labeled set size: 1432
Unlabelled set size: 2630
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 312.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 18.0
Test time: 29.0
Accumulative iteration time: 7987.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 294.0
		
Iteration: 33
Labeled set size: 1452
Unlabelled set size: 2610
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 291.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 18.0
Test time: 28.0
Accumulative iteration time: 8278.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 273.0
		
Iteration: 34
Labeled set size: 1472
Unlabelled set size: 2590
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 302.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 18.0
Test time: 29.0
Accumulative iteration time: 8580.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 284.0
		
Iteration: 35
Labeled set size: 1492
Unlabelled set size: 2570
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 266.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 18.0
Test time: 30.0
Accumulative iteration time: 8846.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 248.0
		
Iteration: 36
Labeled set size: 1512
Unlabelled set size: 2550
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 324.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 17.0
Test time: 28.0
Accumulative iteration time: 9170.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 307.0
		
Iteration: 37
Labeled set size: 1532
Unlabelled set size: 2530
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 272.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 18.0
Test time: 28.0
Accumulative iteration time: 9442.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 254.0
		
Iteration: 38
Labeled set size: 1552
Unlabelled set size: 2510
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 257.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 16.0
Test time: 28.0
Accumulative iteration time: 9699.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 241.0
		
Iteration: 39
Labeled set size: 1572
Unlabelled set size: 2490
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 296.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 16.0
Test time: 28.0
Accumulative iteration time: 9995.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 280.0
		
Iteration: 40
Labeled set size: 1592
Unlabelled set size: 2470
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 251.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 16.0
Test time: 27.0
Accumulative iteration time: 10246.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 235.0
		
Iteration: 41
Labeled set size: 1612
Unlabelled set size: 2450
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 315.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 16.0
Test time: 29.0
Accumulative iteration time: 10561.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 299.0
		
Iteration: 42
Labeled set size: 1632
Unlabelled set size: 2430
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 238.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 16.0
Test time: 28.0
Accumulative iteration time: 10799.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 222.0
		
Iteration: 43
Labeled set size: 1652
Unlabelled set size: 2410
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 302.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 15.0
Test time: 31.0
Accumulative iteration time: 11101.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 287.0
		
Iteration: 44
Labeled set size: 1672
Unlabelled set size: 2390
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 318.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 14.0
Test time: 31.0
Accumulative iteration time: 11419.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 304.0
		
Iteration: 45
Labeled set size: 1692
Unlabelled set size: 2370
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 273.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 14.0
Test time: 31.0
Accumulative iteration time: 11692.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 259.0
		
Iteration: 46
Labeled set size: 1712
Unlabelled set size: 2350
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 261.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 14.0
Test time: 31.0
Accumulative iteration time: 11953.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 247.0
		
Time end:Wed Nov 01 07.08.38 EET 2017