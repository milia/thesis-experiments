Wed Nov 01 05.39.35 EET 2017
Dataset: mushroom
Test set size: 4062
Initial Labelled set size: 812
Initial Unlabelled set size: 3250
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Wed Nov 01 05.39.35 EET 2017
		
Iteration: 1
Labeled set size: 812
Unlabelled set size: 3250
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.17232890201870998
Correctly Classified Instances: 99.8276710979813
Weighted Precision: 0.9982824253441622
Weighted AreaUnderROC: 0.9982124616956077
Root mean squared error: 0.041512516428025656
Relative absolute error: 0.34465780403741997
Root relative squared error: 8.30250328560513
Weighted TruePositiveRate: 0.9982767109798129
Weighted MatthewsCorrelation: 0.9965544712751208
Weighted FMeasure: 0.9982765989627084
Iteration time: 184.0
Weighted AreaUnderPRC: 0.9973898108442917
Mean absolute error: 0.0017232890201870998
Coverage of cases: 99.8276710979813
Instances selection time: 40.0
Test time: 34.0
Accumulative iteration time: 184.0
Weighted Recall: 0.9982767109798129
Weighted FalsePositiveRate: 0.001851787588597374
Kappa statistic: 0.9965485354763794
Training time: 144.0
		
Iteration: 2
Labeled set size: 832
Unlabelled set size: 3230
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.17232890201870998
Correctly Classified Instances: 99.8276710979813
Weighted Precision: 0.9982824253441622
Weighted AreaUnderROC: 0.9982124616956077
Root mean squared error: 0.041512516428025656
Relative absolute error: 0.34465780403741997
Root relative squared error: 8.30250328560513
Weighted TruePositiveRate: 0.9982767109798129
Weighted MatthewsCorrelation: 0.9965544712751208
Weighted FMeasure: 0.9982765989627084
Iteration time: 211.0
Weighted AreaUnderPRC: 0.9973898108442917
Mean absolute error: 0.0017232890201870998
Coverage of cases: 99.8276710979813
Instances selection time: 39.0
Test time: 33.0
Accumulative iteration time: 395.0
Weighted Recall: 0.9982767109798129
Weighted FalsePositiveRate: 0.001851787588597374
Kappa statistic: 0.9965485354763794
Training time: 172.0
		
Iteration: 3
Labeled set size: 852
Unlabelled set size: 3210
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.17232890201870998
Correctly Classified Instances: 99.8276710979813
Weighted Precision: 0.9982824253441622
Weighted AreaUnderROC: 0.9982124616956077
Root mean squared error: 0.041512516428025656
Relative absolute error: 0.34465780403741997
Root relative squared error: 8.30250328560513
Weighted TruePositiveRate: 0.9982767109798129
Weighted MatthewsCorrelation: 0.9965544712751208
Weighted FMeasure: 0.9982765989627084
Iteration time: 190.0
Weighted AreaUnderPRC: 0.9973898108442917
Mean absolute error: 0.0017232890201870998
Coverage of cases: 99.8276710979813
Instances selection time: 39.0
Test time: 34.0
Accumulative iteration time: 585.0
Weighted Recall: 0.9982767109798129
Weighted FalsePositiveRate: 0.001851787588597374
Kappa statistic: 0.9965485354763794
Training time: 151.0
		
Iteration: 4
Labeled set size: 872
Unlabelled set size: 3190
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.17232890201870998
Correctly Classified Instances: 99.8276710979813
Weighted Precision: 0.9982824253441622
Weighted AreaUnderROC: 0.9982124616956077
Root mean squared error: 0.041512516428025656
Relative absolute error: 0.34465780403741997
Root relative squared error: 8.30250328560513
Weighted TruePositiveRate: 0.9982767109798129
Weighted MatthewsCorrelation: 0.9965544712751208
Weighted FMeasure: 0.9982765989627084
Iteration time: 181.0
Weighted AreaUnderPRC: 0.9973898108442917
Mean absolute error: 0.0017232890201870998
Coverage of cases: 99.8276710979813
Instances selection time: 40.0
Test time: 33.0
Accumulative iteration time: 766.0
Weighted Recall: 0.9982767109798129
Weighted FalsePositiveRate: 0.001851787588597374
Kappa statistic: 0.9965485354763794
Training time: 141.0
		
Iteration: 5
Labeled set size: 892
Unlabelled set size: 3170
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.17232890201870998
Correctly Classified Instances: 99.8276710979813
Weighted Precision: 0.9982824253441622
Weighted AreaUnderROC: 0.9982124616956077
Root mean squared error: 0.041512516428025656
Relative absolute error: 0.34465780403741997
Root relative squared error: 8.30250328560513
Weighted TruePositiveRate: 0.9982767109798129
Weighted MatthewsCorrelation: 0.9965544712751208
Weighted FMeasure: 0.9982765989627084
Iteration time: 173.0
Weighted AreaUnderPRC: 0.9973898108442917
Mean absolute error: 0.0017232890201870998
Coverage of cases: 99.8276710979813
Instances selection time: 40.0
Test time: 32.0
Accumulative iteration time: 939.0
Weighted Recall: 0.9982767109798129
Weighted FalsePositiveRate: 0.001851787588597374
Kappa statistic: 0.9965485354763794
Training time: 133.0
		
Iteration: 6
Labeled set size: 912
Unlabelled set size: 3150
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.17232890201870998
Correctly Classified Instances: 99.8276710979813
Weighted Precision: 0.9982824253441622
Weighted AreaUnderROC: 0.9982124616956077
Root mean squared error: 0.041512516428025656
Relative absolute error: 0.34465780403741997
Root relative squared error: 8.30250328560513
Weighted TruePositiveRate: 0.9982767109798129
Weighted MatthewsCorrelation: 0.9965544712751208
Weighted FMeasure: 0.9982765989627084
Iteration time: 208.0
Weighted AreaUnderPRC: 0.9973898108442917
Mean absolute error: 0.0017232890201870998
Coverage of cases: 99.8276710979813
Instances selection time: 39.0
Test time: 33.0
Accumulative iteration time: 1147.0
Weighted Recall: 0.9982767109798129
Weighted FalsePositiveRate: 0.001851787588597374
Kappa statistic: 0.9965485354763794
Training time: 169.0
		
Iteration: 7
Labeled set size: 932
Unlabelled set size: 3130
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.17232890201870998
Correctly Classified Instances: 99.8276710979813
Weighted Precision: 0.9982824253441622
Weighted AreaUnderROC: 0.9982124616956077
Root mean squared error: 0.041512516428025656
Relative absolute error: 0.34465780403741997
Root relative squared error: 8.30250328560513
Weighted TruePositiveRate: 0.9982767109798129
Weighted MatthewsCorrelation: 0.9965544712751208
Weighted FMeasure: 0.9982765989627084
Iteration time: 194.0
Weighted AreaUnderPRC: 0.9973898108442917
Mean absolute error: 0.0017232890201870998
Coverage of cases: 99.8276710979813
Instances selection time: 39.0
Test time: 32.0
Accumulative iteration time: 1341.0
Weighted Recall: 0.9982767109798129
Weighted FalsePositiveRate: 0.001851787588597374
Kappa statistic: 0.9965485354763794
Training time: 155.0
		
Iteration: 8
Labeled set size: 952
Unlabelled set size: 3110
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.17232890201870998
Correctly Classified Instances: 99.8276710979813
Weighted Precision: 0.9982824253441622
Weighted AreaUnderROC: 0.9982124616956077
Root mean squared error: 0.041512516428025656
Relative absolute error: 0.34465780403741997
Root relative squared error: 8.30250328560513
Weighted TruePositiveRate: 0.9982767109798129
Weighted MatthewsCorrelation: 0.9965544712751208
Weighted FMeasure: 0.9982765989627084
Iteration time: 248.0
Weighted AreaUnderPRC: 0.9973898108442917
Mean absolute error: 0.0017232890201870998
Coverage of cases: 99.8276710979813
Instances selection time: 38.0
Test time: 33.0
Accumulative iteration time: 1589.0
Weighted Recall: 0.9982767109798129
Weighted FalsePositiveRate: 0.001851787588597374
Kappa statistic: 0.9965485354763794
Training time: 210.0
		
Iteration: 9
Labeled set size: 972
Unlabelled set size: 3090
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.17232890201870998
Correctly Classified Instances: 99.8276710979813
Weighted Precision: 0.9982824253441622
Weighted AreaUnderROC: 0.9982124616956077
Root mean squared error: 0.041512516428025656
Relative absolute error: 0.34465780403741997
Root relative squared error: 8.30250328560513
Weighted TruePositiveRate: 0.9982767109798129
Weighted MatthewsCorrelation: 0.9965544712751208
Weighted FMeasure: 0.9982765989627084
Iteration time: 224.0
Weighted AreaUnderPRC: 0.9973898108442917
Mean absolute error: 0.0017232890201870998
Coverage of cases: 99.8276710979813
Instances selection time: 38.0
Test time: 33.0
Accumulative iteration time: 1813.0
Weighted Recall: 0.9982767109798129
Weighted FalsePositiveRate: 0.001851787588597374
Kappa statistic: 0.9965485354763794
Training time: 186.0
		
Iteration: 10
Labeled set size: 992
Unlabelled set size: 3070
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.17232890201870998
Correctly Classified Instances: 99.8276710979813
Weighted Precision: 0.9982824253441622
Weighted AreaUnderROC: 0.9982124616956077
Root mean squared error: 0.041512516428025656
Relative absolute error: 0.34465780403741997
Root relative squared error: 8.30250328560513
Weighted TruePositiveRate: 0.9982767109798129
Weighted MatthewsCorrelation: 0.9965544712751208
Weighted FMeasure: 0.9982765989627084
Iteration time: 201.0
Weighted AreaUnderPRC: 0.9973898108442917
Mean absolute error: 0.0017232890201870998
Coverage of cases: 99.8276710979813
Instances selection time: 38.0
Test time: 32.0
Accumulative iteration time: 2014.0
Weighted Recall: 0.9982767109798129
Weighted FalsePositiveRate: 0.001851787588597374
Kappa statistic: 0.9965485354763794
Training time: 163.0
		
Iteration: 11
Labeled set size: 1012
Unlabelled set size: 3050
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.17232890201870998
Correctly Classified Instances: 99.8276710979813
Weighted Precision: 0.9982824253441622
Weighted AreaUnderROC: 0.9982124616956077
Root mean squared error: 0.041512516428025656
Relative absolute error: 0.34465780403741997
Root relative squared error: 8.30250328560513
Weighted TruePositiveRate: 0.9982767109798129
Weighted MatthewsCorrelation: 0.9965544712751208
Weighted FMeasure: 0.9982765989627084
Iteration time: 258.0
Weighted AreaUnderPRC: 0.9973898108442917
Mean absolute error: 0.0017232890201870998
Coverage of cases: 99.8276710979813
Instances selection time: 38.0
Test time: 33.0
Accumulative iteration time: 2272.0
Weighted Recall: 0.9982767109798129
Weighted FalsePositiveRate: 0.001851787588597374
Kappa statistic: 0.9965485354763794
Training time: 220.0
		
Iteration: 12
Labeled set size: 1032
Unlabelled set size: 3030
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.17232890201870998
Correctly Classified Instances: 99.8276710979813
Weighted Precision: 0.9982824253441622
Weighted AreaUnderROC: 0.9982124616956077
Root mean squared error: 0.041512516428025656
Relative absolute error: 0.34465780403741997
Root relative squared error: 8.30250328560513
Weighted TruePositiveRate: 0.9982767109798129
Weighted MatthewsCorrelation: 0.9965544712751208
Weighted FMeasure: 0.9982765989627084
Iteration time: 287.0
Weighted AreaUnderPRC: 0.9973898108442917
Mean absolute error: 0.0017232890201870998
Coverage of cases: 99.8276710979813
Instances selection time: 38.0
Test time: 34.0
Accumulative iteration time: 2559.0
Weighted Recall: 0.9982767109798129
Weighted FalsePositiveRate: 0.001851787588597374
Kappa statistic: 0.9965485354763794
Training time: 249.0
		
Iteration: 13
Labeled set size: 1052
Unlabelled set size: 3010
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.17232890201870998
Correctly Classified Instances: 99.8276710979813
Weighted Precision: 0.9982824253441622
Weighted AreaUnderROC: 0.9982124616956077
Root mean squared error: 0.041512516428025656
Relative absolute error: 0.34465780403741997
Root relative squared error: 8.30250328560513
Weighted TruePositiveRate: 0.9982767109798129
Weighted MatthewsCorrelation: 0.9965544712751208
Weighted FMeasure: 0.9982765989627084
Iteration time: 204.0
Weighted AreaUnderPRC: 0.9973898108442917
Mean absolute error: 0.0017232890201870998
Coverage of cases: 99.8276710979813
Instances selection time: 38.0
Test time: 32.0
Accumulative iteration time: 2763.0
Weighted Recall: 0.9982767109798129
Weighted FalsePositiveRate: 0.001851787588597374
Kappa statistic: 0.9965485354763794
Training time: 166.0
		
Iteration: 14
Labeled set size: 1072
Unlabelled set size: 2990
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.17232890201870998
Correctly Classified Instances: 99.8276710979813
Weighted Precision: 0.9982824253441622
Weighted AreaUnderROC: 0.9982124616956077
Root mean squared error: 0.041512516428025656
Relative absolute error: 0.34465780403741997
Root relative squared error: 8.30250328560513
Weighted TruePositiveRate: 0.9982767109798129
Weighted MatthewsCorrelation: 0.9965544712751208
Weighted FMeasure: 0.9982765989627084
Iteration time: 244.0
Weighted AreaUnderPRC: 0.9973898108442917
Mean absolute error: 0.0017232890201870998
Coverage of cases: 99.8276710979813
Instances selection time: 37.0
Test time: 33.0
Accumulative iteration time: 3007.0
Weighted Recall: 0.9982767109798129
Weighted FalsePositiveRate: 0.001851787588597374
Kappa statistic: 0.9965485354763794
Training time: 207.0
		
Iteration: 15
Labeled set size: 1092
Unlabelled set size: 2970
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.22156573116691286
Correctly Classified Instances: 99.77843426883308
Weighted Precision: 0.9977937799414331
Weighted AreaUnderROC: 0.9977017364657814
Root mean squared error: 0.04707076918501681
Relative absolute error: 0.4431314623338257
Root relative squared error: 9.414153837003363
Weighted TruePositiveRate: 0.9977843426883308
Weighted MatthewsCorrelation: 0.9955720487411955
Weighted FMeasure: 0.9977841550896477
Iteration time: 244.0
Weighted AreaUnderPRC: 0.9966461327273141
Mean absolute error: 0.0022156573116691287
Coverage of cases: 99.77843426883308
Instances selection time: 37.0
Test time: 33.0
Accumulative iteration time: 3251.0
Weighted Recall: 0.9977843426883308
Weighted FalsePositiveRate: 0.0023808697567680526
Kappa statistic: 0.9955622454615528
Training time: 207.0
		
Iteration: 16
Labeled set size: 1112
Unlabelled set size: 2950
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.22156573116691286
Correctly Classified Instances: 99.77843426883308
Weighted Precision: 0.9977937799414331
Weighted AreaUnderROC: 0.9977017364657814
Root mean squared error: 0.04707076918501681
Relative absolute error: 0.4431314623338257
Root relative squared error: 9.414153837003363
Weighted TruePositiveRate: 0.9977843426883308
Weighted MatthewsCorrelation: 0.9955720487411955
Weighted FMeasure: 0.9977841550896477
Iteration time: 270.0
Weighted AreaUnderPRC: 0.9966461327273141
Mean absolute error: 0.0022156573116691287
Coverage of cases: 99.77843426883308
Instances selection time: 36.0
Test time: 33.0
Accumulative iteration time: 3521.0
Weighted Recall: 0.9977843426883308
Weighted FalsePositiveRate: 0.0023808697567680526
Kappa statistic: 0.9955622454615528
Training time: 234.0
		
Iteration: 17
Labeled set size: 1132
Unlabelled set size: 2930
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.17232890201870998
Correctly Classified Instances: 99.8276710979813
Weighted Precision: 0.9982824253441622
Weighted AreaUnderROC: 0.9982124616956077
Root mean squared error: 0.041512516428025656
Relative absolute error: 0.34465780403741997
Root relative squared error: 8.30250328560513
Weighted TruePositiveRate: 0.9982767109798129
Weighted MatthewsCorrelation: 0.9965544712751208
Weighted FMeasure: 0.9982765989627084
Iteration time: 220.0
Weighted AreaUnderPRC: 0.9973898108442917
Mean absolute error: 0.0017232890201870998
Coverage of cases: 99.8276710979813
Instances selection time: 37.0
Test time: 33.0
Accumulative iteration time: 3741.0
Weighted Recall: 0.9982767109798129
Weighted FalsePositiveRate: 0.001851787588597374
Kappa statistic: 0.9965485354763794
Training time: 183.0
		
Iteration: 18
Labeled set size: 1152
Unlabelled set size: 2910
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.22156573116691286
Correctly Classified Instances: 99.77843426883308
Weighted Precision: 0.9977937799414331
Weighted AreaUnderROC: 0.9977017364657814
Root mean squared error: 0.04707076918501681
Relative absolute error: 0.4431314623338257
Root relative squared error: 9.414153837003363
Weighted TruePositiveRate: 0.9977843426883308
Weighted MatthewsCorrelation: 0.9955720487411955
Weighted FMeasure: 0.9977841550896477
Iteration time: 239.0
Weighted AreaUnderPRC: 0.9966461327273141
Mean absolute error: 0.0022156573116691287
Coverage of cases: 99.77843426883308
Instances selection time: 36.0
Test time: 33.0
Accumulative iteration time: 3980.0
Weighted Recall: 0.9977843426883308
Weighted FalsePositiveRate: 0.0023808697567680526
Kappa statistic: 0.9955622454615528
Training time: 203.0
		
Iteration: 19
Labeled set size: 1172
Unlabelled set size: 2890
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.22156573116691286
Correctly Classified Instances: 99.77843426883308
Weighted Precision: 0.9977937799414331
Weighted AreaUnderROC: 0.9977017364657814
Root mean squared error: 0.04707076918501681
Relative absolute error: 0.4431314623338257
Root relative squared error: 9.414153837003363
Weighted TruePositiveRate: 0.9977843426883308
Weighted MatthewsCorrelation: 0.9955720487411955
Weighted FMeasure: 0.9977841550896477
Iteration time: 253.0
Weighted AreaUnderPRC: 0.9966461327273141
Mean absolute error: 0.0022156573116691287
Coverage of cases: 99.77843426883308
Instances selection time: 36.0
Test time: 33.0
Accumulative iteration time: 4233.0
Weighted Recall: 0.9977843426883308
Weighted FalsePositiveRate: 0.0023808697567680526
Kappa statistic: 0.9955622454615528
Training time: 217.0
		
Iteration: 20
Labeled set size: 1192
Unlabelled set size: 2870
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.22156573116691286
Correctly Classified Instances: 99.77843426883308
Weighted Precision: 0.9977937799414331
Weighted AreaUnderROC: 0.9977017364657814
Root mean squared error: 0.04707076918501681
Relative absolute error: 0.4431314623338257
Root relative squared error: 9.414153837003363
Weighted TruePositiveRate: 0.9977843426883308
Weighted MatthewsCorrelation: 0.9955720487411955
Weighted FMeasure: 0.9977841550896477
Iteration time: 234.0
Weighted AreaUnderPRC: 0.9966461327273141
Mean absolute error: 0.0022156573116691287
Coverage of cases: 99.77843426883308
Instances selection time: 35.0
Test time: 33.0
Accumulative iteration time: 4467.0
Weighted Recall: 0.9977843426883308
Weighted FalsePositiveRate: 0.0023808697567680526
Kappa statistic: 0.9955622454615528
Training time: 199.0
		
Iteration: 21
Labeled set size: 1212
Unlabelled set size: 2850
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.22156573116691286
Correctly Classified Instances: 99.77843426883308
Weighted Precision: 0.9977937799414331
Weighted AreaUnderROC: 0.9977017364657814
Root mean squared error: 0.04707076918501681
Relative absolute error: 0.4431314623338257
Root relative squared error: 9.414153837003363
Weighted TruePositiveRate: 0.9977843426883308
Weighted MatthewsCorrelation: 0.9955720487411955
Weighted FMeasure: 0.9977841550896477
Iteration time: 292.0
Weighted AreaUnderPRC: 0.9966461327273141
Mean absolute error: 0.0022156573116691287
Coverage of cases: 99.77843426883308
Instances selection time: 35.0
Test time: 33.0
Accumulative iteration time: 4759.0
Weighted Recall: 0.9977843426883308
Weighted FalsePositiveRate: 0.0023808697567680526
Kappa statistic: 0.9955622454615528
Training time: 257.0
		
Iteration: 22
Labeled set size: 1232
Unlabelled set size: 2830
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.22156573116691286
Correctly Classified Instances: 99.77843426883308
Weighted Precision: 0.9977937799414331
Weighted AreaUnderROC: 0.9977017364657814
Root mean squared error: 0.04707076918501681
Relative absolute error: 0.4431314623338257
Root relative squared error: 9.414153837003363
Weighted TruePositiveRate: 0.9977843426883308
Weighted MatthewsCorrelation: 0.9955720487411955
Weighted FMeasure: 0.9977841550896477
Iteration time: 248.0
Weighted AreaUnderPRC: 0.9966461327273141
Mean absolute error: 0.0022156573116691287
Coverage of cases: 99.77843426883308
Instances selection time: 35.0
Test time: 33.0
Accumulative iteration time: 5007.0
Weighted Recall: 0.9977843426883308
Weighted FalsePositiveRate: 0.0023808697567680526
Kappa statistic: 0.9955622454615528
Training time: 213.0
		
Iteration: 23
Labeled set size: 1252
Unlabelled set size: 2810
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.22156573116691286
Correctly Classified Instances: 99.77843426883308
Weighted Precision: 0.9977937799414331
Weighted AreaUnderROC: 0.9977017364657814
Root mean squared error: 0.04707076918501681
Relative absolute error: 0.4431314623338257
Root relative squared error: 9.414153837003363
Weighted TruePositiveRate: 0.9977843426883308
Weighted MatthewsCorrelation: 0.9955720487411955
Weighted FMeasure: 0.9977841550896477
Iteration time: 252.0
Weighted AreaUnderPRC: 0.9966461327273141
Mean absolute error: 0.0022156573116691287
Coverage of cases: 99.77843426883308
Instances selection time: 35.0
Test time: 32.0
Accumulative iteration time: 5259.0
Weighted Recall: 0.9977843426883308
Weighted FalsePositiveRate: 0.0023808697567680526
Kappa statistic: 0.9955622454615528
Training time: 217.0
		
Iteration: 24
Labeled set size: 1272
Unlabelled set size: 2790
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.22156573116691286
Correctly Classified Instances: 99.77843426883308
Weighted Precision: 0.9977937799414331
Weighted AreaUnderROC: 0.9977017364657814
Root mean squared error: 0.04707076918501681
Relative absolute error: 0.4431314623338257
Root relative squared error: 9.414153837003363
Weighted TruePositiveRate: 0.9977843426883308
Weighted MatthewsCorrelation: 0.9955720487411955
Weighted FMeasure: 0.9977841550896477
Iteration time: 261.0
Weighted AreaUnderPRC: 0.9966461327273141
Mean absolute error: 0.0022156573116691287
Coverage of cases: 99.77843426883308
Instances selection time: 35.0
Test time: 32.0
Accumulative iteration time: 5520.0
Weighted Recall: 0.9977843426883308
Weighted FalsePositiveRate: 0.0023808697567680526
Kappa statistic: 0.9955622454615528
Training time: 226.0
		
Iteration: 25
Labeled set size: 1292
Unlabelled set size: 2770
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.22156573116691286
Correctly Classified Instances: 99.77843426883308
Weighted Precision: 0.9977937799414331
Weighted AreaUnderROC: 0.9977017364657814
Root mean squared error: 0.04707076918501681
Relative absolute error: 0.4431314623338257
Root relative squared error: 9.414153837003363
Weighted TruePositiveRate: 0.9977843426883308
Weighted MatthewsCorrelation: 0.9955720487411955
Weighted FMeasure: 0.9977841550896477
Iteration time: 241.0
Weighted AreaUnderPRC: 0.9966461327273141
Mean absolute error: 0.0022156573116691287
Coverage of cases: 99.77843426883308
Instances selection time: 35.0
Test time: 33.0
Accumulative iteration time: 5761.0
Weighted Recall: 0.9977843426883308
Weighted FalsePositiveRate: 0.0023808697567680526
Kappa statistic: 0.9955622454615528
Training time: 206.0
		
Iteration: 26
Labeled set size: 1312
Unlabelled set size: 2750
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 0.9992339121552605
Root mean squared error: 0.0271763212599322
Relative absolute error: 0.14771048744460857
Root relative squared error: 5.43526425198644
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 320.0
Weighted AreaUnderPRC: 0.9988799500610819
Mean absolute error: 7.385524372230429E-4
Coverage of cases: 99.9261447562777
Instances selection time: 35.0
Test time: 35.0
Accumulative iteration time: 6081.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 285.0
		
Iteration: 27
Labeled set size: 1332
Unlabelled set size: 2730
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.049236829148202856
Correctly Classified Instances: 99.9507631708518
Weighted Precision: 0.9995080992947397
Weighted AreaUnderROC: 0.9994892747701737
Root mean squared error: 0.022189373390928115
Relative absolute error: 0.09847365829640572
Root relative squared error: 4.437874678185623
Weighted TruePositiveRate: 0.999507631708518
Weighted MatthewsCorrelation: 0.999014440317274
Weighted FMeasure: 0.9995076228639044
Iteration time: 254.0
Weighted AreaUnderPRC: 0.999253066580491
Mean absolute error: 4.923682914820286E-4
Coverage of cases: 99.9507631708518
Instances selection time: 34.0
Test time: 33.0
Accumulative iteration time: 6335.0
Weighted Recall: 0.999507631708518
Weighted FalsePositiveRate: 5.290821681706783E-4
Kappa statistic: 0.999013954653566
Training time: 220.0
		
Iteration: 28
Labeled set size: 1352
Unlabelled set size: 2710
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.024618414574101428
Correctly Classified Instances: 99.9753815854259
Weighted Precision: 0.9997539328063473
Weighted AreaUnderROC: 0.9997446373850868
Root mean squared error: 0.015690256395005604
Relative absolute error: 0.04923682914820286
Root relative squared error: 3.138051279001121
Weighted TruePositiveRate: 0.9997538158542589
Weighted MatthewsCorrelation: 0.9995071075347904
Weighted FMeasure: 0.9997538136580851
Iteration time: 226.0
Weighted AreaUnderPRC: 0.9996264164492228
Mean absolute error: 2.461841457410143E-4
Coverage of cases: 99.9753815854259
Instances selection time: 34.0
Test time: 33.0
Accumulative iteration time: 6561.0
Weighted Recall: 0.9997538158542589
Weighted FalsePositiveRate: 2.6454108408533914E-4
Kappa statistic: 0.9995069860633139
Training time: 192.0
		
Iteration: 29
Labeled set size: 1372
Unlabelled set size: 2690
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.024618414574101428
Correctly Classified Instances: 99.9753815854259
Weighted Precision: 0.9997539328063473
Weighted AreaUnderROC: 0.9997446373850868
Root mean squared error: 0.015690256395005604
Relative absolute error: 0.04923682914820286
Root relative squared error: 3.138051279001121
Weighted TruePositiveRate: 0.9997538158542589
Weighted MatthewsCorrelation: 0.9995071075347904
Weighted FMeasure: 0.9997538136580851
Iteration time: 278.0
Weighted AreaUnderPRC: 0.9996264164492228
Mean absolute error: 2.461841457410143E-4
Coverage of cases: 99.9753815854259
Instances selection time: 33.0
Test time: 32.0
Accumulative iteration time: 6839.0
Weighted Recall: 0.9997538158542589
Weighted FalsePositiveRate: 2.6454108408533914E-4
Kappa statistic: 0.9995069860633139
Training time: 245.0
		
Iteration: 30
Labeled set size: 1392
Unlabelled set size: 2670
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 262.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 33.0
Test time: 33.0
Accumulative iteration time: 7101.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 229.0
		
Iteration: 31
Labeled set size: 1412
Unlabelled set size: 2650
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 280.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 33.0
Test time: 34.0
Accumulative iteration time: 7381.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 247.0
		
Iteration: 32
Labeled set size: 1432
Unlabelled set size: 2630
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 261.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 32.0
Test time: 33.0
Accumulative iteration time: 7642.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 229.0
		
Iteration: 33
Labeled set size: 1452
Unlabelled set size: 2610
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 265.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 32.0
Test time: 33.0
Accumulative iteration time: 7907.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 233.0
		
Iteration: 34
Labeled set size: 1472
Unlabelled set size: 2590
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 282.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 33.0
Test time: 32.0
Accumulative iteration time: 8189.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 249.0
		
Iteration: 35
Labeled set size: 1492
Unlabelled set size: 2570
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 264.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 32.0
Test time: 33.0
Accumulative iteration time: 8453.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 232.0
		
Iteration: 36
Labeled set size: 1512
Unlabelled set size: 2550
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 287.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 32.0
Test time: 33.0
Accumulative iteration time: 8740.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 255.0
		
Iteration: 37
Labeled set size: 1532
Unlabelled set size: 2530
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 303.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 32.0
Test time: 32.0
Accumulative iteration time: 9043.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 271.0
		
Iteration: 38
Labeled set size: 1552
Unlabelled set size: 2510
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 340.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 31.0
Test time: 33.0
Accumulative iteration time: 9383.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 309.0
		
Iteration: 39
Labeled set size: 1572
Unlabelled set size: 2490
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 360.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 32.0
Test time: 33.0
Accumulative iteration time: 9743.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 328.0
		
Iteration: 40
Labeled set size: 1592
Unlabelled set size: 2470
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 341.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 31.0
Test time: 33.0
Accumulative iteration time: 10084.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 310.0
		
Iteration: 41
Labeled set size: 1612
Unlabelled set size: 2450
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 325.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 30.0
Test time: 33.0
Accumulative iteration time: 10409.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 295.0
		
Iteration: 42
Labeled set size: 1632
Unlabelled set size: 2430
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 342.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 30.0
Test time: 33.0
Accumulative iteration time: 10751.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 312.0
		
Iteration: 43
Labeled set size: 1652
Unlabelled set size: 2410
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 303.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 30.0
Test time: 33.0
Accumulative iteration time: 11054.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 273.0
		
Iteration: 44
Labeled set size: 1672
Unlabelled set size: 2390
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 354.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 27.0
Test time: 35.0
Accumulative iteration time: 11408.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 327.0
		
Iteration: 45
Labeled set size: 1692
Unlabelled set size: 2370
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 305.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 27.0
Test time: 34.0
Accumulative iteration time: 11713.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 278.0
		
Iteration: 46
Labeled set size: 1712
Unlabelled set size: 2350
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 335.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 27.0
Test time: 33.0
Accumulative iteration time: 12048.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 308.0
		
Time end:Wed Nov 01 05.39.53 EET 2017