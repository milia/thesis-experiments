Wed Nov 01 09.19.00 EET 2017
Dataset: sonar
Test set size: 104
Initial Labelled set size: 20
Initial Unlabelled set size: 84
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Wed Nov 01 09.19.00 EET 2017
		
Iteration: 1
Labeled set size: 20
Unlabelled set size: 84
	
Mean region size: 50.0
Incorrectly Classified Instances: 31.73076923076923
Correctly Classified Instances: 68.26923076923077
Weighted Precision: 0.681877444589309
Weighted AreaUnderROC: 0.6785714285714286
Root mean squared error: 0.5390061726585496
Relative absolute error: 66.78321678321682
Root relative squared error: 107.80123453170991
Weighted TruePositiveRate: 0.6826923076923077
Weighted MatthewsCorrelation: 0.35935553470461673
Weighted FMeasure: 0.6817132376739671
Iteration time: 5.0
Weighted AreaUnderPRC: 0.6238090462340788
Mean absolute error: 0.33391608391608407
Coverage of cases: 68.26923076923077
Instances selection time: 4.0
Test time: 5.0
Accumulative iteration time: 5.0
Weighted Recall: 0.6826923076923077
Weighted FalsePositiveRate: 0.3255494505494506
Kappa statistic: 0.3587443946188341
Training time: 1.0
		
Iteration: 2
Labeled set size: 40
Unlabelled set size: 64
	
Mean region size: 50.0
Incorrectly Classified Instances: 24.03846153846154
Correctly Classified Instances: 75.96153846153847
Weighted Precision: 0.8049728049728049
Weighted AreaUnderROC: 0.7425595238095238
Root mean squared error: 0.4790664070478801
Relative absolute error: 50.54945054945053
Root relative squared error: 95.81328140957602
Weighted TruePositiveRate: 0.7596153846153846
Weighted MatthewsCorrelation: 0.5516145518407064
Weighted FMeasure: 0.7449392712550609
Iteration time: 6.0
Weighted AreaUnderPRC: 0.6991402472171704
Mean absolute error: 0.2527472527472527
Coverage of cases: 75.96153846153847
Instances selection time: 5.0
Test time: 8.0
Accumulative iteration time: 11.0
Weighted Recall: 0.7596153846153846
Weighted FalsePositiveRate: 0.274496336996337
Kappa statistic: 0.5007680491551458
Training time: 1.0
		
Iteration: 3
Labeled set size: 60
Unlabelled set size: 44
	
Mean region size: 50.0
Incorrectly Classified Instances: 25.0
Correctly Classified Instances: 75.0
Weighted Precision: 0.8125
Weighted AreaUnderROC: 0.730654761904762
Root mean squared error: 0.492133751690076
Relative absolute error: 51.61290322580647
Root relative squared error: 98.4267503380152
Weighted TruePositiveRate: 0.75
Weighted MatthewsCorrelation: 0.545828789512167
Weighted FMeasure: 0.7303921568627452
Iteration time: 6.0
Weighted AreaUnderPRC: 0.691645093688363
Mean absolute error: 0.25806451612903236
Coverage of cases: 75.0
Instances selection time: 5.0
Test time: 10.0
Accumulative iteration time: 17.0
Weighted Recall: 0.75
Weighted FalsePositiveRate: 0.2886904761904762
Kappa statistic: 0.4783950617283951
Training time: 1.0
		
Iteration: 4
Labeled set size: 80
Unlabelled set size: 24
	
Mean region size: 50.0
Incorrectly Classified Instances: 13.461538461538462
Correctly Classified Instances: 86.53846153846153
Weighted Precision: 0.8778064041221937
Weighted AreaUnderROC: 0.8571428571428571
Root mean squared error: 0.36260281790272936
Relative absolute error: 28.705440900562834
Root relative squared error: 72.52056358054587
Weighted TruePositiveRate: 0.8653846153846154
Weighted MatthewsCorrelation: 0.7394738666465356
Weighted FMeasure: 0.8630751634945306
Iteration time: 4.0
Weighted AreaUnderPRC: 0.816369864952861
Mean absolute error: 0.14352720450281417
Coverage of cases: 86.53846153846153
Instances selection time: 4.0
Test time: 13.0
Accumulative iteration time: 21.0
Weighted Recall: 0.8653846153846154
Weighted FalsePositiveRate: 0.1510989010989011
Kappa statistic: 0.7250755287009064
Training time: 0.0
		
Iteration: 5
Labeled set size: 100
Unlabelled set size: 4
	
Mean region size: 50.0
Incorrectly Classified Instances: 9.615384615384615
Correctly Classified Instances: 90.38461538461539
Weighted Precision: 0.9057109557109556
Weighted AreaUnderROC: 0.9002976190476191
Root mean squared error: 0.30718820533923763
Relative absolute error: 20.814479638009047
Root relative squared error: 61.43764106784752
Weighted TruePositiveRate: 0.9038461538461539
Weighted MatthewsCorrelation: 0.8078405915503513
Weighted FMeasure: 0.9034136777764963
Iteration time: 2.0
Weighted AreaUnderPRC: 0.8641092881477496
Mean absolute error: 0.10407239819004523
Coverage of cases: 90.38461538461539
Instances selection time: 1.0
Test time: 16.0
Accumulative iteration time: 23.0
Weighted Recall: 0.9038461538461539
Weighted FalsePositiveRate: 0.10325091575091576
Kappa statistic: 0.8053892215568862
Training time: 1.0
		
Time end:Wed Nov 01 09.19.00 EET 2017