Wed Nov 01 09.18.17 EET 2017
Dataset: sonar
Test set size: 104
Initial Labelled set size: 20
Initial Unlabelled set size: 84
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Wed Nov 01 09.18.17 EET 2017
		
Iteration: 1
Labeled set size: 20
Unlabelled set size: 84
	
Mean region size: 50.0
Incorrectly Classified Instances: 27.884615384615383
Correctly Classified Instances: 72.11538461538461
Weighted Precision: 0.7209249084249085
Weighted AreaUnderROC: 0.7196660482374768
Root mean squared error: 0.5280588545286916
Relative absolute error: 55.769230769230774
Root relative squared error: 105.61177090573833
Weighted TruePositiveRate: 0.7211538461538461
Weighted MatthewsCorrelation: 0.4399037715332818
Weighted FMeasure: 0.7209725586014246
Iteration time: 21.0
Weighted AreaUnderPRC: 0.6593494646379262
Mean absolute error: 0.27884615384615385
Coverage of cases: 72.11538461538461
Instances selection time: 7.0
Test time: 1.0
Accumulative iteration time: 21.0
Weighted Recall: 0.7211538461538461
Weighted FalsePositiveRate: 0.2818217496788925
Kappa statistic: 0.4398216939078752
Training time: 14.0
		
Iteration: 2
Labeled set size: 40
Unlabelled set size: 64
	
Mean region size: 50.0
Incorrectly Classified Instances: 27.884615384615383
Correctly Classified Instances: 72.11538461538461
Weighted Precision: 0.734775641025641
Weighted AreaUnderROC: 0.7263450834879405
Root mean squared error: 0.5280588545286916
Relative absolute error: 55.769230769230774
Root relative squared error: 105.61177090573833
Weighted TruePositiveRate: 0.7211538461538461
Weighted MatthewsCorrelation: 0.45738138208528795
Weighted FMeasure: 0.7197326119344467
Iteration time: 10.0
Weighted AreaUnderPRC: 0.6665854177873408
Mean absolute error: 0.27884615384615385
Coverage of cases: 72.11538461538461
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 31.0
Weighted Recall: 0.7211538461538461
Weighted FalsePositiveRate: 0.2684636791779649
Kappa statistic: 0.44721407624633436
Training time: 10.0
		
Iteration: 3
Labeled set size: 60
Unlabelled set size: 44
	
Mean region size: 50.0
Incorrectly Classified Instances: 25.96153846153846
Correctly Classified Instances: 74.03846153846153
Weighted Precision: 0.7733636977058029
Weighted AreaUnderROC: 0.7489795918367347
Root mean squared error: 0.5095246653650681
Relative absolute error: 51.92307692307693
Root relative squared error: 101.90493307301362
Weighted TruePositiveRate: 0.7403846153846154
Weighted MatthewsCorrelation: 0.5161897363329233
Weighted FMeasure: 0.7358452907541266
Iteration time: 7.0
Weighted AreaUnderPRC: 0.6921308912073082
Mean absolute error: 0.25961538461538464
Coverage of cases: 74.03846153846153
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 38.0
Weighted Recall: 0.7403846153846154
Weighted FalsePositiveRate: 0.242425431711146
Kappa statistic: 0.48871085214857984
Training time: 6.0
		
Iteration: 4
Labeled set size: 80
Unlabelled set size: 24
	
Mean region size: 50.0
Incorrectly Classified Instances: 23.076923076923077
Correctly Classified Instances: 76.92307692307692
Weighted Precision: 0.7718489427701414
Weighted AreaUnderROC: 0.770686456400742
Root mean squared error: 0.4803844614152614
Relative absolute error: 46.15384615384615
Root relative squared error: 96.07689228305229
Weighted TruePositiveRate: 0.7692307692307693
Weighted MatthewsCorrelation: 0.5405711753563136
Weighted FMeasure: 0.7694015196789893
Iteration time: 11.0
Weighted AreaUnderPRC: 0.7093680974678584
Mean absolute error: 0.23076923076923078
Coverage of cases: 76.92307692307692
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 49.0
Weighted Recall: 0.7692307692307693
Weighted FalsePositiveRate: 0.227857856429285
Kappa statistic: 0.5389730328777246
Training time: 11.0
		
Iteration: 5
Labeled set size: 100
Unlabelled set size: 4
	
Mean region size: 50.0
Incorrectly Classified Instances: 23.076923076923077
Correctly Classified Instances: 76.92307692307692
Weighted Precision: 0.7692307692307693
Weighted AreaUnderROC: 0.7684601113172541
Root mean squared error: 0.4803844614152614
Relative absolute error: 46.15384615384615
Root relative squared error: 96.07689228305229
Weighted TruePositiveRate: 0.7692307692307693
Weighted MatthewsCorrelation: 0.5369202226345083
Weighted FMeasure: 0.7692307692307693
Iteration time: 22.0
Weighted AreaUnderPRC: 0.7072784358498645
Mean absolute error: 0.23076923076923078
Coverage of cases: 76.92307692307692
Instances selection time: 3.0
Test time: 5.0
Accumulative iteration time: 71.0
Weighted Recall: 0.7692307692307693
Weighted FalsePositiveRate: 0.23231054659626085
Kappa statistic: 0.5369202226345084
Training time: 19.0
		
Time end:Wed Nov 01 09.18.17 EET 2017