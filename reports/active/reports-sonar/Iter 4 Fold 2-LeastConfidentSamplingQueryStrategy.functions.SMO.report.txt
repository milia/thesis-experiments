Wed Nov 01 09.18.17 EET 2017
Dataset: sonar
Test set size: 104
Initial Labelled set size: 20
Initial Unlabelled set size: 84
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Wed Nov 01 09.18.17 EET 2017
		
Iteration: 1
Labeled set size: 20
Unlabelled set size: 84
	
Mean region size: 50.0
Incorrectly Classified Instances: 33.65384615384615
Correctly Classified Instances: 66.34615384615384
Weighted Precision: 0.6624534443825332
Weighted AreaUnderROC: 0.6577380952380951
Root mean squared error: 0.5801193511153214
Relative absolute error: 67.3076923076923
Root relative squared error: 116.02387022306428
Weighted TruePositiveRate: 0.6634615384615384
Weighted MatthewsCorrelation: 0.3193611435969891
Weighted FMeasure: 0.6614069690992769
Iteration time: 17.0
Weighted AreaUnderPRC: 0.6067079566962261
Mean absolute error: 0.33653846153846156
Coverage of cases: 66.34615384615384
Instances selection time: 2.0
Test time: 1.0
Accumulative iteration time: 17.0
Weighted Recall: 0.6634615384615384
Weighted FalsePositiveRate: 0.34798534798534797
Kappa statistic: 0.3178410794602699
Training time: 15.0
		
Iteration: 2
Labeled set size: 40
Unlabelled set size: 64
	
Mean region size: 50.0
Incorrectly Classified Instances: 33.65384615384615
Correctly Classified Instances: 66.34615384615384
Weighted Precision: 0.6699300699300699
Weighted AreaUnderROC: 0.6666666666666666
Root mean squared error: 0.5801193511153214
Relative absolute error: 67.3076923076923
Root relative squared error: 116.02387022306428
Weighted TruePositiveRate: 0.6634615384615384
Weighted MatthewsCorrelation: 0.33290015143059154
Weighted FMeasure: 0.6636793627084889
Iteration time: 15.0
Weighted AreaUnderPRC: 0.6133405056481979
Mean absolute error: 0.33653846153846156
Coverage of cases: 66.34615384615384
Instances selection time: 5.0
Test time: 5.0
Accumulative iteration time: 32.0
Weighted Recall: 0.6634615384615384
Weighted FalsePositiveRate: 0.3301282051282052
Kappa statistic: 0.3298969072164948
Training time: 10.0
		
Iteration: 3
Labeled set size: 60
Unlabelled set size: 44
	
Mean region size: 50.0
Incorrectly Classified Instances: 45.19230769230769
Correctly Classified Instances: 54.80769230769231
Weighted Precision: 0.6889264581572274
Weighted AreaUnderROC: 0.5773809523809523
Root mean squared error: 0.6722522420364822
Relative absolute error: 90.38461538461539
Root relative squared error: 134.45044840729642
Weighted TruePositiveRate: 0.5480769230769231
Weighted MatthewsCorrelation: 0.23328473740792172
Weighted FMeasure: 0.477162083042596
Iteration time: 16.0
Weighted AreaUnderPRC: 0.554945054945055
Mean absolute error: 0.4519230769230769
Coverage of cases: 54.80769230769231
Instances selection time: 5.0
Test time: 5.0
Accumulative iteration time: 48.0
Weighted Recall: 0.5480769230769231
Weighted FalsePositiveRate: 0.3933150183150183
Kappa statistic: 0.14545454545454556
Training time: 11.0
		
Iteration: 4
Labeled set size: 80
Unlabelled set size: 24
	
Mean region size: 50.0
Incorrectly Classified Instances: 28.846153846153847
Correctly Classified Instances: 71.15384615384616
Weighted Precision: 0.7399038461538461
Weighted AreaUnderROC: 0.7217261904761904
Root mean squared error: 0.5370861555295746
Relative absolute error: 57.692307692307686
Root relative squared error: 107.41723110591492
Weighted TruePositiveRate: 0.7115384615384616
Weighted MatthewsCorrelation: 0.45440347146666077
Weighted FMeasure: 0.7081043956043955
Iteration time: 9.0
Weighted AreaUnderPRC: 0.6644808616863904
Mean absolute error: 0.28846153846153844
Coverage of cases: 71.15384615384616
Instances selection time: 1.0
Test time: 0.0
Accumulative iteration time: 57.0
Weighted Recall: 0.7115384615384616
Weighted FalsePositiveRate: 0.26808608058608063
Kappa statistic: 0.43313953488372103
Training time: 8.0
		
Iteration: 5
Labeled set size: 100
Unlabelled set size: 4
	
Mean region size: 50.0
Incorrectly Classified Instances: 21.153846153846153
Correctly Classified Instances: 78.84615384615384
Weighted Precision: 0.7914201183431953
Weighted AreaUnderROC: 0.7901785714285714
Root mean squared error: 0.45993310550389993
Relative absolute error: 42.30769230769231
Root relative squared error: 91.98662110077998
Weighted TruePositiveRate: 0.7884615384615384
Weighted MatthewsCorrelation: 0.5786375623578447
Weighted FMeasure: 0.7887749287749286
Iteration time: 10.0
Weighted AreaUnderPRC: 0.7303994082840236
Mean absolute error: 0.21153846153846154
Coverage of cases: 78.84615384615384
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 67.0
Weighted Recall: 0.7884615384615384
Weighted FalsePositiveRate: 0.2081043956043956
Kappa statistic: 0.5769230769230769
Training time: 9.0
		
Time end:Wed Nov 01 09.18.17 EET 2017