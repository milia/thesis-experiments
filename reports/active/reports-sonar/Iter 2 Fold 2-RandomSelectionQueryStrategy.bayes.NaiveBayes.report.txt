Wed Nov 01 09.18.39 EET 2017
Dataset: sonar
Test set size: 104
Initial Labelled set size: 20
Initial Unlabelled set size: 84
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Wed Nov 01 09.18.39 EET 2017
		
Iteration: 1
Labeled set size: 20
Unlabelled set size: 84
	
Mean region size: 52.88461538461539
Incorrectly Classified Instances: 28.846153846153847
Correctly Classified Instances: 71.15384615384616
Weighted Precision: 0.7196372606820367
Weighted AreaUnderROC: 0.7652954188668475
Root mean squared error: 0.5314040084225431
Relative absolute error: 58.73288851355748
Root relative squared error: 106.28080168450862
Weighted TruePositiveRate: 0.7115384615384616
Weighted MatthewsCorrelation: 0.4251870652848379
Weighted FMeasure: 0.705599870963958
Iteration time: 1.0
Weighted AreaUnderPRC: 0.7649137257610832
Mean absolute error: 0.2936644425677874
Coverage of cases: 74.03846153846153
Instances selection time: 1.0
Test time: 11.0
Accumulative iteration time: 1.0
Weighted Recall: 0.7115384615384616
Weighted FalsePositiveRate: 0.3037462537462537
Kappa statistic: 0.4133132756675442
Training time: 0.0
		
Iteration: 2
Labeled set size: 40
Unlabelled set size: 64
	
Mean region size: 56.25
Incorrectly Classified Instances: 33.65384615384615
Correctly Classified Instances: 66.34615384615384
Weighted Precision: 0.6891409029566925
Weighted AreaUnderROC: 0.776580562294848
Root mean squared error: 0.5589806681455114
Relative absolute error: 66.58023780698691
Root relative squared error: 111.79613362910229
Weighted TruePositiveRate: 0.6634615384615384
Weighted MatthewsCorrelation: 0.3561786109122854
Weighted FMeasure: 0.6575772287553494
Iteration time: 1.0
Weighted AreaUnderPRC: 0.7752022927144357
Mean absolute error: 0.33290118903493454
Coverage of cases: 72.11538461538461
Instances selection time: 0.0
Test time: 10.0
Accumulative iteration time: 2.0
Weighted Recall: 0.6634615384615384
Weighted FalsePositiveRate: 0.3198622805765663
Kappa statistic: 0.33721777130371444
Training time: 1.0
		
Iteration: 3
Labeled set size: 60
Unlabelled set size: 44
	
Mean region size: 52.88461538461539
Incorrectly Classified Instances: 37.5
Correctly Classified Instances: 62.5
Weighted Precision: 0.6820958646616541
Weighted AreaUnderROC: 0.7831828885400314
Root mean squared error: 0.6008783528648187
Relative absolute error: 74.34808053448569
Root relative squared error: 120.17567057296374
Weighted TruePositiveRate: 0.625
Weighted MatthewsCorrelation: 0.31234632557204534
Weighted FMeasure: 0.6045060240963855
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7914879775161663
Mean absolute error: 0.37174040267242847
Coverage of cases: 67.3076923076923
Instances selection time: 2.0
Test time: 13.0
Accumulative iteration time: 4.0
Weighted Recall: 0.625
Weighted FalsePositiveRate: 0.3474489795918367
Kappa statistic: 0.26945244956772335
Training time: 0.0
		
Iteration: 4
Labeled set size: 80
Unlabelled set size: 24
	
Mean region size: 54.32692307692308
Incorrectly Classified Instances: 37.5
Correctly Classified Instances: 62.5
Weighted Precision: 0.6728603603603603
Weighted AreaUnderROC: 0.7380119880119879
Root mean squared error: 0.5984289000230636
Relative absolute error: 74.32946049612057
Root relative squared error: 119.68578000461272
Weighted TruePositiveRate: 0.625
Weighted MatthewsCorrelation: 0.3033528177270355
Weighted FMeasure: 0.6079626972740316
Iteration time: 5.0
Weighted AreaUnderPRC: 0.7481908825102063
Mean absolute error: 0.37164730248060285
Coverage of cases: 66.34615384615384
Instances selection time: 0.0
Test time: 13.0
Accumulative iteration time: 9.0
Weighted Recall: 0.625
Weighted FalsePositiveRate: 0.34967532467532464
Kappa statistic: 0.267870036101083
Training time: 5.0
		
Iteration: 5
Labeled set size: 100
Unlabelled set size: 4
	
Mean region size: 54.80769230769231
Incorrectly Classified Instances: 36.53846153846154
Correctly Classified Instances: 63.46153846153846
Weighted Precision: 0.7001424501424501
Weighted AreaUnderROC: 0.7908216783216784
Root mean squared error: 0.5904411513040315
Relative absolute error: 72.82932709488918
Root relative squared error: 118.0882302608063
Weighted TruePositiveRate: 0.6346153846153846
Weighted MatthewsCorrelation: 0.3392414539297282
Weighted FMeasure: 0.6128309360016678
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7919880296324279
Mean absolute error: 0.36414663547444587
Coverage of cases: 67.3076923076923
Instances selection time: 1.0
Test time: 13.0
Accumulative iteration time: 11.0
Weighted Recall: 0.6346153846153846
Weighted FalsePositiveRate: 0.33665620094191523
Kappa statistic: 0.2889528607412738
Training time: 1.0
		
Time end:Wed Nov 01 09.18.39 EET 2017