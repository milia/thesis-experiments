Wed Nov 01 09.18.10 EET 2017
Dataset: sonar
Test set size: 104
Initial Labelled set size: 20
Initial Unlabelled set size: 84
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Wed Nov 01 09.18.10 EET 2017
		
Iteration: 1
Labeled set size: 20
Unlabelled set size: 84
	
Mean region size: 98.50961538461537
Incorrectly Classified Instances: 36.44230769230769
Correctly Classified Instances: 63.55769230769231
Weighted Precision: 0.6637719076728448
Weighted AreaUnderROC: 0.7096778370439085
Root mean squared error: 0.4719975422196881
Relative absolute error: 84.11538461538461
Root relative squared error: 94.39950844393763
Weighted TruePositiveRate: 0.635576923076923
Weighted MatthewsCorrelation: 0.29923179673171874
Weighted FMeasure: 0.621839988442172
Iteration time: 9.3
Weighted AreaUnderPRC: 0.6898449549337579
Mean absolute error: 0.42057692307692307
Coverage of cases: 99.71153846153847
Instances selection time: 2.4
Test time: 2.1
Accumulative iteration time: 9.3
Weighted Recall: 0.635576923076923
Weighted FalsePositiveRate: 0.3538430022358594
Kappa statistic: 0.2791654537369822
Training time: 6.9
		
Iteration: 2
Labeled set size: 40
Unlabelled set size: 64
	
Mean region size: 96.44230769230768
Incorrectly Classified Instances: 39.90384615384617
Correctly Classified Instances: 60.09615384615383
Weighted Precision: 0.6915130041930266
Weighted AreaUnderROC: 0.7284789540816325
Root mean squared error: 0.4944781849407847
Relative absolute error: 84.69230769230768
Root relative squared error: 98.89563698815694
Weighted TruePositiveRate: 0.6009615384615385
Weighted MatthewsCorrelation: 0.2869437536231289
Weighted FMeasure: 0.5528822508866451
Iteration time: 10.2
Weighted AreaUnderPRC: 0.7041651238789302
Mean absolute error: 0.42346153846153844
Coverage of cases: 98.36538461538461
Instances selection time: 1.8
Test time: 1.9
Accumulative iteration time: 19.5
Weighted Recall: 0.6009615384615385
Weighted FalsePositiveRate: 0.36883259597545315
Kappa statistic: 0.22788392910073738
Training time: 8.4
		
Iteration: 3
Labeled set size: 60
Unlabelled set size: 44
	
Mean region size: 91.73076923076925
Incorrectly Classified Instances: 41.63461538461538
Correctly Classified Instances: 58.36538461538462
Weighted Precision: 0.7372127142727664
Weighted AreaUnderROC: 0.7566227485312307
Root mean squared error: 0.5135688531740563
Relative absolute error: 82.90384615384615
Root relative squared error: 102.7137706348113
Weighted TruePositiveRate: 0.5836538461538461
Weighted MatthewsCorrelation: 0.29833029183380433
Weighted FMeasure: 0.5149354697748506
Iteration time: 12.6
Weighted AreaUnderPRC: 0.7296254352721938
Mean absolute error: 0.4145192307692308
Coverage of cases: 96.25
Instances selection time: 1.4
Test time: 1.9
Accumulative iteration time: 32.1
Weighted Recall: 0.5836538461538461
Weighted FalsePositiveRate: 0.38082685171970887
Kappa statistic: 0.19727554616715307
Training time: 11.2
		
Iteration: 4
Labeled set size: 80
Unlabelled set size: 24
	
Mean region size: 95.86538461538461
Incorrectly Classified Instances: 28.653846153846153
Correctly Classified Instances: 71.34615384615384
Weighted Precision: 0.7458729409495289
Weighted AreaUnderROC: 0.8325706845238094
Root mean squared error: 0.4211071663879677
Relative absolute error: 71.34615384615384
Root relative squared error: 84.22143327759352
Weighted TruePositiveRate: 0.7134615384615385
Weighted MatthewsCorrelation: 0.46081549822725715
Weighted FMeasure: 0.7064929734393692
Iteration time: 16.7
Weighted AreaUnderPRC: 0.8125376865399121
Mean absolute error: 0.3567307692307692
Coverage of cases: 99.90384615384616
Instances selection time: 1.7
Test time: 2.1
Accumulative iteration time: 48.8
Weighted Recall: 0.7134615384615385
Weighted FalsePositiveRate: 0.2732512725369868
Kappa statistic: 0.43457529963781727
Training time: 15.0
		
Iteration: 5
Labeled set size: 100
Unlabelled set size: 4
	
Mean region size: 95.76923076923077
Incorrectly Classified Instances: 22.5
Correctly Classified Instances: 77.49999999999999
Weighted Precision: 0.7826852841439763
Weighted AreaUnderROC: 0.8626116554576375
Root mean squared error: 0.39371509967778895
Relative absolute error: 65.86538461538461
Root relative squared error: 78.74301993555778
Weighted TruePositiveRate: 0.7750000000000001
Weighted MatthewsCorrelation: 0.5571733041935878
Weighted FMeasure: 0.7745809694166914
Iteration time: 22.4
Weighted AreaUnderPRC: 0.8441532447679638
Mean absolute error: 0.329326923076923
Coverage of cases: 99.8076923076923
Instances selection time: 1.3
Test time: 2.1
Accumulative iteration time: 71.2
Weighted Recall: 0.7750000000000001
Weighted FalsePositiveRate: 0.21990259740259743
Kappa statistic: 0.5512134960715682
Training time: 21.1
		
Time end:Wed Nov 01 09.18.12 EET 2017