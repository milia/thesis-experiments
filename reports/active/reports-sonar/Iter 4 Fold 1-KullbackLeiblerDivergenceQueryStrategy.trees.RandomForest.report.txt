Wed Nov 01 09.18.11 EET 2017
Dataset: sonar
Test set size: 104
Initial Labelled set size: 20
Initial Unlabelled set size: 84
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Wed Nov 01 09.18.11 EET 2017
		
Iteration: 1
Labeled set size: 20
Unlabelled set size: 84
	
Mean region size: 99.51923076923077
Incorrectly Classified Instances: 33.65384615384615
Correctly Classified Instances: 66.34615384615384
Weighted Precision: 0.6791105399976368
Weighted AreaUnderROC: 0.7491651205936919
Root mean squared error: 0.44978627403211424
Relative absolute error: 82.30769230769232
Root relative squared error: 89.95725480642285
Weighted TruePositiveRate: 0.6634615384615384
Weighted MatthewsCorrelation: 0.3450216287518032
Weighted FMeasure: 0.6606171992769932
Iteration time: 10.0
Weighted AreaUnderPRC: 0.7308452633021798
Mean absolute error: 0.4115384615384616
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 10.0
Weighted Recall: 0.6634615384615384
Weighted FalsePositiveRate: 0.32431497074354215
Kappa statistic: 0.33430870519385514
Training time: 9.0
		
Iteration: 2
Labeled set size: 40
Unlabelled set size: 64
	
Mean region size: 98.5576923076923
Incorrectly Classified Instances: 41.34615384615385
Correctly Classified Instances: 58.65384615384615
Weighted Precision: 0.6927176505664878
Weighted AreaUnderROC: 0.8126159554730983
Root mean squared error: 0.4564705736576944
Relative absolute error: 81.73076923076923
Root relative squared error: 91.29411473153888
Weighted TruePositiveRate: 0.5865384615384616
Weighted MatthewsCorrelation: 0.27906796967679703
Weighted FMeasure: 0.5384166569098076
Iteration time: 4.0
Weighted AreaUnderPRC: 0.7831317382611113
Mean absolute error: 0.40865384615384615
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 14.0
Weighted Recall: 0.5865384615384616
Weighted FalsePositiveRate: 0.37503567860710724
Kappa statistic: 0.20313613684960807
Training time: 4.0
		
Iteration: 3
Labeled set size: 60
Unlabelled set size: 44
	
Mean region size: 93.26923076923077
Incorrectly Classified Instances: 46.15384615384615
Correctly Classified Instances: 53.84615384615385
Weighted Precision: 0.6708023159636063
Weighted AreaUnderROC: 0.7821892393320966
Root mean squared error: 0.508674748734395
Relative absolute error: 84.03846153846156
Root relative squared error: 101.734949746879
Weighted TruePositiveRate: 0.5384615384615384
Weighted MatthewsCorrelation: 0.19934749934625418
Weighted FMeasure: 0.4561213434452871
Iteration time: 7.0
Weighted AreaUnderPRC: 0.7509752188858914
Mean absolute error: 0.4201923076923078
Coverage of cases: 99.03846153846153
Instances selection time: 1.0
Test time: 0.0
Accumulative iteration time: 21.0
Weighted Recall: 0.5384615384615384
Weighted FalsePositiveRate: 0.41564150135578704
Kappa statistic: 0.1170852493809692
Training time: 6.0
		
Iteration: 4
Labeled set size: 80
Unlabelled set size: 24
	
Mean region size: 97.11538461538461
Incorrectly Classified Instances: 24.03846153846154
Correctly Classified Instances: 75.96153846153847
Weighted Precision: 0.7699299388767156
Weighted AreaUnderROC: 0.8309833024118738
Root mean squared error: 0.4092676385936224
Relative absolute error: 70.38461538461537
Root relative squared error: 81.85352771872448
Weighted TruePositiveRate: 0.7596153846153846
Weighted MatthewsCorrelation: 0.5303069257700835
Weighted FMeasure: 0.7590148123367333
Iteration time: 10.0
Weighted AreaUnderPRC: 0.8158529450430112
Mean absolute error: 0.3519230769230769
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 31.0
Weighted Recall: 0.7596153846153846
Weighted FalsePositiveRate: 0.2319715998287427
Kappa statistic: 0.5224099926524614
Training time: 9.0
		
Iteration: 5
Labeled set size: 100
Unlabelled set size: 4
	
Mean region size: 95.67307692307692
Incorrectly Classified Instances: 21.153846153846153
Correctly Classified Instances: 78.84615384615384
Weighted Precision: 0.7884184684296666
Weighted AreaUnderROC: 0.8701298701298701
Root mean squared error: 0.3903893519985478
Relative absolute error: 65.19230769230771
Root relative squared error: 78.07787039970955
Weighted TruePositiveRate: 0.7884615384615384
Weighted MatthewsCorrelation: 0.5749932444065337
Weighted FMeasure: 0.788146749084249
Iteration time: 13.0
Weighted AreaUnderPRC: 0.8495719833783771
Mean absolute error: 0.3259615384615385
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 0.0
Accumulative iteration time: 44.0
Weighted Recall: 0.7884615384615384
Weighted FalsePositiveRate: 0.21517767946339375
Kappa statistic: 0.5745630345853476
Training time: 12.0
		
Time end:Wed Nov 01 09.18.12 EET 2017