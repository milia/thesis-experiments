Wed Nov 01 09.18.45 EET 2017
Dataset: sonar
Test set size: 104
Initial Labelled set size: 20
Initial Unlabelled set size: 84
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Wed Nov 01 09.18.45 EET 2017
		
Iteration: 1
Labeled set size: 20
Unlabelled set size: 84
	
Mean region size: 50.0
Incorrectly Classified Instances: 32.69230769230769
Correctly Classified Instances: 67.3076923076923
Weighted Precision: 0.7351714550509733
Weighted AreaUnderROC: 0.6564007421150279
Root mean squared error: 0.5470547622544536
Relative absolute error: 68.53146853146855
Root relative squared error: 109.41095245089072
Weighted TruePositiveRate: 0.6730769230769231
Weighted MatthewsCorrelation: 0.38895512324411025
Weighted FMeasure: 0.6408584169453734
Iteration time: 1.0
Weighted AreaUnderPRC: 0.6172999266720305
Mean absolute error: 0.3426573426573427
Coverage of cases: 67.3076923076923
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 1.0
Weighted Recall: 0.6730769230769231
Weighted FalsePositiveRate: 0.36027543884686747
Kappa statistic: 0.3228648027575643
Training time: 0.0
		
Iteration: 2
Labeled set size: 40
Unlabelled set size: 64
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.61538461538461
Correctly Classified Instances: 65.38461538461539
Weighted Precision: 0.6577559189499488
Weighted AreaUnderROC: 0.6460111317254174
Root mean squared error: 0.5746627037614028
Relative absolute error: 70.69597069597069
Root relative squared error: 114.93254075228056
Weighted TruePositiveRate: 0.6538461538461539
Weighted MatthewsCorrelation: 0.30447881745147176
Weighted FMeasure: 0.6467198451567493
Iteration time: 1.0
Weighted AreaUnderPRC: 0.597526470876069
Mean absolute error: 0.35347985347985345
Coverage of cases: 65.38461538461539
Instances selection time: 1.0
Test time: 8.0
Accumulative iteration time: 2.0
Weighted Recall: 0.6538461538461539
Weighted FalsePositiveRate: 0.3618238903953189
Kappa statistic: 0.295975930801053
Training time: 0.0
		
Iteration: 3
Labeled set size: 60
Unlabelled set size: 44
	
Mean region size: 50.0
Incorrectly Classified Instances: 26.923076923076923
Correctly Classified Instances: 73.07692307692308
Weighted Precision: 0.7309430682312038
Weighted AreaUnderROC: 0.7276437847866419
Root mean squared error: 0.5106917381246517
Relative absolute error: 55.33498759305208
Root relative squared error: 102.13834762493035
Weighted TruePositiveRate: 0.7307692307692307
Weighted MatthewsCorrelation: 0.4587044097705052
Weighted FMeasure: 0.7297642633588881
Iteration time: 1.0
Weighted AreaUnderPRC: 0.667534600340989
Mean absolute error: 0.2766749379652604
Coverage of cases: 73.07692307692308
Instances selection time: 1.0
Test time: 11.0
Accumulative iteration time: 3.0
Weighted Recall: 0.7307692307692307
Weighted FalsePositiveRate: 0.2754816611959469
Kappa statistic: 0.4573238911666044
Training time: 0.0
		
Iteration: 4
Labeled set size: 80
Unlabelled set size: 24
	
Mean region size: 50.0
Incorrectly Classified Instances: 20.192307692307693
Correctly Classified Instances: 79.8076923076923
Weighted Precision: 0.8224385908209437
Weighted AreaUnderROC: 0.7890538033395175
Root mean squared error: 0.44401221247483424
Relative absolute error: 41.838649155722315
Root relative squared error: 88.80244249496685
Weighted TruePositiveRate: 0.7980769230769231
Weighted MatthewsCorrelation: 0.6151762920127817
Weighted FMeasure: 0.7919462465245599
Iteration time: 0.0
Weighted AreaUnderPRC: 0.7400056250310776
Mean absolute error: 0.2091932457786116
Coverage of cases: 79.8076923076923
Instances selection time: 0.0
Test time: 14.0
Accumulative iteration time: 3.0
Weighted Recall: 0.7980769230769231
Weighted FalsePositiveRate: 0.2199693163978878
Kappa statistic: 0.5879245283018869
Training time: 0.0
		
Iteration: 5
Labeled set size: 100
Unlabelled set size: 4
	
Mean region size: 50.0
Incorrectly Classified Instances: 18.26923076923077
Correctly Classified Instances: 81.73076923076923
Weighted Precision: 0.8302125506072874
Weighted AreaUnderROC: 0.8105751391465679
Root mean squared error: 0.42332755929518423
Relative absolute error: 37.78280542986427
Root relative squared error: 84.66551185903684
Weighted TruePositiveRate: 0.8173076923076923
Weighted MatthewsCorrelation: 0.6438909229667017
Weighted FMeasure: 0.8140623744072021
Iteration time: 0.0
Weighted AreaUnderPRC: 0.7597388062342516
Mean absolute error: 0.18891402714932135
Coverage of cases: 81.73076923076923
Instances selection time: 0.0
Test time: 17.0
Accumulative iteration time: 3.0
Weighted Recall: 0.8173076923076923
Weighted FalsePositiveRate: 0.19615741401455689
Kappa statistic: 0.6288504883546205
Training time: 0.0
		
Time end:Wed Nov 01 09.18.45 EET 2017