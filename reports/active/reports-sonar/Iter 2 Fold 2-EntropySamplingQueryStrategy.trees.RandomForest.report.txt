Wed Nov 01 09.17.54 EET 2017
Dataset: sonar
Test set size: 104
Initial Labelled set size: 20
Initial Unlabelled set size: 84
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Nov 01 09.17.54 EET 2017
		
Iteration: 1
Labeled set size: 20
Unlabelled set size: 84
	
Mean region size: 98.07692307692308
Incorrectly Classified Instances: 41.34615384615385
Correctly Classified Instances: 58.65384615384615
Weighted Precision: 0.5903981331284328
Weighted AreaUnderROC: 0.5881696428571429
Root mean squared error: 0.5223835902711869
Relative absolute error: 92.30769230769235
Root relative squared error: 104.47671805423737
Weighted TruePositiveRate: 0.5865384615384616
Weighted MatthewsCorrelation: 0.175107337074172
Weighted FMeasure: 0.5871123417158394
Iteration time: 7.0
Weighted AreaUnderPRC: 0.5741201191915588
Mean absolute error: 0.46153846153846173
Coverage of cases: 99.03846153846153
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 7.0
Weighted Recall: 0.5865384615384616
Weighted FalsePositiveRate: 0.4109432234432234
Kappa statistic: 0.1742983751846381
Training time: 7.0
		
Iteration: 2
Labeled set size: 40
Unlabelled set size: 64
	
Mean region size: 94.23076923076923
Incorrectly Classified Instances: 25.96153846153846
Correctly Classified Instances: 74.03846153846153
Weighted Precision: 0.7423375736361308
Weighted AreaUnderROC: 0.7914806547619049
Root mean squared error: 0.4343430051858163
Relative absolute error: 72.6923076923077
Root relative squared error: 86.86860103716326
Weighted TruePositiveRate: 0.7403846153846154
Weighted MatthewsCorrelation: 0.4808031967121333
Weighted FMeasure: 0.7407454930390709
Iteration time: 12.0
Weighted AreaUnderPRC: 0.7615451633987076
Mean absolute error: 0.36346153846153845
Coverage of cases: 99.03846153846153
Instances selection time: 5.0
Test time: 1.0
Accumulative iteration time: 19.0
Weighted Recall: 0.7403846153846154
Weighted FalsePositiveRate: 0.25824175824175827
Kappa statistic: 0.48000000000000004
Training time: 7.0
		
Iteration: 3
Labeled set size: 60
Unlabelled set size: 44
	
Mean region size: 97.11538461538461
Incorrectly Classified Instances: 27.884615384615383
Correctly Classified Instances: 72.11538461538461
Weighted Precision: 0.7280719280719281
Weighted AreaUnderROC: 0.8337053571428571
Root mean squared error: 0.40596513681311625
Relative absolute error: 69.23076923076923
Root relative squared error: 81.19302736262325
Weighted TruePositiveRate: 0.7211538461538461
Weighted MatthewsCorrelation: 0.4488207398751726
Weighted FMeasure: 0.7213343291013193
Iteration time: 20.0
Weighted AreaUnderPRC: 0.8254508782894319
Mean absolute error: 0.34615384615384615
Coverage of cases: 100.0
Instances selection time: 6.0
Test time: 1.0
Accumulative iteration time: 39.0
Weighted Recall: 0.7211538461538461
Weighted FalsePositiveRate: 0.2717490842490843
Kappa statistic: 0.44477172312223856
Training time: 14.0
		
Iteration: 4
Labeled set size: 80
Unlabelled set size: 24
	
Mean region size: 95.67307692307692
Incorrectly Classified Instances: 23.076923076923077
Correctly Classified Instances: 76.92307692307692
Weighted Precision: 0.7730769230769231
Weighted AreaUnderROC: 0.820126488095238
Root mean squared error: 0.41405592338753905
Relative absolute error: 67.5
Root relative squared error: 82.81118467750781
Weighted TruePositiveRate: 0.7692307692307693
Weighted MatthewsCorrelation: 0.5367450401216932
Weighted FMeasure: 0.7664335664335664
Iteration time: 28.0
Weighted AreaUnderPRC: 0.7938358761096297
Mean absolute error: 0.33749999999999997
Coverage of cases: 98.07692307692308
Instances selection time: 7.0
Test time: 1.0
Accumulative iteration time: 67.0
Weighted Recall: 0.7692307692307693
Weighted FalsePositiveRate: 0.2454212454212454
Kappa statistic: 0.5301204819277109
Training time: 21.0
		
Iteration: 5
Labeled set size: 100
Unlabelled set size: 4
	
Mean region size: 98.5576923076923
Incorrectly Classified Instances: 29.807692307692307
Correctly Classified Instances: 70.1923076923077
Weighted Precision: 0.702440416726131
Weighted AreaUnderROC: 0.7749255952380951
Root mean squared error: 0.4363308554120546
Relative absolute error: 76.15384615384617
Root relative squared error: 87.26617108241092
Weighted TruePositiveRate: 0.7019230769230769
Weighted MatthewsCorrelation: 0.4012635753850881
Weighted FMeasure: 0.7021168670653208
Iteration time: 28.0
Weighted AreaUnderPRC: 0.7494493923797187
Mean absolute error: 0.38076923076923086
Coverage of cases: 100.0
Instances selection time: 4.0
Test time: 5.0
Accumulative iteration time: 95.0
Weighted Recall: 0.7019230769230769
Weighted FalsePositiveRate: 0.3001373626373626
Kappa statistic: 0.401188707280832
Training time: 24.0
		
Time end:Wed Nov 01 09.17.55 EET 2017