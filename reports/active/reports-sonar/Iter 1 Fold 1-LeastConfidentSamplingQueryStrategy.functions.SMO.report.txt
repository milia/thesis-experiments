Wed Nov 01 09.18.15 EET 2017
Dataset: sonar
Test set size: 104
Initial Labelled set size: 20
Initial Unlabelled set size: 84
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Wed Nov 01 09.18.15 EET 2017
		
Iteration: 1
Labeled set size: 20
Unlabelled set size: 84
	
Mean region size: 50.0
Incorrectly Classified Instances: 33.65384615384615
Correctly Classified Instances: 66.34615384615384
Weighted Precision: 0.6946499013806707
Weighted AreaUnderROC: 0.6495361781076066
Root mean squared error: 0.5801193511153214
Relative absolute error: 67.3076923076923
Root relative squared error: 116.02387022306428
Weighted TruePositiveRate: 0.6634615384615384
Weighted MatthewsCorrelation: 0.3447638181286745
Weighted FMeasure: 0.6409581646423752
Iteration time: 16.0
Weighted AreaUnderPRC: 0.6058000493096647
Mean absolute error: 0.33653846153846156
Coverage of cases: 66.34615384615384
Instances selection time: 5.0
Test time: 1.0
Accumulative iteration time: 16.0
Weighted Recall: 0.6634615384615384
Weighted FalsePositiveRate: 0.3643891822463251
Kappa statistic: 0.3069306930693069
Training time: 11.0
		
Iteration: 2
Labeled set size: 40
Unlabelled set size: 64
	
Mean region size: 50.0
Incorrectly Classified Instances: 35.57692307692308
Correctly Classified Instances: 64.42307692307692
Weighted Precision: 0.6458949704142013
Weighted AreaUnderROC: 0.6447124304267161
Root mean squared error: 0.596463939202724
Relative absolute error: 71.15384615384616
Root relative squared error: 119.2927878405448
Weighted TruePositiveRate: 0.6442307692307693
Weighted MatthewsCorrelation: 0.2889427981985323
Weighted FMeasure: 0.6445270515549039
Iteration time: 18.0
Weighted AreaUnderPRC: 0.594582100591716
Mean absolute error: 0.3557692307692308
Coverage of cases: 64.42307692307692
Instances selection time: 5.0
Test time: 3.0
Accumulative iteration time: 34.0
Weighted Recall: 0.6442307692307693
Weighted FalsePositiveRate: 0.354805908377337
Kappa statistic: 0.28846153846153855
Training time: 13.0
		
Iteration: 3
Labeled set size: 60
Unlabelled set size: 44
	
Mean region size: 50.0
Incorrectly Classified Instances: 32.69230769230769
Correctly Classified Instances: 67.3076923076923
Weighted Precision: 0.68328987396784
Weighted AreaUnderROC: 0.6775510204081634
Root mean squared error: 0.5717718748968657
Relative absolute error: 65.38461538461539
Root relative squared error: 114.35437497937313
Weighted TruePositiveRate: 0.6730769230769231
Weighted MatthewsCorrelation: 0.35776700908750886
Weighted FMeasure: 0.6718660968660969
Iteration time: 16.0
Weighted AreaUnderPRC: 0.6220370323939424
Mean absolute error: 0.3269230769230769
Coverage of cases: 67.3076923076923
Instances selection time: 5.0
Test time: 1.0
Accumulative iteration time: 50.0
Weighted Recall: 0.6730769230769231
Weighted FalsePositiveRate: 0.31797488226059656
Kappa statistic: 0.35119266055045884
Training time: 11.0
		
Iteration: 4
Labeled set size: 80
Unlabelled set size: 24
	
Mean region size: 50.0
Incorrectly Classified Instances: 26.923076923076923
Correctly Classified Instances: 73.07692307692308
Weighted Precision: 0.7307692307692307
Weighted AreaUnderROC: 0.72987012987013
Root mean squared error: 0.5188745216627708
Relative absolute error: 53.84615384615385
Root relative squared error: 103.77490433255416
Weighted TruePositiveRate: 0.7307692307692307
Weighted MatthewsCorrelation: 0.45974025974025967
Weighted FMeasure: 0.7307692307692307
Iteration time: 26.0
Weighted AreaUnderPRC: 0.6688811188811189
Mean absolute error: 0.2692307692307692
Coverage of cases: 73.07692307692308
Instances selection time: 2.0
Test time: 11.0
Accumulative iteration time: 76.0
Weighted Recall: 0.7307692307692307
Weighted FalsePositiveRate: 0.271028971028971
Kappa statistic: 0.45974025974025967
Training time: 24.0
		
Iteration: 5
Labeled set size: 100
Unlabelled set size: 4
	
Mean region size: 50.0
Incorrectly Classified Instances: 23.076923076923077
Correctly Classified Instances: 76.92307692307692
Weighted Precision: 0.777120315581854
Weighted AreaUnderROC: 0.7628942486085344
Root mean squared error: 0.4803844614152614
Relative absolute error: 46.15384615384615
Root relative squared error: 96.07689228305229
Weighted TruePositiveRate: 0.7692307692307693
Weighted MatthewsCorrelation: 0.5421275571191733
Weighted FMeasure: 0.7657342657342656
Iteration time: 18.0
Weighted AreaUnderPRC: 0.7059048323471401
Mean absolute error: 0.23076923076923078
Coverage of cases: 76.92307692307692
Instances selection time: 4.0
Test time: 3.0
Accumulative iteration time: 94.0
Weighted Recall: 0.7692307692307693
Weighted FalsePositiveRate: 0.24344227201370056
Kappa statistic: 0.5317073170731709
Training time: 14.0
		
Time end:Wed Nov 01 09.18.15 EET 2017