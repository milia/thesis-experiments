Wed Nov 01 09.18.40 EET 2017
Dataset: sonar
Test set size: 104
Initial Labelled set size: 20
Initial Unlabelled set size: 84
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Wed Nov 01 09.18.40 EET 2017
		
Iteration: 1
Labeled set size: 20
Unlabelled set size: 84
	
Mean region size: 57.69230769230769
Incorrectly Classified Instances: 41.34615384615385
Correctly Classified Instances: 58.65384615384615
Weighted Precision: 0.5849158653846154
Weighted AreaUnderROC: 0.6392286285143427
Root mean squared error: 0.6281511427792277
Relative absolute error: 83.0806796727185
Root relative squared error: 125.63022855584553
Weighted TruePositiveRate: 0.5865384615384616
Weighted MatthewsCorrelation: 0.16446912404575664
Weighted FMeasure: 0.5812682030461277
Iteration time: 13.0
Weighted AreaUnderPRC: 0.653095128442839
Mean absolute error: 0.4154033983635925
Coverage of cases: 65.38461538461539
Instances selection time: 12.0
Test time: 12.0
Accumulative iteration time: 13.0
Weighted Recall: 0.5865384615384616
Weighted FalsePositiveRate: 0.42624161552732975
Kappa statistic: 0.16191904047976025
Training time: 1.0
		
Iteration: 2
Labeled set size: 40
Unlabelled set size: 64
	
Mean region size: 56.73076923076923
Incorrectly Classified Instances: 47.11538461538461
Correctly Classified Instances: 52.88461538461539
Weighted Precision: 0.5305103550295858
Weighted AreaUnderROC: 0.6627212073640645
Root mean squared error: 0.6564067414337712
Relative absolute error: 90.2669325213885
Root relative squared error: 131.28134828675425
Weighted TruePositiveRate: 0.5288461538461539
Weighted MatthewsCorrelation: 0.05778855963970646
Weighted FMeasure: 0.5292385277348728
Iteration time: 1.0
Weighted AreaUnderPRC: 0.6800727994931854
Mean absolute error: 0.4513346626069425
Coverage of cases: 63.46153846153846
Instances selection time: 0.0
Test time: 13.0
Accumulative iteration time: 14.0
Weighted Recall: 0.5288461538461539
Weighted FalsePositiveRate: 0.47096118167546747
Kappa statistic: 0.05769230769230771
Training time: 1.0
		
Iteration: 3
Labeled set size: 60
Unlabelled set size: 44
	
Mean region size: 55.28846153846154
Incorrectly Classified Instances: 46.15384615384615
Correctly Classified Instances: 53.84615384615385
Weighted Precision: 0.5435150888678325
Weighted AreaUnderROC: 0.6556925217639503
Root mean squared error: 0.660064000409172
Relative absolute error: 90.88557833359238
Root relative squared error: 132.0128000818344
Weighted TruePositiveRate: 0.5384615384615384
Weighted MatthewsCorrelation: 0.08299255242890421
Weighted FMeasure: 0.5377785366686587
Iteration time: 1.0
Weighted AreaUnderPRC: 0.6857594433997766
Mean absolute error: 0.4544278916679619
Coverage of cases: 61.53846153846154
Instances selection time: 0.0
Test time: 13.0
Accumulative iteration time: 15.0
Weighted Recall: 0.5384615384615384
Weighted FalsePositiveRate: 0.45571571285857
Kappa statistic: 0.08201544685546153
Training time: 1.0
		
Iteration: 4
Labeled set size: 80
Unlabelled set size: 24
	
Mean region size: 54.80769230769231
Incorrectly Classified Instances: 42.30769230769231
Correctly Classified Instances: 57.69230769230769
Weighted Precision: 0.5846008981602202
Weighted AreaUnderROC: 0.6616080348223204
Root mean squared error: 0.6396038051238213
Relative absolute error: 85.18431223154604
Root relative squared error: 127.92076102476426
Weighted TruePositiveRate: 0.5769230769230769
Weighted MatthewsCorrelation: 0.16336905221655318
Weighted FMeasure: 0.5753561253561253
Iteration time: 4.0
Weighted AreaUnderPRC: 0.6859476775555721
Mean absolute error: 0.4259215611577302
Coverage of cases: 61.53846153846154
Instances selection time: 3.0
Test time: 14.0
Accumulative iteration time: 19.0
Weighted Recall: 0.5769230769230769
Weighted FalsePositiveRate: 0.41477094334237197
Kappa statistic: 0.16036697247706413
Training time: 1.0
		
Iteration: 5
Labeled set size: 100
Unlabelled set size: 4
	
Mean region size: 55.76923076923077
Incorrectly Classified Instances: 42.30769230769231
Correctly Classified Instances: 57.69230769230769
Weighted Precision: 0.5846008981602202
Weighted AreaUnderROC: 0.6633009847295561
Root mean squared error: 0.6391797083610711
Relative absolute error: 85.24541918807368
Root relative squared error: 127.83594167221422
Weighted TruePositiveRate: 0.5769230769230769
Weighted MatthewsCorrelation: 0.16336905221655318
Weighted FMeasure: 0.5753561253561253
Iteration time: 2.0
Weighted AreaUnderPRC: 0.6839954566800135
Mean absolute error: 0.4262270959403684
Coverage of cases: 61.53846153846154
Instances selection time: 1.0
Test time: 7.0
Accumulative iteration time: 21.0
Weighted Recall: 0.5769230769230769
Weighted FalsePositiveRate: 0.41477094334237197
Kappa statistic: 0.16036697247706413
Training time: 1.0
		
Time end:Wed Nov 01 09.18.40 EET 2017