Wed Nov 01 09.19.03 EET 2017
Dataset: sonar
Test set size: 104
Initial Labelled set size: 20
Initial Unlabelled set size: 84
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Wed Nov 01 09.19.03 EET 2017
		
Iteration: 1
Labeled set size: 20
Unlabelled set size: 84
	
Mean region size: 97.59615384615384
Incorrectly Classified Instances: 28.846153846153847
Correctly Classified Instances: 71.15384615384616
Weighted Precision: 0.7471475892528523
Weighted AreaUnderROC: 0.8327752976190477
Root mean squared error: 0.4203935884748591
Relative absolute error: 73.46153846153845
Root relative squared error: 84.07871769497181
Weighted TruePositiveRate: 0.7115384615384616
Weighted MatthewsCorrelation: 0.46217116665408475
Weighted FMeasure: 0.7066930829528814
Iteration time: 10.0
Weighted AreaUnderPRC: 0.8141097989571229
Mean absolute error: 0.3673076923076923
Coverage of cases: 100.0
Instances selection time: 2.0
Test time: 2.0
Accumulative iteration time: 10.0
Weighted Recall: 0.7115384615384616
Weighted FalsePositiveRate: 0.2651098901098901
Kappa statistic: 0.4347826086956522
Training time: 8.0
		
Iteration: 2
Labeled set size: 40
Unlabelled set size: 64
	
Mean region size: 92.3076923076923
Incorrectly Classified Instances: 49.03846153846154
Correctly Classified Instances: 50.96153846153846
Weighted Precision: 0.7622377622377623
Weighted AreaUnderROC: 0.69140625
Root mean squared error: 0.5585868157853626
Relative absolute error: 92.50000000000003
Root relative squared error: 111.71736315707253
Weighted TruePositiveRate: 0.5096153846153846
Weighted MatthewsCorrelation: 0.20806259464411975
Weighted FMeasure: 0.3896852561957949
Iteration time: 4.0
Weighted AreaUnderPRC: 0.6637018694276264
Mean absolute error: 0.46250000000000013
Coverage of cases: 91.34615384615384
Instances selection time: 1.0
Test time: 0.0
Accumulative iteration time: 14.0
Weighted Recall: 0.5096153846153846
Weighted FalsePositiveRate: 0.42032967032967034
Kappa statistic: 0.08298755186721983
Training time: 3.0
		
Iteration: 3
Labeled set size: 60
Unlabelled set size: 44
	
Mean region size: 90.86538461538461
Incorrectly Classified Instances: 45.19230769230769
Correctly Classified Instances: 54.80769230769231
Weighted Precision: 0.7716599190283401
Weighted AreaUnderROC: 0.8578869047619049
Root mean squared error: 0.4948776072463231
Relative absolute error: 79.03846153846155
Root relative squared error: 98.97552144926462
Weighted TruePositiveRate: 0.5480769230769231
Weighted MatthewsCorrelation: 0.2849614140875866
Weighted FMeasure: 0.45895642818719745
Iteration time: 6.0
Weighted AreaUnderPRC: 0.825834522690003
Mean absolute error: 0.39519230769230773
Coverage of cases: 97.11538461538461
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 20.0
Weighted Recall: 0.5480769230769231
Weighted FalsePositiveRate: 0.3873626373626374
Kappa statistic: 0.15020862308762184
Training time: 5.0
		
Iteration: 4
Labeled set size: 80
Unlabelled set size: 24
	
Mean region size: 97.11538461538461
Incorrectly Classified Instances: 28.846153846153847
Correctly Classified Instances: 71.15384615384616
Weighted Precision: 0.7555304172951232
Weighted AreaUnderROC: 0.804873511904762
Root mean squared error: 0.43644102606282237
Relative absolute error: 77.49999999999999
Root relative squared error: 87.28820521256448
Weighted TruePositiveRate: 0.7115384615384616
Weighted MatthewsCorrelation: 0.4709194144722212
Weighted FMeasure: 0.7050513204935994
Iteration time: 10.0
Weighted AreaUnderPRC: 0.7919743244868628
Mean absolute error: 0.3874999999999999
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 30.0
Weighted Recall: 0.7115384615384616
Weighted FalsePositiveRate: 0.2621336996336997
Kappa statistic: 0.4364161849710983
Training time: 10.0
		
Iteration: 5
Labeled set size: 100
Unlabelled set size: 4
	
Mean region size: 94.23076923076923
Incorrectly Classified Instances: 22.115384615384617
Correctly Classified Instances: 77.88461538461539
Weighted Precision: 0.8082730277852228
Weighted AreaUnderROC: 0.8309151785714286
Root mean squared error: 0.4133586634091325
Relative absolute error: 68.65384615384616
Root relative squared error: 82.6717326818265
Weighted TruePositiveRate: 0.7788461538461539
Weighted MatthewsCorrelation: 0.588999439687084
Weighted FMeasure: 0.7766894571018282
Iteration time: 12.0
Weighted AreaUnderPRC: 0.8042479077352275
Mean absolute error: 0.34326923076923077
Coverage of cases: 99.03846153846153
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 42.0
Weighted Recall: 0.7788461538461539
Weighted FalsePositiveRate: 0.20146520146520144
Kappa statistic: 0.5647743813682679
Training time: 12.0
		
Time end:Wed Nov 01 09.19.03 EET 2017