Wed Nov 01 09.18.52 EET 2017
Dataset: sonar
Test set size: 104
Initial Labelled set size: 20
Initial Unlabelled set size: 84
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Wed Nov 01 09.18.52 EET 2017
		
Iteration: 1
Labeled set size: 20
Unlabelled set size: 84
	
Mean region size: 98.5576923076923
Incorrectly Classified Instances: 28.846153846153847
Correctly Classified Instances: 71.15384615384616
Weighted Precision: 0.7127065527065527
Weighted AreaUnderROC: 0.7905505952380951
Root mean squared error: 0.42866070498705605
Relative absolute error: 74.80769230769234
Root relative squared error: 85.73214099741121
Weighted TruePositiveRate: 0.7115384615384616
Weighted MatthewsCorrelation: 0.42167884835071406
Weighted FMeasure: 0.7118595690024262
Iteration time: 13.0
Weighted AreaUnderPRC: 0.7829285169114648
Mean absolute error: 0.3740384615384617
Coverage of cases: 100.0
Instances selection time: 3.0
Test time: 0.0
Accumulative iteration time: 13.0
Weighted Recall: 0.7115384615384616
Weighted FalsePositiveRate: 0.28891941391941395
Kappa statistic: 0.42136498516320486
Training time: 10.0
		
Iteration: 2
Labeled set size: 40
Unlabelled set size: 64
	
Mean region size: 97.11538461538461
Incorrectly Classified Instances: 25.96153846153846
Correctly Classified Instances: 74.03846153846153
Weighted Precision: 0.7443786982248521
Weighted AreaUnderROC: 0.7965029761904762
Root mean squared error: 0.4264116826224663
Relative absolute error: 72.88461538461539
Root relative squared error: 85.28233652449326
Weighted TruePositiveRate: 0.7403846153846154
Weighted MatthewsCorrelation: 0.47809144373375745
Weighted FMeasure: 0.7366113510314137
Iteration time: 4.0
Weighted AreaUnderPRC: 0.7703657864938193
Mean absolute error: 0.36442307692307696
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 17.0
Weighted Recall: 0.7403846153846154
Weighted FalsePositiveRate: 0.2760989010989011
Kappa statistic: 0.47058823529411775
Training time: 4.0
		
Iteration: 3
Labeled set size: 60
Unlabelled set size: 44
	
Mean region size: 98.07692307692308
Incorrectly Classified Instances: 26.923076923076923
Correctly Classified Instances: 73.07692307692308
Weighted Precision: 0.7319088319088318
Weighted AreaUnderROC: 0.8151041666666666
Root mean squared error: 0.4202792112210825
Relative absolute error: 74.03846153846156
Root relative squared error: 84.0558422442165
Weighted TruePositiveRate: 0.7307692307692307
Weighted MatthewsCorrelation: 0.46028324996028647
Weighted FMeasure: 0.731068931068931
Iteration time: 7.0
Weighted AreaUnderPRC: 0.7852918588834866
Mean absolute error: 0.3701923076923078
Coverage of cases: 99.03846153846153
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 24.0
Weighted Recall: 0.7307692307692307
Weighted FalsePositiveRate: 0.269459706959707
Kappa statistic: 0.45994065281899105
Training time: 6.0
		
Iteration: 4
Labeled set size: 80
Unlabelled set size: 24
	
Mean region size: 97.11538461538461
Incorrectly Classified Instances: 25.96153846153846
Correctly Classified Instances: 74.03846153846153
Weighted Precision: 0.7423375736361308
Weighted AreaUnderROC: 0.7896205357142858
Root mean squared error: 0.432901658756173
Relative absolute error: 75.57692307692308
Root relative squared error: 86.5803317512346
Weighted TruePositiveRate: 0.7403846153846154
Weighted MatthewsCorrelation: 0.4808031967121333
Weighted FMeasure: 0.7407454930390709
Iteration time: 8.0
Weighted AreaUnderPRC: 0.754314946462011
Mean absolute error: 0.3778846153846154
Coverage of cases: 99.03846153846153
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 32.0
Weighted Recall: 0.7403846153846154
Weighted FalsePositiveRate: 0.25824175824175827
Kappa statistic: 0.48000000000000004
Training time: 8.0
		
Iteration: 5
Labeled set size: 100
Unlabelled set size: 4
	
Mean region size: 98.5576923076923
Incorrectly Classified Instances: 25.96153846153846
Correctly Classified Instances: 74.03846153846153
Weighted Precision: 0.7400867143308353
Weighted AreaUnderROC: 0.8377976190476191
Root mean squared error: 0.41219394240314355
Relative absolute error: 74.4230769230769
Root relative squared error: 82.43878848062872
Weighted TruePositiveRate: 0.7403846153846154
Weighted MatthewsCorrelation: 0.47698967755199584
Weighted FMeasure: 0.7401669592633728
Iteration time: 12.0
Weighted AreaUnderPRC: 0.8122269343518749
Mean absolute error: 0.3721153846153845
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 44.0
Weighted Recall: 0.7403846153846154
Weighted FalsePositiveRate: 0.2641941391941392
Kappa statistic: 0.47690014903129657
Training time: 12.0
		
Time end:Wed Nov 01 09.18.52 EET 2017