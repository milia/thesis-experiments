Wed Nov 01 09.18.50 EET 2017
Dataset: sonar
Test set size: 104
Initial Labelled set size: 20
Initial Unlabelled set size: 84
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Wed Nov 01 09.18.50 EET 2017
		
Iteration: 1
Labeled set size: 20
Unlabelled set size: 84
	
Mean region size: 97.01923076923075
Incorrectly Classified Instances: 37.019230769230774
Correctly Classified Instances: 62.98076923076924
Weighted Precision: 0.6417285893243557
Weighted AreaUnderROC: 0.696408907312925
Root mean squared error: 0.47804986274367317
Relative absolute error: 83.57692307692307
Root relative squared error: 95.60997254873462
Weighted TruePositiveRate: 0.6298076923076923
Weighted MatthewsCorrelation: 0.26826009071564155
Weighted FMeasure: 0.6227496609310789
Iteration time: 6.1
Weighted AreaUnderPRC: 0.6771862041827426
Mean absolute error: 0.41788461538461535
Coverage of cases: 98.9423076923077
Instances selection time: 2.5
Test time: 1.9
Accumulative iteration time: 6.1
Weighted Recall: 0.6298076923076923
Weighted FalsePositiveRate: 0.37298332619761193
Kappa statistic: 0.2572514553172797
Training time: 3.6
		
Iteration: 2
Labeled set size: 40
Unlabelled set size: 64
	
Mean region size: 96.77884615384616
Incorrectly Classified Instances: 29.90384615384615
Correctly Classified Instances: 70.09615384615384
Weighted Precision: 0.7060605012100657
Weighted AreaUnderROC: 0.7626963029529993
Root mean squared error: 0.44483613129563376
Relative absolute error: 75.6346153846154
Root relative squared error: 88.96722625912676
Weighted TruePositiveRate: 0.7009615384615384
Weighted MatthewsCorrelation: 0.402356450910062
Weighted FMeasure: 0.6976377198134065
Iteration time: 5.3
Weighted AreaUnderPRC: 0.7427502687150191
Mean absolute error: 0.378173076923077
Coverage of cases: 99.13461538461539
Instances selection time: 1.6
Test time: 0.8
Accumulative iteration time: 11.4
Weighted Recall: 0.7009615384615384
Weighted FalsePositiveRate: 0.30759109937681367
Kappa statistic: 0.3956562743535373
Training time: 3.7
		
Iteration: 3
Labeled set size: 60
Unlabelled set size: 44
	
Mean region size: 95.67307692307692
Incorrectly Classified Instances: 29.13461538461538
Correctly Classified Instances: 70.86538461538461
Weighted Precision: 0.7116864839154248
Weighted AreaUnderROC: 0.7894524002782932
Root mean squared error: 0.4310731828419482
Relative absolute error: 72.30769230769232
Root relative squared error: 86.21463656838966
Weighted TruePositiveRate: 0.7086538461538461
Weighted MatthewsCorrelation: 0.41774367519988453
Weighted FMeasure: 0.7076755242022145
Iteration time: 8.5
Weighted AreaUnderPRC: 0.7702741515976581
Mean absolute error: 0.3615384615384617
Coverage of cases: 99.03846153846153
Instances selection time: 2.0
Test time: 1.0
Accumulative iteration time: 19.9
Weighted Recall: 0.7086538461538461
Weighted FalsePositiveRate: 0.2935842728699872
Kappa statistic: 0.4148336387237867
Training time: 6.5
		
Iteration: 4
Labeled set size: 80
Unlabelled set size: 24
	
Mean region size: 96.58653846153847
Incorrectly Classified Instances: 29.03846153846154
Correctly Classified Instances: 70.96153846153847
Weighted Precision: 0.7183578164195594
Weighted AreaUnderROC: 0.7894415777674707
Root mean squared error: 0.4308387740462405
Relative absolute error: 72.86538461538461
Root relative squared error: 86.1677548092481
Weighted TruePositiveRate: 0.7096153846153846
Weighted MatthewsCorrelation: 0.42722566359911207
Weighted FMeasure: 0.7084616197343064
Iteration time: 11.3
Weighted AreaUnderPRC: 0.771189259646638
Mean absolute error: 0.3643269230769231
Coverage of cases: 99.42307692307693
Instances selection time: 1.6
Test time: 1.7
Accumulative iteration time: 31.2
Weighted Recall: 0.7096153846153846
Weighted FalsePositiveRate: 0.28565666476380763
Kappa statistic: 0.4206635741545671
Training time: 9.7
		
Iteration: 5
Labeled set size: 100
Unlabelled set size: 4
	
Mean region size: 96.00961538461539
Incorrectly Classified Instances: 25.384615384615383
Correctly Classified Instances: 74.61538461538461
Weighted Precision: 0.7520636705303971
Weighted AreaUnderROC: 0.8284187538651823
Root mean squared error: 0.41091070071698166
Relative absolute error: 69.19230769230771
Root relative squared error: 82.18214014339635
Weighted TruePositiveRate: 0.7461538461538463
Weighted MatthewsCorrelation: 0.4961480360304783
Weighted FMeasure: 0.7449120615195732
Iteration time: 14.4
Weighted AreaUnderPRC: 0.8085469117985478
Mean absolute error: 0.3459615384615385
Coverage of cases: 99.51923076923077
Instances selection time: 0.7
Test time: 1.7
Accumulative iteration time: 45.6
Weighted Recall: 0.7461538461538463
Weighted FalsePositiveRate: 0.2545065053993626
Kappa statistic: 0.4905963453939132
Training time: 13.7
		
Time end:Wed Nov 01 09.18.53 EET 2017