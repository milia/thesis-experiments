Wed Nov 01 09.19.00 EET 2017
Dataset: sonar
Test set size: 104
Initial Labelled set size: 20
Initial Unlabelled set size: 84
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Wed Nov 01 09.19.00 EET 2017
		
Iteration: 1
Labeled set size: 20
Unlabelled set size: 84
	
Mean region size: 72.25961538461539
Incorrectly Classified Instances: 35.19230769230769
Correctly Classified Instances: 64.8076923076923
Weighted Precision: 0.6533947264999709
Weighted AreaUnderROC: 0.6411521625695733
Root mean squared error: 0.5672189818744895
Relative absolute error: 73.99030136530138
Root relative squared error: 113.44379637489787
Weighted TruePositiveRate: 0.6480769230769231
Weighted MatthewsCorrelation: 0.29592934827244466
Weighted FMeasure: 0.6432649799483229
Iteration time: 3.2
Weighted AreaUnderPRC: 0.599707964360835
Mean absolute error: 0.36995150682650685
Coverage of cases: 79.23076923076924
Instances selection time: 0.4
Test time: 0.9
Accumulative iteration time: 3.2
Weighted Recall: 0.6480769230769231
Weighted FalsePositiveRate: 0.3628388278388278
Kappa statistic: 0.28847354931747693
Training time: 2.8
		
Iteration: 2
Labeled set size: 40
Unlabelled set size: 64
	
Mean region size: 54.27884615384615
Incorrectly Classified Instances: 37.98076923076923
Correctly Classified Instances: 62.01923076923076
Weighted Precision: 0.6389368348763957
Weighted AreaUnderROC: 0.6323939490568955
Root mean squared error: 0.6010509413018579
Relative absolute error: 76.7341169644393
Root relative squared error: 120.21018826037157
Weighted TruePositiveRate: 0.6201923076923077
Weighted MatthewsCorrelation: 0.2608345478426233
Weighted FMeasure: 0.6103293717623487
Iteration time: 5.2
Weighted AreaUnderPRC: 0.5917369964022476
Mean absolute error: 0.3836705848221965
Coverage of cases: 65.48076923076923
Instances selection time: 0.3
Test time: 0.8
Accumulative iteration time: 8.4
Weighted Recall: 0.6201923076923077
Weighted FalsePositiveRate: 0.36870343941772515
Kappa statistic: 0.24857638922570297
Training time: 4.9
		
Iteration: 3
Labeled set size: 60
Unlabelled set size: 44
	
Mean region size: 54.75961538461538
Incorrectly Classified Instances: 36.346153846153854
Correctly Classified Instances: 63.65384615384615
Weighted Precision: 0.663708643593518
Weighted AreaUnderROC: 0.6511486355906
Root mean squared error: 0.591460702474939
Relative absolute error: 73.16712866712867
Root relative squared error: 118.2921404949878
Weighted TruePositiveRate: 0.6365384615384616
Weighted MatthewsCorrelation: 0.30238468452317874
Weighted FMeasure: 0.6256606290264306
Iteration time: 7.1
Weighted AreaUnderPRC: 0.6108288907479938
Mean absolute error: 0.36583564333564333
Coverage of cases: 67.88461538461539
Instances selection time: 0.1
Test time: 1.0
Accumulative iteration time: 15.5
Weighted Recall: 0.6365384615384616
Weighted FalsePositiveRate: 0.3490299581371009
Kappa statistic: 0.28338582983682353
Training time: 7.0
		
Iteration: 4
Labeled set size: 80
Unlabelled set size: 24
	
Mean region size: 57.98076923076924
Incorrectly Classified Instances: 33.36538461538461
Correctly Classified Instances: 66.63461538461539
Weighted Precision: 0.6696731157296677
Weighted AreaUnderROC: 0.6854952748144713
Root mean squared error: 0.556377350893113
Relative absolute error: 66.95693956581053
Root relative squared error: 111.27547017862261
Weighted TruePositiveRate: 0.6663461538461538
Weighted MatthewsCorrelation: 0.33274598158162744
Weighted FMeasure: 0.6642694092443825
Iteration time: 12.7
Weighted AreaUnderPRC: 0.6436931124379935
Mean absolute error: 0.3347846978290527
Coverage of cases: 73.65384615384616
Instances selection time: 0.5
Test time: 0.5
Accumulative iteration time: 28.2
Weighted Recall: 0.6663461538461538
Weighted FalsePositiveRate: 0.33811563436563435
Kappa statistic: 0.32919933204417073
Training time: 12.2
		
Iteration: 5
Labeled set size: 100
Unlabelled set size: 4
	
Mean region size: 52.98076923076924
Incorrectly Classified Instances: 28.36538461538462
Correctly Classified Instances: 71.63461538461539
Weighted Precision: 0.7184344560577249
Weighted AreaUnderROC: 0.7085419082405688
Root mean squared error: 0.5190108931428922
Relative absolute error: 57.88555540072324
Root relative squared error: 103.80217862857845
Weighted TruePositiveRate: 0.716346153846154
Weighted MatthewsCorrelation: 0.4312789753893937
Weighted FMeasure: 0.7149922590598994
Iteration time: 16.0
Weighted AreaUnderPRC: 0.6615282071951246
Mean absolute error: 0.28942777700361616
Coverage of cases: 74.42307692307693
Instances selection time: 0.2
Test time: 0.6
Accumulative iteration time: 44.2
Weighted Recall: 0.716346153846154
Weighted FalsePositiveRate: 0.2895171495171495
Kappa statistic: 0.4283487588647186
Training time: 15.8
		
Time end:Wed Nov 01 09.19.03 EET 2017