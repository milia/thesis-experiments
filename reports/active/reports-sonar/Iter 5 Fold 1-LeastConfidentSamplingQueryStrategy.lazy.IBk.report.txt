Wed Nov 01 09.18.20 EET 2017
Dataset: sonar
Test set size: 104
Initial Labelled set size: 20
Initial Unlabelled set size: 84
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Wed Nov 01 09.18.20 EET 2017
		
Iteration: 1
Labeled set size: 20
Unlabelled set size: 84
	
Mean region size: 50.0
Incorrectly Classified Instances: 31.73076923076923
Correctly Classified Instances: 68.26923076923077
Weighted Precision: 0.681877444589309
Weighted AreaUnderROC: 0.6785714285714286
Root mean squared error: 0.5390061726585496
Relative absolute error: 66.78321678321682
Root relative squared error: 107.80123453170991
Weighted TruePositiveRate: 0.6826923076923077
Weighted MatthewsCorrelation: 0.35935553470461673
Weighted FMeasure: 0.6817132376739671
Iteration time: 5.0
Weighted AreaUnderPRC: 0.6238090462340788
Mean absolute error: 0.33391608391608407
Coverage of cases: 68.26923076923077
Instances selection time: 5.0
Test time: 5.0
Accumulative iteration time: 5.0
Weighted Recall: 0.6826923076923077
Weighted FalsePositiveRate: 0.3255494505494506
Kappa statistic: 0.3587443946188341
Training time: 0.0
		
Iteration: 2
Labeled set size: 40
Unlabelled set size: 64
	
Mean region size: 50.0
Incorrectly Classified Instances: 25.0
Correctly Classified Instances: 75.0
Weighted Precision: 0.7988165680473371
Weighted AreaUnderROC: 0.7321428571428571
Root mean squared error: 0.48853058401626637
Relative absolute error: 52.38095238095237
Root relative squared error: 97.70611680325328
Weighted TruePositiveRate: 0.75
Weighted MatthewsCorrelation: 0.5345224838248487
Weighted FMeasure: 0.7333602258975394
Iteration time: 5.0
Weighted AreaUnderPRC: 0.6893491124260354
Mean absolute error: 0.26190476190476186
Coverage of cases: 75.0
Instances selection time: 5.0
Test time: 8.0
Accumulative iteration time: 10.0
Weighted Recall: 0.75
Weighted FalsePositiveRate: 0.2857142857142857
Kappa statistic: 0.4799999999999999
Training time: 0.0
		
Iteration: 3
Labeled set size: 60
Unlabelled set size: 44
	
Mean region size: 50.0
Incorrectly Classified Instances: 25.96153846153846
Correctly Classified Instances: 74.03846153846153
Weighted Precision: 0.8070936041950534
Weighted AreaUnderROC: 0.7202380952380951
Root mean squared error: 0.5014985946846348
Relative absolute error: 53.473945409429305
Root relative squared error: 100.29971893692695
Weighted TruePositiveRate: 0.7403846153846154
Weighted MatthewsCorrelation: 0.5290913272721803
Weighted FMeasure: 0.7183652165661006
Iteration time: 5.0
Weighted AreaUnderPRC: 0.6819963347276615
Mean absolute error: 0.2673697270471465
Coverage of cases: 74.03846153846153
Instances selection time: 5.0
Test time: 11.0
Accumulative iteration time: 15.0
Weighted Recall: 0.7403846153846154
Weighted FalsePositiveRate: 0.2999084249084249
Kappa statistic: 0.45749613601236483
Training time: 0.0
		
Iteration: 4
Labeled set size: 80
Unlabelled set size: 24
	
Mean region size: 50.0
Incorrectly Classified Instances: 13.461538461538462
Correctly Classified Instances: 86.53846153846153
Weighted Precision: 0.8778064041221937
Weighted AreaUnderROC: 0.8571428571428571
Root mean squared error: 0.36260281790272936
Relative absolute error: 28.705440900562834
Root relative squared error: 72.52056358054587
Weighted TruePositiveRate: 0.8653846153846154
Weighted MatthewsCorrelation: 0.7394738666465356
Weighted FMeasure: 0.8630751634945306
Iteration time: 4.0
Weighted AreaUnderPRC: 0.816369864952861
Mean absolute error: 0.14352720450281417
Coverage of cases: 86.53846153846153
Instances selection time: 4.0
Test time: 13.0
Accumulative iteration time: 19.0
Weighted Recall: 0.8653846153846154
Weighted FalsePositiveRate: 0.1510989010989011
Kappa statistic: 0.7250755287009064
Training time: 0.0
		
Iteration: 5
Labeled set size: 100
Unlabelled set size: 4
	
Mean region size: 50.0
Incorrectly Classified Instances: 9.615384615384615
Correctly Classified Instances: 90.38461538461539
Weighted Precision: 0.9057109557109556
Weighted AreaUnderROC: 0.9002976190476191
Root mean squared error: 0.30718820533923763
Relative absolute error: 20.814479638009047
Root relative squared error: 61.43764106784752
Weighted TruePositiveRate: 0.9038461538461539
Weighted MatthewsCorrelation: 0.8078405915503513
Weighted FMeasure: 0.9034136777764963
Iteration time: 2.0
Weighted AreaUnderPRC: 0.8641092881477496
Mean absolute error: 0.10407239819004523
Coverage of cases: 90.38461538461539
Instances selection time: 2.0
Test time: 15.0
Accumulative iteration time: 21.0
Weighted Recall: 0.9038461538461539
Weighted FalsePositiveRate: 0.10325091575091576
Kappa statistic: 0.8053892215568862
Training time: 0.0
		
Time end:Wed Nov 01 09.18.20 EET 2017