Wed Nov 01 09.17.44 EET 2017
Dataset: sonar
Test set size: 104
Initial Labelled set size: 20
Initial Unlabelled set size: 84
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Nov 01 09.17.44 EET 2017
		
Iteration: 1
Labeled set size: 20
Unlabelled set size: 84
	
Mean region size: 50.0
Incorrectly Classified Instances: 22.115384615384617
Correctly Classified Instances: 77.88461538461539
Weighted Precision: 0.7895499365701765
Weighted AreaUnderROC: 0.7831168831168831
Root mean squared error: 0.4702699715629801
Relative absolute error: 44.230769230769226
Root relative squared error: 94.05399431259602
Weighted TruePositiveRate: 0.7788461538461539
Weighted MatthewsCorrelation: 0.5690916798348435
Weighted FMeasure: 0.7782936273497947
Iteration time: 28.0
Weighted AreaUnderPRC: 0.723448325763154
Mean absolute error: 0.22115384615384615
Coverage of cases: 77.88461538461539
Instances selection time: 11.0
Test time: 2.0
Accumulative iteration time: 28.0
Weighted Recall: 0.7788461538461539
Weighted FalsePositiveRate: 0.2126123876123876
Kappa statistic: 0.5606171932402646
Training time: 17.0
		
Iteration: 2
Labeled set size: 40
Unlabelled set size: 64
	
Mean region size: 50.0
Incorrectly Classified Instances: 25.96153846153846
Correctly Classified Instances: 74.03846153846153
Weighted Precision: 0.7818784565108093
Weighted AreaUnderROC: 0.7500927643784787
Root mean squared error: 0.5095246653650681
Relative absolute error: 51.92307692307693
Root relative squared error: 101.90493307301362
Weighted TruePositiveRate: 0.7403846153846154
Weighted MatthewsCorrelation: 0.5248132615155345
Weighted FMeasure: 0.7343617920540997
Iteration time: 21.0
Weighted AreaUnderPRC: 0.6949224339637236
Mean absolute error: 0.25961538461538464
Coverage of cases: 74.03846153846153
Instances selection time: 6.0
Test time: 4.0
Accumulative iteration time: 49.0
Weighted Recall: 0.7403846153846154
Weighted FalsePositiveRate: 0.24019908662765801
Kappa statistic: 0.48982558139534893
Training time: 15.0
		
Iteration: 3
Labeled set size: 60
Unlabelled set size: 44
	
Mean region size: 50.0
Incorrectly Classified Instances: 32.69230769230769
Correctly Classified Instances: 67.3076923076923
Weighted Precision: 0.7668451801363194
Weighted AreaUnderROC: 0.688682745825603
Root mean squared error: 0.5717718748968657
Relative absolute error: 65.38461538461539
Root relative squared error: 114.35437497937313
Weighted TruePositiveRate: 0.6730769230769231
Weighted MatthewsCorrelation: 0.4408166788002632
Weighted FMeasure: 0.650090144230769
Iteration time: 24.0
Weighted AreaUnderPRC: 0.64410971088308
Mean absolute error: 0.3269230769230769
Coverage of cases: 67.3076923076923
Instances selection time: 6.0
Test time: 4.0
Accumulative iteration time: 73.0
Weighted Recall: 0.6730769230769231
Weighted FalsePositiveRate: 0.29571143142571715
Kappa statistic: 0.3651705565529624
Training time: 18.0
		
Iteration: 4
Labeled set size: 80
Unlabelled set size: 24
	
Mean region size: 50.0
Incorrectly Classified Instances: 28.846153846153847
Correctly Classified Instances: 71.15384615384616
Weighted Precision: 0.7745589584962099
Weighted AreaUnderROC: 0.7239332096474954
Root mean squared error: 0.5370861555295746
Relative absolute error: 57.692307692307686
Root relative squared error: 107.41723110591492
Weighted TruePositiveRate: 0.7115384615384616
Weighted MatthewsCorrelation: 0.4887486922493549
Weighted FMeasure: 0.6996612803894542
Iteration time: 28.0
Weighted AreaUnderPRC: 0.6727781888732294
Mean absolute error: 0.28846153846153844
Coverage of cases: 71.15384615384616
Instances selection time: 6.0
Test time: 2.0
Accumulative iteration time: 101.0
Weighted Recall: 0.7115384615384616
Weighted FalsePositiveRate: 0.26367204224347085
Kappa statistic: 0.436212504517528
Training time: 22.0
		
Iteration: 5
Labeled set size: 100
Unlabelled set size: 4
	
Mean region size: 50.0
Incorrectly Classified Instances: 23.076923076923077
Correctly Classified Instances: 76.92307692307692
Weighted Precision: 0.7934125495101104
Weighted AreaUnderROC: 0.776252319109462
Root mean squared error: 0.4803844614152614
Relative absolute error: 46.15384615384615
Root relative squared error: 96.07689228305229
Weighted TruePositiveRate: 0.7692307692307693
Weighted MatthewsCorrelation: 0.5643559522562586
Weighted FMeasure: 0.7668269230769231
Iteration time: 38.0
Weighted AreaUnderPRC: 0.7186900094381332
Mean absolute error: 0.23076923076923078
Coverage of cases: 76.92307692307692
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 139.0
Weighted Recall: 0.7692307692307693
Weighted FalsePositiveRate: 0.2167261310118453
Kappa statistic: 0.5440263061746438
Training time: 36.0
		
Time end:Wed Nov 01 09.17.44 EET 2017