Wed Nov 01 09.18.31 EET 2017
Dataset: sonar
Test set size: 104
Initial Labelled set size: 20
Initial Unlabelled set size: 84
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Wed Nov 01 09.18.31 EET 2017
		
Iteration: 1
Labeled set size: 20
Unlabelled set size: 84
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.61538461538461
Correctly Classified Instances: 65.38461538461539
Weighted Precision: 0.6538461538461539
Weighted AreaUnderROC: 0.6517857142857142
Root mean squared error: 0.5628067433744858
Relative absolute error: 72.02797202797207
Root relative squared error: 112.56134867489716
Weighted TruePositiveRate: 0.6538461538461539
Weighted MatthewsCorrelation: 0.30357142857142855
Weighted FMeasure: 0.6538461538461539
Iteration time: 4.0
Weighted AreaUnderPRC: 0.6013049450549451
Mean absolute error: 0.3601398601398604
Coverage of cases: 65.38461538461539
Instances selection time: 4.0
Test time: 5.0
Accumulative iteration time: 4.0
Weighted Recall: 0.6538461538461539
Weighted FalsePositiveRate: 0.3502747252747253
Kappa statistic: 0.30357142857142855
Training time: 0.0
		
Iteration: 2
Labeled set size: 40
Unlabelled set size: 64
	
Mean region size: 50.0
Incorrectly Classified Instances: 25.0
Correctly Classified Instances: 75.0
Weighted Precision: 0.7990196078431372
Weighted AreaUnderROC: 0.7633928571428571
Root mean squared error: 0.48853058401626637
Relative absolute error: 52.380952380952394
Root relative squared error: 97.70611680325328
Weighted TruePositiveRate: 0.75
Weighted MatthewsCorrelation: 0.5520048765667758
Weighted FMeasure: 0.7443778110944527
Iteration time: 5.0
Weighted AreaUnderPRC: 0.7096023320570831
Mean absolute error: 0.261904761904762
Coverage of cases: 75.0
Instances selection time: 5.0
Test time: 8.0
Accumulative iteration time: 9.0
Weighted Recall: 0.75
Weighted FalsePositiveRate: 0.22321428571428573
Kappa statistic: 0.5115606936416185
Training time: 0.0
		
Iteration: 3
Labeled set size: 60
Unlabelled set size: 44
	
Mean region size: 50.0
Incorrectly Classified Instances: 20.192307692307693
Correctly Classified Instances: 79.8076923076923
Weighted Precision: 0.8374753451676529
Weighted AreaUnderROC: 0.8095238095238096
Root mean squared error: 0.44234553797001136
Relative absolute error: 42.307692307692314
Root relative squared error: 88.46910759400227
Weighted TruePositiveRate: 0.7980769230769231
Weighted MatthewsCorrelation: 0.6374552583116766
Weighted FMeasure: 0.7951990254738276
Iteration time: 5.0
Weighted AreaUnderPRC: 0.7577909270216963
Mean absolute error: 0.21153846153846156
Coverage of cases: 79.8076923076923
Instances selection time: 5.0
Test time: 11.0
Accumulative iteration time: 14.0
Weighted Recall: 0.7980769230769231
Weighted FalsePositiveRate: 0.179029304029304
Kappa statistic: 0.6037735849056606
Training time: 0.0
		
Iteration: 4
Labeled set size: 80
Unlabelled set size: 24
	
Mean region size: 50.0
Incorrectly Classified Instances: 14.423076923076923
Correctly Classified Instances: 85.57692307692308
Weighted Precision: 0.8563233376792698
Weighted AreaUnderROC: 0.8526785714285714
Root mean squared error: 0.37531542279847707
Relative absolute error: 30.58161350844272
Root relative squared error: 75.06308455969541
Weighted TruePositiveRate: 0.8557692307692307
Weighted MatthewsCorrelation: 0.709727181041618
Weighted FMeasure: 0.8553241989427124
Iteration time: 3.0
Weighted AreaUnderPRC: 0.8034374686591115
Mean absolute error: 0.15290806754221362
Coverage of cases: 85.57692307692308
Instances selection time: 3.0
Test time: 14.0
Accumulative iteration time: 17.0
Weighted Recall: 0.8557692307692307
Weighted FalsePositiveRate: 0.1504120879120879
Kappa statistic: 0.7085201793721972
Training time: 0.0
		
Iteration: 5
Labeled set size: 100
Unlabelled set size: 4
	
Mean region size: 50.0
Incorrectly Classified Instances: 14.423076923076923
Correctly Classified Instances: 85.57692307692308
Weighted Precision: 0.8609547633937878
Weighted AreaUnderROC: 0.8497023809523809
Root mean squared error: 0.37616330470987036
Relative absolute error: 30.24132730015082
Root relative squared error: 75.23266094197407
Weighted TruePositiveRate: 0.8557692307692307
Weighted MatthewsCorrelation: 0.7134787027137358
Weighted FMeasure: 0.8543393156744116
Iteration time: 1.0
Weighted AreaUnderPRC: 0.8032850774407998
Mean absolute error: 0.1512066365007541
Coverage of cases: 85.57692307692308
Instances selection time: 1.0
Test time: 17.0
Accumulative iteration time: 18.0
Weighted Recall: 0.8557692307692307
Weighted FalsePositiveRate: 0.15636446886446884
Kappa statistic: 0.7067669172932329
Training time: 0.0
		
Time end:Wed Nov 01 09.18.32 EET 2017