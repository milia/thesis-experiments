Wed Nov 01 09.18.03 EET 2017
Dataset: sonar
Test set size: 104
Initial Labelled set size: 20
Initial Unlabelled set size: 84
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Wed Nov 01 09.18.03 EET 2017
		
Iteration: 1
Labeled set size: 20
Unlabelled set size: 84
	
Mean region size: 50.0
Incorrectly Classified Instances: 32.21153846153846
Correctly Classified Instances: 67.78846153846153
Weighted Precision: 0.6793233420712526
Weighted AreaUnderROC: 0.6748430735930736
Root mean squared error: 0.5399066375476885
Relative absolute error: 67.65734265734271
Root relative squared error: 107.98132750953773
Weighted TruePositiveRate: 0.6778846153846154
Weighted MatthewsCorrelation: 0.3527877392406732
Weighted FMeasure: 0.6743932240883175
Iteration time: 13.3
Weighted AreaUnderPRC: 0.6247480340343697
Mean absolute error: 0.3382867132867135
Coverage of cases: 67.78846153846153
Instances selection time: 10.2
Test time: 6.6
Accumulative iteration time: 13.3
Weighted Recall: 0.6778846153846154
Weighted FalsePositiveRate: 0.32819846819846826
Kappa statistic: 0.3501426480463673
Training time: 3.1
		
Iteration: 2
Labeled set size: 40
Unlabelled set size: 64
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.326923076923077
Correctly Classified Instances: 70.67307692307692
Weighted Precision: 0.7406077115095067
Weighted AreaUnderROC: 0.7112407235621521
Root mean squared error: 0.5276631003723292
Relative absolute error: 60.62271062271062
Root relative squared error: 105.53262007446585
Weighted TruePositiveRate: 0.7067307692307692
Weighted MatthewsCorrelation: 0.44685576080283856
Weighted FMeasure: 0.6987268660064869
Iteration time: 11.7
Weighted AreaUnderPRC: 0.6591391769631005
Mean absolute error: 0.3031135531135532
Coverage of cases: 70.67307692307692
Instances selection time: 10.8
Test time: 8.4
Accumulative iteration time: 25.0
Weighted Recall: 0.7067307692307692
Weighted FalsePositiveRate: 0.2842493221064649
Kappa statistic: 0.41755184717557714
Training time: 0.9
		
Iteration: 3
Labeled set size: 60
Unlabelled set size: 44
	
Mean region size: 50.0
Incorrectly Classified Instances: 27.788461538461537
Correctly Classified Instances: 72.21153846153848
Weighted Precision: 0.7797113460242225
Weighted AreaUnderROC: 0.7281574675324676
Root mean squared error: 0.5166970945634983
Relative absolute error: 57.00992555831267
Root relative squared error: 103.33941891269964
Weighted TruePositiveRate: 0.7221153846153846
Weighted MatthewsCorrelation: 0.49974126899869614
Weighted FMeasure: 0.7095215183014669
Iteration time: 11.4
Weighted AreaUnderPRC: 0.6807249123672172
Mean absolute error: 0.2850496277915633
Coverage of cases: 72.21153846153848
Instances selection time: 10.5
Test time: 13.0
Accumulative iteration time: 36.4
Weighted Recall: 0.7221153846153846
Weighted FalsePositiveRate: 0.26580044955044957
Kappa statistic: 0.4493747353192143
Training time: 0.9
		
Iteration: 4
Labeled set size: 80
Unlabelled set size: 24
	
Mean region size: 50.0
Incorrectly Classified Instances: 18.846153846153847
Correctly Classified Instances: 81.15384615384616
Weighted Precision: 0.817439469377315
Weighted AreaUnderROC: 0.808505720470006
Root mean squared error: 0.42436987177112134
Relative absolute error: 39.21200750469039
Root relative squared error: 84.87397435422426
Weighted TruePositiveRate: 0.8115384615384617
Weighted MatthewsCorrelation: 0.6262479848906324
Weighted FMeasure: 0.8098993117785689
Iteration time: 12.0
Weighted AreaUnderPRC: 0.7563052083344723
Mean absolute error: 0.19606003752345197
Coverage of cases: 81.15384615384616
Instances selection time: 10.4
Test time: 19.2
Accumulative iteration time: 48.4
Weighted Recall: 0.8115384615384617
Weighted FalsePositiveRate: 0.19452702059844912
Kappa statistic: 0.6195995989149587
Training time: 1.6
		
Iteration: 5
Labeled set size: 100
Unlabelled set size: 4
	
Mean region size: 50.0
Incorrectly Classified Instances: 16.73076923076923
Correctly Classified Instances: 83.26923076923076
Weighted Precision: 0.8370199425420914
Weighted AreaUnderROC: 0.8279719387755101
Root mean squared error: 0.40135186876039713
Relative absolute error: 34.766214177978945
Root relative squared error: 80.27037375207945
Weighted TruePositiveRate: 0.8326923076923076
Weighted MatthewsCorrelation: 0.6667856535392195
Weighted FMeasure: 0.8311504849940832
Iteration time: 3.2
Weighted AreaUnderPRC: 0.7780311119978103
Mean absolute error: 0.17383107088989475
Coverage of cases: 83.26923076923076
Instances selection time: 2.4
Test time: 19.1
Accumulative iteration time: 51.6
Weighted Recall: 0.8326923076923076
Weighted FalsePositiveRate: 0.17674843014128727
Kappa statistic: 0.6610739189205169
Training time: 0.8
		
Time end:Wed Nov 01 09.18.06 EET 2017