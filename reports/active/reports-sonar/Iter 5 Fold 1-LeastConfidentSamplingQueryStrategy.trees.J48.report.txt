Wed Nov 01 09.18.22 EET 2017
Dataset: sonar
Test set size: 104
Initial Labelled set size: 20
Initial Unlabelled set size: 84
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Wed Nov 01 09.18.22 EET 2017
		
Iteration: 1
Labeled set size: 20
Unlabelled set size: 84
	
Mean region size: 61.05769230769231
Incorrectly Classified Instances: 37.5
Correctly Classified Instances: 62.5
Weighted Precision: 0.6311688311688312
Weighted AreaUnderROC: 0.6233258928571429
Root mean squared error: 0.5959599560631311
Relative absolute error: 79.32692307692307
Root relative squared error: 119.19199121262622
Weighted TruePositiveRate: 0.625
Weighted MatthewsCorrelation: 0.25561975913420426
Weighted FMeasure: 0.625242718446602
Iteration time: 2.0
Weighted AreaUnderPRC: 0.582187867077977
Mean absolute error: 0.39663461538461536
Coverage of cases: 69.23076923076923
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 2.0
Weighted Recall: 0.625
Weighted FalsePositiveRate: 0.369047619047619
Kappa statistic: 0.2533136966126657
Training time: 2.0
		
Iteration: 2
Labeled set size: 40
Unlabelled set size: 64
	
Mean region size: 50.0
Incorrectly Classified Instances: 36.53846153846154
Correctly Classified Instances: 63.46153846153846
Weighted Precision: 0.6421703296703296
Weighted AreaUnderROC: 0.6383928571428571
Root mean squared error: 0.6044705248269888
Relative absolute error: 73.07692307692307
Root relative squared error: 120.89410496539776
Weighted TruePositiveRate: 0.6346153846153846
Weighted MatthewsCorrelation: 0.2767857142857143
Weighted FMeasure: 0.6346153846153846
Iteration time: 10.0
Weighted AreaUnderPRC: 0.5907848161453931
Mean absolute error: 0.36538461538461536
Coverage of cases: 63.46153846153846
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 12.0
Weighted Recall: 0.6346153846153846
Weighted FalsePositiveRate: 0.35782967032967034
Kappa statistic: 0.27352941176470585
Training time: 9.0
		
Iteration: 3
Labeled set size: 60
Unlabelled set size: 44
	
Mean region size: 54.32692307692308
Incorrectly Classified Instances: 36.53846153846154
Correctly Classified Instances: 63.46153846153846
Weighted Precision: 0.6567307692307693
Weighted AreaUnderROC: 0.6117931547619048
Root mean squared error: 0.6009807689230868
Relative absolute error: 75.48076923076923
Root relative squared error: 120.19615378461737
Weighted TruePositiveRate: 0.6346153846153846
Weighted MatthewsCorrelation: 0.27115365672539904
Weighted FMeasure: 0.6059577677224737
Iteration time: 20.0
Weighted AreaUnderPRC: 0.5748419943820225
Mean absolute error: 0.37740384615384615
Coverage of cases: 65.38461538461539
Instances selection time: 3.0
Test time: 1.0
Accumulative iteration time: 32.0
Weighted Recall: 0.6346153846153846
Weighted FalsePositiveRate: 0.405448717948718
Kappa statistic: 0.23765432098765427
Training time: 17.0
		
Iteration: 4
Labeled set size: 80
Unlabelled set size: 24
	
Mean region size: 54.80769230769231
Incorrectly Classified Instances: 27.884615384615383
Correctly Classified Instances: 72.11538461538461
Weighted Precision: 0.7567987567987567
Weighted AreaUnderROC: 0.7377232142857142
Root mean squared error: 0.5143026458598687
Relative absolute error: 56.92307692307694
Root relative squared error: 102.86052917197375
Weighted TruePositiveRate: 0.7211538461538461
Weighted MatthewsCorrelation: 0.4636269546145814
Weighted FMeasure: 0.7041295546558705
Iteration time: 16.0
Weighted AreaUnderPRC: 0.6883856501906364
Mean absolute error: 0.2846153846153847
Coverage of cases: 77.88461538461539
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 48.0
Weighted Recall: 0.7211538461538461
Weighted FalsePositiveRate: 0.3134157509157509
Kappa statistic: 0.42089093701996927
Training time: 16.0
		
Iteration: 5
Labeled set size: 100
Unlabelled set size: 4
	
Mean region size: 54.80769230769231
Incorrectly Classified Instances: 31.73076923076923
Correctly Classified Instances: 68.26923076923077
Weighted Precision: 0.682014135311886
Weighted AreaUnderROC: 0.699032738095238
Root mean squared error: 0.542651963827787
Relative absolute error: 62.5
Root relative squared error: 108.53039276555741
Weighted TruePositiveRate: 0.6826923076923077
Weighted MatthewsCorrelation: 0.35852807630228006
Weighted FMeasure: 0.6807551422936039
Iteration time: 25.0
Weighted AreaUnderPRC: 0.6508496370189992
Mean absolute error: 0.3125
Coverage of cases: 74.03846153846153
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 73.0
Weighted Recall: 0.6826923076923077
Weighted FalsePositiveRate: 0.328525641025641
Kappa statistic: 0.3568215892053974
Training time: 25.0
		
Time end:Wed Nov 01 09.18.23 EET 2017