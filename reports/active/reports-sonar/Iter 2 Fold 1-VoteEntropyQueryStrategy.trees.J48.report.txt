Wed Nov 01 09.19.01 EET 2017
Dataset: sonar
Test set size: 104
Initial Labelled set size: 20
Initial Unlabelled set size: 84
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Wed Nov 01 09.19.01 EET 2017
		
Iteration: 1
Labeled set size: 20
Unlabelled set size: 84
	
Mean region size: 100.0
Incorrectly Classified Instances: 42.30769230769231
Correctly Classified Instances: 57.69230769230769
Weighted Precision: 0.5807763664906522
Weighted AreaUnderROC: 0.5788497217068646
Root mean squared error: 0.5889517253077057
Relative absolute error: 87.60683760683753
Root relative squared error: 117.79034506154113
Weighted TruePositiveRate: 0.5769230769230769
Weighted MatthewsCorrelation: 0.15769944341372913
Weighted FMeasure: 0.5769230769230769
Iteration time: 2.0
Weighted AreaUnderPRC: 0.5471544252450846
Mean absolute error: 0.43803418803418764
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 2.0
Weighted Recall: 0.5769230769230769
Weighted FalsePositiveRate: 0.4192236335093478
Kappa statistic: 0.15665315149281228
Training time: 2.0
		
Iteration: 2
Labeled set size: 40
Unlabelled set size: 64
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.61538461538461
Correctly Classified Instances: 65.38461538461539
Weighted Precision: 0.6953773925604911
Weighted AreaUnderROC: 0.6649350649350649
Root mean squared error: 0.5883484054145521
Relative absolute error: 69.23076923076923
Root relative squared error: 117.66968108291043
Weighted TruePositiveRate: 0.6538461538461539
Weighted MatthewsCorrelation: 0.3537824066161761
Weighted FMeasure: 0.6423076923076925
Iteration time: 4.0
Weighted AreaUnderPRC: 0.6150435989615295
Mean absolute error: 0.34615384615384615
Coverage of cases: 65.38461538461539
Instances selection time: 0.0
Test time: 0.0
Accumulative iteration time: 6.0
Weighted Recall: 0.6538461538461539
Weighted FalsePositiveRate: 0.32397602397602393
Kappa statistic: 0.32198478812024633
Training time: 4.0
		
Iteration: 3
Labeled set size: 60
Unlabelled set size: 44
	
Mean region size: 61.05769230769231
Incorrectly Classified Instances: 46.15384615384615
Correctly Classified Instances: 53.84615384615385
Weighted Precision: 0.5510848126232741
Weighted AreaUnderROC: 0.5716141001855287
Root mean squared error: 0.6483707391489959
Relative absolute error: 91.15384615384616
Root relative squared error: 129.67414782979918
Weighted TruePositiveRate: 0.5384615384615384
Weighted MatthewsCorrelation: 0.09449929894737884
Weighted FMeasure: 0.5315703333620466
Iteration time: 7.0
Weighted AreaUnderPRC: 0.5610170678680295
Mean absolute error: 0.4557692307692308
Coverage of cases: 66.34615384615384
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 13.0
Weighted Recall: 0.5384615384615384
Weighted FalsePositiveRate: 0.44681033252461827
Kappa statistic: 0.09004739336492881
Training time: 7.0
		
Iteration: 4
Labeled set size: 80
Unlabelled set size: 24
	
Mean region size: 52.88461538461539
Incorrectly Classified Instances: 47.11538461538461
Correctly Classified Instances: 52.88461538461539
Weighted Precision: 0.5360576923076923
Weighted AreaUnderROC: 0.5448979591836735
Root mean squared error: 0.6768138023688074
Relative absolute error: 93.46153846153847
Root relative squared error: 135.36276047376148
Weighted TruePositiveRate: 0.5288461538461539
Weighted MatthewsCorrelation: 0.06748249899619002
Weighted FMeasure: 0.5264447580961342
Iteration time: 11.0
Weighted AreaUnderPRC: 0.5293337945792399
Mean absolute error: 0.46730769230769237
Coverage of cases: 56.73076923076923
Instances selection time: 1.0
Test time: 0.0
Accumulative iteration time: 24.0
Weighted Recall: 0.5288461538461539
Weighted FalsePositiveRate: 0.46205580134151564
Kappa statistic: 0.06598240469208218
Training time: 10.0
		
Iteration: 5
Labeled set size: 100
Unlabelled set size: 4
	
Mean region size: 51.92307692307692
Incorrectly Classified Instances: 34.61538461538461
Correctly Classified Instances: 65.38461538461539
Weighted Precision: 0.653582216487287
Weighted AreaUnderROC: 0.62152133580705
Root mean squared error: 0.5767422678475064
Relative absolute error: 70.40830546265327
Root relative squared error: 115.34845356950127
Weighted TruePositiveRate: 0.6538461538461539
Weighted MatthewsCorrelation: 0.30277314380982334
Weighted FMeasure: 0.6515107830700034
Iteration time: 14.0
Weighted AreaUnderPRC: 0.5893942562009504
Mean absolute error: 0.3520415273132664
Coverage of cases: 67.3076923076923
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 38.0
Weighted Recall: 0.6538461538461539
Weighted FalsePositiveRate: 0.3551448551448551
Kappa statistic: 0.30070974971983566
Training time: 14.0
		
Time end:Wed Nov 01 09.19.01 EET 2017