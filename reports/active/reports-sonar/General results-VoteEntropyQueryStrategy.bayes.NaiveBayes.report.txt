Wed Nov 01 09.18.53 EET 2017
Dataset: sonar
Test set size: 104
Initial Labelled set size: 20
Initial Unlabelled set size: 84
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Wed Nov 01 09.18.53 EET 2017
		
Iteration: 1
Labeled set size: 20
Unlabelled set size: 84
	
Mean region size: 55.625
Incorrectly Classified Instances: 37.59615384615384
Correctly Classified Instances: 62.40384615384615
Weighted Precision: 0.639223840325456
Weighted AreaUnderROC: 0.6687879568348317
Root mean squared error: 0.5985475929108747
Relative absolute error: 75.66385375436549
Root relative squared error: 119.70951858217492
Weighted TruePositiveRate: 0.6240384615384615
Weighted MatthewsCorrelation: 0.26521079591880226
Weighted FMeasure: 0.6158525251799213
Iteration time: 8.6
Weighted AreaUnderPRC: 0.6586330287572035
Mean absolute error: 0.3783192687718274
Coverage of cases: 67.59615384615384
Instances selection time: 6.8
Test time: 6.8
Accumulative iteration time: 8.6
Weighted Recall: 0.6240384615384615
Weighted FalsePositiveRate: 0.36577547452547454
Kappa statistic: 0.2559860382088997
Training time: 1.8
		
Iteration: 2
Labeled set size: 40
Unlabelled set size: 64
	
Mean region size: 56.34615384615385
Incorrectly Classified Instances: 37.11538461538462
Correctly Classified Instances: 62.884615384615394
Weighted Precision: 0.6723019663449792
Weighted AreaUnderROC: 0.7067427178476285
Root mean squared error: 0.5892313141956567
Relative absolute error: 73.8997125846161
Root relative squared error: 117.84626283913133
Weighted TruePositiveRate: 0.6288461538461539
Weighted MatthewsCorrelation: 0.3048787298371653
Weighted FMeasure: 0.6126258138182326
Iteration time: 5.0
Weighted AreaUnderPRC: 0.6920790523914682
Mean absolute error: 0.3694985629230806
Coverage of cases: 69.61538461538461
Instances selection time: 3.4
Test time: 6.1
Accumulative iteration time: 13.6
Weighted Recall: 0.6288461538461539
Weighted FalsePositiveRate: 0.3480723443223443
Kappa statistic: 0.2743489378485991
Training time: 1.6
		
Iteration: 3
Labeled set size: 60
Unlabelled set size: 44
	
Mean region size: 57.30769230769231
Incorrectly Classified Instances: 36.05769230769231
Correctly Classified Instances: 63.9423076923077
Weighted Precision: 0.6982195468906054
Weighted AreaUnderROC: 0.7299172404975977
Root mean squared error: 0.5770247843216654
Relative absolute error: 72.08783308556033
Root relative squared error: 115.40495686433306
Weighted TruePositiveRate: 0.6394230769230769
Weighted MatthewsCorrelation: 0.34023173734675566
Weighted FMeasure: 0.617840644133637
Iteration time: 4.6
Weighted AreaUnderPRC: 0.713442973486892
Mean absolute error: 0.3604391654278017
Coverage of cases: 71.25
Instances selection time: 3.1
Test time: 6.2
Accumulative iteration time: 18.2
Weighted Recall: 0.6394230769230769
Weighted FalsePositiveRate: 0.33446945911231624
Kappa statistic: 0.2975837419214942
Training time: 1.5
		
Iteration: 4
Labeled set size: 80
Unlabelled set size: 24
	
Mean region size: 59.27884615384615
Incorrectly Classified Instances: 31.05769230769231
Correctly Classified Instances: 68.9423076923077
Weighted Precision: 0.7157397043864725
Weighted AreaUnderROC: 0.8066307910839161
Root mean squared error: 0.5245730217896393
Relative absolute error: 61.908815049128165
Root relative squared error: 104.91460435792783
Weighted TruePositiveRate: 0.6894230769230769
Weighted MatthewsCorrelation: 0.4072316877503144
Weighted FMeasure: 0.6833511599818831
Iteration time: 3.2
Weighted AreaUnderPRC: 0.8041997819509596
Mean absolute error: 0.3095440752456408
Coverage of cases: 78.36538461538461
Instances selection time: 1.6
Test time: 6.0
Accumulative iteration time: 21.4
Weighted Recall: 0.6894230769230769
Weighted FalsePositiveRate: 0.2944393106893107
Kappa statistic: 0.38785505328399994
Training time: 1.6
		
Iteration: 5
Labeled set size: 100
Unlabelled set size: 4
	
Mean region size: 58.5576923076923
Incorrectly Classified Instances: 30.769230769230763
Correctly Classified Instances: 69.23076923076923
Weighted Precision: 0.7177648159771594
Weighted AreaUnderROC: 0.7998139769456734
Root mean squared error: 0.5269247973272941
Relative absolute error: 61.72414020263284
Root relative squared error: 105.38495946545882
Weighted TruePositiveRate: 0.6923076923076923
Weighted MatthewsCorrelation: 0.41244911653533506
Weighted FMeasure: 0.6867357643424117
Iteration time: 2.3
Weighted AreaUnderPRC: 0.7999470382185717
Mean absolute error: 0.3086207010131642
Coverage of cases: 77.11538461538461
Instances selection time: 0.1
Test time: 6.1
Accumulative iteration time: 23.7
Weighted Recall: 0.6923076923076923
Weighted FalsePositiveRate: 0.291541613148756
Kappa statistic: 0.393766174873339
Training time: 2.2
		
Time end:Wed Nov 01 09.18.55 EET 2017