Wed Nov 01 09.18.25 EET 2017
Dataset: sonar
Test set size: 104
Initial Labelled set size: 20
Initial Unlabelled set size: 84
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Wed Nov 01 09.18.25 EET 2017
		
Iteration: 1
Labeled set size: 20
Unlabelled set size: 84
	
Mean region size: 98.5576923076923
Incorrectly Classified Instances: 39.42307692307692
Correctly Classified Instances: 60.57692307692308
Weighted Precision: 0.6185380479735318
Weighted AreaUnderROC: 0.7215213358070501
Root mean squared error: 0.46253897964637275
Relative absolute error: 82.88461538461539
Root relative squared error: 92.50779592927455
Weighted TruePositiveRate: 0.6057692307692307
Weighted MatthewsCorrelation: 0.22724619311661431
Weighted FMeasure: 0.6024372905816204
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7046367321467779
Mean absolute error: 0.41442307692307695
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 2.0
Weighted Recall: 0.6057692307692307
Weighted FalsePositiveRate: 0.3823926073926074
Kappa statistic: 0.2201901975128017
Training time: 2.0
		
Iteration: 2
Labeled set size: 40
Unlabelled set size: 64
	
Mean region size: 94.23076923076923
Incorrectly Classified Instances: 34.61538461538461
Correctly Classified Instances: 65.38461538461539
Weighted Precision: 0.7040178116183419
Weighted AreaUnderROC: 0.7369202226345084
Root mean squared error: 0.47281887896708635
Relative absolute error: 80.96153846153841
Root relative squared error: 94.56377579341726
Weighted TruePositiveRate: 0.6538461538461539
Weighted MatthewsCorrelation: 0.3624110021235896
Weighted FMeasure: 0.639593536467345
Iteration time: 6.0
Weighted AreaUnderPRC: 0.7046757248849852
Mean absolute error: 0.40480769230769204
Coverage of cases: 98.07692307692308
Instances selection time: 2.0
Test time: 1.0
Accumulative iteration time: 8.0
Weighted Recall: 0.6538461538461539
Weighted FalsePositiveRate: 0.321749678892536
Kappa statistic: 0.3234550054210336
Training time: 4.0
		
Iteration: 3
Labeled set size: 60
Unlabelled set size: 44
	
Mean region size: 93.75
Incorrectly Classified Instances: 26.923076923076923
Correctly Classified Instances: 73.07692307692308
Weighted Precision: 0.742503259452412
Weighted AreaUnderROC: 0.8137291280148423
Root mean squared error: 0.417985093587724
Relative absolute error: 70.1923076923077
Root relative squared error: 83.5970187175448
Weighted TruePositiveRate: 0.7307692307692307
Weighted MatthewsCorrelation: 0.47440578321008225
Weighted FMeasure: 0.7297720797720798
Iteration time: 7.0
Weighted AreaUnderPRC: 0.7950488228257033
Mean absolute error: 0.35096153846153844
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 15.0
Weighted Recall: 0.7307692307692307
Weighted FalsePositiveRate: 0.2598972456115313
Kappa statistic: 0.46568807339449536
Training time: 7.0
		
Iteration: 4
Labeled set size: 80
Unlabelled set size: 24
	
Mean region size: 96.63461538461539
Incorrectly Classified Instances: 27.884615384615383
Correctly Classified Instances: 72.11538461538461
Weighted Precision: 0.7306899434897937
Weighted AreaUnderROC: 0.8384044526901668
Root mean squared error: 0.4078555551247938
Relative absolute error: 70.38461538461539
Root relative squared error: 81.57111102495877
Weighted TruePositiveRate: 0.7211538461538461
Weighted MatthewsCorrelation: 0.45273741764056363
Weighted FMeasure: 0.7204571823106107
Iteration time: 10.0
Weighted AreaUnderPRC: 0.8175478090900934
Mean absolute error: 0.35192307692307695
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 0.0
Accumulative iteration time: 25.0
Weighted Recall: 0.7211538461538461
Weighted FalsePositiveRate: 0.27069002426145283
Kappa statistic: 0.44599559147685525
Training time: 9.0
		
Iteration: 5
Labeled set size: 100
Unlabelled set size: 4
	
Mean region size: 97.59615384615384
Incorrectly Classified Instances: 22.115384615384617
Correctly Classified Instances: 77.88461538461539
Weighted Precision: 0.7895499365701765
Weighted AreaUnderROC: 0.8840445269016697
Root mean squared error: 0.38192327058636333
Relative absolute error: 65.57692307692311
Root relative squared error: 76.38465411727266
Weighted TruePositiveRate: 0.7788461538461539
Weighted MatthewsCorrelation: 0.5690916798348435
Weighted FMeasure: 0.7782936273497947
Iteration time: 13.0
Weighted AreaUnderPRC: 0.8663171470277446
Mean absolute error: 0.32788461538461555
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 38.0
Weighted Recall: 0.7788461538461539
Weighted FalsePositiveRate: 0.2126123876123876
Kappa statistic: 0.5606171932402646
Training time: 13.0
		
Time end:Wed Nov 01 09.18.25 EET 2017