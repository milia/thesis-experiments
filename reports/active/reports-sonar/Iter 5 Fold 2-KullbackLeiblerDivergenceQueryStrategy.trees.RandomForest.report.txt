Wed Nov 01 09.18.12 EET 2017
Dataset: sonar
Test set size: 104
Initial Labelled set size: 20
Initial Unlabelled set size: 84
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Wed Nov 01 09.18.12 EET 2017
		
Iteration: 1
Labeled set size: 20
Unlabelled set size: 84
	
Mean region size: 98.5576923076923
Incorrectly Classified Instances: 41.34615384615385
Correctly Classified Instances: 58.65384615384615
Weighted Precision: 0.7143793706293706
Weighted AreaUnderROC: 0.7532467532467534
Root mean squared error: 0.4759605672095762
Relative absolute error: 86.15384615384615
Root relative squared error: 95.19211344191524
Weighted TruePositiveRate: 0.5865384615384616
Weighted MatthewsCorrelation: 0.29569349549626406
Weighted FMeasure: 0.5318324884737962
Iteration time: 10.0
Weighted AreaUnderPRC: 0.726351124173618
Mean absolute error: 0.43076923076923074
Coverage of cases: 100.0
Instances selection time: 4.0
Test time: 1.0
Accumulative iteration time: 10.0
Weighted Recall: 0.5865384615384616
Weighted FalsePositiveRate: 0.3728093335236193
Kappa statistic: 0.20483641536273126
Training time: 6.0
		
Iteration: 2
Labeled set size: 40
Unlabelled set size: 64
	
Mean region size: 95.67307692307692
Incorrectly Classified Instances: 34.61538461538461
Correctly Classified Instances: 65.38461538461539
Weighted Precision: 0.6558185404339251
Weighted AreaUnderROC: 0.6905380333951764
Root mean squared error: 0.4831785782312151
Relative absolute error: 84.2307692307692
Root relative squared error: 96.63571564624301
Weighted TruePositiveRate: 0.6538461538461539
Weighted MatthewsCorrelation: 0.30339248609421626
Weighted FMeasure: 0.6486013986013988
Iteration time: 9.0
Weighted AreaUnderPRC: 0.6573534408493984
Mean absolute error: 0.421153846153846
Coverage of cases: 97.11538461538461
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 19.0
Weighted Recall: 0.6538461538461539
Weighted FalsePositiveRate: 0.3595975453118311
Kappa statistic: 0.29756097560975614
Training time: 8.0
		
Iteration: 3
Labeled set size: 60
Unlabelled set size: 44
	
Mean region size: 93.75
Incorrectly Classified Instances: 39.42307692307692
Correctly Classified Instances: 60.57692307692308
Weighted Precision: 0.6308097390414462
Weighted AreaUnderROC: 0.7408163265306122
Root mean squared error: 0.48516452403758387
Relative absolute error: 80.0
Root relative squared error: 97.03290480751677
Weighted TruePositiveRate: 0.6057692307692307
Weighted MatthewsCorrelation: 0.21859961205255626
Weighted FMeasure: 0.5696574167068668
Iteration time: 7.0
Weighted AreaUnderPRC: 0.7124357627082966
Mean absolute error: 0.4
Coverage of cases: 98.07692307692308
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 26.0
Weighted Recall: 0.6057692307692307
Weighted FalsePositiveRate: 0.4269195090623662
Kappa statistic: 0.1843917368018361
Training time: 6.0
		
Iteration: 4
Labeled set size: 80
Unlabelled set size: 24
	
Mean region size: 93.75
Incorrectly Classified Instances: 23.076923076923077
Correctly Classified Instances: 76.92307692307692
Weighted Precision: 0.777120315581854
Weighted AreaUnderROC: 0.8337662337662337
Root mean squared error: 0.4187894642712676
Relative absolute error: 68.84615384615383
Root relative squared error: 83.75789285425353
Weighted TruePositiveRate: 0.7692307692307693
Weighted MatthewsCorrelation: 0.5421275571191733
Weighted FMeasure: 0.7657342657342656
Iteration time: 9.0
Weighted AreaUnderPRC: 0.8071260374081708
Mean absolute error: 0.3442307692307691
Coverage of cases: 99.03846153846153
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 35.0
Weighted Recall: 0.7692307692307693
Weighted FalsePositiveRate: 0.24344227201370056
Kappa statistic: 0.5317073170731709
Training time: 8.0
		
Iteration: 5
Labeled set size: 100
Unlabelled set size: 4
	
Mean region size: 97.11538461538461
Incorrectly Classified Instances: 22.115384615384617
Correctly Classified Instances: 77.88461538461539
Weighted Precision: 0.7787030677655677
Weighted AreaUnderROC: 0.8538033395176252
Root mean squared error: 0.397709789825542
Relative absolute error: 67.88461538461542
Root relative squared error: 79.5419579651084
Weighted TruePositiveRate: 0.7788461538461539
Weighted MatthewsCorrelation: 0.5558243599778628
Weighted FMeasure: 0.7787023740631986
Iteration time: 20.0
Weighted AreaUnderPRC: 0.8336226183373884
Mean absolute error: 0.3394230769230771
Coverage of cases: 100.0
Instances selection time: 4.0
Test time: 1.0
Accumulative iteration time: 55.0
Weighted Recall: 0.7788461538461539
Weighted FalsePositiveRate: 0.22374411302982733
Kappa statistic: 0.5557206537890045
Training time: 16.0
		
Time end:Wed Nov 01 09.18.12 EET 2017