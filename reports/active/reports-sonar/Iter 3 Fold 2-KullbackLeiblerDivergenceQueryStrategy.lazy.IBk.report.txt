Wed Nov 01 09.18.05 EET 2017
Dataset: sonar
Test set size: 104
Initial Labelled set size: 20
Initial Unlabelled set size: 84
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Wed Nov 01 09.18.05 EET 2017
		
Iteration: 1
Labeled set size: 20
Unlabelled set size: 84
	
Mean region size: 50.0
Incorrectly Classified Instances: 41.34615384615385
Correctly Classified Instances: 58.65384615384615
Weighted Precision: 0.5904576436222005
Weighted AreaUnderROC: 0.5669642857142857
Root mean squared error: 0.6147684454952161
Relative absolute error: 84.26573426573434
Root relative squared error: 122.95368909904323
Weighted TruePositiveRate: 0.5865384615384616
Weighted MatthewsCorrelation: 0.15624434890323327
Weighted FMeasure: 0.5566249073098388
Iteration time: 10.0
Weighted AreaUnderPRC: 0.5423095648266048
Mean absolute error: 0.4213286713286717
Coverage of cases: 58.65384615384615
Instances selection time: 8.0
Test time: 5.0
Accumulative iteration time: 10.0
Weighted Recall: 0.5865384615384616
Weighted FalsePositiveRate: 0.4526098901098901
Kappa statistic: 0.13867488443759643
Training time: 2.0
		
Iteration: 2
Labeled set size: 40
Unlabelled set size: 64
	
Mean region size: 50.0
Incorrectly Classified Instances: 33.65384615384615
Correctly Classified Instances: 66.34615384615384
Weighted Precision: 0.6674635020916929
Weighted AreaUnderROC: 0.6651785714285714
Root mean squared error: 0.5666389626003993
Relative absolute error: 68.8644688644689
Root relative squared error: 113.32779252007985
Weighted TruePositiveRate: 0.6634615384615384
Weighted MatthewsCorrelation: 0.32943922737683207
Weighted FMeasure: 0.6639286502338227
Iteration time: 17.0
Weighted AreaUnderPRC: 0.61203117509145
Mean absolute error: 0.34432234432234454
Coverage of cases: 66.34615384615384
Instances selection time: 16.0
Test time: 9.0
Accumulative iteration time: 27.0
Weighted Recall: 0.6634615384615384
Weighted FalsePositiveRate: 0.3331043956043956
Kappa statistic: 0.3279172821270309
Training time: 1.0
		
Iteration: 3
Labeled set size: 60
Unlabelled set size: 44
	
Mean region size: 50.0
Incorrectly Classified Instances: 38.46153846153846
Correctly Classified Instances: 61.53846153846154
Weighted Precision: 0.6323407775020679
Weighted AreaUnderROC: 0.6235119047619049
Root mean squared error: 0.6103020419482038
Relative absolute error: 77.66749379652605
Root relative squared error: 122.06040838964076
Weighted TruePositiveRate: 0.6153846153846154
Weighted MatthewsCorrelation: 0.25097644145275744
Weighted FMeasure: 0.6123876123876124
Iteration time: 11.0
Weighted AreaUnderPRC: 0.5800032267153855
Mean absolute error: 0.3883374689826302
Coverage of cases: 61.53846153846154
Instances selection time: 11.0
Test time: 12.0
Accumulative iteration time: 38.0
Weighted Recall: 0.6153846153846154
Weighted FalsePositiveRate: 0.3683608058608059
Kappa statistic: 0.24198250728862977
Training time: 0.0
		
Iteration: 4
Labeled set size: 80
Unlabelled set size: 24
	
Mean region size: 50.0
Incorrectly Classified Instances: 28.846153846153847
Correctly Classified Instances: 71.15384615384616
Weighted Precision: 0.7109906585168952
Weighted AreaUnderROC: 0.7083333333333334
Root mean squared error: 0.5306360449323001
Relative absolute error: 58.72420262664162
Root relative squared error: 106.12720898646002
Weighted TruePositiveRate: 0.7115384615384616
Weighted MatthewsCorrelation: 0.4182254699658259
Weighted FMeasure: 0.7110000861400637
Iteration time: 20.0
Weighted AreaUnderPRC: 0.6499782653052173
Mean absolute error: 0.2936210131332081
Coverage of cases: 71.15384615384616
Instances selection time: 16.0
Test time: 15.0
Accumulative iteration time: 58.0
Weighted Recall: 0.7115384615384616
Weighted FalsePositiveRate: 0.2948717948717948
Kappa statistic: 0.4179104477611941
Training time: 4.0
		
Iteration: 5
Labeled set size: 100
Unlabelled set size: 4
	
Mean region size: 50.0
Incorrectly Classified Instances: 23.076923076923077
Correctly Classified Instances: 76.92307692307692
Weighted Precision: 0.7730769230769231
Weighted AreaUnderROC: 0.761904761904762
Root mean squared error: 0.47575252050780137
Relative absolute error: 47.2096530920062
Root relative squared error: 95.15050410156027
Weighted TruePositiveRate: 0.7692307692307693
Weighted MatthewsCorrelation: 0.5367450401216932
Weighted FMeasure: 0.7664335664335664
Iteration time: 3.0
Weighted AreaUnderPRC: 0.7047337278106508
Mean absolute error: 0.236048265460031
Coverage of cases: 76.92307692307692
Instances selection time: 3.0
Test time: 18.0
Accumulative iteration time: 61.0
Weighted Recall: 0.7692307692307693
Weighted FalsePositiveRate: 0.2454212454212454
Kappa statistic: 0.5301204819277109
Training time: 0.0
		
Time end:Wed Nov 01 09.18.05 EET 2017