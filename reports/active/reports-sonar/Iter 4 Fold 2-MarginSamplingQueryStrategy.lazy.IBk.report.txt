Wed Nov 01 09.18.32 EET 2017
Dataset: sonar
Test set size: 104
Initial Labelled set size: 20
Initial Unlabelled set size: 84
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Wed Nov 01 09.18.32 EET 2017
		
Iteration: 1
Labeled set size: 20
Unlabelled set size: 84
	
Mean region size: 50.0
Incorrectly Classified Instances: 35.57692307692308
Correctly Classified Instances: 64.42307692307692
Weighted Precision: 0.6462904465124221
Weighted AreaUnderROC: 0.644345238095238
Root mean squared error: 0.5705196658565357
Relative absolute error: 73.77622377622382
Root relative squared error: 114.10393317130713
Weighted TruePositiveRate: 0.6442307692307693
Weighted MatthewsCorrelation: 0.2878883338338082
Weighted FMeasure: 0.6447253052757641
Iteration time: 9.0
Weighted AreaUnderPRC: 0.5953014621054405
Mean absolute error: 0.3688811188811191
Coverage of cases: 64.42307692307692
Instances selection time: 9.0
Test time: 8.0
Accumulative iteration time: 9.0
Weighted Recall: 0.6442307692307693
Weighted FalsePositiveRate: 0.3555402930402931
Kappa statistic: 0.28740740740740744
Training time: 0.0
		
Iteration: 2
Labeled set size: 40
Unlabelled set size: 64
	
Mean region size: 50.0
Incorrectly Classified Instances: 31.73076923076923
Correctly Classified Instances: 68.26923076923077
Weighted Precision: 0.705976949879389
Weighted AreaUnderROC: 0.6919642857142858
Root mean squared error: 0.5502405797665585
Relative absolute error: 65.20146520146521
Root relative squared error: 110.0481159533117
Weighted TruePositiveRate: 0.6826923076923077
Weighted MatthewsCorrelation: 0.3916542665960507
Weighted FMeasure: 0.6795979167113188
Iteration time: 10.0
Weighted AreaUnderPRC: 0.6364941343834403
Mean absolute error: 0.32600732600732607
Coverage of cases: 68.26923076923077
Instances selection time: 10.0
Test time: 16.0
Accumulative iteration time: 19.0
Weighted Recall: 0.6826923076923077
Weighted FalsePositiveRate: 0.29876373626373626
Kappa statistic: 0.3755458515283843
Training time: 0.0
		
Iteration: 3
Labeled set size: 60
Unlabelled set size: 44
	
Mean region size: 50.0
Incorrectly Classified Instances: 25.96153846153846
Correctly Classified Instances: 74.03846153846153
Weighted Precision: 0.8048524245707345
Weighted AreaUnderROC: 0.7559523809523809
Root mean squared error: 0.5014985946846349
Relative absolute error: 53.47394540942928
Root relative squared error: 100.29971893692698
Weighted TruePositiveRate: 0.7403846153846154
Weighted MatthewsCorrelation: 0.5482992570485232
Weighted FMeasure: 0.7319276889666843
Iteration time: 10.0
Weighted AreaUnderPRC: 0.7048908116080381
Mean absolute error: 0.2673697270471464
Coverage of cases: 74.03846153846153
Instances selection time: 10.0
Test time: 23.0
Accumulative iteration time: 29.0
Weighted Recall: 0.7403846153846154
Weighted FalsePositiveRate: 0.2284798534798535
Kappa statistic: 0.49496402877697854
Training time: 0.0
		
Iteration: 4
Labeled set size: 80
Unlabelled set size: 24
	
Mean region size: 50.0
Incorrectly Classified Instances: 12.5
Correctly Classified Instances: 87.5
Weighted Precision: 0.8749533407988056
Weighted AreaUnderROC: 0.8735119047619049
Root mean squared error: 0.3494280190705954
Relative absolute error: 26.82926829268287
Root relative squared error: 69.88560381411908
Weighted TruePositiveRate: 0.875
Weighted MatthewsCorrelation: 0.7482775566596936
Weighted FMeasure: 0.8748952026082907
Iteration time: 8.0
Weighted AreaUnderPRC: 0.8277610099149423
Mean absolute error: 0.13414634146341434
Coverage of cases: 87.5
Instances selection time: 8.0
Test time: 29.0
Accumulative iteration time: 37.0
Weighted Recall: 0.875
Weighted FalsePositiveRate: 0.1279761904761905
Kappa statistic: 0.7481371087928465
Training time: 0.0
		
Iteration: 5
Labeled set size: 100
Unlabelled set size: 4
	
Mean region size: 50.0
Incorrectly Classified Instances: 14.423076923076923
Correctly Classified Instances: 85.57692307692308
Weighted Precision: 0.8580603536760609
Weighted AreaUnderROC: 0.8511904761904762
Root mean squared error: 0.3761633047098704
Relative absolute error: 30.24132730015082
Root relative squared error: 75.23266094197409
Weighted TruePositiveRate: 0.8557692307692307
Weighted MatthewsCorrelation: 0.7110304706499
Weighted FMeasure: 0.8548887010425472
Iteration time: 5.0
Weighted AreaUnderPRC: 0.8031579992194673
Mean absolute error: 0.1512066365007541
Coverage of cases: 85.57692307692308
Instances selection time: 5.0
Test time: 32.0
Accumulative iteration time: 42.0
Weighted Recall: 0.8557692307692307
Weighted FalsePositiveRate: 0.1533882783882784
Kappa statistic: 0.7076461769115442
Training time: 0.0
		
Time end:Wed Nov 01 09.18.33 EET 2017