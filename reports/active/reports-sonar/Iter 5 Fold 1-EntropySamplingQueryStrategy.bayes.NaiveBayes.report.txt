Wed Nov 01 09.17.41 EET 2017
Dataset: sonar
Test set size: 104
Initial Labelled set size: 20
Initial Unlabelled set size: 84
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Nov 01 09.17.41 EET 2017
		
Iteration: 1
Labeled set size: 20
Unlabelled set size: 84
	
Mean region size: 53.84615384615385
Incorrectly Classified Instances: 52.88461538461539
Correctly Classified Instances: 47.11538461538461
Weighted Precision: 0.49176266567570914
Weighted AreaUnderROC: 0.48491872710622713
Root mean squared error: 0.7147984146238692
Relative absolute error: 104.17653138369565
Root relative squared error: 142.95968292477383
Weighted TruePositiveRate: 0.47115384615384615
Weighted MatthewsCorrelation: -0.017874707002438525
Weighted FMeasure: 0.42834174950748394
Iteration time: 14.0
Weighted AreaUnderPRC: 0.5132221275374248
Mean absolute error: 0.5208826569184782
Coverage of cases: 52.88461538461539
Instances selection time: 13.0
Test time: 11.0
Accumulative iteration time: 14.0
Weighted Recall: 0.47115384615384615
Weighted FalsePositiveRate: 0.4860347985347986
Kappa statistic: -0.014184397163120532
Training time: 1.0
		
Iteration: 2
Labeled set size: 40
Unlabelled set size: 64
	
Mean region size: 55.28846153846154
Incorrectly Classified Instances: 45.19230769230769
Correctly Classified Instances: 54.80769230769231
Weighted Precision: 0.5725752508361205
Weighted AreaUnderROC: 0.4994848901098901
Root mean squared error: 0.664378689600308
Relative absolute error: 91.07137580769712
Root relative squared error: 132.8757379200616
Weighted TruePositiveRate: 0.5480769230769231
Weighted MatthewsCorrelation: 0.12873618766298695
Weighted FMeasure: 0.5364891518737672
Iteration time: 8.0
Weighted AreaUnderPRC: 0.5525819274374114
Mean absolute error: 0.4553568790384856
Coverage of cases: 59.61538461538461
Instances selection time: 7.0
Test time: 15.0
Accumulative iteration time: 22.0
Weighted Recall: 0.5480769230769231
Weighted FalsePositiveRate: 0.4260531135531135
Kappa statistic: 0.11832611832611839
Training time: 1.0
		
Iteration: 3
Labeled set size: 60
Unlabelled set size: 44
	
Mean region size: 60.09615384615385
Incorrectly Classified Instances: 38.46153846153846
Correctly Classified Instances: 61.53846153846154
Weighted Precision: 0.6365384615384614
Weighted AreaUnderROC: 0.5959821428571429
Root mean squared error: 0.5991308672021401
Relative absolute error: 78.03882517842027
Root relative squared error: 119.82617344042804
Weighted TruePositiveRate: 0.6153846153846154
Weighted MatthewsCorrelation: 0.25617376914899
Weighted FMeasure: 0.6108058608058609
Iteration time: 7.0
Weighted AreaUnderPRC: 0.6010790794482074
Mean absolute error: 0.39019412589210134
Coverage of cases: 71.15384615384616
Instances selection time: 5.0
Test time: 15.0
Accumulative iteration time: 29.0
Weighted Recall: 0.6153846153846154
Weighted FalsePositiveRate: 0.36538461538461536
Kappa statistic: 0.24418604651162798
Training time: 2.0
		
Iteration: 4
Labeled set size: 80
Unlabelled set size: 24
	
Mean region size: 56.73076923076923
Incorrectly Classified Instances: 28.846153846153847
Correctly Classified Instances: 71.15384615384616
Weighted Precision: 0.7283216783216784
Weighted AreaUnderROC: 0.7512591575091575
Root mean squared error: 0.5184283284485135
Relative absolute error: 58.04073242406007
Root relative squared error: 103.6856656897027
Weighted TruePositiveRate: 0.7115384615384616
Weighted MatthewsCorrelation: 0.44145935672082387
Weighted FMeasure: 0.7102564102564103
Iteration time: 5.0
Weighted AreaUnderPRC: 0.7263809180198169
Mean absolute error: 0.29020366212030035
Coverage of cases: 78.84615384615384
Instances selection time: 3.0
Test time: 12.0
Accumulative iteration time: 34.0
Weighted Recall: 0.7115384615384616
Weighted FalsePositiveRate: 0.27403846153846156
Kappa statistic: 0.4298245614035087
Training time: 2.0
		
Iteration: 5
Labeled set size: 100
Unlabelled set size: 4
	
Mean region size: 58.17307692307692
Incorrectly Classified Instances: 28.846153846153847
Correctly Classified Instances: 71.15384615384616
Weighted Precision: 0.7200091575091575
Weighted AreaUnderROC: 0.795630151098901
Root mean squared error: 0.5064088027840916
Relative absolute error: 57.00768691772331
Root relative squared error: 101.28176055681833
Weighted TruePositiveRate: 0.7115384615384616
Weighted MatthewsCorrelation: 0.431547619047619
Weighted FMeasure: 0.7115384615384616
Iteration time: 7.0
Weighted AreaUnderPRC: 0.7728618385144237
Mean absolute error: 0.28503843458861655
Coverage of cases: 78.84615384615384
Instances selection time: 5.0
Test time: 11.0
Accumulative iteration time: 41.0
Weighted Recall: 0.7115384615384616
Weighted FalsePositiveRate: 0.2799908424908425
Kappa statistic: 0.4264705882352942
Training time: 2.0
		
Time end:Wed Nov 01 09.17.41 EET 2017