Wed Nov 01 09.18.23 EET 2017
Dataset: sonar
Test set size: 104
Initial Labelled set size: 20
Initial Unlabelled set size: 84
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Wed Nov 01 09.18.23 EET 2017
		
Iteration: 1
Labeled set size: 20
Unlabelled set size: 84
	
Mean region size: 98.50961538461537
Incorrectly Classified Instances: 36.44230769230769
Correctly Classified Instances: 63.55769230769231
Weighted Precision: 0.6637719076728448
Weighted AreaUnderROC: 0.7096778370439085
Root mean squared error: 0.4719975422196881
Relative absolute error: 84.11538461538461
Root relative squared error: 94.39950844393763
Weighted TruePositiveRate: 0.635576923076923
Weighted MatthewsCorrelation: 0.29923179673171874
Weighted FMeasure: 0.621839988442172
Iteration time: 5.6
Weighted AreaUnderPRC: 0.6898449549337579
Mean absolute error: 0.42057692307692307
Coverage of cases: 99.71153846153847
Instances selection time: 1.6
Test time: 1.9
Accumulative iteration time: 5.6
Weighted Recall: 0.635576923076923
Weighted FalsePositiveRate: 0.3538430022358594
Kappa statistic: 0.2791654537369822
Training time: 4.0
		
Iteration: 2
Labeled set size: 40
Unlabelled set size: 64
	
Mean region size: 97.21153846153847
Incorrectly Classified Instances: 31.153846153846153
Correctly Classified Instances: 68.84615384615384
Weighted Precision: 0.723270897116401
Weighted AreaUnderROC: 0.7823463106833644
Root mean squared error: 0.4461114553775526
Relative absolute error: 78.63461538461539
Root relative squared error: 89.22229107551054
Weighted TruePositiveRate: 0.6884615384615385
Weighted MatthewsCorrelation: 0.41188798556565154
Weighted FMeasure: 0.6775268141710766
Iteration time: 7.2
Weighted AreaUnderPRC: 0.7547689935144861
Mean absolute error: 0.3931730769230769
Coverage of cases: 99.51923076923077
Instances selection time: 1.5
Test time: 1.7
Accumulative iteration time: 12.8
Weighted Recall: 0.6884615384615385
Weighted FalsePositiveRate: 0.2973445007373579
Kappa statistic: 0.38587475288430245
Training time: 5.7
		
Iteration: 3
Labeled set size: 60
Unlabelled set size: 44
	
Mean region size: 97.40384615384615
Incorrectly Classified Instances: 28.55769230769231
Correctly Classified Instances: 71.4423076923077
Weighted Precision: 0.730171580620608
Weighted AreaUnderROC: 0.8080638334879406
Root mean squared error: 0.42403124414649945
Relative absolute error: 73.1346153846154
Root relative squared error: 84.8062488292999
Weighted TruePositiveRate: 0.714423076923077
Weighted MatthewsCorrelation: 0.44554489889231813
Weighted FMeasure: 0.7119107753335151
Iteration time: 9.5
Weighted AreaUnderPRC: 0.7890635739246039
Mean absolute error: 0.365673076923077
Coverage of cases: 100.0
Instances selection time: 1.6
Test time: 0.9
Accumulative iteration time: 22.3
Weighted Recall: 0.714423076923077
Weighted FalsePositiveRate: 0.275203070738785
Kappa statistic: 0.43390884050353684
Training time: 7.9
		
Iteration: 4
Labeled set size: 80
Unlabelled set size: 24
	
Mean region size: 96.68269230769229
Incorrectly Classified Instances: 24.80769230769231
Correctly Classified Instances: 75.1923076923077
Weighted Precision: 0.7616233434894308
Weighted AreaUnderROC: 0.8393112728045764
Root mean squared error: 0.4060061571328837
Relative absolute error: 69.05769230769229
Root relative squared error: 81.20123142657674
Weighted TruePositiveRate: 0.7519230769230768
Weighted MatthewsCorrelation: 0.512396947862983
Weighted FMeasure: 0.750589012943508
Iteration time: 12.4
Weighted AreaUnderPRC: 0.8193285971433493
Mean absolute error: 0.3452884615384616
Coverage of cases: 99.51923076923077
Instances selection time: 1.8
Test time: 1.6
Accumulative iteration time: 34.7
Weighted Recall: 0.7519230769230768
Weighted FalsePositiveRate: 0.2449920317777461
Kappa statistic: 0.5040505948858796
Training time: 10.6
		
Iteration: 5
Labeled set size: 100
Unlabelled set size: 4
	
Mean region size: 95.4326923076923
Incorrectly Classified Instances: 22.980769230769234
Correctly Classified Instances: 77.01923076923079
Weighted Precision: 0.7762431529061888
Weighted AreaUnderROC: 0.8584018920068027
Root mean squared error: 0.39332604002828353
Relative absolute error: 65.57692307692307
Root relative squared error: 78.66520800565672
Weighted TruePositiveRate: 0.7701923076923076
Weighted MatthewsCorrelation: 0.5454566388096433
Weighted FMeasure: 0.7695698347172855
Iteration time: 17.0
Weighted AreaUnderPRC: 0.8411999005357081
Mean absolute error: 0.32788461538461544
Coverage of cases: 99.8076923076923
Instances selection time: 1.2
Test time: 2.1
Accumulative iteration time: 51.7
Weighted Recall: 0.7701923076923076
Weighted FalsePositiveRate: 0.22744493601636456
Kappa statistic: 0.5406539791068887
Training time: 15.8
		
Time end:Wed Nov 01 09.18.26 EET 2017