Tue Oct 31 13.17.48 EET 2017
Dataset: ionosphere
Test set size: 176
Initial Labelled set size: 35
Initial Unlabelled set size: 140
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Tue Oct 31 13.17.48 EET 2017
		
Iteration: 1
Labeled set size: 35
Unlabelled set size: 140
	
Mean region size: 73.86363636363636
Incorrectly Classified Instances: 14.772727272727273
Correctly Classified Instances: 85.22727272727273
Weighted Precision: 0.8683973885193397
Weighted AreaUnderROC: 0.9454277286135693
Root mean squared error: 0.34861739903379874
Relative absolute error: 41.25
Root relative squared error: 69.72347980675974
Weighted TruePositiveRate: 0.8522727272727273
Weighted MatthewsCorrelation: 0.6819882916338393
Weighted FMeasure: 0.843200146627566
Iteration time: 13.0
Weighted AreaUnderPRC: 0.9364093672824779
Mean absolute error: 0.20625
Coverage of cases: 98.86363636363636
Instances selection time: 4.0
Test time: 1.0
Accumulative iteration time: 13.0
Weighted Recall: 0.8522727272727273
Weighted FalsePositiveRate: 0.250924223269356
Kappa statistic: 0.6516973664180241
Training time: 9.0
		
Iteration: 2
Labeled set size: 55
Unlabelled set size: 120
	
Mean region size: 75.56818181818181
Incorrectly Classified Instances: 11.931818181818182
Correctly Classified Instances: 88.06818181818181
Weighted Precision: 0.8873401988636364
Weighted AreaUnderROC: 0.9138924006180642
Root mean squared error: 0.31550824798324134
Relative absolute error: 35.68181818181817
Root relative squared error: 63.101649596648265
Weighted TruePositiveRate: 0.8806818181818182
Weighted MatthewsCorrelation: 0.7402975449912698
Weighted FMeasure: 0.8763330002956559
Iteration time: 12.0
Weighted AreaUnderPRC: 0.906788201539742
Mean absolute error: 0.17840909090909085
Coverage of cases: 97.1590909090909
Instances selection time: 5.0
Test time: 1.0
Accumulative iteration time: 25.0
Weighted Recall: 0.8806818181818182
Weighted FalsePositiveRate: 0.19294477646247554
Kappa statistic: 0.7259786476868328
Training time: 7.0
		
Iteration: 3
Labeled set size: 75
Unlabelled set size: 100
	
Mean region size: 79.26136363636364
Incorrectly Classified Instances: 10.227272727272727
Correctly Classified Instances: 89.77272727272727
Weighted Precision: 0.8984785875281743
Weighted AreaUnderROC: 0.9146649810366625
Root mean squared error: 0.31891150097565535
Relative absolute error: 39.545454545454525
Root relative squared error: 63.78230019513107
Weighted TruePositiveRate: 0.8977272727272727
Weighted MatthewsCorrelation: 0.7750877934058509
Weighted FMeasure: 0.8960086523645846
Iteration time: 12.0
Weighted AreaUnderPRC: 0.9089995456817324
Mean absolute error: 0.19772727272727264
Coverage of cases: 97.72727272727273
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 37.0
Weighted Recall: 0.8977272727272727
Weighted FalsePositiveRate: 0.14832426668709855
Kappa statistic: 0.7710651828298887
Training time: 10.0
		
Iteration: 4
Labeled set size: 95
Unlabelled set size: 80
	
Mean region size: 73.01136363636364
Incorrectly Classified Instances: 10.227272727272727
Correctly Classified Instances: 89.77272727272727
Weighted Precision: 0.9018983957219252
Weighted AreaUnderROC: 0.9457789015311139
Root mean squared error: 0.2897883240009382
Relative absolute error: 31.136363636363644
Root relative squared error: 57.957664800187636
Weighted TruePositiveRate: 0.8977272727272727
Weighted MatthewsCorrelation: 0.7770824396860777
Weighted FMeasure: 0.8949228016565477
Iteration time: 9.0
Weighted AreaUnderPRC: 0.938377976848615
Mean absolute error: 0.15568181818181823
Coverage of cases: 98.29545454545455
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 46.0
Weighted Recall: 0.8977272727272727
Weighted FalsePositiveRate: 0.16237118338888248
Kappa statistic: 0.7677078750549934
Training time: 8.0
		
Iteration: 5
Labeled set size: 115
Unlabelled set size: 60
	
Mean region size: 74.43181818181819
Incorrectly Classified Instances: 11.931818181818182
Correctly Classified Instances: 88.06818181818181
Weighted Precision: 0.881287257824143
Weighted AreaUnderROC: 0.9148756847871892
Root mean squared error: 0.30720514318611264
Relative absolute error: 33.75
Root relative squared error: 61.44102863722253
Weighted TruePositiveRate: 0.8806818181818182
Weighted MatthewsCorrelation: 0.736819234445517
Weighted FMeasure: 0.8783774735902395
Iteration time: 10.0
Weighted AreaUnderPRC: 0.9100657438749541
Mean absolute error: 0.16874999999999998
Coverage of cases: 96.5909090909091
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 56.0
Weighted Recall: 0.8806818181818182
Weighted FalsePositiveRate: 0.17187440140979962
Kappa statistic: 0.7319408181026981
Training time: 10.0
		
Iteration: 6
Labeled set size: 135
Unlabelled set size: 40
	
Mean region size: 69.88636363636364
Incorrectly Classified Instances: 10.795454545454545
Correctly Classified Instances: 89.20454545454545
Weighted Precision: 0.89479824610873
Weighted AreaUnderROC: 0.9169124877089477
Root mean squared error: 0.31060790834631474
Relative absolute error: 31.363636363636356
Root relative squared error: 62.12158166926295
Weighted TruePositiveRate: 0.8920454545454546
Weighted MatthewsCorrelation: 0.7633724905503553
Weighted FMeasure: 0.8893876853287971
Iteration time: 13.0
Weighted AreaUnderPRC: 0.9110745725744471
Mean absolute error: 0.15681818181818177
Coverage of cases: 96.5909090909091
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 69.0
Weighted Recall: 0.8920454545454546
Weighted FalsePositiveRate: 0.1655389227291882
Kappa statistic: 0.7556984219754531
Training time: 12.0
		
Iteration: 7
Labeled set size: 155
Unlabelled set size: 20
	
Mean region size: 68.18181818181819
Incorrectly Classified Instances: 7.386363636363637
Correctly Classified Instances: 92.61363636363636
Weighted Precision: 0.9309828276562148
Weighted AreaUnderROC: 0.9226014889731704
Root mean squared error: 0.28593944050508946
Relative absolute error: 27.613636363636363
Root relative squared error: 57.18788810101789
Weighted TruePositiveRate: 0.9261363636363636
Weighted MatthewsCorrelation: 0.8413037888896028
Weighted FMeasure: 0.9243178899618084
Iteration time: 14.0
Weighted AreaUnderPRC: 0.9171214819407826
Mean absolute error: 0.1380681818181818
Coverage of cases: 96.5909090909091
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 83.0
Weighted Recall: 0.9261363636363636
Weighted FalsePositiveRate: 0.125462111634678
Kappa statistic: 0.8328462887200468
Training time: 13.0
		
Time end:Tue Oct 31 13.17.49 EET 2017