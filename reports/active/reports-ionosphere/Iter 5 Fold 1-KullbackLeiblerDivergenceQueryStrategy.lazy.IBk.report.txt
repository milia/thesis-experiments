Tue Oct 31 13.16.38 EET 2017
Dataset: ionosphere
Test set size: 176
Initial Labelled set size: 35
Initial Unlabelled set size: 140
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Tue Oct 31 13.16.38 EET 2017
		
Iteration: 1
Labeled set size: 35
Unlabelled set size: 140
	
Mean region size: 50.0
Incorrectly Classified Instances: 23.863636363636363
Correctly Classified Instances: 76.13636363636364
Weighted Precision: 0.7627418202235721
Weighted AreaUnderROC: 0.6982722292456806
Root mean squared error: 0.47588607977857517
Relative absolute error: 50.55282555282554
Root relative squared error: 95.17721595571503
Weighted TruePositiveRate: 0.7613636363636364
Weighted MatthewsCorrelation: 0.45772938576167976
Weighted FMeasure: 0.7447433155080213
Iteration time: 12.0
Weighted AreaUnderPRC: 0.6796405527120524
Mean absolute error: 0.2527641277641277
Coverage of cases: 76.13636363636364
Instances selection time: 9.0
Test time: 6.0
Accumulative iteration time: 12.0
Weighted Recall: 0.7613636363636364
Weighted FalsePositiveRate: 0.36481917787227525
Kappa statistic: 0.43304187758858725
Training time: 3.0
		
Iteration: 2
Labeled set size: 55
Unlabelled set size: 120
	
Mean region size: 50.0
Incorrectly Classified Instances: 23.863636363636363
Correctly Classified Instances: 76.13636363636364
Weighted Precision: 0.7846560254195722
Weighted AreaUnderROC: 0.6807135833684507
Root mean squared error: 0.4801780345935505
Relative absolute error: 49.56140350877195
Root relative squared error: 96.0356069187101
Weighted TruePositiveRate: 0.7613636363636364
Weighted MatthewsCorrelation: 0.46705994453936467
Weighted FMeasure: 0.7328709334144116
Iteration time: 13.0
Weighted AreaUnderPRC: 0.6735528859948083
Mean absolute error: 0.24780701754385975
Coverage of cases: 76.13636363636364
Instances selection time: 12.0
Test time: 10.0
Accumulative iteration time: 25.0
Weighted Recall: 0.7613636363636364
Weighted FalsePositiveRate: 0.39993646962673507
Kappa statistic: 0.4104322858510128
Training time: 1.0
		
Iteration: 3
Labeled set size: 75
Unlabelled set size: 100
	
Mean region size: 50.0
Incorrectly Classified Instances: 25.0
Correctly Classified Instances: 75.0
Weighted Precision: 0.7827152317880794
Weighted AreaUnderROC: 0.6613288383199888
Root mean squared error: 0.4936346381817409
Relative absolute error: 51.29870129870133
Root relative squared error: 98.72692763634818
Weighted TruePositiveRate: 0.75
Weighted MatthewsCorrelation: 0.4430910938115398
Weighted FMeasure: 0.7140151515151515
Iteration time: 15.0
Weighted AreaUnderPRC: 0.6596291082863555
Mean absolute error: 0.25649350649350666
Coverage of cases: 75.0
Instances selection time: 14.0
Test time: 12.0
Accumulative iteration time: 40.0
Weighted Recall: 0.75
Weighted FalsePositiveRate: 0.4273423233600225
Kappa statistic: 0.37234559896255465
Training time: 1.0
		
Iteration: 4
Labeled set size: 95
Unlabelled set size: 80
	
Mean region size: 50.0
Incorrectly Classified Instances: 27.272727272727273
Correctly Classified Instances: 72.72727272727273
Weighted Precision: 0.7624633431085044
Weighted AreaUnderROC: 0.629582806573957
Root mean squared error: 0.5169238930546122
Relative absolute error: 55.48266166822864
Root relative squared error: 103.38477861092244
Weighted TruePositiveRate: 0.7272727272727273
Weighted MatthewsCorrelation: 0.38327578126356426
Weighted FMeasure: 0.680461329715061
Iteration time: 15.0
Weighted AreaUnderPRC: 0.6336771503789467
Mean absolute error: 0.2774133083411432
Coverage of cases: 72.72727272727273
Instances selection time: 14.0
Test time: 16.0
Accumulative iteration time: 55.0
Weighted Recall: 0.7272727272727273
Weighted FalsePositiveRate: 0.46810711412481326
Kappa statistic: 0.30400395452298573
Training time: 1.0
		
Iteration: 5
Labeled set size: 115
Unlabelled set size: 60
	
Mean region size: 50.0
Incorrectly Classified Instances: 27.84090909090909
Correctly Classified Instances: 72.1590909090909
Weighted Precision: 0.7569857226107227
Weighted AreaUnderROC: 0.6216462986374491
Root mean squared error: 0.5231854475279905
Relative absolute error: 56.4393939393939
Root relative squared error: 104.63708950559808
Weighted TruePositiveRate: 0.7215909090909091
Weighted MatthewsCorrelation: 0.3675031739266934
Weighted FMeasure: 0.67172491927833
Iteration time: 15.0
Weighted AreaUnderPRC: 0.6273076591968638
Mean absolute error: 0.2821969696969695
Coverage of cases: 72.1590909090909
Instances selection time: 15.0
Test time: 18.0
Accumulative iteration time: 70.0
Weighted Recall: 0.7215909090909091
Weighted FalsePositiveRate: 0.4782983118160109
Kappa statistic: 0.2865651886168101
Training time: 0.0
		
Iteration: 6
Labeled set size: 135
Unlabelled set size: 40
	
Mean region size: 50.0
Incorrectly Classified Instances: 22.15909090909091
Correctly Classified Instances: 77.8409090909091
Weighted Precision: 0.7992029671717172
Weighted AreaUnderROC: 0.7045231071779743
Root mean squared error: 0.4673427913771203
Relative absolute error: 45.13105507631057
Root relative squared error: 93.46855827542406
Weighted TruePositiveRate: 0.7784090909090909
Weighted MatthewsCorrelation: 0.5084235268782055
Weighted FMeasure: 0.7556192169493418
Iteration time: 13.0
Weighted AreaUnderPRC: 0.6937700872359964
Mean absolute error: 0.22565527538155283
Coverage of cases: 77.8409090909091
Instances selection time: 12.0
Test time: 21.0
Accumulative iteration time: 83.0
Weighted Recall: 0.7784090909090909
Weighted FalsePositiveRate: 0.36936287655314204
Kappa statistic: 0.45901639344262296
Training time: 1.0
		
Iteration: 7
Labeled set size: 155
Unlabelled set size: 20
	
Mean region size: 50.0
Incorrectly Classified Instances: 22.15909090909091
Correctly Classified Instances: 77.8409090909091
Weighted Precision: 0.7820604946524065
Weighted AreaUnderROC: 0.7185700238797584
Root mean squared error: 0.46776988419468235
Relative absolute error: 45.02750434279095
Root relative squared error: 93.55397683893646
Weighted TruePositiveRate: 0.7784090909090909
Weighted MatthewsCorrelation: 0.5000698165645474
Weighted FMeasure: 0.7639024766320357
Iteration time: 8.0
Weighted AreaUnderPRC: 0.6982905171366067
Mean absolute error: 0.22513752171395474
Coverage of cases: 77.8409090909091
Instances selection time: 7.0
Test time: 22.0
Accumulative iteration time: 91.0
Weighted Recall: 0.7784090909090909
Weighted FalsePositiveRate: 0.3412690431495741
Kappa statistic: 0.47555012224938886
Training time: 1.0
		
Time end:Tue Oct 31 13.16.38 EET 2017