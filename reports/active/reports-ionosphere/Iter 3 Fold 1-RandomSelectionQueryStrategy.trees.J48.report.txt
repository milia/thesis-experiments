Tue Oct 31 13.17.45 EET 2017
Dataset: ionosphere
Test set size: 176
Initial Labelled set size: 35
Initial Unlabelled set size: 140
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Tue Oct 31 13.17.45 EET 2017
		
Iteration: 1
Labeled set size: 35
Unlabelled set size: 140
	
Mean region size: 50.0
Incorrectly Classified Instances: 15.340909090909092
Correctly Classified Instances: 84.6590909090909
Weighted Precision: 0.8696150579016713
Weighted AreaUnderROC: 0.7892260148897318
Root mean squared error: 0.3779490105099557
Relative absolute error: 34.590909090909115
Root relative squared error: 75.58980210199114
Weighted TruePositiveRate: 0.8465909090909091
Weighted MatthewsCorrelation: 0.6739776644575179
Weighted FMeasure: 0.8352445304291384
Iteration time: 8.0
Weighted AreaUnderPRC: 0.7776916485822444
Mean absolute error: 0.17295454545454558
Coverage of cases: 84.6590909090909
Instances selection time: 3.0
Test time: 4.0
Accumulative iteration time: 8.0
Weighted Recall: 0.8465909090909091
Weighted FalsePositiveRate: 0.26813887931144564
Kappa statistic: 0.634123806590699
Training time: 5.0
		
Iteration: 2
Labeled set size: 55
Unlabelled set size: 120
	
Mean region size: 50.0
Incorrectly Classified Instances: 11.931818181818182
Correctly Classified Instances: 88.06818181818181
Weighted Precision: 0.8945850550964187
Weighted AreaUnderROC: 0.8368450625087793
Root mean squared error: 0.3454246398538787
Relative absolute error: 23.863636363636363
Root relative squared error: 69.08492797077574
Weighted TruePositiveRate: 0.8806818181818182
Weighted MatthewsCorrelation: 0.7458583809586894
Weighted FMeasure: 0.8747147712100983
Iteration time: 3.0
Weighted AreaUnderPRC: 0.823035037878788
Mean absolute error: 0.11931818181818182
Coverage of cases: 88.06818181818181
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 11.0
Weighted Recall: 0.8806818181818182
Weighted FalsePositiveRate: 0.20699169316425953
Kappa statistic: 0.7218543046357617
Training time: 2.0
		
Iteration: 3
Labeled set size: 75
Unlabelled set size: 100
	
Mean region size: 55.68181818181818
Incorrectly Classified Instances: 15.909090909090908
Correctly Classified Instances: 84.0909090909091
Weighted Precision: 0.8656116875541336
Weighted AreaUnderROC: 0.8702767242590251
Root mean squared error: 0.34996392310457863
Relative absolute error: 29.545454545454547
Root relative squared error: 69.99278462091573
Weighted TruePositiveRate: 0.8409090909090909
Weighted MatthewsCorrelation: 0.661888387675527
Weighted FMeasure: 0.8284343434343434
Iteration time: 3.0
Weighted AreaUnderPRC: 0.8530864847030735
Mean absolute error: 0.14772727272727273
Coverage of cases: 91.47727272727273
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 14.0
Weighted Recall: 0.8409090909090909
Weighted FalsePositiveRate: 0.27833007700264334
Kappa statistic: 0.619106507961045
Training time: 3.0
		
Iteration: 4
Labeled set size: 95
Unlabelled set size: 80
	
Mean region size: 50.0
Incorrectly Classified Instances: 13.068181818181818
Correctly Classified Instances: 86.93181818181819
Weighted Precision: 0.8748668323863636
Weighted AreaUnderROC: 0.8315072341621014
Root mean squared error: 0.3614994027406106
Relative absolute error: 26.136363636363637
Root relative squared error: 72.29988054812212
Weighted TruePositiveRate: 0.8693181818181818
Weighted MatthewsCorrelation: 0.7136855417399497
Weighted FMeasure: 0.864555190800004
Iteration time: 5.0
Weighted AreaUnderPRC: 0.8097895682679063
Mean absolute error: 0.13068181818181818
Coverage of cases: 86.93181818181819
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 19.0
Weighted Recall: 0.8693181818181818
Weighted FalsePositiveRate: 0.20630371349397897
Kappa statistic: 0.6998813760379595
Training time: 5.0
		
Iteration: 5
Labeled set size: 115
Unlabelled set size: 60
	
Mean region size: 52.27272727272727
Incorrectly Classified Instances: 11.931818181818182
Correctly Classified Instances: 88.06818181818181
Weighted Precision: 0.884738455988456
Weighted AreaUnderROC: 0.8486444725382778
Root mean squared error: 0.3417209398874699
Relative absolute error: 27.020202020202017
Root relative squared error: 68.34418797749397
Weighted TruePositiveRate: 0.8806818181818182
Weighted MatthewsCorrelation: 0.7385405420675035
Weighted FMeasure: 0.8770634380985401
Iteration time: 7.0
Weighted AreaUnderPRC: 0.8282965662880013
Mean absolute error: 0.13510101010101008
Coverage of cases: 88.63636363636364
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 26.0
Weighted Recall: 0.8806818181818182
Weighted FalsePositiveRate: 0.18592131811158358
Kappa statistic: 0.727995289961731
Training time: 7.0
		
Iteration: 6
Labeled set size: 135
Unlabelled set size: 40
	
Mean region size: 52.27272727272727
Incorrectly Classified Instances: 13.068181818181818
Correctly Classified Instances: 86.93181818181819
Weighted Precision: 0.8693958022851466
Weighted AreaUnderROC: 0.8419019525214216
Root mean squared error: 0.3608701604833416
Relative absolute error: 27.499999999999996
Root relative squared error: 72.17403209666831
Weighted TruePositiveRate: 0.8693181818181818
Weighted MatthewsCorrelation: 0.7111196337197121
Weighted FMeasure: 0.866794375836929
Iteration time: 9.0
Weighted AreaUnderPRC: 0.8146374109182094
Mean absolute error: 0.13749999999999998
Coverage of cases: 87.5
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 35.0
Weighted Recall: 0.8693181818181818
Weighted FalsePositiveRate: 0.18523333844130302
Kappa statistic: 0.7064113722077168
Training time: 9.0
		
Iteration: 7
Labeled set size: 155
Unlabelled set size: 20
	
Mean region size: 53.40909090909091
Incorrectly Classified Instances: 11.363636363636363
Correctly Classified Instances: 88.63636363636364
Weighted Precision: 0.8921413232436854
Weighted AreaUnderROC: 0.8820761342885237
Root mean squared error: 0.3280235232695695
Relative absolute error: 23.011363636363637
Root relative squared error: 65.6047046539139
Weighted TruePositiveRate: 0.8863636363636364
Weighted MatthewsCorrelation: 0.7525608728493296
Weighted FMeasure: 0.8825757575757575
Iteration time: 10.0
Weighted AreaUnderPRC: 0.8630724902634026
Mean absolute error: 0.11505681818181818
Coverage of cases: 91.47727272727273
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 45.0
Weighted Recall: 0.8863636363636364
Weighted FalsePositiveRate: 0.18275357877127787
Kappa statistic: 0.7399911360614566
Training time: 10.0
		
Time end:Tue Oct 31 13.17.45 EET 2017