Tue Oct 31 13.16.31 EET 2017
Dataset: ionosphere
Test set size: 176
Initial Labelled set size: 35
Initial Unlabelled set size: 140
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Tue Oct 31 13.16.31 EET 2017
		
Iteration: 1
Labeled set size: 35
Unlabelled set size: 140
	
Mean region size: 50.0
Incorrectly Classified Instances: 15.340909090909092
Correctly Classified Instances: 84.6590909090909
Weighted Precision: 0.876176948051948
Weighted AreaUnderROC: 0.7857142857142857
Root mean squared error: 0.39167472590032015
Relative absolute error: 30.681818181818183
Root relative squared error: 78.33494518006403
Weighted TruePositiveRate: 0.8465909090909091
Weighted MatthewsCorrelation: 0.6791351042288407
Weighted FMeasure: 0.8338573481854114
Iteration time: 22.0
Weighted AreaUnderPRC: 0.7776813385478157
Mean absolute error: 0.1534090909090909
Coverage of cases: 84.6590909090909
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 22.0
Weighted Recall: 0.8465909090909091
Weighted FalsePositiveRate: 0.2751623376623376
Kappa statistic: 0.6312849162011173
Training time: 21.0
		
Iteration: 2
Labeled set size: 55
Unlabelled set size: 120
	
Mean region size: 50.0
Incorrectly Classified Instances: 16.477272727272727
Correctly Classified Instances: 83.52272727272727
Weighted Precision: 0.833442364911535
Weighted AreaUnderROC: 0.8119820199466218
Root mean squared error: 0.40592207044299433
Relative absolute error: 32.95454545454545
Root relative squared error: 81.18441408859887
Weighted TruePositiveRate: 0.8352272727272727
Weighted MatthewsCorrelation: 0.636376388034683
Weighted FMeasure: 0.8336061321598511
Iteration time: 9.0
Weighted AreaUnderPRC: 0.7768277493769049
Mean absolute error: 0.16477272727272727
Coverage of cases: 83.52272727272727
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 31.0
Weighted Recall: 0.8352272727272727
Weighted FalsePositiveRate: 0.21126323283402926
Kappa statistic: 0.6351158135544752
Training time: 8.0
		
Iteration: 3
Labeled set size: 75
Unlabelled set size: 100
	
Mean region size: 50.0
Incorrectly Classified Instances: 19.886363636363637
Correctly Classified Instances: 80.11363636363636
Weighted Precision: 0.7983060286487706
Weighted AreaUnderROC: 0.7643629723275741
Root mean squared error: 0.44594129250792236
Relative absolute error: 39.77272727272727
Root relative squared error: 89.18825850158447
Weighted TruePositiveRate: 0.8011363636363636
Weighted MatthewsCorrelation: 0.5555556949790289
Weighted FMeasure: 0.7962404729740998
Iteration time: 13.0
Weighted AreaUnderPRC: 0.7328003397811865
Mean absolute error: 0.19886363636363635
Coverage of cases: 80.11363636363636
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 44.0
Weighted Recall: 0.8011363636363636
Weighted FalsePositiveRate: 0.27241041898121543
Kappa statistic: 0.5499707773232029
Training time: 12.0
		
Iteration: 4
Labeled set size: 95
Unlabelled set size: 80
	
Mean region size: 50.0
Incorrectly Classified Instances: 19.318181818181817
Correctly Classified Instances: 80.68181818181819
Weighted Precision: 0.8056166274670211
Weighted AreaUnderROC: 0.76527602191319
Root mean squared error: 0.4395245364957663
Relative absolute error: 38.63636363636363
Root relative squared error: 87.90490729915325
Weighted TruePositiveRate: 0.8068181818181818
Weighted MatthewsCorrelation: 0.5674630498606344
Weighted FMeasure: 0.8003787878787879
Iteration time: 13.0
Weighted AreaUnderPRC: 0.7362932558736387
Mean absolute error: 0.19318181818181818
Coverage of cases: 80.68181818181819
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 57.0
Weighted Recall: 0.8068181818181818
Weighted FalsePositiveRate: 0.2762661379918017
Kappa statistic: 0.5579849313044762
Training time: 12.0
		
Iteration: 5
Labeled set size: 115
Unlabelled set size: 60
	
Mean region size: 50.0
Incorrectly Classified Instances: 16.477272727272727
Correctly Classified Instances: 83.52272727272727
Weighted Precision: 0.8504094251336899
Weighted AreaUnderROC: 0.7803764573676077
Root mean squared error: 0.40592207044299433
Relative absolute error: 32.95454545454545
Root relative squared error: 81.18441408859887
Weighted TruePositiveRate: 0.8352272727272727
Weighted MatthewsCorrelation: 0.6414777338450106
Weighted FMeasure: 0.824440303136642
Iteration time: 11.0
Weighted AreaUnderPRC: 0.7637985385269811
Mean absolute error: 0.16477272727272727
Coverage of cases: 83.52272727272727
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 68.0
Weighted Recall: 0.8352272727272727
Weighted FalsePositiveRate: 0.27447435799205705
Kappa statistic: 0.6100244498777506
Training time: 10.0
		
Iteration: 6
Labeled set size: 135
Unlabelled set size: 40
	
Mean region size: 50.0
Incorrectly Classified Instances: 16.477272727272727
Correctly Classified Instances: 83.52272727272727
Weighted Precision: 0.8555426981485333
Weighted AreaUnderROC: 0.7768647281921617
Root mean squared error: 0.40592207044299433
Relative absolute error: 32.95454545454545
Root relative squared error: 81.18441408859887
Weighted TruePositiveRate: 0.8352272727272727
Weighted MatthewsCorrelation: 0.6451724024505915
Weighted FMeasure: 0.8230404215720377
Iteration time: 14.0
Weighted AreaUnderPRC: 0.7632769061926736
Mean absolute error: 0.16477272727272727
Coverage of cases: 83.52272727272727
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 82.0
Weighted Recall: 0.8352272727272727
Weighted FalsePositiveRate: 0.28149781634294907
Kappa statistic: 0.6070218663381582
Training time: 13.0
		
Iteration: 7
Labeled set size: 155
Unlabelled set size: 20
	
Mean region size: 50.0
Incorrectly Classified Instances: 17.045454545454547
Correctly Classified Instances: 82.95454545454545
Weighted Precision: 0.8414244066683092
Weighted AreaUnderROC: 0.7759516786065458
Root mean squared error: 0.41286141192238524
Relative absolute error: 34.090909090909086
Root relative squared error: 82.57228238447705
Weighted TruePositiveRate: 0.8295454545454546
Weighted MatthewsCorrelation: 0.6259126367272496
Weighted FMeasure: 0.8190770922625762
Iteration time: 14.0
Weighted AreaUnderPRC: 0.7572309733514001
Mean absolute error: 0.17045454545454544
Coverage of cases: 82.95454545454545
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 96.0
Weighted Recall: 0.8295454545454546
Weighted FalsePositiveRate: 0.27764209733236284
Kappa statistic: 0.598112345866951
Training time: 14.0
		
Time end:Tue Oct 31 13.16.31 EET 2017