Tue Oct 31 13.17.28 EET 2017
Dataset: ionosphere
Test set size: 175
Initial Labelled set size: 35
Initial Unlabelled set size: 140
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Tue Oct 31 13.17.28 EET 2017
		
Iteration: 1
Labeled set size: 35
Unlabelled set size: 140
	
Mean region size: 86.66477272727272
Incorrectly Classified Instances: 14.299675324675325
Correctly Classified Instances: 85.7003246753247
Weighted Precision: 0.8608336381313487
Weighted AreaUnderROC: 0.9128054577288143
Root mean squared error: 0.33582157661372547
Relative absolute error: 47.88363636363638
Root relative squared error: 67.1643153227451
Weighted TruePositiveRate: 0.8570032467532467
Weighted MatthewsCorrelation: 0.6872171317325109
Weighted FMeasure: 0.8531671284795092
Iteration time: 4.5
Weighted AreaUnderPRC: 0.9040016403570272
Mean absolute error: 0.2394181818181819
Coverage of cases: 98.85909090909091
Instances selection time: 0.6
Test time: 1.7
Accumulative iteration time: 4.5
Weighted Recall: 0.8570032467532467
Weighted FalsePositiveRate: 0.20368729647933184
Kappa statistic: 0.6772311888728298
Training time: 3.9
		
Iteration: 2
Labeled set size: 55
Unlabelled set size: 120
	
Mean region size: 87.80519480519482
Incorrectly Classified Instances: 9.00292207792208
Correctly Classified Instances: 90.99707792207792
Weighted Precision: 0.9113599582720557
Weighted AreaUnderROC: 0.9472553328115907
Root mean squared error: 0.2941037114002546
Relative absolute error: 42.047467532467564
Root relative squared error: 58.82074228005092
Weighted TruePositiveRate: 0.9099707792207792
Weighted MatthewsCorrelation: 0.8045079061971846
Weighted FMeasure: 0.9091849901797472
Iteration time: 6.9
Weighted AreaUnderPRC: 0.9410544646692186
Mean absolute error: 0.2102373376623378
Coverage of cases: 99.25974025974027
Instances selection time: 1.3
Test time: 1.0
Accumulative iteration time: 11.4
Weighted Recall: 0.9099707792207792
Weighted FalsePositiveRate: 0.11952356051028619
Kappa statistic: 0.8016346853931322
Training time: 5.6
		
Iteration: 3
Labeled set size: 75
Unlabelled set size: 100
	
Mean region size: 84.0965909090909
Incorrectly Classified Instances: 10.424675324675325
Correctly Classified Instances: 89.57532467532468
Weighted Precision: 0.8980394847666213
Weighted AreaUnderROC: 0.9481192808982
Root mean squared error: 0.28974653930680394
Relative absolute error: 38.953766233766245
Root relative squared error: 57.94930786136079
Weighted TruePositiveRate: 0.8957532467532469
Weighted MatthewsCorrelation: 0.7767245513920477
Weighted FMeasure: 0.895998490873076
Iteration time: 8.5
Weighted AreaUnderPRC: 0.9420116414169503
Mean absolute error: 0.19476883116883123
Coverage of cases: 99.48668831168831
Instances selection time: 0.8
Test time: 1.1
Accumulative iteration time: 19.9
Weighted Recall: 0.8957532467532469
Weighted FalsePositiveRate: 0.11634251490888659
Kappa statistic: 0.7750315781972701
Training time: 7.7
		
Iteration: 4
Labeled set size: 95
Unlabelled set size: 80
	
Mean region size: 84.58392857142857
Incorrectly Classified Instances: 9.004545454545454
Correctly Classified Instances: 90.99545454545454
Weighted Precision: 0.9122526798884861
Weighted AreaUnderROC: 0.9571991702285636
Root mean squared error: 0.2776818361339206
Relative absolute error: 37.000779220779236
Root relative squared error: 55.53636722678414
Weighted TruePositiveRate: 0.9099545454545455
Weighted MatthewsCorrelation: 0.8057529057555521
Weighted FMeasure: 0.9092813317569046
Iteration time: 11.4
Weighted AreaUnderPRC: 0.9514759220479606
Mean absolute error: 0.18500389610389617
Coverage of cases: 99.71493506493506
Instances selection time: 0.8
Test time: 1.1
Accumulative iteration time: 31.3
Weighted Recall: 0.9099545454545455
Weighted FalsePositiveRate: 0.11674535174756413
Kappa statistic: 0.8022026594936852
Training time: 10.6
		
Iteration: 5
Labeled set size: 115
Unlabelled set size: 60
	
Mean region size: 79.97288961038961
Incorrectly Classified Instances: 7.3487012987012985
Correctly Classified Instances: 92.6512987012987
Weighted Precision: 0.9273564253190234
Weighted AreaUnderROC: 0.960647562859952
Root mean squared error: 0.25889510605988886
Relative absolute error: 31.517987012987028
Root relative squared error: 51.77902121197777
Weighted TruePositiveRate: 0.9265129870129872
Weighted MatthewsCorrelation: 0.8404888415662628
Weighted FMeasure: 0.9261055479568876
Iteration time: 13.9
Weighted AreaUnderPRC: 0.955063010359375
Mean absolute error: 0.15758993506493513
Coverage of cases: 99.08896103896105
Instances selection time: 0.7
Test time: 1.0
Accumulative iteration time: 45.2
Weighted Recall: 0.9265129870129872
Weighted FalsePositiveRate: 0.09490303477250381
Kappa statistic: 0.8388442021232126
Training time: 13.2
		
Iteration: 6
Labeled set size: 135
Unlabelled set size: 40
	
Mean region size: 77.8362012987013
Incorrectly Classified Instances: 7.635064935064935
Correctly Classified Instances: 92.36493506493505
Weighted Precision: 0.9244259709603793
Weighted AreaUnderROC: 0.9584734262436537
Root mean squared error: 0.25893383721254504
Relative absolute error: 30.335974025974036
Root relative squared error: 51.786767442509
Weighted TruePositiveRate: 0.9236493506493504
Weighted MatthewsCorrelation: 0.833960449282535
Weighted FMeasure: 0.923119308092093
Iteration time: 17.0
Weighted AreaUnderPRC: 0.9538811105300811
Mean absolute error: 0.1516798701298702
Coverage of cases: 99.03149350649352
Instances selection time: 0.7
Test time: 1.2
Accumulative iteration time: 62.2
Weighted Recall: 0.9236493506493504
Weighted FalsePositiveRate: 0.10001541329859916
Kappa statistic: 0.8321465513054624
Training time: 16.3
		
Iteration: 7
Labeled set size: 155
Unlabelled set size: 20
	
Mean region size: 77.15
Incorrectly Classified Instances: 7.406818181818181
Correctly Classified Instances: 92.59318181818182
Weighted Precision: 0.9268019393472369
Weighted AreaUnderROC: 0.9619315388397247
Root mean squared error: 0.2523871121592326
Relative absolute error: 29.29584415584417
Root relative squared error: 50.477422431846506
Weighted TruePositiveRate: 0.9259318181818184
Weighted MatthewsCorrelation: 0.8391868351077176
Weighted FMeasure: 0.9254838830923118
Iteration time: 18.9
Weighted AreaUnderPRC: 0.9572570534021629
Mean absolute error: 0.14647922077922085
Coverage of cases: 99.08896103896103
Instances selection time: 0.6
Test time: 1.2
Accumulative iteration time: 81.1
Weighted Recall: 0.9259318181818184
Weighted FalsePositiveRate: 0.09590916752863655
Kappa statistic: 0.8374394188005461
Training time: 18.3
		
Iteration: 8
Labeled set size: 175
Unlabelled set size: 1
	
Mean region size: 74.85714285714286
Incorrectly Classified Instances: 8.0
Correctly Classified Instances: 92.0
Weighted Precision: 0.9204697934049841
Weighted AreaUnderROC: 0.9645124716553287
Root mean squared error: 0.2536520329784385
Relative absolute error: 28.480000000000008
Root relative squared error: 50.73040659568771
Weighted TruePositiveRate: 0.9199999999999999
Weighted MatthewsCorrelation: 0.8256648766666924
Weighted FMeasure: 0.9193459263826302
Iteration time: 23.2
Weighted AreaUnderPRC: 0.9605912688784274
Mean absolute error: 0.14240000000000003
Coverage of cases: 99.20000000000002
Instances selection time: 0.8
Test time: 1.2
Accumulative iteration time: 105.8
Weighted Recall: 0.9199999999999999
Weighted FalsePositiveRate: 0.10611111111111109
Kappa statistic: 0.8239310957202239
Training time: 22.4
		
Time end:Tue Oct 31 13.17.32 EET 2017