Tue Oct 31 13.18.05 EET 2017
Dataset: ionosphere
Test set size: 175
Initial Labelled set size: 35
Initial Unlabelled set size: 141
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Tue Oct 31 13.18.05 EET 2017
		
Iteration: 1
Labeled set size: 35
Unlabelled set size: 141
	
Mean region size: 60.285714285714285
Incorrectly Classified Instances: 12.0
Correctly Classified Instances: 88.0
Weighted Precision: 0.8804673423423424
Weighted AreaUnderROC: 0.9010770975056689
Root mean squared error: 0.33500533044799663
Relative absolute error: 29.02857142857142
Root relative squared error: 67.00106608959932
Weighted TruePositiveRate: 0.88
Weighted MatthewsCorrelation: 0.7405409779360643
Weighted FMeasure: 0.8802033826489176
Iteration time: 3.0
Weighted AreaUnderPRC: 0.8669600707850708
Mean absolute error: 0.1451428571428571
Coverage of cases: 92.0
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 3.0
Weighted Recall: 0.88
Weighted FalsePositiveRate: 0.13694444444444445
Kappa statistic: 0.740484429065744
Training time: 3.0
		
Iteration: 2
Labeled set size: 55
Unlabelled set size: 121
	
Mean region size: 70.0
Incorrectly Classified Instances: 20.571428571428573
Correctly Classified Instances: 79.42857142857143
Weighted Precision: 0.7914231499051233
Weighted AreaUnderROC: 0.7422052154195012
Root mean squared error: 0.44130002017554015
Relative absolute error: 46.639455782312915
Root relative squared error: 88.26000403510803
Weighted TruePositiveRate: 0.7942857142857143
Weighted MatthewsCorrelation: 0.540720113939946
Weighted FMeasure: 0.7886886708296165
Iteration time: 4.0
Weighted AreaUnderPRC: 0.7247922730396504
Mean absolute error: 0.23319727891156458
Coverage of cases: 87.42857142857143
Instances selection time: 1.0
Test time: 0.0
Accumulative iteration time: 7.0
Weighted Recall: 0.7942857142857143
Weighted FalsePositiveRate: 0.28238095238095234
Kappa statistic: 0.5341614906832297
Training time: 3.0
		
Iteration: 3
Labeled set size: 75
Unlabelled set size: 101
	
Mean region size: 59.142857142857146
Incorrectly Classified Instances: 14.857142857142858
Correctly Classified Instances: 85.14285714285714
Weighted Precision: 0.8501227352425482
Weighted AreaUnderROC: 0.8389314058956916
Root mean squared error: 0.38154240023174785
Relative absolute error: 31.390476190476186
Root relative squared error: 76.30848004634957
Weighted TruePositiveRate: 0.8514285714285714
Weighted MatthewsCorrelation: 0.6738913336101775
Weighted FMeasure: 0.8502962323842393
Iteration time: 5.0
Weighted AreaUnderPRC: 0.8074401842848365
Mean absolute error: 0.15695238095238093
Coverage of cases: 88.0
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 12.0
Weighted Recall: 0.8514285714285714
Weighted FalsePositiveRate: 0.18773809523809523
Kappa statistic: 0.6730382293762576
Training time: 5.0
		
Iteration: 4
Labeled set size: 95
Unlabelled set size: 81
	
Mean region size: 52.57142857142857
Incorrectly Classified Instances: 14.285714285714286
Correctly Classified Instances: 85.71428571428571
Weighted Precision: 0.8560869565217392
Weighted AreaUnderROC: 0.8570011337868482
Root mean squared error: 0.362617228564666
Relative absolute error: 29.60601503759399
Root relative squared error: 72.5234457129332
Weighted TruePositiveRate: 0.8571428571428571
Weighted MatthewsCorrelation: 0.6872025780312658
Weighted FMeasure: 0.8563446867948856
Iteration time: 8.0
Weighted AreaUnderPRC: 0.8301955129264597
Mean absolute error: 0.14803007518796996
Coverage of cases: 88.57142857142857
Instances selection time: 1.0
Test time: 0.0
Accumulative iteration time: 20.0
Weighted Recall: 0.8571428571428571
Weighted FalsePositiveRate: 0.17757936507936506
Kappa statistic: 0.6867167919799497
Training time: 7.0
		
Iteration: 5
Labeled set size: 115
Unlabelled set size: 61
	
Mean region size: 50.0
Incorrectly Classified Instances: 14.285714285714286
Correctly Classified Instances: 85.71428571428571
Weighted Precision: 0.8721804511278195
Weighted AreaUnderROC: 0.7851473922902494
Root mean squared error: 0.36968712640362716
Relative absolute error: 31.657142857142855
Root relative squared error: 73.93742528072544
Weighted TruePositiveRate: 0.8571428571428571
Weighted MatthewsCorrelation: 0.6935194938140639
Weighted FMeasure: 0.8489795918367345
Iteration time: 7.0
Weighted AreaUnderPRC: 0.7909110050874694
Mean absolute error: 0.15828571428571428
Coverage of cases: 85.71428571428571
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 27.0
Weighted Recall: 0.8571428571428571
Weighted FalsePositiveRate: 0.24007936507936506
Kappa statistic: 0.6655965757089352
Training time: 7.0
		
Iteration: 6
Labeled set size: 135
Unlabelled set size: 41
	
Mean region size: 57.142857142857146
Incorrectly Classified Instances: 10.285714285714286
Correctly Classified Instances: 89.71428571428571
Weighted Precision: 0.8966465343687086
Weighted AreaUnderROC: 0.8948412698412699
Root mean squared error: 0.29696116516017385
Relative absolute error: 23.383261561505318
Root relative squared error: 59.39223303203477
Weighted TruePositiveRate: 0.8971428571428571
Weighted MatthewsCorrelation: 0.7754666623413939
Weighted FMeasure: 0.8967684841564374
Iteration time: 10.0
Weighted AreaUnderPRC: 0.8898890001017854
Mean absolute error: 0.11691630780752658
Coverage of cases: 94.28571428571429
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 37.0
Weighted Recall: 0.8971428571428571
Weighted FalsePositiveRate: 0.1273015873015873
Kappa statistic: 0.7752247752247752
Training time: 10.0
		
Iteration: 7
Labeled set size: 155
Unlabelled set size: 21
	
Mean region size: 55.142857142857146
Incorrectly Classified Instances: 11.428571428571429
Correctly Classified Instances: 88.57142857142857
Weighted Precision: 0.8946808510638299
Weighted AreaUnderROC: 0.8795351473922902
Root mean squared error: 0.3209179617790501
Relative absolute error: 23.26530612244898
Root relative squared error: 64.18359235581002
Weighted TruePositiveRate: 0.8857142857142857
Weighted MatthewsCorrelation: 0.7542267863267427
Weighted FMeasure: 0.8812121212121211
Iteration time: 14.0
Weighted AreaUnderPRC: 0.8704764826443397
Mean absolute error: 0.1163265306122449
Coverage of cases: 92.57142857142857
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 51.0
Weighted Recall: 0.8857142857142857
Weighted FalsePositiveRate: 0.18928571428571428
Kappa statistic: 0.7373949579831932
Training time: 14.0
		
Iteration: 8
Labeled set size: 175
Unlabelled set size: 1
	
Mean region size: 55.714285714285715
Incorrectly Classified Instances: 10.285714285714286
Correctly Classified Instances: 89.71428571428571
Weighted Precision: 0.9014041745730551
Weighted AreaUnderROC: 0.9259495464852607
Root mean squared error: 0.29050779255028547
Relative absolute error: 25.155877678174093
Root relative squared error: 58.101558510057096
Weighted TruePositiveRate: 0.8971428571428571
Weighted MatthewsCorrelation: 0.7764992333905041
Weighted FMeasure: 0.8943443354148082
Iteration time: 19.0
Weighted AreaUnderPRC: 0.908530219851513
Mean absolute error: 0.12577938839087047
Coverage of cases: 94.85714285714286
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 70.0
Weighted Recall: 0.8971428571428571
Weighted FalsePositiveRate: 0.16202380952380951
Kappa statistic: 0.7670807453416149
Training time: 19.0
		
Time end:Tue Oct 31 13.18.05 EET 2017