Tue Oct 31 13.16.01 EET 2017
Dataset: ionosphere
Test set size: 175
Initial Labelled set size: 35
Initial Unlabelled set size: 140
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Tue Oct 31 13.16.01 EET 2017
		
Iteration: 1
Labeled set size: 35
Unlabelled set size: 140
	
Mean region size: 50.0
Incorrectly Classified Instances: 21.199675324675326
Correctly Classified Instances: 78.80032467532467
Weighted Precision: 0.8220153379027053
Weighted AreaUnderROC: 0.7187895069532237
Root mean squared error: 0.44494486700717795
Relative absolute error: 45.506910319410245
Root relative squared error: 88.9889734014356
Weighted TruePositiveRate: 0.7880032467532467
Weighted MatthewsCorrelation: 0.535962392663342
Weighted FMeasure: 0.7600672052401615
Iteration time: 21.8
Weighted AreaUnderPRC: 0.7098244868087286
Mean absolute error: 0.22753455159705127
Coverage of cases: 78.80032467532467
Instances selection time: 21.5
Test time: 17.0
Accumulative iteration time: 21.8
Weighted Recall: 0.7880032467532467
Weighted FalsePositiveRate: 0.3504804205136064
Kappa statistic: 0.47816214824608083
Training time: 0.3
		
Iteration: 2
Labeled set size: 55
Unlabelled set size: 120
	
Mean region size: 50.0
Incorrectly Classified Instances: 18.28961038961039
Correctly Classified Instances: 81.71038961038961
Weighted Precision: 0.8432088173385448
Weighted AreaUnderROC: 0.7543955561575663
Root mean squared error: 0.41693208840788837
Relative absolute error: 38.799265291918886
Root relative squared error: 83.38641768157767
Weighted TruePositiveRate: 0.8171038961038961
Weighted MatthewsCorrelation: 0.6023173606402809
Weighted FMeasure: 0.7975218601162746
Iteration time: 17.0
Weighted AreaUnderPRC: 0.7431227648896527
Mean absolute error: 0.19399632645959441
Coverage of cases: 81.71038961038961
Instances selection time: 16.6
Test time: 23.5
Accumulative iteration time: 38.8
Weighted Recall: 0.8171038961038961
Weighted FalsePositiveRate: 0.30845362921247876
Kappa statistic: 0.5557151773309328
Training time: 0.4
		
Iteration: 3
Labeled set size: 75
Unlabelled set size: 100
	
Mean region size: 50.0
Incorrectly Classified Instances: 18.51590909090909
Correctly Classified Instances: 81.48409090909091
Weighted Precision: 0.8410876315285204
Weighted AreaUnderROC: 0.7546874686452751
Root mean squared error: 0.42062287810488963
Relative absolute error: 38.665158320831516
Root relative squared error: 84.12457562097794
Weighted TruePositiveRate: 0.8148409090909092
Weighted MatthewsCorrelation: 0.5976279806998943
Weighted FMeasure: 0.795097788066599
Iteration time: 17.4
Weighted AreaUnderPRC: 0.7422833032399894
Mean absolute error: 0.1933257916041576
Coverage of cases: 81.48409090909091
Instances selection time: 16.9
Test time: 31.9
Accumulative iteration time: 56.2
Weighted Recall: 0.8148409090909092
Weighted FalsePositiveRate: 0.3055084888071614
Kappa statistic: 0.5515837753485427
Training time: 0.5
		
Iteration: 4
Labeled set size: 95
Unlabelled set size: 80
	
Mean region size: 50.0
Incorrectly Classified Instances: 19.087337662337664
Correctly Classified Instances: 80.91266233766234
Weighted Precision: 0.8359457864521719
Weighted AreaUnderROC: 0.7463839850099332
Root mean squared error: 0.42819780965026977
Relative absolute error: 39.447675391618674
Root relative squared error: 85.63956193005397
Weighted TruePositiveRate: 0.8091266233766234
Weighted MatthewsCorrelation: 0.5829818785340092
Weighted FMeasure: 0.7872265147825802
Iteration time: 15.5
Weighted AreaUnderPRC: 0.7347368725798994
Mean absolute error: 0.1972383769580934
Coverage of cases: 80.91266233766234
Instances selection time: 15.3
Test time: 31.7
Accumulative iteration time: 71.7
Weighted Recall: 0.8091266233766234
Weighted FalsePositiveRate: 0.31640117036355975
Kappa statistic: 0.5347630835177457
Training time: 0.2
		
Iteration: 5
Labeled set size: 115
Unlabelled set size: 60
	
Mean region size: 50.0
Incorrectly Classified Instances: 19.827272727272728
Correctly Classified Instances: 80.17272727272726
Weighted Precision: 0.8317235894924482
Weighted AreaUnderROC: 0.7346697344129393
Root mean squared error: 0.4376388312766756
Relative absolute error: 40.684639115673576
Root relative squared error: 87.52776625533512
Weighted TruePositiveRate: 0.8017272727272727
Weighted MatthewsCorrelation: 0.5666296266679137
Weighted FMeasure: 0.7771974351426867
Iteration time: 14.7
Weighted AreaUnderPRC: 0.7250278201837153
Mean absolute error: 0.2034231955783679
Coverage of cases: 80.17272727272726
Instances selection time: 14.3
Test time: 38.0
Accumulative iteration time: 86.4
Weighted Recall: 0.8017272727272727
Weighted FalsePositiveRate: 0.332430320908197
Kappa statistic: 0.5134360931981679
Training time: 0.4
		
Iteration: 6
Labeled set size: 135
Unlabelled set size: 40
	
Mean region size: 50.0
Incorrectly Classified Instances: 18.289935064935065
Correctly Classified Instances: 81.71006493506495
Weighted Precision: 0.8430096461768093
Weighted AreaUnderROC: 0.7543605642847109
Root mean squared error: 0.4222875269756526
Relative absolute error: 37.50446933916228
Root relative squared error: 84.45750539513051
Weighted TruePositiveRate: 0.8171006493506493
Weighted MatthewsCorrelation: 0.6041156504465091
Weighted FMeasure: 0.7988776629245511
Iteration time: 12.0
Weighted AreaUnderPRC: 0.7424431806408245
Mean absolute error: 0.1875223466958114
Coverage of cases: 81.71006493506495
Instances selection time: 11.2
Test time: 42.6
Accumulative iteration time: 98.4
Weighted Recall: 0.8171006493506493
Weighted FalsePositiveRate: 0.308450382459232
Kappa statistic: 0.5573363737074024
Training time: 0.8
		
Iteration: 7
Labeled set size: 155
Unlabelled set size: 20
	
Mean region size: 50.0
Incorrectly Classified Instances: 16.295129870129873
Correctly Classified Instances: 83.70487012987013
Weighted Precision: 0.8531897097516736
Weighted AreaUnderROC: 0.7835682429313909
Root mean squared error: 0.40008810742747053
Relative absolute error: 33.44753600963798
Root relative squared error: 80.01762148549412
Weighted TruePositiveRate: 0.8370487012987013
Weighted MatthewsCorrelation: 0.6468714287091888
Weighted FMeasure: 0.8261844292238351
Iteration time: 7.3
Weighted AreaUnderPRC: 0.7674113204960558
Mean absolute error: 0.1672376800481899
Coverage of cases: 83.70487012987013
Instances selection time: 7.0
Test time: 47.0
Accumulative iteration time: 105.7
Weighted Recall: 0.8370487012987013
Weighted FalsePositiveRate: 0.27002559412072685
Kappa statistic: 0.6149018376661731
Training time: 0.3
		
Iteration: 8
Labeled set size: 175
Unlabelled set size: 1
	
Mean region size: 50.0
Incorrectly Classified Instances: 14.285714285714286
Correctly Classified Instances: 85.71428571428571
Weighted Precision: 0.87457871355389
Weighted AreaUnderROC: 0.807936507936508
Root mean squared error: 0.3744821287299489
Relative absolute error: 29.375747391257324
Root relative squared error: 74.89642574598977
Weighted TruePositiveRate: 0.8571428571428571
Weighted MatthewsCorrelation: 0.694854934497344
Weighted FMeasure: 0.847987162866429
Iteration time: 4.4
Weighted AreaUnderPRC: 0.7930522857562922
Mean absolute error: 0.14687873695628662
Coverage of cases: 85.71428571428571
Instances selection time: 4.0
Test time: 49.8
Accumulative iteration time: 99.8
Weighted Recall: 0.8571428571428571
Weighted FalsePositiveRate: 0.24146825396825394
Kappa statistic: 0.6638269924014957
Training time: 0.4
		
Time end:Tue Oct 31 13.16.10 EET 2017