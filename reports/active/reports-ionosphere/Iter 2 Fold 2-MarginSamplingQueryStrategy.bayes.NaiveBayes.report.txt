Tue Oct 31 13.17.11 EET 2017
Dataset: ionosphere
Test set size: 175
Initial Labelled set size: 35
Initial Unlabelled set size: 141
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Tue Oct 31 13.17.11 EET 2017
		
Iteration: 1
Labeled set size: 35
Unlabelled set size: 141
	
Mean region size: 55.714285714285715
Incorrectly Classified Instances: 11.428571428571429
Correctly Classified Instances: 88.57142857142857
Weighted Precision: 0.8867132867132868
Weighted AreaUnderROC: 0.9403344671201814
Root mean squared error: 0.3105171790090636
Relative absolute error: 22.721862098916407
Root relative squared error: 62.10343580181272
Weighted TruePositiveRate: 0.8857142857142857
Weighted MatthewsCorrelation: 0.7539233048860803
Weighted FMeasure: 0.8860923423423424
Iteration time: 5.0
Weighted AreaUnderPRC: 0.9463549339358772
Mean absolute error: 0.11360931049458203
Coverage of cases: 93.14285714285714
Instances selection time: 5.0
Test time: 5.0
Accumulative iteration time: 5.0
Weighted Recall: 0.8857142857142857
Weighted FalsePositiveRate: 0.12678571428571428
Kappa statistic: 0.753694581280788
Training time: 0.0
		
Iteration: 2
Labeled set size: 55
Unlabelled set size: 121
	
Mean region size: 54.857142857142854
Incorrectly Classified Instances: 8.0
Correctly Classified Instances: 92.0
Weighted Precision: 0.9196778832326719
Weighted AreaUnderROC: 0.9464285714285714
Root mean squared error: 0.2574984792475317
Relative absolute error: 16.606492687181653
Root relative squared error: 51.499695849506345
Weighted TruePositiveRate: 0.92
Weighted MatthewsCorrelation: 0.8254322978015354
Weighted FMeasure: 0.9197088210105624
Iteration time: 4.0
Weighted AreaUnderPRC: 0.9455519386005348
Mean absolute error: 0.08303246343590827
Coverage of cases: 96.0
Instances selection time: 4.0
Test time: 6.0
Accumulative iteration time: 9.0
Weighted Recall: 0.92
Weighted FalsePositiveRate: 0.10055555555555555
Kappa statistic: 0.8251748251748252
Training time: 0.0
		
Iteration: 3
Labeled set size: 75
Unlabelled set size: 101
	
Mean region size: 54.857142857142854
Incorrectly Classified Instances: 6.285714285714286
Correctly Classified Instances: 93.71428571428571
Weighted Precision: 0.9376186265841439
Weighted AreaUnderROC: 0.9617346938775511
Root mean squared error: 0.22985518606234623
Relative absolute error: 14.326879454324743
Root relative squared error: 45.97103721246925
Weighted TruePositiveRate: 0.9371428571428572
Weighted MatthewsCorrelation: 0.8629007718639348
Weighted FMeasure: 0.9365303691941246
Iteration time: 4.0
Weighted AreaUnderPRC: 0.9669643070131092
Mean absolute error: 0.07163439727162371
Coverage of cases: 96.57142857142857
Instances selection time: 3.0
Test time: 6.0
Accumulative iteration time: 13.0
Weighted Recall: 0.9371428571428572
Weighted FalsePositiveRate: 0.0909126984126984
Kappa statistic: 0.8611812216052499
Training time: 1.0
		
Iteration: 4
Labeled set size: 95
Unlabelled set size: 81
	
Mean region size: 55.142857142857146
Incorrectly Classified Instances: 8.571428571428571
Correctly Classified Instances: 91.42857142857143
Weighted Precision: 0.9140451041964031
Weighted AreaUnderROC: 0.9489795918367347
Root mean squared error: 0.26329870674014744
Relative absolute error: 17.504438267191645
Root relative squared error: 52.659741348029485
Weighted TruePositiveRate: 0.9142857142857143
Weighted MatthewsCorrelation: 0.8134029515918056
Weighted FMeasure: 0.9141333333333334
Iteration time: 4.0
Weighted AreaUnderPRC: 0.9563963159719762
Mean absolute error: 0.08752219133595823
Coverage of cases: 96.57142857142857
Instances selection time: 3.0
Test time: 5.0
Accumulative iteration time: 17.0
Weighted Recall: 0.9142857142857143
Weighted FalsePositiveRate: 0.10376984126984126
Kappa statistic: 0.8133399701343952
Training time: 1.0
		
Iteration: 5
Labeled set size: 115
Unlabelled set size: 61
	
Mean region size: 56.0
Incorrectly Classified Instances: 6.857142857142857
Correctly Classified Instances: 93.14285714285714
Weighted Precision: 0.9314285714285714
Weighted AreaUnderROC: 0.9632936507936508
Root mean squared error: 0.2431381149036285
Relative absolute error: 15.80774099142256
Root relative squared error: 48.6276229807257
Weighted TruePositiveRate: 0.9314285714285714
Weighted MatthewsCorrelation: 0.851190476190476
Weighted FMeasure: 0.9314285714285714
Iteration time: 3.0
Weighted AreaUnderPRC: 0.9690089153307078
Mean absolute error: 0.07903870495711281
Coverage of cases: 96.0
Instances selection time: 2.0
Test time: 6.0
Accumulative iteration time: 20.0
Weighted Recall: 0.9314285714285714
Weighted FalsePositiveRate: 0.08023809523809523
Kappa statistic: 0.851190476190476
Training time: 1.0
		
Iteration: 6
Labeled set size: 135
Unlabelled set size: 41
	
Mean region size: 55.714285714285715
Incorrectly Classified Instances: 10.857142857142858
Correctly Classified Instances: 89.14285714285714
Weighted Precision: 0.8965714285714285
Weighted AreaUnderROC: 0.9512471655328799
Root mean squared error: 0.3077368169552515
Relative absolute error: 23.128020874938915
Root relative squared error: 61.547363391050304
Weighted TruePositiveRate: 0.8914285714285715
Weighted MatthewsCorrelation: 0.7727556926637408
Weighted FMeasure: 0.8925345622119816
Iteration time: 3.0
Weighted AreaUnderPRC: 0.9586258288306762
Mean absolute error: 0.11564010437469457
Coverage of cases: 93.14285714285714
Instances selection time: 2.0
Test time: 6.0
Accumulative iteration time: 23.0
Weighted Recall: 0.8914285714285715
Weighted FalsePositiveRate: 0.10273809523809523
Kappa statistic: 0.7699757869249395
Training time: 1.0
		
Iteration: 7
Labeled set size: 155
Unlabelled set size: 21
	
Mean region size: 57.42857142857143
Incorrectly Classified Instances: 13.714285714285714
Correctly Classified Instances: 86.28571428571429
Weighted Precision: 0.8752
Weighted AreaUnderROC: 0.9481292517006802
Root mean squared error: 0.34213562046498464
Relative absolute error: 28.798821432477673
Root relative squared error: 68.42712409299693
Weighted TruePositiveRate: 0.8628571428571429
Weighted MatthewsCorrelation: 0.7216878364870322
Weighted FMeasure: 0.8649384741591468
Iteration time: 3.0
Weighted AreaUnderPRC: 0.9558834624456553
Mean absolute error: 0.14399410716238836
Coverage of cases: 91.42857142857143
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 26.0
Weighted Recall: 0.8628571428571429
Weighted FalsePositiveRate: 0.1188095238095238
Kappa statistic: 0.7142857142857143
Training time: 2.0
		
Iteration: 8
Labeled set size: 175
Unlabelled set size: 1
	
Mean region size: 56.857142857142854
Incorrectly Classified Instances: 17.714285714285715
Correctly Classified Instances: 82.28571428571429
Weighted Precision: 0.8376076555023924
Weighted AreaUnderROC: 0.935374149659864
Root mean squared error: 0.4009330350633211
Relative absolute error: 36.84268254598583
Root relative squared error: 80.18660701266423
Weighted TruePositiveRate: 0.8228571428571428
Weighted MatthewsCorrelation: 0.639835879076119
Weighted FMeasure: 0.82568379419687
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9442509162963691
Mean absolute error: 0.18421341272992914
Coverage of cases: 87.42857142857143
Instances selection time: 0.0
Test time: 6.0
Accumulative iteration time: 28.0
Weighted Recall: 0.8228571428571428
Weighted FalsePositiveRate: 0.16214285714285714
Kappa statistic: 0.6321784527764595
Training time: 2.0
		
Time end:Tue Oct 31 13.17.11 EET 2017