Tue Oct 31 13.17.49 EET 2017
Dataset: ionosphere
Test set size: 175
Initial Labelled set size: 35
Initial Unlabelled set size: 141
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Tue Oct 31 13.17.49 EET 2017
		
Iteration: 1
Labeled set size: 35
Unlabelled set size: 141
	
Mean region size: 89.14285714285714
Incorrectly Classified Instances: 10.285714285714286
Correctly Classified Instances: 89.71428571428571
Weighted Precision: 0.89806993006993
Weighted AreaUnderROC: 0.9541524943310657
Root mean squared error: 0.29674423811953415
Relative absolute error: 44.685714285714276
Root relative squared error: 59.34884762390683
Weighted TruePositiveRate: 0.8971428571428571
Weighted MatthewsCorrelation: 0.7785613213856254
Weighted FMeasure: 0.897483108108108
Iteration time: 5.0
Weighted AreaUnderPRC: 0.9531344598112518
Mean absolute error: 0.2234285714285714
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 5.0
Weighted Recall: 0.8971428571428571
Weighted FalsePositiveRate: 0.11341269841269841
Kappa statistic: 0.7783251231527093
Training time: 5.0
		
Iteration: 2
Labeled set size: 55
Unlabelled set size: 121
	
Mean region size: 86.85714285714286
Incorrectly Classified Instances: 9.142857142857142
Correctly Classified Instances: 90.85714285714286
Weighted Precision: 0.9085714285714286
Weighted AreaUnderROC: 0.9516014739229026
Root mean squared error: 0.28364968333693374
Relative absolute error: 39.085714285714296
Root relative squared error: 56.72993666738675
Weighted TruePositiveRate: 0.9085714285714286
Weighted MatthewsCorrelation: 0.8015873015873015
Weighted FMeasure: 0.9085714285714286
Iteration time: 6.0
Weighted AreaUnderPRC: 0.9436526526362548
Mean absolute error: 0.19542857142857148
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 11.0
Weighted Recall: 0.9085714285714286
Weighted FalsePositiveRate: 0.10698412698412697
Kappa statistic: 0.8015873015873016
Training time: 6.0
		
Iteration: 3
Labeled set size: 75
Unlabelled set size: 101
	
Mean region size: 82.0
Incorrectly Classified Instances: 8.0
Correctly Classified Instances: 92.0
Weighted Precision: 0.9198363530099357
Weighted AreaUnderROC: 0.9673327664399093
Root mean squared error: 0.2771281292110202
Relative absolute error: 35.8857142857143
Root relative squared error: 55.42562584220404
Weighted TruePositiveRate: 0.92
Weighted MatthewsCorrelation: 0.8249880451819662
Weighted FMeasure: 0.9193902789761289
Iteration time: 9.0
Weighted AreaUnderPRC: 0.956767569555829
Mean absolute error: 0.1794285714285715
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 20.0
Weighted Recall: 0.92
Weighted FalsePositiveRate: 0.1075
Kappa statistic: 0.8239436619718311
Training time: 8.0
		
Iteration: 4
Labeled set size: 95
Unlabelled set size: 81
	
Mean region size: 84.28571428571429
Incorrectly Classified Instances: 9.142857142857142
Correctly Classified Instances: 90.85714285714286
Weighted Precision: 0.9107131011608623
Weighted AreaUnderROC: 0.9608843537414966
Root mean squared error: 0.27599171830224994
Relative absolute error: 36.68571428571429
Root relative squared error: 55.198343660449986
Weighted TruePositiveRate: 0.9085714285714286
Weighted MatthewsCorrelation: 0.805269578105233
Weighted FMeasure: 0.9091468531468531
Iteration time: 11.0
Weighted AreaUnderPRC: 0.9536217863370734
Mean absolute error: 0.18342857142857147
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 31.0
Weighted Recall: 0.9085714285714286
Weighted FalsePositiveRate: 0.09309523809523808
Kappa statistic: 0.8043052837573386
Training time: 10.0
		
Iteration: 5
Labeled set size: 115
Unlabelled set size: 61
	
Mean region size: 80.0
Incorrectly Classified Instances: 8.0
Correctly Classified Instances: 92.0
Weighted Precision: 0.9198363530099357
Weighted AreaUnderROC: 0.9624433106575964
Root mean squared error: 0.26262412031538246
Relative absolute error: 31.88571428571428
Root relative squared error: 52.524824063076494
Weighted TruePositiveRate: 0.92
Weighted MatthewsCorrelation: 0.8249880451819662
Weighted FMeasure: 0.9193902789761289
Iteration time: 13.0
Weighted AreaUnderPRC: 0.95394855927439
Mean absolute error: 0.1594285714285714
Coverage of cases: 99.42857142857143
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 44.0
Weighted Recall: 0.92
Weighted FalsePositiveRate: 0.1075
Kappa statistic: 0.8239436619718311
Training time: 12.0
		
Iteration: 6
Labeled set size: 135
Unlabelled set size: 41
	
Mean region size: 78.28571428571429
Incorrectly Classified Instances: 6.857142857142857
Correctly Classified Instances: 93.14285714285714
Weighted Precision: 0.9314285714285714
Weighted AreaUnderROC: 0.9799461451247166
Root mean squared error: 0.23250192011495902
Relative absolute error: 27.428571428571434
Root relative squared error: 46.5003840229918
Weighted TruePositiveRate: 0.9314285714285714
Weighted MatthewsCorrelation: 0.851190476190476
Weighted FMeasure: 0.9314285714285714
Iteration time: 15.0
Weighted AreaUnderPRC: 0.9771198552451967
Mean absolute error: 0.13714285714285718
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 59.0
Weighted Recall: 0.9314285714285714
Weighted FalsePositiveRate: 0.08023809523809523
Kappa statistic: 0.851190476190476
Training time: 15.0
		
Iteration: 7
Labeled set size: 155
Unlabelled set size: 21
	
Mean region size: 74.0
Incorrectly Classified Instances: 6.285714285714286
Correctly Classified Instances: 93.71428571428571
Weighted Precision: 0.9376186265841439
Weighted AreaUnderROC: 0.966907596371882
Root mean squared error: 0.23700512351303407
Relative absolute error: 25.71428571428572
Root relative squared error: 47.40102470260681
Weighted TruePositiveRate: 0.9371428571428572
Weighted MatthewsCorrelation: 0.8629007718639348
Weighted FMeasure: 0.9365303691941246
Iteration time: 18.0
Weighted AreaUnderPRC: 0.9642010845547
Mean absolute error: 0.1285714285714286
Coverage of cases: 98.85714285714286
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 77.0
Weighted Recall: 0.9371428571428572
Weighted FalsePositiveRate: 0.0909126984126984
Kappa statistic: 0.8611812216052499
Training time: 17.0
		
Iteration: 8
Labeled set size: 175
Unlabelled set size: 1
	
Mean region size: 77.42857142857143
Incorrectly Classified Instances: 6.857142857142857
Correctly Classified Instances: 93.14285714285714
Weighted Precision: 0.9314285714285714
Weighted AreaUnderROC: 0.9781746031746031
Root mean squared error: 0.2333605426312315
Relative absolute error: 26.628571428571433
Root relative squared error: 46.6721085262463
Weighted TruePositiveRate: 0.9314285714285714
Weighted MatthewsCorrelation: 0.851190476190476
Weighted FMeasure: 0.9314285714285714
Iteration time: 21.0
Weighted AreaUnderPRC: 0.9751496729512087
Mean absolute error: 0.13314285714285717
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 98.0
Weighted Recall: 0.9314285714285714
Weighted FalsePositiveRate: 0.08023809523809523
Kappa statistic: 0.851190476190476
Training time: 20.0
		
Time end:Tue Oct 31 13.17.50 EET 2017