Tue Oct 31 13.16.45 EET 2017
Dataset: ionosphere
Test set size: 176
Initial Labelled set size: 35
Initial Unlabelled set size: 140
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Tue Oct 31 13.16.45 EET 2017
		
Iteration: 1
Labeled set size: 35
Unlabelled set size: 140
	
Mean region size: 87.7840909090909
Incorrectly Classified Instances: 17.613636363636363
Correctly Classified Instances: 82.38636363636364
Weighted Precision: 0.8414703383953956
Weighted AreaUnderROC: 0.8835510605422111
Root mean squared error: 0.3809378379258698
Relative absolute error: 53.636363636363626
Root relative squared error: 76.18756758517397
Weighted TruePositiveRate: 0.8238636363636364
Weighted MatthewsCorrelation: 0.6163671404436651
Weighted FMeasure: 0.8108363127149367
Iteration time: 4.0
Weighted AreaUnderPRC: 0.8712967087117979
Mean absolute error: 0.26818181818181813
Coverage of cases: 98.29545454545455
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 4.0
Weighted Recall: 0.8238636363636364
Weighted FalsePositiveRate: 0.29485675337445244
Kappa statistic: 0.5799199260856175
Training time: 3.0
		
Iteration: 2
Labeled set size: 55
Unlabelled set size: 120
	
Mean region size: 81.81818181818181
Incorrectly Classified Instances: 15.909090909090908
Correctly Classified Instances: 84.0909090909091
Weighted Precision: 0.8411229946524064
Weighted AreaUnderROC: 0.8877651355527462
Root mean squared error: 0.35419563161414824
Relative absolute error: 47.04545454545453
Root relative squared error: 70.83912632282964
Weighted TruePositiveRate: 0.8409090909090909
Weighted MatthewsCorrelation: 0.6464554011142059
Weighted FMeasure: 0.8365465803546299
Iteration time: 5.0
Weighted AreaUnderPRC: 0.8772756565262431
Mean absolute error: 0.23522727272727267
Coverage of cases: 97.72727272727273
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 9.0
Weighted Recall: 0.8409090909090909
Weighted FalsePositiveRate: 0.2291658685463995
Kappa statistic: 0.6386566945299897
Training time: 5.0
		
Iteration: 3
Labeled set size: 75
Unlabelled set size: 100
	
Mean region size: 82.95454545454545
Incorrectly Classified Instances: 17.045454545454547
Correctly Classified Instances: 82.95454545454545
Weighted Precision: 0.8346460791117281
Weighted AreaUnderROC: 0.8833403567916842
Root mean squared error: 0.3647072300302855
Relative absolute error: 49.20454545454543
Root relative squared error: 72.94144600605709
Weighted TruePositiveRate: 0.8295454545454546
Weighted MatthewsCorrelation: 0.6219362435385224
Weighted FMeasure: 0.8216281669150521
Iteration time: 8.0
Weighted AreaUnderPRC: 0.8734471813288276
Mean absolute error: 0.24602272727272717
Coverage of cases: 96.5909090909091
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 17.0
Weighted Recall: 0.8295454545454546
Weighted FalsePositiveRate: 0.26359518063057885
Kappa statistic: 0.6041385515069726
Training time: 7.0
		
Iteration: 4
Labeled set size: 95
Unlabelled set size: 80
	
Mean region size: 76.98863636363636
Incorrectly Classified Instances: 22.15909090909091
Correctly Classified Instances: 77.8409090909091
Weighted Precision: 0.7992029671717172
Weighted AreaUnderROC: 0.8960528164067987
Root mean squared error: 0.3900029137420292
Relative absolute error: 50.11363636363636
Root relative squared error: 78.00058274840585
Weighted TruePositiveRate: 0.7784090909090909
Weighted MatthewsCorrelation: 0.5084235268782055
Weighted FMeasure: 0.7556192169493418
Iteration time: 8.0
Weighted AreaUnderPRC: 0.8824150100425832
Mean absolute error: 0.2505681818181818
Coverage of cases: 97.1590909090909
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 25.0
Weighted Recall: 0.7784090909090909
Weighted FalsePositiveRate: 0.36936287655314204
Kappa statistic: 0.45901639344262296
Training time: 8.0
		
Iteration: 5
Labeled set size: 115
Unlabelled set size: 60
	
Mean region size: 73.29545454545455
Incorrectly Classified Instances: 12.5
Correctly Classified Instances: 87.5
Weighted Precision: 0.8902998776009792
Weighted AreaUnderROC: 0.9419159994381233
Root mean squared error: 0.3187332883319674
Relative absolute error: 34.999999999999986
Root relative squared error: 63.74665766639348
Weighted TruePositiveRate: 0.875
Weighted MatthewsCorrelation: 0.7339299540829789
Weighted FMeasure: 0.8682888479828196
Iteration time: 11.0
Weighted AreaUnderPRC: 0.9355043217012523
Mean absolute error: 0.17499999999999993
Coverage of cases: 97.1590909090909
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 36.0
Weighted Recall: 0.875
Weighted FalsePositiveRate: 0.2171828908554572
Kappa statistic: 0.707508687112857
Training time: 10.0
		
Iteration: 6
Labeled set size: 135
Unlabelled set size: 40
	
Mean region size: 73.29545454545455
Incorrectly Classified Instances: 9.090909090909092
Correctly Classified Instances: 90.9090909090909
Weighted Precision: 0.914053475935829
Weighted AreaUnderROC: 0.9541368169686755
Root mean squared error: 0.2767506261797958
Relative absolute error: 28.636363636363633
Root relative squared error: 55.350125235959155
Weighted TruePositiveRate: 0.9090909090909091
Weighted MatthewsCorrelation: 0.803207847400452
Weighted FMeasure: 0.9065980459169314
Iteration time: 16.0
Weighted AreaUnderPRC: 0.9487955361538177
Mean absolute error: 0.14318181818181816
Coverage of cases: 98.29545454545455
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 52.0
Weighted Recall: 0.9090909090909091
Weighted FalsePositiveRate: 0.1490122463573791
Kappa statistic: 0.793518111159994
Training time: 15.0
		
Iteration: 7
Labeled set size: 155
Unlabelled set size: 20
	
Mean region size: 71.875
Incorrectly Classified Instances: 7.386363636363637
Correctly Classified Instances: 92.61363636363636
Weighted Precision: 0.9288530799801292
Weighted AreaUnderROC: 0.9411434190195253
Root mean squared error: 0.26777704430093596
Relative absolute error: 27.27272727272726
Root relative squared error: 53.55540886018719
Weighted TruePositiveRate: 0.9261363636363636
Weighted MatthewsCorrelation: 0.8396176373487361
Weighted FMeasure: 0.9247098646034815
Iteration time: 18.0
Weighted AreaUnderPRC: 0.9349307133351132
Mean absolute error: 0.1363636363636363
Coverage of cases: 97.72727272727273
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 70.0
Weighted Recall: 0.9261363636363636
Weighted FalsePositiveRate: 0.11843865328378601
Kappa statistic: 0.8340586016826226
Training time: 18.0
		
Time end:Tue Oct 31 13.16.46 EET 2017