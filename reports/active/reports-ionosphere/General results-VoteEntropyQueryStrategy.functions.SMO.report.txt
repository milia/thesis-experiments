Tue Oct 31 13.17.55 EET 2017
Dataset: ionosphere
Test set size: 175
Initial Labelled set size: 35
Initial Unlabelled set size: 140
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Tue Oct 31 13.17.55 EET 2017
		
Iteration: 1
Labeled set size: 35
Unlabelled set size: 140
	
Mean region size: 50.0
Incorrectly Classified Instances: 18.178896103896108
Correctly Classified Instances: 81.8211038961039
Weighted Precision: 0.8484183708777773
Weighted AreaUnderROC: 0.7544919405815425
Root mean squared error: 0.42385782700440267
Relative absolute error: 36.357792207792215
Root relative squared error: 84.77156540088052
Weighted TruePositiveRate: 0.818211038961039
Weighted MatthewsCorrelation: 0.6098121543357212
Weighted FMeasure: 0.7989621475191562
Iteration time: 9.3
Weighted AreaUnderPRC: 0.7441556498238866
Mean absolute error: 0.18178896103896103
Coverage of cases: 81.8211038961039
Instances selection time: 1.4
Test time: 1.2
Accumulative iteration time: 9.3
Weighted Recall: 0.818211038961039
Weighted FalsePositiveRate: 0.30922715779795423
Kappa statistic: 0.5584191583817755
Training time: 7.9
		
Iteration: 2
Labeled set size: 55
Unlabelled set size: 120
	
Mean region size: 50.0
Incorrectly Classified Instances: 17.321753246753246
Correctly Classified Instances: 82.67824675324675
Weighted Precision: 0.8509621033997593
Weighted AreaUnderROC: 0.7737278761061945
Root mean squared error: 0.4127443380473066
Relative absolute error: 34.64350649350649
Root relative squared error: 82.54886760946133
Weighted TruePositiveRate: 0.8267824675324675
Weighted MatthewsCorrelation: 0.627876951022437
Weighted FMeasure: 0.8116353093207916
Iteration time: 11.6
Weighted AreaUnderPRC: 0.7584529395371711
Mean absolute error: 0.1732175324675325
Coverage of cases: 82.67824675324675
Instances selection time: 0.8
Test time: 1.2
Accumulative iteration time: 20.9
Weighted Recall: 0.8267824675324675
Weighted FalsePositiveRate: 0.27932671532007813
Kappa statistic: 0.58678482658428
Training time: 10.8
		
Iteration: 3
Labeled set size: 75
Unlabelled set size: 100
	
Mean region size: 50.0
Incorrectly Classified Instances: 19.48538961038961
Correctly Classified Instances: 80.5146103896104
Weighted Precision: 0.8371761953211247
Weighted AreaUnderROC: 0.7516303202697008
Root mean squared error: 0.43958996580728826
Relative absolute error: 38.97077922077922
Root relative squared error: 87.91799316145764
Weighted TruePositiveRate: 0.8051461038961041
Weighted MatthewsCorrelation: 0.5859363086316054
Weighted FMeasure: 0.7870892592959464
Iteration time: 9.2
Weighted AreaUnderPRC: 0.7359053586705071
Mean absolute error: 0.1948538961038961
Coverage of cases: 80.5146103896104
Instances selection time: 0.8
Test time: 1.1
Accumulative iteration time: 30.1
Weighted Recall: 0.8051461038961041
Weighted FalsePositiveRate: 0.3018854633567023
Kappa statistic: 0.5360611253522928
Training time: 8.4
		
Iteration: 4
Labeled set size: 95
Unlabelled set size: 80
	
Mean region size: 50.0
Incorrectly Classified Instances: 19.485064935064937
Correctly Classified Instances: 80.51493506493506
Weighted Precision: 0.8398940119483335
Weighted AreaUnderROC: 0.7439440230369434
Root mean squared error: 0.43784157605756724
Relative absolute error: 38.97012987012988
Root relative squared error: 87.56831521151344
Weighted TruePositiveRate: 0.8051493506493508
Weighted MatthewsCorrelation: 0.5818576213940855
Weighted FMeasure: 0.7825523386183776
Iteration time: 8.9
Weighted AreaUnderPRC: 0.7323276758100677
Mean absolute error: 0.19485064935064933
Coverage of cases: 80.51493506493506
Instances selection time: 0.6
Test time: 0.9
Accumulative iteration time: 39.0
Weighted Recall: 0.8051493506493508
Weighted FalsePositiveRate: 0.31726130457546386
Kappa statistic: 0.5260805608219656
Training time: 8.3
		
Iteration: 5
Labeled set size: 115
Unlabelled set size: 60
	
Mean region size: 50.0
Incorrectly Classified Instances: 19.655519480519477
Correctly Classified Instances: 80.34448051948053
Weighted Precision: 0.8413402632817896
Weighted AreaUnderROC: 0.7370294282904903
Root mean squared error: 0.43837888498794547
Relative absolute error: 39.31103896103896
Root relative squared error: 87.6757769975891
Weighted TruePositiveRate: 0.8034448051948052
Weighted MatthewsCorrelation: 0.5767252884498028
Weighted FMeasure: 0.7775277405976173
Iteration time: 9.9
Weighted AreaUnderPRC: 0.7286711598425837
Mean absolute error: 0.19655519480519482
Coverage of cases: 80.34448051948053
Instances selection time: 0.5
Test time: 1.0
Accumulative iteration time: 48.9
Weighted Recall: 0.8034448051948052
Weighted FalsePositiveRate: 0.32938594861382475
Kappa statistic: 0.5157257180061509
Training time: 9.4
		
Iteration: 6
Labeled set size: 135
Unlabelled set size: 40
	
Mean region size: 50.0
Incorrectly Classified Instances: 16.69448051948052
Correctly Classified Instances: 83.30551948051948
Weighted Precision: 0.8606150728880733
Weighted AreaUnderROC: 0.7751545160837197
Root mean squared error: 0.4044889776104915
Relative absolute error: 33.38896103896104
Root relative squared error: 80.89779552209833
Weighted TruePositiveRate: 0.8330551948051947
Weighted MatthewsCorrelation: 0.6433137543249254
Weighted FMeasure: 0.8168112640626577
Iteration time: 13.0
Weighted AreaUnderPRC: 0.763847696559032
Mean absolute error: 0.1669448051948052
Coverage of cases: 83.30551948051948
Instances selection time: 0.5
Test time: 1.3
Accumulative iteration time: 61.9
Weighted Recall: 0.8330551948051947
Weighted FalsePositiveRate: 0.28274616263775554
Kappa statistic: 0.5972870691676576
Training time: 12.5
		
Iteration: 7
Labeled set size: 155
Unlabelled set size: 20
	
Mean region size: 50.0
Incorrectly Classified Instances: 13.73279220779221
Correctly Classified Instances: 86.26720779220778
Weighted Precision: 0.8763952513034565
Weighted AreaUnderROC: 0.8174660240202275
Root mean squared error: 0.3677919330565779
Relative absolute error: 27.46558441558442
Root relative squared error: 73.5583866113156
Weighted TruePositiveRate: 0.8626720779220779
Weighted MatthewsCorrelation: 0.7041606605067611
Weighted FMeasure: 0.8547458964900126
Iteration time: 11.9
Weighted AreaUnderPRC: 0.8012219213054708
Mean absolute error: 0.13732792207792208
Coverage of cases: 86.26720779220778
Instances selection time: 0.6
Test time: 1.0
Accumulative iteration time: 73.8
Weighted Recall: 0.8626720779220779
Weighted FalsePositiveRate: 0.22774002988162279
Kappa statistic: 0.6786611764554273
Training time: 11.3
		
Iteration: 8
Labeled set size: 175
Unlabelled set size: 1
	
Mean region size: 50.0
Incorrectly Classified Instances: 11.657142857142857
Correctly Classified Instances: 88.34285714285714
Weighted Precision: 0.8926279668506529
Weighted AreaUnderROC: 0.8457341269841269
Root mean squared error: 0.3398243096583946
Relative absolute error: 23.314285714285713
Root relative squared error: 67.96486193167893
Weighted TruePositiveRate: 0.8834285714285715
Weighted MatthewsCorrelation: 0.7492216416543103
Weighted FMeasure: 0.878708193720166
Iteration time: 13.2
Weighted AreaUnderPRC: 0.8284999956811685
Mean absolute error: 0.11657142857142858
Coverage of cases: 88.34285714285714
Instances selection time: 0.4
Test time: 1.0
Accumulative iteration time: 84.6
Weighted Recall: 0.8834285714285715
Weighted FalsePositiveRate: 0.19196031746031744
Kappa statistic: 0.7319196723754667
Training time: 12.8
		
Time end:Tue Oct 31 13.17.58 EET 2017