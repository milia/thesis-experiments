Tue Oct 31 13.17.32 EET 2017
Dataset: ionosphere
Test set size: 175
Initial Labelled set size: 35
Initial Unlabelled set size: 141
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Tue Oct 31 13.17.32 EET 2017
		
Iteration: 1
Labeled set size: 35
Unlabelled set size: 141
	
Mean region size: 53.42857142857143
Incorrectly Classified Instances: 8.0
Correctly Classified Instances: 92.0
Weighted Precision: 0.92049955396967
Weighted AreaUnderROC: 0.9379251700680272
Root mean squared error: 0.27548698609609024
Relative absolute error: 17.50671998194702
Root relative squared error: 55.097397219218045
Weighted TruePositiveRate: 0.92
Weighted MatthewsCorrelation: 0.8250805543510701
Weighted FMeasure: 0.9190434782608697
Iteration time: 7.0
Weighted AreaUnderPRC: 0.9454892493196969
Mean absolute error: 0.0875335999097351
Coverage of cases: 93.71428571428571
Instances selection time: 6.0
Test time: 18.0
Accumulative iteration time: 7.0
Weighted Recall: 0.92
Weighted FalsePositiveRate: 0.11444444444444445
Kappa statistic: 0.822695035460993
Training time: 1.0
		
Iteration: 2
Labeled set size: 55
Unlabelled set size: 121
	
Mean region size: 53.42857142857143
Incorrectly Classified Instances: 8.571428571428571
Correctly Classified Instances: 91.42857142857143
Weighted Precision: 0.9139130434782609
Weighted AreaUnderROC: 0.9498299319727892
Root mean squared error: 0.2778185061588364
Relative absolute error: 17.924788461709504
Root relative squared error: 55.56370123176728
Weighted TruePositiveRate: 0.9142857142857143
Weighted MatthewsCorrelation: 0.812604508328942
Weighted FMeasure: 0.9138068120769312
Iteration time: 8.0
Weighted AreaUnderPRC: 0.953023441143882
Mean absolute error: 0.08962394230854752
Coverage of cases: 94.28571428571429
Instances selection time: 3.0
Test time: 29.0
Accumulative iteration time: 15.0
Weighted Recall: 0.9142857142857143
Weighted FalsePositiveRate: 0.11071428571428571
Kappa statistic: 0.8120300751879699
Training time: 5.0
		
Iteration: 3
Labeled set size: 75
Unlabelled set size: 101
	
Mean region size: 53.142857142857146
Incorrectly Classified Instances: 9.142857142857142
Correctly Classified Instances: 90.85714285714286
Weighted Precision: 0.9081622088006903
Weighted AreaUnderROC: 0.9451530612244898
Root mean squared error: 0.29519510672435634
Relative absolute error: 19.720675711534973
Root relative squared error: 59.039021344871266
Weighted TruePositiveRate: 0.9085714285714286
Weighted MatthewsCorrelation: 0.8004494800714647
Weighted FMeasure: 0.9082386525835
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9522889349902042
Mean absolute error: 0.09860337855767487
Coverage of cases: 92.57142857142857
Instances selection time: 1.0
Test time: 11.0
Accumulative iteration time: 17.0
Weighted Recall: 0.9085714285714286
Weighted FalsePositiveRate: 0.11392857142857143
Kappa statistic: 0.8001998001998002
Training time: 1.0
		
Iteration: 4
Labeled set size: 95
Unlabelled set size: 81
	
Mean region size: 53.42857142857143
Incorrectly Classified Instances: 9.142857142857142
Correctly Classified Instances: 90.85714285714286
Weighted Precision: 0.9085714285714286
Weighted AreaUnderROC: 0.9526643990929705
Root mean squared error: 0.28408377259683437
Relative absolute error: 18.13930704563785
Root relative squared error: 56.816754519366874
Weighted TruePositiveRate: 0.9085714285714286
Weighted MatthewsCorrelation: 0.8015873015873015
Weighted FMeasure: 0.9085714285714286
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9591609256582114
Mean absolute error: 0.09069653522818925
Coverage of cases: 93.71428571428571
Instances selection time: 1.0
Test time: 7.0
Accumulative iteration time: 19.0
Weighted Recall: 0.9085714285714286
Weighted FalsePositiveRate: 0.10698412698412697
Kappa statistic: 0.8015873015873016
Training time: 1.0
		
Iteration: 5
Labeled set size: 115
Unlabelled set size: 61
	
Mean region size: 53.42857142857143
Incorrectly Classified Instances: 9.142857142857142
Correctly Classified Instances: 90.85714285714286
Weighted Precision: 0.9085714285714286
Weighted AreaUnderROC: 0.94968820861678
Root mean squared error: 0.2947162891892347
Relative absolute error: 19.63942953185406
Root relative squared error: 58.94325783784694
Weighted TruePositiveRate: 0.9085714285714286
Weighted MatthewsCorrelation: 0.8015873015873015
Weighted FMeasure: 0.9085714285714286
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9563118360808899
Mean absolute error: 0.09819714765927029
Coverage of cases: 92.57142857142857
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 21.0
Weighted Recall: 0.9085714285714286
Weighted FalsePositiveRate: 0.10698412698412697
Kappa statistic: 0.8015873015873016
Training time: 1.0
		
Iteration: 6
Labeled set size: 135
Unlabelled set size: 41
	
Mean region size: 53.42857142857143
Incorrectly Classified Instances: 9.142857142857142
Correctly Classified Instances: 90.85714285714286
Weighted Precision: 0.9085714285714286
Weighted AreaUnderROC: 0.9513888888888888
Root mean squared error: 0.2909163623773344
Relative absolute error: 19.696085417805644
Root relative squared error: 58.183272475466886
Weighted TruePositiveRate: 0.9085714285714286
Weighted MatthewsCorrelation: 0.8015873015873015
Weighted FMeasure: 0.9085714285714286
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9581078183438324
Mean absolute error: 0.09848042708902822
Coverage of cases: 93.14285714285714
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 23.0
Weighted Recall: 0.9085714285714286
Weighted FalsePositiveRate: 0.10698412698412697
Kappa statistic: 0.8015873015873016
Training time: 1.0
		
Iteration: 7
Labeled set size: 155
Unlabelled set size: 21
	
Mean region size: 54.857142857142854
Incorrectly Classified Instances: 9.714285714285714
Correctly Classified Instances: 90.28571428571429
Weighted Precision: 0.9032488738738739
Weighted AreaUnderROC: 0.947987528344671
Root mean squared error: 0.30009281629954293
Relative absolute error: 21.02254088020641
Root relative squared error: 60.01856325990859
Weighted TruePositiveRate: 0.9028571428571428
Weighted MatthewsCorrelation: 0.7899762902148404
Weighted FMeasure: 0.9030217859538857
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9545317162858181
Mean absolute error: 0.10511270440103206
Coverage of cases: 92.57142857142857
Instances selection time: 0.0
Test time: 6.0
Accumulative iteration time: 25.0
Weighted Recall: 0.9028571428571428
Weighted FalsePositiveRate: 0.1101984126984127
Kappa statistic: 0.7899159663865545
Training time: 2.0
		
Iteration: 8
Labeled set size: 175
Unlabelled set size: 1
	
Mean region size: 53.714285714285715
Incorrectly Classified Instances: 9.142857142857142
Correctly Classified Instances: 90.85714285714286
Weighted Precision: 0.9085714285714286
Weighted AreaUnderROC: 0.9513888888888888
Root mean squared error: 0.291381640383731
Relative absolute error: 19.334809871764698
Root relative squared error: 58.276328076746196
Weighted TruePositiveRate: 0.9085714285714286
Weighted MatthewsCorrelation: 0.8015873015873015
Weighted FMeasure: 0.9085714285714286
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9577084033109139
Mean absolute error: 0.0966740493588235
Coverage of cases: 92.57142857142857
Instances selection time: 0.0
Test time: 5.0
Accumulative iteration time: 27.0
Weighted Recall: 0.9085714285714286
Weighted FalsePositiveRate: 0.10698412698412697
Kappa statistic: 0.8015873015873016
Training time: 2.0
		
Time end:Tue Oct 31 13.17.33 EET 2017