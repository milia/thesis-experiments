Tue Oct 31 13.17.55 EET 2017
Dataset: ionosphere
Test set size: 175
Initial Labelled set size: 35
Initial Unlabelled set size: 141
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Tue Oct 31 13.17.55 EET 2017
		
Iteration: 1
Labeled set size: 35
Unlabelled set size: 141
	
Mean region size: 50.0
Incorrectly Classified Instances: 15.428571428571429
Correctly Classified Instances: 84.57142857142857
Weighted Precision: 0.858796992481203
Weighted AreaUnderROC: 0.7961309523809523
Root mean squared error: 0.3927922024247863
Relative absolute error: 30.857142857142854
Root relative squared error: 78.55844048495726
Weighted TruePositiveRate: 0.8457142857142858
Weighted MatthewsCorrelation: 0.6656449160884182
Weighted FMeasure: 0.8368979591836735
Iteration time: 8.0
Weighted AreaUnderPRC: 0.7777435016111708
Mean absolute error: 0.15428571428571428
Coverage of cases: 84.57142857142857
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 8.0
Weighted Recall: 0.8457142857142858
Weighted FalsePositiveRate: 0.25345238095238093
Kappa statistic: 0.6388443017656502
Training time: 7.0
		
Iteration: 2
Labeled set size: 55
Unlabelled set size: 121
	
Mean region size: 50.0
Incorrectly Classified Instances: 17.142857142857142
Correctly Classified Instances: 82.85714285714286
Weighted Precision: 0.8268848626534191
Weighted AreaUnderROC: 0.8070436507936508
Root mean squared error: 0.4140393356054125
Relative absolute error: 34.285714285714285
Root relative squared error: 82.8078671210825
Weighted TruePositiveRate: 0.8285714285714286
Weighted MatthewsCorrelation: 0.623525763086248
Weighted FMeasure: 0.827264883520276
Iteration time: 21.0
Weighted AreaUnderPRC: 0.7702616348000333
Mean absolute error: 0.17142857142857143
Coverage of cases: 82.85714285714286
Instances selection time: 1.0
Test time: 0.0
Accumulative iteration time: 29.0
Weighted Recall: 0.8285714285714286
Weighted FalsePositiveRate: 0.21448412698412697
Kappa statistic: 0.6227364185110665
Training time: 20.0
		
Iteration: 3
Labeled set size: 75
Unlabelled set size: 101
	
Mean region size: 50.0
Incorrectly Classified Instances: 18.857142857142858
Correctly Classified Instances: 81.14285714285714
Weighted Precision: 0.813744787322769
Weighted AreaUnderROC: 0.8005952380952382
Root mean squared error: 0.43424811867344754
Relative absolute error: 37.714285714285715
Root relative squared error: 86.8496237346895
Weighted TruePositiveRate: 0.8114285714285714
Weighted MatthewsCorrelation: 0.5953963410126891
Weighted FMeasure: 0.8123413658844575
Iteration time: 9.0
Weighted AreaUnderPRC: 0.7593903491004409
Mean absolute error: 0.18857142857142858
Coverage of cases: 81.14285714285714
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 38.0
Weighted Recall: 0.8114285714285714
Weighted FalsePositiveRate: 0.21023809523809522
Kappa statistic: 0.5949926362297495
Training time: 8.0
		
Iteration: 4
Labeled set size: 95
Unlabelled set size: 81
	
Mean region size: 50.0
Incorrectly Classified Instances: 15.428571428571429
Correctly Classified Instances: 84.57142857142857
Weighted Precision: 0.8442857142857143
Weighted AreaUnderROC: 0.8204365079365079
Root mean squared error: 0.3927922024247863
Relative absolute error: 30.857142857142854
Root relative squared error: 78.55844048495726
Weighted TruePositiveRate: 0.8457142857142858
Weighted MatthewsCorrelation: 0.659452892869409
Weighted FMeasure: 0.8435141329258977
Iteration time: 9.0
Weighted AreaUnderPRC: 0.7870530612244897
Mean absolute error: 0.15428571428571428
Coverage of cases: 84.57142857142857
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 47.0
Weighted Recall: 0.8457142857142858
Weighted FalsePositiveRate: 0.20484126984126982
Kappa statistic: 0.6568378240976107
Training time: 9.0
		
Iteration: 5
Labeled set size: 115
Unlabelled set size: 61
	
Mean region size: 50.0
Incorrectly Classified Instances: 13.714285714285714
Correctly Classified Instances: 86.28571428571429
Weighted Precision: 0.8647438330170779
Weighted AreaUnderROC: 0.8303571428571429
Root mean squared error: 0.3703280399090206
Relative absolute error: 27.42857142857143
Root relative squared error: 74.06560798180412
Weighted TruePositiveRate: 0.8628571428571429
Weighted MatthewsCorrelation: 0.6979061935736514
Weighted FMeasure: 0.8591257805530775
Iteration time: 10.0
Weighted AreaUnderPRC: 0.8036502033071294
Mean absolute error: 0.13714285714285715
Coverage of cases: 86.28571428571429
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 57.0
Weighted Recall: 0.8628571428571429
Weighted FalsePositiveRate: 0.20214285714285715
Kappa statistic: 0.68944099378882
Training time: 9.0
		
Iteration: 6
Labeled set size: 135
Unlabelled set size: 41
	
Mean region size: 50.0
Incorrectly Classified Instances: 13.714285714285714
Correctly Classified Instances: 86.28571428571429
Weighted Precision: 0.8667573696145126
Weighted AreaUnderROC: 0.8268849206349205
Root mean squared error: 0.3703280399090206
Relative absolute error: 27.42857142857143
Root relative squared error: 74.06560798180412
Weighted TruePositiveRate: 0.8628571428571429
Weighted MatthewsCorrelation: 0.6989093588106654
Weighted FMeasure: 0.8583193277310923
Iteration time: 12.0
Weighted AreaUnderPRC: 0.8023730482669258
Mean absolute error: 0.13714285714285715
Coverage of cases: 86.28571428571429
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 69.0
Weighted Recall: 0.8628571428571429
Weighted FalsePositiveRate: 0.2090873015873016
Kappa statistic: 0.6871741397288843
Training time: 11.0
		
Iteration: 7
Labeled set size: 155
Unlabelled set size: 21
	
Mean region size: 50.0
Incorrectly Classified Instances: 14.285714285714286
Correctly Classified Instances: 85.71428571428571
Weighted Precision: 0.8617125984251967
Weighted AreaUnderROC: 0.8189484126984128
Root mean squared error: 0.3779644730092272
Relative absolute error: 28.57142857142857
Root relative squared error: 75.59289460184544
Weighted TruePositiveRate: 0.8571428571428571
Weighted MatthewsCorrelation: 0.6862893314354459
Weighted FMeasure: 0.8519733122243581
Iteration time: 11.0
Weighted AreaUnderPRC: 0.7946882264716909
Mean absolute error: 0.14285714285714285
Coverage of cases: 85.71428571428571
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 80.0
Weighted Recall: 0.8571428571428571
Weighted FalsePositiveRate: 0.21924603174603174
Kappa statistic: 0.67294610151753
Training time: 11.0
		
Iteration: 8
Labeled set size: 175
Unlabelled set size: 1
	
Mean region size: 50.0
Incorrectly Classified Instances: 15.428571428571429
Correctly Classified Instances: 84.57142857142857
Weighted Precision: 0.8549063150589868
Weighted AreaUnderROC: 0.7996031746031745
Root mean squared error: 0.3927922024247863
Relative absolute error: 30.857142857142854
Root relative squared error: 78.55844048495726
Weighted TruePositiveRate: 0.8457142857142858
Weighted MatthewsCorrelation: 0.6629697402987389
Weighted FMeasure: 0.8380477673935617
Iteration time: 13.0
Weighted AreaUnderPRC: 0.7785245960146724
Mean absolute error: 0.15428571428571428
Coverage of cases: 84.57142857142857
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 93.0
Weighted Recall: 0.8457142857142858
Weighted FalsePositiveRate: 0.24650793650793648
Kappa statistic: 0.641529474243229
Training time: 12.0
		
Time end:Tue Oct 31 13.17.55 EET 2017