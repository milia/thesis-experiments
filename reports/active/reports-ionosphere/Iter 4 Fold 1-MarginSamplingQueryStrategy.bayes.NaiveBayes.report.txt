Tue Oct 31 13.17.12 EET 2017
Dataset: ionosphere
Test set size: 176
Initial Labelled set size: 35
Initial Unlabelled set size: 140
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Tue Oct 31 13.17.12 EET 2017
		
Iteration: 1
Labeled set size: 35
Unlabelled set size: 140
	
Mean region size: 55.39772727272727
Incorrectly Classified Instances: 10.227272727272727
Correctly Classified Instances: 89.77272727272727
Weighted Precision: 0.8986565236565237
Weighted AreaUnderROC: 0.9533642365500774
Root mean squared error: 0.31239507862992005
Relative absolute error: 22.999140784556023
Root relative squared error: 62.47901572598401
Weighted TruePositiveRate: 0.8977272727272727
Weighted MatthewsCorrelation: 0.7792835580968152
Weighted FMeasure: 0.898069703733766
Iteration time: 5.0
Weighted AreaUnderPRC: 0.9597501066627327
Mean absolute error: 0.11499570392278012
Coverage of cases: 93.75
Instances selection time: 5.0
Test time: 5.0
Accumulative iteration time: 5.0
Weighted Recall: 0.8977272727272727
Weighted FalsePositiveRate: 0.11320697493263863
Kappa statistic: 0.7790486818245222
Training time: 0.0
		
Iteration: 2
Labeled set size: 55
Unlabelled set size: 120
	
Mean region size: 54.54545454545455
Incorrectly Classified Instances: 10.795454545454545
Correctly Classified Instances: 89.20454545454545
Weighted Precision: 0.8915058312523246
Weighted AreaUnderROC: 0.9345413681696868
Root mean squared error: 0.3088646120498881
Relative absolute error: 22.34265669135406
Root relative squared error: 61.77292240997762
Weighted TruePositiveRate: 0.8920454545454546
Weighted MatthewsCorrelation: 0.7624482523909463
Weighted FMeasure: 0.890983327966799
Iteration time: 5.0
Weighted AreaUnderPRC: 0.9410435899175976
Mean absolute error: 0.11171328345677028
Coverage of cases: 92.61363636363636
Instances selection time: 4.0
Test time: 5.0
Accumulative iteration time: 10.0
Weighted Recall: 0.8920454545454546
Weighted FalsePositiveRate: 0.14446854767651227
Kappa statistic: 0.7609379468115529
Training time: 1.0
		
Iteration: 3
Labeled set size: 75
Unlabelled set size: 100
	
Mean region size: 56.25
Incorrectly Classified Instances: 7.954545454545454
Correctly Classified Instances: 92.04545454545455
Weighted Precision: 0.9238537003723383
Weighted AreaUnderROC: 0.947464531535328
Root mean squared error: 0.27628622469655495
Relative absolute error: 19.175731687076183
Root relative squared error: 55.25724493931099
Weighted TruePositiveRate: 0.9204545454545454
Weighted MatthewsCorrelation: 0.8274619795640561
Weighted FMeasure: 0.9187111471228946
Iteration time: 5.0
Weighted AreaUnderPRC: 0.9532624501089116
Mean absolute error: 0.09587865843538092
Coverage of cases: 94.31818181818181
Instances selection time: 4.0
Test time: 5.0
Accumulative iteration time: 15.0
Weighted Recall: 0.9204545454545454
Weighted FalsePositiveRate: 0.12862985097498372
Kappa statistic: 0.8206434706653079
Training time: 1.0
		
Iteration: 4
Labeled set size: 95
Unlabelled set size: 80
	
Mean region size: 54.26136363636363
Incorrectly Classified Instances: 11.931818181818182
Correctly Classified Instances: 88.06818181818181
Weighted Precision: 0.8798931379841666
Weighted AreaUnderROC: 0.9339794915016154
Root mean squared error: 0.30511038070034485
Relative absolute error: 22.485571394143204
Root relative squared error: 61.02207614006897
Weighted TruePositiveRate: 0.8806818181818182
Weighted MatthewsCorrelation: 0.7372338795196935
Weighted FMeasure: 0.8795078888054093
Iteration time: 8.0
Weighted AreaUnderPRC: 0.944095457645912
Mean absolute error: 0.11242785697071601
Coverage of cases: 93.18181818181819
Instances selection time: 6.0
Test time: 16.0
Accumulative iteration time: 23.0
Weighted Recall: 0.8806818181818182
Weighted FalsePositiveRate: 0.15782748470801566
Kappa statistic: 0.7357735201601374
Training time: 2.0
		
Iteration: 5
Labeled set size: 115
Unlabelled set size: 60
	
Mean region size: 55.39772727272727
Incorrectly Classified Instances: 8.522727272727273
Correctly Classified Instances: 91.47727272727273
Weighted Precision: 0.9147312177886403
Weighted AreaUnderROC: 0.9491501615395421
Root mean squared error: 0.2723734417941448
Relative absolute error: 18.455172614249012
Root relative squared error: 54.474688358828956
Weighted TruePositiveRate: 0.9147727272727273
Weighted MatthewsCorrelation: 0.8128769981334514
Weighted FMeasure: 0.9139342062895782
Iteration time: 9.0
Weighted AreaUnderPRC: 0.9552584535887121
Mean absolute error: 0.09227586307124507
Coverage of cases: 94.88636363636364
Instances selection time: 5.0
Test time: 15.0
Accumulative iteration time: 32.0
Weighted Recall: 0.9147727272727273
Weighted FalsePositiveRate: 0.11775067361350548
Kappa statistic: 0.8112668001143838
Training time: 4.0
		
Iteration: 6
Labeled set size: 135
Unlabelled set size: 40
	
Mean region size: 56.25
Incorrectly Classified Instances: 12.5
Correctly Classified Instances: 87.5
Weighted Precision: 0.875
Weighted AreaUnderROC: 0.9366484056749542
Root mean squared error: 0.31869927335333975
Relative absolute error: 24.839872960466973
Root relative squared error: 63.73985467066795
Weighted TruePositiveRate: 0.875
Weighted MatthewsCorrelation: 0.7280516926534627
Weighted FMeasure: 0.875
Iteration time: 11.0
Weighted AreaUnderPRC: 0.9461652734149023
Mean absolute error: 0.12419936480233486
Coverage of cases: 93.75
Instances selection time: 4.0
Test time: 23.0
Accumulative iteration time: 43.0
Weighted Recall: 0.875
Weighted FalsePositiveRate: 0.14694830734653744
Kappa statistic: 0.7280516926534626
Training time: 7.0
		
Iteration: 7
Labeled set size: 155
Unlabelled set size: 20
	
Mean region size: 56.53409090909091
Incorrectly Classified Instances: 15.909090909090908
Correctly Classified Instances: 84.0909090909091
Weighted Precision: 0.8458036274980607
Weighted AreaUnderROC: 0.9336985531675798
Root mean squared error: 0.36066885868796217
Relative absolute error: 30.808263287934196
Root relative squared error: 72.13377173759244
Weighted TruePositiveRate: 0.8409090909090909
Weighted MatthewsCorrelation: 0.6627770494577843
Weighted FMeasure: 0.8423553719008264
Iteration time: 7.0
Weighted AreaUnderPRC: 0.943794806048662
Mean absolute error: 0.15404131643967098
Coverage of cases: 91.47727272727273
Instances selection time: 3.0
Test time: 15.0
Accumulative iteration time: 50.0
Weighted Recall: 0.8409090909090909
Weighted FalsePositiveRate: 0.1659547433883717
Kappa statistic: 0.6610262759664328
Training time: 4.0
		
Time end:Tue Oct 31 13.17.12 EET 2017