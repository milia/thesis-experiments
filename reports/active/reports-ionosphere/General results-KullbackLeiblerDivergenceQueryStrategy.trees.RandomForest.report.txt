Tue Oct 31 13.16.44 EET 2017
Dataset: ionosphere
Test set size: 175
Initial Labelled set size: 35
Initial Unlabelled set size: 140
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Tue Oct 31 13.16.44 EET 2017
		
Iteration: 1
Labeled set size: 35
Unlabelled set size: 140
	
Mean region size: 86.66477272727272
Incorrectly Classified Instances: 14.299675324675325
Correctly Classified Instances: 85.7003246753247
Weighted Precision: 0.8608336381313487
Weighted AreaUnderROC: 0.9128054577288143
Root mean squared error: 0.33582157661372547
Relative absolute error: 47.88363636363638
Root relative squared error: 67.1643153227451
Weighted TruePositiveRate: 0.8570032467532467
Weighted MatthewsCorrelation: 0.6872171317325109
Weighted FMeasure: 0.8531671284795092
Iteration time: 8.8
Weighted AreaUnderPRC: 0.9040016403570272
Mean absolute error: 0.2394181818181819
Coverage of cases: 98.85909090909091
Instances selection time: 1.9
Test time: 1.7
Accumulative iteration time: 8.8
Weighted Recall: 0.8570032467532467
Weighted FalsePositiveRate: 0.20368729647933184
Kappa statistic: 0.6772311888728298
Training time: 6.9
		
Iteration: 2
Labeled set size: 55
Unlabelled set size: 120
	
Mean region size: 85.09626623376624
Incorrectly Classified Instances: 14.636363636363637
Correctly Classified Instances: 85.36363636363637
Weighted Precision: 0.8566201306437915
Weighted AreaUnderROC: 0.9212297197640117
Root mean squared error: 0.3317881107605702
Relative absolute error: 46.358376623376635
Root relative squared error: 66.35762215211403
Weighted TruePositiveRate: 0.8536363636363637
Weighted MatthewsCorrelation: 0.6780710251848184
Weighted FMeasure: 0.849265140489875
Iteration time: 9.5
Weighted AreaUnderPRC: 0.9126059133653174
Mean absolute error: 0.23179188311688317
Coverage of cases: 98.97500000000001
Instances selection time: 1.5
Test time: 2.5
Accumulative iteration time: 18.3
Weighted Recall: 0.8536363636363637
Weighted FalsePositiveRate: 0.2091023700979453
Kappa statistic: 0.6681190045047891
Training time: 8.0
		
Iteration: 3
Labeled set size: 75
Unlabelled set size: 100
	
Mean region size: 82.90470779220777
Incorrectly Classified Instances: 13.44577922077922
Correctly Classified Instances: 86.55422077922078
Weighted Precision: 0.8702368318284106
Weighted AreaUnderROC: 0.9284830709569963
Root mean squared error: 0.32465476464288767
Relative absolute error: 43.54266233766235
Root relative squared error: 64.93095292857753
Weighted TruePositiveRate: 0.8655422077922077
Weighted MatthewsCorrelation: 0.707353892221421
Weighted FMeasure: 0.8620275577342401
Iteration time: 14.5
Weighted AreaUnderPRC: 0.9193216525770431
Mean absolute error: 0.2177133116883118
Coverage of cases: 98.97532467532469
Instances selection time: 2.4
Test time: 2.1
Accumulative iteration time: 32.8
Weighted Recall: 0.8655422077922077
Weighted FalsePositiveRate: 0.18775389482690366
Kappa statistic: 0.6970275576444831
Training time: 12.1
		
Iteration: 4
Labeled set size: 95
Unlabelled set size: 80
	
Mean region size: 81.51233766233767
Incorrectly Classified Instances: 12.704870129870129
Correctly Classified Instances: 87.29512987012987
Weighted Precision: 0.8783118948523116
Weighted AreaUnderROC: 0.9315303112395401
Root mean squared error: 0.3236581126385529
Relative absolute error: 42.78993506493508
Root relative squared error: 64.73162252771057
Weighted TruePositiveRate: 0.8729512987012986
Weighted MatthewsCorrelation: 0.7218660037901428
Weighted FMeasure: 0.8679547165712881
Iteration time: 16.9
Weighted AreaUnderPRC: 0.9216711229617791
Mean absolute error: 0.2139496753246754
Coverage of cases: 98.74642857142858
Instances selection time: 1.8
Test time: 2.1
Accumulative iteration time: 49.7
Weighted Recall: 0.8729512987012986
Weighted FalsePositiveRate: 0.1934202901326795
Kappa statistic: 0.70882749669075
Training time: 15.1
		
Iteration: 5
Labeled set size: 115
Unlabelled set size: 60
	
Mean region size: 79.14675324675325
Incorrectly Classified Instances: 11.110064935064935
Correctly Classified Instances: 88.88993506493506
Weighted Precision: 0.8958078911482856
Weighted AreaUnderROC: 0.9498350114382037
Root mean squared error: 0.30363809797599556
Relative absolute error: 38.31311688311689
Root relative squared error: 60.72761959519911
Weighted TruePositiveRate: 0.8888993506493506
Weighted MatthewsCorrelation: 0.7619253513440339
Weighted FMeasure: 0.8862255375422909
Iteration time: 22.0
Weighted AreaUnderPRC: 0.9421271338279416
Mean absolute error: 0.19156558441558444
Coverage of cases: 98.91818181818184
Instances selection time: 1.5
Test time: 2.6
Accumulative iteration time: 71.7
Weighted Recall: 0.8888993506493506
Weighted FalsePositiveRate: 0.1578846365041055
Kappa statistic: 0.750578081721487
Training time: 20.5
		
Iteration: 6
Labeled set size: 135
Unlabelled set size: 40
	
Mean region size: 76.86607142857143
Incorrectly Classified Instances: 10.025649350649351
Correctly Classified Instances: 89.97435064935065
Weighted Precision: 0.905416305707926
Weighted AreaUnderROC: 0.9528288232697208
Root mean squared error: 0.29035456184942227
Relative absolute error: 34.586038961038966
Root relative squared error: 58.070912369884454
Weighted TruePositiveRate: 0.8997435064935064
Weighted MatthewsCorrelation: 0.7873129686754787
Weighted FMeasure: 0.8988970345471605
Iteration time: 24.5
Weighted AreaUnderPRC: 0.9462329861402365
Mean absolute error: 0.17293019480519484
Coverage of cases: 98.91785714285716
Instances selection time: 1.8
Test time: 2.1
Accumulative iteration time: 96.2
Weighted Recall: 0.8997435064935064
Weighted FalsePositiveRate: 0.13224104828308367
Kappa statistic: 0.7802609329468585
Training time: 22.7
		
Iteration: 7
Labeled set size: 155
Unlabelled set size: 20
	
Mean region size: 77.83279220779221
Incorrectly Classified Instances: 9.00357142857143
Correctly Classified Instances: 90.99642857142857
Weighted Precision: 0.9128695508350629
Weighted AreaUnderROC: 0.9572866499107017
Root mean squared error: 0.2738732475473235
Relative absolute error: 32.93422077922078
Root relative squared error: 54.77464950946471
Weighted TruePositiveRate: 0.9099642857142858
Weighted MatthewsCorrelation: 0.8067906179289575
Weighted FMeasure: 0.9094239021815638
Iteration time: 26.1
Weighted AreaUnderPRC: 0.9518380057541285
Mean absolute error: 0.16467110389610395
Coverage of cases: 99.1461038961039
Instances selection time: 1.5
Test time: 1.5
Accumulative iteration time: 122.3
Weighted Recall: 0.9099642857142858
Weighted FalsePositiveRate: 0.1153345975558365
Kappa statistic: 0.8029173628290376
Training time: 24.6
		
Iteration: 8
Labeled set size: 175
Unlabelled set size: 1
	
Mean region size: 73.88571428571427
Incorrectly Classified Instances: 7.542857142857143
Correctly Classified Instances: 92.45714285714286
Weighted Precision: 0.9251268404900015
Weighted AreaUnderROC: 0.9634920634920634
Root mean squared error: 0.2543879982971948
Relative absolute error: 27.908571428571427
Root relative squared error: 50.87759965943896
Weighted TruePositiveRate: 0.9245714285714286
Weighted MatthewsCorrelation: 0.8351879119646972
Weighted FMeasure: 0.9236687152056036
Iteration time: 26.6
Weighted AreaUnderPRC: 0.9586259879888864
Mean absolute error: 0.1395428571428572
Coverage of cases: 99.08571428571429
Instances selection time: 1.2
Test time: 1.8
Accumulative iteration time: 156.6
Weighted Recall: 0.9245714285714286
Weighted FalsePositiveRate: 0.10770634920634921
Kappa statistic: 0.8328706856223521
Training time: 25.4
		
Time end:Tue Oct 31 13.16.49 EET 2017