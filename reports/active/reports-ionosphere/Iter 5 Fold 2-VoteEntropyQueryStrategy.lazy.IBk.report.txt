Tue Oct 31 13.18.02 EET 2017
Dataset: ionosphere
Test set size: 175
Initial Labelled set size: 35
Initial Unlabelled set size: 141
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Tue Oct 31 13.18.02 EET 2017
		
Iteration: 1
Labeled set size: 35
Unlabelled set size: 141
	
Mean region size: 50.0
Incorrectly Classified Instances: 28.0
Correctly Classified Instances: 72.0
Weighted Precision: 0.8052173913043479
Weighted AreaUnderROC: 0.6111111111111112
Root mean squared error: 0.5153534530046986
Relative absolute error: 58.3483483483484
Root relative squared error: 103.07069060093971
Weighted TruePositiveRate: 0.72
Weighted MatthewsCorrelation: 0.3931785497463924
Weighted FMeasure: 0.656037296037296
Iteration time: 5.0
Weighted AreaUnderPRC: 0.6260173913043477
Mean absolute error: 0.291741741741742
Coverage of cases: 72.0
Instances selection time: 5.0
Test time: 6.0
Accumulative iteration time: 5.0
Weighted Recall: 0.72
Weighted FalsePositiveRate: 0.4977777777777778
Kappa statistic: 0.26778242677824254
Training time: 0.0
		
Iteration: 2
Labeled set size: 55
Unlabelled set size: 121
	
Mean region size: 50.0
Incorrectly Classified Instances: 28.0
Correctly Classified Instances: 72.0
Weighted Precision: 0.8052173913043479
Weighted AreaUnderROC: 0.6111111111111112
Root mean squared error: 0.5200775126082188
Relative absolute error: 57.524167561761544
Root relative squared error: 104.01550252164375
Weighted TruePositiveRate: 0.72
Weighted MatthewsCorrelation: 0.3931785497463924
Weighted FMeasure: 0.656037296037296
Iteration time: 7.0
Weighted AreaUnderPRC: 0.6260173913043477
Mean absolute error: 0.2876208378088077
Coverage of cases: 72.0
Instances selection time: 7.0
Test time: 9.0
Accumulative iteration time: 12.0
Weighted Recall: 0.72
Weighted FalsePositiveRate: 0.4977777777777778
Kappa statistic: 0.26778242677824254
Training time: 0.0
		
Iteration: 3
Labeled set size: 75
Unlabelled set size: 101
	
Mean region size: 50.0
Incorrectly Classified Instances: 28.0
Correctly Classified Instances: 72.0
Weighted Precision: 0.8052173913043479
Weighted AreaUnderROC: 0.6111111111111112
Root mean squared error: 0.5223930534758223
Relative absolute error: 57.12821013572896
Root relative squared error: 104.47861069516446
Weighted TruePositiveRate: 0.72
Weighted MatthewsCorrelation: 0.3931785497463924
Weighted FMeasure: 0.656037296037296
Iteration time: 8.0
Weighted AreaUnderPRC: 0.6260173913043477
Mean absolute error: 0.2856410506786448
Coverage of cases: 72.0
Instances selection time: 7.0
Test time: 12.0
Accumulative iteration time: 20.0
Weighted Recall: 0.72
Weighted FalsePositiveRate: 0.4977777777777778
Kappa statistic: 0.26778242677824254
Training time: 1.0
		
Iteration: 4
Labeled set size: 95
Unlabelled set size: 81
	
Mean region size: 50.0
Incorrectly Classified Instances: 28.0
Correctly Classified Instances: 72.0
Weighted Precision: 0.8052173913043479
Weighted AreaUnderROC: 0.6111111111111112
Root mean squared error: 0.5237672963455673
Relative absolute error: 56.89555719194891
Root relative squared error: 104.75345926911346
Weighted TruePositiveRate: 0.72
Weighted MatthewsCorrelation: 0.3931785497463924
Weighted FMeasure: 0.656037296037296
Iteration time: 9.0
Weighted AreaUnderPRC: 0.6260173913043477
Mean absolute error: 0.28447778595974454
Coverage of cases: 72.0
Instances selection time: 8.0
Test time: 15.0
Accumulative iteration time: 29.0
Weighted Recall: 0.72
Weighted FalsePositiveRate: 0.4977777777777778
Kappa statistic: 0.26778242677824254
Training time: 1.0
		
Iteration: 5
Labeled set size: 115
Unlabelled set size: 61
	
Mean region size: 50.0
Incorrectly Classified Instances: 28.571428571428573
Correctly Classified Instances: 71.42857142857143
Weighted Precision: 0.8024691358024691
Weighted AreaUnderROC: 0.6031746031746031
Root mean squared error: 0.5300025562627138
Relative absolute error: 57.86577407267056
Root relative squared error: 106.00051125254275
Weighted TruePositiveRate: 0.7142857142857143
Weighted MatthewsCorrelation: 0.37770514915502107
Weighted FMeasure: 0.646369573568959
Iteration time: 8.0
Weighted AreaUnderPRC: 0.6196119929453263
Mean absolute error: 0.2893288703633528
Coverage of cases: 71.42857142857143
Instances selection time: 8.0
Test time: 17.0
Accumulative iteration time: 37.0
Weighted Recall: 0.7142857142857143
Weighted FalsePositiveRate: 0.5079365079365079
Kappa statistic: 0.2496998799519809
Training time: 0.0
		
Iteration: 6
Labeled set size: 135
Unlabelled set size: 41
	
Mean region size: 50.0
Incorrectly Classified Instances: 24.571428571428573
Correctly Classified Instances: 75.42857142857143
Weighted Precision: 0.8224516129032259
Weighted AreaUnderROC: 0.6587301587301587
Root mean squared error: 0.4921181443279929
Relative absolute error: 49.881156842298985
Root relative squared error: 98.42362886559857
Weighted TruePositiveRate: 0.7542857142857143
Weighted MatthewsCorrelation: 0.4789474720713998
Weighted FMeasure: 0.7104228148549253
Iteration time: 7.0
Weighted AreaUnderPRC: 0.6651944700460829
Mean absolute error: 0.24940578421149492
Coverage of cases: 75.42857142857143
Instances selection time: 6.0
Test time: 19.0
Accumulative iteration time: 44.0
Weighted Recall: 0.7542857142857143
Weighted FalsePositiveRate: 0.43682539682539684
Kappa statistic: 0.37317784256559783
Training time: 1.0
		
Iteration: 7
Labeled set size: 155
Unlabelled set size: 21
	
Mean region size: 50.0
Incorrectly Classified Instances: 18.857142857142858
Correctly Classified Instances: 81.14285714285714
Weighted Precision: 0.8543448275862069
Weighted AreaUnderROC: 0.7380952380952382
Root mean squared error: 0.43152015160456536
Relative absolute error: 38.5041179627167
Root relative squared error: 86.30403032091307
Weighted TruePositiveRate: 0.8114285714285714
Weighted MatthewsCorrelation: 0.6064784348631227
Weighted FMeasure: 0.7900790761892806
Iteration time: 4.0
Weighted AreaUnderPRC: 0.7336591133004926
Mean absolute error: 0.1925205898135835
Coverage of cases: 81.14285714285714
Instances selection time: 4.0
Test time: 21.0
Accumulative iteration time: 48.0
Weighted Recall: 0.8114285714285714
Weighted FalsePositiveRate: 0.3352380952380953
Kappa statistic: 0.5378151260504201
Training time: 0.0
		
Iteration: 8
Labeled set size: 175
Unlabelled set size: 1
	
Mean region size: 50.0
Incorrectly Classified Instances: 16.571428571428573
Correctly Classified Instances: 83.42857142857143
Weighted Precision: 0.868368794326241
Weighted AreaUnderROC: 0.7698412698412699
Root mean squared error: 0.4048130320780229
Relative absolute error: 33.89509501797637
Root relative squared error: 80.96260641560458
Weighted TruePositiveRate: 0.8342857142857143
Weighted MatthewsCorrelation: 0.6547396453187239
Weighted FMeasure: 0.8190114502261522
Iteration time: 1.0
Weighted AreaUnderPRC: 0.7623116514690982
Mean absolute error: 0.16947547508988184
Coverage of cases: 83.42857142857143
Instances selection time: 1.0
Test time: 22.0
Accumulative iteration time: 49.0
Weighted Recall: 0.8342857142857143
Weighted FalsePositiveRate: 0.2946031746031746
Kappa statistic: 0.6001103143960287
Training time: 0.0
		
Time end:Tue Oct 31 13.18.02 EET 2017