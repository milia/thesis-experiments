Tue Oct 31 13.16.48 EET 2017
Dataset: ionosphere
Test set size: 176
Initial Labelled set size: 35
Initial Unlabelled set size: 140
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Tue Oct 31 13.16.48 EET 2017
		
Iteration: 1
Labeled set size: 35
Unlabelled set size: 140
	
Mean region size: 88.35227272727273
Incorrectly Classified Instances: 17.045454545454547
Correctly Classified Instances: 82.95454545454545
Weighted Precision: 0.8275920360631104
Weighted AreaUnderROC: 0.8851664559629161
Root mean squared error: 0.3601609488699587
Relative absolute error: 53.52272727272728
Root relative squared error: 72.03218977399175
Weighted TruePositiveRate: 0.8295454545454546
Weighted MatthewsCorrelation: 0.6216683538863422
Weighted FMeasure: 0.8266810872743076
Iteration time: 5.0
Weighted AreaUnderPRC: 0.8712332003431654
Mean absolute error: 0.2676136363636364
Coverage of cases: 98.86363636363636
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 5.0
Weighted Recall: 0.8295454545454546
Weighted FalsePositiveRate: 0.22847788887611895
Kappa statistic: 0.6184419713831479
Training time: 4.0
		
Iteration: 2
Labeled set size: 55
Unlabelled set size: 120
	
Mean region size: 91.19318181818181
Incorrectly Classified Instances: 19.318181818181817
Correctly Classified Instances: 80.68181818181819
Weighted Precision: 0.8068181818181818
Weighted AreaUnderROC: 0.8975277426604861
Root mean squared error: 0.3499999999999999
Relative absolute error: 54.54545454545456
Root relative squared error: 69.99999999999999
Weighted TruePositiveRate: 0.8068181818181818
Weighted MatthewsCorrelation: 0.579716252282624
Weighted FMeasure: 0.8068181818181818
Iteration time: 6.0
Weighted AreaUnderPRC: 0.8928761645847754
Mean absolute error: 0.2727272727272728
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 11.0
Weighted Recall: 0.8068181818181818
Weighted FalsePositiveRate: 0.22710192953555783
Kappa statistic: 0.5797162522826238
Training time: 5.0
		
Iteration: 3
Labeled set size: 75
Unlabelled set size: 100
	
Mean region size: 86.36363636363636
Incorrectly Classified Instances: 17.045454545454547
Correctly Classified Instances: 82.95454545454545
Weighted Precision: 0.8275920360631104
Weighted AreaUnderROC: 0.9034976822587442
Root mean squared error: 0.34641016151377535
Relative absolute error: 48.40909090909092
Root relative squared error: 69.28203230275507
Weighted TruePositiveRate: 0.8295454545454546
Weighted MatthewsCorrelation: 0.6216683538863422
Weighted FMeasure: 0.8266810872743076
Iteration time: 9.0
Weighted AreaUnderPRC: 0.8899977123497274
Mean absolute error: 0.2420454545454546
Coverage of cases: 98.86363636363636
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 20.0
Weighted Recall: 0.8295454545454546
Weighted FalsePositiveRate: 0.22847788887611895
Kappa statistic: 0.6184419713831479
Training time: 8.0
		
Iteration: 4
Labeled set size: 95
Unlabelled set size: 80
	
Mean region size: 84.0909090909091
Incorrectly Classified Instances: 12.5
Correctly Classified Instances: 87.5
Weighted Precision: 0.8860687022900763
Weighted AreaUnderROC: 0.9627756707402725
Root mean squared error: 0.3207767561291298
Relative absolute error: 44.65909090909091
Root relative squared error: 64.15535122582597
Weighted TruePositiveRate: 0.875
Weighted MatthewsCorrelation: 0.7306091438738711
Weighted FMeasure: 0.8691939890710383
Iteration time: 10.0
Weighted AreaUnderPRC: 0.9542216296937496
Mean absolute error: 0.22329545454545452
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 30.0
Weighted Recall: 0.875
Weighted FalsePositiveRate: 0.21015943250456523
Kappa statistic: 0.7097016044384465
Training time: 9.0
		
Iteration: 5
Labeled set size: 115
Unlabelled set size: 60
	
Mean region size: 79.54545454545455
Incorrectly Classified Instances: 11.363636363636363
Correctly Classified Instances: 88.63636363636364
Weighted Precision: 0.8897433155080214
Weighted AreaUnderROC: 0.9452170248630426
Root mean squared error: 0.32377391101592085
Relative absolute error: 42.613636363636346
Root relative squared error: 64.75478220318416
Weighted TruePositiveRate: 0.8863636363636364
Weighted MatthewsCorrelation: 0.7509570319717033
Weighted FMeasure: 0.8832475573961642
Iteration time: 27.0
Weighted AreaUnderPRC: 0.9367700861393576
Mean absolute error: 0.21306818181818174
Coverage of cases: 99.43181818181819
Instances selection time: 4.0
Test time: 3.0
Accumulative iteration time: 57.0
Weighted Recall: 0.8863636363636364
Weighted FalsePositiveRate: 0.17573012042038588
Kappa statistic: 0.7418976389499926
Training time: 23.0
		
Iteration: 6
Labeled set size: 135
Unlabelled set size: 40
	
Mean region size: 76.13636363636364
Incorrectly Classified Instances: 9.090909090909092
Correctly Classified Instances: 90.9090909090909
Weighted Precision: 0.916862664894161
Weighted AreaUnderROC: 0.9598960528164068
Root mean squared error: 0.28742587977361456
Relative absolute error: 33.4090909090909
Root relative squared error: 57.48517595472291
Weighted TruePositiveRate: 0.9090909090909091
Weighted MatthewsCorrelation: 0.805445965131814
Weighted FMeasure: 0.9060606060606061
Iteration time: 32.0
Weighted AreaUnderPRC: 0.955144247986587
Mean absolute error: 0.1670454545454545
Coverage of cases: 98.86363636363636
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 89.0
Weighted Recall: 0.9090909090909091
Weighted FalsePositiveRate: 0.15603570470827105
Kappa statistic: 0.7919929088491653
Training time: 30.0
		
Iteration: 7
Labeled set size: 155
Unlabelled set size: 20
	
Mean region size: 80.39772727272727
Incorrectly Classified Instances: 7.386363636363637
Correctly Classified Instances: 92.61363636363636
Weighted Precision: 0.9288530799801292
Weighted AreaUnderROC: 0.9655850540806292
Root mean squared error: 0.27396499444603895
Relative absolute error: 34.6590909090909
Root relative squared error: 54.79299888920779
Weighted TruePositiveRate: 0.9261363636363636
Weighted MatthewsCorrelation: 0.8396176373487361
Weighted FMeasure: 0.9247098646034815
Iteration time: 19.0
Weighted AreaUnderPRC: 0.9610578927722105
Mean absolute error: 0.1732954545454545
Coverage of cases: 99.43181818181819
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 108.0
Weighted Recall: 0.9261363636363636
Weighted FalsePositiveRate: 0.11843865328378601
Kappa statistic: 0.8340586016826226
Training time: 18.0
		
Time end:Tue Oct 31 13.16.48 EET 2017