Tue Oct 31 13.17.24 EET 2017
Dataset: ionosphere
Test set size: 175
Initial Labelled set size: 35
Initial Unlabelled set size: 140
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Tue Oct 31 13.17.24 EET 2017
		
Iteration: 1
Labeled set size: 35
Unlabelled set size: 140
	
Mean region size: 56.00292207792207
Incorrectly Classified Instances: 15.831493506493507
Correctly Classified Instances: 84.1685064935065
Weighted Precision: 0.8476507531033264
Weighted AreaUnderROC: 0.8154559353239822
Root mean squared error: 0.38055494291411696
Relative absolute error: 34.606838983480316
Root relative squared error: 76.1109885828234
Weighted TruePositiveRate: 0.841685064935065
Weighted MatthewsCorrelation: 0.6555726569906126
Weighted FMeasure: 0.8374255182766452
Iteration time: 4.1
Weighted AreaUnderPRC: 0.7944584680475952
Mean absolute error: 0.17303419491740155
Coverage of cases: 87.18409090909091
Instances selection time: 1.1
Test time: 1.3
Accumulative iteration time: 4.1
Weighted Recall: 0.841685064935065
Weighted FalsePositiveRate: 0.22559958944693462
Kappa statistic: 0.6426785608412768
Training time: 3.0
		
Iteration: 2
Labeled set size: 55
Unlabelled set size: 120
	
Mean region size: 56.98977272727272
Incorrectly Classified Instances: 17.030519480519484
Correctly Classified Instances: 82.96948051948053
Weighted Precision: 0.8429334839368086
Weighted AreaUnderROC: 0.8148243884574479
Root mean squared error: 0.3928095233290855
Relative absolute error: 35.910423235297756
Root relative squared error: 78.56190466581708
Weighted TruePositiveRate: 0.8296948051948052
Weighted MatthewsCorrelation: 0.6405943677287502
Weighted FMeasure: 0.8259785668053615
Iteration time: 4.5
Weighted AreaUnderPRC: 0.7950147057836559
Mean absolute error: 0.17955211617648875
Coverage of cases: 87.9275974025974
Instances selection time: 0.7
Test time: 1.5
Accumulative iteration time: 8.6
Weighted Recall: 0.8296948051948052
Weighted FalsePositiveRate: 0.2195906824247532
Kappa statistic: 0.6236030430044522
Training time: 3.8
		
Iteration: 3
Labeled set size: 75
Unlabelled set size: 100
	
Mean region size: 55.92564935064935
Incorrectly Classified Instances: 15.893506493506493
Correctly Classified Instances: 84.1064935064935
Weighted Precision: 0.8610785672506157
Weighted AreaUnderROC: 0.8499483274135612
Root mean squared error: 0.3686446511435131
Relative absolute error: 32.35774107012686
Root relative squared error: 73.72893022870262
Weighted TruePositiveRate: 0.8410649350649351
Weighted MatthewsCorrelation: 0.6759244660727767
Weighted FMeasure: 0.8383117466527581
Iteration time: 7.6
Weighted AreaUnderPRC: 0.8346673270849232
Mean absolute error: 0.1617887053506343
Coverage of cases: 89.52045454545454
Instances selection time: 1.8
Test time: 2.4
Accumulative iteration time: 16.2
Weighted Recall: 0.8410649350649351
Weighted FalsePositiveRate: 0.19640539720849454
Kappa statistic: 0.6550248835960187
Training time: 5.8
		
Iteration: 4
Labeled set size: 95
Unlabelled set size: 80
	
Mean region size: 55.6538961038961
Incorrectly Classified Instances: 13.78603896103896
Correctly Classified Instances: 86.21396103896102
Weighted Precision: 0.8709558093183161
Weighted AreaUnderROC: 0.8410896770212511
Root mean squared error: 0.3575546321010502
Relative absolute error: 29.73839044256956
Root relative squared error: 71.51092642021004
Weighted TruePositiveRate: 0.8621396103896103
Weighted MatthewsCorrelation: 0.7102120519841988
Weighted FMeasure: 0.8610622114194115
Iteration time: 11.6
Weighted AreaUnderPRC: 0.8240069030595567
Mean absolute error: 0.14869195221284784
Coverage of cases: 88.27077922077923
Instances selection time: 2.9
Test time: 1.2
Accumulative iteration time: 27.8
Weighted Recall: 0.8621396103896103
Weighted FalsePositiveRate: 0.1700427568989516
Kappa statistic: 0.7011387258870944
Training time: 8.7
		
Iteration: 5
Labeled set size: 115
Unlabelled set size: 60
	
Mean region size: 55.70974025974026
Incorrectly Classified Instances: 15.208766233766235
Correctly Classified Instances: 84.79123376623376
Weighted Precision: 0.8591516275286526
Weighted AreaUnderROC: 0.8221580704553209
Root mean squared error: 0.3762558389175542
Relative absolute error: 32.66827498054717
Root relative squared error: 75.25116778351085
Weighted TruePositiveRate: 0.8479123376623375
Weighted MatthewsCorrelation: 0.6761109499562417
Weighted FMeasure: 0.8431203105448295
Iteration time: 12.8
Weighted AreaUnderPRC: 0.8030958922660124
Mean absolute error: 0.1633413749027358
Coverage of cases: 87.07175324675325
Instances selection time: 2.2
Test time: 0.7
Accumulative iteration time: 40.6
Weighted Recall: 0.8479123376623375
Weighted FalsePositiveRate: 0.2115176544203093
Kappa statistic: 0.6591213012051848
Training time: 10.6
		
Iteration: 6
Labeled set size: 135
Unlabelled set size: 40
	
Mean region size: 57.665422077922074
Incorrectly Classified Instances: 13.502597402597402
Correctly Classified Instances: 86.49740259740258
Weighted Precision: 0.8713704747767776
Weighted AreaUnderROC: 0.8535605171272049
Root mean squared error: 0.35014604386157816
Relative absolute error: 29.008266554340338
Root relative squared error: 70.02920877231563
Weighted TruePositiveRate: 0.864974025974026
Weighted MatthewsCorrelation: 0.7097494184566792
Weighted FMeasure: 0.8622534969453557
Iteration time: 14.0
Weighted AreaUnderPRC: 0.8381573028474337
Mean absolute error: 0.14504133277170173
Coverage of cases: 90.65097402597402
Instances selection time: 1.5
Test time: 1.4
Accumulative iteration time: 54.6
Weighted Recall: 0.864974025974026
Weighted FalsePositiveRate: 0.17958720900535058
Kappa statistic: 0.699304085286568
Training time: 12.5
		
Iteration: 7
Labeled set size: 155
Unlabelled set size: 20
	
Mean region size: 52.903409090909086
Incorrectly Classified Instances: 11.285064935064934
Correctly Classified Instances: 88.71493506493506
Weighted Precision: 0.8903395146270048
Weighted AreaUnderROC: 0.8645647086268136
Root mean squared error: 0.32748387363264897
Relative absolute error: 24.55853331039855
Root relative squared error: 65.49677472652978
Weighted TruePositiveRate: 0.8871493506493506
Weighted MatthewsCorrelation: 0.7549532436619619
Weighted FMeasure: 0.8851590557157738
Iteration time: 15.3
Weighted AreaUnderPRC: 0.8467200396635306
Mean absolute error: 0.12279266655199275
Coverage of cases: 90.36558441558441
Instances selection time: 1.2
Test time: 1.2
Accumulative iteration time: 69.9
Weighted Recall: 0.8871493506493506
Weighted FalsePositiveRate: 0.15741641765314332
Kappa statistic: 0.7482744949221051
Training time: 14.1
		
Iteration: 8
Labeled set size: 175
Unlabelled set size: 1
	
Mean region size: 52.05714285714286
Incorrectly Classified Instances: 10.514285714285714
Correctly Classified Instances: 89.4857142857143
Weighted Precision: 0.8982599050600404
Weighted AreaUnderROC: 0.8887755102040815
Root mean squared error: 0.311056225757688
Relative absolute error: 23.8666233130295
Root relative squared error: 62.2112451515376
Weighted TruePositiveRate: 0.8948571428571428
Weighted MatthewsCorrelation: 0.7715576009169067
Weighted FMeasure: 0.8926058722801138
Iteration time: 21.4
Weighted AreaUnderPRC: 0.8704458467470069
Mean absolute error: 0.11933311656514749
Coverage of cases: 91.31428571428572
Instances selection time: 1.4
Test time: 1.8
Accumulative iteration time: 87.6
Weighted Recall: 0.8948571428571428
Weighted FalsePositiveRate: 0.15636507936507935
Kappa statistic: 0.7640098509995462
Training time: 20.0
		
Time end:Tue Oct 31 13.17.28 EET 2017