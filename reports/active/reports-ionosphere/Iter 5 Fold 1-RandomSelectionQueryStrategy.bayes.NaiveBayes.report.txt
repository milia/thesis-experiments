Tue Oct 31 13.17.35 EET 2017
Dataset: ionosphere
Test set size: 176
Initial Labelled set size: 35
Initial Unlabelled set size: 140
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Tue Oct 31 13.17.35 EET 2017
		
Iteration: 1
Labeled set size: 35
Unlabelled set size: 140
	
Mean region size: 52.84090909090909
Incorrectly Classified Instances: 21.022727272727273
Correctly Classified Instances: 78.97727272727273
Weighted Precision: 0.8205904150197628
Weighted AreaUnderROC: 0.8960911261796217
Root mean squared error: 0.44056473495524534
Relative absolute error: 41.34686682700119
Root relative squared error: 88.11294699104907
Weighted TruePositiveRate: 0.7897727272727273
Weighted MatthewsCorrelation: 0.591593734422722
Weighted FMeasure: 0.7940212226797594
Iteration time: 8.0
Weighted AreaUnderPRC: 0.8993569387847653
Mean absolute error: 0.20673433413500597
Coverage of cases: 82.38636363636364
Instances selection time: 8.0
Test time: 10.0
Accumulative iteration time: 8.0
Weighted Recall: 0.7897727272727273
Weighted FalsePositiveRate: 0.17339402239844717
Kappa statistic: 0.5740450026164312
Training time: 0.0
		
Iteration: 2
Labeled set size: 55
Unlabelled set size: 120
	
Mean region size: 52.55681818181818
Incorrectly Classified Instances: 22.727272727272727
Correctly Classified Instances: 77.27272727272727
Weighted Precision: 0.8156164512228053
Weighted AreaUnderROC: 0.9065492951001801
Root mean squared error: 0.4689476619302863
Relative absolute error: 45.87045568691041
Root relative squared error: 93.78953238605726
Weighted TruePositiveRate: 0.7727272727272727
Weighted MatthewsCorrelation: 0.5722984965335874
Weighted FMeasure: 0.7773923444976076
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9053717163111408
Mean absolute error: 0.22935227843455205
Coverage of cases: 78.97727272727273
Instances selection time: 0.0
Test time: 10.0
Accumulative iteration time: 9.0
Weighted Recall: 0.7727272727272727
Weighted FalsePositiveRate: 0.17587378206847235
Kappa statistic: 0.546917235165401
Training time: 1.0
		
Iteration: 3
Labeled set size: 75
Unlabelled set size: 100
	
Mean region size: 53.40909090909091
Incorrectly Classified Instances: 20.454545454545453
Correctly Classified Instances: 79.54545454545455
Weighted Precision: 0.8240145332061384
Weighted AreaUnderROC: 0.913618645366433
Root mean squared error: 0.43878726794059897
Relative absolute error: 41.089309731075666
Root relative squared error: 87.75745358811979
Weighted TruePositiveRate: 0.7954545454545454
Weighted MatthewsCorrelation: 0.6004371810483875
Weighted FMeasure: 0.7995351170972929
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9142907968651222
Mean absolute error: 0.20544654865537834
Coverage of cases: 82.95454545454545
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 10.0
Weighted Recall: 0.7954545454545454
Weighted FalsePositiveRate: 0.17022628305814147
Kappa statistic: 0.5841974012337576
Training time: 0.0
		
Iteration: 4
Labeled set size: 95
Unlabelled set size: 80
	
Mean region size: 55.11363636363637
Incorrectly Classified Instances: 18.181818181818183
Correctly Classified Instances: 81.81818181818181
Weighted Precision: 0.8314311431143114
Weighted AreaUnderROC: 0.9286416631549376
Root mean squared error: 0.39556987113739356
Relative absolute error: 35.03779790517385
Root relative squared error: 79.1139742274787
Weighted TruePositiveRate: 0.8181818181818182
Weighted MatthewsCorrelation: 0.6268161243721281
Weighted FMeasure: 0.8209892504894537
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9373296587004801
Mean absolute error: 0.17518898952586928
Coverage of cases: 88.63636363636364
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 11.0
Weighted Recall: 0.8181818181818182
Weighted FalsePositiveRate: 0.1716022423987026
Kappa statistic: 0.6204340207575146
Training time: 0.0
		
Iteration: 5
Labeled set size: 115
Unlabelled set size: 60
	
Mean region size: 56.25
Incorrectly Classified Instances: 14.772727272727273
Correctly Classified Instances: 85.22727272727273
Weighted Precision: 0.8569917993424698
Weighted AreaUnderROC: 0.932574799831437
Root mean squared error: 0.3597359778780174
Relative absolute error: 30.85527561380293
Root relative squared error: 71.94719557560349
Weighted TruePositiveRate: 0.8522727272727273
Weighted MatthewsCorrelation: 0.6870535865451038
Weighted FMeasure: 0.8536157024793389
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9416054126109323
Mean absolute error: 0.15427637806901465
Coverage of cases: 90.9090909090909
Instances selection time: 0.0
Test time: 6.0
Accumulative iteration time: 12.0
Weighted Recall: 0.8522727272727273
Weighted FalsePositiveRate: 0.1525958063568683
Kappa statistic: 0.6852386848259734
Training time: 1.0
		
Iteration: 6
Labeled set size: 135
Unlabelled set size: 40
	
Mean region size: 56.25
Incorrectly Classified Instances: 15.340909090909092
Correctly Classified Instances: 84.6590909090909
Weighted Precision: 0.850381882551
Weighted AreaUnderROC: 0.931591515662312
Root mean squared error: 0.36478488303582207
Relative absolute error: 30.60225289818484
Root relative squared error: 72.95697660716442
Weighted TruePositiveRate: 0.8465909090909091
Weighted MatthewsCorrelation: 0.6732474838646133
Weighted FMeasure: 0.8477831665415859
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9407889524584584
Mean absolute error: 0.1530112644909242
Coverage of cases: 90.3409090909091
Instances selection time: 0.0
Test time: 5.0
Accumulative iteration time: 14.0
Weighted Recall: 0.8465909090909091
Weighted FalsePositiveRate: 0.16278700404806598
Kappa statistic: 0.6720044174489233
Training time: 2.0
		
Iteration: 7
Labeled set size: 155
Unlabelled set size: 20
	
Mean region size: 56.25
Incorrectly Classified Instances: 14.204545454545455
Correctly Classified Instances: 85.79545454545455
Weighted Precision: 0.863668524871355
Weighted AreaUnderROC: 0.9341199606686333
Root mean squared error: 0.360302292932501
Relative absolute error: 30.132557849275404
Root relative squared error: 72.0604585865002
Weighted TruePositiveRate: 0.8579545454545454
Weighted MatthewsCorrelation: 0.7008861716475863
Weighted FMeasure: 0.8594224508968561
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9431939350565518
Mean absolute error: 0.15066278924637702
Coverage of cases: 90.3409090909091
Instances selection time: 0.0
Test time: 6.0
Accumulative iteration time: 15.0
Weighted Recall: 0.8579545454545454
Weighted FalsePositiveRate: 0.14240460866567062
Kappa statistic: 0.6983822319714833
Training time: 1.0
		
Time end:Tue Oct 31 13.17.35 EET 2017