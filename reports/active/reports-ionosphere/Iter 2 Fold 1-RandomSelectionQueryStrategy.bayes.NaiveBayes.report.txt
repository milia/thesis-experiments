Tue Oct 31 13.17.33 EET 2017
Dataset: ionosphere
Test set size: 176
Initial Labelled set size: 35
Initial Unlabelled set size: 140
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Tue Oct 31 13.17.33 EET 2017
		
Iteration: 1
Labeled set size: 35
Unlabelled set size: 140
	
Mean region size: 54.82954545454545
Incorrectly Classified Instances: 12.5
Correctly Classified Instances: 87.5
Weighted Precision: 0.8841435031254157
Weighted AreaUnderROC: 0.9505548532097204
Root mean squared error: 0.3542761998971739
Relative absolute error: 28.310449597848496
Root relative squared error: 70.85523997943478
Weighted TruePositiveRate: 0.875
Weighted MatthewsCorrelation: 0.7425918288827851
Weighted FMeasure: 0.8767020697167756
Iteration time: 3.0
Weighted AreaUnderPRC: 0.9570075250447428
Mean absolute error: 0.14155224798924249
Coverage of cases: 88.63636363636364
Instances selection time: 3.0
Test time: 6.0
Accumulative iteration time: 3.0
Weighted Recall: 0.875
Weighted FalsePositiveRate: 0.11183101559207755
Kappa statistic: 0.7372777853168679
Training time: 0.0
		
Iteration: 2
Labeled set size: 55
Unlabelled set size: 120
	
Mean region size: 53.69318181818182
Incorrectly Classified Instances: 18.75
Correctly Classified Instances: 81.25
Weighted Precision: 0.843070652173913
Weighted AreaUnderROC: 0.9401601348504003
Root mean squared error: 0.4193671603430432
Relative absolute error: 37.55340615358742
Root relative squared error: 83.87343206860865
Weighted TruePositiveRate: 0.8125
Weighted MatthewsCorrelation: 0.6390506611585466
Weighted FMeasure: 0.8162891986062718
Iteration time: 1.0
Weighted AreaUnderPRC: 0.945714971221016
Mean absolute error: 0.1877670307679371
Coverage of cases: 85.22727272727273
Instances selection time: 0.0
Test time: 6.0
Accumulative iteration time: 4.0
Weighted Recall: 0.8125
Weighted FalsePositiveRate: 0.14667614833544038
Kappa statistic: 0.620094191522763
Training time: 1.0
		
Iteration: 3
Labeled set size: 75
Unlabelled set size: 100
	
Mean region size: 55.11363636363637
Incorrectly Classified Instances: 17.613636363636363
Correctly Classified Instances: 82.38636363636364
Weighted Precision: 0.8498858918714912
Weighted AreaUnderROC: 0.9435313948588285
Root mean squared error: 0.39435283904248425
Relative absolute error: 35.00748686624146
Root relative squared error: 78.87056780849684
Weighted TruePositiveRate: 0.8238636363636364
Weighted MatthewsCorrelation: 0.6568886456526487
Weighted FMeasure: 0.8273200520951647
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9498898829602523
Mean absolute error: 0.1750374343312073
Coverage of cases: 87.5
Instances selection time: 0.0
Test time: 6.0
Accumulative iteration time: 5.0
Weighted Recall: 0.8238636363636364
Weighted FalsePositiveRate: 0.14034066965482894
Kappa statistic: 0.6407690281801421
Training time: 1.0
		
Iteration: 4
Labeled set size: 95
Unlabelled set size: 80
	
Mean region size: 53.97727272727273
Incorrectly Classified Instances: 17.045454545454547
Correctly Classified Instances: 82.95454545454545
Weighted Precision: 0.8533891547049443
Weighted AreaUnderROC: 0.9558926815563983
Root mean squared error: 0.3978260118623313
Relative absolute error: 34.460147779987246
Root relative squared error: 79.56520237246626
Weighted TruePositiveRate: 0.8295454545454546
Weighted MatthewsCorrelation: 0.6659551926668414
Weighted FMeasure: 0.8328234265734267
Iteration time: 1.0
Weighted AreaUnderPRC: 0.962572194165709
Mean absolute error: 0.17230073889993622
Coverage of cases: 86.93181818181819
Instances selection time: 0.0
Test time: 6.0
Accumulative iteration time: 6.0
Weighted Recall: 0.8295454545454546
Weighted FalsePositiveRate: 0.13717293031452324
Kappa statistic: 0.6512088783194611
Training time: 1.0
		
Iteration: 5
Labeled set size: 115
Unlabelled set size: 60
	
Mean region size: 55.39772727272727
Incorrectly Classified Instances: 16.477272727272727
Correctly Classified Instances: 83.52272727272727
Weighted Precision: 0.8569602272727272
Weighted AreaUnderROC: 0.9532237673830596
Root mean squared error: 0.39116134750811693
Relative absolute error: 33.89537792168698
Root relative squared error: 78.23226950162339
Weighted TruePositiveRate: 0.8352272727272727
Weighted MatthewsCorrelation: 0.6751254351151984
Weighted FMeasure: 0.8383201726503162
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9593961598958937
Mean absolute error: 0.1694768896084349
Coverage of cases: 89.20454545454545
Instances selection time: 0.0
Test time: 6.0
Accumulative iteration time: 7.0
Weighted Recall: 0.8352272727272727
Weighted FalsePositiveRate: 0.1340051909742175
Kappa statistic: 0.6617179215270412
Training time: 1.0
		
Iteration: 6
Labeled set size: 135
Unlabelled set size: 40
	
Mean region size: 56.53409090909091
Incorrectly Classified Instances: 14.204545454545455
Correctly Classified Instances: 85.79545454545455
Weighted Precision: 0.8719886363636363
Weighted AreaUnderROC: 0.948307346537435
Root mean squared error: 0.3606704880592545
Relative absolute error: 30.327749622345657
Root relative squared error: 72.1340976118509
Weighted TruePositiveRate: 0.8579545454545454
Weighted MatthewsCorrelation: 0.7129291796319394
Weighted FMeasure: 0.8602622383527238
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9545726250727588
Mean absolute error: 0.15163874811172828
Coverage of cases: 90.3409090909091
Instances selection time: 0.0
Test time: 6.0
Accumulative iteration time: 8.0
Weighted Recall: 0.8579545454545454
Weighted FalsePositiveRate: 0.12133423361299468
Kappa statistic: 0.7044599677592691
Training time: 1.0
		
Iteration: 7
Labeled set size: 155
Unlabelled set size: 20
	
Mean region size: 56.25
Incorrectly Classified Instances: 16.477272727272727
Correctly Classified Instances: 83.52272727272727
Weighted Precision: 0.8569602272727272
Weighted AreaUnderROC: 0.9426885798567216
Root mean squared error: 0.38920531447638407
Relative absolute error: 33.98712531130595
Root relative squared error: 77.84106289527682
Weighted TruePositiveRate: 0.8352272727272727
Weighted MatthewsCorrelation: 0.6751254351151984
Weighted FMeasure: 0.8383201726503162
Iteration time: 1.0
Weighted AreaUnderPRC: 0.948871191848923
Mean absolute error: 0.16993562655652975
Coverage of cases: 89.20454545454545
Instances selection time: 0.0
Test time: 6.0
Accumulative iteration time: 9.0
Weighted Recall: 0.8352272727272727
Weighted FalsePositiveRate: 0.1340051909742175
Kappa statistic: 0.6617179215270412
Training time: 1.0
		
Time end:Tue Oct 31 13.17.33 EET 2017