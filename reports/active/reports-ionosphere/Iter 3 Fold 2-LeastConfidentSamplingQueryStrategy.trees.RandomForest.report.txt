Tue Oct 31 13.17.08 EET 2017
Dataset: ionosphere
Test set size: 175
Initial Labelled set size: 35
Initial Unlabelled set size: 141
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Tue Oct 31 13.17.08 EET 2017
		
Iteration: 1
Labeled set size: 35
Unlabelled set size: 141
	
Mean region size: 79.14285714285714
Incorrectly Classified Instances: 13.714285714285714
Correctly Classified Instances: 86.28571428571429
Weighted Precision: 0.8765891472868218
Weighted AreaUnderROC: 0.9149659863945578
Root mean squared error: 0.3399159559992105
Relative absolute error: 43.65714285714294
Root relative squared error: 67.9831911998421
Weighted TruePositiveRate: 0.8628571428571429
Weighted MatthewsCorrelation: 0.7056969481308573
Weighted FMeasure: 0.8555397463656046
Iteration time: 4.0
Weighted AreaUnderPRC: 0.9079879757728957
Mean absolute error: 0.21828571428571467
Coverage of cases: 97.14285714285714
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 4.0
Weighted Recall: 0.8628571428571429
Weighted FalsePositiveRate: 0.2299206349206349
Kappa statistic: 0.6801705756929638
Training time: 4.0
		
Iteration: 2
Labeled set size: 55
Unlabelled set size: 121
	
Mean region size: 86.28571428571429
Incorrectly Classified Instances: 5.142857142857143
Correctly Classified Instances: 94.85714285714286
Weighted Precision: 0.950546218487395
Weighted AreaUnderROC: 0.9595379818594103
Root mean squared error: 0.2608776396265933
Relative absolute error: 34.62857142857148
Root relative squared error: 52.17552792531867
Weighted TruePositiveRate: 0.9485714285714286
Weighted MatthewsCorrelation: 0.8891384979709833
Weighted FMeasure: 0.9478380443086326
Iteration time: 5.0
Weighted AreaUnderPRC: 0.9566446787893538
Mean absolute error: 0.1731428571428574
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 9.0
Weighted Recall: 0.9485714285714286
Weighted FalsePositiveRate: 0.08448412698412698
Kappa statistic: 0.8856126080325369
Training time: 5.0
		
Iteration: 3
Labeled set size: 75
Unlabelled set size: 101
	
Mean region size: 83.42857142857143
Incorrectly Classified Instances: 8.571428571428571
Correctly Classified Instances: 91.42857142857143
Weighted Precision: 0.9146396396396397
Weighted AreaUnderROC: 0.958262471655329
Root mean squared error: 0.2614246681988083
Relative absolute error: 33.142857142857146
Root relative squared error: 52.284933639761654
Weighted TruePositiveRate: 0.9142857142857143
Weighted MatthewsCorrelation: 0.8146939463542284
Weighted FMeasure: 0.9144309876063696
Iteration time: 8.0
Weighted AreaUnderPRC: 0.9549826979716537
Mean absolute error: 0.16571428571428573
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 17.0
Weighted Recall: 0.9142857142857143
Weighted FalsePositiveRate: 0.09682539682539681
Kappa statistic: 0.81463173504696
Training time: 7.0
		
Iteration: 4
Labeled set size: 95
Unlabelled set size: 81
	
Mean region size: 85.42857142857143
Incorrectly Classified Instances: 10.285714285714286
Correctly Classified Instances: 89.71428571428571
Weighted Precision: 0.8965984804208066
Weighted AreaUnderROC: 0.9619472789115645
Root mean squared error: 0.2791824800069966
Relative absolute error: 37.48571428571433
Root relative squared error: 55.83649600139932
Weighted TruePositiveRate: 0.8971428571428571
Weighted MatthewsCorrelation: 0.7746224746580367
Weighted FMeasure: 0.8963589301121656
Iteration time: 11.0
Weighted AreaUnderPRC: 0.9523680037390194
Mean absolute error: 0.18742857142857167
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 28.0
Weighted Recall: 0.8971428571428571
Weighted FalsePositiveRate: 0.13424603174603172
Kappa statistic: 0.7736418511066399
Training time: 11.0
		
Iteration: 5
Labeled set size: 115
Unlabelled set size: 61
	
Mean region size: 88.28571428571429
Incorrectly Classified Instances: 4.571428571428571
Correctly Classified Instances: 95.42857142857143
Weighted Precision: 0.9557181088314006
Weighted AreaUnderROC: 0.9731434240362813
Root mean squared error: 0.2454151002456273
Relative absolute error: 34.51428571428575
Root relative squared error: 49.08302004912546
Weighted TruePositiveRate: 0.9542857142857143
Weighted MatthewsCorrelation: 0.9012887336322649
Weighted FMeasure: 0.9537391304347826
Iteration time: 13.0
Weighted AreaUnderPRC: 0.9689048557031325
Mean absolute error: 0.17257142857142874
Coverage of cases: 99.42857142857143
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 41.0
Weighted Recall: 0.9542857142857143
Weighted FalsePositiveRate: 0.07432539682539682
Kappa statistic: 0.8986828774062817
Training time: 12.0
		
Iteration: 6
Labeled set size: 135
Unlabelled set size: 41
	
Mean region size: 79.42857142857143
Incorrectly Classified Instances: 9.142857142857142
Correctly Classified Instances: 90.85714285714286
Weighted Precision: 0.9082174167153712
Weighted AreaUnderROC: 0.9580498866213152
Root mean squared error: 0.26747496545070726
Relative absolute error: 31.7714285714286
Root relative squared error: 53.49499309014145
Weighted TruePositiveRate: 0.9085714285714286
Weighted MatthewsCorrelation: 0.7998052599200014
Weighted FMeasure: 0.9078746045441471
Iteration time: 16.0
Weighted AreaUnderPRC: 0.9521352072731458
Mean absolute error: 0.158857142857143
Coverage of cases: 99.42857142857143
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 57.0
Weighted Recall: 0.9085714285714286
Weighted FalsePositiveRate: 0.12087301587301588
Kappa statistic: 0.7987927565392354
Training time: 15.0
		
Iteration: 7
Labeled set size: 155
Unlabelled set size: 21
	
Mean region size: 77.71428571428571
Incorrectly Classified Instances: 6.857142857142857
Correctly Classified Instances: 93.14285714285714
Weighted Precision: 0.9335757575757576
Weighted AreaUnderROC: 0.9669784580498866
Root mean squared error: 0.2547828206812336
Relative absolute error: 28.800000000000043
Root relative squared error: 50.95656413624672
Weighted TruePositiveRate: 0.9314285714285714
Weighted MatthewsCorrelation: 0.8513828126070514
Weighted FMeasure: 0.9302863822326125
Iteration time: 18.0
Weighted AreaUnderPRC: 0.9617915276413149
Mean absolute error: 0.1440000000000002
Coverage of cases: 99.42857142857143
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 75.0
Weighted Recall: 0.9314285714285714
Weighted FalsePositiveRate: 0.10801587301587302
Kappa statistic: 0.8469387755102039
Training time: 17.0
		
Iteration: 8
Labeled set size: 175
Unlabelled set size: 1
	
Mean region size: 71.14285714285714
Incorrectly Classified Instances: 8.571428571428571
Correctly Classified Instances: 91.42857142857143
Weighted Precision: 0.9140451041964031
Weighted AreaUnderROC: 0.9480583900226758
Root mean squared error: 0.2528692491048627
Relative absolute error: 25.485714285714305
Root relative squared error: 50.57384982097254
Weighted TruePositiveRate: 0.9142857142857143
Weighted MatthewsCorrelation: 0.8134029515918056
Weighted FMeasure: 0.9141333333333334
Iteration time: 21.0
Weighted AreaUnderPRC: 0.9439593469101789
Mean absolute error: 0.12742857142857153
Coverage of cases: 97.71428571428571
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 96.0
Weighted Recall: 0.9142857142857143
Weighted FalsePositiveRate: 0.10376984126984126
Kappa statistic: 0.8133399701343952
Training time: 20.0
		
Time end:Tue Oct 31 13.17.08 EET 2017