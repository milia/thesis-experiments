Tue Oct 31 13.17.50 EET 2017
Dataset: ionosphere
Test set size: 175
Initial Labelled set size: 35
Initial Unlabelled set size: 141
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Tue Oct 31 13.17.50 EET 2017
		
Iteration: 1
Labeled set size: 35
Unlabelled set size: 141
	
Mean region size: 94.85714285714286
Incorrectly Classified Instances: 16.0
Correctly Classified Instances: 84.0
Weighted Precision: 0.8528
Weighted AreaUnderROC: 0.9401218820861676
Root mean squared error: 0.3295017884191655
Relative absolute error: 54.40000000000006
Root relative squared error: 65.90035768383309
Weighted TruePositiveRate: 0.84
Weighted MatthewsCorrelation: 0.6735753140545634
Weighted FMeasure: 0.8424282198523378
Iteration time: 4.0
Weighted AreaUnderPRC: 0.9324449707165978
Mean absolute error: 0.2720000000000003
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 4.0
Weighted Recall: 0.84
Weighted FalsePositiveRate: 0.14555555555555555
Kappa statistic: 0.6666666666666666
Training time: 3.0
		
Iteration: 2
Labeled set size: 55
Unlabelled set size: 121
	
Mean region size: 88.0
Incorrectly Classified Instances: 12.571428571428571
Correctly Classified Instances: 87.42857142857143
Weighted Precision: 0.8741818181818181
Weighted AreaUnderROC: 0.9474206349206349
Root mean squared error: 0.31048349392520025
Relative absolute error: 45.371428571428645
Root relative squared error: 62.09669878504005
Weighted TruePositiveRate: 0.8742857142857143
Weighted MatthewsCorrelation: 0.7231625095035799
Weighted FMeasure: 0.8721917007597896
Iteration time: 6.0
Weighted AreaUnderPRC: 0.9395410621817019
Mean absolute error: 0.22685714285714323
Coverage of cases: 99.42857142857143
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 10.0
Weighted Recall: 0.8742857142857143
Weighted FalsePositiveRate: 0.17488095238095236
Kappa statistic: 0.7193877551020409
Training time: 6.0
		
Iteration: 3
Labeled set size: 75
Unlabelled set size: 101
	
Mean region size: 84.85714285714286
Incorrectly Classified Instances: 13.142857142857142
Correctly Classified Instances: 86.85714285714286
Weighted Precision: 0.8718400000000001
Weighted AreaUnderROC: 0.952735260770975
Root mean squared error: 0.30622120855915336
Relative absolute error: 41.714285714285786
Root relative squared error: 61.24424171183067
Weighted TruePositiveRate: 0.8685714285714285
Weighted MatthewsCorrelation: 0.7115124735378854
Weighted FMeasure: 0.8646159590754638
Iteration time: 8.0
Weighted AreaUnderPRC: 0.944078790460401
Mean absolute error: 0.20857142857142894
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 18.0
Weighted Recall: 0.8685714285714285
Weighted FalsePositiveRate: 0.19892857142857143
Kappa statistic: 0.7012987012987012
Training time: 7.0
		
Iteration: 4
Labeled set size: 95
Unlabelled set size: 81
	
Mean region size: 77.71428571428571
Incorrectly Classified Instances: 9.142857142857142
Correctly Classified Instances: 90.85714285714286
Weighted Precision: 0.9098181818181819
Weighted AreaUnderROC: 0.9441609977324263
Root mean squared error: 0.2940359550414588
Relative absolute error: 36.914285714285725
Root relative squared error: 58.80719100829176
Weighted TruePositiveRate: 0.9085714285714286
Weighted MatthewsCorrelation: 0.8000946913656628
Weighted FMeasure: 0.9070485096434833
Iteration time: 9.0
Weighted AreaUnderPRC: 0.939527215676396
Mean absolute error: 0.1845714285714286
Coverage of cases: 99.42857142857143
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 27.0
Weighted Recall: 0.9085714285714286
Weighted FalsePositiveRate: 0.13476190476190475
Kappa statistic: 0.7959183673469388
Training time: 9.0
		
Iteration: 5
Labeled set size: 115
Unlabelled set size: 61
	
Mean region size: 82.0
Incorrectly Classified Instances: 10.857142857142858
Correctly Classified Instances: 89.14285714285714
Weighted Precision: 0.8909106984969053
Weighted AreaUnderROC: 0.948625283446712
Root mean squared error: 0.2993325909419151
Relative absolute error: 38.85714285714289
Root relative squared error: 59.866518188383026
Weighted TruePositiveRate: 0.8914285714285715
Weighted MatthewsCorrelation: 0.7617400717626528
Weighted FMeasure: 0.8903706376989425
Iteration time: 12.0
Weighted AreaUnderPRC: 0.9355490146170733
Mean absolute error: 0.19428571428571445
Coverage of cases: 99.42857142857143
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 39.0
Weighted Recall: 0.8914285714285715
Weighted FalsePositiveRate: 0.1444047619047619
Kappa statistic: 0.7602221100454316
Training time: 11.0
		
Iteration: 6
Labeled set size: 135
Unlabelled set size: 41
	
Mean region size: 76.57142857142857
Incorrectly Classified Instances: 8.571428571428571
Correctly Classified Instances: 91.42857142857143
Weighted Precision: 0.9142646625405245
Weighted AreaUnderROC: 0.9704506802721088
Root mean squared error: 0.2665118598272342
Relative absolute error: 31.200000000000017
Root relative squared error: 53.30237196544684
Weighted TruePositiveRate: 0.9142857142857143
Weighted MatthewsCorrelation: 0.8123204218132938
Weighted FMeasure: 0.9134505034465338
Iteration time: 15.0
Weighted AreaUnderPRC: 0.9641095796286819
Mean absolute error: 0.15600000000000008
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 54.0
Weighted Recall: 0.9142857142857143
Weighted FalsePositiveRate: 0.11765873015873016
Kappa statistic: 0.8107016658253406
Training time: 14.0
		
Iteration: 7
Labeled set size: 155
Unlabelled set size: 21
	
Mean region size: 85.71428571428571
Incorrectly Classified Instances: 10.285714285714286
Correctly Classified Instances: 89.71428571428571
Weighted Precision: 0.8966465343687086
Weighted AreaUnderROC: 0.95578231292517
Root mean squared error: 0.28284271247461873
Relative absolute error: 37.94285714285719
Root relative squared error: 56.56854249492375
Weighted TruePositiveRate: 0.8971428571428571
Weighted MatthewsCorrelation: 0.7754666623413939
Weighted FMeasure: 0.8967684841564374
Iteration time: 17.0
Weighted AreaUnderPRC: 0.950303055008676
Mean absolute error: 0.18971428571428597
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 71.0
Weighted Recall: 0.8971428571428571
Weighted FalsePositiveRate: 0.1273015873015873
Kappa statistic: 0.7752247752247752
Training time: 16.0
		
Iteration: 8
Labeled set size: 175
Unlabelled set size: 1
	
Mean region size: 80.85714285714286
Incorrectly Classified Instances: 8.571428571428571
Correctly Classified Instances: 91.42857142857143
Weighted Precision: 0.9146396396396397
Weighted AreaUnderROC: 0.9598214285714286
Root mean squared error: 0.2630589287593179
Relative absolute error: 33.028571428571446
Root relative squared error: 52.61178575186358
Weighted TruePositiveRate: 0.9142857142857143
Weighted MatthewsCorrelation: 0.8146939463542284
Weighted FMeasure: 0.9144309876063696
Iteration time: 20.0
Weighted AreaUnderPRC: 0.9532156626065563
Mean absolute error: 0.16514285714285723
Coverage of cases: 99.42857142857143
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 91.0
Weighted Recall: 0.9142857142857143
Weighted FalsePositiveRate: 0.09682539682539681
Kappa statistic: 0.81463173504696
Training time: 20.0
		
Time end:Tue Oct 31 13.17.50 EET 2017