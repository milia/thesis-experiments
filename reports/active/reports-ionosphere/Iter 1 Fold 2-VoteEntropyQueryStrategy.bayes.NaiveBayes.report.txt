Tue Oct 31 13.17.51 EET 2017
Dataset: ionosphere
Test set size: 175
Initial Labelled set size: 35
Initial Unlabelled set size: 141
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Tue Oct 31 13.17.51 EET 2017
		
Iteration: 1
Labeled set size: 35
Unlabelled set size: 141
	
Mean region size: 53.142857142857146
Incorrectly Classified Instances: 15.428571428571429
Correctly Classified Instances: 84.57142857142857
Weighted Precision: 0.845224093634028
Weighted AreaUnderROC: 0.917375283446712
Root mean squared error: 0.3805329680513955
Relative absolute error: 30.912757657556256
Root relative squared error: 76.1065936102791
Weighted TruePositiveRate: 0.8457142857142858
Weighted MatthewsCorrelation: 0.6640633643962476
Weighted FMeasure: 0.84544
Iteration time: 4.0
Weighted AreaUnderPRC: 0.9252510518809134
Mean absolute error: 0.15456378828778128
Coverage of cases: 88.0
Instances selection time: 4.0
Test time: 6.0
Accumulative iteration time: 4.0
Weighted Recall: 0.8457142857142858
Weighted FalsePositiveRate: 0.18400793650793648
Kappa statistic: 0.6640119462419115
Training time: 0.0
		
Iteration: 2
Labeled set size: 55
Unlabelled set size: 121
	
Mean region size: 54.57142857142857
Incorrectly Classified Instances: 15.428571428571429
Correctly Classified Instances: 84.57142857142857
Weighted Precision: 0.847723102585488
Weighted AreaUnderROC: 0.9206349206349205
Root mean squared error: 0.38246227321232157
Relative absolute error: 32.036267288784856
Root relative squared error: 76.49245464246431
Weighted TruePositiveRate: 0.8457142857142858
Weighted MatthewsCorrelation: 0.669084006979606
Weighted FMeasure: 0.846461117541829
Iteration time: 4.0
Weighted AreaUnderPRC: 0.9261860922294592
Mean absolute error: 0.1601813364439243
Coverage of cases: 88.0
Instances selection time: 3.0
Test time: 6.0
Accumulative iteration time: 8.0
Weighted Recall: 0.8457142857142858
Weighted FalsePositiveRate: 0.17011904761904761
Kappa statistic: 0.6686303387334316
Training time: 1.0
		
Iteration: 3
Labeled set size: 75
Unlabelled set size: 101
	
Mean region size: 54.285714285714285
Incorrectly Classified Instances: 16.571428571428573
Correctly Classified Instances: 83.42857142857143
Weighted Precision: 0.8403809523809523
Weighted AreaUnderROC: 0.9241780045351473
Root mean squared error: 0.38424626364626435
Relative absolute error: 33.098306228822885
Root relative squared error: 76.84925272925287
Weighted TruePositiveRate: 0.8342857142857143
Weighted MatthewsCorrelation: 0.6512532252637815
Weighted FMeasure: 0.8359738054814456
Iteration time: 4.0
Weighted AreaUnderPRC: 0.9297766424734796
Mean absolute error: 0.16549153114411444
Coverage of cases: 88.0
Instances selection time: 3.0
Test time: 5.0
Accumulative iteration time: 12.0
Weighted Recall: 0.8342857142857143
Weighted FalsePositiveRate: 0.1696031746031746
Kappa statistic: 0.648910411622276
Training time: 1.0
		
Iteration: 4
Labeled set size: 95
Unlabelled set size: 81
	
Mean region size: 55.714285714285715
Incorrectly Classified Instances: 17.714285714285715
Correctly Classified Instances: 82.28571428571429
Weighted Precision: 0.8250708924103419
Weighted AreaUnderROC: 0.923327664399093
Root mean squared error: 0.3769078158096958
Relative absolute error: 32.398599202695436
Root relative squared error: 75.38156316193916
Weighted TruePositiveRate: 0.8228571428571428
Weighted MatthewsCorrelation: 0.6199588963349947
Weighted FMeasure: 0.8237146164369147
Iteration time: 3.0
Weighted AreaUnderPRC: 0.9302424706622076
Mean absolute error: 0.1619929960134772
Coverage of cases: 89.71428571428571
Instances selection time: 2.0
Test time: 6.0
Accumulative iteration time: 15.0
Weighted Recall: 0.8228571428571428
Weighted FalsePositiveRate: 0.19686507936507935
Kappa statistic: 0.6195385370643103
Training time: 1.0
		
Iteration: 5
Labeled set size: 115
Unlabelled set size: 61
	
Mean region size: 53.42857142857143
Incorrectly Classified Instances: 14.857142857142858
Correctly Classified Instances: 85.14285714285714
Weighted Precision: 0.8526433566433567
Weighted AreaUnderROC: 0.927012471655329
Root mean squared error: 0.37238471589003175
Relative absolute error: 30.00054857198381
Root relative squared error: 74.47694317800635
Weighted TruePositiveRate: 0.8514285714285714
Weighted MatthewsCorrelation: 0.680009255387445
Weighted FMeasure: 0.851920045045045
Iteration time: 3.0
Weighted AreaUnderPRC: 0.9374513207655527
Mean absolute error: 0.15000274285991905
Coverage of cases: 86.85714285714286
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 18.0
Weighted Recall: 0.8514285714285714
Weighted FalsePositiveRate: 0.1669047619047619
Kappa statistic: 0.6798029556650246
Training time: 1.0
		
Iteration: 6
Labeled set size: 135
Unlabelled set size: 41
	
Mean region size: 54.0
Incorrectly Classified Instances: 18.285714285714285
Correctly Classified Instances: 81.71428571428571
Weighted Precision: 0.8247453954496208
Weighted AreaUnderROC: 0.9220521541950114
Root mean squared error: 0.41123756001039996
Relative absolute error: 36.854595449562794
Root relative squared error: 82.24751200207999
Weighted TruePositiveRate: 0.8171428571428572
Weighted MatthewsCorrelation: 0.6167796349533392
Weighted FMeasure: 0.8192150359314538
Iteration time: 3.0
Weighted AreaUnderPRC: 0.9330311220357732
Mean absolute error: 0.18427297724781397
Coverage of cases: 85.14285714285714
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 21.0
Weighted Recall: 0.8171428571428572
Weighted FalsePositiveRate: 0.18619047619047616
Kappa statistic: 0.613899613899614
Training time: 2.0
		
Iteration: 7
Labeled set size: 155
Unlabelled set size: 21
	
Mean region size: 54.857142857142854
Incorrectly Classified Instances: 11.428571428571429
Correctly Classified Instances: 88.57142857142857
Weighted Precision: 0.884979544126242
Weighted AreaUnderROC: 0.9284297052154195
Root mean squared error: 0.33909580429126646
Relative absolute error: 26.011198855451816
Root relative squared error: 67.8191608582533
Weighted TruePositiveRate: 0.8857142857142857
Weighted MatthewsCorrelation: 0.7494396893960719
Weighted FMeasure: 0.8848432556801842
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9398386415514316
Mean absolute error: 0.13005599427725908
Coverage of cases: 89.14285714285714
Instances selection time: 0.0
Test time: 6.0
Accumulative iteration time: 23.0
Weighted Recall: 0.8857142857142857
Weighted FalsePositiveRate: 0.14761904761904762
Kappa statistic: 0.7484909456740442
Training time: 2.0
		
Iteration: 8
Labeled set size: 175
Unlabelled set size: 1
	
Mean region size: 54.57142857142857
Incorrectly Classified Instances: 11.428571428571429
Correctly Classified Instances: 88.57142857142857
Weighted Precision: 0.884979544126242
Weighted AreaUnderROC: 0.9304138321995464
Root mean squared error: 0.32429889004612433
Relative absolute error: 24.001235875517153
Root relative squared error: 64.85977800922487
Weighted TruePositiveRate: 0.8857142857142857
Weighted MatthewsCorrelation: 0.7494396893960719
Weighted FMeasure: 0.8848432556801842
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9414811494917348
Mean absolute error: 0.12000617937758577
Coverage of cases: 90.85714285714286
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 25.0
Weighted Recall: 0.8857142857142857
Weighted FalsePositiveRate: 0.14761904761904762
Kappa statistic: 0.7484909456740442
Training time: 1.0
		
Time end:Tue Oct 31 13.17.51 EET 2017