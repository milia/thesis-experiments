Tue Oct 31 13.15.57 EET 2017
Dataset: ionosphere
Test set size: 175
Initial Labelled set size: 35
Initial Unlabelled set size: 141
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Tue Oct 31 13.15.57 EET 2017
		
Iteration: 1
Labeled set size: 35
Unlabelled set size: 141
	
Mean region size: 50.0
Incorrectly Classified Instances: 17.714285714285715
Correctly Classified Instances: 82.28571428571429
Weighted Precision: 0.8464748201438849
Weighted AreaUnderROC: 0.7609126984126985
Root mean squared error: 0.420883424647321
Relative absolute error: 35.42857142857142
Root relative squared error: 84.1766849294642
Weighted TruePositiveRate: 0.8228571428571428
Weighted MatthewsCorrelation: 0.6196491347384232
Weighted FMeasure: 0.8082289025715319
Iteration time: 18.0
Weighted AreaUnderPRC: 0.7478941189905218
Mean absolute error: 0.17714285714285713
Coverage of cases: 82.28571428571429
Instances selection time: 6.0
Test time: 2.0
Accumulative iteration time: 18.0
Weighted Recall: 0.8228571428571428
Weighted FalsePositiveRate: 0.301031746031746
Kappa statistic: 0.575807334428024
Training time: 12.0
		
Iteration: 2
Labeled set size: 55
Unlabelled set size: 121
	
Mean region size: 50.0
Incorrectly Classified Instances: 17.714285714285715
Correctly Classified Instances: 82.28571428571429
Weighted Precision: 0.8532415519399249
Weighted AreaUnderROC: 0.757440476190476
Root mean squared error: 0.420883424647321
Relative absolute error: 35.42857142857142
Root relative squared error: 84.1766849294642
Weighted TruePositiveRate: 0.8228571428571428
Weighted MatthewsCorrelation: 0.6246505072066502
Weighted FMeasure: 0.8065294812762315
Iteration time: 17.0
Weighted AreaUnderPRC: 0.7477279456463436
Mean absolute error: 0.17714285714285713
Coverage of cases: 82.28571428571429
Instances selection time: 5.0
Test time: 8.0
Accumulative iteration time: 35.0
Weighted Recall: 0.8228571428571428
Weighted FalsePositiveRate: 0.30797619047619046
Kappa statistic: 0.5725317153888582
Training time: 12.0
		
Iteration: 3
Labeled set size: 75
Unlabelled set size: 101
	
Mean region size: 50.0
Incorrectly Classified Instances: 23.428571428571427
Correctly Classified Instances: 76.57142857142857
Weighted Precision: 0.8284967320261437
Weighted AreaUnderROC: 0.6746031746031746
Root mean squared error: 0.48403069560278333
Relative absolute error: 46.85714285714286
Root relative squared error: 96.80613912055667
Weighted TruePositiveRate: 0.7657142857142857
Weighted MatthewsCorrelation: 0.5055968524395742
Weighted FMeasure: 0.7273340732519422
Iteration time: 21.0
Weighted AreaUnderPRC: 0.6785538748832866
Mean absolute error: 0.2342857142857143
Coverage of cases: 76.57142857142857
Instances selection time: 4.0
Test time: 4.0
Accumulative iteration time: 56.0
Weighted Recall: 0.7657142857142857
Weighted FalsePositiveRate: 0.4165079365079365
Kappa statistic: 0.40717177559282813
Training time: 17.0
		
Iteration: 4
Labeled set size: 95
Unlabelled set size: 81
	
Mean region size: 50.0
Incorrectly Classified Instances: 26.285714285714285
Correctly Classified Instances: 73.71428571428571
Weighted Precision: 0.8136708860759494
Weighted AreaUnderROC: 0.6349206349206349
Root mean squared error: 0.5126959555693246
Relative absolute error: 52.57142857142857
Root relative squared error: 102.53919111386492
Weighted TruePositiveRate: 0.7371428571428571
Weighted MatthewsCorrelation: 0.4373555618514747
Weighted FMeasure: 0.683962962962963
Iteration time: 29.0
Weighted AreaUnderPRC: 0.6454423146473779
Mean absolute error: 0.26285714285714284
Coverage of cases: 73.71428571428571
Instances selection time: 7.0
Test time: 1.0
Accumulative iteration time: 85.0
Weighted Recall: 0.7371428571428571
Weighted FalsePositiveRate: 0.4673015873015873
Kappa statistic: 0.32113341204250284
Training time: 22.0
		
Iteration: 5
Labeled set size: 115
Unlabelled set size: 61
	
Mean region size: 50.0
Incorrectly Classified Instances: 26.857142857142858
Correctly Classified Instances: 73.14285714285714
Weighted Precision: 0.810817610062893
Weighted AreaUnderROC: 0.626984126984127
Root mean squared error: 0.5182387756347729
Relative absolute error: 53.714285714285715
Root relative squared error: 103.64775512695459
Weighted TruePositiveRate: 0.7314285714285714
Weighted MatthewsCorrelation: 0.42296084571634357
Weighted FMeasure: 0.6748264748470268
Iteration time: 25.0
Weighted AreaUnderPRC: 0.6389318957771788
Mean absolute error: 0.26857142857142857
Coverage of cases: 73.14285714285714
Instances selection time: 7.0
Test time: 1.0
Accumulative iteration time: 110.0
Weighted Recall: 0.7314285714285714
Weighted FalsePositiveRate: 0.4774603174603175
Kappa statistic: 0.3034973325429758
Training time: 18.0
		
Iteration: 6
Labeled set size: 135
Unlabelled set size: 41
	
Mean region size: 50.0
Incorrectly Classified Instances: 20.0
Correctly Classified Instances: 80.0
Weighted Precision: 0.8476190476190475
Weighted AreaUnderROC: 0.7222222222222222
Root mean squared error: 0.4472135954999579
Relative absolute error: 40.0
Root relative squared error: 89.44271909999159
Weighted TruePositiveRate: 0.8
Weighted MatthewsCorrelation: 0.5819143739626463
Weighted FMeasure: 0.775051975051975
Iteration time: 26.0
Weighted AreaUnderPRC: 0.7196190476190476
Mean absolute error: 0.2
Coverage of cases: 80.0
Instances selection time: 2.0
Test time: 8.0
Accumulative iteration time: 136.0
Weighted Recall: 0.8
Weighted FalsePositiveRate: 0.35555555555555557
Kappa statistic: 0.5059288537549409
Training time: 24.0
		
Iteration: 7
Labeled set size: 155
Unlabelled set size: 21
	
Mean region size: 50.0
Incorrectly Classified Instances: 14.857142857142858
Correctly Classified Instances: 85.14285714285714
Weighted Precision: 0.8678121587186021
Weighted AreaUnderROC: 0.8005952380952382
Root mean squared error: 0.38544964466377263
Relative absolute error: 29.714285714285715
Root relative squared error: 77.08992893275453
Weighted TruePositiveRate: 0.8514285714285714
Weighted MatthewsCorrelation: 0.6813135880891331
Weighted FMeasure: 0.8423577235772357
Iteration time: 28.0
Weighted AreaUnderPRC: 0.7846632482188362
Mean absolute error: 0.14857142857142858
Coverage of cases: 85.14285714285714
Instances selection time: 3.0
Test time: 4.0
Accumulative iteration time: 164.0
Weighted Recall: 0.8514285714285714
Weighted FalsePositiveRate: 0.2502380952380952
Kappa statistic: 0.6509129967776583
Training time: 25.0
		
Iteration: 8
Labeled set size: 175
Unlabelled set size: 1
	
Mean region size: 50.0
Incorrectly Classified Instances: 13.142857142857142
Correctly Classified Instances: 86.85714285714286
Weighted Precision: 0.8718400000000001
Weighted AreaUnderROC: 0.8348214285714286
Root mean squared error: 0.3625307868699863
Relative absolute error: 26.285714285714285
Root relative squared error: 72.50615737399725
Weighted TruePositiveRate: 0.8685714285714285
Weighted MatthewsCorrelation: 0.7115124735378854
Weighted FMeasure: 0.8646159590754638
Iteration time: 33.0
Weighted AreaUnderPRC: 0.8101257142857142
Mean absolute error: 0.13142857142857142
Coverage of cases: 86.85714285714286
Instances selection time: 8.0
Test time: 2.0
Accumulative iteration time: 197.0
Weighted Recall: 0.8685714285714285
Weighted FalsePositiveRate: 0.19892857142857143
Kappa statistic: 0.7012987012987012
Training time: 25.0
		
Time end:Tue Oct 31 13.15.58 EET 2017