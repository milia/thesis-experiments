Tue Oct 31 13.17.28 EET 2017
Dataset: ionosphere
Test set size: 176
Initial Labelled set size: 35
Initial Unlabelled set size: 140
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Tue Oct 31 13.17.28 EET 2017
		
Iteration: 1
Labeled set size: 35
Unlabelled set size: 140
	
Mean region size: 84.375
Incorrectly Classified Instances: 7.386363636363637
Correctly Classified Instances: 92.61363636363636
Weighted Precision: 0.9288530799801292
Weighted AreaUnderROC: 0.9712038207613428
Root mean squared error: 0.29008227672977055
Relative absolute error: 41.47727272727273
Root relative squared error: 58.01645534595411
Weighted TruePositiveRate: 0.9261363636363636
Weighted MatthewsCorrelation: 0.8396176373487361
Weighted FMeasure: 0.9247098646034815
Iteration time: 4.0
Weighted AreaUnderPRC: 0.9650952753311928
Mean absolute error: 0.20738636363636365
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 4.0
Weighted Recall: 0.9261363636363636
Weighted FalsePositiveRate: 0.11843865328378601
Kappa statistic: 0.8340586016826226
Training time: 3.0
		
Iteration: 2
Labeled set size: 55
Unlabelled set size: 120
	
Mean region size: 95.17045454545455
Incorrectly Classified Instances: 10.227272727272727
Correctly Classified Instances: 89.77272727272727
Weighted Precision: 0.9017444867201063
Weighted AreaUnderROC: 0.9542070515521843
Root mean squared error: 0.31033339843113583
Relative absolute error: 50.79545454545461
Root relative squared error: 62.06667968622717
Weighted TruePositiveRate: 0.8977272727272727
Weighted MatthewsCorrelation: 0.7841597348943817
Weighted FMeasure: 0.8986570247933884
Iteration time: 6.0
Weighted AreaUnderPRC: 0.9450167233281735
Mean absolute error: 0.25397727272727305
Coverage of cases: 99.43181818181819
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 10.0
Weighted Recall: 0.8977272727272727
Weighted FalsePositiveRate: 0.09916005823085468
Kappa statistic: 0.7820883202641353
Training time: 5.0
		
Iteration: 3
Labeled set size: 75
Unlabelled set size: 100
	
Mean region size: 88.63636363636364
Incorrectly Classified Instances: 13.636363636363637
Correctly Classified Instances: 86.36363636363636
Weighted Precision: 0.8730065651863764
Weighted AreaUnderROC: 0.9447253827784802
Root mean squared error: 0.31442588373675007
Relative absolute error: 47.04545454545457
Root relative squared error: 62.885176747350016
Weighted TruePositiveRate: 0.8636363636363636
Weighted MatthewsCorrelation: 0.718535844733444
Weighted FMeasure: 0.8654931669637552
Iteration time: 9.0
Weighted AreaUnderPRC: 0.9372315885020522
Mean absolute error: 0.23522727272727284
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 19.0
Weighted Recall: 0.8636363636363636
Weighted FalsePositiveRate: 0.12518995262358093
Kappa statistic: 0.7133939476184014
Training time: 8.0
		
Iteration: 4
Labeled set size: 95
Unlabelled set size: 80
	
Mean region size: 86.64772727272727
Incorrectly Classified Instances: 7.386363636363637
Correctly Classified Instances: 92.61363636363636
Weighted Precision: 0.9288442018221429
Weighted AreaUnderROC: 0.9740834386852086
Root mean squared error: 0.2515858790660845
Relative absolute error: 34.545454545454575
Root relative squared error: 50.3171758132169
Weighted TruePositiveRate: 0.9261363636363636
Weighted MatthewsCorrelation: 0.8436338643332253
Weighted FMeasure: 0.9267104135200228
Iteration time: 12.0
Weighted AreaUnderPRC: 0.9698922243583852
Mean absolute error: 0.17272727272727287
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 31.0
Weighted Recall: 0.9261363636363636
Weighted FalsePositiveRate: 0.06927444482754218
Kappa statistic: 0.842076200993926
Training time: 11.0
		
Iteration: 5
Labeled set size: 115
Unlabelled set size: 60
	
Mean region size: 82.67045454545455
Incorrectly Classified Instances: 5.113636363636363
Correctly Classified Instances: 94.88636363636364
Weighted Precision: 0.9512620073281838
Weighted AreaUnderROC: 0.9757690686894226
Root mean squared error: 0.23907967481383868
Relative absolute error: 32.04545454545457
Root relative squared error: 47.815934962767734
Weighted TruePositiveRate: 0.9488636363636364
Weighted MatthewsCorrelation: 0.8923156873242575
Weighted FMeasure: 0.949261055513862
Iteration time: 14.0
Weighted AreaUnderPRC: 0.9716059841230841
Mean absolute error: 0.16022727272727286
Coverage of cases: 99.43181818181819
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 45.0
Weighted Recall: 0.9488636363636364
Weighted FalsePositiveRate: 0.04255657076453537
Kappa statistic: 0.890668139149641
Training time: 13.0
		
Iteration: 6
Labeled set size: 135
Unlabelled set size: 40
	
Mean region size: 78.125
Incorrectly Classified Instances: 5.681818181818182
Correctly Classified Instances: 94.31818181818181
Weighted Precision: 0.9431818181818182
Weighted AreaUnderROC: 0.9787189211967973
Root mean squared error: 0.23294068694919653
Relative absolute error: 28.29545454545455
Root relative squared error: 46.588137389839304
Weighted TruePositiveRate: 0.9431818181818182
Weighted MatthewsCorrelation: 0.876387133024301
Weighted FMeasure: 0.9431818181818182
Iteration time: 18.0
Weighted AreaUnderPRC: 0.977699728036108
Mean absolute error: 0.14147727272727276
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 63.0
Weighted Recall: 0.9431818181818182
Weighted FalsePositiveRate: 0.06679468515751702
Kappa statistic: 0.8763871330243013
Training time: 17.0
		
Iteration: 7
Labeled set size: 155
Unlabelled set size: 20
	
Mean region size: 78.4090909090909
Incorrectly Classified Instances: 3.977272727272727
Correctly Classified Instances: 96.02272727272727
Weighted Precision: 0.9612086776859503
Weighted AreaUnderROC: 0.9828627616238236
Root mean squared error: 0.21226591384821578
Relative absolute error: 26.25
Root relative squared error: 42.453182769643156
Weighted TruePositiveRate: 0.9602272727272727
Weighted MatthewsCorrelation: 0.9149893988661607
Weighted FMeasure: 0.9604222167445653
Iteration time: 21.0
Weighted AreaUnderPRC: 0.9799085555376504
Mean absolute error: 0.13125
Coverage of cases: 99.43181818181819
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 84.0
Weighted Recall: 0.9602272727272727
Weighted FalsePositiveRate: 0.03622109208392394
Kappa statistic: 0.9143730886850152
Training time: 21.0
		
Time end:Tue Oct 31 13.17.28 EET 2017