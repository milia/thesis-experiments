Tue Oct 31 13.17.21 EET 2017
Dataset: ionosphere
Test set size: 176
Initial Labelled set size: 35
Initial Unlabelled set size: 140
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Tue Oct 31 13.17.21 EET 2017
		
Iteration: 1
Labeled set size: 35
Unlabelled set size: 140
	
Mean region size: 50.0
Incorrectly Classified Instances: 25.568181818181817
Correctly Classified Instances: 74.43181818181819
Weighted Precision: 0.8171389528193326
Weighted AreaUnderROC: 0.6428571428571429
Root mean squared error: 0.49253282655966524
Relative absolute error: 53.762711575211256
Root relative squared error: 98.50656531193304
Weighted TruePositiveRate: 0.7443181818181818
Weighted MatthewsCorrelation: 0.45203978125120226
Weighted FMeasure: 0.6945236497819524
Iteration time: 7.0
Weighted AreaUnderPRC: 0.6529796036457789
Mean absolute error: 0.26881355787605626
Coverage of cases: 74.43181818181819
Instances selection time: 7.0
Test time: 8.0
Accumulative iteration time: 7.0
Weighted Recall: 0.7443181818181818
Weighted FalsePositiveRate: 0.4586038961038961
Kappa statistic: 0.3393393393393392
Training time: 0.0
		
Iteration: 2
Labeled set size: 55
Unlabelled set size: 120
	
Mean region size: 50.0
Incorrectly Classified Instances: 14.204545454545455
Correctly Classified Instances: 85.79545454545455
Weighted Precision: 0.868586432506887
Weighted AreaUnderROC: 0.8123331928641662
Root mean squared error: 0.3706317507734446
Relative absolute error: 30.911262531328394
Root relative squared error: 74.12635015468892
Weighted TruePositiveRate: 0.8579545454545454
Weighted MatthewsCorrelation: 0.6911164814387856
Weighted FMeasure: 0.85085091810726
Iteration time: 7.0
Weighted AreaUnderPRC: 0.7940428227075954
Mean absolute error: 0.15455631265664196
Coverage of cases: 85.79545454545455
Instances selection time: 7.0
Test time: 9.0
Accumulative iteration time: 14.0
Weighted Recall: 0.8579545454545454
Weighted FalsePositiveRate: 0.2337095672272663
Kappa statistic: 0.6688741721854303
Training time: 0.0
		
Iteration: 3
Labeled set size: 75
Unlabelled set size: 100
	
Mean region size: 50.0
Incorrectly Classified Instances: 13.068181818181818
Correctly Classified Instances: 86.93181818181819
Weighted Precision: 0.8748668323863636
Weighted AreaUnderROC: 0.8315072341621014
Root mean squared error: 0.35701001071168315
Relative absolute error: 28.054899645808533
Root relative squared error: 71.40200214233663
Weighted TruePositiveRate: 0.8693181818181818
Weighted MatthewsCorrelation: 0.7136855417399497
Weighted FMeasure: 0.864555190800004
Iteration time: 6.0
Weighted AreaUnderPRC: 0.8097895682679063
Mean absolute error: 0.14027449822904267
Coverage of cases: 86.93181818181819
Instances selection time: 6.0
Test time: 12.0
Accumulative iteration time: 20.0
Weighted Recall: 0.8693181818181818
Weighted FalsePositiveRate: 0.20630371349397897
Kappa statistic: 0.6998813760379595
Training time: 0.0
		
Iteration: 4
Labeled set size: 95
Unlabelled set size: 80
	
Mean region size: 50.0
Incorrectly Classified Instances: 11.931818181818182
Correctly Classified Instances: 88.06818181818181
Weighted Precision: 0.8827367189262351
Weighted AreaUnderROC: 0.8508919792105633
Root mean squared error: 0.34200043032284144
Relative absolute error: 25.433458294283028
Root relative squared error: 68.40008606456828
Weighted TruePositiveRate: 0.8806818181818182
Weighted MatthewsCorrelation: 0.7373953911039396
Weighted FMeasure: 0.8777442837844599
Iteration time: 5.0
Weighted AreaUnderPRC: 0.8266067349988723
Mean absolute error: 0.12716729147141514
Coverage of cases: 88.06818181818181
Instances selection time: 5.0
Test time: 14.0
Accumulative iteration time: 25.0
Weighted Recall: 0.8806818181818182
Weighted FalsePositiveRate: 0.1788978597606916
Kappa statistic: 0.7299824663939218
Training time: 0.0
		
Iteration: 5
Labeled set size: 115
Unlabelled set size: 60
	
Mean region size: 50.0
Incorrectly Classified Instances: 14.204545454545455
Correctly Classified Instances: 85.79545454545455
Weighted Precision: 0.8651584828215263
Weighted AreaUnderROC: 0.8156342182890856
Root mean squared error: 0.3737517604940867
Relative absolute error: 29.63286713286713
Root relative squared error: 74.75035209881734
Weighted TruePositiveRate: 0.8579545454545454
Weighted MatthewsCorrelation: 0.6887680284297514
Weighted FMeasure: 0.8518462745017281
Iteration time: 5.0
Weighted AreaUnderPRC: 0.794462278704497
Mean absolute error: 0.14816433566433565
Coverage of cases: 85.79545454545455
Instances selection time: 5.0
Test time: 16.0
Accumulative iteration time: 30.0
Weighted Recall: 0.8579545454545454
Weighted FalsePositiveRate: 0.22668610887637436
Kappa statistic: 0.6713474753510605
Training time: 0.0
		
Iteration: 6
Labeled set size: 135
Unlabelled set size: 40
	
Mean region size: 50.0
Incorrectly Classified Instances: 14.204545454545455
Correctly Classified Instances: 85.79545454545455
Weighted Precision: 0.8651584828215263
Weighted AreaUnderROC: 0.8156342182890856
Root mean squared error: 0.3741992485539189
Relative absolute error: 29.45421366954194
Root relative squared error: 74.83984971078378
Weighted TruePositiveRate: 0.8579545454545454
Weighted MatthewsCorrelation: 0.6887680284297514
Weighted FMeasure: 0.8518462745017281
Iteration time: 4.0
Weighted AreaUnderPRC: 0.794462278704497
Mean absolute error: 0.1472710683477097
Coverage of cases: 85.79545454545455
Instances selection time: 3.0
Test time: 20.0
Accumulative iteration time: 34.0
Weighted Recall: 0.8579545454545454
Weighted FalsePositiveRate: 0.22668610887637436
Kappa statistic: 0.6713474753510605
Training time: 1.0
		
Iteration: 7
Labeled set size: 155
Unlabelled set size: 20
	
Mean region size: 50.0
Incorrectly Classified Instances: 14.772727272727273
Correctly Classified Instances: 85.22727272727273
Weighted Precision: 0.8603573907009022
Weighted AreaUnderROC: 0.8076977103525778
Root mean squared error: 0.3819502143456896
Relative absolute error: 30.442964678633615
Root relative squared error: 76.39004286913791
Weighted TruePositiveRate: 0.8522727272727273
Weighted MatthewsCorrelation: 0.6762726937061966
Weighted FMeasure: 0.8454110779930453
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7868942833084489
Mean absolute error: 0.15221482339316808
Coverage of cases: 85.22727272727273
Instances selection time: 2.0
Test time: 23.0
Accumulative iteration time: 36.0
Weighted Recall: 0.8522727272727273
Weighted FalsePositiveRate: 0.23687730656757203
Kappa statistic: 0.6569200779727096
Training time: 0.0
		
Time end:Tue Oct 31 13.17.21 EET 2017