Tue Oct 31 13.16.15 EET 2017
Dataset: ionosphere
Test set size: 176
Initial Labelled set size: 35
Initial Unlabelled set size: 140
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Tue Oct 31 13.16.15 EET 2017
		
Iteration: 1
Labeled set size: 35
Unlabelled set size: 140
	
Mean region size: 50.0
Incorrectly Classified Instances: 23.863636363636363
Correctly Classified Instances: 76.13636363636364
Weighted Precision: 0.7674864245871966
Weighted AreaUnderROC: 0.7509481668773703
Root mean squared error: 0.48850421045919723
Relative absolute error: 47.72727272727273
Root relative squared error: 97.70084209183945
Weighted TruePositiveRate: 0.7613636363636364
Weighted MatthewsCorrelation: 0.4928412898465481
Weighted FMeasure: 0.7635330578512396
Iteration time: 8.0
Weighted AreaUnderPRC: 0.7115235011216297
Mean absolute error: 0.23863636363636365
Coverage of cases: 76.13636363636364
Instances selection time: 5.0
Test time: 5.0
Accumulative iteration time: 8.0
Weighted Recall: 0.7613636363636364
Weighted FalsePositiveRate: 0.25946730260889556
Kappa statistic: 0.4915394139496492
Training time: 3.0
		
Iteration: 2
Labeled set size: 55
Unlabelled set size: 120
	
Mean region size: 51.13636363636363
Incorrectly Classified Instances: 11.363636363636363
Correctly Classified Instances: 88.63636363636364
Weighted Precision: 0.8951801538392335
Weighted AreaUnderROC: 0.8477314229526619
Root mean squared error: 0.33920026264998404
Relative absolute error: 23.863636363636363
Root relative squared error: 67.84005252999681
Weighted TruePositiveRate: 0.8863636363636364
Weighted MatthewsCorrelation: 0.7548155312182607
Weighted FMeasure: 0.881855747558227
Iteration time: 16.0
Weighted AreaUnderPRC: 0.8304642677506021
Mean absolute error: 0.11931818181818182
Coverage of cases: 88.63636363636364
Instances selection time: 6.0
Test time: 2.0
Accumulative iteration time: 24.0
Weighted Recall: 0.8863636363636364
Weighted FalsePositiveRate: 0.18977703712216984
Kappa statistic: 0.7380562583717815
Training time: 10.0
		
Iteration: 3
Labeled set size: 75
Unlabelled set size: 100
	
Mean region size: 51.13636363636363
Incorrectly Classified Instances: 10.795454545454545
Correctly Classified Instances: 89.20454545454545
Weighted Precision: 0.903319778048039
Weighted AreaUnderROC: 0.8524371400477595
Root mean squared error: 0.3299448989810819
Relative absolute error: 22.5
Root relative squared error: 65.98897979621638
Weighted TruePositiveRate: 0.8920454545454546
Weighted MatthewsCorrelation: 0.7696913748941281
Weighted FMeasure: 0.8874031686213134
Iteration time: 16.0
Weighted AreaUnderPRC: 0.8381499316312805
Mean absolute error: 0.1125
Coverage of cases: 89.20454545454545
Instances selection time: 4.0
Test time: 1.0
Accumulative iteration time: 40.0
Weighted Recall: 0.8920454545454546
Weighted FalsePositiveRate: 0.18660929778186414
Kappa statistic: 0.7502240812668062
Training time: 12.0
		
Iteration: 4
Labeled set size: 95
Unlabelled set size: 80
	
Mean region size: 55.68181818181818
Incorrectly Classified Instances: 14.204545454545455
Correctly Classified Instances: 85.79545454545455
Weighted Precision: 0.8651584828215263
Weighted AreaUnderROC: 0.8132462424497823
Root mean squared error: 0.37856053783784815
Relative absolute error: 31.818181818181827
Root relative squared error: 75.71210756756963
Weighted TruePositiveRate: 0.8579545454545454
Weighted MatthewsCorrelation: 0.6887680284297514
Weighted FMeasure: 0.8518462745017281
Iteration time: 23.0
Weighted AreaUnderPRC: 0.7912659248689387
Mean absolute error: 0.15909090909090914
Coverage of cases: 86.36363636363636
Instances selection time: 8.0
Test time: 3.0
Accumulative iteration time: 63.0
Weighted Recall: 0.8579545454545454
Weighted FalsePositiveRate: 0.22668610887637436
Kappa statistic: 0.6713474753510605
Training time: 15.0
		
Iteration: 5
Labeled set size: 115
Unlabelled set size: 60
	
Mean region size: 50.0
Incorrectly Classified Instances: 20.454545454545453
Correctly Classified Instances: 79.54545454545455
Weighted Precision: 0.7982363413819962
Weighted AreaUnderROC: 0.7423795476892822
Root mean squared error: 0.45226701686664544
Relative absolute error: 40.909090909090914
Root relative squared error: 90.4534033733291
Weighted TruePositiveRate: 0.7954545454545454
Weighted MatthewsCorrelation: 0.5408482322315525
Weighted FMeasure: 0.7844726603355228
Iteration time: 24.0
Weighted AreaUnderPRC: 0.7189897718421558
Mean absolute error: 0.20454545454545456
Coverage of cases: 79.54545454545455
Instances selection time: 6.0
Test time: 1.0
Accumulative iteration time: 87.0
Weighted Recall: 0.7954545454545454
Weighted FalsePositiveRate: 0.31069545007598104
Kappa statistic: 0.5213778516392203
Training time: 18.0
		
Iteration: 6
Labeled set size: 135
Unlabelled set size: 40
	
Mean region size: 52.55681818181818
Incorrectly Classified Instances: 15.340909090909092
Correctly Classified Instances: 84.6590909090909
Weighted Precision: 0.8640792112299465
Weighted AreaUnderROC: 0.7960387694900969
Root mean squared error: 0.3884701930767545
Relative absolute error: 31.818181818181817
Root relative squared error: 77.6940386153509
Weighted TruePositiveRate: 0.8465909090909091
Weighted MatthewsCorrelation: 0.6697593173011033
Weighted FMeasure: 0.8365478684375631
Iteration time: 28.0
Weighted AreaUnderPRC: 0.7848198189806606
Mean absolute error: 0.1590909090909091
Coverage of cases: 85.79545454545455
Instances selection time: 8.0
Test time: 1.0
Accumulative iteration time: 115.0
Weighted Recall: 0.8465909090909091
Weighted FalsePositiveRate: 0.2611154209605537
Kappa statistic: 0.636919315403423
Training time: 20.0
		
Iteration: 7
Labeled set size: 155
Unlabelled set size: 20
	
Mean region size: 50.0
Incorrectly Classified Instances: 7.954545454545454
Correctly Classified Instances: 92.04545454545455
Weighted Precision: 0.9238537003723383
Weighted AreaUnderROC: 0.8805309734513274
Root mean squared error: 0.28062296965103395
Relative absolute error: 16.837732160312804
Root relative squared error: 56.12459393020679
Weighted TruePositiveRate: 0.9204545454545454
Weighted MatthewsCorrelation: 0.8274619795640561
Weighted FMeasure: 0.9187111471228946
Iteration time: 40.0
Weighted AreaUnderPRC: 0.8730029031568391
Mean absolute error: 0.08418866080156402
Coverage of cases: 92.04545454545455
Instances selection time: 6.0
Test time: 1.0
Accumulative iteration time: 155.0
Weighted Recall: 0.9204545454545454
Weighted FalsePositiveRate: 0.12862985097498372
Kappa statistic: 0.8206434706653079
Training time: 34.0
		
Time end:Tue Oct 31 13.16.16 EET 2017