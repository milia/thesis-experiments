Tue Oct 31 13.16.41 EET 2017
Dataset: ionosphere
Test set size: 175
Initial Labelled set size: 35
Initial Unlabelled set size: 141
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Tue Oct 31 13.16.41 EET 2017
		
Iteration: 1
Labeled set size: 35
Unlabelled set size: 141
	
Mean region size: 50.0
Incorrectly Classified Instances: 16.571428571428573
Correctly Classified Instances: 83.42857142857143
Weighted Precision: 0.8329565217391305
Weighted AreaUnderROC: 0.8149801587301587
Root mean squared error: 0.4070801956792859
Relative absolute error: 33.14285714285714
Root relative squared error: 81.41603913585718
Weighted TruePositiveRate: 0.8342857142857143
Weighted MatthewsCorrelation: 0.6370418059121953
Weighted FMeasure: 0.8333598366820673
Iteration time: 12.0
Weighted AreaUnderPRC: 0.7778443064182194
Mean absolute error: 0.1657142857142857
Coverage of cases: 83.42857142857143
Instances selection time: 4.0
Test time: 1.0
Accumulative iteration time: 12.0
Weighted Recall: 0.8342857142857143
Weighted FalsePositiveRate: 0.20432539682539683
Kappa statistic: 0.6365914786967418
Training time: 8.0
		
Iteration: 2
Labeled set size: 55
Unlabelled set size: 121
	
Mean region size: 50.0
Incorrectly Classified Instances: 20.0
Correctly Classified Instances: 80.0
Weighted Precision: 0.8052631578947369
Weighted AreaUnderROC: 0.7465277777777778
Root mean squared error: 0.4392825656973987
Relative absolute error: 42.04651162790694
Root relative squared error: 87.85651313947974
Weighted TruePositiveRate: 0.8
Weighted MatthewsCorrelation: 0.5541466051858356
Weighted FMeasure: 0.7885714285714286
Iteration time: 12.0
Weighted AreaUnderPRC: 0.7235508771929826
Mean absolute error: 0.2102325581395347
Coverage of cases: 80.0
Instances selection time: 0.0
Test time: 5.0
Accumulative iteration time: 24.0
Weighted Recall: 0.8
Weighted FalsePositiveRate: 0.3069444444444444
Kappa statistic: 0.5318352059925094
Training time: 12.0
		
Iteration: 3
Labeled set size: 75
Unlabelled set size: 101
	
Mean region size: 56.857142857142854
Incorrectly Classified Instances: 13.714285714285714
Correctly Classified Instances: 86.28571428571429
Weighted Precision: 0.8870588235294118
Weighted AreaUnderROC: 0.9132653061224489
Root mean squared error: 0.32071349029490925
Relative absolute error: 25.71428571428571
Root relative squared error: 64.14269805898185
Weighted TruePositiveRate: 0.8628571428571429
Weighted MatthewsCorrelation: 0.7140055472954167
Weighted FMeasure: 0.8533586337760911
Iteration time: 16.0
Weighted AreaUnderPRC: 0.8984692677070828
Mean absolute error: 0.12857142857142856
Coverage of cases: 94.85714285714286
Instances selection time: 5.0
Test time: 1.0
Accumulative iteration time: 40.0
Weighted Recall: 0.8628571428571429
Weighted FalsePositiveRate: 0.2438095238095238
Kappa statistic: 0.6753246753246753
Training time: 11.0
		
Iteration: 4
Labeled set size: 95
Unlabelled set size: 81
	
Mean region size: 56.857142857142854
Incorrectly Classified Instances: 13.714285714285714
Correctly Classified Instances: 86.28571428571429
Weighted Precision: 0.8870588235294118
Weighted AreaUnderROC: 0.9132653061224489
Root mean squared error: 0.32071349029490925
Relative absolute error: 25.71428571428571
Root relative squared error: 64.14269805898185
Weighted TruePositiveRate: 0.8628571428571429
Weighted MatthewsCorrelation: 0.7140055472954167
Weighted FMeasure: 0.8533586337760911
Iteration time: 17.0
Weighted AreaUnderPRC: 0.8984692677070828
Mean absolute error: 0.12857142857142856
Coverage of cases: 94.85714285714286
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 57.0
Weighted Recall: 0.8628571428571429
Weighted FalsePositiveRate: 0.2438095238095238
Kappa statistic: 0.6753246753246753
Training time: 17.0
		
Iteration: 5
Labeled set size: 115
Unlabelled set size: 61
	
Mean region size: 66.57142857142857
Incorrectly Classified Instances: 12.571428571428571
Correctly Classified Instances: 87.42857142857143
Weighted Precision: 0.8949253731343284
Weighted AreaUnderROC: 0.9288548752834467
Root mean squared error: 0.31468805415613005
Relative absolute error: 27.771428571428576
Root relative squared error: 62.93761083122601
Weighted TruePositiveRate: 0.8742857142857143
Weighted MatthewsCorrelation: 0.7375275805057283
Weighted FMeasure: 0.8666103814884303
Iteration time: 24.0
Weighted AreaUnderPRC: 0.9146001312394302
Mean absolute error: 0.13885714285714287
Coverage of cases: 96.0
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 81.0
Weighted Recall: 0.8742857142857143
Weighted FalsePositiveRate: 0.22349206349206346
Kappa statistic: 0.7046186895810956
Training time: 24.0
		
Iteration: 6
Labeled set size: 135
Unlabelled set size: 41
	
Mean region size: 57.142857142857146
Incorrectly Classified Instances: 22.857142857142858
Correctly Classified Instances: 77.14285714285714
Weighted Precision: 0.809009009009009
Weighted AreaUnderROC: 0.6578798185941043
Root mean squared error: 0.4483859354416812
Relative absolute error: 45.62749731471535
Root relative squared error: 89.67718708833624
Weighted TruePositiveRate: 0.7714285714285715
Weighted MatthewsCorrelation: 0.5035810477464647
Weighted FMeasure: 0.7415384615384616
Iteration time: 37.0
Weighted AreaUnderPRC: 0.7210712664486737
Mean absolute error: 0.22813748657357674
Coverage of cases: 84.0
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 118.0
Weighted Recall: 0.7714285714285715
Weighted FalsePositiveRate: 0.39246031746031745
Kappa statistic: 0.43310657596371893
Training time: 37.0
		
Iteration: 7
Labeled set size: 155
Unlabelled set size: 21
	
Mean region size: 50.0
Incorrectly Classified Instances: 10.857142857142858
Correctly Classified Instances: 89.14285714285714
Weighted Precision: 0.8909106984969053
Weighted AreaUnderROC: 0.8735119047619047
Root mean squared error: 0.3295017884191656
Relative absolute error: 21.714285714285715
Root relative squared error: 65.90035768383312
Weighted TruePositiveRate: 0.8914285714285715
Weighted MatthewsCorrelation: 0.7617400717626528
Weighted FMeasure: 0.8903706376989425
Iteration time: 44.0
Weighted AreaUnderPRC: 0.8450034103827208
Mean absolute error: 0.10857142857142857
Coverage of cases: 89.14285714285714
Instances selection time: 3.0
Test time: 5.0
Accumulative iteration time: 162.0
Weighted Recall: 0.8914285714285715
Weighted FalsePositiveRate: 0.1444047619047619
Kappa statistic: 0.7602221100454316
Training time: 41.0
		
Iteration: 8
Labeled set size: 175
Unlabelled set size: 1
	
Mean region size: 53.142857142857146
Incorrectly Classified Instances: 11.428571428571429
Correctly Classified Instances: 88.57142857142857
Weighted Precision: 0.8860606060606061
Weighted AreaUnderROC: 0.876984126984127
Root mean squared error: 0.3275692099413302
Relative absolute error: 22.98412698412698
Root relative squared error: 65.51384198826604
Weighted TruePositiveRate: 0.8857142857142857
Weighted MatthewsCorrelation: 0.7488065701242741
Weighted FMeasure: 0.8838106370543541
Iteration time: 44.0
Weighted AreaUnderPRC: 0.8629807642353672
Mean absolute error: 0.1149206349206349
Coverage of cases: 91.42857142857143
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 206.0
Weighted Recall: 0.8857142857142857
Weighted FalsePositiveRate: 0.1615079365079365
Kappa statistic: 0.7448979591836734
Training time: 42.0
		
Time end:Tue Oct 31 13.16.41 EET 2017