Tue Oct 31 13.16.31 EET 2017
Dataset: ionosphere
Test set size: 175
Initial Labelled set size: 35
Initial Unlabelled set size: 141
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Tue Oct 31 13.16.31 EET 2017
		
Iteration: 1
Labeled set size: 35
Unlabelled set size: 141
	
Mean region size: 50.0
Incorrectly Classified Instances: 26.285714285714285
Correctly Classified Instances: 73.71428571428571
Weighted Precision: 0.8136708860759494
Weighted AreaUnderROC: 0.6349206349206349
Root mean squared error: 0.5126959555693246
Relative absolute error: 52.57142857142857
Root relative squared error: 102.53919111386492
Weighted TruePositiveRate: 0.7371428571428571
Weighted MatthewsCorrelation: 0.4373555618514747
Weighted FMeasure: 0.683962962962963
Iteration time: 21.0
Weighted AreaUnderPRC: 0.6454423146473779
Mean absolute error: 0.26285714285714284
Coverage of cases: 73.71428571428571
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 21.0
Weighted Recall: 0.7371428571428571
Weighted FalsePositiveRate: 0.4673015873015873
Kappa statistic: 0.32113341204250284
Training time: 20.0
		
Iteration: 2
Labeled set size: 55
Unlabelled set size: 121
	
Mean region size: 50.0
Incorrectly Classified Instances: 13.142857142857142
Correctly Classified Instances: 86.85714285714286
Weighted Precision: 0.8687052341597795
Weighted AreaUnderROC: 0.841765873015873
Root mean squared error: 0.3625307868699863
Relative absolute error: 26.285714285714285
Root relative squared error: 72.50615737399725
Weighted TruePositiveRate: 0.8685714285714285
Weighted MatthewsCorrelation: 0.7103107882060647
Weighted FMeasure: 0.8660548035655332
Iteration time: 18.0
Weighted AreaUnderPRC: 0.8129320564956928
Mean absolute error: 0.13142857142857142
Coverage of cases: 86.85714285714286
Instances selection time: 2.0
Test time: 1.0
Accumulative iteration time: 39.0
Weighted Recall: 0.8685714285714285
Weighted FalsePositiveRate: 0.18503968253968253
Kappa statistic: 0.7055811571940603
Training time: 16.0
		
Iteration: 3
Labeled set size: 75
Unlabelled set size: 101
	
Mean region size: 50.0
Incorrectly Classified Instances: 13.714285714285714
Correctly Classified Instances: 86.28571428571429
Weighted Precision: 0.8617416715371128
Weighted AreaUnderROC: 0.8442460317460316
Root mean squared error: 0.3703280399090206
Relative absolute error: 27.42857142857143
Root relative squared error: 74.06560798180412
Weighted TruePositiveRate: 0.8628571428571429
Weighted MatthewsCorrelation: 0.6990741188721423
Weighted FMeasure: 0.8618119068162209
Iteration time: 11.0
Weighted AreaUnderPRC: 0.8104261167237203
Mean absolute error: 0.13714285714285715
Coverage of cases: 86.28571428571429
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 50.0
Weighted Recall: 0.8628571428571429
Weighted FalsePositiveRate: 0.17436507936507933
Kappa statistic: 0.6981891348088531
Training time: 10.0
		
Iteration: 4
Labeled set size: 95
Unlabelled set size: 81
	
Mean region size: 50.0
Incorrectly Classified Instances: 13.142857142857142
Correctly Classified Instances: 86.85714285714286
Weighted Precision: 0.874251968503937
Weighted AreaUnderROC: 0.8313492063492064
Root mean squared error: 0.3625307868699863
Relative absolute error: 26.285714285714285
Root relative squared error: 72.50615737399725
Weighted TruePositiveRate: 0.8685714285714285
Weighted MatthewsCorrelation: 0.712972431413496
Weighted FMeasure: 0.8638154472464096
Iteration time: 11.0
Weighted AreaUnderPRC: 0.8090048743907011
Mean absolute error: 0.13142857142857142
Coverage of cases: 86.85714285714286
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 61.0
Weighted Recall: 0.8685714285714285
Weighted FalsePositiveRate: 0.20587301587301587
Kappa statistic: 0.6991104133961276
Training time: 10.0
		
Iteration: 5
Labeled set size: 115
Unlabelled set size: 61
	
Mean region size: 50.0
Incorrectly Classified Instances: 12.0
Correctly Classified Instances: 88.0
Weighted Precision: 0.8989473684210527
Weighted AreaUnderROC: 0.8333333333333334
Root mean squared error: 0.34641016151377546
Relative absolute error: 24.0
Root relative squared error: 69.2820323027551
Weighted TruePositiveRate: 0.88
Weighted MatthewsCorrelation: 0.7492686492653552
Weighted FMeasure: 0.8731428571428572
Iteration time: 23.0
Weighted AreaUnderPRC: 0.8221473684210525
Mean absolute error: 0.12
Coverage of cases: 88.0
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 84.0
Weighted Recall: 0.88
Weighted FalsePositiveRate: 0.21333333333333332
Kappa statistic: 0.7191011235955056
Training time: 23.0
		
Iteration: 6
Labeled set size: 135
Unlabelled set size: 41
	
Mean region size: 50.0
Incorrectly Classified Instances: 11.428571428571429
Correctly Classified Instances: 88.57142857142857
Weighted Precision: 0.9030303030303031
Weighted AreaUnderROC: 0.8412698412698413
Root mean squared error: 0.3380617018914066
Relative absolute error: 22.857142857142858
Root relative squared error: 67.61234037828132
Weighted TruePositiveRate: 0.8857142857142857
Weighted MatthewsCorrelation: 0.7610023515893882
Weighted FMeasure: 0.8796164553046705
Iteration time: 17.0
Weighted AreaUnderPRC: 0.829887445887446
Mean absolute error: 0.11428571428571428
Coverage of cases: 88.57142857142857
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 101.0
Weighted Recall: 0.8857142857142857
Weighted FalsePositiveRate: 0.20317460317460317
Kappa statistic: 0.7334754797441363
Training time: 17.0
		
Iteration: 7
Labeled set size: 155
Unlabelled set size: 21
	
Mean region size: 50.0
Incorrectly Classified Instances: 11.428571428571429
Correctly Classified Instances: 88.57142857142857
Weighted Precision: 0.9030303030303031
Weighted AreaUnderROC: 0.8412698412698413
Root mean squared error: 0.3380617018914066
Relative absolute error: 22.857142857142858
Root relative squared error: 67.61234037828132
Weighted TruePositiveRate: 0.8857142857142857
Weighted MatthewsCorrelation: 0.7610023515893882
Weighted FMeasure: 0.8796164553046705
Iteration time: 28.0
Weighted AreaUnderPRC: 0.829887445887446
Mean absolute error: 0.11428571428571428
Coverage of cases: 88.57142857142857
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 129.0
Weighted Recall: 0.8857142857142857
Weighted FalsePositiveRate: 0.20317460317460317
Kappa statistic: 0.7334754797441363
Training time: 27.0
		
Iteration: 8
Labeled set size: 175
Unlabelled set size: 1
	
Mean region size: 50.0
Incorrectly Classified Instances: 9.714285714285714
Correctly Classified Instances: 90.28571428571429
Weighted Precision: 0.9156589147286822
Weighted AreaUnderROC: 0.8650793650793651
Root mean squared error: 0.31167748898959186
Relative absolute error: 19.428571428571427
Root relative squared error: 62.33549779791837
Weighted TruePositiveRate: 0.9028571428571428
Weighted MatthewsCorrelation: 0.7962011440856127
Weighted FMeasure: 0.8987079827934067
Iteration time: 28.0
Weighted AreaUnderPRC: 0.8534874861572537
Mean absolute error: 0.09714285714285714
Coverage of cases: 90.28571428571429
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 157.0
Weighted Recall: 0.9028571428571428
Weighted FalsePositiveRate: 0.17269841269841268
Kappa statistic: 0.7759620453347389
Training time: 27.0
		
Time end:Tue Oct 31 13.16.32 EET 2017