Tue Oct 31 13.15.48 EET 2017
Dataset: ionosphere
Test set size: 175
Initial Labelled set size: 35
Initial Unlabelled set size: 140
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Tue Oct 31 13.15.48 EET 2017
		
Iteration: 1
Labeled set size: 35
Unlabelled set size: 140
	
Mean region size: 54.7
Incorrectly Classified Instances: 15.208766233766235
Correctly Classified Instances: 84.79123376623377
Weighted Precision: 0.8616074700295229
Weighted AreaUnderROC: 0.9394057401721753
Root mean squared error: 0.36586742405132205
Relative absolute error: 30.41695047368408
Root relative squared error: 73.1734848102644
Weighted TruePositiveRate: 0.8479123376623378
Weighted MatthewsCorrelation: 0.6905863825491085
Weighted FMeasure: 0.8496170801852854
Iteration time: 38.9
Weighted AreaUnderPRC: 0.946416444799944
Mean absolute error: 0.1520847523684204
Coverage of cases: 89.46233766233766
Instances selection time: 23.7
Test time: 34.6
Accumulative iteration time: 38.9
Weighted Recall: 0.8479123376623378
Weighted FalsePositiveRate: 0.14518196822842838
Kappa statistic: 0.6822016286677068
Training time: 15.2
		
Iteration: 2
Labeled set size: 55
Unlabelled set size: 120
	
Mean region size: 55.92451298701299
Incorrectly Classified Instances: 9.742207792207791
Correctly Classified Instances: 90.25779220779222
Weighted Precision: 0.9051296259100537
Weighted AreaUnderROC: 0.9509872975738967
Root mean squared error: 0.29019898018814283
Relative absolute error: 21.219723796882384
Root relative squared error: 58.039796037628584
Weighted TruePositiveRate: 0.9025779220779221
Weighted MatthewsCorrelation: 0.7912475215385413
Weighted FMeasure: 0.9025197197486359
Iteration time: 14.7
Weighted AreaUnderPRC: 0.9545306455996698
Mean absolute error: 0.10609861898441193
Coverage of cases: 94.58798701298703
Instances selection time: 11.8
Test time: 22.0
Accumulative iteration time: 53.6
Weighted Recall: 0.9025779220779221
Weighted FalsePositiveRate: 0.11246870730056571
Kappa statistic: 0.7886686981195112
Training time: 2.9
		
Iteration: 3
Labeled set size: 75
Unlabelled set size: 100
	
Mean region size: 55.896428571428565
Incorrectly Classified Instances: 7.464935064935065
Correctly Classified Instances: 92.53506493506492
Weighted Precision: 0.9267452156179674
Weighted AreaUnderROC: 0.9566834477153693
Root mean squared error: 0.25073520678517247
Relative absolute error: 16.91763355973437
Root relative squared error: 50.147041357034496
Weighted TruePositiveRate: 0.9253506493506494
Weighted MatthewsCorrelation: 0.8382609458370505
Weighted FMeasure: 0.9247668534190048
Iteration time: 11.5
Weighted AreaUnderPRC: 0.9597896650783069
Mean absolute error: 0.08458816779867184
Coverage of cases: 96.01103896103898
Instances selection time: 8.7
Test time: 19.2
Accumulative iteration time: 65.1
Weighted Recall: 0.9253506493506494
Weighted FalsePositiveRate: 0.10045863502279431
Kappa statistic: 0.8357102619777959
Training time: 2.8
		
Iteration: 4
Labeled set size: 95
Unlabelled set size: 80
	
Mean region size: 55.98116883116883
Incorrectly Classified Instances: 8.317532467532466
Correctly Classified Instances: 91.68246753246754
Weighted Precision: 0.9179516967345265
Weighted AreaUnderROC: 0.9572305249533439
Root mean squared error: 0.2596585937563125
Relative absolute error: 18.018849905978676
Root relative squared error: 51.931718751262494
Weighted TruePositiveRate: 0.9168246753246754
Weighted MatthewsCorrelation: 0.8197759638576022
Weighted FMeasure: 0.916414481343207
Iteration time: 10.2
Weighted AreaUnderPRC: 0.9627899745334583
Mean absolute error: 0.0900942495298934
Coverage of cases: 95.89707792207791
Instances selection time: 6.7
Test time: 16.6
Accumulative iteration time: 75.3
Weighted Recall: 0.9168246753246754
Weighted FalsePositiveRate: 0.10522280006129563
Kappa statistic: 0.8178548762340178
Training time: 3.5
		
Iteration: 5
Labeled set size: 115
Unlabelled set size: 60
	
Mean region size: 56.20795454545455
Incorrectly Classified Instances: 8.601623376623376
Correctly Classified Instances: 91.39837662337662
Weighted Precision: 0.915163702958505
Weighted AreaUnderROC: 0.9592497692292257
Root mean squared error: 0.2676661714089067
Relative absolute error: 19.025819044814273
Root relative squared error: 53.53323428178136
Weighted TruePositiveRate: 0.9139837662337662
Weighted MatthewsCorrelation: 0.8143220680477432
Weighted FMeasure: 0.9139016329816625
Iteration time: 8.6
Weighted AreaUnderPRC: 0.9650285457922058
Mean absolute error: 0.09512909522407136
Coverage of cases: 95.1564935064935
Instances selection time: 5.9
Test time: 16.8
Accumulative iteration time: 83.9
Weighted Recall: 0.9139837662337662
Weighted FalsePositiveRate: 0.10262420028349233
Kappa statistic: 0.813008836506361
Training time: 2.7
		
Iteration: 6
Labeled set size: 135
Unlabelled set size: 40
	
Mean region size: 56.89253246753246
Incorrectly Classified Instances: 13.213311688311688
Correctly Classified Instances: 86.78668831168831
Weighted Precision: 0.8794870447396622
Weighted AreaUnderROC: 0.95119423875745
Root mean squared error: 0.32777691551491817
Relative absolute error: 26.679925608311674
Root relative squared error: 65.55538310298364
Weighted TruePositiveRate: 0.8678668831168832
Weighted MatthewsCorrelation: 0.730377281463672
Weighted FMeasure: 0.8691334298324609
Iteration time: 9.9
Weighted AreaUnderPRC: 0.9583085925769612
Mean absolute error: 0.1333996280415584
Coverage of cases: 93.05064935064937
Instances selection time: 5.8
Test time: 14.9
Accumulative iteration time: 93.8
Weighted Recall: 0.8678668831168832
Weighted FalsePositiveRate: 0.12911231786895502
Kappa statistic: 0.7228950691240101
Training time: 4.1
		
Iteration: 7
Labeled set size: 155
Unlabelled set size: 20
	
Mean region size: 56.60941558441558
Incorrectly Classified Instances: 16.175324675324674
Correctly Classified Instances: 83.82467532467533
Weighted Precision: 0.8586656340921722
Weighted AreaUnderROC: 0.9452426103184637
Root mean squared error: 0.37287482435693015
Relative absolute error: 33.05885384363578
Root relative squared error: 74.57496487138602
Weighted TruePositiveRate: 0.8382467532467531
Weighted MatthewsCorrelation: 0.6799564301762712
Weighted FMeasure: 0.8405168104657929
Iteration time: 7.9
Weighted AreaUnderPRC: 0.9532841709187677
Mean absolute error: 0.16529426921817889
Coverage of cases: 90.03311688311689
Instances selection time: 3.6
Test time: 13.8
Accumulative iteration time: 101.7
Weighted Recall: 0.8382467532467531
Weighted FalsePositiveRate: 0.14289803853963146
Kappa statistic: 0.6676443842062709
Training time: 4.3
		
Iteration: 8
Labeled set size: 175
Unlabelled set size: 1
	
Mean region size: 55.37142857142858
Incorrectly Classified Instances: 15.885714285714286
Correctly Classified Instances: 84.11428571428571
Weighted Precision: 0.8560900184851477
Weighted AreaUnderROC: 0.9381235827664399
Root mean squared error: 0.37340309363769514
Relative absolute error: 32.06804593143293
Root relative squared error: 74.68061872753904
Weighted TruePositiveRate: 0.8411428571428571
Weighted MatthewsCorrelation: 0.6783997941368336
Weighted FMeasure: 0.843083037136019
Iteration time: 5.6
Weighted AreaUnderPRC: 0.9465496010556876
Mean absolute error: 0.1603402296571646
Coverage of cases: 88.68571428571428
Instances selection time: 1.4
Test time: 13.2
Accumulative iteration time: 74.0
Weighted Recall: 0.8411428571428571
Weighted FalsePositiveRate: 0.15046825396825397
Kappa statistic: 0.6693678782817487
Training time: 4.2
		
Time end:Tue Oct 31 13.15.55 EET 2017