Tue Oct 31 13.17.47 EET 2017
Dataset: ionosphere
Test set size: 175
Initial Labelled set size: 35
Initial Unlabelled set size: 140
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Tue Oct 31 13.17.47 EET 2017
		
Iteration: 1
Labeled set size: 35
Unlabelled set size: 140
	
Mean region size: 86.07938311688312
Incorrectly Classified Instances: 16.41103896103896
Correctly Classified Instances: 83.58896103896103
Weighted Precision: 0.8452860991121852
Weighted AreaUnderROC: 0.8998878127947343
Root mean squared error: 0.3540270198060793
Relative absolute error: 50.56870129870132
Root relative squared error: 70.80540396121587
Weighted TruePositiveRate: 0.8358896103896104
Weighted MatthewsCorrelation: 0.6498594316818902
Weighted FMeasure: 0.8325142314102306
Iteration time: 5.0
Weighted AreaUnderPRC: 0.8914251238012423
Mean absolute error: 0.2528435064935066
Coverage of cases: 98.5211038961039
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 5.0
Weighted Recall: 0.8358896103896104
Weighted FalsePositiveRate: 0.2115296230318354
Kappa statistic: 0.6363967078833291
Training time: 4.0
		
Iteration: 2
Labeled set size: 55
Unlabelled set size: 120
	
Mean region size: 82.8297077922078
Incorrectly Classified Instances: 11.73538961038961
Correctly Classified Instances: 88.2646103896104
Weighted Precision: 0.886372102666102
Weighted AreaUnderROC: 0.9322285809604077
Root mean squared error: 0.30943600771788565
Relative absolute error: 41.2129220779221
Root relative squared error: 61.887201543577135
Weighted TruePositiveRate: 0.8826461038961039
Weighted MatthewsCorrelation: 0.7483849901766791
Weighted FMeasure: 0.8819937734758257
Iteration time: 6.8
Weighted AreaUnderPRC: 0.9240348334407471
Mean absolute error: 0.2060646103896105
Coverage of cases: 98.74772727272729
Instances selection time: 0.8
Test time: 1.1
Accumulative iteration time: 11.8
Weighted Recall: 0.8826461038961039
Weighted FalsePositiveRate: 0.14322343217254724
Kappa statistic: 0.7437392971875918
Training time: 6.0
		
Iteration: 3
Labeled set size: 75
Unlabelled set size: 100
	
Mean region size: 82.31233766233767
Incorrectly Classified Instances: 11.339610389610389
Correctly Classified Instances: 88.66038961038961
Weighted Precision: 0.8907378767748175
Weighted AreaUnderROC: 0.9426019781068768
Root mean squared error: 0.301969241085609
Relative absolute error: 39.6831168831169
Root relative squared error: 60.39384821712181
Weighted TruePositiveRate: 0.886603896103896
Weighted MatthewsCorrelation: 0.7559966556435391
Weighted FMeasure: 0.8851497598965452
Iteration time: 10.0
Weighted AreaUnderPRC: 0.9354378506583979
Mean absolute error: 0.19841558441558452
Coverage of cases: 99.31753246753246
Instances selection time: 1.1
Test time: 1.8
Accumulative iteration time: 21.8
Weighted Recall: 0.886603896103896
Weighted FalsePositiveRate: 0.15010122718972274
Kappa statistic: 0.7495416593784527
Training time: 8.9
		
Iteration: 4
Labeled set size: 95
Unlabelled set size: 80
	
Mean region size: 79.94366883116882
Incorrectly Classified Instances: 9.229220779220778
Correctly Classified Instances: 90.77077922077922
Weighted Precision: 0.909098448264267
Weighted AreaUnderROC: 0.9530229717255635
Root mean squared error: 0.2802454258585965
Relative absolute error: 34.98532467532469
Root relative squared error: 56.0490851717193
Weighted TruePositiveRate: 0.9077077922077923
Weighted MatthewsCorrelation: 0.7999630799822179
Weighted FMeasure: 0.9071196867523735
Iteration time: 11.7
Weighted AreaUnderPRC: 0.9480196613303583
Mean absolute error: 0.17492662337662346
Coverage of cases: 99.20292207792207
Instances selection time: 0.5
Test time: 1.4
Accumulative iteration time: 33.5
Weighted Recall: 0.9077077922077923
Weighted FalsePositiveRate: 0.11736768123715025
Kappa statistic: 0.7974747317342408
Training time: 11.2
		
Iteration: 5
Labeled set size: 115
Unlabelled set size: 60
	
Mean region size: 78.43392857142858
Incorrectly Classified Instances: 10.59512987012987
Correctly Classified Instances: 89.40487012987012
Weighted Precision: 0.897367911166792
Weighted AreaUnderROC: 0.9440910139867155
Root mean squared error: 0.2848684269273297
Relative absolute error: 34.185129870129884
Root relative squared error: 56.97368538546594
Weighted TruePositiveRate: 0.8940487012987013
Weighted MatthewsCorrelation: 0.7735148935921854
Weighted FMeasure: 0.8938443930889234
Iteration time: 13.7
Weighted AreaUnderPRC: 0.9378452331976803
Mean absolute error: 0.1709256493506494
Coverage of cases: 98.86233766233767
Instances selection time: 0.9
Test time: 1.6
Accumulative iteration time: 47.2
Weighted Recall: 0.8940487012987013
Weighted FalsePositiveRate: 0.12705456588642428
Kappa statistic: 0.7700984298401267
Training time: 12.8
		
Iteration: 6
Labeled set size: 135
Unlabelled set size: 40
	
Mean region size: 77.325
Incorrectly Classified Instances: 9.74090909090909
Correctly Classified Instances: 90.2590909090909
Weighted Precision: 0.9038301391139768
Weighted AreaUnderROC: 0.9573625283446712
Root mean squared error: 0.2699031589820286
Relative absolute error: 31.5309090909091
Root relative squared error: 53.98063179640572
Weighted TruePositiveRate: 0.9025909090909092
Weighted MatthewsCorrelation: 0.7891109763251501
Weighted FMeasure: 0.9022213298736164
Iteration time: 14.5
Weighted AreaUnderPRC: 0.9519066999119857
Mean absolute error: 0.15765454545454552
Coverage of cases: 99.20422077922078
Instances selection time: 0.5
Test time: 1.0
Accumulative iteration time: 61.7
Weighted Recall: 0.9025909090909092
Weighted FalsePositiveRate: 0.11954465961766847
Kappa statistic: 0.7872248227935564
Training time: 14.0
		
Iteration: 7
Labeled set size: 155
Unlabelled set size: 20
	
Mean region size: 78.32564935064934
Incorrectly Classified Instances: 8.03344155844156
Correctly Classified Instances: 91.96655844155842
Weighted Precision: 0.9205619361679445
Weighted AreaUnderROC: 0.9546418161659942
Root mean squared error: 0.26612777306004143
Relative absolute error: 31.7633116883117
Root relative squared error: 53.225554612008274
Weighted TruePositiveRate: 0.9196655844155843
Weighted MatthewsCorrelation: 0.8253086783998977
Weighted FMeasure: 0.9190746815376933
Iteration time: 19.3
Weighted AreaUnderPRC: 0.9504191970972359
Mean absolute error: 0.15881655844155848
Coverage of cases: 99.03279220779221
Instances selection time: 1.1
Test time: 1.1
Accumulative iteration time: 81.0
Weighted Recall: 0.9196655844155843
Weighted FalsePositiveRate: 0.10579337624027889
Kappa statistic: 0.8232446527897498
Training time: 18.2
		
Iteration: 8
Labeled set size: 175
Unlabelled set size: 1
	
Mean region size: 79.14285714285715
Incorrectly Classified Instances: 7.085714285714286
Correctly Classified Instances: 92.91428571428571
Weighted Precision: 0.9298721807478849
Weighted AreaUnderROC: 0.9714285714285713
Root mean squared error: 0.24937946100248815
Relative absolute error: 30.582857142857158
Root relative squared error: 49.87589220049763
Weighted TruePositiveRate: 0.9291428571428572
Weighted MatthewsCorrelation: 0.8467098136557112
Weighted FMeasure: 0.9289549798601767
Iteration time: 20.2
Weighted AreaUnderPRC: 0.9672670233419348
Mean absolute error: 0.15291428571428578
Coverage of cases: 99.65714285714287
Instances selection time: 0.4
Test time: 1.0
Accumulative iteration time: 98.8
Weighted Recall: 0.9291428571428572
Weighted FalsePositiveRate: 0.08569047619047618
Kappa statistic: 0.84561151262258
Training time: 19.8
		
Time end:Tue Oct 31 13.17.50 EET 2017