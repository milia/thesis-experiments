Tue Oct 31 13.16.17 EET 2017
Dataset: ionosphere
Test set size: 175
Initial Labelled set size: 35
Initial Unlabelled set size: 141
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Tue Oct 31 13.16.17 EET 2017
		
Iteration: 1
Labeled set size: 35
Unlabelled set size: 141
	
Mean region size: 81.42857142857143
Incorrectly Classified Instances: 10.285714285714286
Correctly Classified Instances: 89.71428571428571
Weighted Precision: 0.897939393939394
Weighted AreaUnderROC: 0.9312641723356009
Root mean squared error: 0.30387967543561883
Relative absolute error: 39.31428571428574
Root relative squared error: 60.77593508712377
Weighted TruePositiveRate: 0.8971428571428571
Weighted MatthewsCorrelation: 0.7744506307449684
Weighted FMeasure: 0.8954295733489188
Iteration time: 14.0
Weighted AreaUnderPRC: 0.9191393270338853
Mean absolute error: 0.19657142857142867
Coverage of cases: 98.28571428571429
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 14.0
Weighted Recall: 0.8971428571428571
Weighted FalsePositiveRate: 0.14813492063492062
Kappa statistic: 0.770408163265306
Training time: 13.0
		
Iteration: 2
Labeled set size: 55
Unlabelled set size: 121
	
Mean region size: 87.71428571428571
Incorrectly Classified Instances: 12.0
Correctly Classified Instances: 88.0
Weighted Precision: 0.8806611570247933
Weighted AreaUnderROC: 0.9446570294784582
Root mean squared error: 0.3041616111778172
Relative absolute error: 41.02857142857152
Root relative squared error: 60.83232223556344
Weighted TruePositiveRate: 0.88
Weighted MatthewsCorrelation: 0.7360840388666623
Weighted FMeasure: 0.877702211951139
Iteration time: 25.0
Weighted AreaUnderPRC: 0.9370462167358639
Mean absolute error: 0.20514285714285757
Coverage of cases: 100.0
Instances selection time: 10.0
Test time: 2.0
Accumulative iteration time: 39.0
Weighted Recall: 0.88
Weighted FalsePositiveRate: 0.17166666666666666
Kappa statistic: 0.7311827956989247
Training time: 15.0
		
Iteration: 3
Labeled set size: 75
Unlabelled set size: 101
	
Mean region size: 78.57142857142857
Incorrectly Classified Instances: 11.428571428571429
Correctly Classified Instances: 88.57142857142857
Weighted Precision: 0.884979544126242
Weighted AreaUnderROC: 0.923327664399093
Root mean squared error: 0.30284366546831654
Relative absolute error: 37.37142857142858
Root relative squared error: 60.568733093663305
Weighted TruePositiveRate: 0.8857142857142857
Weighted MatthewsCorrelation: 0.7494396893960719
Weighted FMeasure: 0.8848432556801842
Iteration time: 29.0
Weighted AreaUnderPRC: 0.9178792511899976
Mean absolute error: 0.18685714285714292
Coverage of cases: 97.71428571428571
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 68.0
Weighted Recall: 0.8857142857142857
Weighted FalsePositiveRate: 0.14761904761904762
Kappa statistic: 0.7484909456740442
Training time: 28.0
		
Iteration: 4
Labeled set size: 95
Unlabelled set size: 81
	
Mean region size: 79.42857142857143
Incorrectly Classified Instances: 9.142857142857142
Correctly Classified Instances: 90.85714285714286
Weighted Precision: 0.92
Weighted AreaUnderROC: 0.9434523809523809
Root mean squared error: 0.2937443008565684
Relative absolute error: 35.885714285714315
Root relative squared error: 58.74886017131368
Weighted TruePositiveRate: 0.9085714285714286
Weighted MatthewsCorrelation: 0.8079466429027217
Weighted FMeasure: 0.904969696969697
Iteration time: 41.0
Weighted AreaUnderPRC: 0.9384167868773283
Mean absolute error: 0.17942857142857158
Coverage of cases: 98.85714285714286
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 109.0
Weighted Recall: 0.9085714285714286
Weighted FalsePositiveRate: 0.16253968253968254
Kappa statistic: 0.7899159663865547
Training time: 40.0
		
Iteration: 5
Labeled set size: 115
Unlabelled set size: 61
	
Mean region size: 75.42857142857143
Incorrectly Classified Instances: 9.714285714285714
Correctly Classified Instances: 90.28571428571429
Weighted Precision: 0.904573002754821
Weighted AreaUnderROC: 0.9597505668934241
Root mean squared error: 0.26131535409484485
Relative absolute error: 27.314285714285734
Root relative squared error: 52.26307081896897
Weighted TruePositiveRate: 0.9028571428571428
Weighted MatthewsCorrelation: 0.7876305401878569
Weighted FMeasure: 0.9009970287223505
Iteration time: 57.0
Weighted AreaUnderPRC: 0.9550437430921737
Mean absolute error: 0.13657142857142868
Coverage of cases: 99.42857142857143
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 166.0
Weighted Recall: 0.9028571428571428
Weighted FalsePositiveRate: 0.14492063492063492
Kappa statistic: 0.7823860727086532
Training time: 56.0
		
Iteration: 6
Labeled set size: 135
Unlabelled set size: 41
	
Mean region size: 69.71428571428571
Incorrectly Classified Instances: 8.571428571428571
Correctly Classified Instances: 91.42857142857143
Weighted Precision: 0.9185115697310818
Weighted AreaUnderROC: 0.9574121315192744
Root mean squared error: 0.26500673845880096
Relative absolute error: 25.257142857142874
Root relative squared error: 53.001347691760195
Weighted TruePositiveRate: 0.9142857142857143
Weighted MatthewsCorrelation: 0.8148380100109122
Weighted FMeasure: 0.9121924144310823
Iteration time: 52.0
Weighted AreaUnderPRC: 0.9520158273215678
Mean absolute error: 0.12628571428571436
Coverage of cases: 98.85714285714286
Instances selection time: 6.0
Test time: 3.0
Accumulative iteration time: 218.0
Weighted Recall: 0.9142857142857143
Weighted FalsePositiveRate: 0.1384920634920635
Kappa statistic: 0.8066013408973697
Training time: 46.0
		
Iteration: 7
Labeled set size: 155
Unlabelled set size: 21
	
Mean region size: 74.0
Incorrectly Classified Instances: 9.142857142857142
Correctly Classified Instances: 90.85714285714286
Weighted Precision: 0.9114259201979585
Weighted AreaUnderROC: 0.9361536281179138
Root mean squared error: 0.2834481560053921
Relative absolute error: 29.485714285714337
Root relative squared error: 56.68963120107841
Weighted TruePositiveRate: 0.9085714285714286
Weighted MatthewsCorrelation: 0.8010883163763989
Weighted FMeasure: 0.9065841438255232
Iteration time: 48.0
Weighted AreaUnderPRC: 0.9297559707526609
Mean absolute error: 0.1474285714285717
Coverage of cases: 98.28571428571429
Instances selection time: 7.0
Test time: 2.0
Accumulative iteration time: 266.0
Weighted Recall: 0.9085714285714286
Weighted FalsePositiveRate: 0.1417063492063492
Kappa statistic: 0.7944501541623844
Training time: 41.0
		
Iteration: 8
Labeled set size: 175
Unlabelled set size: 1
	
Mean region size: 75.42857142857143
Incorrectly Classified Instances: 8.571428571428571
Correctly Classified Instances: 91.42857142857143
Weighted Precision: 0.9165289256198347
Weighted AreaUnderROC: 0.9602465986394557
Root mean squared error: 0.26886002731107905
Relative absolute error: 29.82857142857145
Root relative squared error: 53.77200546221581
Weighted TruePositiveRate: 0.9142857142857143
Weighted MatthewsCorrelation: 0.8134037908484545
Weighted FMeasure: 0.9126444371079564
Iteration time: 52.0
Weighted AreaUnderPRC: 0.9558333600256271
Mean absolute error: 0.14914285714285724
Coverage of cases: 99.42857142857143
Instances selection time: 5.0
Test time: 2.0
Accumulative iteration time: 318.0
Weighted Recall: 0.9142857142857143
Weighted FalsePositiveRate: 0.13154761904761905
Kappa statistic: 0.8079877112135176
Training time: 47.0
		
Time end:Tue Oct 31 13.16.18 EET 2017