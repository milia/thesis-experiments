Tue Oct 31 13.17.45 EET 2017
Dataset: ionosphere
Test set size: 176
Initial Labelled set size: 35
Initial Unlabelled set size: 140
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Tue Oct 31 13.17.45 EET 2017
		
Iteration: 1
Labeled set size: 35
Unlabelled set size: 140
	
Mean region size: 50.0
Incorrectly Classified Instances: 18.75
Correctly Classified Instances: 81.25
Weighted Precision: 0.8274671052631579
Weighted AreaUnderROC: 0.818865009130496
Root mean squared error: 0.4330127018922193
Relative absolute error: 37.5
Root relative squared error: 86.60254037844386
Weighted TruePositiveRate: 0.8125
Weighted MatthewsCorrelation: 0.6172193889262022
Weighted FMeasure: 0.8155461546255952
Iteration time: 1.0
Weighted AreaUnderPRC: 0.7744722569595475
Mean absolute error: 0.1875
Coverage of cases: 81.25
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 1.0
Weighted Recall: 0.8125
Weighted FalsePositiveRate: 0.1747699817390083
Kappa statistic: 0.6098871574422353
Training time: 1.0
		
Iteration: 2
Labeled set size: 55
Unlabelled set size: 120
	
Mean region size: 61.93181818181818
Incorrectly Classified Instances: 17.045454545454547
Correctly Classified Instances: 82.95454545454545
Weighted Precision: 0.8395957513692586
Weighted AreaUnderROC: 0.8756847871892119
Root mean squared error: 0.3818038079004356
Relative absolute error: 34.375
Root relative squared error: 76.36076158008713
Weighted TruePositiveRate: 0.8295454545454546
Weighted MatthewsCorrelation: 0.6463678922854207
Weighted FMeasure: 0.831866458704694
Iteration time: 3.0
Weighted AreaUnderPRC: 0.8528933458933579
Mean absolute error: 0.171875
Coverage of cases: 94.31818181818181
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 4.0
Weighted Recall: 0.8295454545454546
Weighted FalsePositiveRate: 0.16526676371809115
Kappa statistic: 0.6417424345230018
Training time: 2.0
		
Iteration: 3
Labeled set size: 75
Unlabelled set size: 100
	
Mean region size: 59.375
Incorrectly Classified Instances: 13.068181818181818
Correctly Classified Instances: 86.93181818181819
Weighted Precision: 0.869825487012987
Weighted AreaUnderROC: 0.8857283326309875
Root mean squared error: 0.3414564208998528
Relative absolute error: 29.289772727272762
Root relative squared error: 68.29128417997056
Weighted TruePositiveRate: 0.8693181818181818
Weighted MatthewsCorrelation: 0.7167397092446456
Weighted FMeasure: 0.8695422731249504
Iteration time: 4.0
Weighted AreaUnderPRC: 0.8660913709135691
Mean absolute error: 0.1464488636363638
Coverage of cases: 93.75
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 8.0
Weighted Recall: 0.8693181818181818
Weighted FalsePositiveRate: 0.15011604668684314
Kappa statistic: 0.7166853303471443
Training time: 4.0
		
Iteration: 4
Labeled set size: 95
Unlabelled set size: 80
	
Mean region size: 50.85227272727273
Incorrectly Classified Instances: 13.636363636363637
Correctly Classified Instances: 86.36363636363636
Weighted Precision: 0.8663002751048758
Weighted AreaUnderROC: 0.8786346396965867
Root mean squared error: 0.3524236403735464
Relative absolute error: 29.67836257309944
Root relative squared error: 70.48472807470928
Weighted TruePositiveRate: 0.8636363636363636
Weighted MatthewsCorrelation: 0.7082811945363152
Weighted FMeasure: 0.8645057645057644
Iteration time: 7.0
Weighted AreaUnderPRC: 0.8512306412039593
Mean absolute error: 0.1483918128654972
Coverage of cases: 87.5
Instances selection time: 1.0
Test time: 0.0
Accumulative iteration time: 15.0
Weighted Recall: 0.8636363636363636
Weighted FalsePositiveRate: 0.14626032767625688
Kappa statistic: 0.7074387034215265
Training time: 6.0
		
Iteration: 5
Labeled set size: 115
Unlabelled set size: 60
	
Mean region size: 50.85227272727273
Incorrectly Classified Instances: 12.5
Correctly Classified Instances: 87.5
Weighted Precision: 0.8775332055319732
Weighted AreaUnderROC: 0.8865009130495857
Root mean squared error: 0.3402878915215198
Relative absolute error: 30.501893939393977
Root relative squared error: 68.05757830430396
Weighted TruePositiveRate: 0.875
Weighted MatthewsCorrelation: 0.7326903369366268
Weighted FMeasure: 0.8757969507969509
Iteration time: 8.0
Weighted AreaUnderPRC: 0.8591104328860178
Mean absolute error: 0.15250946969696988
Coverage of cases: 88.06818181818181
Instances selection time: 1.0
Test time: 0.0
Accumulative iteration time: 23.0
Weighted Recall: 0.875
Weighted FalsePositiveRate: 0.13290139064475348
Kappa statistic: 0.7318188114697326
Training time: 7.0
		
Iteration: 6
Labeled set size: 135
Unlabelled set size: 40
	
Mean region size: 50.0
Incorrectly Classified Instances: 15.909090909090908
Correctly Classified Instances: 84.0909090909091
Weighted Precision: 0.8411229946524064
Weighted AreaUnderROC: 0.813597415367327
Root mean squared error: 0.39266804898176816
Relative absolute error: 33.91381345926802
Root relative squared error: 78.53360979635363
Weighted TruePositiveRate: 0.8409090909090909
Weighted MatthewsCorrelation: 0.6464554011142059
Weighted FMeasure: 0.8365465803546299
Iteration time: 13.0
Weighted AreaUnderPRC: 0.7843111590845844
Mean absolute error: 0.1695690672963401
Coverage of cases: 84.0909090909091
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 36.0
Weighted Recall: 0.8409090909090909
Weighted FalsePositiveRate: 0.2291658685463995
Kappa statistic: 0.6386566945299897
Training time: 13.0
		
Iteration: 7
Labeled set size: 155
Unlabelled set size: 20
	
Mean region size: 50.0
Incorrectly Classified Instances: 17.045454545454547
Correctly Classified Instances: 82.95454545454545
Weighted Precision: 0.8303379691174967
Weighted AreaUnderROC: 0.7728613569321534
Root mean squared error: 0.40563065860282566
Relative absolute error: 36.98680351906163
Root relative squared error: 81.12613172056513
Weighted TruePositiveRate: 0.8295454545454546
Weighted MatthewsCorrelation: 0.6203481421431186
Weighted FMeasure: 0.8238636363636364
Iteration time: 12.0
Weighted AreaUnderPRC: 0.7550005553993926
Mean absolute error: 0.18493401759530814
Coverage of cases: 82.95454545454545
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 48.0
Weighted Recall: 0.8295454545454546
Weighted FalsePositiveRate: 0.24954826392879487
Kappa statistic: 0.609986704092185
Training time: 12.0
		
Time end:Tue Oct 31 13.17.46 EET 2017