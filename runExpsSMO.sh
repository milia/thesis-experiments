#!/bin/bash

# bash for loop
for f in $( ls thesis-experiments/arffs/twentyUCI/ | sed 's/.arff//' ); do
	echo "starting experiment for $f"
	java -jar jclal-1.1.jar -d thesis-experiments/cfgs/SMO/$f.cfgs
	sleep 2s
	#echo $f.cfgs
	echo "cleaning reports"
	cd reports/
	mkdir reports-SMO-$f
	mv *.txt reports-SMO-$f
	zip -r reports-SMO-$f.zip reports-SMO-$f/
	cd ..
	echo "finished cleaning"
	echo "sleeping 10s"
	sleep 10s
done 
