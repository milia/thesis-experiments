#!/usr/bin/python

import xml.etree.ElementTree as ET
from sultan.api import Sultan

algorithms = ['lazy.IBk',
			 'bayes.NaiveBayes'
			 ,'trees.J48',
			 'functions.SMOsync',
			 'trees.RandomForest']

# these use net.sf.jclal.classifier.WekaClassifier wrapper classifier
strategies = ['EntropySamplingQueryStrategy',
			'LeastConfidentSamplingQueryStrategy',
			'MarginSamplingQueryStrategy',
			'RandomSelectionQueryStrategy']

## these use net.sf.jclal.classifier.WekaComitteClassifier wrapper classifier and must
#  dealt with seperately
otherstrategies = ['KullbackLeiblerDivergenceQueryStrategy',
				   'VoteEntropyQueryStrategy']

def cfgs(algo, strategy, datafile, templatexml, xmlname):
'''
This function creates the *.cfg files for all the datasets and for all combinations of the above algorithms and strategies.

Input:
--------
algo: the name of the algorithm
strategy: the name of the strategy
datafile: the name of the arff data file
templatexml: the XML file that we use as a template

Output:
-------
output: xmlname + ".cfg"
'''
	# choose datafile that you'll change
	tree = ET.parse(templatexml)
	root = tree.getroot()

	# change arff address
	root[0][2].text = "thesis-experiments/arffs/twentyUCI/" + datafile
	root[0][2].set('updated','yes') 	

	# change percentage to select
	# for percentage in root.iter('persentage-to-select'):
	#  	print percentage.tag, persentage.attrib
	# for sampling in root.iter('sampling-method'):
	# 	##percentage = root.find('percentage-to-select')
	# 	for percentage in sampling.iter('percentage-to-select'):
	# 		print  percentage.text, "ASD" # data file


	for algorithm in root.iter('algorithm'):
		for listener in algorithm.iter('listener'):
			for report in listener.iter('report-title'):
				#print report.text
				report.text = strategy + "." + algo
				report.set('updated','yes')

	# change query strategy
	for query_strategy in root.iter('query-strategy'):		
		strategyAddress = 'net.sf.jclal.activelearning.singlelabel.querystrategy.' + strategy
		query_strategy.attrib = {'type': str(strategyAddress)}
		query_strategy.set('updated','yes')

	# change classifier
	for classifier in root.iter('classifier'):
		algorithmAddress = "weka.classifiers." + algo
		classifier.attrib = {'type': str(algorithmAddress)}
		classifier.set('updated','yes')

	# write data to xml file
	output = xmlname + ".cfg"
	tree.write(output)


def main():
	s1 = Sultan()
	res = s1.ls('arffs/twentyUCI').run()
	datafiles = res.stdout

	for d in datafiles:
		for a in algorithms:
			for s in strategies:
				xmlname = s + "-" + str(a)
				filename = d
				cfgs(a, s, filename, 'EntropySamplingQueryStrategy.xml', xmlname)
			for s2 in otherstrategies:
				xmlname = s2 + "-" + str(a)
				filename = d
				cfgs(a, s2, filename, 'VoteEntropyQueryStrategy.xml', xmlname)  

		# Calling Sultan to tidy things up by
		# creating a dir for every datafile
		# and putting the relevant cfg files there			
		s = Sultan()
		direc = d[:-5] + ".cfgs" # keeping only the filenames
		s.mkdir(direc).run()
		s.mv("*.cfg",direc).run()
	s.mkdir("final").run()
	s.mv("*.cfgs","final").run()
	s.mv("final","cfgs").run()

main()
